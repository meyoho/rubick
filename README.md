# Rubick

![Rubick](rubick.jpg?raw=true)

## Quick start with local development environment

1. install node, npm

- nodejs and npm on your local environment (node version: ~10.1.0) download from https://nodejs.org/

2. install dependencies

```shell
cd rubick
git pull
npm install
cd rubick/koa
npm install
```

> When executing the statement :"npm install",<br> some problem such as "mozjpeg pre-build test failed" on mac, try execute statement:

```shell
brew install automake autoconf libtool dpkg pkgconfig nasm libpng
```

3. start local development server

Before doing the following operations, create a file named `custom.env` in project root folder, url may change in the future, ask old drivers.

custom.env

```
# CN API
API_SERVER_URL=http://api.alauda.cn
```

> There are a few useful environment variables on development

```
MOCK_FIRST_ENABLED=true # try to mock request before forwarding to jakiro
TRY_MOCK_ON_ERROR_ENABLED=true # only try to mock request on jakiro error, MOCK_FIRST_ENABLED should not be set, or `TRY_MOCK_ON_ERROR_ENABLED` will be ignored
```

> You perhaps encounter such a hint "/var/log/mathilde not found" . <br> Then you need to create a folder "mathilde" in "/var/log". If you meet `permission denied` error, you will need to run `sudo chmod a+rw- /var/log/mathilde` then.

> If you are not developing legacy Angular.js App projects, you can override weblab LEGACY_ANGULAR_JS_ENABLED to false by setting LIGHTKEEPER_OVERRIDDEN_WEBLABS environment, check int.env for example.

OK, you will be able to start running our App through:

```shell
npm start
```

if you want to start running our main app with terminal app:

```shell
npm run start:terminal
```

4. All things are ready now! Unbelievable! Go to check it at `http://localhost:3000`.

5. maybe you want to access webpack dev server via LAN:

```shell
// please replace the part of IP with your own computer in *build/webpack.dev.js*
publicPath: 'http://192.168.1.137:3000/'
```

## Quick start with docker

1. Specify the rubick image for local dev(you'd better put'em in /user/.bash_profile):

```shell
export RUBICK_IMAGE=index.alauda.cn/alaudaorg/rubick:latest
```

2. Login docker client with your docker account, then login index.alauda.cn:

```shell
docker login index.alauda.cn
username: org/sub
pass: pass
```

3. Launch the server

```shell
docker pull $RUBICK_IMAGE
docker run --name rubick --env-file int.env -p 3000:80 -d $RUBICK_IMAGE
```

4. All things are ready now ! Unbelievable! Go to check it at `http://localhost:3000`.

### Useful commands in local dev

#### Check logs

```shell
docker logs -f rubick --tail 100
```

> `-f` alwarys follow the logs. `--tail <number>` specify tail numbers.

#### Check the service status

After you run `docker run --name rubick --env-file int.env -p 3000:80 -d $RUBICK_IMAGE`, the service named `rubick` will start running in the background. You can use the following commands to check the service status:

```shell
docker ps
```

#### Restart service

```shell
docker restart rubick
```

#### Restart backend servcie only (to enforce changes to envfile)

```
docker stop rubick
docker rm -f rubick
docker run --name rubick --env-file int.env -p 3000:80 -d $RUBICK_IMAGE
```

#### Use local dev code launch the service

```
npm run build
docker stop rubick
docker rm -f rubick
docker run --name rubick --env-file int.env -p 3000:80 -v `pwd`/static:/rubick/static -v `pwd`/koa:/rubick/koa -v `pwd`/templates:/rubick/templates $RUBICK_IMAG
```

#### Debug inside the running service

> You can debug into the service that running background use `exec` command:

- Exec into the service: `docker exec -it rubick /bin/sh`

### Explore more

All above powers are given by the docker. <br> Read the [docker docs](https://docs.docker.com/) to explore more.

## Quick start with docker compose

This development mode is just suggested for special development environment, such as windows or no internet office.

1. specify the compose base image

```shell
export RUBICK_DEV_IMAGE=index.alauda.cn/alaudaorg/rubick-dev:latest
```

2. pull the rubick-dev image

```shell
docker pull $RUBICK_DEV_IMAGE
```

3. clone rubick source code and run `docker-compose up` in project root folder, now you can start to develop

Note: the rubick-dev image version need to be update to date with npm package version.

## Lazy Loading Modules Notes

You should prevent importing `BrowserModule` again in lazy loaded modules, so make sure no `BrowserModule` is imported by yourself or third party packages(`BrowserAnimationsModule` from `@angular/platform-browser/animations` for example).

And also, you should only use `TranslateModule.forChild` if you need to access `TranslateModule` except App Root module. You can easily use `import { TranslateModule } from 'app2/core/translate/translate.module';` for most cases.

Legacy `routerUtil` will not work with lazy loaded routes because it could not recognize lazy loaded route configs, and then it will not be able to handle route params. So, just use official `Router#navigate` instead.

You can check `app2/app.routing.module.ts (ImageModule)` and `app2/features/image/image.routing.module.ts` for example.

If there are two or three lazy loading modules sharing same components, you'd better to add a lazy shared module in `features/lazy` folder, take `features/lazy/build-image-downgrade.shared.module.ts` for example.

## Useful npm scripts

```shell
npm run start # run main app
npm run start:terminal # run main app with terminal app
npm run start:pm2 # run koa in pm2 mode and run main app

npm run lint:common # lint most types of files
npm run lint:ts # lint all .ts files specific
npm run lint:fix # concat lint:common && lint:ts
```

## quick debug with vscode

1. debug node sever

- need the custom.env ([worked example](http://confluence.alaudatech.com/display/FRON/custom.env)) file to provide vscode runtime env
- start debug in vscode -> debug -> koa debug

2. debug angular

- start webpack dev server with npm script `npm run webpack:dev`
- start debug in vscode -> debug -> angular debug

Do not forget to restart debug process when you've changed the source codes.

## Specification and examples of commit msg

1. commit msg specification:

```
feat (new feature)
fix (bug fix)
docs (changes to documentation)
style (formatting, missing semi colons, etc; no code change)
refactor (refactoring production code)
test (adding missing tests, refactoring tests; no production code change)
chore (updating grunt tasks etc; no production code change)
```

2. commit msg examples:

```
feat: add beta sequence
fix: remove broken confirmation message
docs: explain hat wobble
style: convert tabs to spaces
refactor: share logic between 4d3d3d3 and flarhgunnstow
test: ensure Tayne retains clothing
chore: add Oyster build script
```

3. commit msg with JIRA issue link (this is the new specification), eg:

```
chore: remove weblab SERVICE_LINKS_GRAPH_ENABLED - #DEV-1153
```

## Build image with dockerfile

You can build rubick image locally using `rubick/dockerfiles/prod-alpine`. It bases on rubick-base which is base on alpine linux to decrease image size.

[rubick-base](https://enterprise.alauda.cn/console/image/repository/detail?repositoryName=rubick-base&registryName=alauda_public_registry) image provides openresty and node running environment. It is built by `rubick/dockerfiles/rubick-base/Dockerfile`.

## Build base image with dockerfile

You can build rubick base image locally using `rubick/dockerfiles/rubick-base`. It will be built with openresty and nodejs.

Also, you can use rubick-base build config to build it in Alauda EE. The image will be pushed to rubick-base-staging repository, and you need to test it and push it to rubic-base manually after testing is fine.

## Q&A

Q: What does the environment variable LOCAL_BACKEND_ENDPOINT do?

A: The default value of LOCAL_BACKEND_ENDPOINT is “http://127.0.0.1:2333”, you can setting LOCAL_BACKEND_ENDPOINT to change nginx "location / { proxy_pass }" and koa "default_port"

Q: Where is the entry of legacy Angular.js App now?
A: Check `static/app2/features/downgrade/downgrade.module.ts`
