import { Context } from 'koa';
import * as _ from 'lodash';

import {
  extract_favicon_path_from_envvar,
  getConfigFile,
} from '../common/util';
import weblabClient from '../common/weblab_client';
import config from '../config/config';

class Console {
  constructor() {}

  async weblabs(ctx: Context) {
    const feature_flags = await weblabClient.prefetch(
      _.get(ctx.query, 'namespace'),
    );
    ctx.body = {
      feature_flags: feature_flags,
    };
  }

  async navConfig(ctx: Context) {
    let nav_config = await getConfigFile('nav-config.yaml');
    if (ctx.params.view && nav_config) {
      nav_config = nav_config[ctx.params.view] || null;
    }
    ctx.body = {
      nav_config: nav_config,
    };
  }

  async token(ctx: Context) {
    ctx.body = {
      token: ctx.user.token,
    };
  }

  async consoleView(ctx: Context) {
    const favicon_path = extract_favicon_path_from_envvar();
    const namespace = _.get(ctx, 'user.namespace', '');
    const username = _.get(ctx, 'user.username');
    const data = namespace
      ? {
          namespace,
          username,
        }
      : {
          username: '',
          namespace: username,
        };
    data['favicon_path'] = favicon_path;
    await ctx.render(
      'index.html',
      Object.assign({}, config.viewBasicData, {
        ...data,
        ...config.browserData,
      }),
    );
  }

  async terminalView(ctx: Context) {
    const favicon_path = extract_favicon_path_from_envvar();
    await ctx.render('terminal/index.html', {
      favicon_path: favicon_path,
      ...config.browserData,
    });
  }

  async currentTime(ctx: Context) {
    ctx.body = { serverTime: new Date().getTime() };
  }
}

const consoleService = new Console();
export default consoleService;
