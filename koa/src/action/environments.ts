import { Context } from 'koa';
import { mapValues } from 'lodash';

import { JakiroRequest } from '../common/request';
import { getEnv, processStringType } from '../common/util';
import { OidcConfig } from './sso';

class Environments {
  constructor() {}

  private _handleEnv(options: any) {
    return mapValues(options, val => {
      return processStringType(val);
    });
  }

  getEnvironments = async (ctx: Context): Promise<any> => {
    const env = {
      debug: getEnv('DEBUG'),
      alauda_image_index: getEnv('DOCKER_INDEX'),
      is_private_deploy_enabled: getEnv('PRIVATE_DEPLOY_ENABLED'),
      user_docs_url: getEnv('USER_DOCS_URL'),
      predefined_login_organization: getEnv('PREDEFINED_LOGIN_ORGANIZATION'),
      overridden_logo_sources: getEnv('OVERRIDDEN_LOGO_SOURCES'),
      vendor_customer: getEnv('VENDOR_CUSTOMER', 'PLATFORM'),
      sso_guide_page_on: getEnv('SSO_GUIDE_PAGE_ON'),
      login_pattern: getEnv('LOGIN_PATTERN'),
      default_quota: getEnv('DEFAULT_QUOTA'),
      outsourcing_image_index: getEnv('OUTSOURCING_IMAGE_INDEX'),
      label_base_domain: getEnv('LABEL_BASE_DOMAIN', 'alauda.io'),
      auto_logout_latency: getEnv('AUTO_LOGOUT_LATENCY', 720),
      browser_download_url: getEnv('BROWSER_DOWNLOAD_URL'),
      staged_namespace_create: getEnv('STAGED_NAMESPACE_CREATE'),
      ajax_request_timeout: getEnv('AJAX_REQUEST_TIMEOUT'),
      landing_bg_url: getEnv('LANDING_BG_URL'),
      embedded_mode: getEnv('EMBEDDED_MODE'),
      docker_versions: getEnv('DOCKER_VERSIONS', '1.12.6,18.09.2'),
      log_ttl: getEnv('LOG_TTL'),
      event_ttl: getEnv('EVENT_TTL'),
      api_market_url: getEnv('API_MARKET_URL'),
    };
    if (getEnv('SSO_CONFIG_REQUIRED')) {
      // SSO_CONFIG_REQUIRED,告诉koa层需不需要去获取sso相关的configs
      env['sso_configuration'] = await this.getSsoConfiguration(ctx);
    }
    const options = this._handleEnv(env);
    ctx.body = options;
  };

  private getSsoConfiguration = async (
    ctx: Context,
  ): Promise<{ configs: OidcConfig[]; default_org_name: string }> => {
    const tpName = getEnv('SSO_TP_NAME') || 'oidc';
    return JakiroRequest({
      ctx,
      method: 'GET',
      path: `/v1/tp_sso/${tpName}/configuration`,
    });
  };

  getSsoConfigContent = async (ctx: Context): Promise<string> => {
    const configuration = await this.getSsoConfiguration(ctx);
    return JSON.stringify(configuration.configs);
  };
}

const environmentsService = new Environments();
export default environmentsService;
