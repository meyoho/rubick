import { IncomingMessage } from 'http';
import { Context } from 'koa';
import * as multiparty from 'multiparty';

import { JakiroRequest, JakiroRequestOption } from '../common/request';
import { getCommonHeaders, getEnv } from '../common/util';

const AJAX_REGEXP = /^\/ajax\//;
const STATIC_REGEXP = /^[^?]+\.(css|gif|html|jpe?g|js(on)?|png|svg)(\?.*)*$/i;
const HEADER_AJAX_REQUEST = 'RUBICK-AJAX-REQUEST';
const HEDER_CONTENT_TYPE = 'Content-Type';

const getPartStream = (req: IncomingMessage) => {
  const form: multiparty.Form = new multiparty.Form();
  const parts = {};
  return new Promise((resolve, reject) => {
    form.on('part', part => {
      if (!part.filename) {
        // filename is not defined when this is a field and not a file
        parts[part.name] = part;
      }

      if (part.filename) {
        // filename is defined when this is a file
        parts[part.name] = part;
        resolve(parts);
      }

      part.on('error', function(err) {
        reject(err);
      });
    });
    form.parse(req);
  });
};

export default async function redirect(
  ctx: Context,
  next: Function,
): Promise<any> {
  const mockFirstEnabled = getEnv('MOCK_FIRST_ENABLED');

  if (mockFirstEnabled) {
    await next();
  }

  // mock method not allowed should also be ignored
  if (![404, 405].includes(ctx.status)) {
    return;
  }

  let body, status;

  // this middleware is always after koa-router, so ctx.matched will never be undefined
  // mock routes may not implement specific method for some urls but ctx.matched macthes, which will cause 405 NOT ALLOWED error, so just disable judgement `!ctx.matched.length`
  // following judgements should be fine enough
  if (
    AJAX_REGEXP.test(ctx.url) &&
    ctx.get(HEADER_AJAX_REQUEST) === 'true' &&
    !STATIC_REGEXP.test(ctx.url)
  ) {
    const option: JakiroRequestOption = {
      ctx: ctx,
      method: ctx.method,
      path: ctx.path,
      args: {
        data: ctx.request.body,
        query: ctx.query,
      },
      proxy: true,
      headers: getCommonHeaders(ctx),
    };

    if (
      ctx.get(HEDER_CONTENT_TYPE) &&
      ctx.get(HEDER_CONTENT_TYPE).includes('multipart/form-data')
    ) {
      option.formData = await getPartStream(ctx.req);
      option.headers['Transfer-Encoding'] = 'chunked';
      delete option.args.data;
    }

    const res = await JakiroRequest(option);
    body = Array.isArray(res.result) ? res : res.result;
    status = res.code || 200;
  }

  // if mockFirst not enabled and jakiro can not handle the request, try next middleware
  if (
    !mockFirstEnabled &&
    (!status || (getEnv('TRY_MOCK_ON_ERROR_ENABLED') && status >= 400))
  ) {
    await next();
  }

  // when status exists and next middleware can not handle the request, use origin generated jakiro result
  if (status && [404, 405].includes(ctx.status)) {
    ctx.body = body;
    ctx.status = status;
  }
}
