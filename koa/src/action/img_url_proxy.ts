import { Context } from 'koa';

import { Request } from '../common/request';

class ImageProxy {
  async proxy(ctx: Context) {
    const { url } = ctx.query;
    let rep;
    try {
      const { statusCode, body, headers } = await Request({
        url,
        encoding: null,
      });
      const type = headers['content-type'];
      const prefix = `data:${type};base64,`;

      rep = {
        code: statusCode,
        body: prefix + body.toString('base64'),
      };
    } catch (err) {
      rep = {
        code: 502,
        body: err.message,
      };
    }
    ctx.body = rep;
  }
}
const imageproxy = new ImageProxy();
export default imageproxy;
