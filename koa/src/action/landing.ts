import * as captchapng from 'captchapng';
import * as crypto from 'crypto';
import { Context } from 'koa';
import { get } from 'lodash';
import * as qs from 'querystring';

import {
  MOBILE_CODE_COOKIE_NAME,
  SESSION_COOKIE_NAME,
} from '../common/constants';
import { DEFAULT_REDIRECT_URL } from '../common/constants';
import { JakiroRequest } from '../common/request';
import { send } from '../common/sms';
import {
  extract_favicon_path_from_envvar,
  getCommonHeaders,
  getDecAse192,
  getEncAse192,
} from '../common/util';
import { getEnv } from '../common/util';
import config from '../config/config';
import rbLogger from '../logger/logger';
import { Dictionary } from '../types';

const logger = rbLogger.logger;
const LOGIN_ATTEMPTS_THRESHOLD = 3;
const CAPTCHA_NAME_COOKIE = 'captcha';
const AES_BLOCK_SIZE = 32;
const LOGIN_ATTEMPTS_COOKIE_PREFIX = 'login_attempts_cookie_name';

class Landing {
  imageData: any;

  constructor() {}

  // captcha --------------
  private getImage(rstring: string) {
    // width,height,numeric captcha
    const p = new captchapng(80, 30, rstring);
    // First color: background (red, green, blue, alpha)
    p.color(0, 0, 0, 0);
    // Second color: paint (red, green, blue, alpha)
    p.color(80, 80, 80, 255);
    const img = p.getBase64();
    return new Buffer(img, 'base64');
  }

  private genRedirectUrl(user, redirect_url): string {
    const username = user.username,
      organization = get(user, 'namespace', username);
    const cipher_key = `${config.loginSecretKey}${organization}${username}`;
    const hash = crypto
      .createHash('sha256')
      .update(cipher_key)
      .digest('hex');
    const cipher = crypto.createCipheriv(
      'aes-256-ecb',
      hash.substring(0, 32),
      '',
    );

    let combined_token = `${organization};${username};${parseInt(
      new Date().getTime() / 1000 + '',
      10,
    )};${user.token}`;
    let padding;
    if ((AES_BLOCK_SIZE - combined_token.length) / AES_BLOCK_SIZE > 0) {
      padding = (AES_BLOCK_SIZE - combined_token.length) % AES_BLOCK_SIZE;
    } else {
      const d = Math.floor(
        (AES_BLOCK_SIZE - combined_token.length) / AES_BLOCK_SIZE,
      );
      padding = AES_BLOCK_SIZE - combined_token.length - d * AES_BLOCK_SIZE;
    }
    let cryptQ = '';
    if (padding > 0) {
      for (let i = 0; i < padding; i++) {
        cryptQ = cryptQ + '?';
      }
    }
    combined_token = combined_token + cryptQ;
    cipher.setAutoPadding(false);
    const encrpyted_token =
      cipher.update(combined_token, 'utf8', 'base64') + cipher.final('base64');
    const query = {
      token: encrpyted_token,
      username: username,
      organization: organization,
    };
    return `${redirect_url}?${qs.stringify(query)}`;
  }

  async captchaRefresh(ctx: Context, next: Function) {
    let rstring = parseInt(Math.random() * 9000 + 1000 + '', 10).toString();
    ctx.session[CAPTCHA_NAME_COOKIE] = rstring;
    rstring = getEncAse192(rstring);
    ctx.body = {
      image_url: '/captcha-image/' + rstring + '/',
    };
    ctx.status = 200;
    return next();
  }

  captchaImage = async (ctx: Context, next: Function) => {
    const pathArr = ctx.path.split('/');
    const rstring = getDecAse192(pathArr[pathArr.length - 2]);
    ctx.type = 'image/png';
    ctx.body = await this.getImage(rstring);
    ctx.status = 200;
    next();
  };

  async requiresCaptcha(ctx: Context, next: Function) {
    const COOKIE_NAME = ctx.query.mode
      ? `${LOGIN_ATTEMPTS_COOKIE_PREFIX}_${ctx.query.mode}`
      : LOGIN_ATTEMPTS_COOKIE_PREFIX;

    ctx.body = {
      result: ctx.session[COOKIE_NAME] >= 3,
    };
    ctx.status = 200;
    next();
  }

  private generateVericationCode(number: string) {
    const code = parseInt(Math.random() * 900000 + 100000 + '', 10).toString();
    number = `mobile:${number}`;
    logger.log(`Generationg code: ${code}, ${number}`);
    return code;
  }

  purePasswordLogin = async (ctx: Context, next: Function) => {
    ctx.session[LOGIN_ATTEMPTS_COOKIE_PREFIX] =
      (ctx.session[LOGIN_ATTEMPTS_COOKIE_PREFIX] || 0) + 1;

    if (ctx.session[LOGIN_ATTEMPTS_COOKIE_PREFIX] > LOGIN_ATTEMPTS_THRESHOLD) {
      if (ctx.session.captcha !== get(ctx, 'request.body.captcha')) {
        ctx.body = {
          errors: [{ code: 'invalid_captcha' }],
        };
        ctx.status = 401;
        return next();
      }
    }

    let returnUrl = '';
    const data: Dictionary = ctx.request.body;
    data['user_name'] = data.account;
    delete data.account;
    const tpName = getEnv('SSO_TP_NAME') || 'oidc';
    const result = await JakiroRequest({
      ctx,
      method: 'POST',
      path: `/tp_sso/${tpName}/login`,
      args: {
        data: data,
      },
    });
    const { is_valid, alauda_user_info: user } = result;
    if (is_valid && user.token) {
      if (user.is_first_login) {
        ctx.session.temporary_user = user;
        ctx.body = {
          isFirstLogin: true,
        };
      } else {
        ctx.session.user = user;
        ctx.user = user;
        if (data.next) {
          returnUrl = data.next;
        } else if (data.redirect_url) {
          returnUrl = this.genRedirectUrl(user, data.redirect_url);
        }
        const defaultRedirectUrl = DEFAULT_REDIRECT_URL;
        ctx.body = {
          url: returnUrl ? returnUrl : defaultRedirectUrl,
          namespace: ctx.user.namespace,
          username: ctx.user.username,
        };
      }
    } else {
      ctx.body = result;
      ctx.status = 400;
    }
  };

  async purePasswordLoginChangePassword(ctx: Context, next: Function) {
    let returnUrl = '';
    const { namespace, username, token } = ctx.session.temporary_user;
    const data: Dictionary = ctx.request.body;
    const { result, code } = await JakiroRequest({
      ctx,
      method: 'PUT',
      path: `/orgs/${namespace}/accounts/${username}`,
      proxy: true,
      headers: {
        Authorization: `Token ${token}`,
      },
      args: {
        data,
      },
    });
    if (/^2\.*/.test(code)) {
      ctx.session.user = ctx.session.temporary_user;
      ctx.user = ctx.session.temporary_user;
      ctx.session.temporary_user = null;
      if (data.next) {
        returnUrl = data.next;
      } else if (data.redirect_url) {
        returnUrl = this.genRedirectUrl(ctx.user, data.redirect_url);
      }
      const defaultRedirectUrl = DEFAULT_REDIRECT_URL;
      ctx.body = {
        url: returnUrl ? returnUrl : defaultRedirectUrl,
      };
    } else {
      ctx.body = result;
      ctx.status = 400;
    }
  }

  // login and register --------------------
  login = async (ctx: Context, next: Function) => {
    const COOKIE_NAME = get(ctx, 'request.body.mode')
      ? `${LOGIN_ATTEMPTS_COOKIE_PREFIX}_${get(ctx, 'request.body.mode')}`
      : LOGIN_ATTEMPTS_COOKIE_PREFIX;

    ctx.session[COOKIE_NAME] = (ctx.session[COOKIE_NAME] || 0) + 1;

    if (ctx.session[COOKIE_NAME] > LOGIN_ATTEMPTS_THRESHOLD) {
      const captchaT = ctx.session.captcha;
      if (captchaT !== get(ctx, 'request.body.captcha')) {
        ctx.body = {
          errors: [
            {
              code: 'invalid_captcha',
            },
          ],
        };
        ctx.status = 401;
        return next();
      }
    }
    let returnUrl = '';
    let data: Dictionary = ctx.request.body;
    if (data.email) {
      data = {
        email: data.email,
        password: data.password,
      };
    } else {
      data.username = data.account;
      delete data.account;
    }
    const headers = {
      CLIENT: ctx.ips.length ? ctx.ips.join(', ') : ctx.ip,
      ...getCommonHeaders(ctx),
    };

    const result = await JakiroRequest({
      ctx,
      method: 'POST',
      path: '/generate-api-token/',
      args: {
        data: data,
      },
      headers,
    });
    //  Currently only for api market entry
    if (
      data.next &&
      data.next.startsWith('http') &&
      result.token &&
      getEnv('API_MARKET_URL')
    ) {
      ctx.session.user = result;
      ctx.user = result;
      const tokens = await JakiroRequest({
        ctx,
        method: 'GET',
        path: `/v3/api-token`,
        headers: {
          ...headers,
          Authorization: `Token ${result.token}`,
        },
      });
      ctx.body = {
        url: `${data.next}?auth_url=${get(
          ctx,
          'request.header.origin',
          ctx.origin,
        )}/ap/logout&id_token=${tokens.kubernetes_id_token}&ace_token=${
          result.token
        }`,
      };
    } else if (result.token) {
      ctx.session.user = result;
      ctx.user = result;
      const profile = await JakiroRequest({
        ctx,
        method: 'GET',
        path: `/auth/${
          data.organization ? data.organization : data.username
        }/profile`,
      });
      if (profile.password_is_empty) {
        returnUrl = '/console/admin/user/password';
      } else if (data.redirect_url) {
        returnUrl = this.genRedirectUrl(result, data.redirect_url);
      } else if (data.next) {
        returnUrl = data.next;
      }
      //default login url
      if (!returnUrl || returnUrl === '/') {
        returnUrl = data.next || DEFAULT_REDIRECT_URL;
      }
      ctx.body = {
        url: returnUrl,
      };
    } else {
      ctx.body = result;
      ctx.status = 400;
    }
  };

  async register(ctx: Context, next: Function) {
    const data: Dictionary = ctx.request.body;
    const md5 = crypto.createHash('md5');
    if (
      ctx.cookies.get(MOBILE_CODE_COOKIE_NAME) ===
        md5.update(data.mobile + data.code).digest('hex') &&
      process.env.LOCALE === 'cn'
    ) {
      ctx.body = await JakiroRequest({
        ctx,
        method: 'POST',
        path: '/v1/auth/register ',
        args: {
          data: data,
        },
      });
      return next();
    } else {
      logger.debug(`code wrong`);
      ctx.body = {
        errors: [
          {
            code: 'mobile_vcode_invalid',
          },
        ],
      };
      ctx.status = 401;
    }
  }

  async resetPassword(ctx: Context, next: Function) {
    const data = ctx.request.body;
    const endpoint = '/reset-password';
    const res = await JakiroRequest({
      ctx,
      method: 'POST',
      path: endpoint,
      args: {
        data: data,
      },
      proxy: true,
    });
    ctx.body = res.result;
    ctx.status = res.code;
  }

  // mobile ---------------
  sendSms = async (ctx: Context, next: Function) => {
    const { captcha, number } = ctx.request.body as Dictionary;
    if (captcha !== ctx.session[CAPTCHA_NAME_COOKIE]) {
      ctx.body = {
        errors: [
          {
            code: 'invalid_captcha',
          },
        ],
      };
      ctx.status = 401;
      return next();
    }
    const code = this.generateVericationCode(number);
    const md5 = crypto.createHash('md5');
    ctx.body = await send(number, code);
    ctx.cookies.set(
      MOBILE_CODE_COOKIE_NAME,
      md5.update(number + code).digest('hex'),
      {
        maxAge: 60 * 60 * 1000,
      },
    );
  };

  sendCaptchaCode = async (ctx: Context, next: Function) => {
    const endpoint = '/send_captch_code';
    const { account, captcha } = ctx.request.body as Dictionary;
    if (captcha !== ctx.session[CAPTCHA_NAME_COOKIE]) {
      ctx.body = {
        errors: [
          {
            code: 'invalid_captcha',
          },
        ],
      };
      ctx.status = 401;
      return next();
    }
    const res = await JakiroRequest({
      ctx,
      method: 'GET',
      path: endpoint,
      args: {
        query: {
          account: account,
        },
      },
      proxy: true,
    });
    ctx.status = res.code === 204 ? 200 : res.code;
    ctx.body = res.result;
  };

  async checkMobileExist(ctx: Context) {
    const endpoint = '/auth/mobile/exist';
    const mobile = ctx.query.mobile;
    ctx.body = await JakiroRequest({
      ctx,
      method: 'GET',
      path: endpoint,
      args: {
        query: {
          mobile: mobile,
        },
      },
    });
    ctx.status = 200;
  }

  async logoutView(ctx: Context) {
    let redirectUrl = '/landing';
    JakiroRequest({
      ctx,
      method: 'POST',
      path: '/v1/auth/logout',
      proxy: true,
    });
    // support oidc logout
    if (ctx.session.tp_logout_url) {
      redirectUrl = ctx.session.tp_logout_url;
    }
    ctx.session = null;
    ctx.cookies.set(SESSION_COOKIE_NAME, '');
    ctx.cookies.set(`koa${SESSION_COOKIE_NAME}`, '');
    ctx.body = {
      url: redirectUrl,
    };
    ctx.status = 200;
  }

  async logoutRedirect(ctx: Context) {
    const redirectUrl = `/landing${
      ctx.query.next ? '?next=' + ctx.query.next : ''
    }`;
    JakiroRequest({
      ctx,
      method: 'POST',
      path: '/v1/auth/logout',
      proxy: true,
    });
    ctx.session = null;
    ctx.cookies.set(SESSION_COOKIE_NAME, '');
    ctx.cookies.set(`koa${SESSION_COOKIE_NAME}`, '');
    ctx.redirect(redirectUrl);
  }

  landingView = async (ctx: Context, next: Function) => {
    const favicon_path = extract_favicon_path_from_envvar();
    const redirect_url = ctx.query.redirect_url;
    let next_url = ctx.query.next;
    // 登录页免登录逻辑：http://confluence.alaudatech.com/pages/viewpage.action?pageId=25270711
    if (
      getEnv('THIRD_PARTY_LOGIN_TYPE') === 'password' &&
      ctx.query &&
      ctx.query.accSlave &&
      ctx.query.ticket
    ) {
      const data = {
        organization: '',
        username: '',
        password: '',
        next: '/',
      };
      [data.organization, data.username] = ctx.query.accSlave.split('/');
      if (!data.username) {
        data.username = data.organization;
        delete data.organization;
      }
      data.password = ctx.query.ticket;
      const headers = {
        CLIENT: ctx.ips.length ? ctx.ips.join(', ') : ctx.ip,
        ...getCommonHeaders(ctx),
      };

      const result = await JakiroRequest({
        ctx,
        method: 'POST',
        path: '/generate-api-token/',
        args: {
          data: data,
        },
        headers,
      });
      if (result.token) {
        ctx.session.user = result;
        ctx.user = result;
        const profile = await JakiroRequest({
          ctx,
          method: 'GET',
          path: `/auth/${
            data.organization ? data.organization : data.username
          }/profile`,
        });
        if (profile.password_is_empty) {
          next_url = '/console/admin/user/password';
        } else if (data.next) {
          next_url = data.next;
        }
      }
    }

    if (ctx.user && ctx.user.token && redirect_url) {
      const url = this.genRedirectUrl(ctx.user, redirect_url);
      ctx.redirect(url);
      return next();
    }
    if (ctx.user && ctx.user.token) {
      if (!next_url) {
        next_url = DEFAULT_REDIRECT_URL;
        logger.debug(`login redirect: ${next_url}`);
      }
      ctx.redirect(next_url);
    } else {
      await ctx.render(
        'index.html',
        Object.assign({}, config.viewBasicData, {
          favicon_path: favicon_path,
          template_type: 'landing',
          ...config.browserData,
        }),
      );
    }
    next();
  };
}
const landingService = new Landing();
export default landingService;
