import { Context } from 'koa';

import { JakiroRequest } from '../common/request';

class Loadbalancer {
  constructor() {}

  handleLoadbalancerPatch = async (
    ctx: Context,
    next: Function,
  ): Promise<any> => {
    if (
      ctx.params.resource_type &&
      // 目前只有 alaudaloadbalancer2 才会设置 content-type 为 application/merge-patch+json
      ctx.params.resource_type === 'alaudaloadbalancer2'
    ) {
      const options = {
        ctx,
        method: ctx.method,
        path: ctx.path,
        headers: {
          'content-type': 'application/merge-patch+json',
        },
        args: {
          query: ctx.query,
          data: ctx.request.body,
        },
        proxy: true,
      };
      const rep = await JakiroRequest(options);
      ctx.body = rep.result;
      ctx.status = rep.code;
    } else {
      next();
    }
  };
}

const LoadbalancerService = new Loadbalancer();
export default LoadbalancerService;
