import { Context } from 'koa';

import { JakiroRequest } from '../common/request';

export default async function filteredList(
  ctx: Context,
  next: Function,
): Promise<any> {
  const orgs_endpoint = `/orgs/${ctx.query.namespace}/accounts`;
  const permission_endpoint = `/permissions/${ctx.query.namespace}/subaccount/`;
  const result = await JakiroRequest({
    ctx,
    path: orgs_endpoint,
    method: 'GET',
    args: {
      query: ctx.query,
    },
  });
  const res_em = result.results;
  if (res_em && res_em.length) {
    const item = res_em[0];
    if (!item['resource_actions']) {
      const data = [];
      res_em.forEach(r => {
        data.push({ name: r.username });
      });
      const actions = await JakiroRequest({
        ctx,
        path: permission_endpoint,
        method: 'POST',
        args: {
          data: data,
        },
      });
      res_em.forEach((r, id) => {
        r['resource_actions'] = actions[id]['resource_actions'];
      });
      const filter_action =
        'subaccount:' + (ctx.query.action ? ctx.query.action : 'get');
      result.results = res_em.filter(
        r => r.resource_actions.indexOf(filter_action) !== -1,
      );
    }
  }
  ctx.body = result;
  ctx.status = 200;
}
