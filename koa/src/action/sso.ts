import { Context } from 'koa';
import { get } from 'lodash';

import { DEFAULT_REDIRECT_URL } from '../common/constants';
import { JakiroRequest } from '../common/request';
import {
  extract_favicon_path_from_envvar,
  getCommonHeaders,
  getEnv,
} from '../common/util';
import environmentsService from './environments';

export interface OidcConfig {
  issuer: string;
  client_id: string;
  client_secret: string;
  display_name: string;
  redirect_uri: string;
  authorization_endpoint: string;
  token_endpoint: string;
  userinfo_endpoint: string;
  scopes_supported: string[];
  response_types_supported: string[];
  claims_supported: string[];
}

class Sso {
  async ssoLandingView(ctx: Context, next: Function) {
    // 'tp' refers to 'third party'
    const tpName = getEnv('SSO_TP_NAME') || 'oidc';
    const grant_type = getEnv('SSO_TP_GRANT_TYPE') || 'code';
    const params_keyword = getEnv('SSO_PARAMS_KEYWORD');

    const code = params_keyword ? ctx.query[params_keyword] : ctx.query['code'];
    const redirectUrl = ctx.query.next || DEFAULT_REDIRECT_URL;

    if (tpName !== 'oauth2' && code && get(ctx, 'user.ticket') === code) {
      ctx.redirect(redirectUrl);
      return next();
    }

    let data;
    switch (tpName) {
      case 'oauth2':
        data = {
          access_token: ctx.query['access_token'],
          grant_type,
        };
        break;
      default:
        data = {
          code: ctx.query[params_keyword] || code,
          grant_type,
        };
    }
    // refers to http://internal-api-doc.alauda.cn/jakiro.html#tp_sso__tp_name__login_post
    const result = await JakiroRequest({
      ctx,
      method: 'POST',
      path: `/v1/tp_sso/${tpName}/login`,
      args: {
        data,
      },
      headers: getCommonHeaders(ctx),
    });
    // TODO: delete me
    // const result = await Promise.resolve({
    //   is_valid: true,
    //   alauda_user_info: {
    //     token: '7893a0e56bbe60ef2826d65b5c1f572d8d0da517',
    //     username: 'dongliu',
    //     namespace: 'alauda',
    //     is_first_login: false,
    //   },
    // } as any);

    const favicon_path = extract_favicon_path_from_envvar();
    const { is_valid, alauda_user_info: user, tp_logout_url } = result;
    if (is_valid && user.token) {
      ctx.user = ctx.session.user = Object.assign(user, {
        ticket: code,
      });
      if (tp_logout_url) {
        ctx.session.tp_logout_url = tp_logout_url; //for oidc logout request
      }
      ctx.redirect(redirectUrl);
    } else {
      const data = {
        favicon_path: favicon_path,
        sso_configs: (await environmentsService.getSsoConfigContent(ctx)) || '',
      };
      await ctx.render('sso.html', data);
    }
    next();
  }
}
const ssoService = new Sso();

export default ssoService;
