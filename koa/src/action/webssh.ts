import { Context } from 'koa';
import * as request from 'request-promise';

class Webssh {
  async webssh(ctx: Context, next: Function) {
    const options = {
      url: ctx.query.url,
      json: true,
      strictSSL: false,
    };
    await request(options)
      .then(rep => {
        ctx.body = rep;
      })
      .catch(err => {
        ctx.body = err;
      });
    ctx.status = 200;
    next();
  }
}
const websshService = new Webssh();

export default websshService;
