import { Context } from 'koa';

import { getEnv } from '../common/util';
import config from '../config/config';
import { JakiroRequest } from './request';

export interface User {
  username: string;
  namespace: string;
  token: string;
  ticket?: string; // sso login ticket
}

export const LOGIN_URL = '/landing';

function _redirectToLogin(ctx) {
  let loginUrl = LOGIN_URL;
  if (ctx.url !== '/' && !ctx.path.startsWith(LOGIN_URL)) {
    loginUrl += `?next=${encodeURIComponent(ctx.url)}`;
  }
  ctx.redirect(loginUrl);
}

function login_required(ctx, next) {
  const is_ajax = ctx.request.get('X-Requested-With') === 'XMLHttpRequest';
  if (ctx.skipLogin) {
    return next();
  }
  if (is_ajax) {
    if (!ctx.user || !ctx.user.token) {
      ctx.throw(401);
    } else {
      return next();
    }
  } else {
    if (!ctx.user || !ctx.user.token) {
      _redirectToLogin(ctx);
    }
  }
  return next();
}

async function authentication(ctx, next) {
  ctx.user = ctx.session.user;
  return next();
}

export { authentication, login_required };
