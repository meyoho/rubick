import { Request } from '../common/request';
import { getEnv } from '../common/util';

class WeblabClient {
  url: any;
  headers: any;

  constructor() {
    this.url = getEnv('LIGHTKEEPER_ENDPOINT') + 'weblab-states/';
    this.headers = {
      Accept: 'application/json',
      'User-Agent': 'AlaudaClient/',
    };
  }

  async prefetch(namespace, weblabs = []) {
    let res = {};
    if (getEnv('LIGHTKEEPER_OVERRIDDEN_WEBLABS')) {
      res = this.parseOverriddenWeblabs(
        getEnv('LIGHTKEEPER_OVERRIDDEN_WEBLABS'),
      );
    } else {
      let url = this.url;
      if (namespace) {
        url += namespace + '/';
      }
      if (weblabs.length) {
        url += '?weblabs=' + weblabs.join(',');
      }
      try {
        res = await Request({
          url,
          method: 'GET',
          headers: this.headers,
          // timeout: this.max_timeout,
          qs: {
            weblabs,
          },
          json: true,
        }).then(rep => rep.body);
      } catch (err) {
        console.error(
          'Failed to retrieve weblab states. ',
          typeof err,
          err.message,
        );
      }
    }
    // Convert keys to upper case
    return Object.keys(res).reduce(
      (accum, key) => ({ ...accum, [key.toUpperCase()]: res[key] }),
      {},
    );
  }

  parseOverriddenWeblabs(str) {
    const weblabStrArr = str.split(',');
    const weblabs = {};
    weblabStrArr.forEach(str => {
      const weblab = str.split(':');
      weblabs[weblab[0]] = weblab[1] === '1';
    });
    return weblabs;
  }
}

const weblabClient = new WeblabClient();
export default weblabClient;
