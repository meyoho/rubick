import { SESSION_COOKIE_NAME } from '../common/constants';
import { getConfigFileSync, getEnv } from '../common/util';
import { jsonErrorHandler } from '../error/error_handler';

const root = process.cwd();
const appConfig: any = getConfigFileSync('config.yaml');
const config = {
  secretKeys: ['some secrhurr'],
  sessionConf: maxAge => ({
    key: SESSION_COOKIE_NAME,
    maxAge: maxAge /** ms */,
    overwrite: true /** (boolean) can overwrite or not (default true) */,
    httpOnly: true /** (boolean) httpOnly or not (default true) */,
    signed: true /** (boolean) signed or not (default true) */,
    rolling: true,
  }),
  default_port: getEnv('LOCAL_BACKEND_ENDPOINT')
    ? getEnv('LOCAL_BACKEND_ENDPOINT').match(/:(\d+)$/)[1]
    : 2333,
  staticConf: root,
  viewsConf: {
    root: root + '/dist/static',
    opts: {
      map: {
        html: 'lodash',
      },
    },
  },
  loggerConfig: {
    logDir: '/var/log/mathilde/',
  },
  errorConfig: {
    json: jsonErrorHandler,
    accepts: function() {
      if (this.accepts('html')) {
        return 'html';
      } else {
        return 'json';
      }
    },
  },
  captchaConfig: {
    size: 4,
    fileMode: 2,
    noise: false,
    complexity: 2,
  },
  ccp: {
    account_sid: getEnv('CCP_ACCOUNTSID'),
    account_token: getEnv('CCP_ACCOUNTTOKEN'),
    app_id: getEnv('CCP_APPID'),
    template_id: getEnv('CCP_TEMPLATEID'),
    server_ip: getEnv('CCP_SERVERIP'),
    server_port: getEnv('CCP_SERVERPORT'),
    soft_version: '2013-12-26',
  },
  loginSecretKey: 'gou li guojia shengsi yi',
  loginAESIV: 'alauda-developer',
  viewBasicData: {
    favicon_path: '',
    namespace: '',
    username: '',
    /**
     * @template_type: landing || console || user
     * Decide the render template type
     */
    //
    template_type: '',
  },
  browserData: {
    browser_download_url:
      getEnv('BROWSER_DOWNLOAD_URL') ||
      appConfig.supported_browsers.chrome.official_download_url,
    chrome_minimum_version: appConfig.supported_browsers.chrome.minimum_version,
    firefox_minimum_version:
      appConfig.supported_browsers.firefox.minimum_version,
  },
};

export default config;
