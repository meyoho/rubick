import { login_required } from '../common/auth';
import { Routes, RoutesKey } from './router';

/**
 * 针对要求登录的路由类成员方法装饰器，必须写在 RequestMapping 上面，即
 *
 * ```
 * @LoginRequired
 * @RequestMapping()
 * method() {}
 * ```
 */
export const LoginRequired = (
  target: any,
  propertyKey: string,
  descriptor: PropertyDescriptor,
) => {
  target = propertyKey ? target : target.prototype;

  const routes: Routes = target[RoutesKey];

  if (!routes) {
    throw new ReferenceError('no routes found');
  }

  let handler = descriptor.value;

  const index = routes.findIndex(route => {
    const routeHandler = route.handler;
    return Array.isArray(routeHandler)
      ? routeHandler.includes(handler)
      : routeHandler === handler;
  });

  const oldHandler = routes[index].handler;

  handler = Array.isArray(oldHandler) ? oldHandler : [oldHandler];

  routes[index].handler = [login_required, ...handler];
};
