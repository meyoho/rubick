import * as log4js from 'koa-log4';
import * as _ from 'lodash';

// must use the format of 'var = require' when use typescript to load json file
import { default as defaultDevLogConfig } from './default_conf/log_dev.config';
import { default as defaultProdLogConfig } from './default_conf/log_prod.config';

class RubickLogger {
  public logger: any;

  constructor() {
    log4js.configure(this.getEnvConfig());
    this.logger = log4js.getLogger('log');
  }

  public getHttpLoggerMiddleware() {
    return log4js.koaLogger(log4js.getLogger('access'), { level: 'info' });
  }

  private getEnvConfig() {
    let envConfig;
    if (process.env.NODE_ENV !== 'production') {
      envConfig = _.cloneDeep(defaultDevLogConfig);
    } else {
      envConfig = _.cloneDeep(defaultProdLogConfig);
    }
    return envConfig;
  }
}
const rbLogger = new RubickLogger();

export default rbLogger;
