import { mock } from 'mockjs';

import { Controller, RequestMapping } from '../decorators';

@Controller
export class ApiTokenController {
  @RequestMapping('/v3/api-token')
  getRoleTypes(ctx) {
    ctx.body = mock({
      platform_token: '@id',
      kubernetes_id_token: '@id',
      openshift_token: '@id',
    });
  }
}
