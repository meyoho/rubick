import { Controller, Method, RequestMapping } from '../decorators';

@Controller
@RequestMapping('/v2/regions/')
export class RegionController {
  @RequestMapping({
    path: 'version-check',
    method: Method.POST,
  })
  regionCheck(ctx) {
    ctx.body = { version: '1.9.1' };
  }

  @RequestMapping({
    method: Method.POST,
  })
  accessRegion(ctx) {
    ctx.body = null;
  }
}
