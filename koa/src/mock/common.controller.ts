import { mock } from 'mockjs';

import { Controller, RequestMapping } from '../decorators';

@Controller
@RequestMapping('/v1/configuration/')
export class RegionController {
  @RequestMapping(':namespace')
  regionCheck(ctx) {
    ctx.body = mock({
      code: 200,
      result: [
        {
          namespace: 'alauda',
          name: 'cpu',
          value: 'http://wisp',
          display_name: 'dis_env',
          type: 'quota',
          id: 1,
        },
        {
          namespace: 'alauda',
          name: 'memory',
          value: 'http://wisp',
          display_name: 'dis_env',
          type: 'quota',
          id: 1,
        },
        {
          namespace: 'alauda',
          name: 'storage',
          value: 'http://wisp',
          display_name: 'dis_env',
          type: 'quota',
          id: 1,
        },
      ],
    });
  }
}
