import { Context } from 'koa';
import { mock } from 'mockjs';

import { Controller, LoginRequired, RequestMapping } from '../decorators';

const dsl =
  '(AND (IN HOST www.baidu.com baidu.com) (RANGE SRC_IP 114.114.114.114 255.255.255.255) (EQ URL /search) (OR (RANGE HEADER uid 100 999) (RANGE HEADER uid 10000 11000)))';

const mockFrontendDetail = {
  protocol: '@pick(http,https)',
  app_name: '@ctitle',
  certificate_id: '@id',
  certificate_name: '@word',
  load_balancer_id: '@id',
  service_name: '@word',
  port: '@natural(80,8000)',
  'rules|0-10': [
    {
      description: '@pick(@title,)',
      dsl,
      'services|0-5': [
        {
          app_name: '@word',
          container_port: 80,
          service_name: '@word',
          weight: '@natural(80,8000)',
        },
      ],
      rule_id: '@id',
    },
  ],
};

@Controller
@RequestMapping('/load_balancers/:namespace')
export class LoadBalancerController {
  @LoginRequired
  @RequestMapping('/:id/frontends')
  getFrontends(ctx: Context) {
    ctx.body = mock({
      'data|0-10': [mockFrontendDetail],
    });
  }

  @LoginRequired
  @RequestMapping('/:id/frontends/:port')
  getFrontendDetail(ctx: Context) {
    ctx.body = mock({ data: mockFrontendDetail });
  }

  @LoginRequired
  @RequestMapping('/:id/frontends/:port/rules')
  getRules(ctx: Context) {
    ctx.body = mock({
      count: '@natural(100,200)',
      page: '@natural(0,10)',
      page_size: 20,
      'data|0-10': [
        {
          dsl,
          'services|0-5': [
            {
              app_name: '@word',
              container_port: 80,
              service_name: '@word',
              weight: '@natural(80,8000)',
            },
          ],
          rule_id: '@id',
        },
      ],
    });
  }
}
