import { mock } from 'mockjs';

import { Controller, Method, RequestMapping } from '../decorators';

const MIRROR_COLORS = [
  '#37d9f0',
  '#4da8ee',
  '#aa17d0',
  '#82ccd2',
  '#89b0cd',
  '#9389b0',
  '#def3f3',
  '#cde0ef',
  '#dfdbec',
  '#00a3af',
  '#3876a6',
  '#765c83',
];

export const mockMirror = {
  name: '@word',
  display_name: '@ctitle',
  description: '@sentence',
  flag: `@pick(${MIRROR_COLORS})`,
  created_at: '@date()T@time()Z',
  'regions|1-5': [
    {
      name: '@word',
      display_name: '@ctitle',
      id: '@id',
      created_at: '@date()T@time()Z',
    },
  ],
  resource_actions: ['view', 'update', 'delete', 'clear'].map(
    action => 'cluster:' + action,
  ),
};

@Controller
@RequestMapping('/v2/mirrors/:namespace/')
export class MirrorController {
  @RequestMapping()
  getMirrors(ctx) {
    ctx.body = mock({
      'result|0-10': [mockMirror],
    });
  }

  @RequestMapping(':name')
  getMirror(ctx) {
    ctx.body = mock(mockMirror);
  }

  @RequestMapping({
    method: Method.POST,
  })
  createMirror(ctx) {
    ctx.body = null;
  }

  @RequestMapping({
    method: Method.PUT,
    path: ':name',
  })
  updateMirror(ctx) {
    ctx.body = null;
  }

  @RequestMapping({
    method: Method.DELETE,
    path: ':name',
  })
  deleteMirror(ctx) {
    ctx.body = null;
  }
}
