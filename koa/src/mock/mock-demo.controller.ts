import { Context } from 'koa';
import { mock } from 'mockjs';

import { Controller, LoginRequired, RequestMapping } from '../decorators';

@Controller
@RequestMapping('/mock')
export class MockDemoController {
  @LoginRequired
  @RequestMapping('/demo')
  demo(ctx: Context) {
    ctx.body = {
      message: 'mock demo successfully!',
      mockjs: mock({
        title: '@title',
        'names|0-10': ['@name'],
      }),
    };
  }
}
