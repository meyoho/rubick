import { mock } from 'mockjs';

import { Controller, Method, RequestMapping } from '../decorators';

const mockProject = {
  namespace: '@word',
  name: 'yinmock',
  uuid: '123456',
  display_name: '@word',
  status: '@word',
  description: '@word',
  template_uuid: '@id',
  template: '@word',
  created_at: '@date()T@time()Z',
  updated_at: '@date()T@time()Z',
  created_by: '@word',
  service_type: ['Kubernetes'],
  'clusters|3-10': [
    {
      name: '@word',
      display_name: '@word',
      uuid: '@id',
      quota: {
        cpu: 123123123,
        memory: 12,
        storage: 123213,
      },
      project_quota: {
        cpu: 123123,
        memory: 123213,
        storage: 123123123,
      },
    },
  ],
  'admin_list|0-10': ['@name'],
};

const mockQuota = {
  name: '@word',
  uuid: '@id',
  'clusters|1-3': [
    {
      name: '@word',
      display_name: '@word',
      uuid: '@id',
      service_type: '@word',
      quota: {
        cpu: {
          max: '10',
          used: '2',
        },
        memory: {
          max: '1',
          used: '0.2',
        },
        storage: {
          max: '500',
          used: '100',
        },
        pvc_num: {
          max: '10',
          used: '5',
        },
        pods: {
          max: '100',
          used: '50',
        },
      },
    },
  ],
};

@Controller
@RequestMapping('/v2/batch/')
export class ProjectBatchController {
  @RequestMapping({
    method: Method.POST,
  })
  getProjectInfo(ctx) {
    ctx.body = {
      responses: {
        request1: {
          code: 200,
          header: {
            header: 'value',
          },
        },
        request2: {
          code: 204,
          header: {
            'Content-Type': 'application/json',
          },
          body: '成功',
        },
      },
    };
  }
}

@Controller
@RequestMapping('/v1/projects/')
export class ProjectController {
  @RequestMapping(':namespace')
  getProjects(ctx) {
    ctx.body = mock({
      result: {
        count: 100,
        next: null,
        previous: null,
        page_size: 20,
        num_pages: 2,
        'results|1-10': [mockProject],
      },
    });
  }

  @RequestMapping(':namespace/available-resources')
  getProjectAvailableResource(ctx) {
    ctx.body = {
      code: 200,
      result: {
        clusters: [
          {
            resource_actions: [
              'cluster:clear',
              'cluster:create',
              'cluster:delete',
              'cluster:deploy',
              'cluster:update',
              'cluster:view',
            ],
            display_name: 'ACE',
            name: 'ace',
            container_manager: 'KUBERNETES',
            created_at: '2018-11-05T10:31:26.117115Z',
            mirror: {},
            namespace: 'alauda',
            updated_at: '2018-11-30T02:03:08.560491Z',
            platform_version: 'v4',
            state: 'RUNNING',
            features: {
              metric: {
                integration_uuid: '281301bf-c643-436d-8269-114513b7eca4',
                type: 'prometheus',
              },
            },
            quota: {
              cpu: 100,
              memory: 100,
              storage: 100,
              pods: 100,
              pvc_num: 100,
            },
            env_uuid: null,
            type: 'CLAAS',
            id: 'fb6e015e-04fd-494b-9d61-9c72c374d9b1',
            attr: {
              cluster: { nic: 'eth0' },
              docker: { path: '/var/lib/docker', version: '1.12.6' },
              feature_namespace: 'default',
              cloud: { name: 'PRIVATE' },
              kubernetes: {
                token:
                  'eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLWJ6NTg2Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiIyMDYyMDA4MS1lMGU1LTExZTgtYmIxZC01MjU0MDAwM2Y0MmEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.WJcG8U4scaCX-u77I1MsH_hcAvQkm6lamcgFvRls3HBXNlqK_F06ScuY_5kMNCM4AXXQ0vF0zQATsgT95GV7rDXtlirI09hpc7M9ZZG5MYLA7HuzRh1Ua5Ux599Yat2V2Ot56WwK9HFSYsWMbLURQgGdFTQiRnVKXlWXuBqpRLie8nX9Hf00fpDffAt_qAqLJnGnsPSrY4YYcpDmBGNEt1iovHC-JHv5Xsc9rd1woS-rwPnuiqAbTarY-Io5Akp6RNHafUXCkwoSbXyYMXTmAgIR4Dp2ECW6L5rP6cMx_b5DOxmfjz51EjYwivTXaHGEqhDMnNkVyQPW91cO6DeAAg',
                endpoint: 'https://10.0.96.34:6443',
                type: 'original',
                version: '1.11.2',
                cni: {
                  network_policy: '',
                  cidr: '10.199.0.0/16',
                  type: 'flannel',
                  backend: 'vxlan',
                },
              },
            },
          },
          {
            resource_actions: [
              'cluster:clear',
              'cluster:create',
              'cluster:delete',
              'cluster:deploy',
              'cluster:update',
              'cluster:view',
            ],
            quota: {
              cpu: 1200,
              memory: 2100,
              storage: 3100,
              pods: 4100,
              pvc_num: 5100,
            },
            display_name: 'e2e-region-test2',
            name: 'e2e-region-test2',
            container_manager: 'KUBERNETES',
            created_at: '2018-11-16T09:28:54.616491Z',
            mirror: {},
            namespace: 'alauda',
            updated_at: '2018-11-30T11:59:09.926279Z',
            platform_version: 'v4',
            state: 'RUNNING',
            features: {},
            env_uuid: null,
            type: 'CLAAS',
            id: '1e61c6f3-b15c-47dc-9577-5d4a99e2be1f',
            attr: {
              cluster: { nic: 'eth0' },
              docker: { path: '/var/lib/docker', version: '1.12.6' },
              feature_namespace: 'default',
              kubernetes: {
                token:
                  'eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLXJxenZqIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJmMWEwOGZjOC1lOTgxLTExZTgtOWYxNC01MjU0MDA2OTVhNzUiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.O-zBgut0b7O0PuZXwHAUmAlnPQG4n7HXkFLtY2CMVPc3qc2cv7uAE1BGygbW2linTff3GjDyiUrY0Wz8v44X2TMOlZ0CjIcliMiSFoP_ITkLXm4ym6VJ5jHLojvVjXWkrCfuPfFAn11dcJWwTMIScjFLvBigNyja7Mo5qvr5MCNSlVPuiJoioKYSe678j_rYjYulEQUqTbldgcIt7LPelSwPNnVwxwYBZhex7IGC7Oy64xtSifi980mvfaBNz_q3aQYTuii8MeXpvZjLauAJLlzv1sw_eVZ7jda0QyEhZd6NIImOWtWToZdG7SeBEVKC244gUmcm3b5ZggsgEIiuaQ',
                endpoint: 'https://10.0.96.26:6443',
                type: 'original',
                version: '1.11',
                cni: {
                  network_policy: 'calico',
                  cidr: '10.1.0.0/16',
                  type: 'flannel',
                  backend: 'vxlan',
                },
              },
              cloud: { name: 'PRIVATE' },
            },
          },
          {
            resource_actions: [
              'cluster:clear',
              'cluster:create',
              'cluster:delete',
              'cluster:deploy',
              'cluster:update',
              'cluster:view',
            ],
            display_name: '联邦一',
            name: 'federal1',
            container_manager: 'KUBERNETES',
            created_at: '2018-11-09T08:42:44.254245Z',
            quota: {
              cpu: 100,
              memory: 100,
              storage: 100,
              pods: 100,
              pvc_num: 100,
            },
            mirror: {
              display_name: '联邦集群',
              description: '',
              created_at: '2018-11-26T09:37:20.379241Z',
              regions: [
                {
                  created_at: '2018-11-09T08:42:44.254245Z',
                  display_name: '联邦一',
                  name: 'federal1',
                  id: '2d4046f8-e636-4c68-bd1b-a4a834c27ca5',
                },
                {
                  created_at: '2018-11-09T08:42:57.749956Z',
                  display_name: '联邦二',
                  name: 'federal2',
                  id: '740f0e39-dc41-49dd-a247-af7f37ed179d',
                },
              ],
              flag: '#37d9f0',
              id: 'fbd72213-5fbe-489d-9292-8c66f9a7aec7',
              name: 'federation',
            },
            namespace: 'alauda',
            updated_at: '2018-11-27T09:27:50.194946Z',
            platform_version: 'v4',
            state: 'RUNNING',
            features: {
              log: {
                application_name: 'official-log',
                type: 'official',
                storage: {
                  read_log_source: 'default',
                  write_log_source: 'default',
                },
                application_namespace: 'default',
              },
            },
            env_uuid: null,
            type: 'CLAAS',
            id: '2d4046f8-e636-4c68-bd1b-a4a834c27ca5',
            attr: {
              cluster: { nic: 'eth0' },
              docker: { path: '/var/lib/docker', version: '1.12.6' },
              feature_namespace: 'default',
              cloud: { name: 'PRIVATE' },
              kubernetes: {
                token:
                  'eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLTU3a2p2Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiI3ZmI1MzMzYi1lM2M5LTExZTgtYTlmYy01MjU0MDBjMzY2NDAiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.0fgMdzUSqMT7yxNM3UwwglZOhyNJ536khZ113T48EW034Y2PVlsmLokJU6i6Gb-ifyGGdMAIsopK33gzl5zbmZoDzz6dWhEGSLQydFrLJmZStqyZ8k19bs35cYKM_ugEYkixZNmehUN0IRWgkfTr8dH8Z9sJ1aYgurvt2tYaep15zqKeVIF2j0G4ZlKYTRemV5LJvTJWpt3oB3twMpN6I2Y_eXyow5AjdwxroG1XpWmRt93iKGP21w8FlvMZYi-pE9MDuuDPRPcHvElEK3A3F5zyfULJMnd6Z3Ms2KD5V2Ecl_4cKwSANz53Ef6WPxgo29xQzRQzWY17SWG_A0-nPw',
                endpoint: 'https://10.0.96.17:6443',
                type: 'original',
                version: '1.11.2',
                cni: {
                  network_policy: '',
                  cidr: '10.22.0.0/16',
                  type: 'flannel',
                  backend: 'vxlan',
                },
              },
            },
          },
          {
            resource_actions: [
              'cluster:clear',
              'cluster:create',
              'cluster:delete',
              'cluster:deploy',
              'cluster:update',
              'cluster:view',
            ],
            display_name: '联邦二',
            name: 'federal2',
            container_manager: 'KUBERNETES',
            created_at: '2018-11-09T08:42:57.749956Z',
            mirror: {
              display_name: '联邦集群',
              description: '',
              created_at: '2018-11-26T09:37:20.379241Z',
              regions: [
                {
                  created_at: '2018-11-09T08:42:44.254245Z',
                  display_name: '联邦一',
                  name: 'federal1',
                  id: '2d4046f8-e636-4c68-bd1b-a4a834c27ca5',
                },
                {
                  created_at: '2018-11-09T08:42:57.749956Z',
                  display_name: '联邦二',
                  name: 'federal2',
                  id: '740f0e39-dc41-49dd-a247-af7f37ed179d',
                },
              ],
              flag: '#37d9f0',
              id: 'fbd72213-5fbe-489d-9292-8c66f9a7aec7',
              name: 'federation',
            },
            quota: {
              cpu: 5,
              memory: 5,
              storage: 6,
              pods: 7,
              pvc_num: 8,
            },
            namespace: 'alauda',
            updated_at: '2018-11-27T09:26:48.931165Z',
            platform_version: 'v4',
            state: 'RUNNING',
            features: {
              metric: {
                integration_uuid: '09831251-452f-40df-acba-f637cfd3f52c',
                type: 'prometheus',
              },
              registry: {
                application_name: 'offical-registry',
                type: 'official',
                application_namespace: 'default',
              },
            },
            env_uuid: null,
            type: 'CLAAS',
            id: '740f0e39-dc41-49dd-a247-af7f37ed179d',
            attr: {
              cluster: { nic: 'eth0' },
              docker: { path: '/var/lib/docker', version: '1.12.6' },
              feature_namespace: 'default',
              cloud: { name: 'PRIVATE' },
              kubernetes: {
                token:
                  'eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJjbHVzdGVycm9sZS1hZ2dyZWdhdGlvbi1jb250cm9sbGVyLXRva2VuLXE0dHh4Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQubmFtZSI6ImNsdXN0ZXJyb2xlLWFnZ3JlZ2F0aW9uLWNvbnRyb2xsZXIiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC51aWQiOiJkNjliZWZlOS1lM2M5LTExZTgtYjU5MS01MjU0MDA3ODBmNGEiLCJzdWIiOiJzeXN0ZW06c2VydmljZWFjY291bnQ6a3ViZS1zeXN0ZW06Y2x1c3RlcnJvbGUtYWdncmVnYXRpb24tY29udHJvbGxlciJ9.P7i8SMUTAcpsYGVch2MH5QhcNw4GwJRdBGBJxKHfitTQQK_qegQbmO4ufDEqwOLsVbcSiHGsb06elzZSVqQ5GVt2Ib3PMMHVT2dJge5KNyCrwzDSR77G4TOsKG3KYDNVT2CoxjeKcMktTu9Mhh_sQ_Ruo0QTuZm6DjaIFSRNKl_L02yJ94eQZSuxNPwe4s3Wva0MxhkAH31NbaglIpGG5xLEITi3kEj7Clb_s3TPnhwM51uH8D8tr7lGyR9kwDMklqm-yyT2Wf6SeFsJHVtuiBAUZhKJSNliSYMHO0_PxbtPZzkMUlVSQ42-E4aKR5oZWRtR25cuMRPJarBQeTkvFw',
                endpoint: 'https://10.0.96.45:6443',
                type: 'original',
                version: '1.11.2',
                cni: {
                  network_policy: 'calico',
                  cidr: '10.222.0.0/16',
                  type: 'calico',
                  backend: 'bird',
                },
              },
            },
          },
        ],
      },
    };
  }

  @RequestMapping(':namespace/:uuid')
  getProjectInfo(ctx) {
    ctx.body = mock(mockProject);
  }

  @RequestMapping(':namespace/:name/quota')
  getProjectQuota(ctx) {
    ctx.body = mock({
      // 项目名称
      name: '@word',
      // 项目UUID
      uuid: '@id',
      // 租户下的集群信息列表
      'clusters|1-10': [
        {
          // 集群名称
          name: '@id',
          // 集群名称
          display_name: '@word',
          // 集群UUID
          uuid: '@id',
          // 集群服务类型
          service_type: 'kubernetes',
          // 集群配额信息
          quota: {
            cpu: {
              max: 100,
              'used|0-100': 0,
            },
            memory: {
              max: 100,
              'used|0-100': 0,
            },
            storage: {
              max: 100,
              'used|0-100': 0,
            },
            pvc_num: {
              max: 100,
              'used|0-100': 0,
            },
            pods: {
              max: 100,
              'used|0-100': 0,
            },
          },
        },
      ],
    });
  }
  @RequestMapping(':namespace/:uuid/quota')
  getQuota(ctx) {
    ctx.body = mock(mockQuota);
  }

  @RequestMapping(':namespace/:name/namespace')
  getProjectNamespaces(ctx) {
    ctx.body = mock({
      code: 200,
      result: {
        count: 100,
        next: null,
        previous: null,
        page_size: 20,
        num_pages: 2,
        'results|1-10': [
          {
            project_uuid: '@id',
            created_at: '@time',
            quota: {
              cpu: 522,
              memory: 34425,
              storage: 23426,
              pods: 234237,
              pvc_num: 234238,
            },
            'spaces|1-10': [
              {
                name: '@word',
                display_name: '@word',
                uuid: '@id',
                create_at: '@time',
                updated_at: '@time',
              },
            ],
            updated_at: '@time',
            cluster_uuid: '@id',
            namespace_uuid: '@id',
            namespace_name: '@word',
          },
        ],
      },
    });
  }
}
@Controller
@RequestMapping('/v2/spaces/')
export class SpaceController {
  @RequestMapping({
    method: Method.POST,
    path: ':namepsace',
  })
  createSpace(ctx) {
    ctx.body = {
      project_uuid: '715f73df-40f8-43b4-bf7a-fe5252a0069e',
      name: 'space1',
      display_name: '第一个',
      uuid: '4a42a89f-c5f6-4614-bc7c-4427b5aeab16',
      created_at: '2018-05-24T17:53:02.472432Z',
      updated_at: '2018-05-24T17:53:02.472432Z',
      failed_users: [
        {
          username: 'user1',
          code: 'create_space_role_error',
        },
      ],
    };
  }
}
