import { mock } from 'mockjs';

import { Controller, RequestMapping } from '../decorators';

@Controller
export class RBACController {
  @RequestMapping('/v1/role-types/:org_name')
  getRoleTypes(ctx) {
    ctx.body = mock([
      {
        role_uuid: '@id',
        role_name: 'role1',
        template_display_name: '项目测试',
        template_uuid: '@id',
      },
      {
        role_uuid: '@id',
        role_name: 'role1',
        template_display_name: '项目管理员',
        template_uuid: '@id',
      },
      {
        role_uuid: '@id',
        role_name: 'role1',
        template_display_name: '项目开发',
        template_uuid: '@id',
      },
    ]);
  }
}
