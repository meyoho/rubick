import { mock } from 'mockjs';

import { Controller, Method, RequestMapping } from '../decorators';

const mockQuota = {
  resource_actions: ['view', 'update', 'delete'].map(
    action => 'resourcequotas:' + action,
  ),
  kubernetes: {
    apiVersion: 'v1',
    kind: 'ResourceQuota',
    metadata: {
      creationTimestamp: '@date()T@time()Z',
      name: '@pick(default,@word)',
      namespace: '@word',
      resourceVersion: '279347',
      selfLink: '/api/v1/namespaces/default/resourcequotas/object-quota-demo',
      uid: '@id',
    },
    spec: {
      hard: {
        pods: '100', //POD
        persistentvolumeclaims: '100', //PVC
        'requests.cpu': '1000M',
        'requests.memory': '1000M',
        'requests.storage': '1024M',
      },
    },
    status: {
      hard: {
        pods: '100', //POD
        persistentvolumeclaims: '100', //PVC
        'requests.cpu': '1000M',
        'requests.memory': '1000M',
        'requests.storage': '1024M',
      },
      used: {
        pods: '@integer(1,100)', //POD
        persistentvolumeclaims: '@integer(1,100)', //PVC
        'requests.cpu': '@integer(1,1000)M',
        'requests.memory': '@integer(1,1000)M',
        'requests.storage': '@integer(1,1000)M',
      },
    },
  },
};

@Controller
@RequestMapping('/v2/kubernetes/clusters/:cluster_id/resourcequotas/')
export class ResourceQuotaController {
  @RequestMapping(':namespace_id')
  getResourceQuotas(ctx) {
    ctx.body = mock({
      'result|0-5': [mockQuota],
    });
  }

  @RequestMapping(':namespace_id/:quota_name')
  getResourceQuota(ctx) {
    ctx.body = mock(mockQuota);
  }

  @RequestMapping({
    path: '',
    method: Method.POST,
  })
  createResourceQuota(ctx) {
    ctx.body = null;
  }

  @RequestMapping({
    path: ':namespace_id/:quota_name',
    method: Method.PUT,
  })
  updateResourceQuota(ctx) {
    ctx.body = null;
  }
}
