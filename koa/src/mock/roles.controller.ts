import { mock } from 'mockjs';

import { Controller, Method, RequestMapping } from '../decorators';

@Controller
@RequestMapping('/v1/roles/')
export class RolesV1Controller {
  @RequestMapping(':namespace/types')
  getTypes(ctx) {
    ctx.body = mock([
      {
        role_uuid: '@id',
        role_name: 'role1',
        template_display_name: '',
      },
      {
        role_uuid: '@id',
        role_name: 'role2',
        template_display_name: '@word',
      },
      {
        role_uuid: '@id',
        role_name: '@word',
        template_display_name: '@word',
      },
      {
        role_uuid: '@id',
        role_name: 'role4',
        template_display_name: '',
      },
      {
        role_uuid: '@id',
        role_name: '@word',
        template_display_name: '',
      },
    ]);
  }
}

// See: http://confluence.alaudatech.com/pages/viewpage.action?pageId=31920356#
@Controller
@RequestMapping('/v3/roles/')
export class RolesV3Controller {
  // Eg.: fetch('ajax/v3/roles/alauda/administrator', { headers: { 'RUBICK-AJAX-REQUEST': true } }).then(a => a.json()).then(console.log)
  @RequestMapping(':namespace/:name')
  get(ctx) {
    ctx.body = mock({
      resource_actions: [
        'role:assign',
        'role:create',
        'role:delete',
        'role:revoke',
        'role:update',
        'role:view',
      ],
      template_uuid: '',
      admin_role: false,
      project_name: 'system',
      uuid: '3094202b-9d7e-443c-81a7-5f2a39212162',
      project_uuid: '3f8673b0-b8f6-42de-b48d-21cc2d76b1de',
      created_at: '2018-11-27T00:28:36.573328Z',
      namespace: ctx.params.namespace,
      updated_at: '2018-11-27T06:17:37.145826Z',
      created_by: 'alauda',
      assigned_at: '0001-01-01T00:00:00Z',
      space_name: '',
      permissions: [
        {
          actions: ['application:create', 'application:view'],
          resource: ['testname'],
          uuid: '03531aa1-565a-4195-915c-645826220dfe',
          resource_type: 'application',
          constraints: [
            {
              'res:project': 'system',
            },
          ],
          group: '',
          api_group: '',
          resource_class: 'platform',
        },
        {
          actions: [
            'role:assign',
            'role:update',
            'role:create',
            'role:delete',
            'role:revoke',
          ],
          resource: ['rsname01', 'rsname02'],
          uuid: '0a3bc017-11f5-472a-8e82-af7adaede60f',
          resource_type: 'role',
          constraints: [
            {
              'res:project': 'testcons',
            },
          ],
          group: '',
          api_group: '',
          resource_class: 'platform',
        },
        {
          actions: ['service:*'],
          resource: ['*'],
          uuid: '28ac10c1-031b-477e-8658-7906b6b531e1',
          resource_type: 'service',
          constraints: [
            {
              'res:project': 'system',
            },
          ],
          group: '',
          api_group: '',
          resource_class: 'platform',
        },
        {
          actions: ['subaccount:create'],
          resource: ['*'],
          uuid: '41471838-f853-417d-b1d0-5681f0e3af48',
          resource_type: 'subaccount',
          group: '',
          api_group: '',
          resource_class: 'platform',
        },
        {
          actions: ['configmap:create', 'configmap:get'],
          resource: ['testname2'],
          resource_type: 'configmap',
          group: '',
          api_group: 'core',
          resource_class: 'kubernetes',
        },
        {
          actions: ['configmap:list'],
          resource: [],
          resource_type: 'configmap',
          group: '',
          api_group: 'core',
          resource_class: 'kubernetes',
        },
        {
          actions: ['pod:create', 'pod:get'],
          resource: [],
          resource_type: 'pod',
          group: '',
          api_group: 'core',
          resource_class: 'kubernetes',
          constraints: [
            {
              'res:project': 'testcons',
            },
          ],
        },
        {
          actions: ['pod:create', 'pod:get'],
          resource: ['testname1'],
          resource_type: 'pod',
          group: '',
          api_group: 'core',
          resource_class: 'kubernetes',
          constraints: [
            {
              'res:project': 'testcons',
            },
          ],
        },
        {
          actions: ['pod:create', 'pod:get'],
          resource: ['testname1'],
          resource_type: 'customcrd1',
          group: '',
          api_group: 'api.example.io',
          resource_class: 'kubernetes',
          constraints: [
            {
              'res:project': 'constraint0',
            },
            {
              'res:project': 'constraint1',
            },
          ],
        },
        {
          actions: ['*'],
          resource: ['testname1'],
          resource_type: 'customcrd1',
          group: '',
          api_group: 'api.example.io',
          resource_class: 'kubernetes',
          constraints: [
            {
              'res:project': '*',
            },
          ],
        },
      ],
      space_uuid: '',
      name: ctx.params.name,
    });
  }
}

// See: http://confluence.alaudatech.com/pages/viewpage.action?pageId=31920356#
@Controller
export class RolesV3RoleSchemaController {
  @RequestMapping('/v3/role-schema')
  getK8SSchema(ctx) {
    const result = [
      {
        constraints: ['res:project', 'res:cluster', 'res:namespace'],
        api_group: '',
        resource_type: 'application',
        actions: [
          'application:create',
          'application:delete',
          'application:update',
          'application:view',
        ],
        resource_class: 'platform',
      },
      {
        constraints: ['res:project', 'res:cluster', 'res:namespace'],
        api_group: '',
        resource_type: 'registry',
        actions: [
          'registry:create',
          'registry:delete',
          'registry:update',
          'registry:view',
        ],
        resource_class: 'platform',
      },
      {
        constraints: ['res:project', 'res:cluster', 'res:namespace'],
        api_group: 'core',
        resource_type: 'pod',
        actions: [
          'pod:create',
          'pod:patch',
          'pod:delete',
          'pod:update',
          'pod:get',
          'pod:list',
        ],
        resource_class: 'kubernetes',
      },
      {
        constraints: ['res:project', 'res:cluster', 'res:namespace'],
        api_group: 'core',
        resource_type: 'configmap',
        actions: [
          'configmap:create',
          'configmap:patch',
          'configmap:delete',
          'configmap:update',
          'configmap:get',
          'configmap:list',
        ],
        resource_class: 'kubernetes',
      },
      {
        constraints: ['res:project', 'res:cluster', 'res:namespace'],
        api_group: 'api.example.io',
        resource_type: 'customcrd0',
        actions: [
          'customcrd0:create',
          'customcrd0:patch',
          'customcrd0:delete',
          'customcrd0:update',
          'customcrd0:get',
          'customcrd0:list',
        ],
        resource_class: 'kubernetes',
      },
      {
        constraints: ['res:project', 'res:cluster', 'res:namespace'],
        api_group: 'api.example.io',
        resource_type: 'customcrd1',
        actions: [
          'customcrd1:create',
          'customcrd1:patch',
          'customcrd1:delete',
          'customcrd1:update',
          'customcrd1:get',
          'customcrd1:list',
        ],
        resource_class: 'kubernetes',
      },
    ];

    ctx.body = mock({
      result,
      code: 200,
    });
  }
}

// See: http://confluence.alaudatech.com/pages/viewpage.action?pageId=31920356#
@RequestMapping('/v3/role-templates/')
@Controller
export class RolesV3RoleTemplateController {
  @RequestMapping(':namespace/:name')
  getTemplate(ctx) {
    const result = {
      description: '负责绑定命名空间开发人员',
      updated_at: '2018-11-14T07:36:50Z',
      display_name_en: 'Namespace Admin',
      permissions: [
        {
          template_uuid: '9985d18e-64c1-4533-8a45-225cdd03ded1',
          resource: ['*'],
          uuid: '2a4e2c5c-5fba-11e8-8035-acbc32a179e7',
          actions: ['registry:view'],
          resource_type: 'registry',
          constraints: [],
          group: '',
          api_group: '',
          resource_class: 'platform',
        },
        {
          actions: ['configmap:create', 'configmap:get'],
          resource: ['testname2'],
          uuid: '50c543e7-4780-4dba-abc4-5132b3566a48',
          resource_type: 'configmap',
          group: '',
          api_group: 'core',
          resource_class: 'kubernetes',
        },
        {
          constraints: [
            {
              'res:project': 'system',
              'res:type': 'heheda',
            },
          ],
          api_group: 'api.example.io',
          resource_type: 'customcrd0',
          resource: ['testname2'],
          actions: [
            'customcrd0:create',
            'customcrd0:patch',
            'customcrd0:delete',
            'customcrd0:update',
            'customcrd0:get',
            'customcrd0:list',
          ],
          resource_class: 'kubernetes',
        },
      ],
      display_name: '命名空间管理员',
      uuid: '9985d18e-64c1-4533-8a45-225cdd03ded1',
      level: 'namespace',
      created_at: '2017-01-01T00:00:06Z',
      official: true,
      created_by: '',
      name: ctx.params.name,
      scope: {
        project: true,
        namespace: true,
      },
      resource_privilege: [],
    };

    ctx.body = mock(result);
  }

  @RequestMapping(':namespace/:name/generate', Method.POST)
  generate(ctx) {
    const result = [];
    ctx.body = mock({
      result,
      code: 200,
    });
  }
}

// See: http://confluence.alaudatech.com/pages/viewpage.action?pageId=31920356#
@Controller
export class RolesV3RolePermissionMapController {
  @RequestMapping('/v3/role-permission-map/:namespace/')
  getPermissionsMap(ctx) {
    ctx.body = mock({
      resource: {
        application: [
          {
            resource_type: 'pod',
            api_group: 'core',
            resource_class: 'kubernetes',
          },
          {
            resource_type: 'customcrd0',
            api_group: 'api.example.io',
            resource_class: 'kubernetes',
          },
        ],
      },
      action: {
        view: ['get', 'list'],
      },
    });
  }
}
