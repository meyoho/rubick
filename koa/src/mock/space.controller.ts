import { mock } from 'mockjs';

import { Controller, RequestMapping } from '../decorators';

@Controller
@RequestMapping('/v2/spaces/')
export class SpaceController {
  @RequestMapping(':namespace')
  getSpaces(ctx) {
    ctx.body = {
      result: [
        {
          status: 'running',
          resource_actions: [
            'space:consume',
            'space:create',
            'space:delete',
            'space:update',
            'space:view',
          ],
          uuid: 'cee9abf4-c901-496f-862e-5d942d99d3f2',
          display_name: 'chhu-group2',
          description: '',
          project_uuid: 'e5fba326-85b8-4e0c-9dc8-97dea0705449',
          created_at: '2018-12-06T08:00:09.103181Z',
          updated_at: '2018-12-10T03:19:57.508501Z',
          created_by: 'alauda',
          namespaces: [],
          name: 'chhu-group',
        },
        {
          status: 'running',
          resource_actions: [
            'space:consume',
            'space:create',
            'space:delete',
            'space:update',
            'space:view',
          ],
          uuid: 'b982b1f2-077b-46ba-af75-0ed57e1a1e02',
          display_name: 'morefree',
          description: '',
          project_uuid: 'e5fba326-85b8-4e0c-9dc8-97dea0705449',
          created_at: '2018-12-06T07:43:24.143667Z',
          updated_at: '2018-12-06T08:19:55.3201Z',
          created_by: 'alauda',
          namespaces: [],
          name: 'fasdfasdf231321',
        },
        {
          status: 'running',
          resource_actions: [
            'space:consume',
            'space:create',
            'space:delete',
            'space:update',
            'space:view',
          ],
          uuid: '5ab8a162-3db1-48d8-ad0f-86c23a45ea81',
          display_name: 'morefree',
          description: '',
          project_uuid: 'e5fba326-85b8-4e0c-9dc8-97dea0705449',
          created_at: '2018-12-10T05:04:33.732994Z',
          updated_at: '2018-12-10T05:04:33.732994Z',
          created_by: 'alauda',
          namespaces: [],
          name: 'space-mayday',
        },
        {
          status: 'running',
          resource_actions: [
            'space:consume',
            'space:create',
            'space:delete',
            'space:update',
            'space:view',
          ],
          uuid: 'b1634cda-7c4a-4052-bf18-386ffcede1f7',
          display_name: '组1',
          description: '',
          project_uuid: 'e5fba326-85b8-4e0c-9dc8-97dea0705449',
          created_at: '2018-12-06T07:32:48.944813Z',
          updated_at: '2018-12-06T07:32:48.944813Z',
          created_by: 'alauda',
          namespaces: [],
          name: 'space1',
        },
        {
          status: 'running',
          resource_actions: [
            'space:consume',
            'space:create',
            'space:delete',
            'space:update',
            'space:view',
          ],
          uuid: '7a286643-2eb0-4ed7-a3f5-adbfc3f3cac1',
          display_name: 'test',
          description: '',
          project_uuid: 'e5fba326-85b8-4e0c-9dc8-97dea0705449',
          created_at: '2018-12-06T08:06:40.327861Z',
          updated_at: '2018-12-06T08:06:40.327862Z',
          created_by: 'alauda',
          namespaces: [],
          name: 'test',
        },
        {
          status: 'running',
          resource_actions: [
            'space:consume',
            'space:create',
            'space:delete',
            'space:update',
            'space:view',
          ],
          uuid: 'b8016783-761c-4f7c-a890-ab6fff08ac64',
          display_name: 'safsadfsdfasfsadf',
          description: '',
          project_uuid: 'e5fba326-85b8-4e0c-9dc8-97dea0705449',
          created_at: '2018-12-06T08:12:23.201357Z',
          updated_at: '2018-12-06T08:12:23.201357Z',
          created_by: 'alauda',
          namespaces: [],
          name: 'tetssss',
        },
      ],
      code: 200,
    };
  }
}
