import * as Router from 'koa-router';

import consoleService from '../action/console';
import environmentsService from '../action/environments';
import healthCheckAction from '../action/health_check';
import imageproxy from '../action/img_url_proxy';
import landingService from '../action/landing';
import LoadbalancerService from '../action/loadbalancer';
import filteredList from '../action/orgs';
import PublicRepositoryService from '../action/public_repository';
import ssoService from '../action/sso';
import websshService from '../action/webssh';
import { login_required } from '../common/auth';

const router = new Router();

const AppRouters = [
  {
    path: '/',
    method: 'get',
    action: landingService.landingView,
    login_required: true,
  },
  {
    path: '/sso',
    method: 'get',
    action: ssoService.ssoLandingView,
    login_required: false,
  },
  {
    path: /^\/(console)(?:\/|$)/,
    method: 'get',
    action: consoleService.consoleView,
    login_required: true,
    sub_page_entry: true,
  },
  {
    path: /^\/terminal(?:\/|$)/,
    method: 'get',
    action: consoleService.terminalView,
    login_required: true,
  },
  {
    path: '/ajax/landing/login/requires-captcha',
    method: 'get',
    action: landingService.requiresCaptcha,
    login_required: false,
  },
  {
    path: /^\/landing(?:\/|$)/,
    method: 'get',
    action: landingService.landingView,
    login_required: false,
  },
  {
    path: '/ajax/ap/logout',
    method: 'get',
    action: landingService.logoutView,
    login_required: false,
  },
  {
    path: '/ap/logout',
    method: 'get',
    action: landingService.logoutRedirect,
    login_required: false,
  },
  {
    path: '/captcha-image/*',
    method: 'get',
    action: landingService.captchaImage,
    login_required: false,
  },
  {
    path: '/auth/mobile/exist',
    method: 'get',
    action: landingService.checkMobileExist,
    login_required: false,
  },
  {
    path: '/ap/register',
    method: 'post',
    action: landingService.register,
    login_required: false,
  },
  {
    path: '/ap/reset-password',
    method: 'post',
    action: landingService.resetPassword,
    login_required: false,
  },
  {
    path: '/ajax/landing/login/password/change-password',
    method: 'put',
    action: landingService.purePasswordLoginChangePassword,
    login_required: false,
  },
  {
    path: '/ajax/landing/login/password',
    method: 'post',
    action: landingService.purePasswordLogin,
    login_required: false,
  },
  {
    path: '/ajax/landing/login',
    method: 'post',
    action: landingService.login,
    login_required: false,
  },
  {
    path: '/ajax-sp/captcha-refresh/',
    method: 'get',
    action: landingService.captchaRefresh,
    login_required: false,
  },
  {
    path: '/ajax/user/sendsms',
    method: 'post',
    action: landingService.sendSms,
    login_required: false,
  },
  {
    path: '/ajax/user/sendCaptchaCode',
    method: 'post',
    action: landingService.sendCaptchaCode,
    login_required: false,
  },
  {
    path: '/ajax-sp/webssh',
    method: 'get',
    action: websshService.webssh,
  },
  {
    path: '/ajax-sp/account/weblabs',
    method: 'get',
    action: consoleService.weblabs,
    login_required: true,
  },
  {
    path: '/ajax-sp/nav-config',
    method: 'get',
    action: consoleService.navConfig,
    login_required: false,
  },
  {
    path: '/ajax-sp/nav-config/:view',
    method: 'get',
    action: consoleService.navConfig,
    login_required: false,
  },
  {
    path: '/ajax-sp/account/token',
    method: 'get',
    action: consoleService.token,
    login_required: true,
  },
  {
    path: '/ajax-sp/global/environments',
    method: 'get',
    action: environmentsService.getEnvironments,
    login_required: false,
  },
  {
    path:
      '/ajax-sp/v2/kubernetes/clusters/:cluster_id_name/:resource_type/:namespace/:name',
    method: 'patch',
    action: LoadbalancerService.handleLoadbalancerPatch,
    login_required: true,
  },
  {
    path: '/ajax/org/account/filtered_list',
    method: 'get',
    action: filteredList,
    login_required: true,
  },
  {
    path: '/ajax/image-proxy',
    method: 'get',
    action: imageproxy.proxy,
  },
  {
    path: '/ajax/current_time',
    method: 'get',
    action: consoleService.currentTime,
  },
];

const publicRepoRoutes = [
  {
    path: '/ajax/public_repository/exist/',
    method: 'get',
    action: PublicRepositoryService.PublicRepositoryExist,
    login_required: true,
  },
  {
    path: '/ajax/public_repository/tag/list',
    method: 'get',
    action: PublicRepositoryService.publicRepositoryTagList,
    login_required: true,
  },
  {
    path: '/ajax/public_repository/tag/',
    method: 'get',
    action: PublicRepositoryService.publicRepositoryTag,
    login_required: true,
  },
];

const healthCheckRoutes = [
  {
    path: '/ping',
    method: 'get',
    action: healthCheckAction.ping,
    login_required: true,
  },
  {
    path: '/_ping',
    method: 'get',
    action: healthCheckAction.ping,
    login_required: true,
  },
  {
    path: '/diagnose',
    method: 'get',
    action: healthCheckAction.diagnose,
    login_required: true,
  },
  {
    path: '/_diagnose',
    method: 'get',
    action: healthCheckAction.diagnose,
    login_required: true,
  },
];

const allRoutes = AppRouters.concat(healthCheckRoutes).concat(publicRepoRoutes);

allRoutes.forEach(route => {
  router[route.method](
    route.path,
    route.login_required ? login_required : (ctx, next) => next(),
    route.action,
  );
});

export { router };
