import * as Koa from 'koa';

import { User } from '../common/auth';

declare module 'koa' {
  interface Context {
    matched?: any[];
    user: User;
    skipLogin: boolean;
    ssoConfigs: OidcConfig[];
  }
}

export default Koa;
