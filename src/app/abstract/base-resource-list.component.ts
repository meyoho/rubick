import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { ChangeDetectorRef, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { debounce } from 'lodash-es';
import { BehaviorSubject, combineLatest, Observable, of } from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import {
  isHttpErrorResponse,
  isNotHttpErrorResponse,
  ListMeta,
  PageParams,
  ResourceList,
} from 'app_user/core/types';

export interface FetchParams {
  search: string;
  pageParams: PageParams;
}

export interface TableState {
  search?: string;

  // page params
  pageSize?: number;
  pageIndex?: number;
}

/**
 * Base for resource list. Note, unlike simple resource list, this normally requires
 * the user to feed standalone fetch methods.
 *
 * Standard unidirection data flow:
 * [Action](setTableState)
 * ↓↓↓
 * [Table State](construct fetch params)
 * ↓↓↓
 * [Fetch Params](fetchResources)
 * ↓↓↓
 * [Fetch Observable](map)
 * ↓↓↓
 * [Resource List]
 *
 * Typically, every fetch is made through setTableState. A fetch will land if
 * tableState$ emits a new state.
 */
export abstract class BaseResourceListComponent<R extends any>
  implements OnInit {
  private defaultPageIndex = 1;
  private defaultPageSize = 10;
  private currentPageIndex = 1;
  protected poll$ = new BehaviorSubject(null);
  protected debouncedUpdate: () => void;

  updated$ = new BehaviorSubject(null);
  fetchParams$: Observable<FetchParams>;

  rawResponse$: Observable<ResourceList | HttpErrorResponse>;
  list$: Observable<R[]>;
  listMeta$: Observable<ListMeta>;

  // List of fetch related params. They should be read only most of the time.
  pageParams$: Observable<PageParams>;
  search$: Observable<string>;

  fetching = false;
  searching = false;
  showZeroState = true;

  /**
   * Override this if you want to change poll timing.
   */
  getPollInterval(): number {
    return 15000;
  }

  /**
   * The fetch method to be called upon various parameters.
   */
  abstract fetchResources(
    params: FetchParams,
  ): Observable<ResourceList | Error>;

  /**
   * Map API result into Resource list
   */
  map(value: ResourceList): any[] {
    return value.results;
  }

  /**
   * Jump to the previous page if there is no data and not the first page
   */
  emptyListCheck(value: ResourceList) {
    if (!value || !value.results) {
      return;
    }
    if (!value.results.length && this.currentPageIndex > 1) {
      this.setTableState({ pageIndex: --this.currentPageIndex });
    }
  }
  /**
   * The source of the table's state.
   * One particular usage is the activatedRoute's params.
   */
  getTableState$() {
    return this.activedRoute.queryParams;
  }

  /**
   * Set new table state. Returns true if state is successfully updated.
   */
  setTableState(newState: TableState) {
    const mergedParams = {
      pageIndex: this.defaultPageIndex,
      pageSize: this.defaultPageSize,
      ...this.activedRoute.snapshot.queryParams,
      ...newState,
    };
    return this.router.navigate(['.'], {
      queryParams: mergedParams,
      relativeTo: this.activedRoute,
    });
  }

  /**
   * Called when data is updated edgerly.
   */
  onUpdate(item: any) {
    this.updated$.next(item);
  }

  ngOnInit() {
    // The ordering of the following is significant.
    this.initStateParams$();
    this.initFetchParams$();
    this.initRawResponse$();
    this.initList$();
  }

  onPageEvent(pageEvent: Partial<PageParams>) {
    this.setTableState(pageEvent);
  }

  async searchByName(name: string) {
    // Only mark as searching state when set state success.
    this.searching = await this.setTableState({
      search: name,
    });
    this.cdr.markForCheck();
  }

  initStateParams$() {
    const tableState$ = this.getTableState$();

    this.pageParams$ = tableState$.pipe(
      tap(
        params =>
          (this.currentPageIndex = params.pageIndex || this.defaultPageIndex),
      ),
      map(params => ({
        pageSize: +(params.pageSize || this.defaultPageSize),
        pageIndex: +(params.pageIndex || this.defaultPageIndex),
      })),
    );

    this.search$ = tableState$.pipe(map(params => params.search || ''));
  }

  /**
   * Init fetching params to be used when fetching list result.
   */
  initFetchParams$() {
    this.fetchParams$ = this.getDefaultFetchParams$();
    return this.fetchParams$;
  }

  getDefaultFetchParams$() {
    return combineLatest(
      this.search$,
      this.pageParams$,

      // Polling pulses
      this.poll$,
      this.updated$,
    ).pipe(
      map(([search, pageParams]) => ({
        search,
        pageParams,
      })),
    );
    return this.fetchParams$;
  }

  /**
   * Initialize raw response of the given resource. The result will be used for
   */
  initRawResponse$(): Observable<ResourceList | HttpErrorResponse> {
    this.rawResponse$ = this.fetchParams$.pipe(
      debounceTime(50),
      tap(() => {
        this.fetching = true;
        this.cdr.markForCheck();
      }),
      switchMap(params =>
        this.fetchResources(params).pipe(catchError(error => of(error))),
      ),
      tap(res => {
        this.fetching = false;
        this.searching = false;
        this.showZeroState =
          isHttpErrorResponse(res) ||
          (res as ResourceList).results.length === 0;
        this.cdr.markForCheck();
        this.debouncedUpdate();
      }),
      publishReplay(1),
      refCount(),
    );

    return this.rawResponse$;
  }

  /**
   * Init list$ related observables.
   */
  initList$() {
    this.listMeta$ = this.rawResponse$.pipe(
      filter(res => isNotHttpErrorResponse(res)),
      map((res: ResourceList) => {
        return {
          count: res.count,
          num_pages: res.num_pages,
        };
      }),
    );

    this.list$ = this.rawResponse$.pipe(
      filter(res => isNotHttpErrorResponse(res)),
      tap((res: ResourceList) => this.emptyListCheck(res)),
      map((res: ResourceList) => this.map(res)),
    );
  }

  constructor(
    public http: HttpClient,
    public router: Router,
    public activedRoute: ActivatedRoute,
    public cdr: ChangeDetectorRef,
  ) {
    this.debouncedUpdate = debounce(() => {
      this.poll$.next(null);
    }, this.getPollInterval());
  }
}
