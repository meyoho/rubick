if (!window['rubick']) {
  window['rubick'] = new Map();
}

export function setGlobal(key: any, value: any) {
  window['rubick'].set(key, value);
}

export function getGlobal(key: any) {
  return window['rubick'].get(key);
}
