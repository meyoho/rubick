import { Component, OnInit, ViewEncapsulation } from '@angular/core';

import { RouterUtilService } from './services/router-util.service';
import { transportCode } from './utils/oauth-code';

@Component({
  selector: 'rc-root',
  template: `
    <router-outlet></router-outlet>
  `,
  styleUrls: ['app.component.scss'],
  encapsulation: ViewEncapsulation.None,
})
export class AppComponent implements OnInit {
  constructor(_routerUtil: RouterUtilService) {}

  ngOnInit(): void {
    const queryParams = getParams(window.location.search);
    if (
      ['true', '1'].includes(
        (queryParams.is_secret_validate || '').trim().toLowerCase(),
      )
    ) {
      transportCode(queryParams.code);
      return;
    }
  }
}

function getParams(search: string): { [name: string]: string } {
  if (!search) {
    return {};
  }
  const queryString = search.substr(1);
  if (!queryString) {
    return {};
  }

  return queryString.split('&').reduce((accum: any, segment: string) => {
    if (!segment) {
      return accum;
    }
    const [key, value] = segment.split('=');
    if (!key) {
      return accum;
    }
    return {
      ...accum,
      [key]: value,
    };
  }, {});
}
