import { ApplicationRef, enableProdMode } from '@angular/core';
import { enableDebugTools } from '@angular/platform-browser';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { ENVIRONMENTS, I18NMANIFEST } from 'app/shared/tokens';
import {
  initEnvironments,
  initI18nManifest,
  initStateInjectToken,
} from 'app/utils/bootstrap';
import { setGlobal } from './app-global';
import { AppModule } from './app.module';
import './vendor.entry';

// Custom assets dir
const customAssetsDir = '/static/custom';
const customCssName = 'custom.css';

async function main() {
  const [environments, i18nManifest] = await Promise.all([
    initEnvironments(),
    initI18nManifest(),
  ]);

  // must before initTranslations
  setGlobal(I18NMANIFEST, i18nManifest);

  // Enable custom css:
  if (environments.is_private_deploy_enabled) {
    const elLink = document.createElement('link');
    elLink.href = `${customAssetsDir}/${customCssName}`;
    elLink.rel = 'stylesheet';
    document.head.appendChild(elLink);
  }

  setGlobal(ENVIRONMENTS, environments);

  if (!environments.debug) {
    enableProdMode();
  } else {
    Error.stackTraceLimit = Infinity;
    // require('zone.js/dist/long-stack-trace-zone');
  }

  await initStateInjectToken();

  const platformRef = await platformBrowserDynamic().bootstrapModule(AppModule);

  if (environments.debug) {
    const appRef = platformRef.injector.get(ApplicationRef);
    const appComponent = appRef.components[0];
    enableDebugTools(appComponent);
  }
}

main();
