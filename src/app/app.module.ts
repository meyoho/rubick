import {
  IconModule,
  IconRegistryService,
  MessageModule,
  MessageType,
  NOTIFICATION_CONFIG,
  NotificationGlobalConfig,
  NotificationModule,
} from '@alauda/ui';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import {
  RouterStateSerializer,
  StoreRouterConnectingModule,
} from '@ngrx/router-store';
import { StoreModule } from '@ngrx/store';
import { StoreDevtoolsModule } from '@ngrx/store-devtools';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { CustomSerializer, metaReducers, reducers } from 'app/store';
import { GlobalTranslateModule } from 'app/translate/translate.module';
import { getGlobal } from './app-global';
import { AppComponent } from './app.component';
import { EntryRoutingModule } from './app.routing.module';
import { TranslateService } from './translate/translate.service';

const basicIconsUrl = require('@alauda/ui/assets/basic-icons.svg');

export const DEFAULT_NOTIFICATION_OPTIONS: NotificationGlobalConfig = {
  maxStack: 8,
  duration: {
    [MessageType.Success]: 6000,
    [MessageType.Warning]: 6000,
    [MessageType.Error]: 0,
    [MessageType.Info]: 6000,
  },
};

@NgModule({
  imports: [
    // angular modules
    BrowserModule,
    BrowserAnimationsModule,
    HttpClientModule,
    // ngrx
    StoreModule.forRoot(reducers, { metaReducers }),
    process.env.NODE_ENV === 'development'
      ? StoreDevtoolsModule.instrument({ maxAge: 25 })
      : [],
    StoreRouterConnectingModule.forRoot({
      stateKey: 'router',
    }),
    // aui modules
    NotificationModule,
    MessageModule,
    IconModule,
    // global shared modules
    GlobalTranslateModule,
    // routing module
    EntryRoutingModule,
  ],
  // Defines the components that should be bootstrapped when
  // this module is bootstrapped. The components listed here
  bootstrap: [AppComponent],
  declarations: [AppComponent],

  providers: [
    { useFactory: () => getGlobal(ENVIRONMENTS), provide: ENVIRONMENTS },
    {
      provide: NOTIFICATION_CONFIG,
      useValue: DEFAULT_NOTIFICATION_OPTIONS,
    },
    {
      provide: RouterStateSerializer,
      useClass: CustomSerializer,
    },
  ],
})
export class AppModule {
  constructor(
    iconRegistryService: IconRegistryService,
    translate: TranslateService,
  ) {
    iconRegistryService.registrySvgSymbolsByUrl(basicIconsUrl);
    iconRegistryService.registrySvgSymbolsByUrl(
      'static/images/pipeline_new/diablo-icons.svg',
    );
    translate.currentLang$.subscribe(lang => {
      document.querySelector('body').setAttribute('lang', lang);
    });
  }
}
