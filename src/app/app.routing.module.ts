import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DelayPreloadingStrategy } from './delay-preloading-strategy';
/**
 * Define base level App base routes.
 */
export const appBaseRoutes: Routes = [
  {
    path: 'console',
    loadChildren: './console/console.module#ConsoleModule',
  },
  {
    path: 'landing',
    loadChildren: '../landing/app/app.module#AppModule',
  },
];

// Refer to https://angular.io/docs/ts/latest/guide/router.html
// see why we may want to use a module to define
@NgModule({
  imports: [
    RouterModule.forRoot(appBaseRoutes, {
      preloadingStrategy: DelayPreloadingStrategy,
    }),
  ],
  exports: [RouterModule],
  providers: [DelayPreloadingStrategy],
})
export class EntryRoutingModule {}
