import { Location } from '@angular/common';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import * as Highcharts from 'highcharts';

import { TranslateService } from 'app/translate/translate.service';

@Component({
  selector: 'rc-console-root',
  template: '<router-outlet></router-outlet>',
})
export class ConsoleComponent implements OnInit, OnDestroy {
  constructor(
    private router: Router,
    private location: Location,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    Highcharts.setOptions({
      global: {
        useUTC: false,
      },
      lang: {
        noData: this.translate.get('no_data'),
      },
    });
    document.addEventListener('click', this.linkHandler);
  }

  ngOnDestroy() {
    document.removeEventListener('click', this.linkHandler);
  }

  // Workaround for events href links, https://stackoverflow.com/a/34787493
  private linkHandler = async (e: MouseEvent) => {
    const el = this.getTargetLinkElement(e.target as Node);
    if (el) {
      const href = el.getAttribute('href');
      if (!href || href === '#' || href.startsWith('javascript:')) {
        e.preventDefault();
        return false;
      }
      const { pathname } = el;
      if (pathname && pathname.startsWith('/console/')) {
        e.preventDefault();
        this.router.navigateByUrl(
          this.location.normalize(pathname) + el.search,
        );
        return false;
      }
    }
  };

  private getTargetLinkElement(el: Node): HTMLAnchorElement {
    while (el) {
      if (el.nodeName === 'A') {
        return el as HTMLAnchorElement;
      }
      el = el.parentNode;
    }
    return null;
  }
}
