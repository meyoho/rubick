import { CodeEditorIntl, CodeEditorModule } from '@alauda/code-editor';
import { PaginatorIntl, TooltipCopyIntl } from '@alauda/ui';
import { HttpClientModule } from '@angular/common/http';
import { NgModule, NgModuleRef } from '@angular/core';
import { NavigationEnd, Router } from '@angular/router';
import { EffectsModule } from '@ngrx/effects';

import { get } from 'lodash-es';
import { MonacoProviderService } from 'ng-monaco-editor';
import { filter } from 'rxjs/operators';

import { getGlobal } from 'app/app-global';
import { AppPaginatorIntl } from 'app/services/app-paginator-intl.service';
import { AppTooltipCopyIntl } from 'app/services/app-tooltip-copy-intl.service';
import { CustomCodeEditorIntlService } from 'app/services/code-editor-intl.service';
import { CustomMonacoProviderService } from 'app/services/custom-monaco-provider';
import { DefaultErrorMapperService } from 'app/services/default-error-mapper.service';
import { HttpService } from 'app/services/http.service';
import { UiStateService } from 'app/services/ui-state.service';
import { SharedModule } from 'app/shared/shared.module';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import { effects } from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { ConsoleComponent } from './console.component';
import { ConsoleRoutingModule } from './console.routing.module';
import { FeaturesServicesModule } from './features-services.module';

const DEFAULT_MONACO_OPTIONS = {
  fontSize: 12,
  folding: true,
  scrollBeyondLastLine: false,
  minimap: { enabled: false },
  find: { seedSearchStringFromSelection: false, autoFindInSelection: false },
  mouseWheelZoom: true,
  scrollbar: {
    vertical: 'visible' as 'auto' | 'visible' | 'hidden',
    horizontal: 'visible' as 'auto' | 'visible' | 'hidden',
  },
  fixedOverflowWidgets: true,
};
@NgModule({
  imports: [
    HttpClientModule,
    SharedModule,
    FeaturesServicesModule,
    // ngrx
    EffectsModule.forRoot(effects),
    CodeEditorModule.forRoot({
      baseUrl: '/static/monaco_lib/v1',
      defaultOptions: DEFAULT_MONACO_OPTIONS,
    }),
    ConsoleRoutingModule,
  ],
  exports: [],
  declarations: [ConsoleComponent],
  providers: [
    { useFactory: () => getGlobal(WEBLABS), provide: WEBLABS },
    {
      useFactory: () => getGlobal(ACCOUNT),
      provide: ACCOUNT,
    },
    {
      provide: MonacoProviderService,
      useClass: CustomMonacoProviderService,
    },
    {
      provide: CodeEditorIntl,
      useClass: CustomCodeEditorIntlService,
    },
    {
      provide: PaginatorIntl,
      useClass: AppPaginatorIntl,
    },
    {
      provide: TooltipCopyIntl,
      useClass: AppTooltipCopyIntl,
    },
    HttpService,
    UiStateService,
    DefaultErrorMapperService,
  ],
})
export class ConsoleModule {
  constructor(
    // Inject the following to make sure they are loaded ahead of other components.
    _translate: TranslateService,
    monacoProvider: MonacoProviderService,
    private router: Router,
    private consoleModule: NgModuleRef<ConsoleModule>,
  ) {
    monacoProvider.initMonaco();
    const subscription = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(() => {
        const url = this.router.routerState.snapshot.url;
        const consoleRoute = this.router.config.find(
          route => route.path === 'console',
        );
        if (
          url.startsWith('/landing') &&
          get(consoleRoute, '_loadedConfig.module._destroyed') === false
        ) {
          consoleRoute['_loadedConfig'] = undefined;
          this.consoleModule.destroy();
          subscription.unsubscribe();
        }
      });
  }
}
