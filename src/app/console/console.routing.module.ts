import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ConsoleComponent } from './console.component';

/**
 * Define base level App base routes.
 */
export const routes: Routes = [
  {
    path: '',
    component: ConsoleComponent,
    children: [
      {
        path: 'user',
        loadChildren: '../../app_user/app.module#AppModule',
      },
      {
        path: 'admin',
        loadChildren: '../../app2/app.module#AppModule',
      },
      {
        path: 'user-center',
        loadChildren: './user-center/module#UserCenterModule',
      },
      {
        path: '',
        redirectTo: 'user',
        pathMatch: 'full',
      },
    ],
  },
];

// Refer to https://angular.io/docs/ts/latest/guide/router.html
// see why we may want to use a module to define
@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ConsoleRoutingModule {}
