import { NgModule } from '@angular/core';

import { AccountService } from 'app/services/api/account.service';
import { AlarmService } from 'app/services/api/alarm.service';
import { AppCatalogService } from 'app/services/api/app-catalog.service';
import { AppService } from 'app/services/api/app.service';
import { AppUtilitiesService } from 'app/services/api/app.utilities.service';
import { AuditService } from 'app/services/api/audit.service';
import { CertificateService } from 'app/services/api/certificate.service';
import { ConfigSecretService } from 'app/services/api/config-secret.service';
import { ConfigmapService } from 'app/services/api/configmap.service';
import { CredentialApiService } from 'app/services/api/credential.service';
import { DomainService } from 'app/services/api/domain.service';
import { EnvironmentService } from 'app/services/api/environment.service';
import { EventService } from 'app/services/api/event.service';
import { ImageProjectService } from 'app/services/api/image-project.service';
import { ImageRegistryService } from 'app/services/api/image-registry.service';
import { ImageRepositoryService } from 'app/services/api/image-repository.service';
import { IntegrationCenterService } from 'app/services/api/integration-center.service';
import { IntegrationService } from 'app/services/api/integration.service';
import { SonarQubeService } from 'app/services/api/integration.sonar-qube.service';
import { JenkinsService } from 'app/services/api/jenkins.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { LogService } from 'app/services/api/log.service';
import { MetricService } from 'app/services/api/metric.service';
import { MonitorService } from 'app/services/api/monitor.service';
import { MonitorUtilitiesService } from 'app/services/api/monitor.utilities.service';
import { NamespaceService } from 'app/services/api/namespace.service';
import { NetworkService } from 'app/services/api/network.service';
import { NotificationService } from 'app/services/api/notification.service';
import { OrgService } from 'app/services/api/org.service';
import { PodService } from 'app/services/api/pod.service';
import { PrivateBuildCodeClientService } from 'app/services/api/private-build-code-client.service';
import { ProjectService } from 'app/services/api/project.service';
import { RBACService } from 'app/services/api/rbac.service';
import { RegionService } from 'app/services/api/region.service';
import { RegionUtilitiesService } from 'app/services/api/region.utilities.service';
import { RegistryApiService } from 'app/services/api/registry/registry-api.service';
import { RSRCManagementService } from 'app/services/api/resource-manage.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { RoleService } from 'app/services/api/role.service';
import { SpaceService } from 'app/services/api/space.service';
import { StorageService } from 'app/services/api/storage.service';
import { TerminalService } from 'app/services/api/terminal.service';
import { OauthValidatorService } from 'app/services/api/tool-chain/oauth-validator.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { LoggerUtilitiesService } from 'app/services/logger.service';
import { PatternHelperService } from 'app/services/pattern-helper.service';
import { TimeService } from 'app/services/time.service';
import { YamlCommentParserService } from 'app/services/yaml-comment-parser.service';

const FEATURE_SERVICES = [
  AccountService,
  AlarmService,
  AppCatalogService,
  AppService,
  AppUtilitiesService,
  AuditService,
  CertificateService,
  ConfigmapService,
  ConfigSecretService,
  DomainService,
  EnvironmentService,
  EventService,
  ImageProjectService,
  ImageRegistryService,
  ImageRepositoryService,
  IntegrationCenterService,
  IntegrationService,
  JenkinsService,
  K8sResourceService,
  LogService,
  MetricService,
  MonitorService,
  MonitorUtilitiesService,
  NamespaceService,
  NetworkService,
  NotificationService,
  OrgService,
  PodService,
  PrivateBuildCodeClientService,
  ProjectService,
  RBACService,
  RegionService,
  RegionUtilitiesService,
  RoleService,
  RoleUtilitiesService,
  RSRCManagementService,
  SonarQubeService,
  StorageService,
  CredentialApiService,
  TerminalService,
  SpaceService,
];

const HELPER_SERVICES = [
  ErrorsToastService,
  LoggerUtilitiesService,
  PatternHelperService,
  YamlCommentParserService,
  ToolChainApiService,
  OauthValidatorService,
  TimeService,
  RegistryApiService,
];

@NgModule({
  providers: [...FEATURE_SERVICES, ...HELPER_SERVICES],
})
export class FeaturesServicesModule {}
