import { DialogRef } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { AccountService } from 'app/services/api/account.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

import { AvatarImgData } from './avatar-uploader.component';
@Component({
  templateUrl: './avatar-dialog.component.html',
  styleUrls: ['./avatar-dialog.component.scss'],
})
export class AvatarDialogComponent {
  src: string;
  previewing: boolean;
  sizeExceeded: boolean;

  constructor(
    private dialogRef: DialogRef,
    public accountService: AccountService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  onFilesChange(elem: HTMLInputElement) {
    const file = elem.files[0];
    const fileReader = new FileReader();

    fileReader.addEventListener('load', () => {
      if (file.size <= 3 * 1024 * 1024) {
        this.sizeExceeded = false;
        this.previewing = true;
        this.src = fileReader.result as string;
      } else {
        this.sizeExceeded = true;
      }
    });

    fileReader.addEventListener('error', () => {
      fileReader.abort();
    });

    if (file) {
      fileReader.readAsDataURL(file);
    }
  }

  async update(avatarFile: HTMLInputElement) {
    if (this.src) {
      this.dialogRef.close({
        url: this.src,
        files: avatarFile.files,
      } as AvatarImgData);
    }
  }

  cancel() {
    this.dialogRef.close();
  }
}
