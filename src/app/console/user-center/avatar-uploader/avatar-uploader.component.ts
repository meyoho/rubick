import { DialogService } from '@alauda/ui';
import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AccountService } from 'app/services/api/account.service';
import { first } from 'rxjs/operators';

import { AvatarDialogComponent } from '../avatar-uploader/avatar-dialog.component';

export interface AvatarImgData {
  url: string;
  files: FileList;
}

@Component({
  selector: 'rc-avatar-uploader',
  templateUrl: './avatar-uploader.component.html',
  styleUrls: ['./avatar-uploader.component.scss'],
})
export class AvatarUploaderComponent implements OnInit {
  @Input()
  canUpdate = false;
  @Output()
  change = new EventEmitter<AvatarImgData>();

  constructor(
    private dialogService: DialogService,
    public accountService: AccountService,
  ) {}

  async ngOnInit() {
    const profile = await this.accountService.getAuthProfile(false);
    this.accountService.avatarPath$$.next(profile.logo_file);
  }

  openDialog() {
    const dialogRef = this.dialogService.open(AvatarDialogComponent);

    dialogRef
      .afterClosed()
      .pipe(first())
      .subscribe((res: AvatarImgData) => {
        if (res) {
          this.change.emit(res);
        }
      });
  }
}
