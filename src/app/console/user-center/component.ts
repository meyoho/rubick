import { TemplatePortal } from '@angular/cdk/portal';
import { Component, Injector, OnInit } from '@angular/core';
import {
  TemplateHolderType,
  UiStateService,
} from 'app/services/ui-state.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './template.html',
  styleUrls: ['../../shared/layout/layout.common.scss', './styles.scss'],
})
export class UserCenterComponent implements OnInit {
  pageHeaderContentTemplate$: Observable<TemplatePortal<any>>;
  environments: Environments;
  userDocsUrl: string;

  constructor(injector: Injector, private uiState: UiStateService) {
    this.environments = injector.get(ENVIRONMENTS);
  }

  ngOnInit() {
    this.userDocsUrl = this.environments.user_docs_url;
    this.pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
      TemplateHolderType.PageHeaderContent,
    ).templatePortal$;
  }
}
