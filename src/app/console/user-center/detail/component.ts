import { DialogService, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { cloneDeep } from 'lodash-es';

import {
  AccountService,
  CodeClient,
  RcAccount,
} from 'app/services/api/account.service';
import { OrgService } from 'app/services/api/org.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { RoleService } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { FormBase } from '../form-base';
import { ACCOUNT_MODELS_VIEW, USER_MODELS_VIEW } from '../form-models';
import { filterToken, getFormModel, getUserType } from '../form-utils';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class UserCenterDetailComponent implements OnInit {
  models: FormBase<any>[] = [];
  private user: RcAccount;
  username: string;
  /**
   * 账号中心 -> 路由 username参数为空 （根账号登录，从account-menu进入）
   * 用户中心 -> 路由 username参数不为空
   */
  isSubaccount: boolean;
  initialized: boolean;
  codeClients: CodeClient[];

  canUpdateInfo = false;
  canDeleteUser = false;
  canUpdatePassword = false;
  operationViewEnabled = false;

  constructor(
    @Inject(ACCOUNT) public account: Account,
    private activatedRoute: ActivatedRoute,
    private accountService: AccountService,
    private orgService: OrgService,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private notificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
    private roleService: RoleService,
    private roleUtil: RoleUtilitiesService,
  ) {}

  ngOnInit() {
    this.roleUtil
      .resourceTypeSupportPermissions('operation_view', {}, 'get')
      .then(res => (this.operationViewEnabled = res));

    this.activatedRoute.queryParams.subscribe(async params => {
      this.initialized = false;
      this.username = params.username;

      const promises: Promise<any>[] = [
        this.accountService.getAuthProfile(false),
      ];
      let profile, user, codeClients;
      const modelsMap: {
        type?: string;
        token?: { name: string; value: string }[];
        is_valid?: boolean;
        account?: string;
      } = {}; /** 替换model的value */

      if (!params.username) {
        this.isSubaccount = false;
        this.canUpdateInfo = true;
        this.canDeleteUser = false;

        this.models = cloneDeep(ACCOUNT_MODELS_VIEW);
        promises.push(this.accountService.getPrivateBuildCodeClients());
        [profile, codeClients] = await Promise.all(promises);
        this.codeClients = codeClients;
      } else {
        const resource_type = 'subaccount';
        const result = await this.roleService.getContextPermissions(
          resource_type,
          {
            subaccount_username: this.username,
          },
        );
        this.canUpdateInfo = result.includes(`${resource_type}:update`);
        const hasDeleteUserPermission = result.includes(
          `${resource_type}:delete`,
        );
        const hasUpdatePasswordPermission = result.includes(
          `${resource_type}:update_password`,
        );

        this.isSubaccount = true;
        this.models = cloneDeep(USER_MODELS_VIEW);

        promises.push(this.orgService.getUser(params.username));
        [profile, user] = await Promise.all(promises);
        this.user = user;
        this.canDeleteUser =
          this.account.username !== this.username &&
          hasDeleteUserPermission &&
          !(user.type !== 'organizations.Account' && user.is_valid);

        if (
          this.account.username === this.username ||
          (hasUpdatePasswordPermission && user.type === 'organizations.Account')
        ) {
          this.models = this.models.map(model => {
            if (model.label === 'password') {
              model.readonly = false;
            }
            return model;
          });
        }

        modelsMap.type = this.translateService.get(getUserType(user.type));
        modelsMap.is_valid = this.translateService.get(
          user.is_valid ? 'valid' : 'invalid',
        );
        modelsMap.account = profile.company;
      }

      if (this.account.username !== this.username && this.isSubaccount) {
        this.models = this.models.filter(model => model.label !== 'token');
      } else {
        const tokenMap = await this.accountService.getTokens();
        modelsMap.token = filterToken(tokenMap);
      }

      // override the default value
      this.models = getFormModel(
        this.models,
        Object.assign({}, profile, user, modelsMap),
      );
      this.initialized = true;
    });
  }

  async deleteAccount() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('confirm_remove_account_from_org', {
          username: this.user.username,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });

      if (
        this.user.type === 'organizations.LDAPAccount' &&
        !this.user.is_valid
      ) {
        await this.orgService.deleteLdapAccounts({
          usernames: this.user.username,
        });
      } else {
        await this.orgService.removeOrgAccount({
          username: this.user.username,
        });
      }

      this.notificationService.success({
        content: this.translateService.get('delete_user_successfully', {
          username: this.user.username,
        }),
      });
      this.goback();
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  goback() {
    // FIXME: 需要在url里记录是从用户视图还是管理视图过来的
    this.router.navigate(['/console/user']);
  }

  get title() {
    return !!this.username
      ? this.translateService.get('user_center')
      : this.translateService.get('account_center');
  }
}
