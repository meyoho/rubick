export class FormBase<T> {
  value?: T;
  key?: string;
  label?: string;
  pattern?: RegExp;
  required?: boolean;
  readonly?: boolean;
  controlType?: 'text' | 'select';
  options?: { label: string; value: any }[];
  message?: string;

  constructor(
    options: {
      value?: any;
      key?: string;
      label?: string;
      pattern?: RegExp;
      required?: boolean;
      readonly?: boolean;
      controlType?: 'text' | 'select';
      options?: { label: string; value: any }[];
      message?: string;
    } = {},
  ) {
    this.value = options.value;
    this.key = options.key || '';
    this.label = options.label || this.key;
    this.required = !!options.required;
    this.readonly = options.readonly;
    this.pattern = options.pattern;
    this.controlType = options.controlType;
    this.options = options.options;
    this.message = options.message;
  }
}
