import {
  ACCOUNT_EMAIL_PATTERN,
  ACCOUNT_ENTERPRISE_NAME_PATTERN,
  ACCOUNT_MOBILE_PATTERN,
} from 'app/utils/patterns.ts';
import { FormBase } from './form-base';

export const ACCOUNT_MODELS_VIEW: FormBase<any>[] = [
  {
    label: 'account',
    key: 'username',
  },
  {
    label: 'mobile',
    key: 'mobile',
  },
  {
    label: 'enterprise_name',
    key: 'company',
  },
  {
    label: 'email',
    key: 'email',
  },
  {
    label: 'token',
    key: 'token',
  },
  {
    label: 'password',
    key: 'password',
    value: '******',
    readonly: false,
  },
  {
    label: 'created_at',
    key: 'joined_at',
  },
];

export const USER_MODELS_VIEW: FormBase<any>[] = [
  {
    label: 'username',
    key: 'username',
  },
  {
    label: 'own_account',
    key: 'account',
  },
  {
    label: 'password',
    key: 'password',
    value: '******',
    readonly: true,
  },
  {
    label: 'own_company',
    key: 'company',
  },
  {
    label: 'alias_name',
    key: 'realname',
  },
  {
    label: 'department',
    key: 'department',
    controlType: 'text',
  },
  {
    label: 'position',
    key: 'position',
  },
  {
    label: 'user_type',
    key: 'type',
  },
  {
    label: 'mobile',
    key: 'mobile',
  },
  {
    label: 'user_status',
    key: 'is_valid',
  },
  {
    label: 'token',
    key: 'token',
  },
  {
    label: 'email',
    key: 'email',
  },
  {
    label: 'created_at',
    key: 'created_at',
  },
  {
    label: 'landline_phone',
    key: 'landline_phone',
  },
  {
    label: 'extra_info',
    key: 'extra_info',
  },
  {
    label: 'office_address',
    key: 'office_address',
  },
];

export const ACCOUNT_MODELS: FormBase<any>[] = [
  {
    label: 'account',
    key: 'username',
  },
  {
    label: 'enterprise_name',
    key: 'company',
    controlType: 'text',
    pattern: ACCOUNT_ENTERPRISE_NAME_PATTERN.pattern,
    message: ACCOUNT_ENTERPRISE_NAME_PATTERN.tip,
  },
  {
    label: 'password',
    key: 'password',
    value: '******',
  },
  {
    label: 'mobile',
    key: 'mobile',
    pattern: ACCOUNT_MOBILE_PATTERN.pattern,
    controlType: 'text',
    required: true,
    message: ACCOUNT_MOBILE_PATTERN.tip,
  },
  {
    label: 'email',
    key: 'email',
    pattern: ACCOUNT_EMAIL_PATTERN.pattern,
    controlType: 'text',
    required: true,
    message: ACCOUNT_EMAIL_PATTERN.tip,
  },
];

export const USER_MODELS: FormBase<any>[] = [
  {
    label: 'username',
    key: 'username',
  },
  {
    label: 'alias_name',
    key: 'realname',
    controlType: 'text',
  },
  {
    label: 'password',
    key: 'password',
    value: '******',
  },
  {
    label: 'department',
    key: 'department',
    controlType: 'text',
  },
  {
    label: 'position',
    key: 'position',
    controlType: 'text',
  },
  {
    label: 'mobile',
    key: 'mobile',
    pattern: ACCOUNT_MOBILE_PATTERN.pattern,
    controlType: 'text',
    message: ACCOUNT_MOBILE_PATTERN.tip,
  },
  {
    label: 'email',
    key: 'email',
    pattern: ACCOUNT_EMAIL_PATTERN.pattern,
    controlType: 'text',
    message: ACCOUNT_EMAIL_PATTERN.tip,
  },
  {
    label: 'landline_phone',
    key: 'landline_phone',
    controlType: 'text',
  },
  {
    label: 'office_address',
    key: 'office_address',
    controlType: 'text',
  },
  {
    label: 'extra_info',
    key: 'extra_info',
    controlType: 'text',
  },
];
