import { FormControl, FormGroup, Validators } from '@angular/forms';

import { FormBase } from './form-base';

export const getFormModel = function getFormModel(
  models: FormBase<any>[],
  data: object = {},
) {
  return models.map((item: FormBase<any>) => {
    if (data && data[item.key]) {
      item.value = data[item.key];
    }
    return new FormBase(item);
  });
};

export const toFormGroup = function toFormGroup(models: FormBase<any>[] = []) {
  const group: any = {};
  models.forEach(model => {
    group[model.key] = model.required
      ? new FormControl(model.value || '', Validators.required)
      : new FormControl(model.value || '');
  });

  return new FormGroup(group);
};

export const getUserType = function getUserType(type: string) {
  return type === 'organizations.LDAPAccount'
    ? 'synced_by_ldap'
    : type === 'organizations.TPAccount'
    ? 'create_by_thrid_party'
    : 'created_on_platform';
};

export const filterToken = function filterToken(tokenMap: any) {
  return Object.entries(tokenMap).reduce((accu, [key, value]) => {
    if (value) {
      accu.push({ name: key, value });
    }
    return accu;
  }, []);
};
