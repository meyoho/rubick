import { LayoutModule as AuiLayoutModule } from '@alauda/ui';
import { NgModule } from '@angular/core';
import { AccountSharedModule } from 'app/features-shared/account/module';
import { SharedLayoutModule } from 'app/shared/layout/module';
import { SharedModule } from 'app/shared/shared.module';

import { AvatarDialogComponent } from './avatar-uploader/avatar-dialog.component';
import { AvatarUploaderComponent } from './avatar-uploader/avatar-uploader.component';
import { UserCenterComponent } from './component';
import { UserCenterDetailComponent } from './detail/component';
import { AccountInfoOsChinaLinkComponent } from './os-china/account-info-os-china-link.component';
import { RoleCardComponent } from './roles-card/component';
import { UserCenterRoutingModule } from './routing.module';
import { UserUpdateFormComponent } from './update-form/component';
import { UserCenterUpdateComponent } from './update/component';
import { UserInfosComponent } from './user-infos/component';

const COMPONENTS = [
  UserCenterComponent,
  UserCenterDetailComponent,
  UserCenterUpdateComponent,
  AvatarDialogComponent,
  AvatarUploaderComponent,
  UserUpdateFormComponent,
  AccountInfoOsChinaLinkComponent,
  RoleCardComponent,
  UserInfosComponent,
];

const ENTRY_COMPONENTS = [
  AvatarDialogComponent,
  AvatarUploaderComponent,
  UserUpdateFormComponent,
  AccountInfoOsChinaLinkComponent,
];

@NgModule({
  imports: [
    AuiLayoutModule,
    SharedModule,
    SharedLayoutModule,
    AccountSharedModule,
    UserCenterRoutingModule,
  ],
  declarations: COMPONENTS,
  entryComponents: ENTRY_COMPONENTS,
})
export class UserCenterModule {}
