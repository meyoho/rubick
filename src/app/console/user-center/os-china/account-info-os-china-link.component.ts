import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { NotificationService } from '@alauda/ui';
import { Component, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AccountService } from 'app/services/api/account.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './account-info-os-china-link.component.html',
})
export class AccountInfoOsChinaLinkComponent {
  @ViewChild(NgForm)
  ngForm: NgForm;

  infoName = '';
  infoPass = '';

  submitting: boolean;

  constructor(
    @Inject(DIALOG_DATA)
    private data: {
      codeClientName: string;
      beforeLink: () => void;
    },
    private dialogRef: DialogRef,
    private accountService: AccountService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private errorToast: ErrorsToastService,
  ) {}

  async confirm() {
    if (this.ngForm.invalid) {
      return;
    }
    this.data.beforeLink();
    this.submitting = true;
    try {
      await this.accountService.linkPrivateBuildCodeClient({
        codeClientName: this.data.codeClientName,
        oschinaUserEmail: this.infoName,
        oschinaUserPwd: this.infoPass,
      });

      this.dialogRef.close(true);

      this.auiNotificationService.success(
        this.translate.get('oschina_link_success'),
      );
    } catch (e) {
      this.errorToast.error(e);
    }
    this.submitting = false;
  }

  cancel() {
    this.dialogRef.close();
  }
}
