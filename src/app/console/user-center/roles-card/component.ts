import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { Component, Input, OnInit } from '@angular/core';

import { get } from 'lodash-es';
import { first } from 'rxjs/operators';

import { RolesAddComponent } from 'app/features-shared/account/roles-add/account-roles-add-dialog.component';
import { RcAccount } from 'app/services/api/account.service';
import { OrgService } from 'app/services/api/org.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { Role, RoleService } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { isUserView } from 'app/utils/page';
@Component({
  selector: 'rc-roles-card',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class RoleCardComponent implements OnInit {
  @Input()
  user: RcAccount;
  operationViewEnabled: boolean;
  roles: Role[];
  initialized: boolean;
  roleAssignEnabled = false;

  get isUserView() {
    return isUserView();
  }

  constructor(
    private orgService: OrgService,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private roleService: RoleService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
  ) {}

  async ngOnInit() {
    this.initialized = false;
    this.roleUtil
      .resourceTypeSupportPermissions('operation_view', {}, 'get')
      .then(res => (this.operationViewEnabled = res));

    try {
      const hasAssignPermission = await this.roleUtil.resourceTypeSupportPermissions(
        'role',
        {},
        'assign',
      );
      this.roleAssignEnabled =
        hasAssignPermission &&
        (this.user.type !== 'organizations.LDAPAccount' || this.user.is_valid);
      this.roles = (await this.orgService.getAccountRoles(this.user.username))[
        'result'
      ];
    } catch (err) {
      this.roles = [];
      this.errorsToastService.error(err);
    }

    this.initialized = true;
  }

  async updateRoleList() {
    this.roles = (await this.orgService.getAccountRoles(this.user.username))[
      'result'
    ];
  }

  async addRoles() {
    const dialogRef = this.dialogService.open(RolesAddComponent, {
      fitViewport: true,
      size: DialogSize.Big,
      data: {
        username: this.user.username,
      },
    });

    dialogRef
      .afterClosed()
      .pipe(first())
      .subscribe(() => {
        this.updateRoleList();
      });
  }

  async deleteRole(role: any) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete_role'),
        content: this.translateService.get('delete_role_confirm', {
          name: role.role_name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      await this.roleService.deleteRoleUsers(role.role_name, [
        {
          user: this.user.username,
        },
      ]);
      this.updateRoleList();
      this.auiNotificationService.success(
        this.translateService.get('delete_role_success', {
          roleName: role.role_name,
        }),
      );
    } catch (e) {
      // TODO: refactor
      const error = get(e, 'errors[0]');
      if (error && error.code === 'role_is_parent') {
        this.auiNotificationService.error(
          this.translateService.get('role_is_parent', {
            roles: error.message.split(':')[1],
          }),
        );
      } else {
        this.errorsToastService.error(e);
      }
    }
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'role', action);
  }
}
