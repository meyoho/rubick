import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { UserCenterComponent } from './component';
import { UserCenterDetailComponent } from './detail/component';
import { UserCenterUpdateComponent } from './update/component';

const routes: Routes = [
  {
    path: '',
    component: UserCenterComponent,
    children: [
      {
        path: 'detail',
        component: UserCenterDetailComponent,
      },
      {
        path: 'update',
        component: UserCenterUpdateComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UserCenterRoutingModule {}
