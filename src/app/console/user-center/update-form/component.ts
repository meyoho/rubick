import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { CodeClient } from 'app/services/api/account.service';

import { FormBase } from '../form-base';
import { toFormGroup } from '../form-utils';

@Component({
  selector: 'rc-user-update-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserUpdateFormComponent implements OnInit {
  @Input()
  models: FormBase<any>[];

  @Input()
  clients: CodeClient[] = [];

  @Output()
  submit = new EventEmitter();

  @Output()
  cancel = new EventEmitter();

  form: FormGroup;

  constructor() {}

  async ngOnInit() {
    this.form = toFormGroup(this.models);
  }

  updateHandler() {
    this.submit.emit(this.form.value);
  }

  cancelHandler() {
    this.cancel.emit();
  }
}
