import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { pick } from 'lodash-es';
import { RcProfile } from 'src/landing/app/types';

import { AccountService, CodeClient } from 'app/services/api/account.service';
import { OrgService, User } from 'app/services/api/org.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import { getFormModel } from '..//form-utils';
import { FormBase } from '../form-base';
import { ACCOUNT_MODELS, USER_MODELS } from '../form-models';

@Component({
  templateUrl: './template.html',
})
export class UserCenterUpdateComponent implements OnInit {
  initialized: boolean;
  models: FormBase<any>[] = [];
  codeClients: CodeClient[] = [];
  isRootView: boolean;
  username: string;

  get isSubAccount(): boolean {
    return this.accountService.isSubAccount();
  }

  constructor(
    @Inject(ACCOUNT) public account: Account,
    private activatedRoute: ActivatedRoute,
    private orgService: OrgService,
    private accountService: AccountService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.activatedRoute.queryParams.subscribe(async params => {
      this.initialized = false;
      this.username = params.username;
      let profile, user, clients, models;
      const promises: Promise<any>[] = [this.accountService.getAuthProfile()];
      if (!params.username) {
        this.isRootView = true;
        models = ACCOUNT_MODELS;

        promises.push(this.accountService.getPrivateBuildCodeClients());
        [profile, clients] = await Promise.all(promises);

        this.codeClients = clients;
      } else {
        this.isRootView = false;
        models = USER_MODELS;
        promises.push(this.orgService.getUser(params.username));
        [profile, user] = await Promise.all(promises);
      }

      // override the default value of models
      const data = Object.assign({}, profile, user, {
        account: profile.username,
      });
      this.models = getFormModel(models, data);
      this.initialized = true;
    });
  }

  async getCodeClients() {
    this.codeClients = await this.accountService.getPrivateBuildCodeClients();
  }

  submit(formValue: any) {
    if (this.isRootView) {
      this.updateAccount(formValue);
    } else {
      this.updateUser(formValue);
    }
  }

  async updateUser(formValue: object) {
    const params = pick(formValue, [
      'username',
      'realname',
      'department',
      'position',
      'mobile',
      'email',
      'office_address',
      'landline_phone',
      'extra_info',
    ]) as User;

    try {
      await this.orgService.updateUser(params);
      window.history.back();
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  async updateAccount(formValue: object) {
    const params = pick(formValue, ['company', 'email', 'mobile']) as RcProfile;

    try {
      await this.accountService.updateProfile(params);
      this.toDetailPage();
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  cancel() {
    this.toDetailPage();
  }

  toDetailPage() {
    this.router.navigateByUrl(
      `/console/user-center/detail?username=${this.username}`,
    );
  }
}
