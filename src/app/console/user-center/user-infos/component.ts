import { DialogService, NotificationService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';

import { AccountUpdatePwdComponent } from 'app/features-shared/account/update-password/component';
import { AccountService, CodeClient } from 'app/services/api/account.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { AvatarImgData } from '../avatar-uploader/avatar-uploader.component';
import { FormBase } from '../form-base';
import { AccountInfoOsChinaLinkComponent } from '../os-china/account-info-os-china-link.component';

interface TokenItem {
  name: string;
  value: string;
}
@Component({
  selector: 'rc-user-infos',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class UserInfosComponent {
  @ViewChild('tokenDialogTemplate')
  tokenDialogRef: TemplateRef<any>;
  @Input()
  models: FormBase<any>[] = [];
  @Input()
  username: string;
  @Input()
  codeClients: CodeClient[];

  @Output()
  update = new EventEmitter();

  currentToken: TokenItem;
  tokenCopied: boolean;
  isRootView = false;
  linking: boolean;

  get helpText1() {
    switch (this.currentToken.name) {
      case 'kubernetes_id_token':
        return this.translateService.get('token_help_text1', { type: 'K8S' });
      case 'openshift_token':
        return this.translateService.get('token_help_text1', {
          type: 'openshift',
        });
      default:
        return this.translateService.get('token_help_text1', {
          type: 'alauda',
        });
    }
  }

  constructor(
    @Inject(ACCOUNT) private account: Account,
    private accountService: AccountService,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
  ) {}

  async avatarChange(data: AvatarImgData) {
    try {
      await this.accountService.updateNamespaceLogo(
        this.account.namespace,
        data.files[0],
      );
      this.auiNotificationService.success(
        this.translateService.get('org_logo_update_success'),
      );
      this.accountService.avatarPath$$.next(data.url);
    } catch (e) {}
  }

  onTokenItemClick(token: TokenItem) {
    this.tokenCopied = false;
    this.currentToken = token;
    this.dialogService.open(this.tokenDialogRef);
  }

  copyText(input: HTMLInputElement) {
    input.select();
    document.execCommand('Copy');
    setTimeout(() => (this.tokenCopied = true));
  }

  resetPassword() {
    this.dialogService.open(AccountUpdatePwdComponent, {
      data: {
        username: this.username,
      },
    });
  }

  async getCodeClients() {
    this.codeClients = await this.accountService.getPrivateBuildCodeClients();
  }

  oschinaLinkClient(codeClient: CodeClient) {
    const dialogRef = this.dialogService.open(AccountInfoOsChinaLinkComponent, {
      data: {
        codeClientName: codeClient.name,
        beforeLink: () => (codeClient.is_authed = 'linking'),
      },
    });

    dialogRef.afterClosed().subscribe(async (isConfirm: boolean) => {
      if (isConfirm) {
        this.getCodeClients();
      } else {
        codeClient.is_authed = false;
      }
    });
  }

  async linkClient(codeClient: CodeClient) {
    if (codeClient.name === 'OSCHINA') {
      return this.oschinaLinkClient(codeClient);
    }
    this.linking = true;
    try {
      location.href = await this.accountService.getPrivateBuildCodeClientAuthUrl(
        {
          codeClientName: codeClient.name,
        },
      );
    } catch (e) {
      this.linking = false;
    }
  }

  async unlinkClient(codeClient: CodeClient) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get(
          'unlink_oauth_code_client_confirm_title',
        ),
        content: this.translateService.get(
          'unlink_oauth_code_client_confirm_content',
          {
            clientName: codeClient.name,
          },
        ),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    codeClient.is_authed = 'unlinking';
    try {
      await this.accountService.unlinkPrivateBuildCodeClient({
        codeClientName: codeClient.name,
      });
    } catch (e) {
      codeClient.is_authed = true;
      return;
    }

    await this.getCodeClients();
  }
}
