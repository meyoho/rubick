import { Inject, Injectable } from '@angular/core';
import { PreloadingStrategy, Route } from '@angular/router';

import { get } from 'lodash-es';
import { Observable, of } from 'rxjs';
import { catchError, delay, switchMap } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';

@Injectable()
export class DelayPreloadingStrategy implements PreloadingStrategy {
  constructor(@Inject(ENVIRONMENTS) private envs: Environments) {}

  private loadCounter = 2; // starting from 2 to unblock other xhr.

  preload(route: Route, fn: () => Observable<any>): Observable<any> {
    // If the route is at root, skip preload:
    if (get(route, 'data.preloadDisabled') || this.envs.debug) {
      return of(null);
    }
    return of(null).pipe(
      delay(this.loadCounter++ * 2000),
      switchMap(() => fn()),
      catchError(() => of(null)),
    );
  }
}
