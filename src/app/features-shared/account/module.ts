import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { RolesAddComponent } from './roles-add/account-roles-add-dialog.component';
import { UpdateCompanyComponent } from './update-company/component';
import { AccountUpdatePwdComponent } from './update-password/component';
@NgModule({
  imports: [SharedModule],
  declarations: [
    RolesAddComponent,
    AccountUpdatePwdComponent,
    UpdateCompanyComponent,
  ],
  entryComponents: [
    RolesAddComponent,
    AccountUpdatePwdComponent,
    UpdateCompanyComponent,
  ],
})
export class AccountSharedModule {}
