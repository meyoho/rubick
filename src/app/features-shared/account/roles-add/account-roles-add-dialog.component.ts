import { DIALOG_DATA, DialogRef, PageEvent } from '@alauda/ui';
import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { clone } from 'lodash-es';
import {
  BehaviorSubject,
  combineLatest,
  from,
  Observable,
  Subject,
} from 'rxjs';
import {
  catchError,
  debounceTime,
  pluck,
  publishReplay,
  refCount,
  scan,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { OrgService } from 'app/services/api/org.service';
import { Project } from 'app/services/api/project.service';
import { Role, RoleService, RoleUser } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import * as fromStore from 'app/store';
import { Account, Weblabs } from 'app/typings';
import { ResourceList } from 'app_user/core/types';

@Component({
  selector: 'rc-app-roles-add',
  templateUrl: './account-roles-add-dialog.component.html',
  styleUrls: ['./account-roles-add-dialog.component.scss'],
})
export class RolesAddComponent implements OnInit {
  initialized: boolean;
  username: string;
  accountRoles$: Observable<Role[]>;
  roles$: Observable<Role[]>;
  search$: Subject<string> = new Subject();
  pagination$: Observable<PageEvent>;
  paginationEvent$ = new BehaviorSubject<Partial<PageEvent>>({});
  projects$: Observable<Project[]>;
  list$: Observable<ResourceList>;
  ALL_PROJECTS = 'all_project';
  currentProjectName = this.ALL_PROJECTS;
  currentProjectName$: Subject<string> = new Subject();
  checkedMap: { [name: string]: boolean } = {};
  disabledMap: { [name: string]: boolean } = {};

  constructor(
    private cdr: ChangeDetectorRef,
    private dialogRef: DialogRef,
    private orgService: OrgService,
    private errorsToastService: ErrorsToastService,
    private roleService: RoleService,
    private store: Store<fromStore.AppState>,
    @Inject(ACCOUNT) public account: Account,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(DIALOG_DATA)
    public dialogData: {
      username: string;
    },
  ) {}

  get disabled() {
    return !this.initialized || !Object.keys(this.checkedMap).length;
  }

  async ngOnInit() {
    this.username = this.dialogData.username || '';
    this.store.dispatch(new fromStore.LoadProjects());
    this.projects$ = this.store.select(fromStore.getAllProjects).pipe(
      publishReplay(1),
      refCount(),
    );

    try {
      const { result } = (await this.orgService.getAccountRoles(
        this.username,
      )) as { result: RoleUser[] };
      const map = result.reduce((acc, curr) => {
        acc[curr.role_name] = true;
        return acc;
      }, {});
      this.checkedMap = clone(map);
      this.disabledMap = clone(map);
    } catch (err) {}

    this.pagination$ = this.paginationEvent$.pipe(
      scan((prev, changes) => ({ ...prev, ...changes }), {
        pageSize: 10,
        pageIndex: 0,
        previousPageIndex: 0,
        length: 0,
      }),
      publishReplay(1),
      refCount(),
    );

    this.list$ = combineLatest(
      this.pagination$,
      this.search$.pipe(startWith('')),
      this.currentProjectName$.pipe(startWith(this.ALL_PROJECTS)),
    ).pipe(
      debounceTime(100),
      tap(() => {
        this.initialized = false;
      }),
      switchMap(([pagination, key, project]) => {
        return this.fetchRoles(pagination, key, project).pipe(
          catchError(() => []),
        );
      }),
      tap(() => {
        this.initialized = true;
      }),
      publishReplay(1),
      refCount(),
    );

    this.roles$ = this.list$.pipe(pluck('results'));
  }

  onProjectChange(project: string) {
    this.currentProjectName$.next(project);
  }

  pageChange(page: PageEvent) {
    this.paginationEvent$.next(page);
  }

  search(key: string) {
    this.paginationEvent$.next({ pageIndex: 0 });
    this.search$.next(key);
  }

  onCheckedChange(checked: boolean, role: Role) {
    if (checked) {
      this.checkedMap[role.name] = true;
    } else {
      delete this.checkedMap[role.name];
    }
    this.cdr.markForCheck();
  }

  fetchRoles(pagination: PageEvent, key: string, projectName?: string) {
    return from(
      this.roleService.getRoleList({
        search: key,
        project_name: projectName === this.ALL_PROJECTS ? '' : projectName,
        page: pagination.pageIndex + 1,
        page_size: pagination.pageSize,
      }),
    );
  }

  async confirm() {
    try {
      const roleNames = Object.keys(this.checkedMap).reduce((acc, name) => {
        if (!this.disabledMap[name]) {
          acc.push({ name });
        }
        return acc;
      }, []);
      await this.orgService.addAccountRoles(this.username, roleNames);
      this.dialogRef.close(true);
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  cancel() {
    this.dialogRef.close(false);
  }
}
