import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { OrgService } from 'app/services/api/org.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, Weblabs } from 'app/typings';

@Component({
  selector: 'rc-app-update-company',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class UpdateCompanyComponent implements OnInit {
  formerCompanyName: string;
  orgName: string;
  submitting: boolean;
  companyName: string;

  @ViewChild('updateCompanyForm')
  updateCompanyForm: NgForm;

  constructor(
    private orgService: OrgService,
    private auiMessageService: MessageService,
    private errorsToastService: ErrorsToastService,
    private translate: TranslateService,
    private dialogRef: DialogRef<any>,
    @Inject(DIALOG_DATA)
    public data: {
      formerCompanyName: string;
      orgName: string;
    },
    @Inject(ACCOUNT) public account: Account,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  ngOnInit() {
    this.formerCompanyName = this.data.formerCompanyName;
    this.orgName = this.data.orgName;
  }

  confirm() {
    this.updateCompanyForm.onSubmit(null);
    if (this.updateCompanyForm.invalid) {
      return;
    }
    this._updateOrg();
  }

  cancel() {
    this.dialogRef.close();
  }

  _updateOrg() {
    this.submitting = true;
    this.orgService
      .updateOrg(this.orgName, this.companyName)
      .then(data => {
        this.auiMessageService.success({
          content: this.translate.get('update_company_success'),
        });
        this.dialogRef.close(data);
      })
      .catch(e => {
        this.errorsToastService.error(e);
      })
      .then(() => {
        this.submitting = false;
      });
  }
}
