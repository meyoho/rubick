import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { AccountService } from 'app/services/api/account.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import {
  PatternHelperService,
  PwdPattern,
} from 'app/services/pattern-helper.service';
import { ACCOUNT, ENVIRONMENTS, WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, Environments, Weblabs } from 'app/typings';
import { RBAC_PASSWORD_PATTERN } from 'app/utils/patterns';

@Component({
  selector: 'rc-app-account-update-pwd',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class AccountUpdatePwdComponent implements OnInit {
  isSubAccount: boolean;
  passwordNotSame: boolean;
  passwordPatternObj: PwdPattern;
  getPasswordStrength: Function;
  currentUserName: String;
  confirm_new_password: string;
  new_password: string;
  submitting: boolean;
  old_password: string;
  changePasswordErrorMapper: any;
  username: string;
  accountSubType: string;
  pwdVisible = false;
  passwordPattern = RBAC_PASSWORD_PATTERN;
  @ViewChild('updatePasswordForm')
  form: NgForm;

  constructor(
    private translate: TranslateService,
    private accountService: AccountService,
    private auiNotificationService: NotificationService,
    private patternHelperService: PatternHelperService,
    private errorHandle: ErrorsToastService,
    private dialogRef: DialogRef,
    @Inject(DIALOG_DATA)
    public dialogData: {
      username: string;
    },
    @Inject(ACCOUNT) public account: Account,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  async ngOnInit() {
    this.isSubAccount = this.accountService.isSubAccount();
    this.passwordNotSame = false;
    this.passwordPatternObj = this.patternHelperService.getPasswordPattern();
    this.getPasswordStrength = this.patternHelperService.getPasswordStrength;
    this.changePasswordErrorMapper = this.patternHelperService.getChangePasswordErrorMapper();
    this.currentUserName = this.account.username;
    this.username = this.dialogData.username || '';
  }

  checkPasswordStrength = (value: string) =>
    !value ||
    this.getPasswordStrength(value) >= this.passwordPatternObj.strength;

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const params: any = {
      username: this.username || undefined,
      password: this.new_password,
    };
    if ((!this.isSubAccount && !this.username) || this.isSubAccount) {
      params.old_password = this.old_password;
    }
    this.submitting = true;

    try {
      if (this.username) {
        await this.accountService.updateSubAccountPassword(params);
      } else {
        await this.accountService.updateProfile(params as any);
      }
      this.auiNotificationService.success({
        content: this.translate.get('update_password_success'),
      });
      this.dialogRef.close();
    } catch (e) {
      this.errorHandle.error(e);
    } finally {
      this.submitting = false;
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  showOldPasswordField() {
    return this.isSubAccount
      ? this.currentUserName === this.username
      : !this.username;
  }

  get cPwdPattern() {
    return this.new_password === this.confirm_new_password ? /\.*/ : /\.{,0}/;
  }
}
