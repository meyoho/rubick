import { NotificationService } from '@alauda/ui';
import { ChangeDetectorRef, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { FormBuilder, NgForm } from '@angular/forms';

import * as Highcharts from 'highcharts';
import { cloneDeep } from 'lodash-es';
import moment from 'moment';
import { Subject } from 'rxjs';

import {
  MetricNumericOptions,
  MetricPercentOptions,
} from 'app/features-shared/app/utils/monitor';
import {
  Alarm,
  AlarmMetric,
  AlarmService,
} from 'app/services/api/alarm.service';
import {
  IndicatorType,
  Metric,
  MetricLabels,
  MetricService,
} from 'app/services/api/metric.service';
import { TranslateService } from 'app/translate/translate.service';
import { formatNumber } from 'app/utils/format-number';
import { getUnitType, parseChartOptions } from 'app2/features/alarm/alarm.util';
require('highcharts/modules/no-data-to-display')(Highcharts);

interface Notifications {
  uuid: string;
  name: string;
}

interface AlarmActions {
  action_type: string;
  notifications?: Notifications[];
}

export abstract class BaseAlarmFormComponent implements OnInit, OnDestroy {
  @ViewChild('alarmForm')
  alarmForm: NgForm;

  private default_labels: Array<string>;
  private end_time: number;
  private labels_preset = {};
  private step: number;

  chartLoading = false;
  cluster_name: string;
  data: Alarm;
  formSubmitText = 'create';
  initialized = false;
  metric_type: IndicatorType[];
  name: string;
  resource_name: string;
  submitting = false;
  payloadMetricLabels: MetricLabels[];
  unit_name = '';
  Highcharts = Highcharts;
  chartPercentOptions: Highcharts.Options = cloneDeep(MetricPercentOptions);
  chartNumericOptions: Highcharts.Options = cloneDeep(MetricNumericOptions);
  actions: AlarmActions[] = [];
  form = this.fb.group({
    alarm_metric: this.fb.control({}),
    info: this.fb.control({}),
  });

  onDestroy$ = new Subject<void>();

  constructor(
    protected cdr: ChangeDetectorRef,
    protected fb: FormBuilder,
    protected metricService: MetricService,
    protected translate: TranslateService,
    protected auiNotificationService: NotificationService,
    protected alarmService: AlarmService,
  ) {}

  ngOnInit() {
    parseChartOptions(this.chartPercentOptions);
    parseChartOptions(this.chartNumericOptions);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  protected async initViewModel() {
    this.default_labels = await this.alarmService.getAlertConfig();
    if (this.data) {
      let threshold = 0;
      if (this.data.unit === '%') {
        threshold = +formatNumber(this.data.threshold * 100, [1, 0, 4]);
      } else {
        threshold = this.data.threshold;
      }
      if (this.data.labels) {
        this.data.level = this.data.labels['severity'];
        if (this.default_labels) {
          this.default_labels.forEach(el => {
            if (this.data.labels[el] !== undefined) {
              this.labels_preset[el] = this.data.labels[el];
              delete this.data.labels[el];
            }
          });
        }
      }
      if (this.data.metric_name === 'workload.log.keyword.count') {
        this.data.unit = 'items';
      }
      this.form.patchValue({
        alarm_metric: {
          metric: this.data.metric,
          metric_name: this.data.metric_name,
          expr: this.data.expr,
          unit: this.data.unit,
          type: this.data.type,
          resource_name: this.data.display_name,
          query: this.data.query,
        },
        info: {
          name: this.data.name,
          level: this.data.level,
          compare: this.data.compare,
          threshold,
          wait: this.data.wait,
          labels: this.data.labels,
          annotations: this.data.annotations,
        },
      });
      if (this.data.notifications) {
        this.actions.push({
          action_type: 'notification',
          notifications: this.data.notifications,
        });
      }
      if (this.data.scale_up) {
        this.actions.push({
          action_type: 'scale_up',
        });
      }
      if (this.data.scale_down) {
        this.actions.push({
          action_type: 'scale_down',
        });
      }
    }
  }

  protected generatePayload() {
    const body = {
      ...this.form.get('alarm_metric').value,
      ...this.form.get('info').value,
      threshold: +this.form.get('info').value.threshold,
      labels: {
        ...this.form.get('info').value.labels,
        ...this.labels_preset,
        severity: this.form.get('info').value.level,
      },
    };
    if (body.metric.queries[0].labels.length < 5) {
      body.metric.queries[0].labels = body.metric.queries[0].labels.concat(
        this.payloadMetricLabels,
      );
    }
    const notificationActions = this.actions.filter(el => {
      return el.action_type === 'notification';
    });
    if (notificationActions.length > 0) {
      let notifications: Array<Notifications> = [];
      notificationActions.forEach(el => {
        if (el.notifications) {
          notifications = notifications.concat(el.notifications);
        }
      });
      body['notifications'] = notifications.filter(
        (notification, index, self) =>
          self.findIndex(t => t.uuid === notification.uuid) === index,
      );
    }
    return body;
  }

  protected loadChart(
    name: string,
    cluster: string,
    alarm_metric: AlarmMetric,
  ) {
    const current_time = parseInt((moment().valueOf() / 1000).toFixed(0), 10);
    const step = alarm_metric.metric.queries[0].range;
    if (step !== 0 && step !== undefined) {
      this.step = step;
    } else {
      this.step = 300;
    }
    this.end_time = current_time;
    this.chartLoading = true;
    this.chartPercentOptions.series = [];
    this.chartNumericOptions.series = [];
    this.cdr.markForCheck();
    this.metricService
      .queryMetrics(cluster, {
        start: current_time - this.step * 29,
        end: current_time,
        step: this.step,
        queries: alarm_metric.metric.queries,
      })
      .then(
        result => {
          if (result) {
            if (this.percentFlag()) {
              this.chartPercentOptions['series'] = this.parseMetricsResponse(
                name,
                result,
                100,
              );
              this.chartNumericOptions.series = [];
            } else {
              this.chartNumericOptions['series'] = this.parseMetricsResponse(
                name,
                result,
              );
              this.chartPercentOptions.series = [];
            }
            this.chartLoading = false;
            this.cdr.markForCheck();
          }
        },
        error => {
          this.chartLoading = false;
          this.cdr.markForCheck();
          throw error;
        },
      );
    return;
  }

  onAlarmActionAdd(actions: AlarmActions[]) {
    this.actions = actions;
  }

  protected getUnitType(metric: string) {
    return getUnitType(metric, this.metric_type);
  }

  protected percentFlag() {
    if (this.form.get('alarm_metric').value.unit === '%') {
      return true;
    } else if (
      this.getUnitType(this.form.get('alarm_metric').value.metric_name) === '%'
    ) {
      return true;
    }
    return false;
  }

  protected checkAlarmMetric(alarm_metric: AlarmMetric) {
    if (
      !alarm_metric.metric_name &&
      (alarm_metric.expr === undefined || alarm_metric.expr === '')
    ) {
      return true;
    } else if (
      alarm_metric.metric_name === 'workload.log.keyword.count' &&
      (alarm_metric.query === undefined || alarm_metric.query === '')
    ) {
      return true;
    }
    return false;
  }

  private parseMetricsResponse(
    name: string,
    metric: Metric[],
    multiplier?: number,
  ) {
    return metric.map(el => {
      if (el.values.length < 30) {
        el.values = this.metricService.fillUpResult(
          el.values,
          this.end_time,
          this.step,
        );
      }
      if (this.form.get('alarm_metric').value.expr) {
        if (Object.entries(el.metric).length) {
          name = Object.entries(el.metric)
            .map(el => `${el[0]}: ${el[1]}`)
            .join('<br>');
        }
      }
      if (el.metric.instance) {
        name = el.metric.instance;
      }
      return {
        name,
        data: el.values.map((value: Array<number | string>) => {
          let y = null;
          if (value[1] !== '+Inf' && value[1] !== '') {
            y = Number(value[1]);
            if (multiplier) {
              y = y * 100;
            }
          }
          return {
            x: Number(value[0]) * 1000,
            y,
          };
        }),
      };
    });
  }
}
