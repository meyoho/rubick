import { MessageService } from '@alauda/ui';
import { OnInit } from '@angular/core';

import moment from 'moment';

import { TIME_STAMP_OPTIONS } from 'app/features-shared/app/utils/monitor';
import { LogService } from 'app/services/api/log.service';
import { TranslateService } from 'app/translate/translate.service';

const QUERY_TIME_STAMP_OPTIONS = TIME_STAMP_OPTIONS.concat([
  {
    type: 'custom_time_range',
    offset: 0,
  },
]);

/**
 * 日期选择器基础类，用于监控告警图表的日期选择
 */
export abstract class BaseTimeSelectComponent implements OnInit {
  abstract loadCharts(): void;
  chartLoading = true;
  timeStampOptions = QUERY_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type,
    };
  });
  selectedTimeStampOption = this.timeStampOptions[0].type;
  current_time = moment();
  start_time = this.current_time
    .clone()
    .startOf('day')
    .subtract(6, 'days');
  step: number;
  end_time: number;
  queryDates = {
    start_time: this.start_time.valueOf(),
    end_time: this.current_time.valueOf(),
  };
  queryDatesShown = {
    start_time: this.logService.dateNumToStr(this.queryDates.start_time),
    end_time: this.logService.dateNumToStr(this.queryDates.end_time),
  };
  dateTimeOptions = {
    maxDate: this.current_time
      .clone()
      .endOf('day')
      .valueOf(),
    enableTime: true,
    enableSeconds: true,
    time_24hr: true,
  };

  constructor(
    private logService: LogService,
    private auiMessageService: MessageService,
    protected translateService: TranslateService,
  ) {}

  ngOnInit() {}

  protected resetTimeRange() {
    this.queryDates = this.logService.getTimeRangeByRangeType(
      this.selectedTimeStampOption,
    );
    this.fillCalendar(this.queryDates.start_time, this.queryDates.end_time);
  }

  fillCalendar(start_time: number, end_time: number) {
    this.queryDatesShown.start_time = this.logService.dateNumToStr(start_time);
    this.queryDatesShown.end_time = this.logService.dateNumToStr(end_time);
  }

  onStartTimeSelect(event: any) {
    this.queryDates.start_time = this.logService.dateStrToNum(event);
  }

  onEndTimeSelect(event: any) {
    this.queryDates.end_time = this.logService.dateStrToNum(event);
  }

  checkTimeRange() {
    if (
      this.floorTime(this.queryDates.end_time) <=
      this.floorTime(this.queryDates.start_time)
    ) {
      this.auiMessageService.warning({
        content: this.translateService.get('metric_timerange_invalid'),
      });
      return false;
    }
    return true;
  }

  floorTime(time: number) {
    return Math.floor(time / 1000);
  }

  onTimeStampOptionSelect() {
    if (this.selectedTimeStampOption !== 'custom_time_range') {
      this.resetTimeRange();
    }
    this.onSearch();
  }

  onSearch() {
    if (this.checkTimeRange()) {
      this.chartLoading = true;
      this.loadCharts();
    }
  }
}
