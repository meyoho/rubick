import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { Alarm } from 'app/services/api/alarm.service';
import { IndicatorType } from 'app/services/api/metric.service';
import { getThreshold, getUnit } from 'app2/features/alarm/alarm.util';

@Component({
  selector: 'rc-alarm-basic-info',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmBasicInfoComponent {
  @Input()
  alarm: Alarm;
  @Input()
  metric_type: IndicatorType[];

  constructor() {}

  getUnit(item: Alarm) {
    return getUnit(item, this.metric_type);
  }

  getThreshold(item: Alarm) {
    return getThreshold(item, this.metric_type);
  }
}
