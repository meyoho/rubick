import { NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { cloneDeep } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';

import { LEVELS } from 'app/features-shared/app/utils/monitor';
import {
  AlarmInfoFormModel,
  AlarmService,
} from 'app/services/api/alarm.service';
import { MetricService } from 'app/services/api/metric.service';
import { TranslateService } from 'app/translate/translate.service';
import { formatNumber } from 'app/utils/format-number';
import {
  ALARM_LABEL_KEY_PATTERN,
  ALARM_RESOURCE_PATTERN,
  ALARM_THRESHOLD_PATTERN,
  NATURAL_NUMBER_PATTERN,
} from 'app/utils/patterns';

const ALARM_TIME_STAMP_OPTIONS = [1, 2, 3, 5, 10, 30].map(el => ({
  type: el,
  offset: el * 60,
}));

@Component({
  selector: 'rc-alarm-info-form',
  templateUrl: './template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmInfoFormComponent
  extends BaseResourceFormGroupComponent<AlarmInfoFormModel>
  implements OnInit, OnChanges, OnDestroy {
  @Input()
  alarm_name: string;
  @Input()
  unit_name: string;
  @Input()
  metric_name: string;
  levels = LEVELS;
  timeStampOptions = ALARM_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type + this.translate.get('minutes'),
    };
  });
  compareOptions = ['>', '>=', '==', '<=', '<', '!='];
  resourceNameReg = ALARM_RESOURCE_PATTERN;
  thresholdReg = ALARM_THRESHOLD_PATTERN;
  logThresholdReg = NATURAL_NUMBER_PATTERN;
  thresholdPlaceholder = this.translate.get(this.thresholdReg.tip);
  thresholdTooltipMapper = {
    pattern: this.translate.get(this.thresholdReg.tip),
  };
  labelValidator = {
    key: [Validators.pattern(ALARM_LABEL_KEY_PATTERN.pattern)],
  };
  labelErrorMapper = {
    key: {
      pattern: this.translate.get(ALARM_LABEL_KEY_PATTERN.tip),
    },
  };
  onDestroy$ = new Subject<void>();

  constructor(
    injector: Injector,
    protected metricService: MetricService,
    protected auiNotificationService: NotificationService,
    protected translate: TranslateService,
    protected alarmService: AlarmService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.alarm_name) {
      this.form.get('name').disable();
    }
  }

  ngOnChanges({ metric_name }: SimpleChanges) {
    if (metric_name && metric_name.currentValue) {
      if (this.metric_name === 'workload.log.keyword.count') {
        this.thresholdPlaceholder = this.translate.get(
          this.logThresholdReg.tip,
        );
        this.thresholdTooltipMapper = {
          pattern: this.translate.get(this.logThresholdReg.tip),
        };
        this.form.get('wait').disable();
        this.form
          .get('threshold')
          .setValidators([
            Validators.required,
            Validators.pattern(this.logThresholdReg.pattern),
          ]);
      } else {
        this.thresholdPlaceholder = this.translate.get(this.thresholdReg.tip);
        this.thresholdTooltipMapper = {
          pattern: this.translate.get(this.thresholdReg.tip),
        };
        this.form.get('wait').enable();
        this.form
          .get('threshold')
          .setValidators([
            Validators.required,
            Validators.pattern(this.thresholdReg.pattern),
          ]);
      }
      this.form.get('threshold').updateValueAndValidity();
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createForm() {
    return this.fb.group({
      name: ['', Validators.required],
      level: ['', Validators.required],
      compare: ['', Validators.required],
      threshold: [
        '',
        [Validators.required, Validators.pattern(this.thresholdReg.pattern)],
      ],
      wait: [0, Validators.required],
      labels: [{}],
      annotations: [{}],
    });
  }

  getDefaultFormModel() {
    return {
      name: '',
      level: this.levels[2],
      compare: this.compareOptions[0],
      threshold: '',
      wait: this.timeStampOptions[0].offset,
      labels: {},
      annotations: {},
    };
  }

  adaptFormModel(formModel: AlarmInfoFormModel) {
    const resource = cloneDeep(formModel);
    if (this.unit_name === '%') {
      resource.threshold = String((+resource.threshold * 10000) / 1000000);
    }
    return resource;
  }

  adaptResourceModel(resource: AlarmInfoFormModel) {
    const formModel = cloneDeep(resource);
    if (this.unit_name === '%') {
      formModel.threshold = formatNumber(+formModel.threshold * 100, [1, 0, 4]);
    }
    return formModel;
  }
}
