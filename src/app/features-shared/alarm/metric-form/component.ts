import { NotificationService } from '@alauda/ui';
import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { from, Subject } from 'rxjs';
import {
  catchError,
  debounceTime,
  distinctUntilChanged,
  filter,
  map,
  takeUntil,
} from 'rxjs/operators';

import { AGGREGATORS, UNITS } from 'app/features-shared/app/utils/monitor';
import {
  AlarmMetric,
  AlarmMetricFormModel,
  AlarmService,
} from 'app/services/api/alarm.service';
import {
  IndicatorType,
  MetricQueries,
  MetricService,
} from 'app/services/api/metric.service';
import { TranslateService } from 'app/translate/translate.service';
import { ALARM_METRIC_PATTERN } from 'app/utils/patterns';
import {
  capitalizeFirstLetter,
  getUnitType,
} from 'app2/features/alarm/alarm.util';

const ALARM_TIME_STAMP_OPTIONS = [1, 2, 3, 5, 10, 30].map(el => ({
  type: el,
  offset: el * 60,
}));

@Component({
  selector: 'rc-alarm-metric-form',
  templateUrl: './template.html',
  styles: [
    `
      aui-tags-input {
        width: 100%;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmMetricFormComponent
  extends BaseResourceFormGroupComponent<AlarmMetric, AlarmMetricFormModel>
  implements OnInit, OnDestroy {
  // rc-alarm-metric-form同时用在用户视角和管理视角，formType用来区分视角，两个视角下的表单字段有所不同
  @Input()
  formType: string;
  @Input()
  alarm_name: string;
  @Input()
  cluster_name: string;
  @Input()
  metric_type: Array<IndicatorType>;
  @Input()
  nodes?: Array<any>;
  @Input()
  node_view_premission?: boolean;
  @Input()
  pod_name: string;
  @Input()
  kind: string;
  @Input()
  namespace: string;
  metrics: Array<IndicatorType>;
  timeStampOptions = ALARM_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type + this.translate.get('minutes'),
    };
  });
  onDestroy$ = new Subject<void>();
  units = UNITS;
  aggregators = AGGREGATORS;
  prometheus_metrics: string[];
  prometheus_metrics_cluster: string[];
  function_names: string[];
  prometheus_metrics_suggestion: string[];
  function_names_suggestion: string[];
  prometheus_metric_labels: string[];
  metricReg = ALARM_METRIC_PATTERN;

  expressionChanged$: Subject<string> = new Subject<string>();

  constructor(
    injector: Injector,
    protected metricService: MetricService,
    protected auiNotificationService: NotificationService,
    protected translate: TranslateService,
    protected alarmService: AlarmService,
  ) {
    super(injector);
  }

  async ngOnInit() {
    super.ngOnInit();
    this.expressionChanged$
      .pipe(
        takeUntil(this.onDestroy$),
        debounceTime(300),
        distinctUntilChanged(),
      )
      .subscribe(expression => {
        this.predictExpression(expression);
      });
    if (this.formType === 'admin') {
      this.form
        .get('resource_type')
        .valueChanges.pipe(
          takeUntil(this.onDestroy$),
          filter(value => !!value),
        )
        .subscribe((resource_type: string) => {
          if (resource_type === 'Cluster') {
            this.form.patchValue({
              resource_name: this.cluster_name,
            });
            this.form.get('resource_name').disable();
          } else if (resource_type === 'Node') {
            if (!this.alarm_name) {
              this.form.get('resource_name').enable();
            }
            this.form.patchValue({
              resource_name: this.node_view_premission ? '.*' : '',
            });
          }
          this.filterMetrics(resource_type.toLocaleLowerCase());
          this.cdr.markForCheck();
        });
      this.form
        .get('resource_name')
        .valueChanges.pipe(
          takeUntil(this.onDestroy$),
          filter(value => !!value),
        )
        .subscribe((resource_name: string) => {
          if (this.form.get('resource_type').value === 'Node') {
            this.updatePredictExpression(resource_name);
          } else {
            this.prometheus_metrics = this.prometheus_metrics_cluster;
          }
        });
    } else if (this.formType === 'user') {
      this.metrics = this.metric_type.filter(el => {
        return el.kind === 'workload' && el.type === 'metric';
      });
      this.form.patchValue({
        monitor_metric: this.metrics[0].name,
      });
    }
    this.form
      .get('alarm_type')
      .valueChanges.pipe(
        takeUntil(this.onDestroy$),
        filter(value => !!value),
      )
      .subscribe((alarm_type: string) => {
        if (alarm_type === 'custom') {
          this.form.get('monitor_metric').disable();
          this.form.get('metric_expression').enable();
          this.form.patchValue({
            data_type: false,
          });
        } else {
          this.form.get('monitor_metric').enable();
          this.form.get('metric_expression').disable();
          this.form.patchValue({
            unit: '',
          });
        }
        if (alarm_type === 'log') {
          this.form.get('log_content').enable();
          this.form.patchValue({
            data_type: false,
            unit: 'items',
          });
        } else {
          this.form.get('log_content').disable();
          this.form.patchValue({
            unit: '',
          });
        }
      });
    this.form
      .get('data_type')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((data_type: string) => {
        if (data_type) {
          this.form.get('aggregator').enable();
          this.form.patchValue({
            timeStampOption: this.timeStampOptions[0].offset,
            aggregator: this.aggregators[0].key,
          });
        } else {
          this.form.patchValue({
            timeStampOption: 0,
            aggregator: '',
          });
          if (this.form.get('alarm_type').value === 'log') {
            this.form.patchValue({
              timeStampOption: this.timeStampOptions[0].offset,
            });
          }
          this.form.get('aggregator').disable();
        }
      });
    if (this.alarm_name) {
      this.form.get('resource_name').disable();
    } else if (this.formType === 'admin' && this.cluster_name) {
      this.form.patchValue({
        resource_name: this.cluster_name,
        resource_type: 'Cluster',
      });
      this.form.get('resource_name').disable();
    }
    if (this.formType === 'admin') {
      const [prometheus_metrics, function_names] = await Promise.all([
        this.metricService.getPrometheusMetrics(this.cluster_name),
        this.alarmService.getPrometheusFunctions(),
      ]);
      this.prometheus_metrics = this.prometheus_metrics_cluster =
        prometheus_metrics || [];
      this.function_names = function_names || [];
    } else if (this.formType === 'user') {
      // 见http://confluence.alaudatech.com/x/AiFYAg 中间表格的Workload指标
      let pod_name_pattern = '';
      if (this.kind === 'deployment') {
        pod_name_pattern = '.*-.*';
      } else if (this.kind === 'daemonset' || this.kind === 'statefulset') {
        pod_name_pattern = '.*';
      }
      const [prometheus_metrics, function_names] = await Promise.all([
        this.metricService
          .getPrometheusMetricLabels(
            this.cluster_name,
            `{namespace="${this.namespace}",pod_name=~"${this.pod_name}-${pod_name_pattern}"}`,
          )
          .then(labels => {
            const array = labels.map(el => el.metric.__name__);
            return [...new Set(array)];
          }),
        this.alarmService.getPrometheusFunctions(),
      ]);
      this.prometheus_metrics = prometheus_metrics || [];
      this.function_names = function_names || [];
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createForm() {
    return this.fb.group({
      alarm_type: ['default'],
      monitor_metric: ['', Validators.required],
      metric_expression: [
        { value: '', disabled: true },
        {
          validators: [Validators.required],
          asyncValidators: [this.prometheusMetricValidate.bind(this)],
          updateOn: 'blur',
        },
      ],
      unit: [''],
      aggregator: [{ value: '', disabled: true }, Validators.required],
      data_type: [false],
      timeStampOption: [0],
      resource_type: [''],
      resource_name: [''],
      log_content: [{ value: [''], disabled: true }, [Validators.required]],
    });
  }

  getDefaultFormModel(): AlarmMetricFormModel {
    return {
      alarm_type: 'default',
      data_type: false,
    };
  }

  adaptFormModel(formModel: AlarmMetricFormModel): AlarmMetric {
    let resource_name = '';
    let metric_name = formModel.monitor_metric;
    const log_content =
      formModel.log_content &&
      formModel.log_content
        .map(str => {
          // 将多个空格转换为一个空格并且删除字符串两端的空白字符
          return str.replace(/\s{1,}/g, ' ').trim();
        })
        .join(' ');
    if (formModel.resource_name) {
      resource_name = formModel.resource_name;
    } else {
      // 在resource_name disabled时获取它的值
      resource_name = this.form.getRawValue().resource_name;
    }
    const query: MetricQueries = {
      aggregator: formModel.aggregator,
      range: formModel.timeStampOption,
      labels: [],
    };
    if (this.formType === 'admin') {
      query.labels = query.labels.concat([
        {
          type: 'EQUAL',
          name: 'name',
          value: resource_name,
        },
        {
          type: 'EQUAL',
          name: 'kind',
          value: formModel.resource_type,
        },
      ]);
    }
    if (formModel.alarm_type === 'custom') {
      query.labels = query.labels.concat([
        {
          type: 'EQUAL',
          name: '__name__',
          value: 'custom',
        },
        {
          type: 'EQUAL',
          name: 'expr',
          value: formModel.metric_expression,
        },
      ]);
    } else if (formModel.alarm_type === 'default') {
      query.labels = query.labels.concat([
        {
          type: 'EQUAL',
          name: '__name__',
          value: metric_name,
        },
      ]);
    } else if (formModel.alarm_type === 'log') {
      metric_name = 'workload.log.keyword.count';
      query.labels = query.labels.concat([
        {
          type: 'EQUAL',
          name: '__name__',
          value: metric_name,
        },
        {
          type: 'EQUAL',
          name: 'query',
          value: log_content,
        },
      ]);
    }
    return {
      metric: {
        queries: [query],
      },
      unit: formModel.unit,
      metric_name,
      expr: formModel.metric_expression,
      query: log_content,
      resource_name,
    };
  }

  adaptResourceModel(resource: AlarmMetric): AlarmMetricFormModel {
    if (resource) {
      const formModel: AlarmMetricFormModel = {
        alarm_type: 'default',
        metric_expression: resource.expr,
        data_type: false,
        resource_name: resource.resource_name,
      };
      if (resource.type) {
        formModel.resource_type = capitalizeFirstLetter(resource.type);
        formModel.monitor_metric = resource.metric_name;
      }
      if (
        resource.metric &&
        resource.metric.queries &&
        resource.metric.queries[0]
      ) {
        const aggregator = resource.metric.queries[0].aggregator;
        if (aggregator !== undefined && aggregator !== '') {
          formModel.data_type = true;
        }
        formModel.aggregator = aggregator;
        formModel.timeStampOption = resource.metric.queries[0].range;
      }
      if (resource.metric_name === 'custom') {
        formModel.alarm_type = 'custom';
        formModel.metric_expression = resource.expr;
        formModel.unit = resource.unit;
      }
      if (resource.metric_name === 'workload.log.keyword.count') {
        formModel.alarm_type = 'log';
        formModel.log_content = resource.query.split(' ');
      }
      return formModel;
    }
  }

  onExpressionInput(event: Event) {
    this.expressionChanged$.next(event.target['value']);
  }

  private filterMetrics(kind: string) {
    this.metrics = this.metric_type.filter(el => {
      return el.kind === kind && el.type === 'metric';
    });
    if (
      !this.metrics.find(
        element => element.name === this.form.get('monitor_metric').value,
      ) &&
      this.metrics.length
    ) {
      this.form.patchValue({
        monitor_metric: this.metrics[0].name,
      });
    }
  }

  // 见http://confluence.alaudatech.com/x/AiFYAg 中间表格的Node指标
  private updatePredictExpression(node_ip: string) {
    this.metricService
      .getPrometheusMetricLabels(
        this.cluster_name,
        `{instance=~"${node_ip}:.*"}`,
      )
      .then(labels => {
        const array = labels.map(el => el.metric.__name__);
        this.prometheus_metrics = [...new Set(array)];
      });
  }

  // 动态更新自动补全，见http://confluence.alaudatech.com/x/gIIyAg 最下方的辅助输入API
  protected predictExpression(exp: string) {
    const found = exp.match(/([\d\w\-])+$/g);
    if (found && found[0]) {
      const match = found[0].toLowerCase();
      // 监控指标补全
      this.prometheus_metrics_suggestion = this.prometheus_metrics
        .filter(el => el.startsWith(match))
        .map(el => {
          return exp + el.split(found[0])[1];
        });
      // 函数名补全
      this.function_names_suggestion = this.function_names
        .filter(el => el.startsWith(match))
        .map(el => {
          return exp + el.split(found[0])[1];
        });
      this.cdr.markForCheck();
    }
    // label的可用值补全
    let foundLabel = exp.match(/([\d\w\-])+=([\d\w\-])+$/g);
    if (foundLabel && foundLabel[0]) {
      foundLabel = foundLabel[0].split('=');
      this.metricService
        .getPrometheusMetricLabels(this.cluster_name, foundLabel[0])
        .then(labels => {
          this.prometheus_metric_labels = labels
            .filter(el => {
              return el['value'][1].startsWith(foundLabel[1]);
            })
            .map(el => {
              return exp + el['value'][1].split(foundLabel[1])[1];
            });
          this.cdr.markForCheck();
        });
    }
  }

  prometheusMetricValidate(ctrl: AbstractControl) {
    return from(
      this.metricService.getPrometheusMetricLabels(
        this.cluster_name,
        ctrl.value,
      ),
    ).pipe(
      map((res: HttpErrorResponse | any) => {
        if (res.status >= 400) {
          this.auiNotificationService.error({
            title: this.translate.get(ALARM_METRIC_PATTERN.tip),
            content: res.error.errors[0].message,
          });
          return {
            metric: this.translate.get(ALARM_METRIC_PATTERN.tip),
          };
        } else {
          return null;
        }
      }),
      catchError(() => null),
    );
  }

  protected getUnitType(metric: string) {
    return getUnitType(metric, this.metric_type);
  }

  protected percentFlag() {
    if (
      (this.form.get('alarm_type').value === 'custom' &&
        this.form.get('unit').value === '%') ||
      this.getUnitType(this.form.get('monitor_metric').value) === '%'
    ) {
      return true;
    }
    return false;
  }
}
