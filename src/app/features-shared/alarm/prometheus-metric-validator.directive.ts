import { NotificationService } from '@alauda/ui';
import { HttpErrorResponse } from '@angular/common/http';
import { Directive, Input } from '@angular/core';
import {
  AbstractControl,
  AsyncValidator,
  NG_ASYNC_VALIDATORS,
  ValidationErrors,
} from '@angular/forms';
import { MetricService } from 'app/services/api/metric.service';
import { TranslateService } from 'app/translate/translate.service';
import { ALARM_METRIC_PATTERN } from 'app/utils/patterns';
import { Observable, from } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

@Directive({
  selector: 'input[rcPrometheusMetricValidate]',
  providers: [
    {
      provide: NG_ASYNC_VALIDATORS,
      useExisting: PrometheusMetricValidatorDirective,
      multi: true,
    },
  ],
})
export class PrometheusMetricValidatorDirective implements AsyncValidator {
  @Input()
  cluster: string;

  constructor(
    private translate: TranslateService,
    private metricService: MetricService,
    private auiNotificationService: NotificationService,
  ) {}

  validate(ctrl: AbstractControl): Observable<ValidationErrors | null> {
    if (!ctrl.value) {
      return null;
    }
    return from(
      this.metricService.getPrometheusMetricLabels(this.cluster, ctrl.value),
    ).pipe(
      map((res: HttpErrorResponse | any) => {
        if (res.status >= 400) {
          this.auiNotificationService.error({
            title: this.translate.get(ALARM_METRIC_PATTERN.tip),
            content: res.error.errors[0].message,
          });
          return {
            metric: this.translate.get(ALARM_METRIC_PATTERN.tip),
          };
        } else {
          return null;
        }
      }),
      catchError(() => null),
    );
  }
}
