import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { AlarmBasicInfoComponent } from 'app/features-shared/alarm/basic-info/component';
import { AlarmInfoFormComponent } from 'app/features-shared/alarm/info-form/component';
import { AlarmMetricFormComponent } from 'app/features-shared/alarm/metric-form/component';
import { PrometheusMetricValidatorDirective } from 'app/features-shared/alarm/prometheus-metric-validator.directive';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { AlarmActionComponent } from 'app2/features/alarm/alarm-action/alarm-action.component';
import { ComponentAlarmComponent } from 'app_user/features/app/pod-controller/alarm/list/component.ts';

const components = [
  AlarmActionComponent,
  AlarmBasicInfoComponent,
  AlarmInfoFormComponent,
  AlarmMetricFormComponent,
  ComponentAlarmComponent,
  PrometheusMetricValidatorDirective,
];

@NgModule({
  imports: [RouterModule, SharedModule, FormTableModule],
  declarations: components,
  exports: components,
  entryComponents: [AlarmActionComponent],
})
export class AlarmSharedModule {}
