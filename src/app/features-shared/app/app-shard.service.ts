import { DialogService, DialogSize } from '@alauda/ui';
import { Injectable } from '@angular/core';

import { endsWith } from 'lodash-es';
import { filter, first, take, tap } from 'rxjs/operators';

import { ImageSelectComponent } from 'app/features-shared/image/image-select/component';
import {
  ImageRepositoryService,
  ImageRepositoryTagDetail,
} from 'app/services/api/image-repository.service';
import { Space } from 'app/services/api/space.service';
import { SpacesFacadeService } from 'app/store/facades';
import { TranslateService } from 'app/translate/translate.service';
import { RcImageSelection } from 'app/typings/k8s-form-model';

@Injectable()
export class AppShardService {
  constructor(
    private dialogService: DialogService,
    private repositoryService: ImageRepositoryService,
    private translateService: TranslateService,
    private spacesFacade: SpacesFacadeService,
  ) {}

  selectImage(params?: { projectName: string }) {
    const spaceName = this.getCurrentSpaceName();
    const dialogRef = this.dialogService.open(ImageSelectComponent, {
      size: DialogSize.Big,
      data: {
        projectName: params.projectName,
        spaceName,
      },
    });

    return dialogRef.componentInstance.close.pipe(
      first(),
      tap(() => {
        dialogRef.close();
      }),
    );
  }

  formatContainerCPU(rawData: string) {
    if (!rawData) {
      return '-';
    }
    return endsWith(rawData, 'm')
      ? rawData
      : rawData + this.translateService.get('unit_core');
  }

  async getImagePorts(params: RcImageSelection) {
    let data: ImageRepositoryTagDetail;
    if (params.registry_name) {
      data = await this.repositoryService.getRepositoryTagDetail({
        registry_name: params.registry_name,
        project_name: params.project_name,
        repository_name: params.repository_name,
        tag_name: params.tag,
        is_third: params.is_third,
      });
    } else {
      data = await this.repositoryService.getPublicRepositoryTagDetail({
        image_name: params.full_image_name,
        tag_name: params.tag,
      });
    }
    if (data && data.instance_ports && data.instance_ports.length) {
      return data.instance_ports;
    } else {
      return [];
    }
  }

  private getCurrentSpaceName() {
    let space: Space;
    this.spacesFacade.currentSpace$
      .pipe(
        filter(s => !!s),
        take(1),
      )
      .subscribe(s => {
        space = s;
      });
    return space ? space.name : '';
  }
}
