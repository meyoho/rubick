import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { EventEmitter, Output } from '@angular/core';

import { cloneDeep, find, get } from 'lodash-es';

import { AppShardService } from 'app/features-shared/app/app-shard.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, Pod, WorkspaceBaseParams } from 'app/typings';
import { PodController } from 'app/typings/backend-api';
import {
  ContainerVolumeViewModel,
  EXCLUDE_LOG_PATH_KEY,
  LOG_PATH_KEY,
} from 'app/typings/k8s-form-model';
import {
  Container,
  EnvVar,
  JobTemplateSpec,
  Probe,
  Volume,
} from 'app/typings/raw-k8s';
import { DetailDataService } from 'app_user/features/app/detail-data.service';

interface Meta {
  name: string;
  image: string;
  args: string[];
  command: string[];
  resource: {
    cpu: string;
    memory: string;
  };
  pods?: any[];
}

interface Healthcheck {
  livenessProbe: Probe;
  readinessProbe: Probe;
}

interface ContainerViewModel {
  meta: Meta;
  volumes: ContainerVolumeViewModel[];
  healthcheck: Healthcheck;
  logPaths?: string[];
  excludeLogPaths?: string[];
}

@Component({
  selector: 'rc-pod-controller-containers',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  providers: [DetailDataService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerContainersComponent implements OnChanges {
  @Input()
  canUpdate = false;
  @Input()
  @Input()
  podController: PodController | JobTemplateSpec;
  @Input() actions: string[] = []; // ['image', 'resource', 'log', 'exec'];
  @Input()
  baseParams: WorkspaceBaseParams;
  @Output() onUpdateImageTag = new EventEmitter<string>(); // containerName: string
  @Output() onUpdateResourceSize = new EventEmitter<string>(); // containerName: string
  @Output() onOpenTerminal = new EventEmitter<{
    pods: string; // pod names separated by ;
    selectedPod: string;
    container: string;
  }>();
  @Output() onOpenLog = new EventEmitter<{
    pod: string;
    container: string;
  }>();
  private containers: Container[];
  private volumes: Volume[];
  private matchLabels: {
    [key: string]: string;
  };
  private selectedTabIndex = 0;
  private seletedContainer: ContainerViewModel;

  initialized = false;
  containerViewModels: ContainerViewModel[];
  pods: K8sResourceWithActions<Pod>[];
  toggleBarText = {
    volume: this.translateService.get('mounted_volumes'),
    healthcheck: this.translateService.get('health_check'),
    logpath: this.translateService.get('log_path'),
  };
  foldedStatus = {};
  podsLoading: boolean;
  podsLoaded: boolean;

  get templateContext() {
    return { container: this.seletedContainer };
  }

  constructor(
    private cdr: ChangeDetectorRef,
    private appShardService: AppShardService,
    private k8sResourceService: K8sResourceService,
    private translateService: TranslateService,
  ) {}

  ngOnChanges({ podController }: SimpleChanges): void {
    if (podController && podController.currentValue) {
      this.onInputChange();
    }
  }

  private async onInputChange() {
    this.containers = get(
      this.podController,
      'spec.template.spec.containers',
      [],
    );

    this.volumes = get(this.podController, 'spec.template.spec.volumes', []);

    this.matchLabels = get(this.podController, 'spec.selector.matchLabels', {});

    this.containerViewModels = this.containers.map(c =>
      this.getContainerViewModel(c),
    );
    this.seletedContainer = this.containerViewModels[this.selectedTabIndex];
    this.initialized = true;
    this.cdr.markForCheck();
  }

  private getContainerViewModel(container: Container) {
    const meta: Meta = {
      name: container.name || '-',
      image: container.image || '-',
      resource: {
        cpu: this.appShardService.formatContainerCPU(
          get(container, 'resources.limits.cpu'),
        ),
        memory: get(container, 'resources.limits.memory', '-'),
      },
      args: container.args || [],
      command: container.command || [],
    };
    const volumes = this.getVolumes(container);
    const healthcheck = {
      livenessProbe: get(container, 'livenessProbe', null),
      readinessProbe: get(container, 'readinessProbe', null),
    };

    const viewModel: ContainerViewModel = {
      meta,
      volumes,
      healthcheck,
    };

    if (container.env && container.env.length) {
      const logPathEnvVar = container.env.find((item: EnvVar) => {
        return !item.valueFrom && item.name === LOG_PATH_KEY;
      });
      const excludeLogPathEnvVar = container.env.find((item: EnvVar) => {
        return !item.valueFrom && item.name === EXCLUDE_LOG_PATH_KEY;
      });
      if (logPathEnvVar && logPathEnvVar.value) {
        viewModel.logPaths = logPathEnvVar.value.split(',');
      }
      if (excludeLogPathEnvVar && excludeLogPathEnvVar.value) {
        viewModel.excludeLogPaths = excludeLogPathEnvVar.value.split(',');
      }
    }
    return viewModel;
  }

  private getVolumes(co: Container) {
    const volumes = this.volumes.map((item: Volume) =>
      this.getContainerVolumeViewModel(item),
    );
    return co.volumeMounts && co.volumeMounts.length
      ? this.getContainerVolumesViewModel(co, volumes)
      : null;
  }

  private getContainerVolumeViewModel(data: Volume) {
    const volume: ContainerVolumeViewModel = {
      name: data.name,
      containerPath: '', // containerPath定义在每个container的volumeMounts节点中
    };
    if (data.persistentVolumeClaim) {
      volume.type = 'pvc';
      volume.pvcName = get(data, 'persistentVolumeClaim.claimName', '');
    } else if (data.hostPath) {
      volume.type = 'host-path';
      volume.hostPath = get(data, 'hostPath.path', '');
    } else if (data.configMap) {
      volume.type = 'configmap';
      volume.configMapName = data.configMap.name;
    }
    return volume;
  }

  private getContainerVolumesViewModel(container: any, volumes: any[]) {
    return get(container, 'volumeMounts', [])
      .map((item: any) => {
        const volume = cloneDeep(volumes.find(v => v.name === item.name));
        let duplicateFlag = false;
        if (volume) {
          if (volume.type !== 'configmap') {
            volume.containerPath = item.mountPath;
            if (item.subPath) {
              volume.subPath = item.subPath;
            }
          } else {
            if (item.subPath) {
              volume.containerPath = '';
              volume.configMapKeyRef = true;
              if (!volume.configMapKeyMap) {
                volume.configMapKeyMap = [];
              } else {
                duplicateFlag = true;
              }
              volume.configMapKeyMap.push({
                key: item.subPath,
                path: item.mountPath,
              });
            } else {
              volume.containerPath = item.mountPath;
            }
          }
          return duplicateFlag ? null : volume;
        }
        return null;
      })
      .filter((v: any) => !!v);
  }

  async getPods() {
    const labelSelector = Object.keys(this.matchLabels || {})
      .map(key => `${key}=${this.matchLabels[key]}`)
      .join(',');
    return await this.k8sResourceService.getK8sResources<Pod>('pods', {
      clusterName: this.baseParams.cluster,
      namespace: this.baseParams.namespace,
      labelSelector: labelSelector,
    });
  }

  shouldShowAction(action: string) {
    return this.actions.includes(action);
  }

  // ===== actions =====

  canEXEC(pod: K8sResourceWithActions<Pod>, container: Container): boolean {
    const podStatus = get(pod, 'kubernetes.status.containerStatuses', null);
    if (!podStatus) {
      return false;
    }
    const containerStatus = find(
      podStatus,
      co => co.name === get(container, 'meta.name', ''),
    );

    return !!get(containerStatus, 'state.running', null);
  }

  canNotEXECAll(
    pods: K8sResourceWithActions<Pod>[],
    container: Container,
  ): boolean {
    if (pods) {
      return pods.every(pod => {
        return !this.canEXEC(pod, container);
      });
    }
  }

  openTerminal(
    pods: K8sResourceWithActions<Pod>[],
    container: Container,
    selected?: K8sResourceWithActions<Pod>,
  ) {
    const podNames = pods
      .map(pod => get(pod, 'kubernetes.metadata.name', ''))
      .join(';');
    const selectedName = get(selected, 'kubernetes.metadata.name', '');
    const containerName = get(container, 'meta.name', '');
    this.onOpenTerminal.emit({
      pods: podNames,
      selectedPod: selectedName || '',
      container: containerName,
    });
  }

  openLog(pod: K8sResourceWithActions<Pod>, container: Container) {
    const podName = get(pod, 'kubernetes.metadata.name', '');
    const containerName = get(container, 'meta.name', '');
    this.onOpenLog.emit({
      pod: podName,
      container: containerName,
    });
  }

  updateImageTag(container: ContainerViewModel) {
    this.onUpdateImageTag.emit(container.meta.name);
  }

  updateResourceSize(container: ContainerViewModel) {
    this.onUpdateResourceSize.emit(container.meta.name);
  }

  // toggle
  onToggle(folded: boolean, resourceKind: string) {
    this.foldedStatus[resourceKind] = folded;
    this.cdr.markForCheck();
  }

  trackByFn(index: number) {
    return index;
  }

  /**
   * Deployment 和 CronJob 对应的Pod 的全名 podFullName 规则如下：
   * Deployment : {Deployment名字}-{随机 ReplicaSet名字}-{随机串}
   * CronJob: {CronJob名字}-{执行后的Job的名字}-{随机串}
   * 取最后一部分展示
   * @param podFullName
   */
  getPodShortName(podFullName: string) {
    const splitArr = podFullName.split('-');
    return splitArr[splitArr.length - 1];
  }

  onTabIndexChange(index: number) {
    this.selectedTabIndex = index;
    this.seletedContainer = this.containerViewModels[index];
    this.foldedStatus = {
      volume: false,
      healthcheck: false,
    };
    this.cdr.markForCheck();
  }

  async onMenuShow() {
    this.podsLoading = true;

    this.pods = await this.getPods();

    this.podsLoaded = true;
    this.podsLoading = false;
    this.cdr.markForCheck();
  }

  onMenuHide() {
    this.podsLoaded = false;
  }
}
