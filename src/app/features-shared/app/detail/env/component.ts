import {
  Component,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { EventEmitter } from '@angular/core';

import { get } from 'lodash-es';

import {
  adaptEnvFromSourceResource,
  EnvFromSourceFormModel,
} from 'app/features-shared/app/utils/env-from';
import {
  JobTemplateSpec,
  PodController,
  WorkspaceBaseParams,
} from 'app/typings';
import { Container } from 'app/typings/raw-k8s';

interface EnvViewModel extends Container {
  name: string;
  value: string;
  reference: boolean;
  configmap: string;
  configmapKey: string;
}

@Component({
  selector: 'rc-pod-controller-detail-env',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class PodControllerDetailEnvComponent implements OnChanges {
  @Input()
  canUpdate: boolean;
  @Input()
  podController: PodController | JobTemplateSpec;
  @Input()
  baseParams: WorkspaceBaseParams;

  @Output() onUpdateEnv = new EventEmitter<{
    containerName: string;
    type: 'env' | 'envFrom';
  }>();

  containers: Container[] = [];

  // FIXME
  containerViewModel: any[];
  envViewModel: EnvViewModel[];
  envFromViewModel: EnvFromSourceFormModel[];

  initialized = false;

  ngOnChanges({ podController }: SimpleChanges): void {
    if (podController && podController.currentValue) {
      this.containers = get(
        this.podController,
        'spec.template.spec.containers',
        [],
      );
      this.containerViewModel = [];
      this.containers.forEach((co: Container, index: number) => {
        this.containerViewModel[index] = {
          envViewModel: this.getContainerEnvvarViewModelFromYaml(co),
          envFromViewModel: adaptEnvFromSourceResource(co.envFrom || []),
        };
      });
      this.initialized = true;
    }
  }

  getContainerEnvvarViewModelFromYaml(container: Container) {
    let model;
    const env = container.env || [];
    model = env
      .filter((item: any) => {
        return ![
          '__ALAUDA_FILE_LOG_PATH__',
          '__ALAUDA_EXCLUDE_LOG_PATH__',
          '__ALAUDA_SERVICE_ID__',
          '__ALAUDA_SERVICE_NAME__',
          '__ALAUDA_APP_NAME__',
        ].includes(item.name);
      })
      .map((item: any) => {
        const data = {
          name: item.name,
          value: item.value || '',
          reference: !!item.valueFrom,
          valueFrom: item.valueFrom,
          configmap: get(item, 'valueFrom.configMapKeyRef.name', ''),
          configmapKey: get(item, 'valueFrom.configMapKeyRef.key', ''),
          secret: get(item, 'valueFrom.secretKeyRef.name', ''),
          secretKey: get(item, 'valueFrom.secretKeyRef.key', ''),
        };
        return data;
      });
    return model;
  }

  updateEnv(container: Container, type: 'env' | 'envFrom' = 'env') {
    this.onUpdateEnv.emit({
      containerName: container.name,
      type,
    });
  }

  constructor() {}
}
