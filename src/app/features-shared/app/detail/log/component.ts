import { CodeEditorIntl } from '@alauda/code-editor';
import {
  ChangeDetectorRef,
  Component,
  Input,
  NgZone,
  OnChanges,
  OnDestroy,
  SimpleChanges,
} from '@angular/core';

import { get, map as _map } from 'lodash-es';
import { editor } from 'monaco-editor';
import { MonacoProviderService } from 'ng-monaco-editor';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  filter,
  map,
  mergeScan,
  publishReplay,
  refCount,
  tap,
} from 'rxjs/operators';

import { AppService } from 'app/services/api/app.service';
import { HttpService } from 'app/services/http.service';
import { TranslateService } from 'app/translate/translate.service';
import { JobTemplateSpec, WorkspaceBaseParams } from 'app/typings';
import { PodController } from 'app/typings/backend-api';
import { Pod } from 'app/typings/raw-k8s';
import { FormatUtcStrPipe } from 'app2/shared/pipes/format-utc-str.pipe';

interface ComponentInstance {
  uuid: string;
  name: string;
  status: string;
  labels: {
    [key: string]: string;
  };
  containers: any[];
}

interface ComponentContainer {
  name: string;
  state: string; // https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.10/#containerstate-v1-core
  // one of 'running', 'terminated', 'waiting'
}

export interface LogDetails {
  info: LogInfo;
  logs: LogLine[];
  selection: LogSelection;
}

export interface LogInfo {
  podName: string;
  containerName: string;
  initContainerName: string;
  fromDate: string;
  toDate: string;
  truncated: boolean;
}

export interface LogLine {
  timestamp: string;
  content: string;
}

export interface LogSelection {
  logFilePosition: string;
  referencePoint: LogLineReference;
  offsetFrom: number;
  offsetTo: number;
}

export interface LogSelectionParams {
  logFilePosition: string;
  referenceTimestamp: string;
  referenceLineNum: number;
  offsetFrom: number;
  offsetTo: number;
}

export interface LogLineReference {
  timestamp: string;
  lineNum: number;
}

type LogSelectionParamScaner = (prev: LogSelection) => LogSelectionParams;

const logsPerView = 100;
const maxLogSize = 2e9;
// Load logs from the beginning of the log file. This matters only if the log file is too large to
// be loaded completely.
const beginningOfLogFile = 'beginning';
// Load logs from the end of the log file. This matters only if the log file is too large to be
// loaded completely.
const endOfLogFile = 'end';
const oldestTimestamp = 'oldest';
const newestTimestamp = 'newest';

@Component({
  selector: 'rc-pod-controller-detail-log',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class PodControllerDetailLogComponent implements OnDestroy, OnChanges {
  @Input()
  pods: Pod[] = [];
  @Input()
  podController: PodController | JobTemplateSpec;
  @Input()
  baseParams: WorkspaceBaseParams;
  @Input()
  initialState: {
    pod: string;
    container: string;
  };

  private intervalTimer: any;
  private editor: editor.IStandaloneCodeEditor;
  private logDetails$: Observable<LogDetails>;
  private logDetails: LogDetails; // Latest fetched log detail
  private logSelectionParamScanner$ = new BehaviorSubject<
    LogSelectionParamScaner
  >(null);
  private selectedInstance: ComponentInstance;
  private selectedContainer: ComponentContainer;
  selectedInstanceName: string;
  selectedContainerName: string;
  logs$: Observable<string>;
  logInfo$: Observable<LogInfo>;
  logsReady$: Observable<boolean>;

  instances: Array<ComponentInstance>;
  containers: Array<ComponentContainer>;

  monacoOptions: editor.IEditorConstructionOptions = {
    wordWrap: 'on',
    readOnly: true,
    renderLineHighlight: 'none',
  };

  pollNewest = true;
  initialized: boolean;

  ngOnChanges({ pods, podController }: SimpleChanges): void {
    if (
      (pods && pods.currentValue) ||
      (podController && podController.currentValue)
    ) {
      this.getComponentInstances();
      if (!this.initialized) {
        this.intervalTimer = setInterval(() => {
          if (this.pollNewest) {
            this.loadNewest();
          }
        }, 10000);
      }
      this.initialized = true;
      this.cdr.markForCheck();
    }
  }

  async getComponentInstances() {
    if (this.podController) {
      const matchLabels = get(
        this.podController,
        'spec.selector.matchLabels',
        {},
      );
      const labelSelector = Object.keys(matchLabels || {})
        .map(key => `${key}=${matchLabels[key]}`)
        .join(',');

      const res = await this.appService
        .getResourcesInNamespace({
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
          labelSelector,
          resource_type: 'pods',
        })
        .then((res: any) => res.results || [])
        .catch(_err => []);

      this.pods = _map(res, i => i.kubernetes);
    }

    this.instances = _map(
      this.pods,
      ({
        spec: { containers },
        status: { phase },
        metadata: { name, uid, labels },
      }) => ({
        uuid: uid,
        name,
        labels,
        status: phase,
        containers: _map(containers, c => {
          return {
            name: c.name,
          };
        }),
      }),
    );

    if (this.instances.length && !this.selectedInstance) {
      this.selectedInstance =
        this.initialState && this.initialState.pod
          ? this.instances.find(
              (item: ComponentInstance) => item.name === this.initialState.pod,
            )
          : this.instances[0];

      this.selectedInstanceName = this.selectedInstance.name;
      this.resetContainerNamesOptions();
      this.setupLogDetails$();
    }
  }

  ngOnDestroy() {
    clearInterval(this.intervalTimer);
  }

  onInstanceNameSelect(instanceName: string) {
    this.selectedInstance = this.instances.find(
      (item: ComponentInstance) => item.name === instanceName,
    );
    this.resetContainerNamesOptions();
    this.setupLogDetails$();
  }

  onContainerNameSelect(containerName: string) {
    this.selectedContainer = this.selectedInstance.containers.find(
      (item: ComponentContainer) => {
        return item.name === containerName;
      },
    );
    this.setupLogDetails$();
  }

  loadNewest() {
    this.logSelectionParamScanner$.next(_prev => ({
      logFilePosition: endOfLogFile,
      referenceTimestamp: newestTimestamp,
      referenceLineNum: 0,
      offsetFrom: maxLogSize,
      offsetTo: maxLogSize + logsPerView,
    }));
  }

  loadOldest() {
    this.logSelectionParamScanner$.next(_prev => ({
      logFilePosition: beginningOfLogFile,
      referenceTimestamp: oldestTimestamp,
      referenceLineNum: 0,
      offsetFrom: -maxLogSize - logsPerView,
      offsetTo: -maxLogSize,
    }));
    this.pollNewest = false;
  }

  /**
   * Shifts view by maxLogSize lines to the past.
   * @export
   */
  loadOlder() {
    this.logSelectionParamScanner$.next(prev => ({
      logFilePosition: prev.logFilePosition,
      referenceTimestamp: prev.referencePoint.timestamp,
      referenceLineNum: prev.referencePoint.lineNum,
      offsetFrom: prev.offsetFrom - logsPerView,
      offsetTo: prev.offsetFrom,
    }));
    this.pollNewest = false;
  }

  /**
   * Shifts view by maxLogSize lines to the future.
   * @export
   */
  loadNewer() {
    this.logSelectionParamScanner$.next(prev => ({
      logFilePosition: prev.logFilePosition,
      referenceTimestamp: prev.referencePoint.timestamp,
      referenceLineNum: prev.referencePoint.lineNum,
      offsetFrom: prev.offsetTo,
      offsetTo: prev.offsetTo + logsPerView,
    }));
    this.pollNewest = false;
  }

  onFindClicked() {
    return this.checkActionAndRun('actions.find');
  }

  onMonacoEditorChanged(editor: editor.IStandaloneCodeEditor) {
    this.editor = editor;
    this.scrollToBottom();
  }

  private resetContainerNamesOptions() {
    if (!this.selectedInstance) {
      return;
    }
    this.containers = this.selectedInstance.containers.map(item => item);
    if (this.containers.length) {
      this.selectedContainer =
        this.initialState && this.initialState.container
          ? this.containers.find(
              (item: ComponentContainer) =>
                item.name === this.initialState.container,
            )
          : this.containers[0];
      this.selectedContainerName = this.selectedContainer.name;
    }
  }

  private checkActionAndRun(actionName: string) {
    const action = this.getEditorAction(actionName);
    return action && action.run();
  }

  private getEditorAction(actionName: string) {
    return this.editor && this.editor.getAction(actionName);
  }

  private formatAllLogs_(logs: LogLine[]) {
    if (!logs || logs.length === 0) {
      logs = [
        {
          timestamp: new Date().toUTCString(),
          content: this.translate.get('zero_state_hint', {
            resourceName: this.translate.get('logs'),
          }),
        },
      ];
    }
    return logs.map(line => this.formatLine(line));
  }

  /**
   * Formats the given log line as raw HTML to display to the user.
   */
  private formatLine(line: LogLine) {
    const formatter = new FormatUtcStrPipe();
    return `${formatter.transform(line.timestamp)} ${line.content}`;
  }

  private scrollToBottom() {
    this.zone.runOutsideAngular(() => {
      setTimeout(() => {
        if (this.editor && this.logDetails && this.logDetails.logs) {
          this.editor.revealLine(this.logDetails.logs.length);
        }
      });
    });
  }

  private setupLogDetails$() {
    if (!this.selectedInstance || !this.selectedContainer) {
      return;
    }
    this.logDetails$ = this.logSelectionParamScanner$.pipe(
      mergeScan<LogSelectionParamScaner, LogDetails>(
        (prev, scannerFn) => {
          const params: any = scannerFn(prev && prev.selection);
          function sanitizeParam(param: string) {
            if (params[param] !== undefined && params[param] !== null) {
              params[param] = '' + params[param];
            } else {
              params[param] = '';
            }
          }

          Object.keys(params).forEach(param => sanitizeParam(param));

          const endpoint = `/ajax/v2/kubernetes/clusters/${
            this.baseParams.cluster
          }/pods/${this.baseParams.namespace}/${this.selectedInstance.name}/${
            this.selectedContainer.name
          }/log`;

          if (this.selectedContainer.state === 'terminated') {
            params.previous = true;
          }
          return this.httpService.request<LogDetails>(endpoint, {
            method: 'GET',
            params,
          });
        },
        null,
        1,
      ),
      tap(logDetails => {
        this.logDetails = logDetails;
      }),
      publishReplay(1),
      refCount(),
    );

    this.logs$ = this.logDetails$.pipe(
      map(logDetails => this.formatAllLogs_(logDetails.logs).join('\n')),
    );

    this.logInfo$ = this.logDetails$.pipe(map(logDetails => logDetails.info));

    this.logsReady$ = this.logInfo$.pipe(
      filter(logInfo => !!logInfo),
      map(logInfo => logInfo.fromDate && logInfo.fromDate !== '0'),
    );

    this.loadNewest();
  }

  constructor(
    private cdr: ChangeDetectorRef,
    private zone: NgZone,
    private translate: TranslateService,
    private httpService: HttpService,
    public codeEditorIntl: CodeEditorIntl,
    private appService: AppService,
    public monacoProvider: MonacoProviderService,
  ) {
    this.instances = [];
    this.containers = [];
  }
}
