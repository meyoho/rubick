import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
} from '@angular/core';

@Component({
  selector: 'rc-app-detail-toggle-bar',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationToggleBarComponent implements OnInit {
  @Input()
  text: string;
  @Input()
  initialState: boolean; // true -> folded | false -> unfold

  @Output()
  clicked = new EventEmitter<boolean>();

  folded = false;

  toggle() {
    this.folded = !this.folded;
    this.clicked.emit(this.folded);
    this.cdr.markForCheck();
  }

  ngOnInit() {}
  constructor(private cdr: ChangeDetectorRef) {}
}
