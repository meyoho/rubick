import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { ContainerVolumeViewModel } from 'app/typings/k8s-form-model';

@Component({
  selector: 'rc-container-volumes-display',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class ContainerVolumesDisplayComponent implements OnInit, OnChanges {
  @Input()
  volumes: any[] = [];

  ngOnChanges({ volumes }: SimpleChanges): void {
    if (volumes && volumes.currentValue) {
      // TODO
    }
  }

  getItemNameOrPathDisplay(item: ContainerVolumeViewModel) {
    switch (item.type) {
      case 'host-path':
        return item.hostPath;
      case 'volume':
        return item.volumeName;
      case 'pvc':
        return item.pvcName;
      case 'configmap':
        return item.configMapName;
    }
  }

  ngOnInit(): void {}
  constructor() {}
}
