import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { cloneDeep, get, set } from 'lodash-es';

import { PodControllerKindToResourceTypeEnum } from 'app/features-shared/app/utils/pod-controller';
import { AppService } from 'app/services/api/app.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import {
  Container,
  CronJob,
  EnvFromSource,
  EnvVar,
  WorkspaceBaseParams,
} from 'app/typings';
import { PodController } from 'app/typings/backend-api';
import { removeDirtyDataBeforeUpdate } from 'app_user/features/app/util';

@Component({
  templateUrl: './template.html',
})
export class UpdateEnvDialogComponent implements OnInit {
  constructor(
    private appService: AppService,
    private errorToastService: ErrorsToastService,
    private k8sResourceService: K8sResourceService,
    @Inject(DIALOG_DATA)
    public data: {
      podController?: PodController;
      cronJob?: CronJob;
      containerName: string;
      baseParams: WorkspaceBaseParams;
      type: 'env' | 'envFrom';
    },
  ) {}

  @Output()
  close = new EventEmitter<boolean>();
  @ViewChild(NgForm)
  private form: NgForm;
  private defaultEnvs: EnvVar[];
  submitting = false;
  filteredEnvs: EnvVar[];
  envFrom: EnvFromSource[];
  shouldShowContainer: boolean;
  private containerPath: string;
  private resource: PodController | CronJob;

  ngOnInit(): void {
    this.initResource();
    const containers: Container[] = get(this.resource, this.containerPath, []);
    if (containers.length > 1) {
      this.shouldShowContainer = true;
    }
    const container = containers.find(
      container => container.name === this.data.containerName,
    );
    if (this.data.type === 'env') {
      const envs = container.env || [];
      // 不允许编辑默认的env，但不能删掉
      this.filteredEnvs = envs.filter((item: EnvVar) => {
        return !this.isDefaultEnv(item.name);
      });
      this.defaultEnvs = envs.filter((item: EnvVar) => {
        return this.isDefaultEnv(item.name);
      });
    } else {
      // type envFrom
      this.envFrom = container.envFrom || [];
    }
  }

  initResource() {
    if (this.data.podController) {
      this.resource = this.data.podController;
      this.containerPath = 'spec.template.spec.containers';
    }
    if (this.data.cronJob) {
      this.resource = this.data.cronJob;
      this.containerPath = 'spec.jobTemplate.spec.template.spec.containers';
    }
  }

  isDefaultEnv(name: string) {
    return [
      '__ALAUDA_FILE_LOG_PATH__',
      '__ALAUDA_EXCLUDE_LOG_PATH__',
      '__ALAUDA_SERVICE_ID__',
      '__ALAUDA_SERVICE_NAME__',
      '__ALAUDA_APP_NAME__',
    ].includes(name);
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    const resource = cloneDeep(this.resource);
    this.updateResourceContainers(
      resource,
      get(resource, this.containerPath, []),
    );
    this.updateResource(resource)
      .then(() => {
        this.submitting = false;
        this.close.next(true);
      })
      .catch(err => {
        this.submitting = false;
        this.errorToastService.error(err);
      });
  }

  cancel() {
    this.close.next(false);
  }

  private updateResourceContainers(
    resource: PodController | CronJob,
    containers: Container[],
  ) {
    const index = containers.findIndex(c => c.name === this.data.containerName);
    if (this.data.type === 'env') {
      set(resource, `${this.containerPath}[${index}].env`, [
        ...this.defaultEnvs,
        ...this.filteredEnvs,
      ]);
    } else {
      set(resource, `${this.containerPath}[${index}].envFrom`, [
        ...this.envFrom,
      ]);
    }
  }

  private updateResource(resource: PodController | CronJob) {
    if (this.data.podController) {
      return this.appService.updateResource({
        cluster: this.data.baseParams.cluster,
        namespace: this.data.baseParams.namespace,
        resource_type:
          PodControllerKindToResourceTypeEnum[this.data.podController.kind],
        name: resource.metadata.name,
        payload: removeDirtyDataBeforeUpdate(resource),
      });
    } else if (this.data.cronJob) {
      return this.k8sResourceService.updateK8sReource(
        this.data.baseParams.cluster,
        'cronjobs',
        removeDirtyDataBeforeUpdate(resource),
      );
    }
  }
}
