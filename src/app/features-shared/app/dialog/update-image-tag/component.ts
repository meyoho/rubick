import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { PodControllerKindToResourceTypeEnum } from 'app/features-shared/app/utils/pod-controller';
import { AppService } from 'app/services/api/app.service';
import { AppUtilitiesService } from 'app/services/api/app.utilities.service';
import {
  ImageRepositoryService,
  ImageRepositoryTag,
} from 'app/services/api/image-repository.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { CronJob, WorkspaceBaseParams } from 'app/typings';
import { PodController } from 'app/typings/backend-api';
import { RcImageSelection } from 'app/typings/k8s-form-model';
import { Container } from 'app/typings/raw-k8s';
import { removeDirtyDataBeforeUpdate } from 'app_user/features/app/util';

@Component({
  templateUrl: './template.html',
})
export class UpdateImageTagDialogComponent implements OnInit {
  @Output()
  close = new EventEmitter<any>();
  @ViewChild(NgForm)
  private form: NgForm;
  private targetContainer: Container;
  private image: string;
  selectedImageTag = '';
  imageTagOptions: string[] = [];
  imageName: string;
  submitting = false;
  initialized = false;

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    this.targetContainer.image = [this.imageName, this.selectedImageTag].join(
      ':',
    );
    if (this.dialogData.podController) {
      this.appService
        .updateResource({
          cluster: this.dialogData.baseParams.cluster,
          namespace: this.dialogData.baseParams.namespace,
          resource_type:
            PodControllerKindToResourceTypeEnum[
              this.dialogData.podController.kind
            ],
          name: this.dialogData.podController.metadata.name,
          payload: removeDirtyDataBeforeUpdate(this.dialogData.podController),
        })
        .then(() => {
          this.submitting = false;
          this.close.next(true);
        })
        .catch(err => {
          this.submitting = false;
          this.errorToastService.error(err);
        });
    } else if (this.dialogData.cronJob) {
      this.k8sResourceService
        .updateK8sReource(
          this.dialogData.baseParams.cluster,
          'cronjobs',
          removeDirtyDataBeforeUpdate(this.dialogData.cronJob),
        )
        .then(() => {
          this.submitting = false;
          this.close.next(true);
        })
        .catch(err => {
          this.submitting = false;
          this.errorToastService.error(err);
        });
    }
  }

  cancel() {
    this.close.next(null);
  }

  async ngOnInit(): Promise<void> {
    this.targetContainer = this.getTargetContainer();
    this.image = this.targetContainer.image;
    const [endpoint, ...imageRest] = this.image.split('/');
    const [imageName, imageTag] = imageRest.join('/').split(':');
    this.imageName = [endpoint, imageName].join('/');
    this.selectedImageTag = imageTag;
    const params: RcImageSelection = await this.appUtilities.getRepositoryParamsFromRawImageName(
      imageName,
    );
    this.imageTagOptions = await this.getImageTagOptions(params);
    this.initialized = true;
  }

  private async getImageTagOptions(params: RcImageSelection) {
    let tags;
    if (params.registry_name) {
      tags = await this.repositoryService
        .getRepositoryTags({
          registryName: params.registry_name,
          projectName: params.project_name,
          repositoryName: params.repository_name,
        })
        .then(({ results }: { results: ImageRepositoryTag[] }) => {
          if (!results) {
            results = [];
          }
          return results.map((tag: ImageRepositoryTag) => tag.tag_name);
        });
    } else {
      tags = await this.repositoryService.getPublicRepositoryTags(
        params.full_image_name,
      );
    }
    return tags;
  }

  private getTargetContainer() {
    const dialogData = this.dialogData;
    if (dialogData.podController) {
      return dialogData.podController.spec.template.spec.containers.find(
        c => c.name === this.dialogData.containerName,
      );
    } else if (dialogData.cronJob) {
      return dialogData.cronJob.spec.jobTemplate.spec.template.spec.containers.find(
        c => c.name === this.dialogData.containerName,
      );
    }
  }

  constructor(
    private appUtilities: AppUtilitiesService,
    private repositoryService: ImageRepositoryService,
    private errorToastService: ErrorsToastService,
    private appService: AppService,
    private k8sResourceService: K8sResourceService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      podController?: PodController;
      cronJob?: CronJob;
      containerName: string;
      baseParams: WorkspaceBaseParams;
    },
  ) {}
}
