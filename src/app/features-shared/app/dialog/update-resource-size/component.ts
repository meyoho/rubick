import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { PodControllerKindToResourceTypeEnum } from 'app/features-shared/app/utils/pod-controller';
import { AppService } from 'app/services/api/app.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { CronJob, WorkspaceBaseParams } from 'app/typings';
import { PodController } from 'app/typings/backend-api';
import { Container } from 'app/typings/raw-k8s';
import { removeDirtyDataBeforeUpdate } from 'app_user/features/app/util';

@Component({
  templateUrl: './template.html',
})
export class UpdateResourceSizeDialogComponent implements OnInit {
  @Output()
  close = new EventEmitter<any>();
  @ViewChild(NgForm)
  private form: NgForm;
  submitting = false;
  targetContainer: Container;

  get cluster() {
    return this.dialogData.baseParams.cluster;
  }
  get namespace() {
    return this.dialogData.baseParams.namespace;
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    if (this.dialogData.podController) {
      this.appService
        .updateResource({
          cluster: this.cluster,
          namespace: this.namespace,
          resource_type:
            PodControllerKindToResourceTypeEnum[
              this.dialogData.podController.kind
            ],
          name: this.dialogData.podController.metadata.name,
          payload: removeDirtyDataBeforeUpdate(this.dialogData.podController),
        })
        .then(() => {
          this.submitting = false;
          this.close.next(true);
        })
        .catch(err => {
          this.submitting = false;
          this.errorToastService.error(err);
        });
    } else if (this.dialogData.cronJob) {
      this.k8sResourceService
        .updateK8sReource(
          this.dialogData.baseParams.cluster,
          'cronjobs',
          removeDirtyDataBeforeUpdate(this.dialogData.cronJob),
        )
        .then(() => {
          this.submitting = false;
          this.close.next(true);
        })
        .catch(err => {
          this.submitting = false;
          this.errorToastService.error(err);
        });
    }
  }

  cancel() {
    this.close.next(null);
  }

  ngOnInit(): void {
    if (this.dialogData.podController) {
      this.targetContainer = this.dialogData.podController.spec.template.spec.containers.find(
        c => c.name === this.dialogData.containerName,
      );
    } else if (this.dialogData.cronJob) {
      this.targetContainer = this.dialogData.cronJob.spec.jobTemplate.spec.template.spec.containers.find(
        c => c.name === this.dialogData.containerName,
      );
    }
  }

  constructor(
    private appService: AppService,
    private errorToastService: ErrorsToastService,
    private k8sResourceService: K8sResourceService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      podController?: PodController;
      cronJob?: CronJob;
      containerName: string;
      baseParams: WorkspaceBaseParams;
    },
  ) {}
}
