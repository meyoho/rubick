import { Directive, HostBinding, Inject, Input, Optional } from '@angular/core';

import { CONTAINER_FORM_CONFIG, ContainerFormConfig } from '../tokens';
import { getFormFieldHiddenState } from './utils';

@Directive({ selector: '[rcContainerFormHiddenField]' })
export class ContainerFormHiddenFieldDirective {
  @Input('rcContainerFormHiddenField') key: string | string[];

  constructor(
    @Optional()
    @Inject(CONTAINER_FORM_CONFIG)
    private config: ContainerFormConfig,
  ) {}

  @HostBinding('hidden')
  get hiddenState(): boolean {
    return getFormFieldHiddenState(this.config, this.key);
  }
}
