import { Directive, HostBinding, Inject, Input, Optional } from '@angular/core';

import { POD_SPEC_FORM_CONFIG, PodSpecFormConfig } from '../tokens';
import { getFormFieldHiddenState } from './utils';

@Directive({ selector: '[rcPodSpecFormHiddenField]' })
export class PodSpecFormHiddenFieldDirective {
  @Input('rcPodSpecFormHiddenField') key: string | string[];

  constructor(
    @Optional()
    @Inject(POD_SPEC_FORM_CONFIG)
    private config: PodSpecFormConfig,
  ) {}

  @HostBinding('hidden')
  get hiddenState(): boolean {
    return getFormFieldHiddenState(this.config, this.key);
  }
}
