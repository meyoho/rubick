import { all } from 'ramda';

import { ContainerFormConfig, PodSpecFormConfig } from '../tokens';
export function getFormFieldHiddenState(
  config: PodSpecFormConfig | ContainerFormConfig,
  key: string | string[],
): boolean {
  if (!config || !config.hiddenFields || !key) {
    return false;
  } else {
    if (typeof key === 'string') {
      return config.hiddenFields.includes(key);
    } else if (Array.isArray(key)) {
      return all((key: string) => {
        return config.hiddenFields.includes(key);
      })(key);
    }
  }
}
