import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { FormGroup, ValidationErrors, Validators } from '@angular/forms';

import { isNumber, merge } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  DeploymentTypeMeta,
  HorizontalPodAutoscaler,
  HorizontalPodAutoscalerMeta,
} from 'app/typings/raw-k8s';

@Component({
  selector: 'rc-autoscaler-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AutoScalerFormComponent
  extends BaseResourceFormGroupComponent<HorizontalPodAutoscaler>
  implements OnInit, AfterViewInit {
  constructor(injector: Injector) {
    super(injector);
  }

  getDefaultFormModel() {
    return {
      apiVersion: HorizontalPodAutoscalerMeta.apiVersion,
      kind: HorizontalPodAutoscalerMeta.kind,
      spec: {
        scaleTargetRef: {
          apiVersion: DeploymentTypeMeta.apiVersion,
          kind: DeploymentTypeMeta.kind,
          name: '',
        },
      },
    };
  }

  adaptResource(resource: HorizontalPodAutoscaler): HorizontalPodAutoscaler {
    return resource;
  }

  adaptFormModel(formModel: HorizontalPodAutoscaler): HorizontalPodAutoscaler {
    if (formModel && formModel.spec) {
      if (formModel.spec.minReplicas) {
        formModel.spec.minReplicas = +formModel.spec.minReplicas;
      } else {
        delete formModel.spec.minReplicas;
      }
      if (formModel.spec.maxReplicas) {
        formModel.spec.maxReplicas = +formModel.spec.maxReplicas;
      } else {
        delete formModel.spec.maxReplicas;
      }
      if (formModel.spec.minReplicas || formModel.spec.maxReplicas) {
        return merge({}, formModel, this.getDefaultFormModel());
      }
    }
    return null;
  }

  private replicasValidator(formGroup: FormGroup): ValidationErrors | null {
    let minReplicas = formGroup.get('minReplicas').value;
    const maxReplicas = formGroup.get('maxReplicas').value;
    //minReplicas unset and maxReplicas settled
    if (!isNumber(minReplicas) && isNumber(maxReplicas)) {
      minReplicas = 1;
    }
    if (minReplicas < 0 || maxReplicas < 0) {
      //skip compare min and max
      return null;
    }
    if (minReplicas && minReplicas > maxReplicas) {
      return {
        minMaxErr: 'minReplicas_larger_than_maxReplicas',
      };
    } else {
      return null;
    }
  }

  createForm() {
    return this.fb.group({
      spec: this.fb.group(
        {
          minReplicas: this.fb.control('', [Validators.min(0)]),
          maxReplicas: this.fb.control('', [Validators.min(0)]),
        },
        { validators: [this.replicasValidator] },
      ),
    });
  }

  getFormModelName() {
    return '<Autoscaler Form>';
  }
}
