import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import {
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewEncapsulation,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { lensPath, omit, set } from 'ramda';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { PodControllerKindEnum } from 'app/features-shared/app/utils/pod-controller';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { PodControllerFormModel, PodNetworkFormModel } from 'app/typings';
import { Environments } from 'app/typings';
import { PodController } from 'app/typings/backend-api';
import {
  DaemonSetTypeMeta,
  DeploymentTypeMeta,
  StatefulSet,
  StatefulSetTypeMeta,
} from 'app/typings/raw-k8s';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';
import { SUBNET_NAME_NONE } from './pod-network-form/component';

@Component({
  selector: 'rc-pod-controller-form',
  templateUrl: './template.html',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerFormComponent
  extends BaseResourceFormGroupComponent<PodControllerFormModel>
  implements OnInit, OnDestroy, OnChanges {
  onDestroy$: Subject<void> = new Subject<void>();
  @Input()
  project: string;
  @Input()
  cluster: string;
  @Input()
  namespace: string;
  @Input() appName: string;
  @Input() hasPrefix: boolean;
  // 更新app的时候，podController的名称和类型限制从UI上不能修改
  @Input() isUpdate: boolean;
  environments: Environments;
  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  private subnetNameLabelKey: string;
  private subnetIpAddrsLabelKey: string;
  private calicoCidrLabelKey: string;

  kindOptions = [
    DeploymentTypeMeta.kind,
    DaemonSetTypeMeta.kind,
    StatefulSetTypeMeta.kind,
  ];
  constructor(injector: Injector) {
    super(injector);
    this.environments = this.injector.get(ENVIRONMENTS);
    this.subnetNameLabelKey = `subnet.${
      this.environments.label_base_domain
    }/name`;
    this.subnetIpAddrsLabelKey = `subnet.${
      this.environments.label_base_domain
    }/ipAddrs`;
    this.calicoCidrLabelKey = 'cni.projectcalico.org/ipv4pools';
  }

  ngOnInit() {
    super.ngOnInit();
    this.form
      .get('kind')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((value: string) => {
        if (value === DaemonSetTypeMeta.kind) {
          this.form.get('spec.replicas').disable();
        } else {
          this.form.get('spec.replicas').enable();
        }
      });
  }

  ngOnChanges({ hasPrefix }: SimpleChanges) {
    if (hasPrefix && hasPrefix.currentValue && this.form) {
      this.form.get('suffixName').enable({
        emitEvent: false,
      });
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control(
        {
          value: '',
        },
        [
          Validators.required,
          Validators.maxLength(64),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ],
      ),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });

    const specForm = this.fb.group({
      selector: this.fb.group({
        matchLabels: this.fb.control({}),
      }),
      strategy: this.fb.control({}),
      replicas: this.fb.control(1, [Validators.min(0), Validators.required]),
      template: this.fb.group({
        spec: this.fb.control({}), // PodSpec
        metadata: this.fb.group({
          labels: this.fb.control({}),
        }),
      }),
    });

    return this.fb.group({
      suffixName: this.fb.control(
        {
          value: '',
          disabled: !this.hasPrefix,
        },
        [
          Validators.required,
          Validators.maxLength(30),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ],
      ),
      kind: this.fb.control('', [Validators.required]),
      metadata: metadataForm,
      spec: specForm,
      network: this.fb.control({}),
    });
  }

  getDefaultFormModel() {
    return {};
  }

  adaptResourceModel(resource: PodController): PodControllerFormModel {
    const network: PodNetworkFormModel = {
      subnetName: SUBNET_NAME_NONE,
    };
    const annotations = get(resource, 'spec.template.metadata.annotations', {});
    if (annotations[this.subnetNameLabelKey]) {
      network.subnetName = annotations[this.subnetNameLabelKey];
    }
    if (annotations[this.subnetIpAddrsLabelKey]) {
      network.subnetIpAddrs = annotations[this.subnetIpAddrsLabelKey].split(
        ',',
      );
    }
    if (annotations[this.calicoCidrLabelKey]) {
      try {
        network.cidrBlocks = JSON.parse(annotations[this.calicoCidrLabelKey]);
      } catch (_e) {}
    }
    network.hostNetwork = get(
      resource,
      'spec.template.spec.hostNetwork',
      false,
    );
    const suffixName = resource.metadata ? resource.metadata.name : '';

    return {
      ...resource,
      suffixName,
      network,
    };
  }

  adaptFormModel(formModel: PodControllerFormModel): PodController {
    let resource = { ...formModel };
    resource.apiVersion = DeploymentTypeMeta.apiVersion;
    switch (resource.kind) {
      case StatefulSetTypeMeta.kind:
        resource.apiVersion = StatefulSetTypeMeta.apiVersion;
        break;
      default:
        resource.apiVersion = DeploymentTypeMeta.apiVersion;
        break;
    }
    // appName 作为前缀追加到 name 里
    if (this.hasPrefix) {
      resource.metadata.name = `${this.appName}-${resource.suffixName}`;
    }
    resource = omit(['prefixName', 'suffixName'], resource);
    if (resource.kind === PodControllerKindEnum.StatefulSet) {
      (resource as StatefulSet).spec.serviceName = resource.metadata.name;
    } else {
      delete (resource as any).spec.serviceName;
    }
    // spec.template.metadata.labels 添加service.alauda.io/name=deployment-name
    const labelKey = `service.${this.environments.label_base_domain}/name`;
    const labelPath = lensPath([
      'spec',
      'template',
      'metadata',
      'labels',
      labelKey,
    ]);
    const labelValue = `${resource.kind.toLowerCase()}-${
      resource.metadata.name
    }`;
    resource = set(labelPath, labelValue, resource);
    if (!resource.spec.template.metadata.annotations) {
      resource.spec.template.metadata.annotations = {};
    }

    // network
    if (resource.network) {
      resource.spec.template.spec.hostNetwork = !!resource.network.hostNetwork;
      if (!!resource.network.hostNetwork) {
        delete resource.spec.template.metadata.annotations[
          this.calicoCidrLabelKey
        ];
      } else {
        if (resource.network.subnetName !== SUBNET_NAME_NONE) {
          resource.spec.template.metadata.annotations[this.subnetNameLabelKey] =
            resource.network.subnetName;
          if (resource.network.subnetIpAddrs) {
            resource.spec.template.metadata.annotations[
              this.subnetIpAddrsLabelKey
            ] = resource.network.subnetIpAddrs.join(',');
          }
        } else if (resource.network.subnetName === SUBNET_NAME_NONE) {
          delete resource.spec.template.metadata.annotations[
            this.subnetNameLabelKey
          ];
          delete resource.spec.template.metadata.annotations[
            this.subnetIpAddrsLabelKey
          ];
        }
        if (resource.network.cidrBlocks) {
          resource.spec.template.metadata.annotations[
            this.calicoCidrLabelKey
          ] = JSON.stringify(resource.network.cidrBlocks);
        }
      }
      delete resource.network;
    }
    return resource;
  }

  get shouldDisableReplicas() {
    return this.form.get('kind').value === PodControllerKindEnum.DaemonSet;
  }

  get shouldHideStrategy() {
    return this.form.get('kind').value !== PodControllerKindEnum.Deployment;
  }
}
