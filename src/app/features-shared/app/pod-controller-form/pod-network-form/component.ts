import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { OnDestroy } from '@angular/core';

import { capitalize, get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { TranslateService } from 'app/translate/translate.service';
import { PodNetworkFormModel, PodNetworkTypeEnum } from 'app/typings';
export const SUBNET_NAME_NONE = '$$none';

@Component({
  selector: 'rc-pod-network-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodNetworkFormComponent
  extends BaseResourceFormGroupComponent<PodNetworkFormModel>
  implements OnInit, OnDestroy {
  @Input()
  cluster: string;
  clusterCniType: string;
  typeOptions: {
    name: string;
    value: PodNetworkTypeEnum;
  }[] = [];
  subnetOptions: { label: string; name: string; cidr_block?: string }[];
  subnetIps: string[];
  private onDestroy$ = new Subject<void>();

  async ngOnInit() {
    super.ngOnInit();
    await this.setupTypeOptions();
    this.getSubnetOptions();
    this.form
      .get('type')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe(value => {
        if (value === PodNetworkTypeEnum.host) {
          this.form.get('subnetName').disable({
            emitEvent: false,
          });
          this.form.get('cidrBlocks').disable({
            emitEvent: false,
          });
        } else {
          if (this.clusterCniType === 'calico') {
            this.form.get('cidrBlocks').enable({
              emitEvent: false,
            });
          } else if (this.clusterCniType === 'macvlan') {
            this.form.get('subnetName').enable({
              emitEvent: false,
            });
          }
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  constructor(
    injector: Injector,
    private translate: TranslateService,
    private k8sResourceService: K8sResourceService,
    private regionService: RegionService,
  ) {
    super(injector);
  }

  getResourceMergeStrategy() {
    return true;
  }

  getDefaultFormModel() {
    return {};
  }

  adaptFormModel(formModel: PodNetworkFormModel) {
    const { hostNetwork, type, subnetName, cidrBlocks } = formModel;
    return {
      hostNetwork: !this.clusterCniType
        ? !!hostNetwork
        : type === PodNetworkTypeEnum.host,
      subnetName,
      cidrBlocks,
    };
  }

  adaptResourceModel(resource: PodNetworkFormModel) {
    return {
      subnetName: SUBNET_NAME_NONE,
      ...resource,
      type: resource.hostNetwork
        ? PodNetworkTypeEnum.host
        : PodNetworkTypeEnum.cluster,
    };
  }

  createForm() {
    return this.fb.group({
      hostNetwork: [],
      type: [],
      subnetName: [],
      cidrBlocks: [],
    });
  }

  get shouldHideSubnetOptions() {
    return this.form.get('type').value === PodNetworkTypeEnum.host;
  }

  isSubnetSupported() {
    return (
      this.clusterCniType && ['macvlan', 'calico'].includes(this.clusterCniType)
    );
  }

  private setupTypeOptions() {
    return this.regionService
      .getCluster(this.cluster)
      .then((cluster: Cluster) => {
        this.clusterCniType = get(
          cluster,
          'attr.kubernetes.cni.type',
          '',
        ).toLowerCase();
        if (this.clusterCniType) {
          this.typeOptions.push({
            name: capitalize(this.clusterCniType),
            value: PodNetworkTypeEnum.cluster,
          });
        }

        this.typeOptions.push({
          name: PodNetworkTypeEnum.host,
          value: PodNetworkTypeEnum.host,
        });
        if (!this.isSubnetSupported()) {
          this.form.get('subnetName').disable();
          this.form.get('cidrBlocks').disable();
        } else {
          const hostNetwork = this.form.get('hostNetwork').value;
          if (!hostNetwork) {
            this.form.get('type').setValue(PodNetworkTypeEnum.cluster, {
              emitEvent: false,
            });
          }
        }
        this.cdr.markForCheck();
      });
  }

  private getSubnetOptions() {
    this.k8sResourceService
      .getK8sResources('subnets', {
        clusterName: this.cluster,
      })
      .then(res => {
        const options = res.map(item => {
          return {
            label:
              this.clusterCniType === 'calico'
                ? `${item.kubernetes.metadata.name}(${
                    item.kubernetes.spec.cidr_block
                  })`
                : item.kubernetes.metadata.name,
            cidr_block: item.kubernetes.spec.cidr_block,
            name: item.kubernetes.metadata.name,
          };
        });
        if (this.clusterCniType === 'macvlan') {
          this.subnetOptions = [
            {
              label: this.translate.get('option_none'),
              name: SUBNET_NAME_NONE,
            },
            ...options,
          ];
        } else {
          this.subnetOptions = options;
        }
        this.cdr.markForCheck();
      });
  }
}
