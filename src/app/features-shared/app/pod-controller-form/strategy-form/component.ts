import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Input, OnChanges, SimpleChanges } from '@angular/core';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  PodControllerKindEnum,
  PodControllerUpdateStrategyEnum,
} from 'app/features-shared/app/utils/pod-controller';
import { StrategyFormModel } from 'app/typings/k8s-form-model';

@Component({
  selector: 'rc-update-strategy-form',
  templateUrl: './template.html',
  styleUrls: ['../autoscaler-form/styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateStrategyFormComponent
  extends BaseResourceFormGroupComponent<StrategyFormModel>
  implements OnInit, OnChanges {
  @Input()
  podControllerKind: string;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnChanges({ podControllerKind }: SimpleChanges) {
    if (
      podControllerKind &&
      podControllerKind.currentValue &&
      podControllerKind.previousValue
    ) {
      this.updateStrategyType();
    }
  }

  getResourceMergeStrategy() {
    return true;
  }

  getDefaultFormModel() {
    return {};
  }

  adaptFormModel(formModel: StrategyFormModel) {
    if (
      this.podControllerKind &&
      this.podControllerKind === PodControllerKindEnum.Deployment
    ) {
      if (formModel && formModel.rollingUpdate) {
        const rollingUpdate = formModel.rollingUpdate;
        Object.keys(rollingUpdate).forEach(key => {
          rollingUpdate[key] = this.adaptNumbers(rollingUpdate[key]);
        });
      }
      return Object.assign({}, formModel, {
        type: 'RollingUpdate',
      }) as any;
    } else {
      // TODO: 针对三种PodController需要不同的 strategyForm
      return {
        type: 'OnDelete',
      };
    }
  }

  createForm() {
    return this.fb.group({
      type: [PodControllerUpdateStrategyEnum.RollingUpdate],
      rollingUpdate: this.fb.group({
        maxSurge: ['25%'],
        maxUnavailable: ['25%'],
      }),
    });
  }

  private adaptNumbers(value: string) {
    if (Number.isInteger(+value)) {
      return +value;
    } else {
      return value;
    }
  }

  private updateStrategyType() {
    if (!this.form) {
      return;
    }
    if (this.podControllerKind === PodControllerKindEnum.Deployment) {
      this.form
        .get('type')
        .setValue(PodControllerUpdateStrategyEnum.RollingUpdate);
    } else {
      this.form.get('type').setValue(PodControllerUpdateStrategyEnum.OnDelete);
    }
  }
}
