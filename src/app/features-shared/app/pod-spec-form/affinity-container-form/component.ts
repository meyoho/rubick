import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import jsyaml from 'js-yaml';
import { cloneDeep, get, isEqual, remove, uniqWith } from 'lodash-es';
import { combineLatest, Observable, Subject } from 'rxjs';
import { catchError, first, map, startWith } from 'rxjs/operators';

import {
  Affinity,
  PodAffinityTerm,
  PodSpecAffinity,
  WeightedPodAffinityTerm,
} from 'app/typings';
import { AffinityTerm, PodAffinityType } from 'app/typings/k8s-form-model';
import { getLabels } from '../../utils/affinity';
import { PodAffinityDialogComponent } from '../affinity-dialog/component';

interface AffinityItem {
  isAnti: boolean;
  val: AffinityTerm;
}

@Component({
  selector: 'rc-affinity-container-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AffinityContainerFormComponent),
      multi: true,
    },
  ],
})
export class AffinityContainerFormComponent
  implements OnInit, ControlValueAccessor {
  @Input()
  cluster: string;
  @Input()
  namespace: string;
  affinity: PodSpecAffinity = {
    podAntiAffinity: {
      requiredDuringSchedulingIgnoredDuringExecution: [],
      preferredDuringSchedulingIgnoredDuringExecution: [],
    },
    podAffinity: {
      requiredDuringSchedulingIgnoredDuringExecution: [],
      preferredDuringSchedulingIgnoredDuringExecution: [],
    },
  };

  columns = [
    'affinity',
    'type',
    'weight',
    'topologyKey',
    'labelSelector',
    'actions',
  ];

  expLabelsViewerOptions = {
    language: 'yaml',
    folding: false,
    minimap: { enabled: false },
    readOnly: true,
  };
  expLabelsViewerActionsConfig = {
    diffMode: false,
    clear: false,
    recover: false,
    import: false,
    copy: false,
    find: false,
    export: false,
    fullscreen: false,
    theme: false,
  };

  podAffinities$: Observable<AffinityItem[]>;
  affinitySubject$: Subject<Affinity> = new Subject();
  antiAffinitySubject$: Subject<Affinity> = new Subject();

  constructor(
    private dialogService: DialogService,
    private cdr: ChangeDetectorRef,
  ) {}

  onChange() {
    this.onCvaChange(this.affinity);
    this.updateaffinityTable();
  }

  onCvaChange = (_: PodSpecAffinity) => {};
  onCvaTouched = () => {};
  onValidatorChange = () => {};

  registerOnChange(fn: (value: PodSpecAffinity) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(resource: PodSpecAffinity) {
    if (resource) {
      this.affinity = resource;
    }
    if (this.affinity && this.affinity.podAffinity) {
      this.affinitySubject$.next(this.affinity.podAffinity);
    }
    if (this.affinity && this.affinity.podAntiAffinity) {
      this.antiAffinitySubject$.next(this.affinity.podAntiAffinity);
    }
    this.updateaffinityTable();
    this.cdr.markForCheck();
  }

  ngOnInit() {
    this.podAffinities$ = combineLatest(
      this.affinitySubject$,
      this.antiAffinitySubject$,
    )
      .pipe(
        map(([affinity, antiAffinity]) => [
          ...(affinity[PodAffinityType.required] || []).map(
            (val: Affinity) => ({
              val,
              isAnti: false,
            }),
          ),
          ...(affinity[PodAffinityType.preferred] || []).map(
            (val: Affinity) => ({
              val,
              isAnti: false,
            }),
          ),
          ...(antiAffinity[PodAffinityType.required] || []).map(
            (val: Affinity) => ({
              val,
              isAnti: true,
            }),
          ),
          ...(antiAffinity[PodAffinityType.preferred] || []).map(
            (val: Affinity) => ({
              val,
              isAnti: true,
            }),
          ),
        ]),
        startWith([]),
      )
      .pipe(catchError(() => []));
  }

  podAffinity(
    mode: string = 'add',
    rawData: {
      type: string;
      isAnti: boolean;
      val: AffinityTerm;
    },
  ) {
    const dialogRef = this.dialogService.open(PodAffinityDialogComponent, {
      size: DialogSize.Big,
      data: {
        mode,
        cluster: this.cluster,
        namespace: this.namespace,
        ...rawData,
      },
    });
    dialogRef.componentInstance.finish.pipe(first()).subscribe(res => {
      dialogRef.close();
      const affinityType = get(res.data as WeightedPodAffinityTerm, 'weight')
        ? PodAffinityType.preferred
        : PodAffinityType.required;
      const type = res.type === 'affinity' ? 'podAffinity' : 'podAntiAffinity';
      if (!this.affinity[type]) {
        this.affinity[type] = {
          [affinityType]: [],
        };
      } else if (!this.affinity[type][affinityType]) {
        this.affinity[type][affinityType] = [];
      }
      let val = cloneDeep(this.affinity[type][affinityType]); // 已存在的items
      const isExist = val.some((oldItem: AffinityTerm) =>
        isEqual(oldItem, res.data),
      );
      if (mode === 'add') {
        if (!isExist) {
          this.affinity[type][affinityType] = [...val, res.data];
        } else {
          return;
        }
      } else {
        val.some((item: AffinityTerm, index: number) => {
          // 找到item位置并插入新值
          if (isEqual(item, rawData.val)) {
            val[index] = res.data;
          }
        });
        if (isExist) {
          // 如果有值重复，去重
          val = uniqWith(val, isEqual);
        }
        this.affinity[type][affinityType] = val;
      }
      this.onChange();
    });
  }

  updateaffinityTable() {
    this.affinitySubject$.next(this.affinity.podAffinity || {});
    this.antiAffinitySubject$.next(this.affinity.podAntiAffinity || {});
  }

  private formToYaml(formModel: any) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {}
  }

  getExpLabels(rowData: AffinityItem) {
    return this.formToYaml(
      get(rowData.val as WeightedPodAffinityTerm, 'weight')
        ? (rowData.val as WeightedPodAffinityTerm).podAffinityTerm.labelSelector
            .matchExpressions
        : (rowData.val as PodAffinityTerm).labelSelector.matchExpressions,
    );
  }

  trackByFn(index: number) {
    return index;
  }

  getLabels(rowData: AffinityItem) {
    return getLabels(rowData.val);
  }

  affinityCanUpdate(rowData: AffinityItem) {
    return get(rowData.val as WeightedPodAffinityTerm, 'weight')
      ? !(rowData.val as WeightedPodAffinityTerm).podAffinityTerm.labelSelector
          .matchExpressions
      : !(rowData.val as PodAffinityTerm).labelSelector.matchExpressions;
  }

  removeAffinity(rowData: AffinityItem) {
    const val = cloneDeep(this.affinity);
    remove(
      val[rowData.isAnti ? 'podAntiAffinity' : 'podAffinity'][
        get(rowData.val as WeightedPodAffinityTerm, 'weight')
          ? PodAffinityType.preferred
          : PodAffinityType.required
      ],
      item => isEqual(rowData.val, item),
    );
    this.affinity = val;
    this.onChange();
  }
}
