import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { EventEmitter } from '@angular/core';

import { AffinityTerm } from 'app/typings/k8s-form-model';
import { PodAffinityFormComponent } from '../pod-affinity-form/component';

@Component({
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./styles.scss'],
})
export class PodAffinityDialogComponent implements AfterViewInit {
  @ViewChild(PodAffinityFormComponent)
  formComponent: PodAffinityFormComponent;
  // dialog输出数据类型
  finish = new EventEmitter<{
    type: 'affinity' | 'antiAffinity';
    data: AffinityTerm;
  }>();
  type = 'affinity';
  affinity: AffinityTerm = {
    topologyKey: 'kubernetes.io/hostname',
    labelSelector: {
      matchLabels: {},
    },
  };
  isUpdate: boolean;

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      namespace: string;
      cluster: string;
      mode: string;
      isAnti: boolean;
      val: AffinityTerm;
    },
    private dialogRef: DialogRef,
  ) {}

  async ngAfterViewInit() {
    const data = this.data;
    this.isUpdate = data.mode === 'update';
    if (data.mode === 'update') {
      this.affinity = data.val;
      this.type = data.isAnti ? 'antiAffinity' : 'affinity';
    }
  }

  confirm() {
    this.formComponent.triggerSubmit();
    if (this.formComponent.form.invalid) {
      return;
    }
    this.finish.emit({
      type: this.type as any,
      data: this.affinity,
    });
  }

  cancel() {
    this.dialogRef.close();
  }
}
