import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { FormArray } from '@angular/forms';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { PathParam } from 'ng-resource-form-util';
import { pipe, uniqBy } from 'ramda';
import { Observable } from 'rxjs';

import { AppShardService } from 'app/features-shared/app/app-shard.service';
import { ContainerFormComponent } from 'app/features-shared/app/pod-spec-form/container-form/component';
import { getVolumeFormModelUniqName } from 'app/features-shared/app/utils/volume';
import {
  getVolumeFormModelMountPath,
  getVolumeFormModelSubPath,
} from 'app/features-shared/app/utils/volume';
import { VolumeFormModel } from 'app/typings';
import { RcImageSelection } from 'app/typings/k8s-form-model';
import {
  ContainerFormModel,
  PodSpecFormModel,
} from 'app/typings/k8s-form-model';
import { Container, PodSpec, Volume, VolumeMount } from 'app/typings/raw-k8s';
import { adaptRepoNameToContainerName } from 'app_user/features/app/util';
import { getDefaultContainerConfig } from '../utils/pod-controller';

@Component({
  selector: 'rc-pod-spec-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodSpecFormComponent extends BaseResourceFormGroupComponent<
  PodSpecFormModel
> {
  private appShardService: AppShardService;
  @ViewChildren(ContainerFormComponent)
  containerForms: QueryList<ContainerFormComponent>;
  @Input()
  project: string;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  activeContainerIndex = 0;
  secrets$: Observable<{ name: string }[]>;

  constructor(injector: Injector) {
    super(injector);
    this.appShardService = this.injector.get(AppShardService);
  }

  createForm() {
    return this.fb.group({
      containers: this.fb.array([]),
      nodeSelector: this.fb.control([]),
      affinity: this.fb.control({}),
    });
  }

  pullSecretTrackFn(secret: { name: string }) {
    return secret.name;
  }

  adaptResourceModel(resource: PodSpec): PodSpecFormModel {
    // Makes sure user will not accidently remove the last container:
    if (!resource || !resource.containers) {
      resource = {
        ...resource,
        containers: [{ name: '', image: '' }],
      };
    }

    const _volumes = resource.volumes || [];
    const { containers, ...podSpec } = resource;
    const containerFormModels = containers.map((c: Container) => {
      const { volumeMounts, ...container } = c;
      const volumes: VolumeFormModel[] = (volumeMounts || [])
        .map((vm: VolumeMount) => {
          const volume = _volumes.find((v: Volume) => {
            return v.name === vm.name;
          });
          const { name, ...v } = volume;
          if (volume.hostPath) {
            return {
              name,
              hostPath: {
                ...v.hostPath,
                mountPath: vm.mountPath,
              },
            };
          } else if (volume.persistentVolumeClaim) {
            return {
              name,
              persistentVolumeClaim: {
                ...v.persistentVolumeClaim,
                mountPath: vm.mountPath,
                subPath: vm.subPath,
              },
            };
          } else if (volume.configMap) {
            let formModel: VolumeFormModel;
            if (v.configMap.items) {
              return {
                name,
                other: {
                  yaml: v,
                  mountPath: vm.mountPath,
                  subPath: vm.subPath,
                },
              };
            } else {
              formModel = {
                name,
                configMap: {
                  ...v.configMap,
                  mountPath: vm.mountPath,
                },
              };
              if (vm.subPath) {
                formModel.configMap.subPath = vm.subPath;
              }
            }
            return formModel;
          } else {
            return {
              name,
              other: {
                yaml: v,
                mountPath: vm.mountPath,
                subPath: vm.subPath,
              },
            };
          }
        })
        .reduce((_volumes: VolumeFormModel[], v: VolumeFormModel) => {
          if (v.configMap && !!v.configMap.subPath) {
            const volume = _volumes.find((volume: VolumeFormModel) => {
              return (
                volume.configMap &&
                volume.configMap.name === v.configMap.name &&
                volume.configMap.hasRefKeys
              );
            });
            if (volume) {
              volume.configMap.refKeys.push({
                key: v.configMap.subPath,
                path: v.configMap.mountPath,
              });
            } else {
              _volumes.push({
                name: v.name,
                configMap: {
                  name: v.configMap.name,
                  hasRefKeys: true,
                  refKeys: [
                    {
                      key: v.configMap.subPath,
                      path: v.configMap.mountPath,
                    },
                  ],
                },
              });
            }
          } else {
            _volumes.push(v);
          }
          return _volumes;
        }, []);
      return {
        ...container,
        volumes,
      } as ContainerFormModel;
    });
    return {
      ...podSpec,
      containers: containerFormModels,
    } as PodSpecFormModel;
  }

  adaptFormModel(formModel: PodSpecFormModel): PodSpec {
    const volumeFormModels: ContainerFormModel[] = formModel.containers.flatMap(
      (c: ContainerFormModel) => {
        return c.volumes || [];
      },
    );

    const volumes: Volume[] = pipe(uniqBy(v => getVolumeFormModelUniqName(v)))(
      volumeFormModels,
    ).map((v: VolumeFormModel) => {
      const volume: Volume = {
        name: getVolumeFormModelUniqName(v),
      };
      if (v.hostPath) {
        return Object.assign(volume, {
          hostPath: {
            path: v.hostPath.path,
          },
        });
      } else if (v.persistentVolumeClaim) {
        return Object.assign(volume, {
          persistentVolumeClaim: {
            claimName: v.persistentVolumeClaim.claimName,
          },
        });
      } else if (v.configMap) {
        return Object.assign(volume, {
          configMap: {
            name: v.configMap.name,
          },
        });
      } else if (v.other) {
        return Object.assign(volume, v.other.yaml);
      }
    }) as Volume[];
    const containers: Container[] = formModel.containers.map(
      (c: ContainerFormModel) => {
        const { volumes, ...container } = c;
        const volumeMounts: VolumeMount[] = volumes
          ? volumes
              .flatMap(vf => {
                if (
                  vf.configMap &&
                  vf.configMap.hasRefKeys &&
                  vf.configMap.refKeys
                ) {
                  return vf.configMap.refKeys.map(
                    (keyMap: { key: string; path: string }) => {
                      return {
                        name: vf.name,
                        configMap: {
                          name: vf.configMap.name,
                          mountPath: keyMap.path,
                          subPath: keyMap.key,
                        },
                      };
                    },
                  ) as VolumeFormModel[];
                } else {
                  return vf;
                }
              })
              .map((v: VolumeFormModel) => {
                const vm: VolumeMount = {
                  name: getVolumeFormModelUniqName(v),
                  mountPath: getVolumeFormModelMountPath(v),
                };
                const subPath = getVolumeFormModelSubPath(v);
                if (subPath) {
                  vm.subPath = subPath;
                }
                return vm;
              })
          : [];
        return {
          ...container,
          volumeMounts,
        };
      },
    );
    return {
      ...formModel,
      containers,
      volumes,
    };
  }

  getDefaultFormModel(): PodSpecFormModel {
    return {
      containers: [{ name: '', image: '' }],
      nodeSelector: {},
      affinity: {},
    };
  }

  async addContainer() {
    const containerForms = this.containerForms.toArray();
    containerForms[this.activeContainerIndex].triggerSubmit();
    if (!containerForms[this.activeContainerIndex].isValid) {
      return;
    }

    this.appShardService
      .selectImage({
        projectName: this.project,
      })
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.containersForm.push(
            this.fb.control(
              getDefaultContainerConfig({
                name: adaptRepoNameToContainerName(res.repository_name || ''),
                image: `${res.full_image_name}:${res.tag}`,
              }),
            ),
          );
          this.activeContainerIndex = this.containersForm.length - 1;
          this.cdr.markForCheck();
        }
      });
  }

  removeContainer(index: number) {
    this.containersForm.removeAt(index);
  }

  getOnFormArrayResizeFn() {
    return (path: PathParam) => this.getNewContainerFormControl(path);
  }

  get containersForm(): FormArray {
    return this.form.get('containers') as FormArray;
  }

  getExistedContainerNames(index: number) {
    return (this.containersForm.value as ContainerFormModel[])
      .filter((_value, _index: number) => {
        return _index !== index;
      })
      .map((c: ContainerFormModel) => c.name);
  }

  getNewContainerFormControl(path?: PathParam) {
    let index = this.containersForm.length;
    if (path) {
      index = +path[path.length - 1];
    }
    return this.fb.control({ name: `container-${index}`, image: '' });
  }

  trackByFn(index: number) {
    return index;
  }
}
