import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  SimpleChange,
} from '@angular/core';
import { OnChanges } from '@angular/core';
import { Validators } from '@angular/forms';

import { isEqual, remove } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { AppShardService } from 'app/features-shared/app/app-shard.service';
import { getNamesValidatorFn } from 'app/features-shared/app/utils/pod-controller';
import { ContainerFormModel } from 'app/typings/k8s-form-model';
import { RcImageSelection } from 'app/typings/k8s-form-model';
import { EXCLUDE_LOG_PATH_KEY, LOG_PATH_KEY } from 'app/typings/k8s-form-model';
import { Container, EnvVar } from 'app/typings/raw-k8s';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';

const DEFAULT_CONTAINER: Container = {
  name: '',
  image: '',
};

@Component({
  selector: 'rc-container-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerFormComponent
  extends BaseResourceFormGroupComponent<ContainerFormModel>
  implements OnChanges {
  @Input()
  project: string;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  @Input()
  existedContainerNames: string[];

  private appShardService: AppShardService;
  resourceNamePattern = K8S_RESOURCE_NAME_BASE;

  constructor(injector: Injector) {
    super(injector);
    this.appShardService = this.injector.get(AppShardService);
  }

  ngOnChanges({
    existedContainerNames,
  }: {
    existedContainerNames: SimpleChange;
  }) {
    if (this.form && existedContainerNames) {
      const {
        currentValue,
        previousValue,
        firstChange,
      } = existedContainerNames;

      if (
        !firstChange &&
        this.form.dirty &&
        !isEqual(currentValue, previousValue)
      ) {
        this.form.get('name').updateValueAndValidity();
        this.cdr.markForCheck();
      }
    }
  }

  createForm() {
    return this.fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
          getNamesValidatorFn(() => {
            return this.existedContainerNames;
          }),
        ],
      ],
      image: [],
      volumes: [],
      env: [],
      envFrom: [],
      args: [],
      command: [],
      resources: [],
      probes: [],
      logPaths: [],
      excludeLogPaths: [],
    });
  }

  getDefaultFormModel() {
    return DEFAULT_CONTAINER;
  }

  triggerSubmit() {
    this.ngFormGroupDirective.onSubmit(null);
  }

  get isValid() {
    return this.form.valid;
  }

  adaptFormModel(formModel: ContainerFormModel) {
    const resource = { ...formModel };
    if (resource.args && !resource.args.length) {
      delete resource.args;
    }
    // 健康检查
    const probes = resource.probes;
    if (probes.livenessProbe) {
      resource.livenessProbe = { ...probes.livenessProbe };
    } else {
      delete resource.livenessProbe;
    }
    if (probes.readinessProbe) {
      resource.readinessProbe = { ...probes.readinessProbe };
    } else {
      delete resource.readinessProbe;
    }
    delete resource.probes;
    // 日志文件
    if (resource.logPaths) {
      resource.env = this.getReplacedEnvVarsWithLogPaths(
        resource.env,
        LOG_PATH_KEY,
        resource.logPaths,
      );
      delete resource.logPaths;
    }
    if (resource.excludeLogPaths) {
      resource.env = this.getReplacedEnvVarsWithLogPaths(
        resource.env,
        EXCLUDE_LOG_PATH_KEY,
        resource.excludeLogPaths,
      );
      delete resource.excludeLogPaths;
    }
    return resource;
  }

  adaptResourceModel(resource: ContainerFormModel) {
    const formModel = { ...resource };
    // 健康检查
    formModel.probes = {
      livenessProbe: formModel.livenessProbe
        ? { ...formModel.livenessProbe }
        : null,
      readinessProbe: formModel.readinessProbe
        ? { ...formModel.readinessProbe }
        : null,
    };
    // 日志文件
    if (formModel.env && formModel.env.length) {
      const logPathEnvVar = formModel.env.find((item: EnvVar) => {
        return !item.valueFrom && item.name === LOG_PATH_KEY;
      });
      const excludeLogPathEnvVar = formModel.env.find((item: EnvVar) => {
        return !item.valueFrom && item.name === EXCLUDE_LOG_PATH_KEY;
      });
      if (logPathEnvVar && logPathEnvVar.value) {
        formModel.logPaths = logPathEnvVar.value.split(',');
      }
      if (excludeLogPathEnvVar && excludeLogPathEnvVar.value) {
        formModel.excludeLogPaths = excludeLogPathEnvVar.value.split(',');
      }
    }
    return formModel;
  }

  async selectImage() {
    this.appShardService
      .selectImage({
        projectName: this.project,
      })
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.form.get('image').setValue(`${res.full_image_name}:${res.tag}`);
        }
      });
  }

  getRequiredValidator() {
    return [Validators.required];
  }

  private getReplacedEnvVarsWithLogPaths(
    envs: EnvVar[],
    name: string,
    paths: string[],
  ) {
    if (!envs || !envs.length) {
      if (paths.length) {
        return [
          {
            name,
            value: paths.join(','),
          },
        ];
      } else {
        return [];
      }
    } else {
      const target = envs.find((item: EnvVar) => {
        return item.name === name;
      });
      if (paths.length) {
        const newEnvVar = {
          name,
          value: paths.join(','),
        };
        if (target) {
          target.value = paths.join(',');
        } else {
          envs.push(newEnvVar);
        }
      } else {
        if (target) {
          remove(envs, env => env.name === name && !env.valueFrom);
        }
      }
      return envs;
    }
  }
}
