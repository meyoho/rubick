import { OptionComponent } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  FormControl,
  FormGroup,
  ValidatorFn,
} from '@angular/forms';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { BehaviorSubject, Observable } from 'rxjs';
import {
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import {
  ENV_VAR_SOURCE_TYPE_TO_KIND,
  FIELD_SUPPORT_TYPES,
  getEnvVarSource,
  getEnvVarSourceType,
  isEnvVarSourceMode,
  isEnvVarSourceSupported,
  KIND_TO_ENV_VAR_SOURCE_TYPE,
  SUPPORT_ENV_VAR_SOURCE_KIND,
  SupportedEnvVarSourceKind,
  SupportedEnvVarSourceType,
} from 'app/features-shared/app/utils/env-var';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { TranslateService } from 'app/translate/translate.service';
import { ConfigMap, EnvVar, Secret } from 'app/typings/raw-k8s';
import { K8S_ENV_VARIABLE_NAME } from 'app/utils/patterns';

interface EnvRefObj {
  kind?: SupportedEnvVarSourceKind; // One of Secret, configMap
  name?: string;
}

interface EnvVarFormModel extends EnvVar {
  refObj?: EnvRefObj;
  refObjKey?: string;
}

@Component({
  selector: 'rc-env-form',
  templateUrl: './template.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvFormComponent
  extends BaseResourceFormArrayComponent<EnvVar, EnvVarFormModel>
  implements OnInit {
  @Input()
  cluster: string;

  @Input()
  set namespace(namespace: string) {
    this.namespaceChanged.next(namespace);
  }

  configMaps$: Observable<ConfigMap[]>;
  secrets$: Observable<Secret[]>;
  errorSource$ = new BehaviorSubject(false);

  ENV_VAR_SOURCE_TYPES = Object.values(SUPPORT_ENV_VAR_SOURCE_KIND);
  FIELD_SUPPORT_TYPES = FIELD_SUPPORT_TYPES;
  K8S_ENV_VARIABLE_NAME = K8S_ENV_VARIABLE_NAME;
  SUPPORT_ENV_VAR_SOURCE_KIND = SUPPORT_ENV_VAR_SOURCE_KIND;

  private namespaceChanged = new BehaviorSubject<string>(this.namespace);

  constructor(
    private k8sResourceService: K8sResourceService,
    protected translateService: TranslateService,
    injector: Injector,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();

    this.configMaps$ = this.namespaceChanged.pipe(
      filter(n => !!n),
      switchMap(namespace =>
        this.getNamespaceConfigs$(namespace, 'configmaps'),
      ),
      publishReplay(1),
      refCount(),
    );

    this.secrets$ = this.namespaceChanged.pipe(
      filter(n => !!n),
      switchMap(namespace => this.getNamespaceConfigs$(namespace, 'secrets')),
      publishReplay(1),
      refCount(),
    );
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel() {
    return [] as any;
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  adaptResourceModel(envVars: EnvVar[]) {
    if (!envVars || envVars.length === 0) {
      envVars = this.getDefaultFormModel();
    }
    // Fill in keyRefObj when applied:
    return envVars.map((envVar: EnvVarFormModel) => {
      if (isEnvVarSourceSupported(envVar)) {
        const envVarSource = getEnvVarSource(envVar);
        const kind =
          ENV_VAR_SOURCE_TYPE_TO_KIND[getEnvVarSourceType(envVar.valueFrom)];
        envVar = { ...envVar };
        const overrideObj = {
          kind: kind,
        };
        // Different value access when kind === fieldRef/others
        if (kind === SUPPORT_ENV_VAR_SOURCE_KIND.field) {
          Object.assign(overrideObj, {
            name: envVar.valueFrom.fieldRef.fieldPath,
          });
        } else {
          Object.assign(overrideObj, { name: envVarSource.name });
        }
        Object.assign(envVar, {
          refObj: overrideObj,
          refObjKey: (envVarSource as { key?: string }).key,
        });
      }

      return envVar;
    });
  }

  adaptFormModel(envVars: EnvVarFormModel[]): EnvVar[] {
    if (envVars) {
      envVars = envVars.filter(
        ({ name, value, valueFrom }) => name || value || valueFrom,
      );
    }

    return envVars.map(envVar => {
      if (envVar.refObj && envVar.refObj.kind) {
        const refKind: SupportedEnvVarSourceType =
          KIND_TO_ENV_VAR_SOURCE_TYPE[envVar.refObj.kind];
        const adaptSource: {
          fieldPath?: string;
          apiVersion?: string;
        } = {};
        if (refKind === KIND_TO_ENV_VAR_SOURCE_TYPE.FieldRef) {
          Object.assign(adaptSource, {
            fieldPath: envVar.refObj.name,
            apiVersion: 'v1',
          });
        } else {
          Object.assign(adaptSource, {
            name: envVar.refObj.name,
            key: envVar.refObjKey,
          });
        }
        const envVarSource = {
          [refKind]: adaptSource,
        };
        envVar = {
          name: envVar.name,
          valueFrom: envVarSource,
        };
      } else {
        envVar = {
          name: envVar.name,
          value: envVar.value,
        };
      }
      return envVar;
    });
  }

  envVarViewMode(envVar: EnvVar): 'value' | 'valueFrom' | 'yaml' {
    if (!isEnvVarSourceMode(envVar)) {
      return 'value';
    } else if (isEnvVarSourceSupported(envVar)) {
      return 'valueFrom';
    } else {
      return 'yaml';
    }
  }

  // Overwrite add so that we could have different types of controls
  add(index = this.length, withRef = false) {
    const control = this.createNewControl();
    if (withRef) {
      control.get('valueFrom').reset({});
    } else {
      control.get('refObj.kind').reset('');
    }
    this.form.insert(index, control);
    this.cdr.markForCheck();
  }

  getRefObjName(obj: ConfigMap | Secret): string {
    return obj.metadata.name;
  }

  getRefObjLabel(refObj: ConfigMap | Secret) {
    return refObj && refObj.kind ? refObj.metadata.name : '';
  }

  getRefPlaceHolderFromControl(control: FormGroup): string {
    const kind = (control.controls.refObj as FormGroup).controls['kind'].value;
    const kindTranslated = this.translateService.get(kind);
    return this.translateService.get('choose_ref_name', {
      refKind: kindTranslated,
    });
  }

  getRefObjKind(control: FormGroup): SupportedEnvVarSourceKind {
    return (control.controls['refObj'] as FormGroup).controls.kind.value;
  }

  isFieldKind(control: FormGroup) {
    return this.getRefObjKind(control) === SUPPORT_ENV_VAR_SOURCE_KIND.field;
  }

  filterFn = (filterString: string, option: OptionComponent) => {
    return option.value.includes(filterString);
  };

  getRefObjKeys(control: FormGroup): Observable<string[]> {
    const refObjControl = control.get('refObj');
    return refObjControl.valueChanges.pipe(
      startWith(refObjControl.value),
      switchMap(refObj => {
        const objs$ =
          refObj.kind === 'Secret' ? this.secrets$ : this.configMaps$;
        return objs$.pipe(
          map(objs => {
            const selectObj = objs.find(
              obj => obj.metadata.name === refObj.name,
            );
            return selectObj && selectObj.data
              ? Object.keys(selectObj.data)
              : [];
          }),
        );
      }),
      tap(keys => {
        const keyControl = control.get('refObjKey');
        const enableKeyControl = keys && keys.length > 0;
        if (enableKeyControl) {
          keyControl.enable({ emitEvent: false });
        } else {
          keyControl.disable({ emitEvent: false });
        }
      }),
    );
  }

  //clear both refName and refObjKey
  kindChange(control: FormGroup) {
    control.get('refObj').patchValue({
      name: '',
    });
    control.get('refObjKey').setValue('');
    if (this.getRefObjKind(control) !== SUPPORT_ENV_VAR_SOURCE_KIND.field) {
      control.get('refObjKey').disable();
    }
  }

  valueFromChange(control: AbstractControl) {
    control.get('refObjKey').setValue('');
  }

  rowBackgroundColorFn(row: FormControl) {
    if (row.invalid) {
      return '#f9f2f4';
    } else {
      return '';
    }
  }

  private getOtherKeys(index: number) {
    return this.formModel
      .map(({ name }) => name)
      .filter((name, ind) => {
        return ind !== index && !!name;
      });
  }

  private createNewControl() {
    const valueRequiredValidator: ValidatorFn = (control: AbstractControl) => {
      const { name, refObj, refObjKey, valueFrom } = control.value;
      if (
        name &&
        !!valueFrom &&
        !!refObj &&
        !refObjKey &&
        refObj.kind !== SUPPORT_ENV_VAR_SOURCE_KIND.field
      ) {
        return {
          refObjKeyMissing: true,
        };
      } else {
        return null;
      }
    };

    const fieldNameRequiredValidator: ValidatorFn = control => {
      const { name, refObj, valueFrom } = control.value;
      if (
        name &&
        !!valueFrom &&
        !!refObj &&
        refObj.kind === SUPPORT_ENV_VAR_SOURCE_KIND.field &&
        !refObj.name
      ) {
        return {
          fieldNameMissing: true,
        };
      } else {
        return null;
      }
    };

    const missingKeyValidator: ValidatorFn = control => {
      const { name, value, refObj } = control.value;

      if (!name && (value || (refObj && refObj.name))) {
        //do things
        this.errorSource$.next(true);
        return {
          keyIsMissing: true,
        };
      } else {
        if (this.errorSource$) {
          this.errorSource$.next(false);
        }
        return null;
      }
    };

    const duplicateKeyValidator: ValidatorFn = control => {
      const index = this.form.controls.indexOf(control);
      const previousKeys = this.getOtherKeys(index);

      const { name } = control.value;

      if (previousKeys.includes(name)) {
        return {
          duplicateKey: name,
        };
      } else {
        return null;
      }
    };

    return this.fb.group(
      {
        name: this.fb.control(''),
        value: this.fb.control(''),
        valueFrom: [],

        // The followings are view only controls and will be filtered out later
        refObj: this.fb.group({
          kind: this.fb.control(SUPPORT_ENV_VAR_SOURCE_KIND.configMapKey),
          name: this.fb.control(''),
        }),
        refObjKey: [],
      },
      {
        validator: [
          valueRequiredValidator,
          fieldNameRequiredValidator,
          missingKeyValidator,
          duplicateKeyValidator,
        ],
      },
    );
  }

  private getNamespaceConfigs$(namespace: string, resourceType: string) {
    return this.k8sResourceService
      .getK8sResources(resourceType, {
        namespace: namespace,
        clusterName: this.cluster,
      })
      .then(objs => {
        return objs.map(obj => obj.kubernetes);
      });
  }
}
