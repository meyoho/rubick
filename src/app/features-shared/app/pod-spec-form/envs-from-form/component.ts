import { OptionComponent } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { OnInit } from '@angular/core';
import { FormControl } from '@angular/forms';

import { BaseResourceFormComponent } from 'ng-resource-form-util';
import { BehaviorSubject, Observable } from 'rxjs';
import { filter, publishReplay, refCount, switchMap } from 'rxjs/operators';

import {
  adaptEnvFromSourceResource,
  EnvFromSourceFormModel,
  KIND_TO_SUPPORTED_ENV_FROM_TYPES,
  SupportedEnvFromSourceKind,
} from 'app/features-shared/app/utils/env-from';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { TranslateService } from 'app/translate/translate.service';
import { ConfigMap, EnvFromSource, Secret } from 'app/typings/raw-k8s';

@Component({
  selector: 'rc-env-from-form',
  templateUrl: './template.html',
  styles: [
    `
      :host {
        display: block;
        flex: 1;
      }
    `,
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EnvFromFormComponent
  extends BaseResourceFormComponent<EnvFromSource[], EnvFromSourceFormModel[]>
  implements OnInit {
  @Input()
  cluster: string;

  @Input()
  set namespace(namespace: string) {
    this.namespaceChanged.next(namespace);
  }

  configMaps$: Observable<ConfigMap[]>;
  secrets$: Observable<Secret[]>;
  private namespaceChanged = new BehaviorSubject<string>(this.namespace);

  constructor(
    injector: Injector,
    private translateService: TranslateService,
    private k8sResourceService: K8sResourceService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.configMaps$ = this.namespaceChanged.pipe(
      filter(n => !!n),
      switchMap(namespace =>
        this.getNamespaceConfigs$(namespace, 'configmaps'),
      ),
      publishReplay(1),
      refCount(),
    );

    this.secrets$ = this.namespaceChanged.pipe(
      filter(n => !!n),
      switchMap(namespace => this.getNamespaceConfigs$(namespace, 'secrets')),
      publishReplay(1),
      refCount(),
    );
  }

  createForm() {
    return new FormControl([]);
  }

  getResourceMergeStrategy() {
    return false;
  }

  adaptResourceModel(
    envFromSources: EnvFromSource[],
  ): EnvFromSourceFormModel[] {
    if (!envFromSources || envFromSources.length === 0) {
      return this.getDefaultFormModel();
    }

    return adaptEnvFromSourceResource(envFromSources);
  }

  adaptFormModel(
    envFromSourceFormModels: EnvFromSourceFormModel[],
  ): EnvFromSource[] {
    return envFromSourceFormModels.map(({ kind, name }) => {
      const refType = KIND_TO_SUPPORTED_ENV_FROM_TYPES[kind];

      return {
        [refType]: {
          name,
        },
      };
    });
  }

  getDefaultFormModel(): EnvFromSourceFormModel[] {
    return [];
  }

  getRefObj(obj: ConfigMap | Secret): EnvFromSourceFormModel {
    return {
      name: obj.metadata.name,
      kind: obj.kind as SupportedEnvFromSourceKind,
    };
  }

  getRefObjLabel(refObj: ConfigMap | Secret) {
    return refObj && refObj.kind
      ? this.translateService.get(refObj.kind.toLowerCase()) +
          ': ' +
          refObj.metadata.name
      : '';
  }

  trackFn = (refObj: EnvFromSourceFormModel) => {
    return refObj && refObj.kind
      ? refObj.kind.toLowerCase() + ': ' + refObj.name
      : '';
  };

  filterFn = (filterString: string, option: OptionComponent) => {
    return option.value.name.includes(filterString);
  };

  private getNamespaceConfigs$(namespace: string, resourceType: string) {
    return this.k8sResourceService
      .getK8sResources(resourceType, {
        namespace: namespace,
        clusterName: this.cluster,
      })
      .then(objs => {
        return objs.map(obj => obj.kubernetes);
      });
  }
}
