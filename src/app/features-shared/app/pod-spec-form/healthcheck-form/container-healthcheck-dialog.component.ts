import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { Inject } from '@angular/core';
import { NgForm } from '@angular/forms';

@Component({
  selector: 'rc-container-fields-healthcheck-dialog',
  templateUrl: './container-healthcheck-dialog.component.html',
  styleUrls: ['./container-healthcheck-dialog.component.scss'],
})

/*
* init value
* {
*   protocol: 'HTTP',  // 'HTTP', 'TCP', 'EXEC'
    initialDelaySeconds: 100,
    periodSeconds: 60,
    timeoutSeconds: 20,
    successThreshold: 0,
    failureThreshold: 0,
    scheme: 'test-scheme',
    port: 80,
    path: '/',
    headers: [{name: 'test-name', value: 'test-value'}],
    commands: [command: 'test-commands'],
* }
* output value format is same with init value
*
* */
export class HealthcheckDialogComponent implements OnInit, OnDestroy {
  @Output()
  finish = new EventEmitter<any>();
  @ViewChild('form')
  form: NgForm;
  submitting = false;
  submitted = false;
  isUpdate = false;
  // errorMappers: any = {};
  protocolOptions: string[];
  schemeOptions: string[];
  initHeaders: any[] = [];
  model = {
    protocol: 'HTTP',
    initialDelaySeconds: 300,
    periodSeconds: 60,
    timeoutSeconds: 30,
    successThreshold: 0,
    failureThreshold: 5,
    scheme: 'HTTP',
    port: 80,
    path: '/',
    headers: this.initHeaders,
    commands: [''],
  };

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      formModel: any;
      type: string;
    },
  ) {}

  ngOnInit() {
    this.protocolOptions = ['HTTP', 'TCP', 'EXEC'];
    this.schemeOptions = ['HTTP', 'HTTPS'];
    this.initValue(this.dialogData.formModel);
    if (['livenessProbe', 'liveness'].includes(this.dialogData.type)) {
      this.model.successThreshold = 1;
    }
  }
  ngOnDestroy() {}

  initValue(healthcheck: any) {
    if (healthcheck) {
      Object.assign(this.model, healthcheck, {
        headers: (healthcheck.headers || []).reduce(
          (
            prev: { [key: string]: string },
            curr: { name: string; value: string },
          ) => ({ ...prev, [curr.name]: curr.value }),
          {},
        ),
        commands: (healthcheck.commands || []).map(
          (item: { command: string }) => item.command,
        ),
      });
      this.isUpdate = true;
    }
  }

  onSubmit() {
    this.submitted = true;
  }

  private complete(res?: any) {
    this.finish.next(res);
    this.finish.complete();
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.complete({
      ...this.model,
      headers: Object.keys(this.model.headers).map(key => ({
        name: key,
        value: this.model.headers[key],
      })),
      commands: this.model.commands.map(command => ({ command })),
    });
  }
  cancel() {
    this.complete();
  }

  trackByIndex(index: number) {
    return index;
  }
}
