import { OptionComponent } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormControl } from '@angular/forms';

import { BaseResourceFormComponent } from 'ng-resource-form-util';
import { pipe, sortBy, uniqBy } from 'ramda';
import { Subject } from 'rxjs';
import { startWith, takeUntil } from 'rxjs/operators';

import {
  Label,
  NodeLabels,
  RegionService,
} from 'app/services/api/region.service';

interface KeyValueMap {
  [key: string]: string;
}

@Component({
  selector: 'rc-node-selector-form',
  templateUrl: 'template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NodeSelectorComponent
  extends BaseResourceFormComponent<KeyValueMap, Label[]>
  implements OnInit, OnDestroy {
  @Input()
  cluster: string;
  labels: Label[];
  nodeLabels: NodeLabels;
  onDestroy$ = new Subject<void>();
  matchedNodeCount: number;

  constructor(injector: Injector, private regionService: RegionService) {
    super(injector);
  }

  get selectedLabels() {
    return this.form.value;
  }

  ngOnInit() {
    super.ngOnInit();
    this.getClusterNodeLabels();
    this.form.valueChanges
      .pipe(
        startWith(this.form.value),
        takeUntil(this.onDestroy$),
      )
      .subscribe((labels: Label[]) => {
        this.checkSelectLabels(labels);
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  getResourceMergeStrategy() {
    return false;
  }

  createForm() {
    return new FormControl([]);
  }

  getDefaultFormModel(): Label[] {
    return [];
  }

  adaptFormModel(formModel: Label[]): KeyValueMap {
    const resource = {};
    formModel.forEach((keyValue: Label) => {
      resource[keyValue.key] = keyValue.value;
    });
    return resource;
  }

  adaptResourceModel(resource: KeyValueMap): Label[] {
    return Object.keys(resource).map((key: string) => {
      return {
        key,
        value: resource[key],
      };
    });
  }

  shouldDisableOption(label: Label) {
    const selectedLabels = this.form.value;
    return (
      selectedLabels &&
      selectedLabels.find((l: Label) => {
        return label.key === l.key && label.value !== l.value;
      })
    );
  }

  filterFn(filter: string, option: OptionComponent) {
    const label = option.value;
    return label.key.includes(filter) || label.value.includes(filter);
  }

  trackByFn(index: number) {
    return index;
  }

  getLabelDisplay(label: Label) {
    return `${label.key}:${label.value}`;
  }

  private async getClusterNodeLabels() {
    try {
      this.nodeLabels = await this.regionService.getRegionLabelsV2(
        this.cluster,
      );
      this.labels = pipe(
        uniqBy((label: Label) => {
          return `${label.key}:${label.value}`;
        }),
        sortBy((label: Label) => label.key),
      )(Object.values(this.nodeLabels).flat());
      if (this.selectedLabels) {
        this.checkSelectLabels(this.selectedLabels);
      }
      this.cdr.markForCheck();
    } catch (_e) {
      this.labels = [];
    }
  }

  private checkSelectLabels(labels: Label[]) {
    if (labels && labels.length) {
      const nodes = this.regionService.checkNodeByLabels(
        this.nodeLabels,
        labels,
      );
      this.matchedNodeCount = nodes.length;
    }
  }
}
