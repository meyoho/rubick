import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { get } from 'lodash-es';
import { BaseResourceFormComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AppService, WorkLoads } from 'app/services/api/app.service';
import {
  DaemonSet,
  Deployment,
  PodAffinityTerm,
  StatefulSet,
  WeightedPodAffinityTerm,
} from 'app/typings';
import { PodAffinityFormModel } from 'app/typings/k8s-form-model';
import { AffinityTerm } from 'app/typings/k8s-form-model';

@Component({
  selector: 'rc-pod-affinity-form',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodAffinityFormComponent
  extends BaseResourceFormComponent<AffinityTerm, PodAffinityFormModel>
  implements OnInit, OnDestroy {
  @Input()
  type: string;
  @Input()
  cluster: string;
  @Input()
  namespace: string;
  @Input()
  mode: string;
  method = 'basic';

  workloads: WorkLoads = {};
  workload: Deployment | StatefulSet | DaemonSet;

  onDestroy$ = new Subject<void>();

  constructor(injector: Injector, private appService: AppService) {
    super(injector);
  }

  getResourceMergeStrategy() {
    return true;
  }

  get isUpdate() {
    return this.mode === 'update';
  }

  get isAdvanced() {
    return this.method === 'advanced';
  }

  ngOnInit() {
    super.ngOnInit();
    if (this.mode !== 'update') {
      this.appService
        .getWorkloads({
          cluster: this.cluster,
          namespace: this.namespace,
        })
        .then(res => {
          this.workloads = res;
        });
    } else {
      this.method = 'advanced';
      this.methodChange('advanced');
    }

    this.form
      .get('type')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((type: string) => {
        type === 'required'
          ? this.form.get('weight').disable({ emitEvent: false })
          : this.form.get('weight').enable({ emitEvent: false });
      });

    this.form
      .get('workload')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((workload: Deployment | StatefulSet | DaemonSet) => {
        this.workload = workload;
      });
  }

  methodChange(method: string) {
    if (method === 'advanced') {
      this.form.get('type').value === 'required'
        ? this.form.get('weight').disable({ emitEvent: false })
        : this.form.get('weight').enable({ emitEvent: false });
      this.form.get('weight').setValue(1);
      this.form.get('workload').disable({ emitEvent: false });
    } else {
      this.form.get('weight').disable({ emitEvent: false });
      this.form.get('workload').enable({ emitEvent: false });
    }
    if (this.mode !== 'update') {
      // TO DO: 可能不是最优解
      // 仅在添加时，切换模式重新验证一下表单以更新errorsTooltip状态
      this.ngFormGroupDirective.onSubmit(null);
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createForm() {
    return this.fb.group({
      workload: this.fb.control([]),
      type: this.fb.control('', [Validators.required]), // required, preferred
      weight: this.fb.control('', [
        Validators.min(1),
        Validators.max(100),
        Validators.required,
      ]), // only preferred
      topologyKey: this.fb.control('', [Validators.required]),
      labelSelector: this.fb.group({
        matchLabels: this.fb.control({}),
      }),
    });
  }

  getDefaultFormModel() {
    return {
      type: 'required',
      weight: 1,
      topologyKey: 'kubernetes.io/hostname',
      labelSelector: { matchLabels: {} },
    };
  }

  adaptResourceModel(resource: AffinityTerm): PodAffinityFormModel {
    if (resource) {
      const weight = (resource as WeightedPodAffinityTerm).weight;
      const topologyKey = weight
        ? (resource as WeightedPodAffinityTerm).podAffinityTerm.topologyKey
        : (resource as PodAffinityTerm).topologyKey;
      const labelSelector = weight
        ? (resource as WeightedPodAffinityTerm).podAffinityTerm.labelSelector
        : (resource as PodAffinityTerm).labelSelector;
      return {
        type: weight ? 'preferred' : 'required',
        weight: weight,
        topologyKey,
        labelSelector: { ...labelSelector },
      };
    } else {
      // 添加，初始无数据
      return {} as PodAffinityFormModel;
    }
  }

  adaptFormModel(formModel: PodAffinityFormModel): AffinityTerm {
    if (this.mode === 'add' && !this.isAdvanced) {
      // 添加一个，基本模式，直接根据workload的labels拼接。
      return {
        topologyKey: 'kubernetes.io/hostname',
        labelSelector: {
          matchLabels: get(this.workload, 'spec.template.metadata.labels', {}),
        },
      } as PodAffinityTerm;
    } else {
      // 高级模式 或 更新，转换formModel为resource类型
      const resource: any = {};
      const podAffinityTerm: PodAffinityTerm = {
        topologyKey: formModel.topologyKey,
        labelSelector: formModel.labelSelector,
      };
      if (formModel.type === 'required') {
        Object.assign(resource, podAffinityTerm);
        return resource as PodAffinityTerm;
      } else {
        Object.assign(resource, {
          podAffinityTerm,
          weight: formModel.weight,
        });
        return resource as WeightedPodAffinityTerm;
      }
    }
  }

  triggerSubmit() {
    this.ngFormGroupDirective.onSubmit(null);
  }

  trackByFn(index: number) {
    return index;
  }

  keys(obj: Object) {
    return Object.keys(obj);
  }
}
