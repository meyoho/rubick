import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import {
  AbstractControl,
  AsyncValidatorFn,
  FormGroup,
  ValidationErrors,
  Validators,
} from '@angular/forms';

import { isEmpty } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { TranslateService } from 'app/translate/translate.service';
import { ResourceRequirementsFormModel } from 'app/typings';
import {
  LimitRange,
  ResourceRequirements,
  StringMap,
} from 'app/typings/raw-k8s';
import { ErrorMapper } from 'app2/shared/types';

const AVAILABLE_RESOURCE_TYPES = ['cpu', 'memory'];
type resourceType = 'cpu' | 'memory';
type resourceKind = 'requests' | 'limits';
interface LimitRangeConfig extends StringMap {
  cpu: string;
  memory: string;
}

interface ResourceControl {
  value: string;
  unit: string;
}

@Component({
  selector: 'rc-resources-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourcesFormComponent extends BaseResourceFormGroupComponent {
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  private max: LimitRangeConfig;
  private limitrangeCached: boolean;
  private defaultRequest: LimitRangeConfig;

  errorMapper: ErrorMapper;
  AVAILABLE_RESOURCE_TYPES = AVAILABLE_RESOURCE_TYPES;
  resourceUnits = {
    cpu: ['m', 'c'],
    memory: ['M', 'G'],
  };
  CUSTOM_CPU_OPTIONS = [
    {
      label: '1c',
      value: '1',
    },
    {
      label: '2c',
      value: '2',
    },
    {
      label: '4c',
      value: '4',
    },
    {
      label: '6c',
      value: '6',
    },
  ];

  constructor(
    injector: Injector,
    private translate: TranslateService,
    private k8sResourceService: K8sResourceService,
  ) {
    super(injector);
  }

  getResourceMergeStrategy() {
    return true;
  }

  get limits() {
    return this.form.get('limits');
  }

  getResourceByType(type: resourceType) {
    return this.limits.get(type) as FormGroup;
  }

  createForm() {
    const cpuAsyncValidatorFn = this.getAsyncValidatorFn('cpu');
    const memoryAsyncValidatorFn = this.getAsyncValidatorFn('memory');

    return this.fb.group({
      limits: this.fb.group({
        cpu: this.fb.group(
          {
            value: ['', [Validators.required]],
            unit: [],
          },
          {
            asyncValidator: cpuAsyncValidatorFn,
          },
        ),
        memory: this.fb.group(
          {
            value: ['', Validators.required],
            unit: [],
          },
          {
            asyncValidator: memoryAsyncValidatorFn,
          },
        ),
      }),
    });
  }

  private getAsyncValidatorFn(type: resourceType): AsyncValidatorFn {
    return (control: AbstractControl) =>
      this.getAsyncValidatorFnRes(control, type);
  }

  private getAsyncValidatorFnRes(
    control: AbstractControl,
    type: resourceType,
  ): Promise<ValidationErrors | null> {
    if (!this.limitrangeCached && this.cluster) {
      return this.k8sResourceService
        .getK8sReourceDetail('limitranges', {
          name: 'default',
          clusterName: this.cluster,
          namespace: this.namespace,
        })
        .then((res: { kubernetes: LimitRange; resource_actions: string[] }) => {
          this.cacheLimitRange(res.kubernetes);
          return this.validateControl(control, type);
        })
        .catch(_err => null);
    } else {
      return Promise.resolve(this.validateControl(control, type));
    }
  }

  private validateControl(control: AbstractControl, type: resourceType) {
    if (!this.defaultRequest || !control.value || !control.value.value) {
      return null;
    } else {
      return this.defaultRequestLessThanLimit(
        this.defaultRequest,
        control.value,
        type,
      )
        ? this.limitLessThanMax(this.max, control.value, type)
          ? null
          : { [`${type}_max`]: true }
        : { [`${type}_min`]: true };
    }
  }

  private cacheLimitRange(resource: LimitRange) {
    const defaultRequest = resource.spec.limits[0]
      .defaultRequest as LimitRangeConfig;
    const max = resource.spec.limits[0].max as LimitRangeConfig;
    if (defaultRequest) {
      this.defaultRequest = defaultRequest;
      this.max = max;
      this.errorMapper = {
        cpu_min: this.translate.get('cpu_should_not_less_than', {
          defaultRequestCPU: defaultRequest.cpu,
        }),
        memory_min: this.translate.get('momoery_should_not_less_than', {
          defaultRequestMemory: defaultRequest.memory,
        }),
        cpu_max: this.translate.get('cpu_should_not_greater_than', {
          maxCPU:
            max.cpu[max.cpu.length - 1] === 'm'
              ? max.cpu
              : max.cpu + this.translate.get('unit_core'),
        }),
        memory_max: this.translate.get('momoery_should_not_greater_than', {
          maxMemory: this.max.memory,
        }),
      };
    }
    this.limitrangeCached = true;
  }

  /**
   * limit值不能小于defaultRequest值
   */
  private defaultRequestLessThanLimit(
    dR: LimitRangeConfig,
    limit: ResourceControl | string,
    type: resourceType,
  ) {
    if (type === 'cpu') {
      const controlValue = limit['unit']
        ? limit['unit'] === 'c'
          ? Number(limit['value']) * 1000
          : Number(limit['value'])
        : parseInt(limit as string, 10) * 1000;
      return this.formatCPU(dR.cpu) <= controlValue;
    } else {
      return (
        this.formatMemory(dR.memory) <=
        (limit['unit'] === 'G'
          ? Number(limit['value']) * 1e9
          : Number(limit['value']) * 1e6)
      );
    }
  }

  /**
   * limit值不能大于max值
   */
  private limitLessThanMax(
    max: LimitRangeConfig,
    limit: ResourceControl | string,
    type: resourceType,
  ): boolean {
    if (type === 'cpu') {
      const controlValue = limit['unit']
        ? limit['unit'] === 'c'
          ? Number(limit['value']) * 1000
          : Number(limit['value'])
        : parseInt(limit as string, 10) * 1000;
      return this.formatCPU(max.cpu) >= controlValue;
    } else {
      return (
        this.formatMemory(max.memory) >=
        (limit['unit'] === 'G'
          ? Number(limit['value']) * 1e9
          : Number(limit['value']) * 1e6)
      );
    }
  }

  private formatMemory(memory: string): Number {
    const base = parseFloat(memory);
    switch (memory[memory.length - 1].toLowerCase()) {
      case 'g':
        return base * 1e9;
        break;
      case 'm':
        return base * 1e6;
        break;
      case 'k':
        return base * 1e3;
      default:
        return base;
        break;
    }
  }

  private formatCPU(cpu: string) {
    const base = parseFloat(cpu);
    switch (cpu[cpu.length - 1].toLowerCase()) {
      case 'm':
        return base;
        break;
      default:
        return base * 1e3;
        break;
    }
  }

  adaptFormModel(
    formModel: ResourceRequirementsFormModel,
  ): ResourceRequirements {
    const resource: ResourceRequirements = Object.keys(formModel).reduce(
      (obj, kind: resourceKind) => {
        const res = Object.keys(formModel[kind]).reduce(
          (item, type: resourceType) => {
            const value = this.getResourceValue(formModel[kind][type]);
            if (!!value) {
              item[type] = value;
            }
            return item;
          },
          {},
        );
        if (!isEmpty(res)) {
          obj[kind] = res;
        }
        return obj;
      },
      {},
    );
    return resource;
  }

  adaptResourceModel(
    resources: ResourceRequirements,
  ): ResourceRequirementsFormModel {
    const _resources = {};
    if (resources) {
      Object.keys(resources).reduce((obj, kind: resourceKind) => {
        obj[kind] = Object.keys(resources[kind]).reduce(
          (item, type: resourceType) => {
            item[type] = this.getResourceViewModel(resources[kind][type], type);
            return item;
          },
          {},
        );
        return obj;
      }, _resources);
    }
    return _resources;
  }

  getDefaultFormModel(): ResourceRequirementsFormModel {
    return {
      limits: {
        cpu: {
          value: '',
          unit: 'm',
        },
        memory: {
          value: '',
          unit: 'M',
        },
      },
    };
  }

  private getResourceViewModel(
    data: string,
    type: resourceType,
  ): ResourceControl | string {
    // 对于 '2' 的cpu大小，视图上展示为 '2c'
    if (type === 'cpu' && !isNaN(+data)) {
      data = `${data}c`;
    }
    const reg = /^(\d?\.?\d+)(\w+)$/;
    const res = {
      value: '',
      unit: '',
    };
    if (!isNaN(parseFloat(data))) {
      const _res = reg.exec(data);
      res.value = _res[1] || '';
      switch (type) {
        case 'cpu':
          res.unit = _res[2] || 'm';
          break;
        case 'memory':
          res.unit = _res[2] || '';
          break;
      }
    }
    return res;
  }

  private getResourceValue(data: ResourceControl | string) {
    if (typeof data === 'string') {
      return data;
    } else {
      if (!!data.value) {
        if (data.unit === 'c') {
          return data.value;
        } else {
          return `${data.value}${data.unit}`;
        }
      } else {
        return '';
      }
    }
  }
}
