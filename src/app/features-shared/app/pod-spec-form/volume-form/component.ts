import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Injector, Input, OnDestroy } from '@angular/core';
import { AbstractControl, FormArray } from '@angular/forms';
import { Validators } from '@angular/forms';

import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { OnFormArrayResizeFn, PathParam } from 'ng-resource-form-util';
import { from, Observable, ReplaySubject, Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import {
  KIND_TO_VOLUME_TYPE,
  SupportedVolumeTypes,
} from 'app/features-shared/app/utils/volume';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { Pvc } from 'app/services/api/storage.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  ConfigMap,
  K8sResourceWithActions,
  StringMap,
  VolumeFormModel,
} from 'app/typings';
import { K8S_VOLUME_MOUNT_SUB_PATH } from 'app/utils/patterns';

interface PvcOption {
  name: string;
  uuid: string;
}

interface ConfigMapOption {
  name: string;
  uuid: string;
  namespace: string;
  data: StringMap;
}

@Component({
  selector: 'rc-container-volume-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ContainerVolumeFormComponent
  extends BaseResourceFormGroupComponent<VolumeFormModel>
  implements OnInit, OnDestroy {
  @Input()
  cluster: string;
  @Input()
  namespace: string;
  private onDestroy$ = new Subject<void>();
  configMapName$ = new ReplaySubject<string>(1);
  pvcs: PvcOption[];
  configMaps$: Observable<ConfigMapOption[]>;
  configMapKeys$: Observable<string[]>;
  types = [
    {
      name: this.translate.get('pvc'),
      value: KIND_TO_VOLUME_TYPE.persistentVolumeClaim,
    },
    {
      name: this.translate.get('configmap'),
      value: KIND_TO_VOLUME_TYPE.configMap,
    },
  ];
  subPathRegex = K8S_VOLUME_MOUNT_SUB_PATH;
  constructor(
    injector: Injector,
    private translate: TranslateService,
    private k8sResourceService: K8sResourceService,
  ) {
    super(injector);
    this.types.push({
      name: this.translate.get('host-path'),
      value: KIND_TO_VOLUME_TYPE.hostPath,
    });
  }

  ngOnInit() {
    super.ngOnInit();
    this.getPvcs();
    this.getConfigMaps();
    this.configMapKeys$ = this.configMapName$.pipe(
      switchMap(value => {
        return this.configMaps$.pipe(
          map(configMaps => {
            const configmap = configMaps.find(configMap => {
              return configMap.name === value;
            });
            return configmap ? Object.keys(configmap.data) : [];
          }),
        );
      }),
      publishReplay(1),
      refCount(),
    );
    this.form
      .get('type')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((type: string) => {
        SupportedVolumeTypes.forEach((_type: string) => {
          if (_type === type) {
            this.form.get(_type).enable({
              emitEvent: false,
            });
          } else {
            this.form.get(_type).disable({
              emitEvent: false,
            });
          }
        });
      });
    this.form
      .get('configMap.hasRefKeys')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe((value: boolean) => {
        if (this.form.get('configMap').disabled) {
          return;
        }
        if (value) {
          this.form.get('configMap.mountPath').disable({
            emitEvent: false,
          });
          this.form.get('configMap.refKeys').enable({
            emitEvent: false,
          });
        } else {
          this.form.get('configMap.mountPath').enable({
            emitEvent: false,
          });
          this.form.get('configMap.refKeys').disable({
            emitEvent: false,
          });
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  getResourceMergeStrategy() {
    return false;
  }

  getDefaultFormModel(): VolumeFormModel {
    return {
      type: KIND_TO_VOLUME_TYPE.persistentVolumeClaim,
    };
  }

  adaptFormModel(formModel: VolumeFormModel): VolumeFormModel {
    return {
      ...formModel,
    };
  }

  adaptResourceModel(resource: VolumeFormModel): VolumeFormModel {
    let type;
    if (!resource) {
      return {};
    }
    if (resource.configMap) {
      type = KIND_TO_VOLUME_TYPE.configMap;
      this.configMapChange(resource.configMap.name);
    } else if (resource.persistentVolumeClaim) {
      type = KIND_TO_VOLUME_TYPE.persistentVolumeClaim;
    } else if (resource.hostPath) {
      type = KIND_TO_VOLUME_TYPE.hostPath;
    }
    return {
      type,
      ...resource,
    };
  }

  createForm() {
    return this.fb.group({
      type: [],
      hostPath: this.fb.group(
        {
          path: [],
          mountPath: [],
        },
        {
          disabled: true,
        },
      ),
      persistentVolumeClaim: this.fb.group(
        {
          claimName: [],
          mountPath: [],
          subPath: ['', [Validators.pattern(this.subPathRegex.pattern)]],
        },
        {
          disabled: true,
        },
      ),
      configMap: this.fb.group(
        {
          name: [],
          mountPath: [],
          hasRefKeys: [false],
          refKeys: this.fb.array([]),
        },
        {
          disabled: true,
        },
      ),
    });
  }

  getOnFormArrayResizeFn(): OnFormArrayResizeFn {
    return (_path: PathParam) => {
      return this.getNewConfigMapRefKeyFormControl();
    };
  }

  configMapChange(name: string) {
    this.configMapName$.next(name);
  }

  configMapTrackByFn = (option: ConfigMapOption) => {
    return option.name;
  };

  configMapKeyTrackByFn(_index: number) {
    return _index;
  }

  addRefKey() {
    this.refKeysFormArray.push(this.getNewConfigMapRefKeyFormControl());
  }

  removeRefKey(index: number) {
    this.refKeysFormArray.removeAt(index);
  }

  pvcTrackByFn = (option: PvcOption) => {
    return option.name;
  };

  get selectedType() {
    return this.form.get('type').value;
  }

  get refKeysControls(): AbstractControl[] {
    return this.refKeysFormArray.controls;
  }

  get refKeysFormArray(): FormArray {
    return this.form.get('configMap.refKeys') as FormArray;
  }

  private getNewConfigMapRefKeyFormControl() {
    return this.fb.group({
      key: [],
      path: [],
    });
  }

  private getConfigMaps() {
    this.configMaps$ = from(
      this.k8sResourceService
        .getK8sResources<ConfigMap>('configmaps', {
          clusterName: this.cluster,
          namespace: this.namespace,
        })
        .then((results: K8sResourceWithActions<ConfigMap>[]) => {
          return results.map(result => ({
            name: result.kubernetes.metadata.name,
            uuid: result.kubernetes.metadata.uid,
            namespace: result.kubernetes.metadata.namespace,
            data: result.kubernetes.data,
          }));
        })
        .catch(() => []),
    ).pipe(
      publishReplay(1),
      refCount(),
    );
  }

  private async getPvcs() {
    try {
      const { results } = await this.k8sResourceService.getK8sResourcesPaged(
        'persistentvolumeclaims',
        {
          clusterName: this.cluster,
          namespace: this.namespace,
          page_size: 100,
        },
      );
      this.pvcs = results
        .filter((item: Pvc) => {
          const status: string = get(
            item,
            'kubernetes.status.phase',
            '',
          ).toLowerCase();
          if (status === 'lost') {
            return false;
          }
          return true;
        })
        .map((item: Pvc) => {
          return {
            name: item.kubernetes.metadata.name,
            uuid: item.kubernetes.metadata.uid,
          };
        });
    } catch (_e) {
      this.pvcs = [];
    }
    this.cdr.markForCheck();
  }
}
