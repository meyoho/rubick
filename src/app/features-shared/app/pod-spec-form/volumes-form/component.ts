import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import { TranslateService } from 'app/translate/translate.service';
import { VolumeFormModel } from 'app/typings';
import { safeDump } from 'js-yaml';
import { first } from 'rxjs/operators';

import { ContainerVolumeDialogComponent } from './dialog';
@Component({
  selector: 'rc-container-volumes-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ContainerVolumesFormComponent),
      multi: true,
    },
  ],
})
export class ContainerVolumesFormComponent
  implements OnInit, ControlValueAccessor {
  @Input()
  namespace: string;
  @Input()
  cluster: string;
  volumes: VolumeFormModel[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {}

  /**
   * Wrapps the ControlValueAccessor onChange (cvaConChange) to let
   * the user do some hack before calling onChange
   */
  onChange() {
    this.onCvaChange([...this.volumes]);
  }

  onCvaChange = (_: VolumeFormModel[]) => {};
  onCvaTouched = () => {};
  onValidatorChange = () => {};

  registerOnChange(fn: (value: VolumeFormModel[]) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(_resource: VolumeFormModel[]) {
    this.volumes = [...(_resource || [])];
    this.cdr.markForCheck();
  }

  add() {
    const dialogRef = this.dialogService.open(ContainerVolumeDialogComponent, {
      size: DialogSize.Big,
      data: {
        cluster: this.cluster,
        namespace: this.namespace,
      },
    });

    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: VolumeFormModel) => {
        dialogRef.close();
        if (!!data) {
          this.volumes.push(data);
          this.onChange();
        }
      });
  }

  remove(index: number) {
    this.volumes = this.volumes.filter((_v, i: number) => {
      return index !== i;
    });
    this.onChange();
  }

  update(i: number) {
    const volume = Object.assign({}, this.volumes[i]);
    const dialogRef = this.dialogService.open(ContainerVolumeDialogComponent, {
      size: DialogSize.Big,
      data: {
        cluster: this.cluster,
        namespace: this.namespace,
        formModel: volume,
      },
    });

    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: VolumeFormModel) => {
        dialogRef.close();
        if (!!data) {
          this.volumes[i] = data;
        }
        this.onChange();
      });
  }

  getType(v: VolumeFormModel) {
    if (v.hostPath) {
      return this.translate.get('host-path');
    } else if (v.configMap) {
      return this.translate.get('configmap');
    } else if (v.persistentVolumeClaim) {
      return this.translate.get('pvc');
    }
  }

  getYaml(v: VolumeFormModel) {
    if (v.other) {
      let yaml = '';
      try {
        yaml = safeDump(v.other.yaml, { sortKeys: true });
      } catch (_e) {}
      return yaml;
    } else {
      return '';
    }
  }

  getSubPath(v: VolumeFormModel) {
    if (v.persistentVolumeClaim) {
      return v.persistentVolumeClaim.subPath;
    } else {
      return '';
    }
  }

  getName(v: VolumeFormModel) {
    if (v.hostPath) {
      return v.hostPath.path;
    } else if (v.configMap) {
      return v.configMap.name;
    } else if (v.persistentVolumeClaim) {
      return v.persistentVolumeClaim.claimName;
    }
  }

  getMountPath(v: VolumeFormModel) {
    if (v.hostPath) {
      return v.hostPath.mountPath;
    } else if (v.configMap) {
      return v.configMap.mountPath;
    } else if (v.persistentVolumeClaim) {
      return v.persistentVolumeClaim.mountPath;
    } else if (v.other) {
      return v.other.mountPath;
    }
  }

  hasRefConfigKeys(v: VolumeFormModel) {
    return v.configMap && v.configMap.hasRefKeys;
  }

  getConfigMapRefKeys(v: VolumeFormModel) {
    return v.configMap.refKeys;
  }

  keyRefTrackByFn(index: number) {
    return index;
  }
}
