import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { VolumeFormModel } from 'app/typings';

@Component({
  templateUrl: 'dialog.html',
})
export class ContainerVolumeDialogComponent {
  close = new EventEmitter<VolumeFormModel>();
  @ViewChild(NgForm)
  form: NgForm;
  cluster: string;
  namespace: string;
  formModel: VolumeFormModel;
  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      cluster: string;
      namespace: string;
      formModel?: VolumeFormModel;
    },
  ) {
    if (this.dialogData) {
      this.cluster = this.dialogData.cluster;
      this.namespace = this.dialogData.namespace;
      if (this.dialogData.formModel) {
        this.formModel = this.dialogData.formModel;
      }
    }
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.close.next(this.formModel);
  }

  cancel() {
    this.close.next();
  }
}
