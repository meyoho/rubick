import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  FormArray,
  FormGroup,
  Validators,
} from '@angular/forms';

import {
  BaseResourceFormGroupComponent,
  OnFormArrayResizeFn,
  PathParam,
} from 'ng-resource-form-util';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { PodControllerKindEnum } from 'app/features-shared/app/utils/pod-controller';
import {
  adaptServiceResourceToFormModel,
  ServiceAlbPort,
} from 'app/features-shared/app/utils/service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { LoadBalancer, ServicePort } from 'app/typings';
import {
  ServiceFormModel,
  ServicePortFormModel,
  ServiceTypeDisplay,
  ServiceTypeEnum,
} from 'app/typings';
import { Environments } from 'app/typings';
import { Service, ServiceTypeMeta } from 'app/typings/raw-k8s';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';

const PROTOCOL_VALUE = Object.freeze({
  TCP: 'tcp',
  HTTP: 'http',
});

@Component({
  selector: 'rc-k8s-service-form',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
  styleUrls: ['./styles.scss'],
})
export class ServiceFormComponent
  extends BaseResourceFormGroupComponent<ServiceFormModel>
  implements OnInit, OnDestroy {
  @Input()
  set relatedAlbs(lbs: LoadBalancer[]) {
    this._relatedAlbs$.next(lbs);
    this._relatedAlbs = lbs;
  }
  get relatedAlbs() {
    return this._relatedAlbs;
  }
  private _relatedAlbs$ = new BehaviorSubject<LoadBalancer[]>([]);
  private _relatedAlbs: LoadBalancer[] = [];
  @Input()
  podControllers: {
    name: string;
    kind: PodControllerKindEnum;
  }[] = [];

  private _HttpEnableMap: Record<string, boolean> = {};
  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  typeOptions = [
    {
      name: ServiceTypeDisplay[ServiceTypeEnum.ClusterIP],
      value: ServiceTypeEnum.ClusterIP,
    },
    {
      name: ServiceTypeDisplay[ServiceTypeEnum.NodePort],
      value: ServiceTypeEnum.NodePort,
    },
  ];

  protocolOptions = [
    {
      name: 'TCP',
      value: PROTOCOL_VALUE.TCP,
    },
    {
      name: 'HTTP',
      value: PROTOCOL_VALUE.HTTP,
    },
  ];
  private onDestroy$ = new Subject<void>();

  constructor(
    injector: Injector,
    private translateService: TranslateService,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    super(injector);
    this.isProtocolOptEnable = this.isProtocolOptEnable.bind(this);
  }

  ngOnInit() {
    super.ngOnInit();
    const groups = this.getPortFormControls();
    this._relatedAlbs$.subscribe(albs => {
      albs.forEach(alb => {
        const domains = alb.spec.domains;
        this._HttpEnableMap[alb.metadata.name] =
          Array.isArray(domains) && domains.length > 0;
      });
    });
    this.form.valueChanges
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((formModel: ServiceFormModel) => {
        formModel.ports.forEach(
          (portModel: ServicePortFormModel, index: number) => {
            const albPortControl = groups[index].get('albPort');
            const albProtocolControl = groups[index].get('albProtocol');

            if (!!portModel.albName) {
              albPortControl.enable({
                emitEvent: false,
              });
              albProtocolControl.enable({
                emitEvent: false,
              });
            } else {
              albPortControl.disable({
                emitEvent: false,
              });
              albProtocolControl.disable({
                emitEvent: false,
              });
            }
          },
        );
      });
  }
  isProtocolOptEnable(control: FormGroup, protocolValue: string) {
    if (protocolValue !== PROTOCOL_VALUE.HTTP) {
      return true;
    }
    const albName = control.get('albName').value;
    return this._HttpEnableMap[albName];
  }
  ngOnDestroy() {
    this.onDestroy$.next();
  }
  ablNameChange(control: FormGroup) {
    control.get('albProtocol').setValue(PROTOCOL_VALUE.TCP);
  }
  get albNameOptions(): { name: string; value: string }[] {
    const options = this.relatedAlbs.map(alb => {
      const name = alb.metadata.name;
      return {
        name,
        value: name,
      };
    });
    return [
      {
        name: this.translateService.get('option_none'),
        value: '',
      },
      ...options,
    ];
  }

  get podControllerNameOptions(): { name: string; value: string }[] {
    return this.podControllers.length
      ? [
          {
            name: this.translateService.get('option_none'),
            value: '',
          },
          ...this.podControllers.map(item => {
            return {
              name: `${item.name} (${item.kind.toLowerCase()})`,
              value: `${item.kind.toLowerCase()}-${item.name}`,
            };
          }),
        ]
      : [];
  }

  getPortFormControls(): AbstractControl[] {
    return this.getPortFormArray().controls;
  }

  getPortFormArray(): FormArray {
    return this.form.get('ports') as FormArray;
  }

  albTrackByFn(index: number) {
    return index;
  }
  podControllerTrackByFn(index: number) {
    return index;
  }

  getResourceMergeStrategy() {
    return false;
  }

  getDefaultFormModel(): ServiceFormModel {
    return {
      type: ServiceTypeEnum.ClusterIP,
      ports: [
        {
          port: '',
          albProtocol: PROTOCOL_VALUE.TCP as 'tcp',
        },
      ],
    };
  }

  adaptFormModel(formModel: ServiceFormModel): Service {
    const { type, podControllerName, ports, ...resource } = formModel;
    if (type === ServiceTypeEnum.Headless) {
      resource.spec.clusterIP = 'None';
    } else {
      resource.spec.type = type;
    }
    const selectorKey = `service.${this.env.label_base_domain}/name`;
    if (podControllerName) {
      if (resource.spec.selector) {
        resource.spec.selector[selectorKey] = podControllerName;
      } else {
        resource.spec.selector = {
          [selectorKey]: podControllerName,
        };
      }
    } else if (resource.spec.selector) {
      delete resource.spec.selector[selectorKey];
    }
    if (ports) {
      const servicePorts: ServicePort[] = ports.map(
        (p: ServicePortFormModel) => {
          return {
            port: +p.port,
            // targetPort可以是字符串或数字
            targetPort: p.targetPort
              ? isNaN(+p.targetPort)
                ? p.targetPort
                : +p.targetPort
              : +p.port,
            name: `${p.port}-${p.targetPort || p.port}`,
            protocol: 'TCP',
          } as ServicePort;
        },
      );
      const albPorts: ServiceAlbPort[] = ports
        .filter(p => !!p.albName)
        .map((p: ServicePortFormModel) => {
          return {
            container_port: +p.port,
            port: +p.albPort || +p.port,
            name: p.albName,
            protocol: p.albProtocol,
          } as ServiceAlbPort;
        });
      resource.spec.ports = servicePorts;
      const key = `loadbalancer.${this.env.label_base_domain}/bind`;
      if (resource.metadata.annotations) {
        resource.metadata.annotations[key] = JSON.stringify(albPorts);
      } else {
        resource.metadata.annotations = {
          [key]: JSON.stringify(albPorts),
        };
      }
    }
    return Object.assign({}, ServiceTypeMeta, resource);
  }

  adaptResourceModel(resource: Service): ServiceFormModel {
    if (!resource) {
      return {};
    }
    return adaptServiceResourceToFormModel({
      resource: resource,
      labelBaseDomain: this.env.label_base_domain,
    });
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control(
        {
          value: '',
        },
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ],
      ),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });

    const specForm = this.fb.group({
      ports: [],
      selector: [],
    });

    const portsForm = this.fb.array([]);

    return this.fb.group({
      type: this.fb.control('', [Validators.required]),
      podControllerName: this.fb.control(['']),
      ports: portsForm,
      metadata: metadataForm,
      spec: specForm,
    });
  }

  addPort() {
    const portsControl = this.form.get('ports') as FormArray;
    portsControl.push(
      this.fb.group({
        port: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
        targetPort: [],
        albName: [''],
        albPort: [],
        albProtocol: [PROTOCOL_VALUE.TCP],
      }),
    );
  }

  removePort(index: number) {
    this.getPortFormArray().removeAt(index);
  }

  getOnFormArrayResizeFn(): OnFormArrayResizeFn {
    return (_path: PathParam) => {
      return this.fb.group({
        port: [null, [Validators.required, Validators.pattern(/^\d+$/)]],
        targetPort: [],
        albName: [
          {
            value: '',
          },
        ],
        albPort: [{}],
        albProtocol: [
          {
            value: PROTOCOL_VALUE.TCP,
          },
        ],
      });
    };
  }
}
