import { NgModule } from '@angular/core';

import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { AppShardService } from './app-shard.service';
import { PodControllerContainersComponent } from './detail/containers/component';
import { PodControllerDetailEnvComponent } from './detail/env/component';
import { ContainerHealthcheckDisplayComponent } from './detail/healthcheck/component';
import { PodControllerDetailLogComponent } from './detail/log/component';
import { ApplicationToggleBarComponent } from './detail/toggle-bar/component';
import { ContainerVolumesDisplayComponent } from './detail/volume/component';
import { UpdateEnvDialogComponent } from './dialog/update-env/component';
import { UpdateImageTagDialogComponent } from './dialog/update-image-tag/component';
import { UpdateResourceSizeDialogComponent } from './dialog/update-resource-size/component';
import { ContainerFormHiddenFieldDirective } from './directives/container-form-hidden-field.directive';
import { PodSpecFormHiddenFieldDirective } from './directives/pod-spec-form-hidden-field.directive';
import { AutoScalerFormComponent } from './pod-controller-form/autoscaler-form/component';
import { PodControllerFormComponent } from './pod-controller-form/component';
import { PodNetworkFormComponent } from './pod-controller-form/pod-network-form/component';
import { UpdateStrategyFormComponent } from './pod-controller-form/strategy-form/component';
import { AffinityContainerFormComponent } from './pod-spec-form/affinity-container-form/component';
import { PodAffinityDialogComponent } from './pod-spec-form/affinity-dialog/component';
import { PodSpecFormComponent } from './pod-spec-form/component';
import { ContainerFormComponent } from './pod-spec-form/container-form/component';
import { EnvFormComponent } from './pod-spec-form/envs-form/component';
import { EnvFromFormComponent } from './pod-spec-form/envs-from-form/component';
import { HealthcheckFormComponent } from './pod-spec-form/healthcheck-form/component';
import { HealthcheckDialogComponent } from './pod-spec-form/healthcheck-form/container-healthcheck-dialog.component';
import { NodeSelectorComponent } from './pod-spec-form/node-selector/component';
import { PodAffinityFormComponent } from './pod-spec-form/pod-affinity-form/component';
import { ResourcesFormComponent } from './pod-spec-form/resource-form/component';
import { ContainerVolumeFormComponent } from './pod-spec-form/volume-form/component';
import { ContainerVolumesFormComponent } from './pod-spec-form/volumes-form/component';
import { ContainerVolumeDialogComponent } from './pod-spec-form/volumes-form/dialog';
import { ServiceFormComponent } from './service-form/component';

const EXPORT_COMPONENTS = [
  AffinityContainerFormComponent,
  ApplicationToggleBarComponent,
  AutoScalerFormComponent,
  ContainerFormComponent,
  ContainerHealthcheckDisplayComponent,
  ContainerVolumeDialogComponent,
  ContainerVolumeFormComponent,
  ContainerVolumesDisplayComponent,
  ContainerVolumesFormComponent,
  EnvFormComponent,
  EnvFromFormComponent,
  HealthcheckDialogComponent,
  HealthcheckFormComponent,
  NodeSelectorComponent,
  PodAffinityDialogComponent,
  PodAffinityFormComponent,
  PodControllerContainersComponent,
  PodControllerDetailEnvComponent,
  PodControllerDetailLogComponent,
  PodControllerFormComponent,
  PodNetworkFormComponent,
  PodSpecFormComponent,
  ResourcesFormComponent,
  ServiceFormComponent,
  UpdateImageTagDialogComponent,
  UpdateResourceSizeDialogComponent,
  UpdateStrategyFormComponent,
  UpdateEnvDialogComponent,
  PodSpecFormHiddenFieldDirective,
  ContainerFormHiddenFieldDirective,
];

const ENTRY_COMPONENTS = [
  ContainerVolumeDialogComponent,
  HealthcheckDialogComponent,
  PodAffinityDialogComponent,
  UpdateImageTagDialogComponent,
  UpdateResourceSizeDialogComponent,
  UpdateEnvDialogComponent,
];

@NgModule({
  imports: [SharedModule, FormTableModule],
  exports: [...EXPORT_COMPONENTS],
  declarations: [...EXPORT_COMPONENTS],
  providers: [AppShardService],
  entryComponents: [...ENTRY_COMPONENTS],
})
export class AppSharedModule {}
