import { InjectionToken } from '@angular/core';
export interface PodSpecFormConfig {
  hiddenFields: string[];
}

export interface ContainerFormConfig {
  hiddenFields: string[];
}

export const POD_SPEC_FORM_CONFIG = new InjectionToken<PodSpecFormConfig>(
  'PodSpecFormConfig',
);
export const CONTAINER_FORM_CONFIG = new InjectionToken<ContainerFormConfig>(
  'ContainerFormConfig',
);
