import { get } from 'lodash-es';

import { AffinityTerm } from 'app/typings/k8s-form-model';
import { WeightedPodAffinityTerm } from 'app/typings/raw-k8s';

export function getLabels(val: AffinityTerm) {
  return Object.entries(
    get(
      val,
      get(val as WeightedPodAffinityTerm, 'weight')
        ? 'podAffinityTerm.labelSelector.matchLabels'
        : 'labelSelector.matchLabels',
      [],
    ),
  );
}
