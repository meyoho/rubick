export const LEVELS = ['Critical', 'High', 'Medium', 'Low'];

export const MONITOR_COLOR_SET = [
  '#7cb5ec',
  '#61d44a',
  '#434348',
  '#f7a35c',
  '#8085e9',
  '#f15c80',
  '#e4d354',
  '#2b908f',
  '#f45b5b',
  '#08dccb',
];

export const AGGREGATORS = [
  {
    key: 'avg',
    name: 'aggregator_avg',
  },
  {
    key: 'max',
    name: 'aggregator_max',
  },
  {
    key: 'min',
    name: 'aggregator_min',
  },
];

export const MODES = [
  {
    key: 'workload',
    name: 'compute_component',
  },
  {
    key: 'pod',
    name: 'Pod',
  },
  {
    key: 'container',
    name: 'container',
  },
];

export const UNITS = [
  '%',
  'byte/second',
  'byte',
  'kb',
  'mb',
  'gb',
  'core',
  's',
  'ms',
];

export const QueryMetrics = [
  {
    metric: 'cpu.utilization',
    series: 'cpuChartOptions',
    unit: '%',
  },
  {
    metric: 'memory.utilization',
    series: 'memChartOptions',
    unit: '%',
  },
  {
    metric: 'network.transmit_bytes',
    series: 'sentBytesChartOptions',
  },
  {
    metric: 'network.receive_bytes',
    series: 'receivedBytesChartOptions',
  },
];

export const TIME_STAMP_OPTIONS = [
  {
    type: 'last_30_minites',
    offset: 30 * 60 * 1000,
  },
  {
    type: 'last_hour',
    offset: 60 * 60 * 1000,
  },
  {
    type: 'last_6_hour',
    offset: 6 * 3600 * 1000,
  },
  {
    type: 'last_day',
    offset: 24 * 3600 * 1000,
  },
  {
    type: 'last_3_days',
    offset: 3 * 24 * 3600 * 1000,
  },
  {
    type: 'last_7_days',
    offset: 7 * 24 * 3600 * 1000,
  },
];

export const MetricPercentOptions = {
  chart: {
    type: 'line',
  },
  colors: MONITOR_COLOR_SET,
  credits: {
    enabled: false,
  },
  legend: {
    enabled: false,
  },
  title: {
    text: <string>null,
  },
  tooltip: {
    valueDecimals: 4,
    valueSuffix: '%',
    xDateFormat: '%Y-%m-%d %H:%M:%S',
    shared: true,
  },
  plotOptions: {
    line: {
      connectNulls: true,
      marker: {
        enabled: false,
      },
    },
  },
  xAxis: {
    gridLineWidth: 0,
    dateTimeLabelFormats: {
      day: '%m-%d',
    },
    type: 'datetime',
    crosshair: true,
  },
  yAxis: {
    gridLineWidth: 0,
    title: {
      text: <string>null,
    },
    labels: {
      formatter: function() {
        if (this.value > 0.01 || this.value === 0) {
          return this.value.toPrecision(3) + '%';
        }
        return this.value.toFixed(4) + '%';
      },
    },
  },
};

export const MetricNumericOptions = {
  chart: {
    type: 'line',
  },
  colors: MONITOR_COLOR_SET,
  credits: {
    enabled: false,
  },
  legend: {
    enabled: false,
  },
  title: {
    text: <string>null,
  },
  tooltip: {
    valueDecimals: 4,
    xDateFormat: '%Y-%m-%d %H:%M:%S',
    shared: true,
  },
  plotOptions: {
    line: {
      connectNulls: true,
      marker: {
        enabled: false,
      },
    },
  },
  xAxis: {
    gridLineWidth: 0,
    dateTimeLabelFormats: {
      day: '%m-%d',
    },
    type: 'datetime',
    crosshair: true,
  },
  yAxis: {
    gridLineWidth: 0,
    title: {
      text: <string>null,
    },
    minRange: 0.1,
  },
};

export function getGaugeChartsOptions() {
  return {
    chart: {
      type: 'solidgauge',
    },
    credits: {
      enabled: false,
    },
    title: {
      text: <string>null,
    },
    pane: {
      startAngle: 0,
      endAngle: 360,
      background: {
        backgroundColor: '#EDEDED',
        borderColor: '#FFF',
        innerRadius: '90%',
        outerRadius: '100%',
        shape: 'solid',
      },
    },
    tooltip: {
      enabled: false,
    },
    yAxis: {
      min: 0,
      max: 100,
      lineColor: '#FFF',
      stops: [[0.1, '#36B37E']],
      minorTickInterval: <number>null,
      tickWidth: 0,
      labels: {
        enabled: false,
      },
    },
    plotOptions: {
      solidgauge: {
        dataLabels: {
          format: '{y}%',
          y: -12,
          borderWidth: 0,
          style: {
            color: '#36B37E',
            fontSize: '16px',
            fontWeight: 'normal',
          },
        },
        innerRadius: '90%',
      },
    },
  };
}
