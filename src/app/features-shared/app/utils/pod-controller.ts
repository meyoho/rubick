import { AbstractControl, ValidatorFn } from '@angular/forms';

import { Container } from 'app/typings/raw-k8s';
import {
  DaemonSetUpdateStrategyType,
  DeploymentStrategyType,
  RollingUpdateDaemonSet,
  RollingUpdateDeployment,
  RollingUpdateStatefulSetStrategy,
  StatefulSetUpdateStrategyType,
} from 'app/typings/raw-k8s';

export enum PodControllerKindEnum {
  Deployment = 'Deployment',
  StatefulSet = 'StatefulSet',
  DaemonSet = 'DaemonSet',
}

export enum PodControllerUpdateStrategyEnum {
  OnDelete = 'OnDelete',
  RollingUpdate = 'RollingUpdate',
}

export const PodControllerKinds = Object.keys(PodControllerKindEnum);
export const PodControllerUpdateStrategyOptions = Object.keys(
  PodControllerUpdateStrategyEnum,
);

export enum PodControllerKindToResourceTypeEnum {
  Deployment = 'deployments',
  StatefulSet = 'statefulsets',
  DaemonSet = 'daemonsets',
}

export type PodControllerUpdateStrategyType =
  | DeploymentStrategyType
  | DaemonSetUpdateStrategyType
  | StatefulSetUpdateStrategyType;

export type PodControllerRollingUpdateType =
  | RollingUpdateDeployment
  | RollingUpdateDaemonSet
  | RollingUpdateStatefulSetStrategy;

export function getNamesValidatorFn(namesFn: Function): ValidatorFn {
  return (control: AbstractControl) => {
    const names = namesFn();
    if (names && control.value && names.includes(control.value)) {
      return {
        name_existed: true,
      };
    } else {
      return null;
    }
  };
}

export function getDefaultContainerConfig(
  params: {
    name: string;
    image: string;
  } = {
    name: '',
    image: '',
  },
): Container {
  return {
    name: params.name,
    image: params.image,
    resources: {
      limits: {
        cpu: '1',
        memory: '512M',
      },
    },
  };
}
