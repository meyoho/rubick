import {
  ServiceFormModel,
  ServicePort,
  ServicePortFormModel,
  ServiceTypeEnum,
} from 'app/typings';
import { Service, ServiceTypeMeta } from 'app/typings/raw-k8s';

export interface ServiceAlbPort {
  container_port: number;
  port: number;
  name: string;
  protocol: 'http' | 'tcp';
}

export function getDefaultService(namespace?: string): Service {
  return Object.assign({}, ServiceTypeMeta, {
    metadata: {
      name: '',
      namespace: namespace || '',
    },
    spec: {
      type: 'ClusterIP',
    },
  });
}

export function adaptServiceResourceToFormModel(params: {
  resource: Service;
  labelBaseDomain: string;
}) {
  const resource = params.resource;
  const formModel = { ...resource } as ServiceFormModel;
  if (resource.spec.type === ServiceTypeEnum.NodePort) {
    formModel.type = ServiceTypeEnum.NodePort;
  } else if (resource.spec.clusterIP === 'None') {
    formModel.type = ServiceTypeEnum.Headless;
  } else {
    formModel.type = ServiceTypeEnum.ClusterIP;
  }
  if (resource.spec.selector) {
    const key = `service.${params.labelBaseDomain}/name`;
    formModel.podControllerName = resource.spec.selector[key] || '';
  } else {
    formModel.podControllerName = '';
  }
  let albPorts: ServiceAlbPort[] = [];
  if (resource.metadata.annotations) {
    try {
      const key = `loadbalancer.${params.labelBaseDomain}/bind`;
      albPorts = JSON.parse(resource.metadata.annotations[key]);
    } catch (_e) {}
  }

  if (resource.spec.ports) {
    formModel.ports = resource.spec.ports.map((p: ServicePort) => {
      const port: ServicePortFormModel = {
        port: p.port,
        targetPort: p.targetPort,
      };
      const albPort = albPorts.find((_p: ServiceAlbPort) => {
        return _p.container_port === p.port;
      });
      if (albPort) {
        port.albName = albPort.name;
        port.albPort = albPort.port === 0 ? null : albPort.port;
        port.albProtocol = albPort.protocol;
      }
      return port;
    });
  }
  return formModel;
}
