import { VolumeFormModel } from 'app/typings';
import * as md5 from 'md5';

export enum KIND_TO_VOLUME_TYPE {
  hostPath = 'hostPath',
  configMap = 'configMap',
  persistentVolumeClaim = 'persistentVolumeClaim',
}

export const SupportedVolumeTypes = Object.keys(KIND_TO_VOLUME_TYPE);

export function getVolumeFormModelUniqName(v: VolumeFormModel) {
  if (v.name) {
    return v.name;
  }
  if (v.hostPath) {
    return `${KIND_TO_VOLUME_TYPE.hostPath.toLowerCase()}-${md5(
      v.hostPath.path,
    )}`;
  } else if (v.configMap) {
    return `${KIND_TO_VOLUME_TYPE.configMap.toLowerCase()}-${v.configMap.name}`;
  } else if (v.persistentVolumeClaim) {
    return `${KIND_TO_VOLUME_TYPE.persistentVolumeClaim.toLowerCase()}-${
      v.persistentVolumeClaim.claimName
    }`;
  }
}

export function getVolumeFormModelMountPath(v: VolumeFormModel) {
  if (v.configMap) {
    return v.configMap.mountPath;
  } else if (v.persistentVolumeClaim) {
    return v.persistentVolumeClaim.mountPath;
  } else if (v.hostPath) {
    return v.hostPath.mountPath;
  } else if (v.other) {
    return v.other.mountPath;
  }
}

export function getVolumeFormModelSubPath(v: VolumeFormModel) {
  if (v.configMap) {
    return v.configMap.subPath;
  } else if (v.persistentVolumeClaim) {
    return v.persistentVolumeClaim.subPath;
  } else if (v.other) {
    return v.other.subPath;
  } else if (v.hostPath) {
    return null;
  }
}
