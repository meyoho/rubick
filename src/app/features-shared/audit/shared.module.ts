import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { AuditListComponent } from 'app2/features/audit/list/audit-list.component';

const components = [AuditListComponent];

@NgModule({
  imports: [RouterModule, SharedModule],
  declarations: components,
  exports: components,
  entryComponents: components,
})
export class AuditSharedModule {}
