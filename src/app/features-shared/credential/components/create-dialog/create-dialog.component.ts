import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { EditorStatus } from '../../types';

@Component({
  templateUrl: 'create-dialog.component.html',
  styleUrls: ['create-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CredentialCreateDialogComponent {
  status: EditorStatus = 'normal';

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      namespace: string;
      types: string[];
      showHint: boolean;
      space?: string;
    },
    private dialogRef: DialogRef<CredentialCreateDialogComponent>,
  ) {}

  close(created = '') {
    this.dialogRef.close(created);
  }

  onStatusChange(status: EditorStatus) {
    this.status = status;
  }
}
