import { ChangeDetectionStrategy, Component, Optional } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { get } from 'lodash-es';
import { combineLatest, Observable, of } from 'rxjs';
import { filter, map, publishReplay, refCount, tap } from 'rxjs/operators';

import { CredentialIdentity } from 'app/services/api/credential.service';
import { PorjectTabIndex } from 'app/services/api/project.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { AppState, getCurrentProjectDetail, getCurrentSpace } from 'app/store';
import { isAdminView } from 'app/utils/page';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  selector: 'rc-credential-detail-page',
  templateUrl: 'detail-page.component.html',
  styleUrls: ['detail-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CredentialDetailPageComponent {
  permissions = {
    canUpdate: false,
    canDelete: false,
  };
  tabIndex = PorjectTabIndex.Credential;

  id$: Observable<CredentialIdentity> = this.route.paramMap.pipe(
    map(paramMap => ({
      name: paramMap.get('name'),
    })),
    publishReplay(1),
    refCount(),
  );

  project$ = this.store.select(getCurrentProjectDetail).pipe(
    filter(p => !!p),
    map(p => p.name),
  );

  space$ = this.store.select(getCurrentSpace).pipe(
    filter(s => !!s),
    map(s => s.name),
  );

  info$ = this.getInfo().pipe(
    tap(res => {
      const space_name = get(res, 'space_name');
      const context = space_name ? { space_name } : {};
      this.getPermissions(context);
    }),
    publishReplay(1),
    refCount(),
  );

  isProjectView = this.getViewType() === 'project';

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    @Optional() private workspaceComponent: WorkspaceComponent,
    private roleUtil: RoleUtilitiesService,
    private store: Store<AppState>,
  ) {}

  private getInfo() {
    switch (this.getViewType()) {
      case 'admin':
        return of({});
      case 'user':
        return combineLatest(this.project$, this.space$).pipe(
          map(([project_name, space_name]) => {
            return {
              project_name,
              space_name,
            };
          }),
        );
      case 'project':
        return this.project$.pipe(
          map(project_name => {
            return { project_name };
          }),
        );
    }
  }

  getViewType() {
    let type: string;
    if (isAdminView()) {
      type = 'admin';
    } else if (this.workspaceComponent) {
      type = 'user';
    } else {
      type = 'project';
    }
    return type;
  }

  getNavItemLink() {
    switch (this.getViewType()) {
      case 'admin':
      case 'user':
        return {
          path: ['../../'],
          params: {},
        };
      case 'project':
        return {
          path: ['../../../detail'],
          params: {
            tabIndex: this.tabIndex,
          },
        };
    }
  }

  async getPermissions(context: any) {
    [
      this.permissions.canUpdate,
      this.permissions.canDelete,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_secret',
      context,
      ['update', 'delete'],
    );
  }

  onDeleted() {
    const path = this.isProjectView ? ['../../../detail'] : ['../../'];
    const params = this.isProjectView
      ? {
          tabIndex: this.tabIndex,
        }
      : {};
    setTimeout(() => {
      this.router.navigate(path, {
        queryParams: params,
        relativeTo: this.route,
      });
    }, 1000);
  }
}
