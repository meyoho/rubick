import { ConfirmType, DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { of, Subject } from 'rxjs';
import { catchError } from 'rxjs/operators';

import {
  Credential,
  CredentialApiService,
  CredentialIdentity,
  CredentialType,
} from 'app/services/api/credential.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { CredentialUpdateDialogComponent } from '../../components/update-dialog/update-dialog.component';
import { ProjectQueryParams } from '../../types';

@Component({
  selector: 'rc-credential-detail',
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CredentialDetailComponent {
  @Input()
  get identity(): CredentialIdentity {
    return this._params.id;
  }
  set identity(value: CredentialIdentity) {
    if (!value) {
      return;
    }

    this._params = {
      ...this._params,
      id: value,
    };
  }

  @Input()
  get info(): ProjectQueryParams {
    return this._params.info;
  }
  set info(value: ProjectQueryParams) {
    if (!value) {
      return;
    }

    this._params = {
      ...this._params,
      info: value,
    };
  }

  @Input()
  viewType: string;

  @Input()
  permissions: {
    canUpdate: boolean;
    canDelete: boolean;
  };

  @Output()
  deleted = new EventEmitter<void>();

  @Output()
  fetchError = new EventEmitter<void>();

  secretTypes = CredentialType;

  refresh$ = new Subject<void>();

  _params = {
    id: {
      name: '',
    },
    info: {},
  };

  constructor(
    private secretApi: CredentialApiService,
    private message: MessageService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private errorToastService: ErrorsToastService,
  ) {}

  fetchSecret = ({
    id,
    info,
  }: {
    id: CredentialIdentity;
    info: ProjectQueryParams;
  }) => {
    if (!id || !id.name) {
      return of(null);
    }
    if (this.viewType === 'user' && !info.space_name) {
      return of(null);
    }
    return this.secretApi.get(id, info.space_name).pipe(
      catchError((error: any) => {
        this.errorToastService.error(error);
        this.fetchError.emit();
        throw error;
      }),
    );
  };

  update() {
    const dialogRef = this.dialogService.open(CredentialUpdateDialogComponent, {
      data: {
        ...this.identity,
        space: this.info.space_name || '',
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.refresh$.next();
      }
    });
  }

  delete(item: Credential) {
    this.dialogService
      .confirm({
        title: this.translateService.get('secret_delete_confirm', {
          name: item.name,
        }),
        confirmType: ConfirmType.Danger,
        confirmText: this.translateService.get('delete'),
        cancelText: this.translateService.get('cancel'),
        beforeConfirm: (resolve, reject) =>
          this.secretApi
            .delete(item.name, this.info.space_name || '')
            .subscribe(
              () => {
                this.message.success(
                  this.translateService.get('secret_delete_succ'),
                );
                this.deleted.emit();
                resolve();
              },
              (e: any) => {
                this.errorToastService.error(e);
                reject();
              },
            ),
      })
      .catch(() => {});
  }
}
