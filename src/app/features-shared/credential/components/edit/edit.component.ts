import { MessageService, NotificationService } from '@alauda/ui';
import { HttpErrorResponse } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { head } from 'lodash-es';
import { Observable, of, throwError } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import {
  Credential,
  CredentialApiService,
  CredentialIdentity,
  CredentialType,
} from 'app/services/api/credential.service';
import { TranslateService } from 'app/translate/translate.service';
import { EditorStatus } from '../../types';

const defaultModel = (): Credential => ({
  name: '',
  namespace: '',
  displayName: '',
  type: null,
  ownerReferences: [],
});

@Component({
  selector: 'rc-credential-edit',
  templateUrl: 'edit.component.html',
  styleUrls: ['edit.component.scss'],
  exportAs: 'rc-credential-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CredentialEditComponent implements OnInit {
  @Input()
  name = '';

  @Input()
  namespace = '';

  @Input()
  mode: 'create' | 'update' = 'create';

  @Input()
  types: CredentialType[] = [
    CredentialType.BasicAuth,
    CredentialType.DockerConfig,
    CredentialType.OAuth2,
    CredentialType.SSH,
  ];

  @Input()
  showHint = false;

  @Input()
  space = '';

  @Output()
  saved = new EventEmitter<CredentialIdentity>();

  secretTypes = CredentialType;
  @Output()
  statusChange = new EventEmitter<EditorStatus>();

  model = defaultModel();

  @ViewChild('form')
  form: NgForm;

  showPassword: boolean;

  showClientSecret: boolean;

  showDockerPassword: boolean;

  noWhiteSpacePattern = /^\S*$/;

  constructor(
    private api: CredentialApiService,
    private notification: NotificationService,
    private message: MessageService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.prepareForm();
  }

  prepareForm() {
    this.statusChange.emit('loading');

    this.getModel()
      .pipe(
        tap(model => {
          this.model = model;
          this.form.resetForm(this.model);
        }),
      )
      .subscribe(() => {
        this.statusChange.emit('normal');
        this.cdr.markForCheck();
      });
  }

  save() {
    if (!this.form.valid) {
      return;
    }

    const handler =
      this.mode === 'create'
        ? this.api.post(this.model, this.space)
        : this.api.put(this.model, this.space);
    const succMessage =
      this.mode === 'create' ? 'secret_create_succ' : 'secret_update_succ';
    const errorMessage =
      this.mode === 'create' ? 'secret_create_fail' : 'secret_update_fail';

    this.statusChange.emit('saving');
    handler.subscribe(
      (result: Credential) => {
        this.statusChange.emit('normal');
        this.cdr.markForCheck();
        this.saved.emit(result);
        this.message.success({
          content: this.translate.get(succMessage),
        });
      },
      (e: any) => {
        this.statusChange.emit('normal');
        this.cdr.markForCheck();
        this.notification.error({
          title: this.translate.get(errorMessage),
          content: e.errors[0].message || e.errors[0].code,
        });
      },
    );
  }

  submit() {
    (this.form as any).submitted = true;
    this.form.ngSubmit.emit();
  }

  private getModel(): Observable<Credential> {
    if (this.mode === 'update' && !this.name) {
      return throwError({
        title: 'secret_edit_input_invalid',
        content: 'scret_edit_input_invalid_content',
        data: {},
      });
    }

    if (!this.name) {
      return of({
        ...defaultModel(),
        namespace: this.namespace || '',
        type: head(this.types),
      });
    }

    return this.api.get({ name: this.name }, this.space).pipe(
      catchError(
        (error: HttpErrorResponse): Observable<Credential> => {
          if (error.status === 404) {
            throw {
              title: 'secret_edit_not_found',
              content: 'secret_edit_not_found_content',
              data: {
                name: this.name,
              },
            };
          }

          throw {
            title: 'secret_edit_load_fail',
            content: 'secret_edit_load_fail_content',
            data: {
              name: this.name,
            },
          };
        },
      ),
    );
  }

  getPlaceholder(key: string) {
    return this.mode === 'update' ? this.translate.get(key) : '';
  }
}
