export * from './edit/edit.component';
export * from './create-dialog/create-dialog.component';
export * from './update-dialog/update-dialog.component';
export * from './list/list.component';
export * from './list-page/list-page.component';
export * from './detail/detail.component';
export * from './detail-page/detail-page.component';
