import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  Optional,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { get, toInteger } from 'lodash-es';
import { combineLatest, of } from 'rxjs';
import { filter, map, publishReplay, refCount, tap } from 'rxjs/operators';

import { QueryParams } from 'app/features-shared/credential/types';
import { Credential } from 'app/services/api/credential.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { AppState, getCurrentProjectDetail, getCurrentSpace } from 'app/store';
import { isAdminView } from 'app/utils/page';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  selector: 'rc-credential-list-page',
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CredentialListPageComponent implements OnInit {
  permissions = {
    canCreate: false,
    canUpdate: false,
    canDelete: false,
  };
  params$ = this.getParams();
  itemRoute = this.getItemRoute();

  project$ = this.store.select(getCurrentProjectDetail).pipe(
    filter(p => !!p),
    map(p => p.name),
  );

  space$ = this.store.select(getCurrentSpace).pipe(
    filter(s => !!s),
    map(s => s.name),
  );

  info$ = this.getInfo().pipe(
    tap(res => {
      const space_name = get(res, 'space_name');
      const context = space_name ? { space_name } : {};
      this.getPermissions(context);
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    @Optional() private workspaceComponent: WorkspaceComponent,
    private roleUtil: RoleUtilitiesService,
    private store: Store<AppState>,
  ) {}

  ngOnInit() {}

  onParamsChange(params: Partial<QueryParams>) {
    this.router.navigate([], {
      queryParams: params,
      queryParamsHandling: 'merge',
      relativeTo: this.route,
    });
  }

  getViewType() {
    let type: string;
    if (isAdminView()) {
      type = 'admin';
    } else if (this.workspaceComponent) {
      type = 'user';
    } else {
      type = 'project';
    }
    return type;
  }

  private getInfo() {
    switch (this.getViewType()) {
      case 'admin':
        return of({});
      case 'user':
        return combineLatest(this.project$, this.space$).pipe(
          map(([project_name, space_name]) => {
            return {
              project_name,
              space_name,
            };
          }),
        );
      case 'project':
        return this.project$.pipe(
          map(project_name => {
            return { project_name };
          }),
        );
    }
  }

  private getParams() {
    return this.route.queryParamMap.pipe(
      map(paramMap => {
        return {
          search_by: paramMap.get('search_by') || 'name',
          keywords: paramMap.get('keywords') || '',
          page: toInteger(paramMap.get('page')),
          page_size: toInteger(paramMap.get('page_size')),
          sort: paramMap.get('sort') || 'creationTimestamp',
          direction: paramMap.get('direction') || 'desc',
        };
      }),
      publishReplay(1),
      refCount(),
    );
  }

  private getItemRoute() {
    switch (this.getViewType()) {
      case 'admin':
      case 'user':
        return (item: Credential) => ['../', 'detail', item.name];
      case 'project':
        return (item: Credential) => ['../', 'credential', 'detail', item.name];
    }
  }

  async getPermissions(context: any) {
    [
      this.permissions.canCreate,
      this.permissions.canUpdate,
      this.permissions.canDelete,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_secret',
      context,
      ['create', 'update', 'delete'],
    );
  }
}
