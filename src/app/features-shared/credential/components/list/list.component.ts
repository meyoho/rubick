import {
  ConfirmType,
  DialogService,
  MessageService,
  PageEvent,
  Sort,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';

import { of, Subject } from 'rxjs';

import {
  Credential,
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { CredentialCreateDialogComponent } from '../../components/create-dialog/create-dialog.component';
import { CredentialUpdateDialogComponent } from '../../components/update-dialog/update-dialog.component';
import { ProjectQueryParams, QueryParams } from '../../types';

@Component({
  selector: 'rc-credential-list',
  templateUrl: 'list.component.html',
  styleUrls: ['list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CredentialListComponent implements OnChanges {
  @Input()
  get params(): QueryParams {
    return this._params.query;
  }
  set params(value: QueryParams) {
    if (!value) {
      return;
    }

    this._params = {
      ...this._params,
      query: value,
    };
    this.searchBy = value.search_by;
  }

  @Input()
  get info(): ProjectQueryParams {
    return this._params.info;
  }

  set info(value: ProjectQueryParams) {
    if (!value) {
      return;
    }
    this._params = {
      ...this._params,
      info: value,
    };
  }

  @Input()
  viewType: string;

  @Input()
  itemRoute: (item: Credential) => string[];

  @Input()
  permissions: {
    canCreate: boolean;
    canUpdate: boolean;
    canDelete: boolean;
  };
  @Output()
  paramsChange = new EventEmitter<Partial<QueryParams>>();

  get pageIndex() {
    return Math.max(0, this._params.query.page - 1);
  }

  get pageSize() {
    return Math.max(20, this._params.query.page_size);
  }

  get columns() {
    return ['name', 'type', 'creationTimestamp', 'actions'];
  }

  _params = {
    info: {},
    query: {
      search_by: 'name',
      keywords: '',
      sort: 'name',
      direction: 'asc',
      page: 1,
      page_size: 20,
    },
  };

  searchBy = this._params.query.search_by;

  refresh$ = new Subject<void>();

  constructor(
    private secretApi: CredentialApiService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private message: MessageService,
    private errorToastService: ErrorsToastService,
  ) {}

  fetchSecrets = ({
    info,
    query,
  }: {
    info: ProjectQueryParams;
    query: QueryParams;
  }) => {
    const params = this.getQueryParams(query);
    if (this.viewType === 'user' && !(info.project_name && info.space_name)) {
      return of(null);
    }
    return this.secretApi.find(params, info.project_name, info.space_name);
  };

  getQueryParams(query: QueryParams) {
    let name = '';
    let display_name = '';
    const keyword = query.keywords.trim();
    if (keyword !== '') {
      switch (query.search_by) {
        case 'name':
          name = keyword;
          break;
        case 'display_name':
          display_name = keyword;
          break;
      }
    }
    return {
      page: query.page || 1,
      page_size: query.page_size || 20,
      order_by: `${
        query.direction === 'desc' ? '-' : ''
      }metadata.${query.sort || 'name'}`, //todo: params
      name,
      display_name,
    };
  }

  secretTracker(_: number, secret: Credential) {
    return `${secret.namespace}/${secret.name}`;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (changes.params && changes.params.currentValue) {
      this.searchBy = changes.params.currentValue.search_by;
    }
  }

  onSortChange(sort: Sort) {
    this.paramsChange.emit({
      sort: sort.active,
      direction: sort.direction,
    });
  }

  onPageChange({ pageIndex, pageSize }: PageEvent) {
    this.paramsChange.emit({
      page: pageIndex + 1,
      page_size: pageSize,
    });
  }

  onSearch(keywords: string) {
    if (
      keywords !== this.params.keywords ||
      this.searchBy !== this.params.search_by
    ) {
      this.paramsChange.emit({
        search_by: this.searchBy,
        keywords,
        page: 1,
      });
    } else {
      this.refresh$.next();
    }
  }

  update(item: Credential) {
    const dialogRef = this.dialogService.open(CredentialUpdateDialogComponent, {
      data: {
        name: item.name,
        namespace: item.namespace,
        space: this.info.space_name || '',
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.refresh$.next();
      }
    });
  }

  delete(item: Credential) {
    this.dialogService
      .confirm({
        title: this.translate.get('secret_delete_confirm', { name: item.name }),
        confirmType: ConfirmType.Danger,
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) =>
          this.secretApi.delete(item.name, this.info.space_name).subscribe(
            () => {
              this.message.success(this.translate.get('secret_delete_succ'));
              this.refresh$.next();
              resolve();
            },
            (e: any) => {
              this.errorToastService.error(e);
              reject();
            },
          ),
      })
      .catch(() => {});
  }

  create() {
    const dialogRef = this.dialogService.open(CredentialCreateDialogComponent, {
      data: {
        types: [
          CredentialType.BasicAuth,
          CredentialType.DockerConfig,
          CredentialType.OAuth2,
          CredentialType.SSH,
        ],
        showHint: false,
        space: this.info.space_name,
      },
    });

    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.refresh$.next();
      }
    });
  }
}
