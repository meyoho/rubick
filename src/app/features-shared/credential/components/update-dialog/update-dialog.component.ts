import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { EditorStatus } from '../../types';

@Component({
  templateUrl: 'update-dialog.component.html',
  styleUrls: ['update-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CredentialUpdateDialogComponent {
  status: EditorStatus = 'normal';

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      namespace: string;
      name: string;
      space?: string;
    },
    private dialogRef: DialogRef<CredentialUpdateDialogComponent>,
  ) {}

  close(updated = false) {
    this.dialogRef.close(updated);
  }

  onStatusChange(status: EditorStatus) {
    this.status = status;
  }
}
