import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { SharedModule } from 'app/shared/shared.module';
import {
  CredentialCreateDialogComponent,
  CredentialDetailComponent,
  CredentialDetailPageComponent,
  CredentialEditComponent,
  CredentialListComponent,
  CredentialListPageComponent,
  CredentialUpdateDialogComponent,
} from './components';

const components = [
  CredentialEditComponent,
  CredentialCreateDialogComponent,
  CredentialUpdateDialogComponent,
  CredentialListComponent,
  CredentialListPageComponent,
  CredentialDetailComponent,
  CredentialDetailPageComponent,
];

@NgModule({
  imports: [CommonModule, RouterModule, SharedModule, FormsModule],
  declarations: components,
  exports: components,
  providers: [],
  entryComponents: [
    CredentialCreateDialogComponent,
    CredentialUpdateDialogComponent,
  ],
})
export class CredentialSharedModule {}
