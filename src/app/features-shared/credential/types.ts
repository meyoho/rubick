export interface ProjectQueryParams {
  project_name?: string;
  space_name?: string;
}

export interface QueryParams {
  search_by: string;
  keywords: string;
  sort: string;
  direction: string;
  page: number;
  page_size: number;
}

export type EditorStatus = 'normal' | 'loading' | 'saving';
