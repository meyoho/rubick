import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { EventComponent } from 'app2/features/event/dashboard/event.component';
import { K8sEventListComponent } from 'app2/features/event/k8s-event-list/k8s-event-list.component';
import { EventListComponent } from 'app2/features/event/list/event-list.component';

const components = [EventComponent, K8sEventListComponent, EventListComponent];

@NgModule({
  imports: [RouterModule, SharedModule],
  declarations: components,
  exports: components,
  entryComponents: [EventComponent],
})
export class EventSharedModule {}
