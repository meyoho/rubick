import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

interface ExecInfo {
  user: string;
  command: string;
}

@Component({
  templateUrl: './template.html',
})
export class ExecCommandDialogComponent implements OnInit {
  title = 'EXEC';
  constructor(
    @Inject(DIALOG_DATA)
    private dialogData: {
      title: string;
    },
  ) {
    if (this.dialogData && this.dialogData.title) {
      this.title = dialogData.title;
    }
  }

  @Output()
  close = new EventEmitter<ExecInfo>();
  @ViewChild(NgForm)
  form: NgForm;
  submitting = false;
  user = '';
  command = '/bin/sh';

  ngOnInit(): void {}

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.close.next({
      user: this.user,
      command: this.command,
    });
  }

  cancel() {
    this.close.next(null);
  }
}
