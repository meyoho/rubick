import { NgModule } from '@angular/core';

import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { ExecCommandDialogComponent } from './exec-command/component';

const EXPORT_COMPONENTS = [ExecCommandDialogComponent];

const ENTRY_COMPONENTS = [ExecCommandDialogComponent];

@NgModule({
  imports: [SharedModule, FormTableModule],
  exports: [...EXPORT_COMPONENTS],
  declarations: [...EXPORT_COMPONENTS],
  providers: [],
  entryComponents: [...ENTRY_COMPONENTS],
})
export class ExecSharedModule {}
