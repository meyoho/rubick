import { DIALOG_DATA } from '@alauda/ui';
import { Inject } from '@angular/core';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  OnInit,
  Optional,
  Output,
} from '@angular/core';

import { clone, get, sortBy } from 'lodash-es';
import {
  BehaviorSubject,
  combineLatest,
  from,
  NEVER,
  Observable,
  of,
} from 'rxjs';
import {
  catchError,
  debounceTime,
  map,
  publishReplay,
  refCount,
  share,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import {
  ImageProject as _ImageProject,
  ImageProjectService,
} from 'app/services/api/image-project.service';
import { ImageRegistryService } from 'app/services/api/image-registry.service';
import {
  ImageRepository,
  ImageRepositoryService,
} from 'app/services/api/image-repository.service';
import { RegistryApiService } from 'app/services/api/registry/registry-api.service';
import { CreateAppParams } from 'app/services/api/registry/registry-api.types';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, Weblabs } from 'app/typings';
import { RcImageSelection } from 'app/typings/k8s-form-model';
import { getImageSelectionParamsFromInput } from '../utils';
type ImageProject = _ImageProject & { is_default?: boolean };
@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class ImageSelectComponent implements OnInit {
  @Output()
  close = new EventEmitter<any>();
  private registries: any[] = [];
  private thirdPartyName: string;

  initialized: boolean;
  visibleRegistries: any[] = [];
  moreRegistries: any[] = [];
  visibleProjects: any = [];
  moreProjects: any[] = [];
  repositories: any[] = [];

  filterFnSubject$: BehaviorSubject<any>;
  repositoriesWrap$: BehaviorSubject<any>;
  repositories$: Observable<any>;
  filteredRepositories$: Observable<any>;

  currentRegistry: any;
  currentProject: any;
  currentRepository: ImageRepository;

  thirdPartyRepositoryPath: string;
  demoRepositoryAddress = 'index.docker.io/library/ubuntu:latest';

  loading = false;

  additionalRegistry: any;
  additionalProject: any;

  filterKey: string;

  imageTags: {
    [key: string]: string[];
  } = {};
  currentImageTag: {
    [key: string]: string;
  } = {};

  imageParams: CreateAppParams;

  private defaultProject: ImageProject;

  private updateHarborRepo$ = new BehaviorSubject<void>(null);
  harborRepositories$: Observable<any>;

  constructor(
    @Optional() protected cdr: ChangeDetectorRef,
    private translateService: TranslateService,
    private registryService: ImageRegistryService,
    private repositoryService: ImageRepositoryService,
    private imageProjectService: ImageProjectService,
    @Inject(ACCOUNT) public account: Account,
    @Inject(DIALOG_DATA)
    public data: {
      projectName: string;
      spaceName: string;
    },
    @Inject(WEBLABS) private weblabs: Weblabs,
    private registryApi: RegistryApiService,
  ) {
    this.thirdPartyName = this.translateService.get('third_party');
  }

  ngOnInit() {
    this.defaultProject = {
      project_name: this.translateService.get('default_project_name'),
      is_default: true,
    } as ImageProject;

    this.getRegistries();
    this.getHarborProjects();
    this.filterFnSubject$ = new BehaviorSubject((list: any) => {
      return list;
    });

    this.repositoriesWrap$ = new BehaviorSubject([]);
    this.repositories$ = this.repositoriesWrap$.pipe(
      switchMap((repos: any[]) => {
        return repos;
      }),
      startWith([]),
    );
    this.filteredRepositories$ = combineLatest(
      this.repositories$,
      this.filterFnSubject$.pipe(debounceTime(200)),
    ).pipe(
      map(([repositories, filterFn]) => {
        return repositories.filter(filterFn);
      }),
      share(),
    );

    this.initialized = true;
  }

  confirm() {
    const imageSelection = this.getImageSelection();
    this.close.next(imageSelection);
  }

  cancel() {
    this.close.next(null);
  }

  private getImageSelection() {
    if (this.isThirdParty) {
      return getImageSelectionParamsFromInput(this.thirdPartyRepositoryPath);
    } else if (this.isHarborRegistry) {
      return this.imageParams;
    } else {
      const imageSelection: RcImageSelection = {
        registry_name: this.currentRegistry.name,
        registry_endpoint: this.currentRegistry.endpoint,
        is_public_registry: this.currentRegistry.is_public,
        repository_name: this.currentRepository.name,
        tag: this.currentImageTag[this.currentRepository.name],
        is_third: get(this.currentRepository, 'registry.is_third') || null,
      };
      if (this.currentRegistry.is_public) {
        imageSelection.full_image_name = `${imageSelection.registry_endpoint}/${
          this.account.namespace
        }/${imageSelection.repository_name}`;
      } else {
        if (this.currentProject.is_default) {
          imageSelection.full_image_name = `${
            imageSelection.registry_endpoint
          }/${imageSelection.repository_name}`;
        } else {
          imageSelection.project_name = this.currentProject.project_name;
          imageSelection.full_image_name = `${
            imageSelection.registry_endpoint
          }/${imageSelection.project_name}/${imageSelection.repository_name}`;
        }
      }
      return imageSelection;
    }
  }

  filterKeyChange() {
    this.filterFnSubject$.next((repo: any) => {
      return repo.name.includes(this.filterKey);
    });
  }

  repositorySelected(repo: ImageRepository) {
    if (this.currentRepository && repo.name === this.currentRepository.name) {
      return;
    }
    this.currentRepository = repo;
    if (!this.imageTags[repo.name]) {
      // retrive image tags
      this.getImageTags();
    }
  }

  async projectChange(project: any, additional = false) {
    if (additional) {
      if (
        this.currentProject &&
        project.project_name === this.currentProject.project_name
      ) {
        return;
      }
      this.additionalProject = project;
    } else {
      this.additionalProject = null;
    }
    this.currentRepository = null;

    this.currentProject = project;
    this.repositoriesWrap$.next(from(this.getRepositories(project)));
  }

  async registryChange(registry: any, additional = false) {
    if (additional) {
      if (this.currentRegistry && registry.name === this.currentRegistry.name) {
        return;
      }
      this.additionalRegistry = registry;
    } else {
      this.additionalRegistry = null;
    }
    this.currentRegistry = registry;
    if (registry.registry_type === 'Harbor' && this.weblabs.GROUP_ENABLED) {
      this.updateHarborRepo$.next(null);
    } else {
      await this.getProjects(registry);
    }
    this.typeChange();
  }

  getHarborProjects() {
    this.harborRepositories$ = combineLatest(
      of(this.data.projectName),
      of(this.data.spaceName),
      this.updateHarborRepo$,
    ).pipe(
      tap(() => {
        this.loading = true;
      }),
      switchMap(([project, space]) =>
        this.registryApi
          .findRepositoriesByProjectAndSpace(project, space || '', {})
          .pipe(
            catchError(() => {
              this.loading = false;
              this.cdr.markForCheck();
              return NEVER;
            }),
          ),
      ),
      map(list =>
        list.items.filter(
          (item: any) =>
            item.type === 'Harbor' &&
            item.imageRegistry === this.currentRegistry.name,
        ),
      ),
      tap(() => {
        this.loading = false;
      }),
      publishReplay(1),
      refCount(),
    );
  }

  shouldEnableConfirmButton() {
    return (
      (!this.isThirdParty &&
        !this.isHarborRegistry &&
        this.currentRepository &&
        this.currentImageTag[this.currentRepository.name]) ||
      (this.isThirdParty && this.thirdPartyRepositoryPath) ||
      (this.isHarborRegistry &&
        this.imageParams.full_image_name &&
        this.imageParams.tag)
    );
  }

  get isThirdParty() {
    return (
      this.currentRegistry && this.currentRegistry.name === this.thirdPartyName
    );
  }

  get isHarborRegistry() {
    return (
      this.currentRegistry &&
      this.currentRegistry.registry_type &&
      this.currentRegistry.registry_type === 'Harbor'
    );
  }

  private async getImageTags() {
    let tags = await this.repositoryService.getRepositoryTags({
      registryName: this.currentRegistry.name,
      projectName: this.currentProject.is_default
        ? ''
        : this.currentProject.project_name,
      repositoryName: this.currentRepository.name,
      params: {
        is_third: this.currentRegistry.is_third,
      },
    });
    tags = this.currentRegistry.is_third
      ? tags.map((item: any) => {
          return item.tag_name;
        })
      : tags;
    this.imageTags[this.currentRepository.name] = tags;
    if (tags.length) {
      this.currentImageTag[this.currentRepository.name] = tags[0];
    }
  }

  private async getRepositories(project: any) {
    if (!project) {
      return [];
    }
    if (!project.repositories) {
      this.loading = true;
      const project_name = project.is_default
        ? undefined
        : project.project_name;
      const repositories = await this.repositoryService.getRepositories({
        registryName: this.currentRegistry.name,
        projectName: project_name,
        params: {
          is_third: !!this.currentRegistry.is_third,
        },
      });
      project.repositories = repositories;
      this.loading = false;
    }
    return project.repositories;
  }

  private async getProjects(registry: any) {
    if (registry.name === this.thirdPartyName) {
      return;
    }
    if (!registry.projects) {
      let projects = await this.imageProjectService.getProjects(
        registry.name,
        registry.is_third,
      );
      projects = registry.is_third
        ? projects
        : [clone(this.defaultProject)].concat(projects);
      projects = sortBy(projects, (item: ImageProject) => {
        return item.project_name;
      });
      registry.projects = projects;
      if (projects.length > 5) {
        registry.visibleProjects = projects.slice(0, 4);
        registry.moreProjects = projects.slice(4);
      } else {
        registry.visibleProjects = projects;
        registry.moreProjects = [];
      }
    }
    this.visibleProjects = registry.visibleProjects;
    this.moreProjects = registry.moreProjects;
    this.projectChange(registry.projects[0]);
  }

  private async getHarborRegistry() {
    let result: any[];
    try {
      const imageRegistryList = await this.registryApi
        .findRepositoriesByProjectAndSpace(
          this.data.projectName,
          this.data.spaceName || '',
          {},
        )
        .pipe(
          map(list =>
            list.items
              .filter(item => item.type === 'Harbor')
              .map(item => item.imageRegistry),
          ),
        )
        .toPromise();
      result = [...new Set(imageRegistryList)].map(imageRegistry => {
        return {
          name: imageRegistry,
          display_name: imageRegistry,
          registry_type: 'Harbor',
        };
      });
    } catch (e) {
      result = [];
    }
    return result;
  }

  private getRegistries() {
    Promise.all([this.getHarborRegistry(), this.registryService.find()])
      .then(([harborRegistry, registries]) => {
        registries = sortBy(registries, item => item.name);
        if (harborRegistry.length > 0) {
          registries = registries.concat(harborRegistry as any);
        }
        registries.push({
          name: this.thirdPartyName,
          display_name: this.thirdPartyName,
        } as any);
        this.registries = registries;
        if (this.registries.length) {
          this.registryChange(this.registries[0]);
        }
        if (this.registries.length > 5) {
          this.visibleRegistries = this.registries.slice(0, 4);
          this.moreRegistries = this.registries.slice(4);
        } else {
          this.visibleRegistries = this.registries;
        }
      })
      .catch(() => {
        this.visibleRegistries = [];
        this.moreRegistries = [];
      });
  }

  typeChange() {
    this.imageParams = {
      full_image_name: null,
      repository_name: null,
      registry_endpoint: null,
      secret: null,
      tag: null,
    };
  }
}
