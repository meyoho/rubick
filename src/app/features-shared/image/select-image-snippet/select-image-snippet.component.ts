import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import { ImageTag } from 'app/services/api/pipeline';
import {
  CreateAppParams,
  ImageRepository,
} from 'app/services/api/registry/registry-api.types';

@Component({
  selector: 'rc-select-image-snippet',
  templateUrl: './select-image-snippet.component.html',
  styleUrls: ['./select-image-snippet.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SelectImageSnippetComponent {
  loading = false;
  tagOptions: ImageTag[];

  @Input()
  imageParams: CreateAppParams;
  @Input()
  repositories: any;

  selectedRepoChange(repo: ImageRepository) {
    this.tagOptions = repo.tags;
    this.imageParams.tag = repo.tags[0].name;
    this.imageParams.repository_name = repo.image.split('/')[1];
    this.imageParams.registry_endpoint = repo.endpoint;
    this.imageParams.secret = repo.secretName;
  }
}
