import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { SharedModule } from 'app/shared/shared.module';
import { ImageSelectComponent } from './image-select/component';
import { SelectImageSnippetComponent } from './select-image-snippet/select-image-snippet.component';

@NgModule({
  imports: [CommonModule, FormsModule, SharedModule],
  declarations: [SelectImageSnippetComponent, ImageSelectComponent],
  exports: [SelectImageSnippetComponent, ImageSelectComponent],
  entryComponents: [SelectImageSnippetComponent, ImageSelectComponent],
})
export class ImageSharedModule {}
