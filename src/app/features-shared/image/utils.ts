import { RcImageSelection } from 'app/typings/k8s-form-model';
export function getImageSelectionParamsFromInput(
  imageAddressInput: string,
): RcImageSelection {
  const parts = imageAddressInput.split('/');
  let full_image_name = '';
  const length = parts.length;
  const image = parts[length - 1];
  const [name, tag] = image.split(':');
  if (tag) {
    parts[length - 1] = name;
    full_image_name = parts.join('/');
  } else {
    full_image_name = imageAddressInput;
  }
  return {
    registry_endpoint: parts[0],
    is_public_registry: true,
    registry_name: '',
    project_name: '',
    repository_name: name,
    tag: tag || 'latest',
    full_image_name,
    is_third: false,
  };
}
