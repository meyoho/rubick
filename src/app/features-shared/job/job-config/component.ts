import { Component, Input } from '@angular/core';

import { CronJob, JobSpec } from 'app/typings';

@Component({
  selector: 'rc-job-config',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class JobConfigComponent {
  @Input()
  jobSpec: JobSpec;

  @Input()
  showJobType = true;

  resource: CronJob;

  getJobType() {
    if (this.jobSpec.completions) {
      return 'fixed_number_job';
    }
    if (this.jobSpec.parallelism) {
      return 'parallel_job';
    }
    return 'single_job';
  }
}
