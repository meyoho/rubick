import { DialogService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  GenericStatus,
  Job,
  K8sResourceWithActions,
  StringMap,
} from 'app/typings';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

export interface JobQueryParams {
  activeTabIndex?: number;
  pod?: string;
  container?: string;
}
@Component({
  selector: 'rc-job-record-list',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  providers: [ResourceListPermissionsService],
})
export class JobRecordListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  @Input()
  hasRerfencesJobRecord = true;
  @Input()
  labelSelector: StringMap;
  private onDestroy$ = new Subject<any>();
  clusterName: string;
  namespaceName: string;
  columns = ['name', 'references_cron_job', 'status', 'create_time', 'action'];
  initialized: boolean;
  RESOURCE_TYPE = 'jobs';
  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private k8sResourceService: K8sResourceService,
    private workspaceComponent: WorkspaceComponent,
    private roleUtil: RoleUtilitiesService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    public resourcePermissionService: ResourceListPermissionsService,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    if (!this.hasRerfencesJobRecord) {
      this.columns = ['name', 'status', 'create_time', 'action'];
    }
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.resourcePermissionService.initContext({
      resourceType: 'k8s_others',
      context: {
        project_name: params.project,
        cluster_name: params.cluster,
        namespace_name: params.namespace,
      },
    });
    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
  }
  ngOnDestroy() {
    this.onDestroy$.next();
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    const labelSelector = this.labelSelector
      ? Object.keys(this.labelSelector)
          .map(key => `${key}=${this.labelSelector[key]}`)
          .join(',')
      : '';
    return from(
      this.k8sResourceService.getK8sResourcesPaged(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        namespace: this.namespaceName,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
        labelSelector,
      }),
    );
  }

  canShowDelete(item: K8sResourceWithActions<Job>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'delete');
  }

  viewLogs(item: K8sResourceWithActions<Job>) {
    this.router.navigate(
      ['job_record', 'detail', item.kubernetes.metadata.name],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
        queryParams: { activeTabIndex: 4, pod: '' },
      },
    );
  }

  async delete(item: K8sResourceWithActions<Job>) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('delete_job_record_confirm', {
          name: ` "${item.kubernetes.metadata.name}" `,
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        item.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translate.get('success'),
        content: this.translate.get('delete_job_record_success', {
          name: item.kubernetes.metadata.name,
        }),
      });
      this.onUpdate(null);
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  pageChanged(param: string, value: number) {
    this.onPageEvent({ [param]: value });
  }

  openDetail(item: K8sResourceWithActions<Job>) {
    this.router.navigate(
      ['job_record', 'detail', item.kubernetes.metadata.name],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      },
    );
  }

  openCronJobDetail(name: string) {
    this.router.navigate(['cron_job', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  trackByFn(_index: number, item: K8sResourceWithActions<Job>) {
    return item.kubernetes.metadata.uid;
  }

  getJobStatus(item: K8sResourceWithActions<Job>) {
    const status = item.kubernetes.status;
    if (
      status.conditions &&
      status.conditions.find(_item => _item.type === 'Failed')
    ) {
      return {
        icon: GenericStatus.Error,
        job: 'execute_failed',
      };
    }
    if (status.active) {
      return {
        icon: GenericStatus.Running,
        job: 'executing',
      };
    }
    if (status.succeeded) {
      return {
        icon: GenericStatus.Success,
        job: 'execute_succeeded',
      };
    }
    if (status.failed) {
      return {
        icon: GenericStatus.Error,
        job: 'execute_failed',
      };
    }
    return {
      icon: GenericStatus.Unknown,
      job: 'unknown',
    };
  }

  getOwnerCronJob(item: K8sResourceWithActions<Job>) {
    const ownerReferences = item.kubernetes.metadata.ownerReferences;
    if (ownerReferences) {
      const ownerCronJob = ownerReferences.find(
        reference => reference.kind === 'CronJob',
      );
      return ownerCronJob
        ? { link: true, name: ownerCronJob.name }
        : {
            link: false,
            name: `${ownerReferences[0].kind}:${ownerReferences[0].name}`,
          };
    }
    return { link: false, name: '-' };
  }
}
