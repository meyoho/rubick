import { NgModule } from '@angular/core';

import { JobConfigComponent } from 'app/features-shared/job/job-config/component';
import { JobRecordListComponent } from 'app/features-shared/job/job-record-list/component';
import { SharedModule } from 'app/shared/shared.module';

const EXPORT_COMPONENTS = [JobConfigComponent, JobRecordListComponent];

@NgModule({
  imports: [SharedModule],
  exports: [...EXPORT_COMPONENTS],
  declarations: [...EXPORT_COMPONENTS],
  providers: [],
})
export class JobSharedModule {}
