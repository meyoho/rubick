import { NotificationService } from '@alauda/ui';
import { Component, Inject, Input, OnInit, Optional } from '@angular/core';

import * as Highcharts from 'highcharts';
import { get, mapValues } from 'lodash-es';
import moment from 'moment';

import { AccountService } from 'app/services/api/account.service';
import {
  LogChartParam,
  LogService,
  QueryConditionService,
  Types,
} from 'app/services/api/log.service';
import { LoggerUtilitiesService } from 'app/services/logger.service';
import { ACCOUNT, ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, Environments } from 'app/typings';
import { SerieChartData } from 'app/typings/chart.types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { TIME_STAMP_OPTIONS } from '../log-constant';

@Component({
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss'],
})
export class DashboardComponent implements OnInit {
  @Input()
  service: any;
  @Input()
  source: any;

  get isUserView(): boolean {
    return this.accountService.isUserView();
  }

  loading = false;
  initialized = false;
  loadingTypes = false;
  timeRangeOutOfDate = false;
  hasQueryString = false;
  loadingLogs = false;
  loadingSeries = false;
  showChart = false;
  logTypes: Types;
  tags: Array<any> = [];
  tagRegex = /\w+: .+/;
  _bucketsInterval = 0; // interval of two buckets
  Highcharts = Highcharts;
  chartOptions = {
    chart: {
      type: 'column',
    },
    credits: {
      enabled: false,
    },
    legend: {
      enabled: false,
    },
    title: {
      text: <string>null,
    },
    tooltip: {
      xDateFormat: '%Y-%m-%d %H:%M:%S',
    },
    plotOptions: {
      series: {
        borderRadius: 1,
        color: '#8bd6fa',
        events: {
          click: this.onChartSelect.bind(this),
        },
      },
    },
    xAxis: {
      dateTimeLabelFormats: {
        day: '%m-%d',
      },
      type: 'datetime',
      crosshair: true,
    },
    yAxis: {
      title: {
        text: <string>null,
      },
    },
  };

  suggesstions: Array<any>;
  trackFn = (val: any = {}) => {
    if (val.type) {
      return `${val.type}: ${val.name}`;
    } else if (this.tagRegex.test(val)) {
      return val;
    } else {
      return `search: ${val}`;
    }
  };
  filterFn = (filter: string, option: any) =>
    (option.value.name || option.value).includes(filter);
  current_time = moment();
  start_time = this.current_time
    .clone()
    .startOf('day')
    .subtract(6, 'days');
  queryDates = {
    start_time: this.start_time.valueOf(),
    end_time: this.current_time.valueOf(),
  };
  queryDatesShown = {
    start_time: this.logService.dateNumToStr(this.queryDates.start_time),
    end_time: this.logService.dateNumToStr(this.queryDates.end_time),
  };
  dateTimeOptions = {
    maxDate: this.current_time
      .clone()
      .endOf('day')
      .valueOf(),
    minDate: this.start_time.valueOf(),
    enableTime: true,
    enableSeconds: true,
    time_24hr: true,
  };
  logs: Array<any> = [];
  timestampOptions = TIME_STAMP_OPTIONS.map(option => ({
    ...option,
    name: this.translateService.get(option.type),
  }));
  timestampOption = 'last_30_minites';
  pagination = {
    count: 0,
    size: 50,
    current: 1,
    pages: 0,
  };
  logCount: string;
  chartData: SerieChartData[];
  constructor(
    @Inject(ACCOUNT) public account: Account,
    @Inject(ENVIRONMENTS) private env: Environments,
    @Optional() private workspaceComponent: WorkspaceComponent,
    private accountService: AccountService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private logService: LogService,
    private logger: LoggerUtilitiesService,
  ) {}

  async ngOnInit() {
    this.initQueryTimeRange();
    this.initSuggestions();
    this.search();
    this.initialized = true;
  }

  private initQueryTimeRange() {
    if (this.env.log_ttl) {
      this.dateTimeOptions.minDate = this.current_time
        .clone()
        .startOf('day')
        .subtract(parseInt(this.env.log_ttl, 10) - 1, 'days')
        .valueOf();
    }
  }

  private async initSuggestions() {
    this.loadingTypes = true;
    try {
      if (this.isUserView) {
        this.logTypes = await this.logService.getTypes(
          this.workspaceComponent.baseParamsSnapshot.project,
        );
      } else {
        this.logTypes = await this.logService.getTypes();
      }
    } catch (e) {
      this.logger.error('Cant Load Log Types URL');
    }
    const logTypeMap = mapValues(
      this.logTypes,
      (items: Array<QueryConditionService>, type: string) => {
        return items.map(item => ({
          type: type.endsWith('s') ? type.slice(0, -1) : type,
          name: item,
        }));
      },
    );
    this.suggesstions = Object.values(logTypeMap).reduce(
      (acc, cur) => acc.concat(cur),
      [],
    );
    this.loadingTypes = false;
  }

  private checkQueryDates() {
    if (!this.queryDates.start_time || !this.queryDates.end_time) {
      this.auiNotificationService.warning(
        this.translateService.get('log_query_timerange_required'),
      );
      return false;
    }
    if (
      this.timestampOption === 'custom_time_range' &&
      this.queryDates.start_time >= this.queryDates.end_time
    ) {
      this.auiNotificationService.warning(
        this.translateService.get('log_query_timerange_warning'),
      );
      return false;
    }
    return true;
  }

  currentPageChange(_currentPage: number) {
    this.search();
  }

  pageSizeChange(size: number) {
    if (Math.ceil(this.pagination.count / size) < this.pagination.pages) {
      this.pagination.current = 1;
    }
    this.search();
  }

  fetchLogs() {
    this.pagination.current = 1;
    this.search();
  }

  search() {
    if (this.timestampOption !== 'custom_time_range') {
      this.resetTimeRange();
    }
    if (!this.checkQueryDates()) {
      return;
    }
    this.timeRangeOutOfDate = false;

    let payload = {
      pageno: this.pagination.current,
      size: this.pagination.size,
      ...this.queryDates,
      ...this.logService.generateQuerysFromTagObjects(this.tags),
    };

    payload = this.paramsCompletion(payload);

    this.getLogs(payload);
    this.getLogsEvents(payload);
  }

  onTimestampChanged(type: string) {
    this.timeRangeOutOfDate = false;
    this.timestampOption = type;
    this.fetchLogs();
  }

  addKeywordByClick(type: string, value: string) {
    if (
      !this.tags.find((element: any) => {
        return element.name === value;
      })
    ) {
      this.tags = this.tags.concat([
        {
          type: type,
          name: value,
        },
      ]);
    }
  }

  tagClassFn(_label: string, tag: any) {
    // init aui-multi-select tagClass
    if (tag.type) {
      return `tag-${tag.type}`;
    } else if (typeof tag === 'string' && /\w+: .+/.test(tag)) {
      return `tag-${/(\w+): .+/.exec(tag)[1]}`;
    }
  }

  private getLogs(payload: any) {
    this.loadingLogs = true;
    return this.logService
      .logsSearch(payload)
      .then(data => {
        if (!data.logs) {
          return;
        }
        this.pagination.count =
          data.total_items > 10000 ? 10000 : data.total_items;
        this.pagination.pages = Math.ceil(
          this.pagination.count / this.pagination.size,
        );
        this.logs = data.logs;
      })
      .catch((e: any) => {
        this.logger.error('Can not load logs', e);
        // clear previously loaded logs if server error exists
        this.logs = [];
      })
      .then(() => {
        this.loadingLogs = false;
      });
  }

  private getLogsEvents(payload: any) {
    this.loadingSeries = true;
    this.showChart = false;
    return this.logService
      .getAggregations(payload)
      .then(data => {
        if (!get(data, 'buckets.length')) {
          // the buckets length can't equal 0 except time offset is 0
          return;
        }
        this.showChart = true;
        const buckets = data.buckets;
        buckets.forEach((bucket: any) => {
          const _date = new Date(bucket.time * 1000);
          bucket.time = _date.getTime();
        });
        if (buckets.length > 2) {
          this._bucketsInterval = buckets[1].time - buckets[0].time;
        }
        const totalCount = buckets.reduce(
          (result: any, bucket: any) => result + bucket.count,
          0,
        );
        this.renderLogEventsChart({ totalCount, buckets });
      })
      .catch((e: any) => {
        this.logger.error('Can not load log aggregation', e);
        this.chartData = [];
        this.pagination.count = 0;
      })
      .then(() => {
        this.loadingSeries = false;
      });
  }

  private paramsCompletion(payload: any) {
    // 在用户视角下设置固定的projects
    if (this.isUserView) {
      payload.projects = this.workspaceComponent.baseParamsSnapshot.project;
    }
    // 补充 paths
    if (!payload.paths) {
      payload.paths = this.tags.filter((tag: any) => tag.type === 'path');
    }
    // 补充 query_string
    this.hasQueryString = false;
    if (payload.search) {
      this.hasQueryString = true;
      payload.query_string = payload.search;
      delete payload.search;
    }
    payload.start_time /= 1000;
    payload.end_time /= 1000;
    return payload;
  }

  private resetTimeRange() {
    this.queryDates = this.logService.getTimeRangeByRangeType(
      this.timestampOption,
    );
    this.fillCalendar(this.queryDates.start_time, this.queryDates.end_time);
  }

  fillCalendar(start_time: number, end_time: number) {
    this.queryDatesShown.start_time = this.logService.dateNumToStr(start_time);
    this.queryDatesShown.end_time = this.logService.dateNumToStr(end_time);
  }

  onStartTimeSelect(event: any) {
    this.queryDates.start_time = this.logService.dateStrToNum(event);
  }

  onEndTimeSelect(event: any) {
    this.queryDates.end_time = this.logService.dateStrToNum(event);
  }

  private renderLogEventsChart({
    totalCount = 0,
    buckets = [],
  }: LogChartParam) {
    this.logCount = totalCount + ' Logs';
    this.chartData = buckets.map(bucket => ({
      name: bucket.time,
      value: bucket.count,
    }));
    this.chartOptions['series'] = [
      {
        name: this.translateService.get('log'),
        data: buckets.map(bucket => ({
          x: bucket.time,
          y: bucket.count,
        })),
      },
    ];
  }

  onChartSelect(event: any) {
    this.timestampOption = 'custom_time_range';
    this.queryDates.start_time = event.point.x;
    this.queryDates.end_time =
      this.queryDates.start_time + this._bucketsInterval;
    this.fillCalendar(this.queryDates.start_time, this.queryDates.end_time);
    this.fetchLogs();
  }

  getTip(val: Date) {
    return moment(val).format('YYYY-MM-DD HH:mm:ss');
  }
}
