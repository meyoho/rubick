import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { DashboardComponent } from './dashboard/dashboard.component';

@NgModule({
  imports: [SharedModule, RouterModule],
  declarations: [DashboardComponent],
  exports: [DashboardComponent],
})
export class LogSharedModule {}
