import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';

import { FormTableModule } from 'app/shared/form-table/moduel';
import { DeleteNamespaceComponent } from 'app_user/features/namespace/delete/component';
import { UpdateQuotaComponent } from 'app_user/features/namespace/update-quota/component';
import { MultipleQuotaSettingsComponent } from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';

const EXPORT_COMPONENTS = [
  MultipleQuotaSettingsComponent,
  UpdateQuotaComponent,
  DeleteNamespaceComponent,
];

@NgModule({
  imports: [SharedModule, FormTableModule],
  exports: [...EXPORT_COMPONENTS],
  declarations: [...EXPORT_COMPONENTS],
  providers: [],
  entryComponents: [DeleteNamespaceComponent, UpdateQuotaComponent],
})
export class NamespaceSharedModule {}
