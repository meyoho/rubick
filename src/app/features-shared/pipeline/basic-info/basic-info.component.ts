import { ChangeDetectionStrategy, Component, Input } from '@angular/core';
import {
  mapTriggerIcon,
  mapTriggerTranslateKey,
} from 'app/features-shared/pipeline/utils.ts';
import { PipelineConfig } from 'app/services/api/pipeline';

@Component({
  selector: 'rc-basic-info',
  templateUrl: './basic-info.component.html',
  styleUrls: ['../shared-style/fields.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BasicInfoComponent {
  @Input()
  pipeline: PipelineConfig;
  mapTriggerTranslateKey: Function = mapTriggerTranslateKey;
  mapTriggerIcon = mapTriggerIcon;
  constructor() {}
}
