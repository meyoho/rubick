import { AbstractControl, FormControl, Validators } from '@angular/forms';

import { extend } from 'lodash-es';

const templateModel = {
  template: [null, Validators.required],
};

const basicModel = {
  name: [
    '',
    [
      Validators.required,
      Validators.maxLength(36),
      Validators.pattern('[a-z]([-a-z0-9]*[a-z0-9])?'),
    ],
  ],
  display_name: ['', [Validators.maxLength(100)]],
  jenkins_instance: ['', Validators.required],
  source: 'repo',
  run_policy: 'Serial',
};

const jenkinsfileModel = {
  repo: [
    { repo: '', secret: '', bindingRepository: '' },
    [Validators.required, codeRepositoryValidator],
  ],
  branch: ['master', Validators.required],
  path: ['Jenkinsfile', Validators.required],
};

const scriptModel = {
  script: ['', Validators.required],
};

const codeTriggerModel = {
  enabled: false,
  cron_string: ['H/2 * * * *', Validators.maxLength(20)],
};

const cronTriggerModel: any = {
  enabled: false,
  cron_string: '',
  cron_object: [{ days: {}, times: [] }, [cronRuleValidator]],
  sourceType: 'select',
};

export const MODEL: any = {
  templateModel,
  basicModel,
  jenkinsfileModel,
  scriptModel,
  codeTriggerModel,
  cronTriggerModel,
};

function codeRepositoryValidator(
  control: FormControl,
): null | { [key: string]: any } {
  const value = control.value;
  if (!value.repo && !value.bindingRepository) {
    return { needRepositoryValue: true };
  } else {
    return null;
  }
}

export function cronRuleValidator(
  control: FormControl,
): null | { [key: string]: any } {
  const value = control.value;
  if (control.validator) {
    const validator = control.validator({} as AbstractControl);
    if (validator && validator.required && value) {
      let err = {};
      const days = Object.keys(value.days).map(k => {
        return value.days[k];
      });
      if (!days.includes(true)) {
        err = { leastOneDay: true };
      }
      if (!value.times.length) {
        err = extend(err, { leastOneTime: true });
      }
      return err;
    } else {
      return null;
    }
  }
  return null;
}

export const DROPDOWN_TYPES = [
  'alauda.io/imagetag',
  'alauda.io/jenkinscredentials',
  'alauda.io/coderepostiory',
  'alauda.io/clustername',
  'alauda.io/k8snamespace',
  'alauda.io/applicationName',
  'alauda.io/componentName',
  'alauda.io/componentClass',
  'alauda.io/containerName',
  'alauda.io/toolbinding',
  'alauda.io/codebranch',
];

export const INPUT_DROPDOWN_TYPES: string[] = ['alauda.io/codebranch'];

export const STYLE_ICONS = [
  'ant',
  'golang',
  'maven',
  'java',
  'kubernetes',
  'python',
  'nodejs',
  'sonarqube',
  'docker',
];

export enum STYLE_ICONS_NAME_MAP {
  ant = 'Ant',
  golang = 'Golang',
  maven = 'Maven',
  java = 'Java',
  kubernetes = 'Kubernetes',
  python = 'Python',
  nodejs = 'Node.js',
  sonarqube = 'SonarQube',
  docker = 'Docker',
}
