import { Injectable } from '@angular/core';

import { ControlMapper, FormState } from 'alauda-ui-dynamic-forms';
import { find, get } from 'lodash-es';
import { BehaviorSubject, from, of } from 'rxjs';
import { map, switchMap, take } from 'rxjs/operators';

import { AppService } from 'app/services/api/app.service';
import {
  Credential,
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import {
  Namespace,
  NamespaceService,
} from 'app/services/api/namespace.service';
import { PipelineApiService } from 'app/services/api/pipeline';
import {
  ClusterInProject,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { K8sResourceWithActions } from 'app/typings/backend-api';
import { filterBy, getQuery } from 'app/utils/query-builder';
import { PodControllerKinds } from '../app/utils/pod-controller';

@Injectable()
export class PipelineControlTypesService {
  private _project$: BehaviorSubject<string> = new BehaviorSubject<string>(
    null,
  );
  private _space$: BehaviorSubject<string> = new BehaviorSubject<string>(null);

  set project(project: string) {
    this._project$.next(project);
  }

  set space(space: string) {
    this._space$.next(space);
  }

  constructor(
    private secretApi: CredentialApiService,
    private regionService: RegionService,
    private namespaceService: NamespaceService,
    private appService: AppService,
    private projectService: ProjectService,
    private pipelineService: PipelineApiService,
  ) {}

  getControlTypes(): ControlMapper {
    return {
      'alauda.io/jenkinscredentials': {
        optionsResolver: () => {
          let currentProject, spaceName;
          this._project$.pipe(take(1)).subscribe(v => {
            currentProject = v;
          });
          this._space$.pipe(take(1)).subscribe(v => {
            spaceName = v;
          });
          return this.secretApi
            .find(
              {
                ...getQuery(
                  filterBy('secretType', `${CredentialType.DockerConfig}`),
                ),
                space_name: spaceName,
              },
              currentProject,
            )
            .pipe(
              map((res: { items: Credential[]; length: number }) => {
                return (res.items || []).map((item: Credential) => {
                  return {
                    ...item,
                    opt_key: item.name,
                    opt_value: `${item.namespace ? item.namespace + '-' : ''}${
                      item.name
                    }`,
                  };
                });
              }),
            );
        },
      },
      'alauda.io/clustername': {
        optionsResolver: () => {
          let currentProject;
          this._project$.pipe(take(1)).subscribe(v => {
            currentProject = v;
          });

          return from(this.projectService.getProject(currentProject)).pipe(
            map(
              (project: Project) =>
                project &&
                project.clusters.map((cluster: ClusterInProject) => ({
                  opt_key: cluster.name,
                  opt_value: cluster.name,
                })),
            ),
          );
        },
      },
      'alauda.io/k8snamespace': {
        optionsResolver: (clustername: string) => {
          if (!clustername) {
            return of([]);
          }
          let currentProject: string;
          this._project$.pipe(take(1)).subscribe(v => {
            currentProject = v;
          });
          return from(this.regionService.getCluster(clustername)).pipe(
            switchMap((region: Cluster) =>
              this.namespaceService.getNamespaces(region.id, {
                project_name: currentProject,
              }),
            ),
            map((namespaces: Namespace[]) => {
              return namespaces.map((namespace: Namespace) => ({
                opt_key: get(namespace, 'kubernetes.metadata.name', ''),
                opt_value: get(namespace, 'kubernetes.metadata.name', ''),
              }));
            }),
          );
        },
      },
      'alauda.io/applicationName': {
        optionsResolver: (namespace: string, state: FormState, args?: any) => {
          const clusterNameKey = get(args, 'clusterName', 'clusterName');
          const clusterName = get(
            state,
            `controls.${clusterNameKey}.value`,
            '',
          );
          if (!namespace || !clusterName) {
            return of([]);
          }
          return from(
            this.appService.getAppListCRD({
              cluster_name: clusterName,
              namespace: namespace,
              params: {},
            }),
          ).pipe(
            map(({ results }) => {
              return results.map((itemList: K8sResourceWithActions[]) => {
                const componentsList: any[] = [];
                itemList.forEach((item: K8sResourceWithActions) => {
                  if (PodControllerKinds.includes(item.kubernetes.kind)) {
                    componentsList.push({
                      name: item.kubernetes.metadata.name,
                      ...item.kubernetes,
                    });
                  }
                });
                const app = find(itemList, {
                  kubernetes: { kind: 'Application' },
                });
                return {
                  opt_key: app.kubernetes.metadata.name,
                  opt_value: app.kubernetes.metadata.name,
                  componentsList: componentsList,
                };
              });
            }),
          );
        },
      },
      'alauda.io/componentName': {
        optionsResolver: (appName: string, state: FormState, args?: any) => {
          if (!appName) {
            return of([]);
          }
          const applicationNameKey = get(
            args,
            'applicationName',
            'applicationName',
          );
          const appComponentMap: any[] = get(
            find(get(state, `controls.${applicationNameKey}.options`), {
              opt_key: appName,
            }),
            'componentsList',
          );
          if (!appName || !appComponentMap) {
            return of([]);
          }
          return of(
            appComponentMap.map(component => ({
              opt_key: component.name,
              opt_value: component.name,
              ...component,
            })),
          );
        },
      },
      'alauda.io/componentClass': {
        optionsResolver: (
          componentName: string,
          state: FormState,
          args?: any,
        ) => {
          if (!componentName) {
            return of([]);
          }
          const componentNameKey = get(args, 'componentName', 'componentName');
          const component: any = find(
            get(state, `controls.${componentNameKey}.options`),
            {
              opt_key: componentName,
            },
          );
          if (!component || !componentName) {
            return of([]);
          }
          return of([{ opt_key: component.kind, opt_value: component.kind }]);
        },
      },
      'alauda.io/containerName': {
        optionsResolver: (
          componentName: string,
          state: FormState,
          args?: any,
        ) => {
          if (!componentName) {
            return of([]);
          }
          const componentNameKey = get(args, 'componentName', 'componentName');
          const component: any = find(
            get(state, `controls.${componentNameKey}.options`),
            {
              opt_key: componentName,
            },
          );
          if (!component || !componentName) {
            return of([]);
          }
          return of(
            get(component, 'spec.template.spec.containers', []).map(
              (container: any) => ({
                opt_key: container.name,
                opt_value: container.name,
              }),
            ),
          );
        },
      },
      'alauda.io/toolbinding': {
        optionsResolver: (_, __, args) => {
          if (!args) {
            return from([]);
          }
          let currentProject, spaceName, bindingType;
          this._project$.pipe(take(1)).subscribe(v => {
            currentProject = v;
          });
          this._space$.pipe(take(1)).subscribe(v => {
            spaceName = v;
          });
          if (args.bindingKind === 'codequalitytool') {
            bindingType = 'codequalitybindings';
          }
          return this.pipelineService
            .getBindingsByType(currentProject, spaceName, bindingType)
            .pipe(
              map(tools =>
                tools.map((tool: any) => ({
                  opt_key: tool.name,
                  opt_value: { name: tool.name, namespace: tool.namespace },
                })),
              ),
            );
        },
      },
      'alauda.io/codebranch': {
        optionsResolver: (coderepo: string, state: FormState) => {
          const repository = get(
            state,
            'controls.PlatformCodeRepository.value.bindingRepository',
            '',
          );
          if (!coderepo) {
            return of([]);
          }
          let currentProject, spaceName;
          this._project$.pipe(take(1)).subscribe(v => {
            currentProject = v;
          });
          this._space$.pipe(take(1)).subscribe(v => {
            spaceName = v;
          });
          return this.pipelineService
            .getPipeineCodeRepositoryBranchs(
              repository,
              currentProject,
              spaceName,
            )
            .pipe(
              map(res => {
                const branches = get(res, 'branches', []);
                return branches.map(
                  (branch: { commit?: string; name: string }) => ({
                    opt_key: branch.name,
                    opt_value: branch.name,
                  }),
                );
              }),
            );
        },
      },
    };
  }
}
