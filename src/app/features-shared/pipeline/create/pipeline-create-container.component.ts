import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChange,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { GroupDefine } from 'alauda-ui-dynamic-forms';
import { get, isObject } from 'lodash-es';

import {
  PipelineApiService,
  PipelineConfigModel,
  PipelineTemplate,
  PipelineTemplateResource,
} from 'app/services/api/pipeline';
import { Space } from 'app/services/api/space.service';
import { TranslateService } from 'app/translate/translate.service';
import { cronRuleValidator, MODEL } from '../constant';
import { PipelineDynamicParameterFormComponent } from '../forms/parameters/parameters.component';
import { PreviewJenkinsfileComponent } from '../preview-jenkinsfile/preview-jenkinsfile.component';
interface MODEL {
  templateStep: FormGroup;
  basicStep: FormGroup;
  repoStep: FormGroup;
  scriptStep: FormGroup;
  triggerStep: FormGroup;
}

export enum STEPS {
  template_select,
  basic,
  jenkinsfile,
  trigger,
}

type STEP_NAME_ =
  | 'templateSelect'
  | 'basic'
  | 'repository'
  | 'script'
  | 'dynamicParameters'
  | 'trigger'
  | 'end';

const STEP_NAME = {
  TEMPLATESELECT: 'templateSelect',
  BASIC: 'basic',
  REPOSITORY: 'repository',
  SCRIPT: 'script',
  DYNAMICPARAMETERS: 'dynamicParameters',
  TRIGGER: 'trigger',
  END: 'end',
};
@Component({
  selector: 'rc-pipeline-create-container',
  templateUrl: './pipeline-create-container.component.html',
  styleUrls: ['./pipeline-create-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineCreateContainerComponent implements OnInit, OnChanges {
  editorOptions = { language: 'jenkinsfile', readOnly: true };
  model: MODEL;
  stepIndex: STEPS = STEPS.basic;
  stepConfigs = ['basic_info', 'jenkinsfile', 'trigger'];
  jenkinsInstances: any[];
  jenkinsfilePreview: string;
  template: PipelineTemplate;

  triggerStep: FormGroup;

  forms: any[] = [];
  parameterFormValid = false;

  submitting = false;

  @Input()
  project: string;
  @Input()
  space: Space;
  @Input()
  method: 'template' | 'jenkinsfile';
  @Input()
  templateID = '';
  @Input()
  namespace: string;
  @Input()
  type: 'copy' | 'create';
  @Input()
  name: string;
  @ViewChild('preview')
  previewTemplate: TemplateRef<any>;
  @ViewChild(PipelineDynamicParameterFormComponent)
  parameterForm: PipelineDynamicParameterFormComponent;

  constructor(
    private api: PipelineApiService,
    private router: Router,
    private message: MessageService,
    private notification: NotificationService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private fb: FormBuilder,
    private dialog: DialogService,
  ) {
    this.model = this.formBuilder();
  }

  ngOnInit() {
    if (this.method === 'template') {
      this.stepConfigs = [
        'template_select',
        'basic_info',
        'template_parameter',
        'trigger',
      ];
      this.stepIndex = STEPS.template_select;
    }
  }

  ngOnChanges({
    type,
    name,
    space,
  }: {
    type: SimpleChange;
    name: SimpleChange;
    space: SimpleChange;
  }) {
    if (type && type.currentValue && name && name.currentValue) {
      if (type.currentValue !== 'copy') {
        return;
      } else if (space.currentValue) {
        this.getPipelineConfigModelData(
          space.currentValue.name || '',
          name.currentValue,
        );
      }
    }
  }

  private getPipelineConfigModelData(spaceName: string, name: string) {
    this.api.getPipelineConfigToModel(spaceName, name).subscribe(
      (pipelineConfig: PipelineConfigModel) => {
        pipelineConfig.basic.name = pipelineConfig.basic.name + '-copy';
        this.model.basicStep.patchValue({
          basic: pipelineConfig.basic,
        });
        this.model.triggerStep.patchValue({
          triggers: pipelineConfig.triggers,
        });
        if (this.method === 'jenkinsfile') {
          this.model.repoStep.patchValue({
            jenkinsfile: pipelineConfig.jenkinsfile,
          });
          this.model.scriptStep.patchValue({
            editor_script: pipelineConfig.editor_script,
          });
        } else if (this.method === 'template') {
          this.model.templateStep.patchValue({
            template: pipelineConfig.template,
          });
        }
      },
      () => {},
    );
  }

  step(operation: 'next' | 'prev', form?: NgForm) {
    if (operation === 'prev') {
      this.stepIndex--;
    } else {
      if (this.isStepOn('dynamicParameters') && this.method === 'template') {
        this.parameterForm.onSubmit();
        if (this.parameterForm.valid) {
          this.stepIndex++;
        }
      } else if (form && !form.onSubmit(null) && form.valid) {
        this.stepIndex++;
      }
    }
  }

  isStepOn(name: STEP_NAME_) {
    if (name === STEP_NAME.TEMPLATESELECT) {
      return this.stepIndex === 0;
    }
    if (name === STEP_NAME.BASIC) {
      return this.stepIndex === 1;
    }
    if (name === STEP_NAME.REPOSITORY) {
      return (
        this.stepIndex === 2 &&
        get(this.model, 'basicStep.value.basic.source', '') === 'repo'
      );
    }
    if (name === STEP_NAME.DYNAMICPARAMETERS) {
      return this.stepIndex === 2;
    }
    if (name === STEP_NAME.SCRIPT) {
      return (
        this.stepIndex === 2 &&
        get(this.model, 'basicStep.value.basic.source', '') === 'script'
      );
    }
    if (name === STEP_NAME.TRIGGER || name === STEP_NAME.END) {
      return this.stepIndex === 3;
    }
  }

  isSupport(name: STEP_NAME_) {
    if (
      name === STEP_NAME.TEMPLATESELECT ||
      name === STEP_NAME.DYNAMICPARAMETERS
    ) {
      return this.method === 'template';
    }
    if (name === STEP_NAME.REPOSITORY || name === STEP_NAME.SCRIPT) {
      return this.method === 'jenkinsfile';
    }
  }

  submit(triggerForm: NgForm) {
    triggerForm.onSubmit(null);
    if (triggerForm.valid) {
      const jenkinsfile = {
        ...get(this.model, 'repoStep.value.jenkinsfile.repo', ''),
        branch: get(this.model, 'repoStep.value.jenkinsfile.branch', ''),
        path: get(this.model, 'repoStep.value.jenkinsfile.path', ''),
      };
      let data = {
        ...this.model.basicStep.value,
        ...this.model.scriptStep.value,
        ...this.model.triggerStep.value,
        jenkinsfile: jenkinsfile,
      };
      if (this.method === 'template') {
        const template = this.getMergedTemplate();
        data = { ...data, template: template };
      }
      this.submitting = true;
      this.api.createPipelineConfig(this.space.name, data).subscribe(
        () => {
          this.submitting = false;
          this.message.success(this.translate.get('pipeline_create_success'));
          this.router.navigate(
            ['../', `${get(this.model.basicStep.value, 'basic.name')}`],
            {
              relativeTo: this.route,
            },
          );
        },
        error => {
          this.submitting = false;
          this.errorMessage(error, 'pipeline_create_fail');
        },
      );
    }
  }

  async cancel() {
    if (this.model.templateStep.value.template || this.model.basicStep.dirty) {
      try {
        await this.dialog.confirm({
          title: this.translate.get('pipeline_create_cancel_confirm'),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
        });
        this.router.navigate(['../'], {
          relativeTo: this.route,
        });
      } catch (rejection) {}
    } else {
      this.router.navigate(['../'], {
        relativeTo: this.route,
      });
    }
  }

  jenkinsChanged(values: any[]) {
    this.jenkinsInstances = values;
  }

  previewJenkinsfile() {
    this.dialog.open(PreviewJenkinsfileComponent, {
      size: DialogSize.Large,
      data: {
        template: get(this.model, 'templateStep.controls.template.value'),
        postData: this.parameterForm.getValues(),
        namespace: this.namespace,
        spaceName: this.space.name || '',
      },
    });
  }

  getCodeTriggerControls() {
    return get(this.model, 'triggerStep.controls.triggers.controls[0]');
  }

  getCronTriggerControls() {
    return get(this.model, 'triggerStep.controls.triggers.controls[1]');
  }

  getJenkinsBinding() {
    return get(
      this.model,
      'basicStep.controls.basic.controls.jenkins_instance.value',
      '',
    );
  }

  private formBuilder() {
    return {
      templateStep: this.buildGroup(MODEL.templateModel),
      basicStep: this.fb.group({ basic: this.buildGroup(MODEL.basicModel) }),
      repoStep: this.fb.group({
        jenkinsfile: this.buildGroup(MODEL.jenkinsfileModel),
      }),
      scriptStep: this.fb.group({
        editor_script: this.buildGroup(MODEL.scriptModel),
      }),
      triggerStep: this.fb.group({
        triggers: this.fb.array([
          this.buildGroup(MODEL.codeTriggerModel),
          this.fb.group({
            enabled: false,
            cron_string: '',
            cron_object: [
              {
                days: {
                  mon: true,
                  tue: true,
                  wed: true,
                  thu: true,
                  fri: true,
                  sat: true,
                  sun: true,
                },
                times: ['00:00'],
              },
              [cronRuleValidator],
            ],
            sourceType: 'select',
          }),
        ]),
      }),
    };
  }

  private buildGroup(Model: any, v?: any) {
    return this.fb.group(Model, v);
  }

  private getMergedTemplate(): PipelineTemplateResource {
    const parameters = this.parameterForm.getValues();
    const template = get(
      this.model,
      'templateStep.controls.template.value.__original',
    );
    const dyArguments: GroupDefine[] = get(template, 'spec.arguments');
    dyArguments.forEach((argument: GroupDefine) => {
      argument.items.forEach((item: any) => {
        // TODO: remove after api refactor
        if (item.display.type === 'boolean') {
          item.value = JSON.stringify(!!parameters[item.name]);
          if (typeof item.default === 'boolean') {
            item.default = JSON.stringify(item.default);
          }
        } else if (this.unstring(parameters[item.name])) {
          item.value = JSON.stringify(parameters[item.name]);
        } else {
          item.value = parameters[item.name] || '';
        }
      });
    });
    return template;
  }

  private errorMessage(error: any, translateKey: string) {
    this.notification.error({
      title: this.translate.get(translateKey),
      content: get(error, 'errors.[0].message', ''),
    });
  }

  private unstring(value: any) {
    return isObject(value) || typeof value === 'number';
  }
}
