import { NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChange,
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { get } from 'lodash-es';
import { of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import { PipelineApiService } from 'app/services/api/pipeline';

@Component({
  selector: 'rc-pipeline-basic-form',
  templateUrl: './basic-form.component.html',
  styleUrls: ['./basic-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineBasicFormComponent implements OnChanges {
  jenkinsfielSourceType = [
    { value: 'repo', name: 'pipeline_from_repo' },
    { value: 'script', name: 'pipeline_from_script' },
  ];

  jenkinsRunPolicy = [
    { value: 'Serial', name: 'pipeline_serial' },
    { value: 'Parallel', name: 'pipeline_parallel' },
  ];

  apps: any[];
  jenkins: any[];

  @Input()
  spaceName: string;
  @Input()
  form: FormGroup;
  @Input()
  namespace: string;
  @Input()
  type: 'create' | 'update' = 'create';
  @Input()
  method: string;
  @Output()
  jenkinsChanged = new EventEmitter<any>();
  get values() {
    return this.form.value;
  }

  constructor(
    private pipelineApi: PipelineApiService,
    private notification: NotificationService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnChanges({ spaceName }: { spaceName: SimpleChange }) {
    if (
      spaceName &&
      spaceName.currentValue &&
      spaceName.previousValue !== spaceName.currentValue
    ) {
      this.fetchData();
    }
  }

  fetchData() {
    this.getJenkins()
      .pipe(
        tap(([instanceResult]: any[]) => {
          if (instanceResult && instanceResult.error) {
            this.errorHandler(instanceResult.content);
          }
        }),
      )
      .subscribe();
  }

  getJenkins() {
    return this.pipelineApi.jenkinsBindings(this.spaceName).pipe(
      tap(result => {
        this.jenkins = result;
        if (this.jenkins && !this.jenkins.length) {
          this.form.controls.jenkins_instance.disable();
        }
        this.jenkinsChanged.emit(this.jenkins);
        this.cdr.markForCheck();
      }),
      catchError(error => {
        this.jenkins = [];
        return of({ content: error, error: true });
      }),
    );
  }

  private errorHandler(error: any) {
    this.notification.error({
      content: get(error, 'errors.[0].message', ''),
    });
  }
}
