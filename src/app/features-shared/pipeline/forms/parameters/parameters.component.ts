import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChange,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

// import { CodeRepository } from '@app/api';
import {
  ControlMapper,
  FieldDefine,
  GroupDefine,
} from 'alauda-ui-dynamic-forms';
import { find, get, partition } from 'lodash-es';
import { map, publishReplay, refCount } from 'rxjs/operators';

import {
  DROPDOWN_TYPES,
  INPUT_DROPDOWN_TYPES,
} from 'app/features-shared/pipeline/constant';
import { PipelineControlTypesService } from 'app/features-shared/pipeline/control-types.service';
import { CodeRepository } from 'app/services/api/pipeline';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  selector: 'rc-pipeline-dynamic-parameters-form',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [PipelineControlTypesService],
})
export class PipelineDynamicParameterFormComponent
  implements OnInit, OnChanges {
  model: any = {};
  controlTypes: ControlMapper;
  dropdown_types = DROPDOWN_TYPES;
  secretCreatePermission: boolean;
  input_dropdown_types = INPUT_DROPDOWN_TYPES;

  translateKey$ = this.translate.currentLang$.pipe(
    map((lang: string) => (lang === 'en' ? 'en' : 'zh-CN')),
    publishReplay(1),
    refCount(),
  );
  @ViewChild('ngForm')
  ngForm: NgForm;
  @Input()
  namespace: string;
  @Input()
  templateName: string;
  @Input()
  spaceName: string;
  @Input()
  templateFields: GroupDefine[];
  @Input()
  project: string;

  constructor(
    private controlTypeService: PipelineControlTypesService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
  ) {}

  get valid() {
    return this.ngForm.valid;
  }

  async ngOnInit() {
    this.controlTypes = this.controlTypeService.getControlTypes();
    [
      this.secretCreatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_secret',
      { space_name: this.spaceName },
      ['create'],
    );
  }

  ngOnChanges({
    templateFields,
    project,
    spaceName,
  }: {
    templateFields: SimpleChange;
    project: SimpleChange;
    spaceName: SimpleChange;
  }) {
    if (project && project.currentValue) {
      this.controlTypeService.project = project.currentValue;
    }
    if (spaceName && spaceName.currentValue) {
      this.controlTypeService.space = spaceName.currentValue;
    }
    if (templateFields && templateFields.currentValue) {
      templateFields.currentValue.forEach((g: any) => {
        g.items.forEach((item: any) => {
          if (
            item.display.type !== 'string' &&
            item.value &&
            !this.model[item.name]
          ) {
            try {
              this.model[item.name] = JSON.parse(item.value || item.default);
            } catch (e) {
              this.model[item.name] = item.value || item.default;
            }
          } else {
            this.model[item.name] = item.value || item.default;
          }
        });
      });

      templateFields.currentValue.forEach(
        (
          group: GroupDefine & { partition: boolean; partitionItems: any[] },
        ) => {
          if (group.partition) {
            return;
          }
          group.partitionItems = partition(
            group.items,
            (o: FieldDefine & { display: { advanced: boolean } }) =>
              o.display.advanced,
          );
          group.partition = true;
        },
      );
    }
  }

  groupTracker(_: number, group: GroupDefine) {
    return group.displayName.en;
  }

  fieldTracker(_: number, field: FieldDefine) {
    return field.name;
  }

  getValues() {
    return this.ngForm.value;
  }

  onSubmit() {
    this.ngForm.onSubmit(null);
  }

  trackFn(val: any) {
    return (val && val.key) || (val && val.name) || val;
  }

  valueConverter(
    data: {
      repo: string;
      bindingRepository: string;
      secret: { name: string; namespace: string };
      sourceType: string;
    },
    cds: CodeRepository[],
  ) {
    const code = find(cds, { name: data.bindingRepository });
    const sec: { name: string; namespace?: string } = data.repo
      ? data.secret
      : code && code.secret;
    return {
      ...data,
      url: data.repo || get(code, 'httpURL', ''),
      credentialId: sec
        ? `${sec.namespace ? sec.namespace + '-' : ''}${sec.name}`
        : '',
      kind: data.repo ? 'input' : 'select',
      sourceType: data.sourceType,
    };
  }
}
