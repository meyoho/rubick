import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { FormGroup, NgForm } from '@angular/forms';

import { get } from 'lodash-es';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { PipelineApiService } from 'app/services/api/pipeline';

@Component({
  selector: 'rc-pipeline-repository-form',
  templateUrl: './repository-form.component.html',
  styleUrls: ['./repository-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineRepositoryFormComponent implements OnInit, OnDestroy {
  @Input()
  form: FormGroup;
  @Input()
  type: 'create' | 'update' = 'create';
  @Input()
  namespace: string;
  @Input()
  project: string;
  @Input()
  spaceName: string;

  @ViewChild('repoForm')
  formControl: NgForm;

  branches: { name: string; commit?: string }[] = [];

  private unsubscribe$ = new Subject<void>();

  get values() {
    return this.form.value;
  }

  constructor(private pipelineService: PipelineApiService) {}

  ngOnInit() {
    const bindingRepo = this.form.controls['repo'].value.bindingRepository;
    if (this.project && this.spaceName && bindingRepo) {
      this.getBranches(bindingRepo);
    }

    this.form.controls['repo'].valueChanges
      .pipe(takeUntil(this.unsubscribe$))
      .subscribe(repo => {
        this.branches = [];
        if (repo && repo.bindingRepository) {
          this.form.controls['branch'].setValue('');
          this.getBranches(repo.bindingRepository);
        } else {
          this.form.controls['branch'].setValue('master');
        }
      });
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  getBranches(repository: string) {
    this.pipelineService
      .getPipeineCodeRepositoryBranchs(repository, this.project, this.spaceName)
      .toPromise()
      .then(res => {
        const branches = get(res, 'branches', []);
        this.branches = branches.map(
          (branch: { commit?: string; name: string }) => ({
            opt_key: branch.name,
            opt_value: branch.name,
          }),
        );
      });
  }
}
