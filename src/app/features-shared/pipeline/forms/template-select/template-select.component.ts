import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChange,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import { forkJoin, NEVER, ReplaySubject, Subject } from 'rxjs';
import {
  catchError,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import {
  ListResult,
  PipelineApiService,
  PipelineTemplate,
  TemplateCategory,
} from 'app/services/api/pipeline';
import { TranslateService } from 'app/translate/translate.service';
import { filterBy, getQuery } from 'app/utils/query-builder';

@Component({
  selector: 'rc-pipeline-template-select-form',
  templateUrl: './template-select.component.html',
  styleUrls: ['./template-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateSelectFormComponent
  implements OnInit, OnChanges, AfterViewInit {
  categories: (TemplateCategory & { selected: boolean })[] = [];
  fetchData$ = new Subject<any>();
  searchKey: string;
  selectedTemplateName: string;
  loading = true;
  error$ = new ReplaySubject<void>(1);
  navCategory = this.route.snapshot.paramMap.get('category');

  @Input()
  spaceName: string;
  @Input()
  form: FormGroup;
  @Input()
  namespace: string;

  templates$ = this.fetchData$.pipe(
    switchMap((query: any) =>
      forkJoin(
        this.pipelineApi.clusterTemplateList(query),
        this.pipelineApi.templateList({
          ...query,
          space_name: this.spaceName || '',
        }),
      ).pipe(
        map(([c_templates, templates]) => {
          return c_templates.items.concat(templates.items);
        }),
        tap(() => {
          this.loading = false;
        }),
      ),
    ),
    publishReplay(1),
    refCount(),
    catchError((err: any) => {
      this.loading = false;
      this.error$.next(err);
      return NEVER;
    }),
  );

  constructor(
    private pipelineApi: PipelineApiService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private route: ActivatedRoute,
  ) {}

  ngOnChanges({ spaceName }: { spaceName: SimpleChange }) {
    if (
      spaceName.currentValue &&
      spaceName.previousValue !== spaceName.currentValue
    ) {
      this.search();
    }
  }

  ngOnInit() {
    if (this.navCategory === 'all') {
      this.pipelineApi
        .categories()
        .subscribe((categories: ListResult<TemplateCategory>) => {
          this.categories = categories.items.map(
            (category: TemplateCategory & { selected: boolean }) => {
              category.selected = false;
              return category;
            },
          );
          this.cdr.detectChanges();
        });
    }
  }

  ngAfterViewInit() {
    this.search('');
  }

  search(event?: string) {
    this.loading = true;
    this.searchKey = event;
    let filterCategories;
    if (this.navCategory !== 'all') {
      filterCategories = [`category:${this.navCategory}`];
    } else {
      filterCategories = this.categories
        .filter((item: { name: string; selected: boolean }) => {
          return item.selected;
        })
        .map(item => `category:${item.name}`);
    }
    this.fetchData$.next(
      getQuery(
        filterBy(
          this.translate.currentLang === 'en'
            ? 'displayEnName'
            : 'displayZhName',
          this.searchKey || '',
        ),
        filterBy('label', filterCategories.join('$.$')),
      ),
    );
  }

  selectTemplate(template: PipelineTemplate) {
    this.form.patchValue({ template: template });
  }

  fetchDataWithCategory(categories: { name: string; selected: boolean }[]) {
    let filterCategories;
    if (this.navCategory !== 'all') {
      filterCategories = [this.navCategory];
    } else {
      filterCategories = categories
        .filter((item: { name: string; selected: boolean }) => {
          return item.selected;
        })
        .map(item => `category:${item.name}`);
    }
    const query = getQuery(
      filterBy(
        `${
          this.translate.currentLang === 'en'
            ? 'displayEnName'
            : 'displayZhName'
        }`,
        this.searchKey,
      ),
      filterBy('label', filterCategories.join('$.$')),
    );
    this.fetchData$.next(query);
  }
}
