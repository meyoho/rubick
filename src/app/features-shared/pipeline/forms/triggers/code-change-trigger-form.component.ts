import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';
import { FormGroup } from '@angular/forms';

@Component({
  selector: 'rc-pipeline-code-change-trigger-form',
  templateUrl: './code-change-trigger-form.component.html',
  styleUrls: ['./code-change-trigger-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineCodeChangeTriggerFormComponent {
  codeCheckOptions = [
    { name: 'pipeline_trigger_every_2_minute', value: 'H/2 * * * *' },
    { name: 'pipeline_trigger_every_5_minute', value: 'H/5 * * * *' },
    { name: 'pipeline_trigger_every_30_minute', value: 'H/30 * * * *' },
    { name: 'pipeline_trigger_every_hour', value: 'H * * * *' },
    { name: 'pipeline_trigger_every_day', value: 'H H * * *' },
  ];

  @Input()
  form: FormGroup;

  constructor(private cdr: ChangeDetectorRef) {}

  formValue(name: string) {
    return !!this.form.get(name).value;
  }

  triggerEnabledChanged() {
    this.cdr.markForCheck();
  }
}
