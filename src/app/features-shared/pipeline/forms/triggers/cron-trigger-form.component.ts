import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { FormGroup, Validators } from '@angular/forms';

import { debounceTime } from 'rxjs/operators';

import { cronRuleValidator } from 'app/features-shared/pipeline/constant';
import { PipelineApiService } from 'app/services/api/pipeline';

@Component({
  selector: 'rc-pipeline-cron-trigger-form',
  templateUrl: './cron-trigger-form.component.html',
  styleUrls: ['./cron-trigger-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineCronTriggerFormComponent implements OnInit {
  nextTime: number | string;
  loading = false;
  @Input()
  form: FormGroup;
  @Input()
  namespace: string;
  @Input()
  jenkinsBinding: string;
  @Input()
  spaceName: string;

  constructor(
    private api: PipelineApiService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.form.controls.cron_string.valueChanges
      .pipe(debounceTime(1000))
      .subscribe((value: string) => {
        if (value) {
          this.loading = true;
          this.nextTime = 0;
          this.cdr.detectChanges();
          this.api
            .cronCheck(this.spaceName, this.jenkinsBinding, { cron: value })
            .subscribe(
              ({ data }: { data: { next: string; previous: string } }) => {
                this.nextTime = parseInt(data.next, 10) || 'invalid';
                this.loading = false;
                this.cdr.detectChanges();
              },
              () => {
                this.loading = false;
                this.cdr.detectChanges();
              },
            );
        } else {
          this.nextTime = 0;
        }
      });
  }

  inputTypeChange() {
    let type = this.form.controls.sourceType.value;
    if (type === 'select') {
      type = 'input';
      if (this.formValue('enabled')) {
        this.form.controls.cron_string.setValidators([Validators.required]);
      }
      this.form.controls.cron_object.clearValidators();
    } else {
      type = 'select';
      if (this.formValue('enabled')) {
        this.form.controls.cron_object.setValidators([
          Validators.required,
          cronRuleValidator,
        ]);
      }
      this.form.controls.cron_string.clearValidators();
    }
    this.form.controls.cron_string.updateValueAndValidity();
    this.form.controls.cron_object.updateValueAndValidity();
    this.form.controls.sourceType.patchValue(type);
  }

  triggerEnabledChanged() {
    this.cdr.markForCheck();
  }

  formValue(name: string) {
    return !!this.form.get(name).value;
  }
}
