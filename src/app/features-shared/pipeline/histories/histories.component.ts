import { NotificationService } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  Input,
} from '@angular/core';

import { get, isEmpty } from 'lodash-es';
import { of, Subject } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

import { PipelineApiService, PipelineHistory } from 'app/services/api/pipeline';
import { Space } from 'app/services/api/space.service';
import { TranslateService } from 'app/translate/translate.service';
import { filterBy, getQuery, pageBy, sortBy } from 'app/utils/query-builder';

@Component({
  selector: 'rc-pipeline-histories',
  templateUrl: './histories.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoriesComponent implements AfterViewInit {
  historySearchKey = '';
  historyPageSize = 20;
  historyPageIndex = 0;
  historySortBy = 'creationTimestamp';
  historyDesc = true;
  pullInTime = 5000;
  histories: PipelineHistory[];
  historyParams$: Subject<{
    filterBy?: string;
    sortBy?: string;
    page?: string;
    itemsPerPage?: string;
  }> = new Subject();

  @Input()
  permissions: {
    canAbort: boolean;
    canDelete: boolean;
  };
  @Input()
  space: Space;
  @Input()
  namespace: string;
  @Input()
  name: string;
  constructor(
    private pipelineApi: PipelineApiService,
    private notification: NotificationService,
    private translate: TranslateService,
  ) {}

  fetchData = (params: any) => {
    if (!params || !this.space || !this.space.name) {
      return of({ total: 0, histories: [] });
    }
    params['space_name'] = get(this.space, 'name', '');
    return this.pipelineApi.getPipelineHistories(params).pipe(
      map(historiesList => {
        historiesList.histories = historiesList.histories.map(
          this.mapPipelineHistories,
        );
        return historiesList;
      }),
      catchError(err => {
        this.pullInTime = 0;
        if (err.status === 403) {
          this.notification.error({
            title: this.translate.get('errorType'),
            content: get(err, 'errors.[0].message', ''),
          });
        }
        throw err;
      }),
    );
  };

  historyParamsChanged(params: {
    pageIndex?: number;
    pageSize?: number;
    searchKey?: string;
    active?: string;
    desc?: boolean;
  }) {
    if (isEmpty(params)) {
      this.historyRefresh();
    } else {
      const { pageIndex, pageSize, searchKey, active, desc } = params;
      this.historyPageIndex =
        pageIndex === undefined ? this.historyPageIndex : pageIndex;
      this.historyPageSize = pageSize || this.historyPageSize;
      this.historySearchKey =
        searchKey === undefined ? this.historySearchKey : searchKey;
      this.historySortBy = active === undefined ? this.historySortBy : active;
      this.historyDesc = desc === undefined ? this.historyDesc : desc;
      this.historyRefresh();
    }
  }

  historyRefresh(reset?: boolean) {
    if (reset) {
      this.historySearchKey = '';
      this.historyPageIndex = 0;
      this.historyPageSize = 10;
      this.historyDesc = true;
      this.historySortBy = 'creationTimestamp';
    }
    this.historyParams$.next(this.queryBuilder());
  }

  private queryBuilder(): any {
    return getQuery(
      filterBy('label', `pipelineConfig:${this.name}`),
      filterBy('name', this.historySearchKey),
      sortBy(this.historySortBy, this.historyDesc),
      pageBy(this.historyPageIndex, this.historyPageSize),
    );
  }

  private mapPipelineHistories(history: PipelineHistory) {
    return {
      ...history.status,
      ...history,
      duration:
        new Date(history.status.finishedAt).getTime() -
        new Date(history.status.startedAt).getTime(),
    };
  }

  ngAfterViewInit() {
    this.historyParams$.next(this.queryBuilder());
  }
}
