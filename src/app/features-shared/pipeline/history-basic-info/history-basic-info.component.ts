import {
  ConfirmType,
  DialogService,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { get } from 'lodash-es';

import {
  getHistoryStatus,
  mapTriggerIcon,
  mapTriggerTranslateKey,
} from 'app/features-shared/pipeline/utils.ts';
import { PipelineApiService, PipelineHistory } from 'app/services/api/pipeline';
import { Space } from 'app/services/api/space.service';
import { TranslateService } from 'app/translate/translate.service';
import { ANNOTATION_TOOL_ACCESS_URL } from 'app/utils/config';

@Component({
  selector: 'rc-pipeline-history-basic-info',
  templateUrl: './history-basic-info.component.html',
  styleUrls: [
    './history-basic-info.component.scss',
    '../history-table/history-table.component.scss',
    '../shared-style/fields.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryBasicInfoComponent implements OnChanges {
  @Input()
  history: PipelineHistory;
  @Input()
  namespace: string;
  @Output()
  statusChange = new EventEmitter<string>();
  @Input()
  space: Space;
  @Input()
  permissions: {
    canAbort: boolean;
    canDelete: boolean;
  };
  mapTriggerTranslateKey: Function = mapTriggerTranslateKey;
  mapTriggerIcon = mapTriggerIcon;
  toolAccessUrl = '';

  constructor(
    private auiDialog: DialogService,
    private api: PipelineApiService,
    private translate: TranslateService,
    private notification: NotificationService,
    private message: MessageService,
    private router: Router,
    private route: ActivatedRoute,
  ) {}

  ngOnChanges({ history }: SimpleChanges) {
    if (history && history.currentValue) {
      this.getJenkinsBinding();
    }
  }

  getHistoryStatusIcon(phase: string, type: 'detail' | 'preview') {
    return getHistoryStatus(phase, type);
  }

  getDuration(status: any) {
    if (!status || !status.finishedAt) {
      return 0;
    }
    return (
      new Date(status.finishedAt).getTime() -
      new Date(status.startedAt).getTime()
    );
  }

  delete() {
    this.auiDialog
      .confirm({
        title: this.translate.get('pipeline_delete_history_confirm', {
          name: this.history.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('delete'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: (resolve, reject) => {
          this.api.deletePipeline(this.space.name, this.history.name).subscribe(
            () => {
              this.successNotification('history_delete_succ');
              resolve();
            },
            (err: any) => {
              this.errorMessage(err, 'history_delete_fail');
              reject();
            },
          );
        },
      })
      .then(() => {
        this.router.navigate(['../'], { relativeTo: this.route });
      })
      .catch(() => {
        this.auiDialog.closeAll();
      });
  }

  cancel() {
    this.auiDialog
      .confirm({
        title: this.translate.get('pipeline_cancel_history_confirm', {
          name: this.history.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('pipeline_cancel'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: (resolve, reject) => {
          this.api.abortPipeline(this.space.name, this.history.name).subscribe(
            () => {
              this.successNotification('history_cancel_succ');
              resolve();
            },
            (err: any) => {
              this.errorMessage(err, 'history_cancel_fail');
              reject();
            },
          );
        },
      })
      .then(() => {
        this.statusChange.emit();
      })
      .catch(() => {
        this.auiDialog.closeAll();
      });
  }

  getJenkinsBinding() {
    this.api
      .jenkinsBinding(this.history.jenkins.jenkinsInstanceName, this.space.name)
      .then(res => {
        this.toolAccessUrl = get(
          res,
          ['metadata', 'annotations', ANNOTATION_TOOL_ACCESS_URL],
          '',
        );
      })
      .catch(() => {});
  }
  goToJenkinsDetail() {
    const url = `${this.toolAccessUrl}${this.history.jenkins.jenkinsBuildUri}`;
    window.open(url, '_blank');
  }
  private errorMessage(err: any, translationKey: string) {
    this.notification.error({
      title: this.translate.get(`pipeline_${translationKey}`, {
        name: this.history.name,
      }),
      content: get(err, 'errors.[0].message', ''),
    });
  }

  private successNotification(translationKey: string) {
    this.message.success({
      content: this.translate.get(`pipeline_${translationKey}`, {
        name: this.history.name,
      }),
    });
  }
}
