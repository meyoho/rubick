import { DialogService, DialogSize } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { LogsComponent } from 'app/features-shared/pipeline/logs/logs.component';
import { getHistoryStatus } from 'app/features-shared/pipeline/utils.ts';
import { PipelineHistory } from 'app/services/api/pipeline';

@Component({
  selector: 'rc-history-preview',
  templateUrl: './history-preview.component.html',
  styleUrls: [
    './history-preview.component.scss',
    '../shared-style/fields.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class HistoryPreviewComponent {
  @Input()
  histories: PipelineHistory[];
  @Input()
  project: string;
  @Input()
  pipelineName: string;
  @Input()
  category: string;
  @Input()
  spaceName: string;

  get lastStatus() {
    return this.histories[0].status;
  }

  constructor(private dialog: DialogService) {}

  getHistoryStatusIcon(phase: string) {
    return getHistoryStatus(phase).icon;
  }

  openLogs(history: PipelineHistory) {
    this.dialog.open(LogsComponent, {
      size: DialogSize.Large,
      data: { history, spaceName: this.spaceName || '' },
    });
  }
}
