import { NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
} from '@angular/core';

import { get } from 'lodash-es';
import { of, Subject } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

import {
  PipelineApiService,
  PipelineHistoryLog,
  PipelineHistoryStep,
} from 'app/services/api/pipeline';
import { Space } from 'app/services/api/space.service';

@Component({
  selector: 'rc-pipeline-history-step',
  templateUrl: './history-step.component.html',
  styleUrls: ['./history-step.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryStepComponent {
  more = false;
  errorCount = 0;
  next = 0;
  active = false;
  text: string[] = [];
  @Input()
  step: PipelineHistoryStep;
  @Input()
  namespace: string;
  @Input()
  spaceName: string;
  @Input()
  historyName: string;
  @Input()
  stageId: string;

  params$ = new Subject<boolean>();
  space: Space;
  constructor(
    private api: PipelineApiService,
    private cdr: ChangeDetectorRef,
    private notification: NotificationService,
  ) {}

  stepLogs(step: PipelineHistoryStep) {
    this.params$.next(!this.active);
    if (this.canUnfoldLog(step)) {
      this.active = !this.active;
    }
  }

  canUnfoldLog(step: PipelineHistoryStep) {
    return step.actions && step.actions.length;
  }

  fetchData = (params: boolean) => {
    if (!params) {
      return of(null);
    }
    return this.api
      .getPipelineHistoryStepLog(this.spaceName, this.historyName, {
        start: this.next,
        stage: this.stageId,
        step: this.step.id,
      })
      .pipe(
        tap((res: PipelineHistoryLog) => {
          this.next = res.nextStart;
          this.more = res.more;
          if (res.text) {
            this.text = this.text.concat(res.text.split('\n'));
          }
          this.cdr.detectChanges();
        }),
        catchError(error => {
          if (this.errorCount > 5) {
            this.notification.error({
              content: get(error, 'errors.[0].message', ''),
            });
            this.more = false;
            this.cdr.detectChanges();
          }
          this.errorCount++;
          throw error;
        }),
      );
  };

  getStatusIcon() {
    switch (
      this.step.result !== 'UNKNOWN' ? this.step.result : this.step.state
    ) {
      case 'SUCCESS':
        return 'check_s';
      case 'RUNNING':
        return 'basic:sync_s';
      case 'FAILURE':
        return 'close_8';
      case 'ABORTED':
        return 'basic:minus';
      default:
        return 'check_s';
    }
  }

  trackLogs(index: number) {
    return index;
  }
}
