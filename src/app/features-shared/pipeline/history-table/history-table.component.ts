import {
  ConfirmType,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { get } from 'lodash-es';
import { interval } from 'rxjs';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { LogsComponent } from 'app/features-shared/pipeline/logs/logs.component';
import {
  getHistoryStatus,
  mapTriggerTranslateKey,
} from 'app/features-shared/pipeline/utils';
import { PipelineApiService, PipelineHistory } from 'app/services/api/pipeline';
import { TranslateService } from 'app/translate/translate.service';
@Component({
  selector: 'rc-pipeline-history-table',
  templateUrl: './history-table.component.html',
  styleUrls: ['./history-table.component.scss', '../shared-style/fields.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryTableComponent {
  columns = ['name', 'status', 'startedAt', 'time', 'cause', 'actions'];

  mapTriggerTranslateKey: (_: string) => {} = mapTriggerTranslateKey;

  @Input()
  historyList: { total: number; histories: PipelineHistory[] };
  @Input()
  namespace: string;
  @Input()
  loading: boolean;
  @Input()
  spaceName: string;
  @Input()
  permissions: {
    canAbort: boolean;
    canDelete: boolean;
  };
  @Output()
  paramsChanged: EventEmitter<{
    pageIndex?: number;
    pageSize?: number;
    searchKey?: string;
    active?: string;
    desc?: boolean;
  }> = new EventEmitter();

  pageIndex = 0;
  pageSize = 20;

  currentDate = interval(2000).pipe(
    map(() => new Date().getTime()),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private dialog: DialogService,
    private api: PipelineApiService,
    private auiDialog: DialogService,
    private translate: TranslateService,
    private notification: NotificationService,
    private message: MessageService,
  ) {}

  getHistoryStatusIcon(phase: string) {
    return getHistoryStatus(phase);
  }

  openLogs(history: PipelineHistory) {
    this.dialog.open(LogsComponent, {
      size: DialogSize.Large,
      data: { history, spaceName: this.spaceName },
    });
  }

  search(searchKey: string) {
    this.paramsChanged.emit({ searchKey: searchKey });
  }

  pageChange(event: { pageIndex: number; pageSize: number }) {
    this.pageIndex = event.pageIndex;
    this.pageSize = event.pageSize;
    this.paramsChanged.emit({ ...event });
  }

  getDateTimes(dateString: string) {
    return new Date(dateString).getTime();
  }

  sortChange(event: { active: string; direction: string }) {
    this.paramsChanged.emit({
      active: event.active,
      desc: event.direction === 'desc',
    });
  }

  delete(item: PipelineHistory) {
    this.auiDialog
      .confirm({
        title: this.translate.get('pipeline_delete_history_confirm', {
          name: item.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('delete'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: (resolve, reject) => {
          this.api.deletePipeline(this.spaceName || '', item.name).subscribe(
            () => {
              this.successNotification(item, 'history_delete_succ');
              resolve();
            },
            (err: any) => {
              this.errorMessage(err, item, 'history_delete_fail');
              reject();
            },
          );
        },
      })
      .then(() => {
        this.paramsChanged.emit({});
      })
      .catch(() => {
        this.auiDialog.closeAll();
      });
  }

  cancel(item: PipelineHistory) {
    this.auiDialog
      .confirm({
        title: this.translate.get('pipeline_cancel_history_confirm', {
          name: item.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('pipeline_cancel'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: (resolve, reject) => {
          this.api.abortPipeline(this.spaceName || '', item.name).subscribe(
            () => {
              this.successNotification(item, 'history_cancel_succ');
              resolve();
            },
            (err: any) => {
              this.errorMessage(err, item, 'history_cancel_fail');
              reject();
            },
          );
        },
      })
      .then(() => {
        this.paramsChanged.emit({});
      })
      .catch(() => {
        this.auiDialog.closeAll();
      });
  }

  trackFn(index: number) {
    return index;
  }

  private errorMessage(
    err: any,
    item: PipelineHistory,
    translationKey: string,
  ) {
    this.notification.error({
      title: this.translate.get(`pipeline_${translationKey}`, {
        name: item.name,
      }),
      content: get(err, 'errors.[0].message', ''),
    });
  }

  private successNotification(item: PipelineHistory, translationKey: string) {
    this.message.success({
      content: this.translate.get(`pipeline_${translationKey}`, {
        name: item.name,
      }),
    });
  }
}
