import { CodeEditorModule } from '@alauda/code-editor';
import { CheckboxModule, IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';

import { TranslateModule } from 'app/translate/translate.module';

import { LogViewComponent } from './log-view.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    CodeEditorModule,
    IconModule,
    CheckboxModule,
    TranslateModule,
  ],
  declarations: [LogViewComponent],
  exports: [LogViewComponent],
  entryComponents: [LogViewComponent],
})
export class LogViewModule {}
