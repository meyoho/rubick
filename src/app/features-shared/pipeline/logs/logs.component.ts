import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
} from '@angular/core';

import { get } from 'lodash-es';
import { catchError, tap } from 'rxjs/operators';

import {
  PipelineApiService,
  PipelineHistory,
  PipelineHistoryLog,
} from 'app/services/api/pipeline';

@Component({
  templateUrl: 'logs.component.html',
  styleUrls: ['logs.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LogsComponent {
  editorOptions = { language: 'Logs', readOnly: true };
  logs = '';
  next = 0;
  more = true;
  errorCount = 0;
  text = '';
  spaceName: string;
  history: PipelineHistory;

  constructor(
    private notifaction: NotificationService,
    private pipelineApi: PipelineApiService,
    private dialogRef: DialogRef<LogsComponent>,
    private cdr: ChangeDetectorRef,
    @Inject(DIALOG_DATA)
    public data: { history: PipelineHistory; spaceName: string },
  ) {
    this.history = data.history;
    this.spaceName = data.spaceName;
  }

  fetchLogs = () => {
    return this.pipelineApi
      .getPipelineHistoryLog(this.spaceName, this.history.name, {
        start: this.next,
      })
      .pipe(
        tap((res: PipelineHistoryLog) => {
          this.next = res.nextStart;
          this.more = res.more;
          this.text = this.text + res.text;
          this.cdr.detectChanges();
        }),
        catchError(error => {
          if (this.errorCount > 5) {
            this.notifaction.error({
              content: get(error, 'errors.[0].message', ''),
            });
            this.more = false;
            this.cdr.detectChanges();
          }
          this.errorCount++;
          throw error;
        }),
      );
  };

  cancel() {
    this.dialogRef.close();
  }
}
