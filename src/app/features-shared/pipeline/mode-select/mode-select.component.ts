import { DIALOG_DATA, DialogService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { Router } from '@angular/router';

import { get } from 'lodash-es';

@Component({
  templateUrl: './mode-select.component.html',
  styleUrls: ['./mode-select.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ModeSelectComponent {
  constructor(
    @Inject(DIALOG_DATA)
    private data: { namespace: string; category: string; url: string },
    private router: Router,
    private dialog: DialogService,
  ) {}
  navToCreate(method: string) {
    this.router.navigateByUrl(
      `${get(this.data.url.split('?'), '[0]', '')}/create?method=${method}`,
    );
    this.dialog.closeAll();
  }
}
