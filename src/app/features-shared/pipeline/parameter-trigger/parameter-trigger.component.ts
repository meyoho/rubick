import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { TriggerPipelineParameter } from 'app/services/api/pipeline';

@Component({
  templateUrl: './parameter-trigger.component.html',
  styleUrls: ['./parameter-trigger.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineParameterTriggerComponent {
  parameters: TriggerPipelineParameter[];
  model: any = {};
  constructor(
    private dialogRef: DialogRef<PipelineParameterTriggerComponent>,
    @Inject(DIALOG_DATA) private data: any,
  ) {
    this.parameters = this.data.parameters;
    this.parameters.forEach((p: TriggerPipelineParameter) => {
      this.model[p.name] = p.type === 'boolean' ? JSON.parse(p.value) : p.value;
    });
  }

  submit() {
    this.dialogRef.close({ params: this.mergedValue() });
  }

  cancel() {
    this.dialogRef.close(null);
  }

  private mergedValue() {
    return this.parameters.map((p: TriggerPipelineParameter) => {
      return { ...p, value: this.model[p.name].toString() };
    });
  }
}
