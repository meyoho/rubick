import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChange,
} from '@angular/core';

import { isObject, keys } from 'lodash-es';
import { map, publishReplay, refCount } from 'rxjs/operators';

import { TranslateService } from 'app/translate/translate.service';

@Component({
  selector: 'rc-pipeline-design-parameters',
  templateUrl: './parameters.component.html',
  styleUrls: ['./parameters.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineDesignParametersComponent implements OnChanges {
  translateKey$ = this.translate.currentLang$.pipe(
    map((lang: string) => {
      return lang === 'en' ? 'en' : 'zh-CN';
    }),
    publishReplay(1),
    refCount(),
  );
  @Input()
  field: any;
  constructor(private translate: TranslateService) {}

  ngOnChanges({ field }: { field: SimpleChange }) {
    if (field && field.currentValue && isObject(field.currentValue.value)) {
      this.field._value = keys(field.currentValue.value).map((key: string) => ({
        key: key,
        value: field.currentValue.value[key],
      }));
    }
  }
}
