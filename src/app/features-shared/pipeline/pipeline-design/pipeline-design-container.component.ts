import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChange,
} from '@angular/core';

import { get, merge } from 'lodash-es';

import { RepositoryInfo } from 'app/features-shared/pipeline/pipeline-design/source/pipeline-design-source.component';
import {
  PipelineConfig,
  PipelineRepositorySource,
} from 'app/services/api/pipeline';

@Component({
  selector: 'rc-pipeline-design-container',
  templateUrl: './pipeline-design-container.component.html',
  styleUrls: ['./pipeline-design-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineDesignContainerComponent implements OnChanges {
  fields: any[] = [];
  templateJenkinsfile: string;
  editorOptions = { language: 'jenkinsfile', readOnly: true };
  @Input()
  pipelineConfig: PipelineConfig & {
    type: string;
    source: RepositoryInfo | string;
  };

  ngOnChanges({ pipelineConfig }: { pipelineConfig: SimpleChange }) {
    if (pipelineConfig && pipelineConfig.currentValue) {
      this.pipelineConfig = this.mapPipelineConfig(pipelineConfig.currentValue);
      this.fields = this.getFields(pipelineConfig.currentValue);
      this.templateJenkinsfile = get(
        this.pipelineConfig,
        'strategy.jenkins.jenkinsfile',
        '',
      );
    }
  }

  private mapPipelineConfig(pipelineConfig: PipelineConfig) {
    const type =
      get(pipelineConfig, 'source.git.uri', '') ||
      get(pipelineConfig, 'source.svn.uri', '') ||
      get(pipelineConfig, 'source.codeRepository.name', '')
        ? 'repo'
        : 'script';
    const jenkinsfile: string = get(
      pipelineConfig,
      'strategy.jenkins.jenkinsfile',
      '',
    );
    const repository: PipelineRepositorySource = get(pipelineConfig, 'source');
    const path: string = get(
      pipelineConfig,
      'strategy.jenkins.jenkinsfilePath',
      '',
    );
    const source =
      type === 'repo'
        ? {
            path,
            repo:
              get(repository, 'git.uri', '') ||
              get(repository, 'svn.uri', '') ||
              get(repository, 'codeRepository.name', ''),
            branch:
              get(repository, 'git.ref', '') ||
              get(repository, 'codeRepository.ref', ''),
          }
        : jenkinsfile;
    return merge(pipelineConfig, { type, source });
  }

  private getFields(pipelineConfig: PipelineConfig) {
    const args: any[] =
      get(pipelineConfig, 'strategy.template.arguments') || [];
    return args.reduce((acc: any[], arg) => [...acc, ...arg.items], []);
  }

  trackField(index: number) {
    return index;
  }
}
