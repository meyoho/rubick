import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

export interface RepositoryInfo {
  repo: string;
  branch: string;
  path: string;
}

@Component({
  selector: 'rc-pipeline-design-source',
  templateUrl: './pipeline-design-source.component.html',
  styleUrls: ['./pipeline-design-source.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineDesignSourceComponent {
  editorOptions = { language: 'jenkinsfile', readOnly: true };
  @Input()
  type: string;
  @Input()
  source: string | RepositoryInfo;
}
