import {
  ConfirmType,
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { get } from 'lodash-es';

import {
  mapTriggerIcon,
  mapTriggerTranslateKey,
} from 'app/features-shared/pipeline/utils.ts';
import {
  PipelineApiService,
  PipelineConfig,
  PipelineTrigger,
  TriggerPipelineParameter,
} from 'app/services/api/pipeline';
import { Space } from 'app/services/api/space.service';
import { TranslateService } from 'app/translate/translate.service';
import { PipelineParameterTriggerComponent } from '../parameter-trigger/parameter-trigger.component';

@Component({
  selector: 'rc-pipeline-list',
  templateUrl: 'pipeline-list.component.html',
  styleUrls: ['pipeline-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineListComponent {
  @Input()
  pipelines: PipelineConfig[] = [];
  @Input()
  category: string;
  @Input()
  space: Space;
  @Input()
  columns = ['name', 'history', 'triggers', 'actions'];
  @Input()
  permissions: {
    canCreate: boolean;
    canUpdate: boolean;
    canDelete: boolean;
    canTrigger: boolean;
  };
  @Output()
  started = new EventEmitter<PipelineConfig>();
  @Output()
  deleted = new EventEmitter<PipelineConfig>();
  @Output()
  sortChange = new EventEmitter<{ active: string; direction: string }>();

  mapTriggerIcon = mapTriggerIcon;
  constructor(
    private translate: TranslateService,
    private dialog: DialogService,
    private router: Router,
    private route: ActivatedRoute,
    private notification: NotificationService,
    private message: MessageService,
    private pipelineApi: PipelineApiService,
  ) {}

  getTriggerHint(trigger: PipelineTrigger) {
    return `${this.translate.get(
      trigger.enabled
        ? 'pipeline_trigger_enabled'
        : 'pipeline_trigger_not_enabled',
    )} ${this.translate.get(
      mapTriggerTranslateKey(trigger.type),
    )} ${this.translate.get('pipeline_trigger')}, ${this.translate.get(
      'pipeline_trigger_rules',
    )}: ${trigger.rule}`;
  }

  pipelineIdentity(_: number, pipeline: PipelineConfig) {
    return `${pipeline.namespace}/${pipeline.name}`;
  }

  start(pipeline: PipelineConfig) {
    const parameters: TriggerPipelineParameter[] =
      get(pipeline, 'parameters') || [];
    if (parameters.length) {
      const parameterDialogRef: DialogRef<
        PipelineParameterTriggerComponent
      > = this.dialog.open(PipelineParameterTriggerComponent, {
        size: DialogSize.Medium,
        data: {
          parameters: parameters,
        },
      });
      parameterDialogRef.afterClosed().subscribe((parameterValue: any) => {
        if (parameterValue) {
          this._triggerPipeline(pipeline, parameterValue);
        }
      });
    } else {
      this._triggerPipeline(pipeline);
    }
  }

  sortChanged(event: { active: string; direction: string }) {
    this.sortChange.emit(event);
  }

  delete(pipeline: PipelineConfig) {
    this.dialog
      .confirm({
        title: this.translate.get('pipeline_delete_pipelineconfig_confirm', {
          name: pipeline.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('pipeline_sure'),
        confirmType: ConfirmType.Danger,
        beforeConfirm: (resolve, reject) => {
          this.pipelineApi
            .deletePipelineConfig(get(this.space, 'name', ''), pipeline.name)
            .subscribe(
              () => {
                this.message.success({
                  content: this.translate.get(
                    'pipeline_pipelineconfig_delete_succ',
                    { name: pipeline.name },
                  ),
                });
                resolve();
              },
              (err: any) => {
                this.notification.error({
                  title: this.translate.get(
                    'pipeline_pipelineconfig_delete_failed',
                    { name: pipeline.name },
                  ),
                  content: get(err, 'errors.[0].message', ''),
                });
                reject();
              },
            );
        },
      })
      .then(() => {
        this.deleted.emit(pipeline);
      })
      .catch(() => {});
  }

  update(item: PipelineConfig) {
    this.router.navigate(['../', this.category, item.name, 'update'], {
      relativeTo: this.route,
    });
  }

  copy(item: PipelineConfig) {
    const template = get(item, 'strategy.template');
    this.router.navigate(['../', this.category, 'create'], {
      queryParams: {
        type: 'copy',
        name: item.name,
        method: template ? 'template' : 'jenkinsfile',
      },
      relativeTo: this.route,
    });
  }

  private _triggerPipeline(pipeline: PipelineConfig, paramters?: any) {
    this.pipelineApi
      .triggerPipeline(get(this.space, 'name', ''), pipeline.name, paramters)
      .subscribe(
        () => {
          this.message.success({
            content: this.translate.get('pipeline_start_succ'),
          });
          this.started.emit(pipeline);
        },
        (err: any) => {
          this.notification.error({
            title: this.translate.get('pipeline_start_failed'),
            content: get(err, 'errors.[0].message', ''),
          });
        },
      );
  }
}
