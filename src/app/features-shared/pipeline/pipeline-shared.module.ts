import {
  ImageIntl,
  ImageModule,
  ImageSelectorDataContext,
} from '@alauda/common-snippet';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { DynamicFormsModule } from 'alauda-ui-dynamic-forms';
import { PipelineDiagramModule } from 'alauda-ui-pipeline';

import { ImageSharedModule } from 'app/features-shared/image/shared.module';
import { PipelineApiService } from 'app/services/api/pipeline';
import { ImageIntlService } from 'app/services/image-intl.service';
import { SharedModule } from 'app/shared/shared.module';
import { CredentialSharedModule } from '../credential/shared.module';
import { BasicInfoComponent } from './basic-info/basic-info.component';
import { PipelineCreateContainerComponent } from './create/pipeline-create-container.component';
import {
  PipelineBasicFormComponent,
  PipelineCodeChangeTriggerFormComponent,
  PipelineCronTriggerFormComponent,
  PipelineCronTriggerSelectorComponent,
  PipelineRepositoryFormComponent,
  PipelineScriptFormComponent,
  TriggerTimeSelectorComponent,
} from './forms';
import { PipelineDynamicParameterFormComponent } from './forms/parameters/parameters.component';
import { PipelineTemplateSelectFormComponent } from './forms/template-select/template-select.component';
import { PipelineHistoriesComponent } from './histories/histories.component';
import { PipelineHistoryBasicInfoComponent } from './history-basic-info/history-basic-info.component';
import { PipelineHistoryDetailLogComponent } from './history-detail-log/history-detail-log.component';
import { HistoryPreviewComponent } from './history-preview/history-preview.component';
import { PipelineHistoryStepComponent } from './history-step/history-step.component';
import { PipelineHistoryTableComponent } from './history-table/history-table.component';
import { LogViewModule } from './log-view/log-view.module';
import { LogsComponent } from './logs/logs.component';
import { ModeSelectComponent } from './mode-select/mode-select.component';
import { PipelineParameterTriggerComponent } from './parameter-trigger/parameter-trigger.component';
import { PipelineDesignParametersComponent } from './pipeline-design/parameters/parameters.component';
import { PipelineDesignContainerComponent } from './pipeline-design/pipeline-design-container.component';
import { PipelineDesignSourceComponent } from './pipeline-design/source/pipeline-design-source.component';
import { PipelineListComponent } from './pipeline-list/pipeline-list.component';
import { PreviewJenkinsfileComponent } from './preview-jenkinsfile/preview-jenkinsfile.component';
import { RepositorySelectorComponent } from './repository-selector/repository-selector.component';
import { ImageSelectorActions } from './services/imageSelectorActions';
import { StepsComponent } from './steps/steps.component';
import { PipelineTemplateBasicInfoComponent } from './template/basic-info/pipeline-template-basic-info.component';
import { PipelineTemplateDetailParameterTableComponent } from './template/detail-parameter-table/pipeline-template-detail-parameter-table.component';
import { PipelineTemplateDetailComponent } from './template/detail/pipeline-template-detail.component';
import { PipelineTemplateListCardComponent } from './template/list-card/pipeline-template-list-card.component';
import { PipelineTemplateListContainerComponent } from './template/list-container/list-container.component';
import { PipelineTemplateListComponent } from './template/list/pipeline-template-list.component';
import { PipelineTemplateComponent } from './template/pipeline-template/pipeline-template.component';
import { PipelineTemplateSettingComponent } from './template/setting/pipeline-template-setting.component';
import { PipelineTemplateSyncReportComponent } from './template/sync-report/pipeline-template-sync-report.component';
import { PipelineUpdateContainerComponent } from './update/pipeline-update-container.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule,
    SharedModule,
    PipelineDiagramModule,
    CredentialSharedModule,
    DynamicFormsModule,
    LogViewModule,
    ImageSharedModule,
    ImageModule,
  ],
  declarations: [
    PipelineListComponent,
    HistoryPreviewComponent,
    LogsComponent,
    ModeSelectComponent,
    PipelineCreateContainerComponent,
    StepsComponent,
    PipelineBasicFormComponent,
    PipelineRepositoryFormComponent,
    PipelineCodeChangeTriggerFormComponent,
    PipelineCronTriggerFormComponent,
    TriggerTimeSelectorComponent,
    PipelineScriptFormComponent,
    PipelineCronTriggerSelectorComponent,
    RepositorySelectorComponent,
    PipelineUpdateContainerComponent,
    PipelineListComponent,
    HistoryPreviewComponent,
    LogsComponent,
    BasicInfoComponent,
    PipelineHistoryTableComponent,
    PipelineHistoriesComponent,
    PipelineDesignContainerComponent,
    PipelineDesignSourceComponent,
    PipelineHistoryDetailLogComponent,
    PipelineHistoryBasicInfoComponent,
    PipelineHistoryStepComponent,
    PipelineTemplateSettingComponent,
    PipelineTemplateListComponent,
    PipelineTemplateDetailComponent,
    PipelineTemplateBasicInfoComponent,
    PipelineTemplateComponent,
    PipelineTemplateListCardComponent,
    PipelineTemplateSyncReportComponent,
    PipelineTemplateDetailParameterTableComponent,
    PipelineParameterTriggerComponent,
    PipelineDynamicParameterFormComponent,
    PipelineTemplateSelectFormComponent,
    PipelineTemplateListContainerComponent,
    PipelineDesignParametersComponent,
    PreviewJenkinsfileComponent,
  ],
  exports: [
    PipelineListComponent,
    ModeSelectComponent,
    PipelineCreateContainerComponent,
    RepositorySelectorComponent,
    PipelineUpdateContainerComponent,
    LogsComponent,
    BasicInfoComponent,
    PipelineHistoriesComponent,
    PipelineDesignContainerComponent,
    PipelineHistoryDetailLogComponent,
    PipelineHistoryBasicInfoComponent,
    PipelineTemplateSettingComponent,
    PipelineTemplateListComponent,
    PipelineTemplateDetailComponent,
    PipelineTemplateBasicInfoComponent,
    PipelineTemplateComponent,
    PipelineTemplateListCardComponent,
    PipelineTemplateSyncReportComponent,
    PipelineTemplateDetailParameterTableComponent,
    PipelineParameterTriggerComponent,
    PipelineDynamicParameterFormComponent,
    PipelineTemplateSelectFormComponent,
    PipelineTemplateListContainerComponent,
  ],
  providers: [
    PipelineApiService,
    {
      provide: ImageSelectorDataContext,
      useClass: ImageSelectorActions,
    },
    {
      provide: ImageIntl,
      useClass: ImageIntlService,
    },
  ],
  entryComponents: [
    ModeSelectComponent,
    RepositorySelectorComponent,
    LogsComponent,
    PipelineTemplateSettingComponent,
    PipelineTemplateDetailComponent,
    PipelineTemplateSyncReportComponent,
    PipelineParameterTriggerComponent,
    PreviewJenkinsfileComponent,
  ],
})
export class PipelineModule {}
