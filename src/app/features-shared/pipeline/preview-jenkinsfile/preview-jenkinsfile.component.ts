import { DIALOG_DATA, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';

import { get, isObject } from 'lodash-es';

import {
  PipelineApiService,
  PipelineTemplate,
} from 'app/services/api/pipeline';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './preview-jenkinsfile.component.html',
})
export class PreviewJenkinsfileComponent implements OnInit {
  editorOptions = { language: 'jenkinsfile', readOnly: true };
  template: PipelineTemplate;
  postData: any;
  namespace: string;
  jenkinsfilePreview: string;
  spaceName: string;
  constructor(
    @Inject(DIALOG_DATA) private data: any,
    private api: PipelineApiService,
    private translate: TranslateService,
    private notification: NotificationService,
  ) {
    this.template = this.data.template;
    Object.keys(this.data.postData).forEach(key => {
      if (
        isObject(this.data.postData[key]) ||
        typeof this.data.postData[key] === 'number' ||
        typeof this.data.postData[key] === 'boolean'
      ) {
        this.data.postData[key] = JSON.stringify(this.data.postData[key]);
      }
    });
    this.postData = this.data.postData;
    this.namespace = this.data.namespace;
    this.spaceName = this.data.spaceName;
  }

  ngOnInit() {
    const name = this.template.name;
    const kind = this.template.kind;
    this.api
      .previewPipelineJenkinsfile(this.spaceName, name, kind, {
        values: this.postData,
      })
      .subscribe(
        (res: any) => {
          this.jenkinsfilePreview = res && res.jenkinsfile;
        },
        (error: any) => {
          this.notification.error({
            title: this.translate.get('error'),
            content: get(error, 'errors.[0].message', ''),
          });
        },
      );
  }
}
