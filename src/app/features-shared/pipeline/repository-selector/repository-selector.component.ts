import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  forwardRef,
  HostBinding,
  Input,
  OnInit,
  Optional,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormBuilder,
  FormGroup,
  FormGroupDirective,
  NG_VALUE_ACCESSOR,
} from '@angular/forms';

import { BehaviorSubject } from 'rxjs';
import { map, publishReplay, refCount, switchMap, take } from 'rxjs/operators';

import { CredentialCreateDialogComponent } from 'app/features-shared/credential';
import {
  Credential,
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import { PipelineApiService } from 'app/services/api/pipeline';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { filterBy, getQuery } from 'app/utils/query-builder';

export interface CodeRepositoryModel {
  repo: string;
  secret: any;
  bindingRepository: string;
  kind?: string;
}
@Component({
  selector: 'rc-repository-selector',
  templateUrl: './repository-selector.component.html',
  styleUrls: ['./repository-selector.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => RepositorySelectorComponent),
      multi: true,
    },
    RepositorySelectorComponent,
  ],
})

/**
 * default value model:
 * {
 *    repo: '',
      bindingRepository: '',
      secret: {name:'', string: ''},
    }
 */
export class RepositorySelectorComponent
  implements ControlValueAccessor, OnInit {
  secretCreatePermission: boolean;
  repositoryDisplay: string;
  modelGroup: FormGroup;
  sourceType = 'select';

  secrets$$ = new BehaviorSubject(null);
  codes$$ = new BehaviorSubject(null);
  secrets$ = this.secrets$$.pipe(
    switchMap(() => {
      const query = getQuery(
        filterBy(
          'secretType',
          `${CredentialType.BasicAuth}:${CredentialType.SSH}`,
        ),
      );
      query['space_name'] = this.spaceName;
      return this.secretApi.find(query, this.namespace);
    }),
    map(res =>
      res.items.map((secret: Credential) => ({
        name: secret.name,
        namespace: secret.namespace,
        displayName: secret.displayName,
      })),
    ),
    publishReplay(1),
    refCount(),
  );
  codes$ = this.codes$$.pipe(
    switchMap(() => this.pipelineApi.codeRepositories(this.spaceName)),
    map(res => res.items || []),
    publishReplay(1),
    refCount(),
  );

  @Input()
  namespace: string;
  @Input()
  spaceName: string;
  @Output()
  repoChanged = new EventEmitter();
  @Input()
  valueConverter: (
    data: any,
    codeOptions?: CodeRepositoryModel[],
    secretOptions?: Credential[],
  ) => {};
  @ViewChild('repo')
  repoRef: TemplateRef<any>;

  constructor(
    private dialog: DialogService,
    private secretApi: CredentialApiService,
    private roleUtil: RoleUtilitiesService,
    private cdr: ChangeDetectorRef,
    private pipelineApi: PipelineApiService,
    private fb: FormBuilder,
    @Optional() private parentForm: FormGroupDirective,
  ) {
    this.modelGroup = this.buildForm();
  }

  async ngOnInit() {
    [
      this.secretCreatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_secret',
      { space_name: this.spaceName },
      ['create'],
    );
  }

  buildForm() {
    return this.fb.group({
      repo: '',
      secret: '',
      bindingRepository: '',
      sourceType: 'GIT',
    });
  }

  open() {
    this.dialog.open(this.repoRef, {
      size: DialogSize.Large,
    });
    this.onTouched();
  }

  hideDialog() {
    this.dialog.closeAll();
  }

  addSecret() {
    const ref = this.dialog.open(CredentialCreateDialogComponent, {
      data: {
        namespace: this.namespace,
        space: this.spaceName,
        types: [CredentialType.BasicAuth, CredentialType.SSH],
        env: 'workspace',
      },
    });
    ref.afterClosed().subscribe((secret: any) => {
      if (secret) {
        this.secrets$$.next(null);
        this.modelGroup.controls.secret.patchValue(secret);
      }
    });
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    return (
      this.parentForm && this.parentForm.submitted && this.parentForm.invalid
    );
  }

  getSubmitDisabled() {
    return !(
      (this.sourceType === 'input' && this.modelGroup.controls.repo.valid) ||
      (this.sourceType === 'select' &&
        this.modelGroup.controls.bindingRepository.valid)
    );
  }

  writeValue(value: CodeRepositoryModel) {
    if (!value || (!value.bindingRepository && !value.repo)) {
      return;
    }
    this.modelGroup = this.buildForm();
    if (value.secret && !value.secret.name) {
      value.secret = '';
    }
    this.modelGroup.patchValue(value);
    if (value.bindingRepository) {
      this.sourceType = value.kind || 'select';
      this.repositoryDisplay = value.bindingRepository;
    } else {
      this.sourceType = value.kind || 'input';
      this.repositoryDisplay = value.repo;
    }
    this.cdr.markForCheck();
  }

  valueChange = (_: any) => {};

  onTouched = () => {};

  registerOnChange(fn: any) {
    this.valueChange = fn;
  }

  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  onSubmit() {
    const data = {
      repo: '',
      bindingRepository: '',
      secret: { name: '', namespace: '' },
      sourceType: '',
    };
    if (this.sourceType === 'input') {
      this.repositoryDisplay = this.getControlValue('repo');
      data.repo = this.getControlValue('repo');
      data.secret = this.getControlValue('secret');
      data.sourceType = this.getControlValue('sourceType');
    } else {
      this.repositoryDisplay = this.getControlValue('bindingRepository');
      data.bindingRepository = this.getControlValue('bindingRepository');
    }
    this.cdr.detectChanges();
    let secs, cds;
    this.secrets$.pipe(take(1)).subscribe(x => {
      secs = x;
    });
    this.codes$.pipe(take(1)).subscribe(x => {
      cds = x;
    });
    this.valueChange(
      this.valueConverter ? this.valueConverter(data, cds, secs) : data,
    );
    this.dialog.closeAll();
  }

  trackFn(val: any) {
    return (val && val.name) || val;
  }

  private getControlValue(controlName: string): any {
    return this.modelGroup.controls[controlName].value;
  }
}
