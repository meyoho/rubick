import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';

import { Observable, of } from 'rxjs';
import { catchError, map, take } from 'rxjs/operators';

import { CredentialCreateDialogComponent } from 'app/features-shared/credential';
import {
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import {
  ListResult,
  PipelineApiService,
  Registry,
} from 'app/services/api/pipeline';
import { filterBy, getQuery } from 'app/utils/query-builder';

@Injectable()
export class ImageSelectorActions {
  constructor(
    private pipelineApi: PipelineApiService,
    private dialog: DialogService,
    private credentialApi: CredentialApiService,
  ) {}

  createSecret(params: { namespace: string; space: string }): Observable<any> {
    return this.dialog
      .open(CredentialCreateDialogComponent, {
        data: {
          namespace: params.namespace,
          space: params.space,
          types: [CredentialType.DockerConfig],
          env: 'workspace',
        },
      })
      .afterClosed()
      .pipe(
        map((result: any) => {
          if (!result) {
            return null;
          }

          return result;
        }),
        take(1),
      );
  }

  getImageRepositories(params: {
    spaceName: string;
  }): Observable<ListResult<Registry>> {
    const result = this.pipelineApi.imageRepositories(params.spaceName).pipe(
      map(imageRepositories => {
        const { items, ...rest } = imageRepositories;
        return {
          items: items.map(({ tags, ...item }) => ({
            tags: [...tags].sort((a, b) => {
              const stringA = a.createdAt;
              const stringB = b.createdAt;
              return stringB.localeCompare(stringA);
            }),
            ...item,
          })),
          ...rest,
        };
      }),
    );
    return result;
  }

  getSecrets(params: { spaceName: string }): Observable<any> {
    return this.credentialApi.find({
      ...getQuery(filterBy('secretType', CredentialType.DockerConfig)),
      space_name: params.spaceName,
    });
  }

  getPipelineExportsOptions(params: { tempName: string }) {
    return this.pipelineApi
      .getPipelineExportsOptions(params.tempName)
      .pipe(catchError(_ => of([])));
  }
}
