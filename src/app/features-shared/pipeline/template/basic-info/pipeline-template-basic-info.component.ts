import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Output,
} from '@angular/core';

import { get } from 'lodash-es';
import { interval } from 'rxjs';
import { map, publishReplay, refCount, tap } from 'rxjs/operators';

import {
  PipelineApiService,
  PipelineTemplateSync,
} from 'app/services/api/pipeline';
import { PipelineTemplateSettingComponent } from '../setting/pipeline-template-setting.component';
import { PipelineTemplateSyncReportComponent } from '../sync-report/pipeline-template-sync-report.component';

export interface SyncResultCount {
  success: number;
  failure: number;
  skip: number;
}

type SyncResultCountKey = 'success' | 'failure' | 'skip';
@Component({
  selector: 'rc-pipeline-template-basic-info',
  templateUrl: './pipeline-template-basic-info.component.html',
  styleUrls: [
    './pipeline-template-basic-info.component.scss',
    '../../shared-style/fields.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateBasicInfoComponent {
  loadingStatus = ['Draft', 'Pending'];
  templateSync: PipelineTemplateSync;
  syncing: boolean;
  syncResultCount: SyncResultCount;

  currentDate = interval(2000).pipe(
    map(() => new Date().getTime()),
    publishReplay(1),
    refCount(),
  );

  @Output()
  syncChange = new EventEmitter<void>();
  constructor(
    private pipelineApi: PipelineApiService,
    private dialog: DialogService,
    private cdr: ChangeDetectorRef,
  ) {}

  fetchData = () =>
    this.pipelineApi.templateSyncDetail().pipe(
      tap((result: PipelineTemplateSync) => {
        if (!result) {
          return;
        }
        this.setLocalStatus(result);
      }),
    );

  sync() {
    this.pipelineApi
      .templateSyncTrigger(this.templateSync.name, {
        ...this.templateSync.__original,
        status: { phase: 'Pending' },
      })
      .subscribe(
        (result: PipelineTemplateSync) => {
          this.setLocalStatus(result);
        },
        () => {},
      );
  }

  setting() {
    const ref = this.dialog.open(PipelineTemplateSettingComponent, {
      size: DialogSize.Large,
      data: {
        setting: this.templateSync,
      },
    });
    ref.afterClosed().subscribe(
      (result: PipelineTemplateSync) => {
        if (!result) {
          return;
        }
        this.setLocalStatus(result);
      },
      () => {},
    );
  }

  syncReport() {
    this.dialog.open(PipelineTemplateSyncReportComponent, {
      size: DialogSize.Large,
      data: {
        conditions: get(this.templateSync, 'status.conditions', []),
        count: this.syncResultCount,
      },
    });
  }

  getDateTimes(phase: string) {
    return new Date(phase || '').getTime();
  }

  private setLocalStatus(result: PipelineTemplateSync) {
    this.templateSync = result;
    const currentSyncing = this.loadingStatus.includes(
      get(result, 'status.phase', ''),
    );
    if (!currentSyncing && this.syncing) {
      this.syncChange.emit();
    }
    this.syncing = currentSyncing;
    this.syncResultCount = this.countResult(result);
    this.cdr.detectChanges();
  }

  private countResult(result: PipelineTemplateSync): SyncResultCount {
    return (get(result, 'status.conditions') || []).reduce(
      (accum: SyncResultCount, templateStatus: { status: string }) => {
        const key = this.toStatusKey(templateStatus);
        if (key) {
          return {
            ...accum,
            [key]: accum[key] + 1,
          };
        } else {
          return accum;
        }
      },
      {
        success: 0,
        failure: 0,
        skip: 0,
      },
    );
  }

  private toStatusKey(templateStatus: { status: string }): SyncResultCountKey {
    switch (templateStatus.status) {
      case 'Success':
        return 'success';
      case 'Failure':
        return 'failure';
      case 'Skip':
        return 'skip';
      case 'Deleted':
        return 'success';
      default:
        return;
    }
  }
}
