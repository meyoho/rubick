import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { get } from 'lodash-es';

import { TemplateArgumentField } from 'app/services/api/pipeline';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  selector: 'rc-pipeline-template-detail-parameter-table',
  templateUrl: './pipeline-template-detail-parameter-table.component.html',
  styleUrls: ['./pipeline-template-detail-parameter-table.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateDetailParameterTableComponent {
  columns = ['parameter', 'description'];
  @Input()
  parameterField: TemplateArgumentField;
  constructor(private translate: TranslateService) {}

  getValue(path: string, target?: { [key: string]: any }) {
    const currentLang = this.translate.currentLang;
    return get(
      target || this.parameterField,
      `${path}.${currentLang === 'en' ? 'en' : 'zh-CN'}`,
      '',
    );
  }
}
