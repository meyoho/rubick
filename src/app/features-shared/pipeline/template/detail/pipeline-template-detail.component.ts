import { DIALOG_DATA } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { PipelineTemplate } from 'app/services/api/pipeline';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  selector: 'rc-pipeline-template-detail',
  templateUrl: './pipeline-template-detail.component.html',
  styleUrls: ['./pipeline-template-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateDetailComponent {
  customLabelIndex = ['sonarqube'];
  template: PipelineTemplate;
  constructor(
    @Inject(DIALOG_DATA) public data: { template: PipelineTemplate },
    private translate: TranslateService,
  ) {
    this.template = data.template;
  }

  key() {
    return this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
  }
}
