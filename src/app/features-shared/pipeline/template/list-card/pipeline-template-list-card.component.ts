import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';

import { map } from 'rxjs/operators';

import {
  STYLE_ICONS,
  STYLE_ICONS_NAME_MAP,
} from 'app/features-shared/pipeline/constant';
import {
  PipelineTemplate,
  templateStagesConvert,
} from 'app/services/api/pipeline';
import { TranslateService } from 'app/translate/translate.service';
import { PipelineTemplateDetailComponent } from '../detail/pipeline-template-detail.component';

@Component({
  selector: 'rc-pipeline-template-list-card',
  templateUrl: './pipeline-template-list-card.component.html',
  styleUrls: ['./pipeline-template-list-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateListCardComponent implements OnInit {
  customLabelIndex = ['sonarqube'];
  styleIcons = STYLE_ICONS;
  translateKey$ = this.translate.currentLang$.pipe(
    map((lang: string) => {
      return lang === 'en' ? 'en' : 'zh-CN';
    }),
  );
  @Input()
  template: PipelineTemplate;
  constructor(
    private dialog: DialogService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    this.template.stages = templateStagesConvert(this.template.stages);
  }

  detail() {
    this.dialog.open(PipelineTemplateDetailComponent, {
      size: DialogSize.Large,
      data: {
        template: this.template,
      },
    });
  }

  getIconTitle(name: string) {
    return STYLE_ICONS_NAME_MAP[name];
  }
}
