import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';

import { get, groupBy } from 'lodash-es';
import { Observable, of, Subject } from 'rxjs';
import { map, tap } from 'rxjs/operators';

import {
  ListResult,
  PipelineApiService,
  PipelineTemplate,
} from 'app/services/api/pipeline';
import { TranslateService } from 'app/translate/translate.service';
import { filterBy, getQuery } from 'app/utils/query-builder';

@Component({
  selector: 'rc-pipeline-template-list-container',
  templateUrl: './list-container.component.html',
  styleUrls: ['./list-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateListContainerComponent implements OnInit {
  type = 'custom';
  searchKey = '';
  params$ = new Subject<{ search: string }>();
  clusterTemplateCount = 0;
  templateCount = 0;

  constructor(
    private pipelineApi: PipelineApiService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.getClusterTemplateList().subscribe(
      () => {
        this.cdr.markForCheck();
      },
      () => {},
    );
  }

  fetchData = (params: { search: string }) => {
    if (!params) {
      of();
    }
    return this.getClusterTemplateList(params);
  };

  getClusterTemplateList(
    params?: any,
  ): Observable<{ [key: string]: PipelineTemplate[] }> {
    return this.pipelineApi
      .clusterTemplateList(
        getQuery(
          filterBy(
            this.translate.currentLang === 'en'
              ? 'displayEnName'
              : 'displayZhName',
            get(params, 'search', ''),
          ),
        ),
      )
      .pipe(
        map((templates: ListResult<PipelineTemplate>) =>
          groupBy(templates.items, (item: PipelineTemplate) =>
            item.official ? 'official' : 'private',
          ),
        ),
        tap((groupTemplates: any) => {
          this.templateCount = get(groupTemplates, 'private.length', 0);
          this.clusterTemplateCount = get(groupTemplates, 'official.length', 0);
        }),
      );
  }

  search(event: string) {
    this.searchKey = event;
    this.refetchList();
  }

  typeChange(type: string) {
    this.type = type;
    this.refetchList();
  }

  syncChange() {
    this.type = 'custom';
    this.refetchList();
  }

  refetchList() {
    this.params$.next({ search: this.searchKey });
  }
}
