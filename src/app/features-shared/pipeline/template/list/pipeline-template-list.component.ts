import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { PipelineTemplate } from 'app/services/api/pipeline';

@Component({
  selector: 'rc-pipeline-template-list',
  templateUrl: './pipeline-template-list.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateListComponent {
  @Input()
  templates: PipelineTemplate[];
}
