import { ChangeDetectionStrategy, Component, ViewChild } from '@angular/core';
import { PipelineTemplateListContainerComponent } from '../list-container/list-container.component';

@Component({
  selector: 'rc-pipeline-private-template',
  templateUrl: './pipeline-template.component.html',
  styleUrls: ['./pipeline-template.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateComponent {
  @ViewChild(PipelineTemplateListContainerComponent)
  templateContainer: PipelineTemplateListContainerComponent;
  constructor() {}

  syncChange() {
    this.templateContainer.typeChange('custom');
  }
}
