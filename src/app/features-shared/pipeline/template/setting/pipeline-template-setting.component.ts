import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { get, groupBy } from 'lodash-es';
import { BehaviorSubject } from 'rxjs';
import { map, publishReplay, refCount, switchMap } from 'rxjs/operators';

import { CredentialCreateDialogComponent } from 'app/features-shared/credential';
import {
  Credential,
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import {
  PipelineApiService,
  PipelineTemplateSync,
  PipelineTemplateSyncConfig,
} from 'app/services/api/pipeline';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './pipeline-template-setting.component.html',
  styleUrls: ['./pipeline-template-setting.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateSettingComponent {
  model: any;
  sourceType = 'input';
  namespace: string;

  secrets$$ = new BehaviorSubject(null);
  codes$$ = new BehaviorSubject(null);
  secrets$ = this.secrets$$.pipe(
    switchMap(() => this.secretApi.find({})),
    map((res: { items: Credential[]; length: number }) =>
      groupBy(res.items || [], (item: Credential) =>
        item.namespace && item.namespace === 'global-credentials'
          ? 'public'
          : 'private',
      ),
    ),
    publishReplay(1),
    refCount(),
  );
  codes$ = this.codes$$.pipe(
    switchMap(() => this.pipelineApi.codeRepositories()),
    map(res => res.items),
    publishReplay(1),
    refCount(),
  );

  setting: PipelineTemplateSync;
  constructor(
    @Inject(DIALOG_DATA)
    public data: { setting: PipelineTemplateSync; namespace: string },
    private dialogRef: DialogRef<PipelineTemplateSettingComponent>,
    private pipelineApi: PipelineApiService,
    private secretApi: CredentialApiService,
    private dialog: DialogService,
    private message: MessageService,
    private notification: NotificationService,
    private translate: TranslateService,
  ) {
    ({ setting: this.setting } = data);

    const secret = data.setting
      ? `${
          get(data, 'setting.secret.namespace')
            ? get(data, 'setting.secret.namespace') + '/'
            : ''
        }${get(data, 'setting.secret.name') || ''}`
      : null;

    this.model = {
      codeRepository: get(this.setting, 'codeRepositoryName'),
      repo: get(this.setting, 'gitUri'),
      branch: get(this.setting, 'branch'),
      secret,
    };
    if (this.model.codeRepository) {
      this.model.repo = '';
      this.model.secret = '';
    }
    if (this.model.repo) {
      this.sourceType = 'input';
    }
  }

  addSecret() {
    const ref = this.dialog.open(CredentialCreateDialogComponent, {
      size: DialogSize.Large,
      data: {
        types: [CredentialType.BasicAuth],
        env: 'admin',
      },
    });
    ref.afterClosed().subscribe((secret: any) => {
      if (secret) {
        this.secrets$$.next(null);
        this.secrets$
          .subscribe(() => {
            this.model.secret = this.secretToValue(secret);
          })
          .unsubscribe();
      }
    });
  }

  onSubmit(value: any) {
    const data = this.getData(value);
    if (!this.setting) {
      this.pipelineApi.templateSetting(data).subscribe(
        (result: PipelineTemplateSync) => {
          this.dialogRef.close(result);
          this.message.success({
            content: this.translate.get('pipeline_template_sync_setting_succ'),
          });
        },
        (err: any) => {
          this.notification.error({
            title: this.translate.get('pipeline_template_sync_setting_fail'),
            content: get(err, 'errors.[0].message', ''),
          });
        },
      );
    } else {
      this.pipelineApi.updateTemplateSetting(this.setting.name, data).subscribe(
        (result: PipelineTemplateSync) => {
          this.dialogRef.close(result);
          this.message.success({
            content: this.translate.get(
              'pipeline_template_sync_setting_update_succ',
            ),
          });
        },
        (err: any) => {
          this.notification.error({
            title: this.translate.get(
              'pipeline_template_sync_setting_update_fail',
            ),
            content: get(err, 'errors.[0].message', ''),
          });
        },
      );
    }
  }

  hideDialog() {
    this.dialog.closeAll();
  }

  private getData(value: any) {
    let data: PipelineTemplateSyncConfig;
    if (this.sourceType === 'select') {
      data = {
        codeRepository: {
          name: value.codeRepository,
          ref: value.branch,
        },
      };
    } else {
      data = {
        git: {
          uri: value.repo,
          ref: value.branch,
        },
      };
      if (value.secret) {
        data['secret'] = toSecretIdentity(value.secret);
      }
    }
    return data;
  }

  secretToValue(secret: Credential) {
    return `${secret.namespace}/${secret.name}`;
  }
}

function toSecretIdentity(value: string) {
  const [namespace, name] = (value || '').split('/');

  return {
    name,
    namespace,
  };
}
