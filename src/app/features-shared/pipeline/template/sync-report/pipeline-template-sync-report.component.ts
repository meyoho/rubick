import { DIALOG_DATA } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';

import { SyncResultCount } from '../basic-info/pipeline-template-basic-info.component';
import { PipelineTemplateSyncCondition } from 'app/services/api/pipeline';

@Component({
  selector: 'rc-pipeline-template-sync-report',
  templateUrl: './pipeline-template-sync-report.component.html',
  styleUrls: [
    './pipeline-template-sync-report.component.scss',
    '../../shared-style/fields.scss',
  ],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineTemplateSyncReportComponent {
  columns = ['result', 'name', 'type', 'version', 'action'];

  syncResultCount: SyncResultCount;
  conditions: PipelineTemplateSyncCondition[];
  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      conditions: PipelineTemplateSyncCondition[];
      count: SyncResultCount;
    },
  ) {
    this.conditions = this.data.conditions;
    this.syncResultCount = this.data.count;
  }
}
