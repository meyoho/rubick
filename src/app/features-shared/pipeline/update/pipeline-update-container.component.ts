import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChange,
  ViewChild,
} from '@angular/core';
import { FormArray, FormBuilder, FormGroup, NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { GroupDefine } from 'alauda-ui-dynamic-forms';
import { extend, get, isObject } from 'lodash-es';
import { map, publishReplay, refCount } from 'rxjs/operators';

import {
  PipelineApiService,
  PipelineConfigModel,
  PipelineTemplate,
  PipelineTemplateResource,
  templateStagesConvert,
} from 'app/services/api/pipeline';
import { Space } from 'app/services/api/space.service';
import { TranslateService } from 'app/translate/translate.service';
import { MODEL } from '../constant';
import { PipelineDynamicParameterFormComponent } from '../forms/parameters/parameters.component';
import { PreviewJenkinsfileComponent } from '../preview-jenkinsfile/preview-jenkinsfile.component';
import { PipelineTemplateDetailComponent } from '../template/detail/pipeline-template-detail.component';

@Component({
  selector: 'rc-pipeline-update-container',
  templateUrl: './pipeline-update-container.component.html',
  styleUrls: ['./pipeline-update-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineUpdateContainerComponent implements OnChanges {
  editorOptions = { language: 'jenkinsfile', readOnly: true };
  form: FormGroup;
  method: string;
  jenkinsfilePreview: string;
  key$ = this.translate.currentLang$.pipe(
    map((lang: string) => (lang === 'en' ? 'en' : 'zh-CN')),
    publishReplay(1),
    refCount(),
  );

  @Input()
  name: string;
  @Input()
  namespace: string;
  @Input()
  loading: boolean;
  @Input()
  pipelineConfig: PipelineConfigModel;
  @Input()
  space: Space;
  @Input()
  project: string;

  @ViewChild('updateForm')
  updateForm: NgForm;
  @ViewChild(PipelineDynamicParameterFormComponent)
  parameterForm: PipelineDynamicParameterFormComponent;

  get sourceType() {
    return this.form.get('basic').get('source').value;
  }

  get template(): string {
    return null;
  }

  constructor(
    private api: PipelineApiService,
    private fb: FormBuilder,
    private router: Router,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private notification: NotificationService,
    private message: MessageService,
    private dialog: DialogService,
    private _location: Location,
  ) {
    this.form = this.formBuilder();
  }

  ngOnChanges({ pipelineConfig }: { pipelineConfig: SimpleChange }) {
    if (pipelineConfig && pipelineConfig.currentValue) {
      const type = get(pipelineConfig.currentValue, 'basic.source', 'repo');
      this.method = get(pipelineConfig.currentValue, 'template')
        ? 'template'
        : 'jenkinsfile';
      this.form.patchValue(pipelineConfig.currentValue);
      if (this.method === 'template') {
        this.form.removeControl('editor_script');
        this.form.removeControl('jenkinsfile');
      } else {
        this.form.removeControl('template');
        this.form.removeControl('parameters');
        if (type === 'repo') {
          this.form.removeControl('editor_script');
        } else if (type === 'script') {
          this.form.removeControl('jenkinsfile');
        }
      }
    }
  }

  submit() {
    if (this.submitValid()) {
      let data = {
        ...this.form.value,
      };
      if (this.method === 'template') {
        data = {
          ...data,
          template: this.getMergedTemplate(),
          __original: this.pipelineConfig.__original,
        };
      } else {
        const jenkinsfile = {
          ...(get(this.form, 'value.jenkinsfile.repo') || {}),
          branch: get(this.form, 'value.jenkinsfile.branch', ''),
          path: get(this.form, 'value.jenkinsfile.path', ''),
        };
        data = {
          ...data,
          jenkinsfile: jenkinsfile,
          __original: this.pipelineConfig.__original,
        };
      }
      this.api
        .updatePipelineConfig({
          name: this.name,
          data: data,
          space_name: this.space.name,
        })
        .subscribe(
          () => {
            this.message.success(this.translate.get('pipeline_update_success'));
            this.router.navigate(
              ['../../', `${get(this.form.value, 'basic.name')}`],
              {
                relativeTo: this.route,
              },
            );
          },
          error => {
            this.notification.error({
              title: this.translate.get('pipeline_update_fail'),
              content: get(error, 'errors.[0].message', ''),
            });
          },
        );
    }
  }

  getFormArrayControl(name: string) {
    return (<FormArray>this.form.get(name)).controls;
  }

  async cancel() {
    if (this.form.dirty) {
      try {
        await this.dialog.confirm({
          title: this.translate.get('pipeline_update_cancel_confirm', {
            name: this.name,
          }),
          content: this.translate.get('pipeline_update_cancel_hint'),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
        });
        this._location.back();
      } catch (rejection) {}
    } else {
      this._location.back();
    }
  }

  preview() {
    this.dialog.open(PreviewJenkinsfileComponent, {
      size: DialogSize.Large,
      data: {
        template: get(this.form, 'controls.template.value'),
        postData: this.parameterForm.getValues(),
        namespace: this.namespace,
        spaceName: this.space.name || '',
      },
    });
  }

  templateDetail(template: PipelineTemplate) {
    template.stages = templateStagesConvert(template.stages);
    this.dialog.open(PipelineTemplateDetailComponent, {
      size: DialogSize.Large,
      data: {
        template: template,
      },
    });
  }

  getCodeTriggerControls() {
    return get(this.form, 'controls.triggers.controls[0]', '');
  }

  getCronTriggerControls() {
    return get(this.form, 'controls.triggers.controls[1]', '');
  }

  getJenkinsBinding() {
    return get(this.form, 'controls.basic.controls.jenkins_instance.value');
  }

  private formBuilder() {
    return this.fb.group({
      template: get(MODEL, 'templateModel.template'),
      basic: this.fb.group(MODEL.basicModel),
      jenkinsfile: this.fb.group(MODEL.jenkinsfileModel),
      editor_script: this.fb.group(MODEL.scriptModel),
      triggers: this.fb.array([
        this.fb.group(
          extend(
            MODEL.codeTriggerModel,
            get(this.pipelineConfig, 'triggers.[0]', ''),
          ),
        ),
        this.fb.group(
          extend(
            MODEL.cronTriggerModel,
            get(this.pipelineConfig, 'triggers.[1]', ''),
          ),
        ),
      ]),
    });
  }

  private submitValid() {
    this.updateForm.onSubmit(null);
    return this.updateForm.valid;
  }

  private getMergedTemplate(): PipelineTemplateResource {
    const parameters = this.parameterForm.getValues();
    const template = get(this.form, 'controls.template.value.__original');
    const dyArguments: GroupDefine[] = get(template, 'spec.arguments');
    dyArguments.forEach((argument: GroupDefine) => {
      argument.items.forEach((item: any) => {
        // TODO: remove after api refactor
        if (item.display.type === 'boolean') {
          item.value = JSON.stringify(!!parameters[item.name]);
          if (typeof item.default === 'boolean') {
            item.default = JSON.stringify(item.default);
          }
        } else if (this.unstring(parameters[item.name])) {
          item.value = JSON.stringify(parameters[item.name]);
        } else {
          item.value = parameters[item.name] || '';
        }
      });
    });
    return template;
  }

  private unstring(value: any) {
    return isObject(value) || typeof value === 'number';
  }
}
