export function mapTriggerTranslateKey(triggerType: string) {
  switch (triggerType) {
    case 'cron':
      return 'pipeline_cron_trigger';
    case 'codeChange':
      return 'pipeline_trigger_code_change';
    case 'imageChange':
      return 'pipeline_trigger_image_change';
    default:
      return 'unknown';
  }
}

export function mapTriggerIcon(triggerType: string) {
  switch (triggerType) {
    case 'cron':
      return 'basic:time';
    case 'codeChange':
      return 'basic:code';
    case 'imageChange':
    // TODO: 添加代码仓库图标
    default:
      return 'unknown';
  }
}

const historyStatusIconMap: { [key: string]: string } = {
  Queued: 'basic:hourglass_half_circle_s',
  Pending: 'basic:play_circle_s',
  Running: 'basic:sync_circle_s',
  Failed: 'basic:close_circle_s ',
  Complete: 'check_circle_s',
  Cancelled: 'basic:minus_circle_s',
  Aborted: 'basic:paused_circle_s',
  Unknown: 'basic:question_circle_s',
};

const historyDetailStatusIconMap: { [key: string]: string } = {
  Queued: 'basic:queue_s',
  Pending: 'basic:play_12_s',
  Running: 'basic:sync',
  Failed: 'basic:fail_s ',
  Complete: 'check_s',
  Cancelled: 'basic:cancel_s',
  Aborted: 'basic:abort_s',
  Unknown: 'basic:unknown_s',
};

const historyStatusTranslateMap: { [key: string]: string } = {
  Queued: 'pipeline_history_queued',
  Pending: 'pipeline_history_pending',
  Running: 'pipeline_history_running',
  Failed: 'pipeline_history_failed',
  Complete: 'pipeline_history_complete',
  Cancelled: 'pipeline_history_cancelled',
  Aborted: 'pipeline_history_aborted',
  Unknown: 'pipeline_history_unknown',
};

export function getHistoryStatus(phase: string, type?: 'detail' | 'preview') {
  let icon;
  if (type === 'detail') {
    icon =
      historyDetailStatusIconMap[phase] ||
      historyDetailStatusIconMap['Unknown'];
  } else {
    icon = historyStatusIconMap[phase] || historyStatusIconMap['Unknown'];
  }
  return {
    icon: icon,
    translateKey:
      historyStatusTranslateMap[phase] || historyStatusTranslateMap['Unknown'],
  };
}

export const PIPELINE_ALL_STATUS = [
  'Queued',
  'Pending',
  'Running',
  'Failed',
  'Complete',
  'Cancelled',
  'Aborted',
  'Unknown',
];
