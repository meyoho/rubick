import { Component, Input } from '@angular/core';

@Component({
  selector: 'rc-project-quota-display-list',
  templateUrl: './template.component.html',
  styleUrls: ['./template.component.scss'],
})
export class ProjectQuotaDisplayListComponent {
  @Input()
  values: { [key: string]: string };

  isExist(key: string) {
    return !!this.values[key];
  }
}
