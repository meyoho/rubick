import { NgModule } from '@angular/core';

import { AuditSharedModule } from 'app/features-shared/audit/shared.module';
import { CredentialSharedModule } from 'app/features-shared/credential/shared.module';
import { BatchImportAccountsDialogComponent } from 'app/features-shared/project/space-create-form/batch-import-accounts-dialog/component';
import { SpaceCreateFormComponent } from 'app/features-shared/project/space-create-form/component';
import { ToolChainSharedModule } from 'app/features-shared/tool-chain/tool-chain-shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { AddClusterComponent } from 'app2/features/projects/add-cluster/add-cluster.component';
import { ProjectDetailComponent } from 'app2/features/projects/detail/project-detail.component';
import { ProjectsListComponent } from 'app2/features/projects/list/projects-list.component';
import { BindingSpacesDialogComponent } from 'app2/features/projects/namespace/binding.component';
import { ProjectNamespacesListComponent } from 'app2/features/projects/namespace/project-namespace-list.component';
import { UnbindingSpacesDialogComponent } from 'app2/features/projects/namespace/unbinding.component';
import { QuotaUpdateComponent } from 'app2/features/projects/quota-update/quota-update.component';
import { BindingSpaceDialogComponent } from 'app2/features/projects/spaces/binding.component';
import { DeleteSpaceDialogComponent } from 'app2/features/projects/spaces/delete.component';
import { NoticeDialogComponent } from 'app2/features/projects/spaces/notice.component';
import { ProjectSpaceListComponent } from 'app2/features/projects/spaces/project-space-list.component';
import { UnbindingSpaceDialogComponent } from 'app2/features/projects/spaces/unbinding.component';
import { UpdateSpaceDialogComponent } from 'app2/features/projects/spaces/update.component';
import { CreateRepositoryDialogComponent } from 'app_user/features/project/tool-chain/create-repository-dialog/create-repository-dialog.component';
import { UserToolChainListPageComponent } from 'app_user/features/project/tool-chain/list/list.component';
import { ToolChainSubscribeDialogComponent } from 'app_user/features/project/tool-chain/subscribe-dialog/subscribe-dialog.component';
import { ToolChainSubscribeEditComponent } from 'app_user/features/project/tool-chain/subscribe-edit/subscribe-edit.component';
import { SubscribeToolListComponent } from 'app_user/features/project/tool-chain/subscribe-tool-list/subscribe-tool-list.component';
import { ToolChainSubscribeUpdateDialogComponent } from 'app_user/features/project/tool-chain/subscribe-update-dialog/update-dialog.component';
import { ProjectQuotaDisplayListComponent } from './project-quota-display-list/template.component';

const EXPORT_COMPONENTS = [
  ProjectDetailComponent,
  SpaceCreateFormComponent,
  BatchImportAccountsDialogComponent,
  ProjectSpaceListComponent,
  ProjectNamespacesListComponent,
  BindingSpaceDialogComponent,
  DeleteSpaceDialogComponent,
  NoticeDialogComponent,
  UnbindingSpaceDialogComponent,
  UpdateSpaceDialogComponent,
  UserToolChainListPageComponent,
  ToolChainSubscribeDialogComponent,
  CreateRepositoryDialogComponent,
  ToolChainSubscribeEditComponent,
  ToolChainSubscribeUpdateDialogComponent,
  BindingSpacesDialogComponent,
  UnbindingSpacesDialogComponent,
  AddClusterComponent,
  QuotaUpdateComponent,
  ProjectsListComponent,
  ProjectQuotaDisplayListComponent,
  SubscribeToolListComponent,
];

const ENTRY_COMPONENTS = [
  BatchImportAccountsDialogComponent,
  BindingSpaceDialogComponent,
  DeleteSpaceDialogComponent,
  NoticeDialogComponent,
  UnbindingSpaceDialogComponent,
  UpdateSpaceDialogComponent,
  ToolChainSubscribeDialogComponent,
  CreateRepositoryDialogComponent,
  ToolChainSubscribeUpdateDialogComponent,
  BindingSpacesDialogComponent,
  UnbindingSpacesDialogComponent,
  AddClusterComponent,
  QuotaUpdateComponent,
];

@NgModule({
  imports: [
    AuditSharedModule,
    CredentialSharedModule,
    ToolChainSharedModule,
    SharedModule,
    FormTableModule,
  ],
  exports: [...EXPORT_COMPONENTS],
  declarations: [...EXPORT_COMPONENTS],
  entryComponents: [...ENTRY_COMPONENTS],
})
export class ProjectSharedModule {}
