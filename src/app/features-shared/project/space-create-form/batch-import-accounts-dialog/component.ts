import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';

import { remove } from 'lodash-es';

import { LdapGroup, OrgService } from 'app/services/api/org.service';
import { RoleType, TemplateType } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { AccountControlModel } from '../component';

export interface UserType {
  checked: boolean;
  disabled: boolean;
  username: string;
  roles: TemplateType[] | RoleType[];
}

@Component({
  selector: 'rc-batch-import-accounts-dialog',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class BatchImportAccountsDialogComponent implements OnInit {
  importLoading: boolean;
  users: UserType[] = [];
  selectedUsers: UserType[] = [];
  originSelectedUsers: AccountControlModel[] = [];
  initialized: boolean;
  searchKey: string;
  pagination = {
    count: 0,
    current: 1,
    size: 10,
    hiddenSize: true,
  };
  roleTypes: TemplateType[] | RoleType[];
  roleType: TemplateType | RoleType;
  isRoleType: boolean;
  selectedLdapGroup: LdapGroup = { name: '', uuid: '' };
  ldapGroups: LdapGroup[] = [];

  constructor(
    private orgService: OrgService,
    private errorsToast: ErrorsToastService,
    private translate: TranslateService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      roleTypes: TemplateType[] | RoleType[];
      isRoleType?: boolean;
      originSelectedUsers: AccountControlModel[];
    },
    private dialogRef: DialogRef<any>,
  ) {
    this.roleTypes = this.dialogData.roleTypes;
    this.roleType = this.roleTypes[0];
    this.originSelectedUsers = this.dialogData.originSelectedUsers;
    this.isRoleType = this.dialogData.isRoleType;
  }

  ngOnInit() {
    this.fetchLdapGroups();
    this.fetchUserlist();
  }

  get importEnabled() {
    return this.roleType && this.selectedUsers.length > 0;
  }

  get selectedNum() {
    return this.selectedUsers.length > 0
      ? `(${this.selectedUsers.length})`
      : '';
  }

  select() {
    this.pagination.current = 1;
    this.fetchUserlist();
  }

  search(v: string) {
    this.searchKey = v;
    this.pagination.current = 1;
    this.fetchUserlist();
  }

  pageChanged(num: number) {
    this.pagination.current = num;
    this.fetchUserlist();
  }

  onCheck(v: boolean, user: UserType) {
    v
      ? this.selectedUsers.push(user)
      : remove(
          this.selectedUsers,
          (v: UserType) => v.username === user.username,
        );
  }

  async fetchLdapGroups() {
    try {
      this.ldapGroups = await this.orgService.getLdapGroups();
      this.ldapGroups.unshift({
        name: this.translate.get('all_groups'),
        uuid: '',
      });
      this.selectedLdapGroup = this.ldapGroups[0];
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  async fetchUserlist() {
    const params = {
      search: this.searchKey,
      page: this.pagination.current,
      page_size: this.pagination.size,
      ldap_group_name: this.selectedLdapGroup.uuid
        ? this.selectedLdapGroup.name
        : null,
    };
    try {
      const { count, results } = (await this.orgService.listOrgAccounts(
        params,
      )) as {
        results: UserType[];
        count: number;
      };
      this.pagination.count = count;
      this.users = results.map(user => {
        user.checked = this.checkUserIsChecked(user);
        user.disabled = this.checkUserIsDisabled(user);
        return user;
      });
      this.initialized = true;
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  changeRoleType() {
    this.users.forEach(user => {
      user.checked = this.checkUserIsChecked(user);
      user.disabled = this.checkUserIsDisabled(user);
    });
  }

  checkUserIsChecked(user: UserType) {
    return (
      this.selectedUsers.filter(s => s.username === user.username).length > 0 ||
      this.originSelectedUsers.filter(s => {
        return (
          s.username === user.username &&
          this.roleType[this.roleTypeNameKey] === s.role[this.roleTypeNameKey]
        );
      }).length > 0
    );
  }

  checkUserIsDisabled(user: UserType) {
    return (
      this.originSelectedUsers.filter(s => {
        return (
          s.username === user.username &&
          this.roleType[this.roleTypeNameKey] === s.role[this.roleTypeNameKey]
        );
      }).length > 0
    );
  }

  cancel() {
    this.dialogRef.close();
  }

  import() {
    this.dialogRef.close({
      users: this.selectedUsers,
      roleType: this.roleType,
    });
  }

  get roleTypeNameKey() {
    return this.isRoleType ? 'role_name' : 'name';
  }

  getRoleDisplayName(item: RoleType | TemplateType) {
    return (
      item['display_name'] || item['template_display_name'] || item['role_name']
    );
  }
}
