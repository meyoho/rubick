import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { AbstractControl, FormArray, Validators } from '@angular/forms';

import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { Project } from 'app/services/api/project.service';
import { RBACService } from 'app/services/api/rbac.service';
import { TemplateType } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { SPACE_PATTERN } from 'app/utils/patterns';
import {
  BatchImportAccountsDialogComponent,
  UserType,
} from './batch-import-accounts-dialog/component';

export interface AccountControlModel {
  username: string;
  role: TemplateType;
}

interface SpaceFormModel {
  name: string;
  display_name?: string;
  accounts?: Account[];
}

@Component({
  selector: 'rc-space-create-form',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SpaceCreateFormComponent extends BaseResourceFormGroupComponent<
  SpaceFormModel
> {
  _project: Project;
  spacePattern = SPACE_PATTERN;
  @Input()
  set project(v: Project) {
    this._project = v;
    this.fetchTemplateTypes();
  }
  get project() {
    return this._project;
  }

  roleTypes: TemplateType[] = [];

  constructor(
    injector: Injector,
    private rbacService: RBACService,
    private errorToast: ErrorsToastService,
    private dialogService: DialogService,
  ) {
    super(injector);
  }

  createForm() {
    return this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.maxLength(32),
        Validators.pattern(SPACE_PATTERN.pattern),
      ]),
      display_name: this.fb.control('', [Validators.maxLength(32)]),
      accounts: this.fb.array([]),
    });
  }

  getDefaultFormModel(): SpaceFormModel {
    return {
      name: '',
      display_name: '',
      accounts: [],
    };
  }

  get accountsControls(): AbstractControl[] {
    return this.accountsFormArray.controls;
  }

  get accountsFormArray(): FormArray {
    return this.form.get('accounts') as FormArray;
  }

  get namePrefix() {
    return get(this.project, 'name')
      ? `${this.project.name.replace(/[\._]/g, '-')}-`
      : '';
  }

  remove(index: number) {
    this.accountsFormArray.removeAt(index);
  }

  add() {
    const portsControl = this.form.get('accounts') as FormArray;
    portsControl.push(
      this.fb.group({
        username: [null, [Validators.required, Validators.maxLength(32)]],
        role: [null, [Validators.required]],
      }),
    );
  }

  addAccounts(users: UserType[], roleType: TemplateType) {
    const portsControl = this.form.get('accounts') as FormArray;
    users.forEach(user => {
      if (
        (portsControl.value as Array<AccountControlModel>).filter(v => {
          return v.username === user.username && v.role.name === roleType.name;
        }).length === 0
      ) {
        portsControl.push(
          this.fb.group({
            username: [user.username, { readonly: true }, Validators.required],
            readonly: [true],
            role: [roleType, [Validators.required]],
          }),
        );
      }
    });
  }

  batchAdd() {
    const portsControl = this.form.get('accounts') as FormArray;
    const dialogRef = this.dialogService.open(
      BatchImportAccountsDialogComponent,
      {
        data: {
          roleTypes: this.roleTypes,
          originSelectedUsers: portsControl.value as Array<AccountControlModel>,
        },
        size: DialogSize.Big,
      },
    );
    dialogRef
      .afterClosed()
      .subscribe((v: { users: UserType[]; roleType: TemplateType }) => {
        if (v) {
          this.addAccounts(v.users, v.roleType);
        }
      });
  }

  async fetchTemplateTypes() {
    try {
      if (!get(this.project, 'name')) {
        return;
      }
      this.roleTypes = await (this.rbacService.getTemplateList({
        level: 'space',
      }) as any);
    } catch (e) {
      this.errorToast.error(e);
    }
  }
}
