import { Component, Input, forwardRef } from '@angular/core';
import { NG_VALUE_ACCESSOR } from '@angular/forms';
import { StringMap } from 'app/typings';

import { keys } from '../permissions-table/constants';

interface KeyValuePair {
  key: string;
  value: string;
}

@Component({
  selector: 'rc-constraint-editor',
  templateUrl: './constraint-editor.component.html',
  styleUrls: ['./constraint-editor.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ConstraintEditorComponent),
      multi: true,
    },
  ],
})
export class ConstraintEditorComponent {
  @Input()
  set options(values: string[]) {
    if (values && values.length) {
      this.innerOptions = values.map(value => {
        return value.split(':')[1];
      });
    }
  }

  keys = keys;
  innerOptions: string[] = [];
  list: KeyValuePair[] = [];

  private innerConstraints: StringMap[] = [];
  private onModelChange: Function = () => {};

  writeValue(values: StringMap[]) {
    if (values && values.length) {
      const list: KeyValuePair[] = [];
      const constraints: StringMap[] = [];
      values.forEach((constraintMap: StringMap) => {
        const map = {};
        Object.keys(constraintMap).forEach(key => {
          map[key.split(':')[1]] = constraintMap[key];
        });
        list.push({ key: '', value: '' });
        constraints.push(map);
      });
      this.list = list;
      this.innerConstraints = constraints;
    } else {
      this.list = [{ key: '', value: '' }];
      this.innerConstraints = [{}];
    }
  }

  registerOnChange(fn: Function): void {
    this.onModelChange = fn;
  }

  registerOnTouched(): void {}

  add(index: number, pair: KeyValuePair) {
    if (!pair.key || !pair.value) {
      return;
    }
    this.innerConstraints[index][pair.key] = pair.value;
    pair.key = '';
    pair.value = '';
    this.onModelChange(this.getResult());
  }

  getResult() {
    return this.innerConstraints.map(constraintMap => {
      const map = {};
      Object.keys(constraintMap).forEach(key => {
        map['res:' + key] = constraintMap[key];
      });
      return map;
    });
  }

  insert(constraint?: StringMap) {
    this.list.push({ key: '', value: '' });
    if (constraint) {
      this.innerConstraints.push(constraint);
    } else {
      this.innerConstraints.push({});
    }
  }

  remove(index: number, key: string) {
    delete this.innerConstraints[index][key];
    this.onModelChange(this.getResult());
  }
}
