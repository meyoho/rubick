import { DIALOG_DATA, DialogRef, DialogService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { cloneDeep } from 'lodash-es';

import { TranslateService } from 'app/translate/translate.service';
import {
  RolePermissionItem,
  RoleSchema,
  toIdentifier,
} from 'app2/features/rbac/backend-api';
import {
  createPermissionBySchema,
  getAction,
  onResourceChange,
} from '../permissions-table/constants';

@Component({
  selector: 'rc-permissions-editor',
  templateUrl: './permissions-editor.component.html',
  styleUrls: ['./permissions-editor.component.scss'],
})
export class PermissionsEditorComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;
  @Output()
  update = new EventEmitter<RolePermissionItem[]>();
  schema: RoleSchema;
  toIdentifier = toIdentifier;
  permissions: RolePermissionItem[];
  constraintEditable: boolean;

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      schema: RoleSchema;
      permissions: RolePermissionItem[];
      constraintEditable: boolean;
    },
    private dialogRef: DialogRef<any>,
    private dialogService: DialogService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    const permissions = cloneDeep(this.dialogData.permissions);
    this.schema = this.dialogData.schema;
    this.constraintEditable = !!this.dialogData.constraintEditable;
    this.permissions = permissions.map(permission => {
      if (permission.actions.includes(`${this.schema.resource_type}:*`)) {
        permission = Object.assign(permission, {
          actions: this.schema.actions,
        });
      }
      return permission;
    });
  }

  onResourceChange = onResourceChange;

  remove(index: number) {
    this.permissions.splice(index, 1);
  }

  add() {
    this.permissions.push(createPermissionBySchema(this.schema));
  }

  getActionTranslation(action: string) {
    return this.translateService.get(getAction(action));
  }

  save() {
    this.update.emit(this.permissions);
    this.dialogRef.close();
  }

  async cancel() {
    if (this.form.dirty) {
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('cancel'),
          content: this.translateService.get('rbac_give_up'),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });

        this.dialogService.closeAll();
      } catch (rejection) {}
    } else {
      this.dialogService.closeAll();
    }
  }
}
