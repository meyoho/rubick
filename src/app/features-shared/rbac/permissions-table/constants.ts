import { clone, remove } from 'lodash-es';

import {
  isPlatformResource,
  RolePermissionItem,
  RoleSchema,
  toIdentifier,
} from 'app2/features/rbac/backend-api';
import { PLATFORM_GROUPS_MAP } from 'app2/features/rbac/rbac-constant';

export interface K8sPermissionsMap {
  [apiGroup: string]: RoleSchema[];
}

// 平台资源的表的group与type是两个虚拟的概念，与k8s的api_group不对等
// 只是为了在UI上对应平台资源表的层级
export interface PlatformPermissionsMap {
  [group: string]: {
    [type: string]: RoleSchema[];
  };
}

export interface PlatformRoleType {
  group: string;
  type: string;
}

export interface PlatformRoleTypeMap {
  [resourc_type: string]: PlatformRoleType;
}

export interface K8sApigroup {
  [api_group: string]: string;
}

export interface PermissionsMap {
  [identifier: string]: RolePermissionItem[];
}

export enum TableColumns {
  Type = 'type',
  ResourceType = 'resource_type',
  Actions = 'rbac_actions',
  Constraints = 'constraints',
  ResourceNames = 'resource_name',
}

// 平台资源表头
export const platformHeaders = [
  TableColumns.Type,
  TableColumns.ResourceType,
  TableColumns.Actions,
  TableColumns.Constraints,
  TableColumns.ResourceNames,
];

// k8s资源表头
export const k8sHeaders = [
  TableColumns.ResourceType,
  TableColumns.Actions,
  TableColumns.Constraints,
  TableColumns.ResourceNames,
];

// 表格组件的权限
// Create与CreateTpl的差异在于是否可以编辑“约束条件”字段
export enum TableType {
  View, // 查看
  Edit, // 编辑
  Create, // 创建
  CreateTpl, // 创建模板
}

export const findPlatformGroupType = function(
  permission: RoleSchema | RolePermissionItem,
): PlatformRoleType {
  return RESOURCE_TYPE_MAP[permission.resource_type];
};

export const findK8sApiGroup = function(
  permission: RoleSchema | RolePermissionItem,
): string {
  return permission.api_group;
};

// FIXME:后端端返回的schema里面的条目的“resource_type”（如“build_config”）属性可能不存在于RESOURCE_TYPE_MAP（PLATFORM_GROUPS_MAP）里

// 平台资源的resourc_type具有唯一性
export const RESOURCE_TYPE_MAP: PlatformRoleTypeMap = (function() {
  const map: PlatformRoleTypeMap = {};
  Object.entries(PLATFORM_GROUPS_MAP).forEach(([group, types]) => {
    Object.keys(types).forEach((type: string) => {
      types[type].forEach((resource_type: string) => {
        map[resource_type] = { group, type };
      });
    });
  });
  return map;
})();

export const parseSchema = function parseSchema(
  schemas: RoleSchema[],
  permissions: RolePermissionItem[] = [],
  reserved: boolean = true,
): {
  k8sPermissionsMap: K8sPermissionsMap;
  platformPermissionsMap: PlatformPermissionsMap;
} {
  const k8sPermissionsMap = {};
  const platformPermissionsMap = {};
  schemas = clone(schemas);
  permissions = clone(permissions);
  schemas.forEach(schema => {
    if (isPlatformResource(schema)) {
      if (findPlatformGroupType(schema)) {
        const { group, type } = findPlatformGroupType(schema);
        const index = permissions.findIndex(
          permission => toIdentifier(permission) === toIdentifier(schema),
        );
        if (!reserved && index < 0) {
          return;
        }
        if (!platformPermissionsMap[group]) {
          platformPermissionsMap[group] = {};
        }
        if (!platformPermissionsMap[group][type]) {
          platformPermissionsMap[group][type] = [];
        }
        platformPermissionsMap[group][type].push(clone(schema));
      }
    } else {
      const api_group = findK8sApiGroup(schema);
      if (api_group) {
        const index = permissions.findIndex(
          permission => toIdentifier(permission) === toIdentifier(schema),
        );
        if (!reserved && index < 0) {
          return;
        }
        if (!k8sPermissionsMap[api_group]) {
          k8sPermissionsMap[api_group] = [];
        }
        k8sPermissionsMap[api_group].push(clone(schema));
      }
    }
  });

  return {
    k8sPermissionsMap,
    platformPermissionsMap,
  };
};

export const removePlatformSchema = function removeK8sSchema(
  permission: RolePermissionItem,
  permissionsMap: PlatformPermissionsMap,
) {
  const resource_type = permission.resource_type;
  const { group, type } = findPlatformGroupType(permission);
  remove(permissionsMap[group][type], permission => {
    return permission.resource_type === resource_type;
  });
};

export const removeK8sSchema = function removeSchema(
  permission: RolePermissionItem,
  permissionsMap: K8sPermissionsMap,
) {
  const { api_group, resource_type } = permission;
  remove(permissionsMap[api_group], permission => {
    return permission.resource_type === resource_type;
  });
};

export const createPermissionBySchema = function createPermissionBySchema(
  schema: RoleSchema,
): RolePermissionItem {
  const obj: RolePermissionItem = {};
  if (!isPlatformResource(schema)) {
    obj.api_group = schema.api_group;
  }

  return Object.assign(
    {
      resource_class: schema.resource_class,
      resource_type: schema.resource_type,
      resource: ['*'],
      actions: schema.actions,
      constraints: [],
    },
    obj,
  );
};

export const createPermissionByPermission = function createPermissionByPermission(
  permission: RolePermissionItem,
): RolePermissionItem {
  const obj: RolePermissionItem = {};
  if (!isPlatformResource(permission)) {
    obj.api_group = permission.api_group;
  }
  return Object.assign(
    {
      uuid: permission.uuid,
      resource_class: permission.resource_class,
      resource_type: permission.resource_type,
      resource: permission.resource || [],
      actions: permission.actions || [],
      constraints: permission.constraints || [],
    },
    obj,
  );
};

export const getAction = function getActionTranslation(action: string) {
  return action.split(':')[1];
};

export const onResourceChange = function onResourceChange(resource: string[]) {
  if (resource.length > 1 && resource.includes('*')) {
    resource.shift();
  } else if (resource.length < 1) {
    resource.unshift('*');
  }
};

export const keys = function(object: object) {
  return Object.keys(object);
};
