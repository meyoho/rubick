import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { cloneDeep, flatten } from 'lodash-es';
import { BehaviorSubject, combineLatest, from, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { PermissionsEditorComponent } from 'app/features-shared/rbac/permissions-editor/permissions-editor.component';
import { RBACService } from 'app/services/api/rbac.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  isPlatformResource,
  K8sTranslation,
  LinkageMap,
  RolePermissionItem,
  RoleSchema,
  toIdentifier,
  toRoleResource,
} from 'app2/features/rbac/backend-api';
import { PLATFORM_GROUPS } from 'app2/features/rbac/rbac-constant';
import {
  createPermissionByPermission,
  createPermissionBySchema,
  getAction,
  k8sHeaders,
  K8sPermissionsMap,
  keys,
  parseSchema,
  PermissionsMap,
  platformHeaders,
  PlatformPermissionsMap,
  removeK8sSchema,
  removePlatformSchema,
  TableColumns,
  TableType,
} from './constants';
@Component({
  selector: 'rc-permissions-table',
  templateUrl: './permissions-table.component.html',
  styleUrls: ['./permissions-table.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PermissionsTableComponent),
      multi: true,
    },
  ],
})
export class PermissionsTableComponent
  implements OnInit, ControlValueAccessor, OnDestroy {
  // 保留所有schema
  @Input()
  keepAll: boolean;
  // 平台级的accordion是否自动展开
  @Input()
  expanded: boolean;
  @Input()
  set type(type: TableType) {
    this.tableType = type;
  }

  tableType: TableType;
  TableType = TableType;
  PLATFORM_GROUPS = PLATFORM_GROUPS;
  keys = keys;
  TableColumns = TableColumns;
  platformHeaders = platformHeaders;
  k8sHeaders = k8sHeaders;
  toIdentifier = toIdentifier;
  schemas: RoleSchema[];
  k8sPermissionsMap: K8sPermissionsMap;
  platformPermissionsMap: PlatformPermissionsMap;
  checkedMap: { [identifier: string]: boolean } = {};
  permissionsMap: PermissionsMap = {};
  permissions$: BehaviorSubject<RolePermissionItem[]> = new BehaviorSubject([]);
  translations: K8sTranslation = {};
  // 平台权限与K8S权限联动关系map
  linkageMap: LinkageMap = null;
  outsideAccordionExpanded = false;
  k8sAccordionsExpanded: { [apiGroup: string]: boolean } = {};
  // 已经链接的平台权限与k8s映射关系map
  linkedMap: {
    [identifier: string]: string[];
  } = {};

  private onDestroy$ = new Subject<void>();
  private get permissions(): RolePermissionItem[] {
    return flatten(Object.values(this.permissionsMap));
  }

  constructor(
    private cdr: ChangeDetectorRef,
    private rbacService: RBACService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private messageService: MessageService,
  ) {
    combineLatest(from(this.rbacService.getRoleSchema()), this.permissions$)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(([schemas, permissions]) => {
        const { k8sPermissionsMap, platformPermissionsMap } = parseSchema(
          schemas,
          permissions,
          this.keepAll,
        );
        this.k8sPermissionsMap = k8sPermissionsMap;
        this.platformPermissionsMap = platformPermissionsMap;
        // 将permissons推入permissionsMap
        this.permissionsMap = {};
        permissions.forEach(permission => {
          const identifier = toIdentifier(permission);
          this.checkedMap[identifier] = true;
          if (!this.permissionsMap[identifier]) {
            this.permissionsMap[identifier] = [
              createPermissionByPermission(permission),
            ];
          } else {
            this.permissionsMap[identifier].push(
              createPermissionByPermission(permission),
            );
          }
        });
      });
  }

  async ngOnInit() {
    this.linkageMap = await this.rbacService.getLinkageMap();
    this.translations = await this.rbacService.getK8sSchemaTranslations();
  }

  get lang() {
    return this.translateService.currentLang;
  }

  getResourceTypeTranslation(isPlatform: boolean, resource_type: string) {
    if (isPlatform) {
      return this.translateService.get(resource_type);
    } else {
      const translationMap = this.translations[resource_type] || {};
      for (const key in translationMap) {
        if (this.lang.includes(key)) {
          return translationMap[key];
        }
      }
      return resource_type;
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  private onModelChange: Function = () => {};

  async writeValue(values: RolePermissionItem[]) {
    this.permissions$.next(values || []);
  }

  registerOnChange(fn: Function): void {
    this.onModelChange = fn;
  }

  registerOnTouched(): void {}

  async clear(schema: RoleSchema) {
    delete this.checkedMap[toIdentifier(schema)];
    delete this.permissionsMap[toIdentifier(schema)];
    this.cdr.detectChanges();
    this.onModelChange(this.permissions);
  }

  async remove(permission: RolePermissionItem) {
    if (this.permissions.length === 1) {
      this.messageService.warning({
        content: this.translateService.get('rbac_at_least_one_permission'),
      });
      return;
    }

    try {
      await this.dialogService.confirm({
        title: this.translateService.get('remove'),
        content: this.translateService.get('rbac_confirm_remove_permission'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      if (isPlatformResource(permission)) {
        removePlatformSchema(permission, this.platformPermissionsMap);
      } else {
        removeK8sSchema(permission, this.k8sPermissionsMap);
      }
      delete this.permissionsMap[toIdentifier(permission)];
      this.onModelChange(this.permissions);
    } catch (rejection) {}
  }

  async onCheck(schema: RoleSchema) {
    const identifier = toIdentifier(schema);
    if (!this.checkedMap[identifier]) {
      this.checkedMap[identifier] = true;
      if (!this.permissionsMap[identifier]) {
        this.permissionsMap[identifier] = [createPermissionBySchema(schema)];
        this.onModelChange(this.permissions);
      }
      this.resolveLinkage(schema);
    } else {
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('remove'),
          content: this.translateService.get('rbac_confirm_clear_permission'),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });
        this.clear(schema);
        this.checkedMap[identifier] = false;
        this.onModelChange(this.permissions);
        this.resolveLinkage(schema);
      } catch (rejection) {}
    }
  }

  // 解决平台权限与k8s权限的联动
  // linkageMap是平台权限与k8s的映射关系字典，通过这个字典可以确定与平台权限联动的k8s权限
  // 平台权限与k8s权限是多对多的关系，但是联动是单向的，只能从平台权限到k8s权限，可能产生联动的操作包括：正选、反选、编辑
  // 联动后，对应的k8s权限数据会发生变化，并且在linkedMap中按照索引顺序保存平台权限的resource_type（平台权限的resource_type互斥）
  resolveLinkage(schema: RoleSchema, permissions?: RolePermissionItem[]) {
    if (
      isPlatformResource(schema) &&
      this.linkageMap &&
      this.linkageMap.resource[schema.resource_type]
    ) {
      this.linkageMap.resource[schema.resource_type].forEach(
        ({ api_group, resource_type }) => {
          if (!this.k8sPermissionsMap[api_group]) {
            return;
          }
          const k8sSchema = this.k8sPermissionsMap[api_group].find(
            item => item.resource_type === resource_type,
          );
          if (!k8sSchema) {
            return;
          }
          if (permissions) {
            // 平台权限编辑
            this.updateK8sPermissions(schema, k8sSchema, permissions);
          } else {
            // 平台权限正选、反选
            const identifier = toIdentifier(k8sSchema);
            if (this.checkedMap[toIdentifier(schema)]) {
              this.checkedMap[identifier] = true;
              if (!this.permissionsMap[identifier]) {
                this.permissionsMap[identifier] = [];
              }
              if (!this.linkedMap[identifier]) {
                this.linkedMap[identifier] = [];
              }
              this.permissionsMap[identifier].push(
                createPermissionBySchema(k8sSchema),
              );
              this.linkedMap[identifier].push(schema.resource_type);
            } else {
              this.checkedMap[identifier] = false;
              this.clearPermissionByLinkage(schema, k8sSchema);
            }
          }
        },
      );
      this.setK8sAccordionsExpanded();
      this.onModelChange(this.permissions);
    }
  }

  // 通过联动关系更新k8s权限
  updateK8sPermissions(
    platformSchema: RoleSchema,
    k8sSchema: RoleSchema,
    permissions: RolePermissionItem[],
  ) {
    const identifier = toIdentifier(k8sSchema);
    const linkageActionMap = this.linkageMap.action;
    this.clearPermissionByLinkage(platformSchema, k8sSchema);
    if (!this.permissionsMap[identifier]) {
      this.permissionsMap[identifier] = [];
    }
    // 将平台权限中的部分数据合并到k8s权限中
    this.permissionsMap[identifier] = [
      ...this.permissionsMap[identifier],
      ...permissions.reduce((accu, { actions, constraints, resource }) => {
        const tempActions: string[] = [];
        actions.forEach(action => {
          action = action.split(':')[1];
          if (linkageActionMap[action]) {
            linkageActionMap[action].forEach(k8sAction => {
              k8sAction = `${k8sSchema.resource_type}:${k8sAction}`;
              if (k8sSchema.actions.indexOf(k8sAction) > -1) {
                tempActions.push(k8sAction);
              }
            });
          }
        });
        // 如果平台权限已选择的操作与联动的k8s权限有对应的操作，则在k8s权限中推入这条权限
        if (tempActions.length) {
          accu.push(
            Object.assign(cloneDeep(k8sSchema), {
              actions: tempActions,
              constraints,
              resource,
            }),
          );
          this.linkedMap[identifier] = (
            this.linkedMap[identifier] || []
          ).concat([platformSchema.resource_type]);
        }
        return accu;
      }, []),
    ];

    if (!this.permissionsMap[identifier].length) {
      delete this.checkedMap[identifier];
      delete this.permissionsMap[identifier];
    }
  }

  // 通过联动关系清除k8s权限
  clearPermissionByLinkage(platformSchema: RoleSchema, k8sSchema: RoleSchema) {
    const identifier = toIdentifier(k8sSchema);
    if (this.linkedMap[identifier]) {
      this.linkedMap[identifier].forEach((item, index, arr) => {
        if (item === platformSchema.resource_type) {
          arr.splice(index, 1);
          this.permissionsMap[identifier].splice(index, 1);
        }
      });
    }
  }

  setK8sAccordionsExpanded() {
    this.outsideAccordionExpanded = this.permissions.some(
      permission => !isPlatformResource(permission),
    );
    this.k8sAccordionsExpanded = {};
    Object.keys(this.checkedMap).forEach(identifier => {
      const resource = toRoleResource(identifier);
      if (isPlatformResource(resource)) {
        return;
      }
      this.k8sAccordionsExpanded[resource.api_group] = this.permissions.some(
        permission => permission.api_group === resource.api_group,
      );
    });
  }

  openEditor(schema: RoleSchema) {
    const identifier = toIdentifier(schema);
    const dialogRef = this.dialogService.open(PermissionsEditorComponent, {
      fitViewport: true,
      size: DialogSize.Medium,
      data: {
        schema,
        permissions: this.permissionsMap[identifier],
        constraintEditable: this.tableType !== TableType.CreateTpl,
      },
    });
    dialogRef.componentInstance.update.subscribe(
      (res: RolePermissionItem[]) => {
        this.checkedMap[identifier] = true;
        this.permissionsMap[identifier] = res;
        // 触发联动
        this.resolveLinkage(schema, res);
        this.onModelChange(this.permissions);
      },
    );
  }

  getActionsTranslation(actions: string[], schema: RoleSchema) {
    if (
      actions.includes(`${schema.resource_type}:*`) ||
      actions.length === schema.actions.length
    ) {
      return this.translateService.get('rbac_all');
    } else {
      return actions
        .map(action => {
          return this.translateService.get(getAction(action));
        })
        .join(', ');
    }
  }

  getConstraintTranslation(key: string, value: string): string {
    return `${this.translateService.get(key.split(':')[1])}:${value}`;
  }

  getResourceNames(resource: string[]): string {
    if (resource && resource.includes('*')) {
      return this.translateService.get('rbac_any');
    } else {
      return resource.join(', ');
    }
  }
}
