import { NgModule } from '@angular/core';
import { ConstraintEditorComponent } from 'app/features-shared/rbac/constraint-editor/constraint-editor.component';
import { PermissionsEditorComponent } from 'app/features-shared/rbac/permissions-editor/permissions-editor.component';
import { PermissionsTableComponent } from 'app/features-shared/rbac/permissions-table/permissions-table.component';
import { SharedModule } from 'app/shared/shared.module';

const EXPORT_COMPONENTS = [
  ConstraintEditorComponent,
  PermissionsEditorComponent,
  PermissionsTableComponent,
];

const ENTRY_COMPONENTS = [
  ConstraintEditorComponent,
  PermissionsEditorComponent,
  PermissionsTableComponent,
];

@NgModule({
  imports: [SharedModule],
  exports: [...EXPORT_COMPONENTS],
  declarations: [...EXPORT_COMPONENTS],
  providers: [],
  entryComponents: [...ENTRY_COMPONENTS],
})
export class RbacSharedModule {}
