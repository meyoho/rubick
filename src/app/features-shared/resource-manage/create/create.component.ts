import { ConfirmType, DialogService, NotificationService } from '@alauda/ui';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import {
  RSRCManagementService,
  ResourceType,
} from 'app/services/api/resource-manage.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, KubernetesResource } from 'app/typings';
import { createActions, updateActions } from 'app/utils/code-editor-config';
import { safeDump, safeLoad, safeLoadAll } from 'js-yaml';
import { filter } from 'rxjs/operators';

@Component({
  selector: 'rc-resource-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss'],
})
export class ResourceCreateComponent implements OnInit, OnDestroy {
  edtiorActions = createActions;
  editorOptions = {
    language: 'yaml',
    readOnly: false,
    renderLineHighlight: 'none',
  };
  yamlInputValue = '';
  originalYaml = '';
  resourceData: K8sResourceWithActions<KubernetesResource>;
  update: boolean;
  resourceType: ResourceType;
  updatePayload: K8sResourceWithActions<KubernetesResource>['kubernetes'];
  createPayload: any[];

  @ViewChild('form')
  form: NgForm;

  constructor(
    private router: Router,
    private dialogService: DialogService,
    private translate: TranslateService,
    private RSRCService: RSRCManagementService,
    private auiNotificationService: NotificationService,
    private route: ActivatedRoute,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnInit(): void {
    this.RSRCService.currentResouceData$
      .pipe(filter(data => !!data))
      .subscribe(data => this.onViewChange(data));
    this.RSRCService.currentResourceType$
      .pipe(filter(type => !!type))
      .subscribe(type => (this.resourceType = type));
  }

  ngOnDestroy(): void {
    this.RSRCService.setCurrentResourceData(null);
  }

  private onViewChange(data: K8sResourceWithActions<KubernetesResource>) {
    this.update = true;
    this.resourceData = data;
    this.yamlInputValue = safeDump(data.kubernetes, { lineWidth: 9999 });
    this.originalYaml = this.yamlInputValue;
    this.edtiorActions = Object.assign({}, updateActions);
  }

  private replaceNamespace(rawData: any) {
    if (rawData.items) {
      rawData.items = rawData.items.map((item: any) => {
        item.metadata.namespace = this.RSRCService.namespaceName;
        return item;
      });
    } else {
      rawData.metadata.namespace = this.RSRCService.namespaceName;
    }
    return rawData;
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    try {
      if (!this.update) {
        const loadedYaml = safeLoadAll(this.yamlInputValue).filter(i => !!i);
        this.createPayload = this.RSRCService.isUserView()
          ? loadedYaml.map((item: any) => this.replaceNamespace(item))
          : loadedYaml;
      } else {
        this.updatePayload = this.RSRCService.isUserView()
          ? this.replaceNamespace(safeLoad(this.yamlInputValue))
          : safeLoad(this.yamlInputValue);
      }
    } catch (error) {
      this.auiNotificationService.error(this.translate.get('yaml_format_hint'));
      return;
    }

    this.dialogService
      .confirm({
        title: this.translate.get(
          this.update
            ? 'confirm_update_resource_content'
            : 'confirm_create_resource_content',
        ),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: this.update
          ? this.updateResource()
          : this.createResource(),
      })
      .then(() => {
        this.router.navigate(['../list'], {
          relativeTo: this.route,
        });
      })
      .catch(() => null);
  }

  cancel() {
    if (this.yamlInputValue) {
      this.dialogService
        .confirm({
          title: this.translate.get(
            this.update
              ? 'cancel_resource_update_content'
              : 'cancel_resource_create_content',
          ),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
          confirmType: ConfirmType.Warning,
        })
        .then(() => {
          this.router.navigate(['../list'], {
            relativeTo: this.route,
          });
        })
        .catch(() => null);
    } else {
      this.router.navigate(['../list'], {
        relativeTo: this.route,
      });
    }
  }

  private createResource() {
    return (resolve: () => void, reject: () => void) => {
      this.RSRCService.createResource({
        data: this.createPayload,
      })
        .then(() => resolve())
        .catch(err => {
          reject();
          if (err && err.status === 403) {
            this.auiNotificationService.error({
              title: this.translate.get('action_permission_denied'),
            });
          } else {
            this.errorsToastService.error(err);
          }
        });
    };
  }

  private updateResource() {
    return (resolve: () => void, reject: () => void) => {
      this.RSRCService.updateResource({
        resourceTypeName: this.resourceType.name,
        resourceName: this.resourceData.kubernetes.metadata.name,
        data: this.updatePayload,
        namespace: this.resourceData.kubernetes.metadata.namespace || '',
      })
        .then(() => resolve())
        .catch(err => {
          reject();
          this.errorsToastService.error(err);
        });
    };
  }
}
