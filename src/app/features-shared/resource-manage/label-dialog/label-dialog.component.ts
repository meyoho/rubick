import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { get } from 'lodash-es';
import { filter } from 'rxjs/operators';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import {
  ResourceType,
  RSRCManagementService,
} from 'app/services/api/resource-manage.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { KubernetesResource, StringMap } from 'app/typings';

@Component({
  selector: 'rc-resource-label-dialog',
  templateUrl: './label-dialog.component.html',
  styleUrls: ['./label-dialog.component.scss'],
})
export class ResourceLabelDialogComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;
  loading: boolean;
  resourceType: ResourceType;

  labels: StringMap;
  nullLabels: StringMap;

  constructor(
    private dialogRef: DialogRef,
    private rsrcManagementService: RSRCManagementService,
    private notificationService: NotificationService,
    private translate: TranslateService,
    private errorsToastService: ErrorsToastService,
    @Inject(DIALOG_DATA) private data: KubernetesResource,
    private k8sResourceService: K8sResourceService,
  ) {}

  ngOnInit(): void {
    this.rsrcManagementService.currentResourceType$
      .pipe(filter(type => !!type))
      .subscribe(type => (this.resourceType = type));
    this.labels = get(this.data, 'metadata.labels', {});
    this.nullLabels = Object.keys(this.labels).reduce((acc, cur) => {
      acc[cur] = null;
      return acc;
    }, {});
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    try {
      this.loading = true;
      await this.k8sResourceService.updateResourcerLabelsOrAnnotations({
        cluster: this.rsrcManagementService.clusterName,
        namespace: this.data.metadata.namespace || '',
        actionType: 'label',
        resourceType: this.resourceType.name,
        name: this.data.metadata.name,
        payload: {
          ...this.nullLabels,
          ...this.labels,
        },
      });
      this.notificationService.success(this.translate.get('update_success'));
      this.loading = false;
      this.dialogRef.close(true);
    } catch (err) {
      this.loading = false;
      this.errorsToastService.error(err);
    }
  }
}
