import {
  ConfirmType,
  DialogService,
  DialogSize,
  NotificationService,
} from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  Optional,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { safeDump } from 'js-yaml';
import { find, get, includes, omitBy } from 'lodash-es';
import { BehaviorSubject, from, Observable, Subject, Subscription } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import { BaseResourceListComponent } from 'app/abstract';
import {
  Namespace,
  NamespaceService,
} from 'app/services/api/namespace.service';
import { RegionService } from 'app/services/api/region.service';
import {
  Category,
  ResourceManagementListFetchParams,
  ResourceType,
  RSRCManagementService,
} from 'app/services/api/resource-manage.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import {
  Environments,
  K8sResourceWithActions,
  KubernetesResource,
} from 'app/typings';
import { viewActions } from 'app/utils/code-editor-config';
import { PageParams, ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { ResourceLabelDialogComponent } from '../label-dialog/label-dialog.component';

export interface ColumnDef {
  name: string;
  label?: string;
  sortable?: boolean;
  sortStart?: 'asc' | 'desc';
}

const ALL_COLUMN_DEFS: ColumnDef[] = [
  {
    name: 'name',
    label: 'name',
  },
  {
    name: 'label',
    label: 'label',
  },
  {
    name: 'creationTimestamp',
    label: 'created_at',
  },
  {
    name: 'namespace',
    label: 'namespace',
  },
  {
    name: 'action',
    label: 'action',
  },
];

@Component({
  selector: 'rc-resource-management-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceListComponent
  extends BaseResourceListComponent<K8sResourceWithActions<KubernetesResource>>
  implements OnInit, AfterViewInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  fetchParams$: Observable<ResourceManagementListFetchParams>;
  regionContext$: Subject<boolean> = new BehaviorSubject<boolean>(false);
  columnDefs: ColumnDef[];
  columns$: Observable<string[]>;
  initialized = false;
  listSubscription: Subscription;
  regionName: string;
  keyword: string;

  expandedMap = {
    namespaced: false,
    region: false,
  };

  activePath = '';
  yamlInputValue = '';

  namespacedKinds: string[] = [];
  regionKinds: string[] = [];

  category: Category = {
    namespaced: [],
    region: [],
  };

  edtiorActions = viewActions;
  editorOptions = {
    language: 'yaml',
    readOnly: true,
    renderLineHighlight: 'none',
  };

  currentResourceType: ResourceType;

  userView: boolean;

  selectedNamespace: string;
  namespaceOptions: string[] = [];
  namespaceLoading: boolean;

  /* implements methods of base class */
  map(value: ResourceList) {
    return value.results;
  }

  fetchResources(
    params: ResourceManagementListFetchParams,
  ): Observable<ResourceList> {
    return from(this.RSRCService.getResourceList(params));
  }

  get navZeroState() {
    return !this.category.namespaced.length && !this.category.region.length;
  }

  get navLoading() {
    return this.RSRCService.navLoading;
  }

  isFilterMode = true;
  filterName: string;

  get filteredCategory() {
    return this.isFilterMode && this.filterName
      ? {
          namespaced: this.category.namespaced.filter(item =>
            item.kind.toLowerCase().includes(this.filterName.toLowerCase()),
          ),
          region: this.category.region.filter(item =>
            item.kind.toLowerCase().includes(this.filterName.toLowerCase()),
          ),
        }
      : this.category;
  }

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    http: HttpClient,
    router: Router,
    activatedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    public RSRCService: RSRCManagementService,
    private roleUtil: RoleUtilitiesService,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private errorsToastService: ErrorsToastService,
    private namespaceServcie: NamespaceService,
    @Optional() private workspaceComponent: WorkspaceComponent,
  ) {
    super(http, router, activatedRoute, cdr);
  }

  ngOnInit(): void {
    let params$: Observable<Params>;
    if (this.RSRCService.isUserView()) {
      this.userView = true;
      params$ = this.workspaceComponent.baseParams;
    } else {
      params$ = this.regionService.region$.pipe(
        filter(cluster => !!cluster),
        distinctUntilChanged((prev, current) => prev.name === current.name),
        map(cluster => ({
          cluster: cluster.name,
          namespace: this.RSRCService.namespaceName || 'default',
        })),
      );
    }

    params$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(params => !!params),
      )
      .subscribe(async params => {
        this.initialized = false;
        this.RSRCService.intialized = false;
        if (
          !(await this.roleUtil.resourceTypeSupportPermissions(
            'k8s_others',
            {},
            'list',
          ))
        ) {
          return;
        }
        if (!this.userView) {
          this.selectedNamespace = this.RSRCService.namespaceName || 'default';
          this.initNamespaceOptions(params.cluster);
        }
        this.RSRCService.initScope(params);
        await this.RSRCService.initResourceTypeList$();
        this.cdr.markForCheck();
      });

    this.RSRCService.currentResourceType$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((type: ResourceType) => {
        this.currentResourceType = type;
      });

    this.RSRCService.category$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((category: Category) => {
        this.category = category;
        this.initNav();
        this.initColumns$();
        this.refetch();
      });

    super.ngOnInit();

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe((_list: K8sResourceWithActions<KubernetesResource>[]) => {
        this.initialized = true;
      });

    this.regionContext$
      .pipe(distinctUntilChanged())
      .subscribe(regionContext => {
        if (regionContext) {
          this.RSRCService.namespaceName = '';
        } else if (this.selectedNamespace) {
          this.RSRCService.namespaceName = this.selectedNamespace;
        }
      });
  }

  initNav() {
    if (!this.currentResourceType.kind) {
      this.expandedMap['namespaced'] = true;
      this.activePath = 'namespaced';
    } else {
      const isNamespacedKind = !!find(this.category.namespaced, [
        'kind',
        this.currentResourceType.kind,
      ]);
      this.expandedMap = {
        namespaced: isNamespacedKind,
        region: !isNamespacedKind,
      };
      this.activePath = isNamespacedKind ? 'namespaced' : 'region';
      this.regionContext$.next(isNamespacedKind ? false : true);
    }
  }

  initColumns$() {
    this.columnDefs = this.getColumnDefs();
    const allColumns = this.columnDefs.map(colDef => colDef.name);
    const columnsWithoutNamespace = allColumns.filter(
      column => column !== 'namespace',
    );
    this.columns$ = this.regionContext$.pipe(
      map(regionContext =>
        regionContext || this.RSRCService.isUserView()
          ? columnsWithoutNamespace
          : allColumns,
      ),
    );
  }

  private initNamespaceOptions(cluster: string) {
    this.namespaceLoading = true;
    this.namespaceServcie
      .getNamespaces(cluster)
      .then((res: Namespace[]) => {
        this.namespaceOptions = res
          .map(({ kubernetes: { metadata } }) => metadata.name)
          .sort();
      })
      .catch(_ => [])
      .finally(() => {
        this.namespaceLoading = false;
      });
  }

  getColumnDefs(): ColumnDef[] {
    return ALL_COLUMN_DEFS;
  }

  ngAfterViewInit() {}

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  /**
   * Init fetching params to be used when fetching list result.
   */
  initFetchParams$() {
    this.fetchParams$ = this.RSRCService.currentResourceType$.pipe(
      filter(type => !!type.name),
      switchMap(currentResourceType => {
        return this.getDefaultFetchParams$().pipe(
          map(fetchParams => ({
            ...fetchParams,
            currentResourceType,
          })),
        );
      }),
    );
    return this.fetchParams$;
  }

  getLabelValue(resource: any) {
    return omitBy(get(resource, 'kubernetes.metadata.labels', {}), (_v, k) =>
      includes(k, '.' + this.env.label_base_domain),
    );
  }

  viewResource(resource: any, templateRef: TemplateRef<any>) {
    this.yamlInputValue = safeDump(resource.kubernetes, {
      lineWidth: 9999,
      sortKeys: true,
    });
    this.dialogService.open(templateRef, {
      size: DialogSize.Large,
      data: {
        name: resource.kubernetes.metadata.name,
      },
    });
  }

  /**
   * Actions
   */
  updateAction(item: K8sResourceWithActions<KubernetesResource>) {
    this.RSRCService.setCurrentResourceData(item);
    this.router.navigate(['../update'], {
      relativeTo: this.activedRoute,
    });
  }

  updateLabelAction(item: { kubernetes: any; resource_actions: any }) {
    const dialogRef = this.dialogService.open(ResourceLabelDialogComponent, {
      data: item.kubernetes,
    });
    dialogRef.afterClosed().subscribe(result => !!result && this.refetch());
  }

  deleteAction(item: K8sResourceWithActions<KubernetesResource>) {
    this.dialogService
      .confirm({
        title: this.translateService.get('confirm_delete_resource_content', {
          kind: this.currentResourceType.kind,
          name: item.kubernetes.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
        confirmType: ConfirmType.Danger,
        beforeConfirm: (resolve, reject) => {
          this.RSRCService.deleteResouce({
            resourceTypeName: this.currentResourceType.name,
            resourceName: item.kubernetes.metadata.name,
            namespace: item.kubernetes.metadata.namespace || '',
          })
            .then(() => {
              resolve();
              this.auiNotificationService.success(
                this.translateService.get('delete_success'),
              );
            })
            .catch(err => {
              reject();
              this.errorsToastService.error(err);
            });
        },
      })
      .then(() => {
        this.refetch();
      })
      .catch(() => null);
  }

  refetch() {
    this.onUpdate(null);
    this.cdr.markForCheck();
  }

  pageChanged(page: PageParams) {
    this.onPageEvent({
      pageIndex: ++page.pageIndex,
      pageSize: page.pageSize,
    });
  }

  /* Nav list */
  handleClick(path: string) {
    this.expandedMap[path] = !this.expandedMap[path];
    this.activePath = path;
  }

  onKindsClick(resourceType: ResourceType) {
    this.initialized = false;
    this.regionContext$.next(!resourceType.namespaced);
    this.RSRCService.setCurrentResourceType(resourceType);
  }

  isPathActive(path: string) {
    return this.activePath.startsWith(path);
  }

  isPathExpanded(path: string) {
    return this.expandedMap[path];
  }

  navSearch(queryString: string) {
    if (this.isFilterMode) {
      this.filterName = queryString.trim();
      setTimeout(() => {
        this.expandedMap['namespaced'] = true;
        this.expandedMap['region'] = true;
        this.cdr.markForCheck();
      }, 100);
    }
  }

  onChangeHandler(queryString: string) {
    this.navSearch(queryString);
  }

  trackByFn(index: number, item: any) {
    return get(item, 'kubernetes.metadata.uid', index);
  }

  onNamespaceChange(namespaceName: string) {
    this.initialized = false;
    this.RSRCService.namespaceName =
      namespaceName === '$all' ? '' : namespaceName;
    this.refetch();
  }
}
