import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';

import { ResourceCreateComponent } from './create/create.component';
import { ResourceLabelDialogComponent } from './label-dialog/label-dialog.component';
import { ResourceListComponent } from './list/list.component';

const EXPORTABLES = [
  ResourceListComponent,
  ResourceCreateComponent,
  ResourceLabelDialogComponent,
];

@NgModule({
  imports: [SharedModule, RouterModule, FormTableModule],
  declarations: EXPORTABLES,
  exports: EXPORTABLES,
  entryComponents: [ResourceLabelDialogComponent],
})
export class ResourceManageSharedModule {}
