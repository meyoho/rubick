import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Directive,
  Inject,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  NG_VALIDATORS,
  NgForm,
  ValidationErrors,
  Validator,
} from '@angular/forms';

import { get } from 'lodash-es';
import { from, of } from 'rxjs';
import {
  catchError,
  map,
  publishReplay,
  refCount,
  tap,
} from 'rxjs/internal/operators';

import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { ResourceData } from 'app/services/api/tool-chain/tool-chain-api.types';
import { getToolResourceDisplayName } from 'app/services/api/tool-chain/utils';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';

@Component({
  selector: 'rc-create-resource-dialog',
  templateUrl: 'create-resource-dialog.component.html',
  styleUrls: ['create-resource-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolChainCreateResourceDialogComponent implements OnInit {
  addMode = 'select';
  hideOptions = false;
  resourceCreatePermission: boolean;
  selectedResources: any[];
  loading = true;

  @ViewChild(NgForm)
  form: NgForm;

  resourceName: string;
  constructor(
    @Inject(DIALOG_DATA) public data: ResourceData,
    private toolChainApi: ToolChainApiService,
    private roleUtil: RoleUtilitiesService,
    private dialogRef: DialogRef<ToolChainCreateResourceDialogComponent>,
    private translate: TranslateService,
    private message: MessageService,
    private errorsToastService: ErrorsToastService,
    private cdr: ChangeDetectorRef,
  ) {
    this.resourceName = '';
    this.addMode = this.data.options.mode;
    this.hideOptions = this.data.options.hideOptions;
  }

  resources$ = from(
    this.data.namespace === 'global-credentials'
      ? []
      : this.toolChainApi.getResources({
          name: this.data.toolName,
          kind: this.data.kind,
          secret: {
            name: this.data.secretname,
            namespace: this.data.namespace,
          },
        }),
  ).pipe(
    tap(() => {
      this.loading = false;
    }),
    catchError((e: any) => {
      return of(e);
    }),
    map((data: any) => {
      return (data.items || [])
        .map(toModel)
        .filter((resource: { name: string; __origin: any }) => {
          return this.data.resourceNameList.indexOf(resource.name) === -1;
        });
    }),
    publishReplay(1),
    refCount(),
  );

  async ngOnInit() {
    [
      this.resourceCreatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_tool_projects',
      {},
      ['create'],
    );
    this.cdr.markForCheck();
  }
  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (this.addMode === 'input') {
      const payload = {
        resources: this.data,
        resourceName: this.resourceName,
      };
      this.toolChainApi
        .createResource(payload)
        .then((res: any) => {
          const resourceName = this.getToolResourceDisplayName(this.data.type);
          this.message.success({
            content: this.translate.get('tool_chain_resource_create_succeed', {
              resource: resourceName,
            }),
          });
          this.dialogRef.close({ mode: 'input', data: res });
        })
        .catch(e => {
          this.errorsToastService.error(e);
        });
    } else {
      // select mode
      this.dialogRef.close({ mode: 'select', data: this.selectedResources });
    }
  }

  getHintTips(name: string) {
    const realNameHint = this.translate.get('tool_chain_real_project_name', {
      resource: this.getToolResourceDisplayName(this.data.type),
    });
    if (name === this.data.tenantName) {
      return `${realNameHint}${this.data.tenantName}`;
    }
    return `${realNameHint}${this.data.tenantName}-${name}`;
  }

  getToolResourceDisplayName(tool: string) {
    return this.translate.get(getToolResourceDisplayName(tool));
  }

  trackFn(item: any) {
    return item.name;
  }

  resourceOptionEnabled(mode: string) {
    const selectOnlyResources = ['Github', 'Bitbucket'];

    return !(
      (mode === 'select' && this.data.namespace === 'global-credentials') ||
      (mode === 'input' && selectOnlyResources.includes(this.data.type))
    );
  }
}

@Directive({
  selector: 'input[rcProjectNameValidate]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: ProjectNameValidatorDirective,
      multi: true,
    },
  ],
})
export class ProjectNameValidatorDirective implements Validator, OnChanges {
  @Input()
  resourceNameList: string[];
  @Input()
  tenantName: string;
  @Input()
  resourceType: string;

  private _onChange: () => void;

  constructor(private translate: TranslateService) {}

  validate(control: AbstractControl): ValidationErrors | null {
    if (!control.value) {
      return null;
    }
    if (
      this.resourceNameList.some(
        i => i['name'] === `${this.tenantName}-${control.value}`,
      )
    ) {
      return {
        projectName: this.translate.get('tool_chain_resource_exist', {
          resource: this.translate.get(
            getToolResourceDisplayName(this.resourceType),
          ),
        }),
      };
    } else if (control.value.match(K8S_RESOURCE_NAME_BASE.pattern)) {
      return this.resourceType === 'Jira' && control.value.match('^[0-9]*$')
        ? {
            projectName: this.translate.get(
              'tool_chain_jira_resource_name_invalid',
            ),
          }
        : null;
    } else {
      return {
        projectName: this.translate.get('tool_chain_pattern'),
      };
    }
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.resourceNameList && this._onChange) {
      this._onChange();
    }
  }

  registerOnValidatorChange(fn: () => void): void {
    this._onChange = fn;
  }
}

function toModel(item: any) {
  return {
    name: get(item, 'metadata.name'),
    type: get(item, 'metadata.annotations.type', ''),
    __origin: item,
  };
}
