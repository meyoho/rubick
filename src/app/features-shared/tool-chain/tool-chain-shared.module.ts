import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { CredentialSharedModule } from 'app/features-shared/credential';
import { SharedModule } from 'app/shared/shared.module';
import { HarborProjectComponent } from 'app2/features/tool-chain/harbor-project/harbor-project.component';
import { IntegrateToolComponent } from 'app2/features/tool-chain/integrate-tool/integrate-tool.component';
import { SecretValidatingDialogComponent } from 'app2/features/tool-chain/secret-validating-dialog/secret-validating-dialog.component';
import { ServiceListComponent } from 'app2/features/tool-chain/service-list/service-list.component';
import { ToolListComponent } from 'app2/features/tool-chain/tool-list/tool-list.component';
import { ToolTypeBarComponent } from 'app2/features/tool-chain/tool-type-bar/tool-type-bar.component';
import {
  ProjectNameValidatorDirective,
  ToolChainCreateResourceDialogComponent,
} from './create-resource-dialog/create-resource-dialog.component';

const components = [
  ServiceListComponent,
  ToolTypeBarComponent,
  IntegrateToolComponent,
  ToolListComponent,
  ToolChainCreateResourceDialogComponent,
  ProjectNameValidatorDirective,
  HarborProjectComponent,
  SecretValidatingDialogComponent,
];

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    FormsModule,
    CredentialSharedModule,
  ],
  declarations: components,
  exports: components,
  providers: [],
  entryComponents: [
    IntegrateToolComponent,
    ToolChainCreateResourceDialogComponent,
    SecretValidatingDialogComponent,
  ],
})
export class ToolChainSharedModule {}
