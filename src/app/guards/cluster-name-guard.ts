import { Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  CanActivate,
  RouterStateSnapshot,
} from '@angular/router';

import { RegionService } from 'app/services/api/region.service';
import { RouterUtilService } from 'app/services/router-util.service';
import { LayoutComponent } from 'app2/layout/layout.component';

@Injectable()
export class ClusterNameGuard implements CanActivate {
  constructor(
    private routerUtilService: RouterUtilService,
    private regionService: RegionService,
  ) {}

  async canActivate(
    _route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot,
  ) {
    const clusterInSession = window.sessionStorage.getItem('cluster') || '';
    const clusterInUrl = this.getClusterParamInRouterState(state);
    if (!clusterInUrl) {
      if (clusterInSession) {
        return this.routerUtilService.createUrlTreeWithClusterParam(
          clusterInSession,
          state.url,
        );
      } else {
        try {
          const clusters = await this.regionService.getRegions();
          if (clusters && clusters.length) {
            const cluster = clusters[0];
            this.regionService.setRegionByName(cluster.name);
            return this.routerUtilService.createUrlTreeWithClusterParam(
              cluster.name,
              state.url,
            );
          }
        } catch (_e) {}
      }
    } else {
      this.regionService.setRegionByName(clusterInUrl);
    }
    return true;
  }

  private getClusterParamInRouterState(state: RouterStateSnapshot) {
    let clusterName;
    let leafActivatedRouteSnapshot: ActivatedRouteSnapshot =
      state.root.firstChild;
    while (leafActivatedRouteSnapshot) {
      const cluster = leafActivatedRouteSnapshot.params.cluster;
      if (cluster && leafActivatedRouteSnapshot.component === LayoutComponent) {
        clusterName = cluster;
        break;
      } else {
        leafActivatedRouteSnapshot = leafActivatedRouteSnapshot.firstChild;
      }
    }
    return clusterName;
  }
}
