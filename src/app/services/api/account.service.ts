import { Token } from '@angular/compiler';
import { Inject, Injectable } from '@angular/core';
import { Router } from '@angular/router';

import { ReplaySubject } from 'rxjs';

import { ErrorsToastService } from 'app/services/errors-toast.service';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import { Weblabs } from 'app/typings';
import { isAdminView, isUserView } from 'app/utils/page';

export interface RcAccount {
  namespace: string;
  username: string;
  roles?: AccountType[];
  type?: string;
  sub_type?: string;
  is_valid?: boolean;
}

export interface AccountType {
  role_uuid: string;
  role_name: string;
  template_display_name: string;
  template_name?: string;
  template_uuid: string;
}

export interface RcProfile {
  account_type: number;
  company: string;
  email: string;
  logo_file: string;
  mobile: string;
  username: string;
  is_admin: boolean;
}

export interface Token {
  name: string;
  value: string;
}

export interface Tokens {
  platform_token?: string;
  kubernetes_id_token?: string;
  openshift_token?: string;
}

export enum ACCOUNT_TYPE {
  LDAP_ACCOUNT = 'organizations.LDAPAccount',
  ORG_ACCOUNT = 'organizations.Account',
  // 三方 登录进来的账户
  TPA_ACCOUNT = 'organizations.TPAccount',
}

export interface CodeClient {
  is_authed: boolean | string;
  name: string;
}

const account_transparent_placeholder =
  'data:image/svg+xml;base64,PHN2ZyB4bWxucz0iaHR0cDovL3d3dy53My5vcmcvMjAwMC9zdmciIHhtbG5zOnhsaW5rPSJodHRwOi8vd3d3LnczLm9yZy8xOTk5L3hsaW5rIiB3aWR0aD0iODQiIGhlaWdodD0iODQiIHZpZXdCb3g9IjAgMCA4NCA4NCI+CiAgPGRlZnM+CiAgICA8cmVjdCBpZD0iY29tcGFueS1hIiB3aWR0aD0iODQiIGhlaWdodD0iODQiIHJ4PSIyLjcxNyIvPgogICAgPHJlY3QgaWQ9ImNvbXBhbnktYyIgd2lkdGg9IjYwIiBoZWlnaHQ9IjYwIi8+CiAgPC9kZWZzPgogIDxnIGZpbGw9Im5vbmUiPgogICAgPG1hc2sgaWQ9ImNvbXBhbnktYiIgZmlsbD0iI2ZmZiI+CiAgICAgIDx1c2UgeGxpbms6aHJlZj0iI2NvbXBhbnktYSIvPgogICAgPC9tYXNrPgogICAgPHVzZSBmaWxsPSIjQ0NDIiB4bGluazpocmVmPSIjY29tcGFueS1hIi8+CiAgICA8ZyBtYXNrPSJ1cmwoI2NvbXBhbnktYikiPgogICAgICA8ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxMiAxMikiPgogICAgICAgIDxtYXNrIGlkPSJjb21wYW55LWQiIGZpbGw9IiNmZmYiPgogICAgICAgICAgPHVzZSB4bGluazpocmVmPSIjY29tcGFueS1jIi8+CiAgICAgICAgPC9tYXNrPgogICAgICAgIDxnIG1hc2s9InVybCgjY29tcGFueS1kKSI+CiAgICAgICAgICA8ZyB0cmFuc2Zvcm09InRyYW5zbGF0ZSgxIDIpIj4KICAgICAgICAgICAgPHJlY3Qgd2lkdGg9IjE2IiBoZWlnaHQ9IjEwIiB4PSIyMSIgeT0iNSIgZmlsbD0iI0YwRjBGMCIgc3Ryb2tlPSIjOTc5Nzk3IiBzdHJva2Utd2lkdGg9IjIiIHJ4PSIyIi8+CiAgICAgICAgICAgIDxyZWN0IHdpZHRoPSIzMiIgaGVpZ2h0PSIyMCIgeD0iMTMiIHk9IjE0IiBmaWxsPSIjRDZENkQ2IiBzdHJva2U9IiM5Nzk3OTciIHN0cm9rZS13aWR0aD0iMiIgcng9IjIiLz4KICAgICAgICAgICAgPHBhdGggZmlsbD0iI0Y1RjVGNiIgc3Ryb2tlPSIjOTc5Nzk3IiBzdHJva2Utd2lkdGg9IjIiIGQ9Ik0yOS45Mzc3NSwyMS40ODkxNTQxIEw1MS4zNzk4MzM2LDMyLjg3MTUzOSBDNTIuMjk2Mzg4NiwzMy4zNTgwODYgNTIuNjg4NTg3OCwzNC40NjQ4MDQ5IDUyLjI4MjkyODYsMzUuNDE5OTE4MyBMNDMuNTQyMDgzMiw1NiBDNDEuNTA3NTAyLDU1Ljg3NzI4NzggMzkuNDAwNzgzOCw1NS40NDM0MTI1IDM3LjIyMTkyODgsNTQuNjk4Mzc0MiBDMzMuOTUzNjQ2Miw1My41ODA4MTY4IDMxLjY2NDIwMTcsNTIuOTQ5NzQwOSAyOSw1Mi45NDk3NDA5IEMyNi4xODgyNjg5LDUyLjk0OTc0MDkgMjQuMDEzODMxNSw1My4yMDUzOTMzIDIxLjA3ODg5NzQsNTQuMTY5ODQ5MiBDMTkuNzc2MDAwNyw1NC41OTc5OTczIDE3LjcxNTEzODksNTUuMDk4MjMxNSAxNC44OTYzMTIsNTUuNjcwNTUxNiBMNS43NzExNTM0NCwzNS40NDExNDk3IEM1LjMzNzQ3MzA4LDM0LjQ3OTczMTQgNS43MjQ5MjIyMSwzMy4zNDY3NzMzIDYuNjU2NTA2MiwzMi44NTIyNDgzIEwyOC4wNjIyNSwyMS40ODkxNTQxIEMyOC42NDg2NjUzLDIxLjE3Nzg1OTUgMjkuMzUxMzM0NywyMS4xNzc4NTk1IDI5LjkzNzc1LDIxLjQ4OTE1NDEgWiIvPgogICAgICAgICAgICA8cGF0aCBzdHJva2U9IiM5Nzk3OTciIHN0cm9rZS1saW5lY2FwPSJzcXVhcmUiIHN0cm9rZS13aWR0aD0iMiIgZD0iTTI4LjUsMjEuNDcwNTg4MiBMMjguNSw1Mi41ODY0MDA1Ii8+CiAgICAgICAgICAgIDxwYXRoIHN0cm9rZT0iIzk5OSIgc3Ryb2tlLWxpbmVjYXA9InJvdW5kIiBzdHJva2UtbGluZWpvaW49InJvdW5kIiBzdHJva2Utd2lkdGg9IjIiIGQ9Ik0wLDUwIEM1LjQ0MzM3ODIzLDU0LjE2NjU1MTMgOS44Njg3OTA0OSw1Ni4xMDc5Mjk5IDEzLjI3NjIzNjgsNTUuODI0MTM1OSBDMTguMzg3NDA2Miw1NS4zOTg0NDQ4IDIyLjc2OTQwNTgsNTIuOTEyMDY3OSAyOC42MjkwNTI5LDUyLjkxMjA2NzkgQzM0LjQ4ODY5OTksNTIuOTEyMDY3OSA0MC4yNjQxNzU0LDU2Ljg2OTY0MjggNDcuMDU1MDQ1Nyw1NS44MjQxMzU5IEM1MS41ODIyOTI1LDU1LjEyNzEzMTMgNTUuMjMwNjEwNiw1My4xODU3NTI2IDU4LDUwIi8+CiAgICAgICAgICAgIDxwYXRoIGZpbGw9IiNBOEE4QTgiIGQ9Ik00OS42OTU5NDQgMi41NzUzODUzOEM0OS4zMzY0MjU0IDIuNzU5NTExODUgNDguOTk0NDIzOCAyLjk3MzU1NzkgNDguNjczMTQwMyAzLjIxNDI4NTcxIDQ3LjY5NTM5OCAyLjEyNTM0MzUzIDQ2LjI4NDQwNzMgMS40NDE2MDg3IDQ0LjcxNTUzNDQgMS40NDE2MDg3IDQzLjc2NDMyOTMgMS40NDE2MDg3IDQyLjg3MTE2MTQgMS42OTI5NDgwNCA0Mi4wOTc0MDI2IDIuMTMzNTUwMjIgNDIuODQwNjczNC44NTY3OTAzMzkgNDQuMjEzNjQ5OCA0LjU0NzQ3MzUxZS0xMyA0NS43ODQ0NjkgNC41NDc0NzM1MWUtMTMgNDcuMDMwMzUxOSA0LjU0NzQ3MzUxZS0xMyA0OC4xNTE3NzcuNTM4OTg0OTQyIDQ4LjkzMzI2ODIgMS4zOTkwMDUwNiA0OS44NTgwMzM2Ljc0NDQ0NDc3NiA1MC45ODM3MTUyLjM2MDQwMjE3NiA1Mi4xOTgwNzY3LjM2MDQwMjE3NiA1NC4yODA3NDk0LjM2MDQwMjE3NiA1Ni4xMDI1ODQgMS40OTAwMDQyNiA1Ny4wOTc0MDI2IDMuMTc2NTMwNjYgNTUuOTIxOTk4MyAyLjMxMTk3NTMgNTQuNDc1MjI3NCAxLjgwMjAxMDg4IDUyLjkxMDY5OTggMS44MDIwMTA4OCA1MS43NTQwNjY2IDEuODAyMDEwODggNTAuNjYxNzkyNSAyLjA4MDcyODcgNDkuNjk1OTQ0IDIuNTc1Mzg1Mzh6TTQxLjk5NzgzODkgOC4yMTA0MzI2NEM0MS42ODYyNTU1IDguNTIxMTI4NTggNDEuNDA2Nzk2NyA4Ljg2NDAyMzgyIDQxLjE2NDg3NzEgOS4yMzM3MDM3OSA0MC4wMDA5NTY1IDcuODYyODEyMjcgMzguMjY0OTA1MSA2Ljk5Mjg5MTU1IDM2LjMyNTY3MjggNi45OTI4OTE1NSAzNC4wOTM4Njc2IDYuOTkyODkxNTUgMzIuMTMxMTc2IDguMTQ1MTA0MDggMzEgOS44ODcxMjcyMSAzMS44ODk2OTc5IDcuNDU3NjMyMTUgMzQuMjIyNDc2MyA1LjcyMzgyMDk5IDM2Ljk2MDIwODEgNS43MjM4MjA5OSAzOC4zNjU2NjQ1IDUuNzIzODIwOTkgMzkuNjY0Mzk4IDYuMTgwNzU2MzggNDAuNzE1OTU1OCA2Ljk1NDE3NDQ2IDQxLjg2NDczODQgNS44MDIxNDM5OSA0My40NTM3MDQgNS4wODkyODU3MSA0NS4yMDkxNjY3IDUuMDg5Mjg1NzEgNDcuMjg1MDE1OCA1LjA4OTI4NTcxIDQ5LjEyODA0NzkgNi4wODYwOTM4NCA1MC4yODU3MTQzIDcuNjI3MTYxNDUgNDkuMjI1MTU2NiA2LjgzMDQ1OTI0IDQ3LjkwNjgyOTcgNi4zNTgzNTYyNyA0Ni40NzgyMzcyIDYuMzU4MzU2MjcgNDQuNzI5MjkxNyA2LjM1ODM1NjI3IDQzLjE0NTYwOTEgNy4wNjU5MzE0NyA0MS45OTc4Mzg5IDguMjEwNDMyNjR6Ii8+CiAgICAgICAgICA8L2c+CiAgICAgICAgPC9nPgogICAgICA8L2c+CiAgICA8L2c+CiAgPC9nPgo8L3N2Zz4K';

@Injectable()
export class AccountService {
  avatarPath$$: ReplaySubject<string> = new ReplaySubject(1);
  CODE_CLIENTS_URL = `/ajax/private-build-code-clients/${
    this.account.namespace
  }/`;

  constructor(
    private errorToast: ErrorsToastService,
    private httpService: HttpService,
    private router: Router,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) private weblabs: Weblabs,
  ) {
    //Default transparent placeholder
    this.avatarPath$$.next(account_transparent_placeholder);
  }

  getUserToken() {
    return this.httpService
      .request('/ajax-sp/account/token', {
        method: 'GET',
        cache: true,
      })
      .then(({ token }) => token);
  }

  getTokens(useV3: boolean = false): Promise<Tokens> {
    const K8S_AUTH_ENABLED = this.weblabs.K8S_AUTH_ENABLED;
    const URL =
      K8S_AUTH_ENABLED || useV3
        ? '/ajax/v3/api-token'
        : '/ajax-sp/account/token';
    return this.httpService
      .request(URL, {
        method: 'GET',
        cache: false,
      })
      .then(result => {
        return K8S_AUTH_ENABLED || useV3
          ? result
          : { platform_token: result['token'] };
      });
  }

  getAmpIdTokens(): Promise<Tokens> {
    return this.httpService.request('/ajax/v3/api-token', {
      method: 'GET',
      cache: false,
    });
  }

  getAuthProfile(cache: boolean = true): Promise<RcProfile> {
    return this.httpService.request<RcProfile>('/ajax/auth/profile', {
      method: 'get',
      cache,
      addNamespace: true,
    });
  }

  updateNamespaceLogo(namespace: string, file: File) {
    const body = new FormData();
    body.append('logo_file', file);
    return this.httpService.request(`/ajax/users/${namespace}/logo`, {
      method: 'POST',
      body,
    });
  }

  updateSubAccountPassword(data: { username: string }) {
    return this.httpService.request(
      `/ajax/orgs/${this.account.namespace}/accounts/${data.username}`,
      {
        method: 'PUT',
        body: data,
      },
    );
  }

  isSubAccount(): boolean {
    return !!this.account.username;
  }

  isUserView() {
    return isUserView();
  }

  isAdminView() {
    return isAdminView();
  }

  updateProfile(profile: RcProfile) {
    return this.httpService.request({
      url: '/ajax/auth/profile',
      method: 'PUT',
      body: profile,
    });
  }

  resetAccountPassword(data: RcAccount) {
    return this.httpService.request(
      `/ajax/orgs/${this.account.namespace}/accounts/${
        data.username
      }/reset_password`,
      {
        method: 'POST',
      },
    );
  }

  logout() {
    return this.httpService
      .request('/ajax/ap/logout')
      .then(({ url }) => {
        const redirectUrl = url || '/landing';
        if (redirectUrl.startsWith('http')) {
          return (location.href = redirectUrl);
        }
        this.router.navigateByUrl(redirectUrl);
      })
      .catch(e => {
        this.errorToast.error(e);
      });
  }

  getPrivateBuildCodeClients(): Promise<CodeClient[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.CODE_CLIENTS_URL,
      })
      .then(({ result }) => result);
  }

  unlinkPrivateBuildCodeClient({ codeClientName }: { codeClientName: string }) {
    return this.httpService.request({
      method: 'DELETE',
      url: this.CODE_CLIENTS_URL + codeClientName,
    });
  }

  /**
   * @param {Object} params
   * eg: oschina
   *  {
   *    code_client_name: 'OSCHINA',
   *    oschina_user_email: 'example@example.com',
   *    oschina_user_pwd: 'pwd',
   *    state: 'testorg001'
   *  }
   *
   *  eg: github
   *  {
   *    code_client_name: 'GITHUB',
   *    code: '7206d225d8a7adc7c7ad',
   *    state: 'testorg001',
   *  }
   *
   *  eg: bitbucket
   *  {
   *    code_client_name: 'BITBUCKET',
   *    code : 'eQ9VsD6yGsHZZ83p9k',
   *    state: "testorg001",
   *    redirect_uri: "https://console-staging.alauda.cn/console/auth-redirect/BITBUCKET"
   *  }
   */
  linkPrivateBuildCodeClient({
    codeClientName,
    code,
    oschinaUserEmail,
    oschinaUserPwd,
  }: {
    codeClientName: string;
    code?: string;
    oschinaUserEmail?: string;
    oschinaUserPwd?: string;
  }) {
    const redirectUri = this.generateOauthRedirectUri({
      codeClientName,
    });

    return this.httpService.request({
      method: 'POST',
      url: this.CODE_CLIENTS_URL,
      body: {
        code_client_name: codeClientName,
        oschina_user_email: oschinaUserEmail,
        oschina_user_pwd: oschinaUserPwd,
        code,
        state: this.account.namespace,
        redirect_uri: redirectUri,
      },
    });
  }

  getPrivateBuildCodeClientAuthUrl({
    codeClientName,
  }: {
    codeClientName: string;
  }): Promise<string> {
    return this.httpService
      .request({
        url: this.CODE_CLIENTS_URL + codeClientName + '/auth-url',
        method: 'POST',
      })
      .then(({ auth_url }) => {
        const redirectUri = this.generateOauthRedirectUri({
          codeClientName,
        });

        // Both GitHub / BitBucket follow Oauth2 RFC. We may need to think about for other cases in the future.
        auth_url += '&redirect_uri=' + encodeURIComponent(redirectUri);

        if (codeClientName === 'BITBUCKET') {
          auth_url += '&response_type=code';
        }

        return auth_url;
      });
  }

  private generateOauthRedirectUri({
    codeClientName,
  }: {
    codeClientName: string;
  }) {
    const origin = location.origin;

    // We need to configure /auth-redirect in github application management page
    return `${origin}/console/admin/auth-redirect/${codeClientName}`;
  }
}
