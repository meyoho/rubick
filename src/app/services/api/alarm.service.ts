import { Inject, Injectable } from '@angular/core';

import { LogService } from 'app/services/api/log.service';
import { MetricQueries } from 'app/services/api/metric.service';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account, StringMap } from 'app/typings';
import { ResourceList } from 'app_user/core/types';

export interface Alarm {
  compare?: string;
  description?: string;
  display_name?: string;
  group_name?: string;
  label?: {
    label_name: string;
  };
  namespace?: string;
  metric_name?: string;
  metric?: {
    queries?: MetricQueries[];
  };
  name?: string;
  notifications?: {
    name: string;
    uuid: string;
  }[];
  queries?: MetricQueries[];
  resource_name?: string;
  scale_up?: {
    name: string;
    namespace: string;
  };
  scale_down?: {
    name: string;
    namespace: string;
  };
  severity?: string;
  state?: string;
  summary?: string;
  threshold?: number;
  type?: string;
  wait?: number;
  labels?: {
    severity?: string;
  };
  annotations?: {};
  level?: string;
  unit?: string;
  expr?: string;
  query?: string;
  kind?: string;
  metric_tooltip?: string;
  created_at?: string;
  updated_at?: string;
}

export interface AlarmDetail extends Alarm {
  labelsParsed?: {
    key: string;
    value: {};
  }[];
  annotationsParsed?: {
    key: string;
    value: {};
  }[];
}

export interface AlarmUpdateParam {
  path: string;
  path_from: string;
  name?: string;
  pods?: Array<any>;
  clusters?: Array<string>;
  clusterName?: string;
  accountNamespace?: string;
  namespaceName?: string;
  resource_name?: string;
}

export interface AlarmTemplateRule {
  name: string;
  metric: {
    queries: [
      {
        aggregator: string;
        range: number;
        labels: [
          {
            type: string;
            name: string;
            value: string;
          }
        ];
      }
    ];
  };
  compare: string;
  threshold: number;
  unit: string;
  wait: number;
}

export interface AlarmTemplate {
  name: string;
  kind: string;
  description: string;
  template: AlarmTemplateRule[];
  resource_actions?: [];
}

export interface AlarmActions {
  action_type: string;
  name?: string;
  notifications?: {
    name: string;
    uuid: string;
  }[];
}

export interface TemplateApplyPayload {
  cluster: {
    name: string;
  };
  project?: string;
  metric: {
    queries: [
      {
        labels: {
          name: string;
          value: string;
        }[];
      }
    ];
  };
}

export interface AlarmMetric {
  metric?: {
    queries: Array<MetricQueries>;
  };
  unit?: string;
  metric_name?: string;
  resource_name?: string;
  expr?: string;
  type?: string;
  query?: string;
}

export interface AlarmMetricFormModel {
  alarm_type: 'default' | 'custom' | 'log';
  monitor_metric?: string;
  metric_expression?: string;
  data_type: boolean;
  timeStampOption?: number;
  aggregator?: string;
  unit?: string;
  resource_type?: string;
  resource_name?: string;
  log_content?: string[];
}

export interface AlarmInfoFormModel {
  name: string;
  level: string;
  compare: string;
  threshold: string;
  wait: number;
  labels: StringMap;
  annotations: StringMap;
}

@Injectable()
export class AlarmService {
  LOG_ALARM_V2_URL: string;
  ALERT_URL: string;
  ALERT_CONFIG_URL: string;
  ALERT_TEMPLATE_URL: string;

  constructor(
    private httpService: HttpService,
    private logService: LogService,
    @Inject(ACCOUNT) account: Account,
  ) {
    const namespace = account.namespace;
    this.LOG_ALARM_V2_URL = `/ajax/v2/log_alarms/${namespace}/`;
    this.ALERT_URL = '/ajax/v1/alerts/';
    this.ALERT_CONFIG_URL = '/ajax/v1/config/alerts';
    this.ALERT_TEMPLATE_URL = `/ajax/v1/alert_templates/${namespace}/`;
  }

  getAlertConfig(): Promise<string[]> {
    return this.httpService
      .request(this.ALERT_CONFIG_URL, {
        method: 'GET',
      })
      .then(({ system_label_keys }) => system_label_keys)
      .catch(() => [
        'name',
        'involved_object_name',
        'involved_object_kind',
        'involved_object_namespace',
        'creator',
        'project',
        'cluster',
        'indicator',
        'application',
        'severity',
      ]);
  }

  getPrometheusFunctions(): Promise<string[]> {
    return this.httpService
      .request(this.ALERT_CONFIG_URL, {
        method: 'GET',
      })
      .then(({ prometheus_functions }) => prometheus_functions)
      .catch(() => ['sum', 'rate', 'irate']);
  }

  getAlertsByLabelSelector(
    cluster_name: string,
    kind: string,
    name: string,
    namespace: string,
    params?: {
      page: number;
      page_size: number;
      name: string;
    },
  ): Promise<Alarm> {
    let labelSelector = '';
    switch (kind) {
      case 'deployment':
      case 'daemonset':
      case 'statefulset':
        labelSelector = `alert.alauda.io/namespace=${namespace},alert.alauda.io/kind=workload,alert.alauda.io/name=${name}`;
        break;
      case 'application':
        labelSelector = `alert.alauda.io/namespace=${namespace},alert.alauda.io/${kind}=${name}`;
        break;
      case 'cluster':
        labelSelector = `alert.alauda.io/kind in (cluster,node)`;
        break;
    }
    const endpoint = `${this.ALERT_URL}${cluster_name}/${namespace}`;
    return this.httpService.request(endpoint, {
      method: 'GET',
      params: {
        labelSelector,
        ...params,
      },
    });
  }

  deleteAlertsByLabelSelector(
    cluster_name: string,
    namespace: string,
    kind: string,
    name: string,
  ) {
    let labelSelector = `alert.alauda.io/${kind}=${name}`;
    switch (kind) {
      case 'workload':
        labelSelector = `alert.alauda.io/namespace=${namespace},alert.alauda.io/kind=${kind},alert.alauda.io/name=${name}`;
        break;
      case 'application':
        labelSelector = `alert.alauda.io/namespace=${namespace},alert.alauda.io/${kind}=${name}`;
        break;
    }
    const endpoint = `${this.ALERT_URL}${cluster_name}`;
    return this.httpService.request(endpoint, {
      method: 'DELETE',
      params: {
        labelSelector,
      },
    });
  }

  getAlert(
    cluster_name: string,
    namespace: string,
    resource_name: string,
    group_name: string,
    name: string,
  ): Promise<Alarm> {
    const endpoint = `${
      this.ALERT_URL
    }${cluster_name}/${namespace}/${resource_name}/${group_name ||
      'general'}/${name}`;
    return this.httpService.request(endpoint, {
      method: 'GET',
    });
  }

  createAlert(cluster_name: string, alert: Alarm) {
    return this.httpService.request(`${this.ALERT_URL}${cluster_name}`, {
      method: 'POST',
      body: alert,
    });
  }

  updateAlert(
    cluster_name: string,
    namespace: string,
    resource_name: string,
    group_name: string,
    alert_name: string,
    alert: Alarm,
  ) {
    return this.httpService.request(
      this.ALERT_URL +
        `${cluster_name}/${namespace}/${resource_name}/${group_name}/${alert_name}`,
      {
        method: 'PUT',
        body: alert,
      },
    );
  }

  deleteAlert(
    cluster_name: string,
    namespace: string,
    resource_name: string,
    group_name: string,
    alarm_name: string,
  ) {
    return this.httpService.request(
      this.ALERT_URL +
        `${cluster_name}/${namespace}/${resource_name}/${group_name}/${alarm_name}`,
      {
        method: 'DELETE',
      },
    );
  }

  getAlertTemplate(template_name: string): Promise<AlarmTemplate> {
    const endpoint = `${this.ALERT_TEMPLATE_URL}${template_name}`;
    return this.httpService.request(endpoint, {
      method: 'GET',
    });
  }

  getAlertTemplates(params?: {
    page: number;
    page_size: number;
    name?: string;
  }): Promise<ResourceList<AlarmTemplate>> {
    return this.httpService.request(this.ALERT_TEMPLATE_URL, {
      method: 'GET',
      params,
    });
  }

  createAlertTemplate(alarmTemplate: AlarmTemplate) {
    return this.httpService.request(this.ALERT_TEMPLATE_URL, {
      method: 'POST',
      body: alarmTemplate,
    });
  }

  updateAlertTemplate(template_name: string, alarmTemplate: AlarmTemplate) {
    const endpoint = `${this.ALERT_TEMPLATE_URL}${template_name}`;
    return this.httpService.request(endpoint, {
      method: 'PUT',
      body: alarmTemplate,
    });
  }

  deleteAlertTemplate(template_name: string) {
    const endpoint = `${this.ALERT_TEMPLATE_URL}${template_name}`;
    return this.httpService.request(endpoint, {
      method: 'DELETE',
    });
  }

  applyAlertTemplate(
    template_name: string,
    metricQueries: TemplateApplyPayload,
  ) {
    const endpoint = `${this.ALERT_TEMPLATE_URL}${template_name}/apply`;
    return this.httpService.request(endpoint, {
      method: 'POST',
      body: metricQueries,
    });
  }

  getLogAlarms({
    page = 1,
    page_size = 20,
    name = '',
    all = false,
    service_uuid = '',
  }): Promise<any> {
    return this.httpService
      .request(this.LOG_ALARM_V2_URL, {
        method: 'GET',
        params: {
          page,
          page_size,
          name,
          all,
          service_uuid,
        },
      })
      .then((res: any) => {
        res.result = res.result.map((result: any) => {
          if (result.saved_search_detail) {
            result.saved_search_detail.query_conditions = this.logService.queryConditionFilter(
              result.saved_search_detail.query_conditions,
            );
          }
          return result;
        });
        return res;
      });
  }

  getLogAlarmsByService(service_uuid: string): Promise<any> {
    return this.httpService
      .request(this.LOG_ALARM_V2_URL, {
        method: 'GET',
        params: {
          service_uuid,
        },
      })
      .then((res: any) => {
        return res.alarms || [];
      })
      .catch(() => []);
  }

  getLogAlarmDetail(uuid: string): Promise<any> {
    return this.httpService
      .request(this.LOG_ALARM_V2_URL + uuid, {
        method: 'GET',
      })
      .then((result: any) => {
        if (result.saved_search_detail) {
          result.saved_search_detail.query_conditions = this.logService.queryConditionFilter(
            result.saved_search_detail.query_conditions,
          );
        }
        return result;
      });
  }

  createLogAlarm(payload: any): Promise<any> {
    return this.httpService.request(this.LOG_ALARM_V2_URL, {
      method: 'POST',
      body: payload,
    });
  }

  updateLogAlarm(uuid: string, payload: any): Promise<any> {
    return this.httpService.request(this.LOG_ALARM_V2_URL + uuid, {
      method: 'PUT',
      body: payload,
    });
  }

  deleteLogAlarm(uuid: string): Promise<any> {
    return this.httpService.request(this.LOG_ALARM_V2_URL + uuid, {
      method: 'DELETE',
    });
  }
}
