import { NotificationService } from '@alauda/ui';
import { Injectable } from '@angular/core';

import { FetchDataResult } from 'app/abstract/pagination-data';
import { HttpService } from 'app/services/http.service';
import { LoggerUtilitiesService } from 'app/services/logger.service';
import { TranslateService } from 'app/translate/translate.service';

export enum RepositoryStatus {
  Fail = 'FAIL',
  Success = 'SUCCESS',
  InProgress = 'IN_PROGRESS',
}

export enum VcsType {
  Git = 'git',
  SVN = 'svn',
}

export enum RepositorySteps {
  DownloadCode = 'DOWNLOAD_CODE',
  ParseCode = 'PARSE_CODE',
  Complete = 'COMPLETE',
}

export interface AppCatalogTemplate {
  resource_actions: string[];
  uuid: string;
  is_active: boolean;
  name: string;
  display_name: string;
  description: string;
  icon: string;
  installed_app_num: number;
  versions?: [{ uuid: string; values_yaml_content: string; version: string }];
}

export interface AppCatalogTemplateRepository {
  uuid: string;
  name: string;
  display_name?: string;
  namespace: string;
  status: RepositoryStatus;
  step_at: RepositorySteps;
  url: string;
  type: VcsType;
  branch?: string; // Git only
  path: string;
  has_template?: boolean;
  resource_actions?: string[];
  username?: string;
  password?: string;
  template_num?: number;
  templates: AppCatalogTemplate[];
  _comment?: string; // Description in API.
}

export interface AppCatalogTemplateCreateResponse {
  catalog_info: {
    notes_txt_content: string;
    template: {
      display_name: string;
      name: string;
      uuid: string;
      version?: any;
    };
    values_yaml_content: string;
  };
  cluster: {
    name: string;
    uuid: string;
  };
  namespace: {
    name: string;
    uuid: string;
  };
  resource: {
    description?: string;
    name: string;
    source?: string;
    uuid: string;
  };
  service?: any;
}

const repository: AppCatalogTemplateRepository = {
  resource_actions: [],
  uuid: '',
  name: '',
  namespace: '',
  status: 'SUCCESS' as RepositoryStatus,
  step_at: 'COMPLETE' as RepositorySteps,
  url: '',
  type: 'git' as VcsType,
  branch: '',
  path: '',
  username: '',
  has_template: false,
  templates: [],
};

@Injectable()
export class AppCatalogService {
  constructor(
    private httpService: HttpService,
    private logger: LoggerUtilitiesService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
  ) {}

  getCatalogEndpointBase() {
    return `/ajax/v2/catalog`;
  }

  getCatalogApplicationsEndpoint() {
    return `${this.getCatalogEndpointBase()}/applications`;
  }

  getCatalogTemplatesEndpoint() {
    return `${this.getCatalogEndpointBase()}/templates`;
  }

  getCatalogTemplateEndpoint(template_id: string) {
    return `${this.getCatalogEndpointBase()}/templates/${template_id}`;
  }

  getCatalogTemplateRepositoriesEndpoint() {
    return `${this.getCatalogEndpointBase()}/template_repositories`;
  }

  getCatalogPublicTemplateRepositoriesEndpoint() {
    return `${this.getCatalogEndpointBase()}/template_public_repositories`;
  }

  getCatalogTemplateRepositoryEndpoint(repositoryId: string) {
    return `${this.getCatalogTemplateRepositoriesEndpoint()}/${repositoryId}`;
  }

  getCatalogTemplateRepositoryRefreshEndpoint(repositoryId: string) {
    return `${this.getCatalogTemplateRepositoryEndpoint(repositoryId)}/refresh`;
  }

  getCatalogTemplateRepositoryCancelEndpoint(repositoryId: string) {
    return `${this.getCatalogTemplateRepositoryEndpoint(repositoryId)}/actions`;
  }

  async importTemplateRepository(body: any, repo_uuid?: string): Promise<any> {
    if (repo_uuid) {
      return this.httpService.request(
        this.getCatalogTemplateRepositoryEndpoint(repo_uuid),
        {
          method: 'PUT',
          body,
        },
      );
    } else {
      return this.httpService.request(
        this.getCatalogTemplateRepositoriesEndpoint(),
        {
          method: 'POST',
          body,
        },
      );
    }
  }

  async createAppWithTemplate(
    body: any,
  ): Promise<AppCatalogTemplateCreateResponse> {
    return this.httpService.request<AppCatalogTemplateCreateResponse>(
      this.getCatalogApplicationsEndpoint(),
      {
        method: 'POST',
        body,
      },
    );
  }

  async getTemplateRepositories(
    type = '',
    uuid = '',
  ): Promise<AppCatalogTemplateRepository[]> {
    if (type) {
      return this.getPublicTemplateRepositories(type);
    } else {
      return this.getPrivateTemplateRepository(uuid);
    }
  }

  async getPrivateTemplateRepository(
    uuid: string,
  ): Promise<AppCatalogTemplateRepository[]> {
    let randStatus: AppCatalogTemplateRepository;
    await this.httpService
      .request(this.getCatalogTemplateRepositoriesEndpoint() + `/${uuid}`, {
        method: 'GET',
      })
      .then((res: AppCatalogTemplateRepository) => {
        randStatus = res;
      })
      .catch(err => {
        const message = this.translate.get('app_catalog_failed_to_get_catalog');
        this.auiNotificationService.error(message);
        this.logger.error('Failed to get template repo: ', err);
      });
    return [
      {
        ...repository,
        ...randStatus,
      } as AppCatalogTemplateRepository,
    ];
  }

  async getPublicTemplateRepositories(
    type: string,
  ): Promise<AppCatalogTemplateRepository[]> {
    let randStatus = {};
    let typeNum = 0;
    switch (type) {
      case 'micro_service':
        typeNum = 1;
        break;
      case 'middleware':
        typeNum = 2;
        break;
      case 'big_data':
        typeNum = 4;
        break;
    }
    await this.httpService
      .request(this.getCatalogPublicTemplateRepositoriesEndpoint(), {
        method: 'GET',
        params: {
          name: 'alauda_public_rp',
          template_type: typeNum,
        },
      })
      .then(res => {
        randStatus = res['result'][0];
      })
      .catch(() => {
        const message = this.translate.get(
          'app_catalog_failed_to_get_public_repo',
        );
        this.auiNotificationService.error(message);
      });
    return [
      {
        ...repository,
        ...randStatus,
      } as AppCatalogTemplateRepository,
    ];
  }

  async refreshTemplateRepository(uuid: string): Promise<any> {
    return this.httpService.request(
      this.getCatalogTemplateRepositoryRefreshEndpoint(uuid),
      {
        method: 'PUT',
      },
    );
  }

  async getTemplateById(uuid: string): Promise<any> {
    return this.httpService.request(this.getCatalogTemplateEndpoint(uuid), {
      method: 'GET',
    });
  }

  getTemplateRepositoryList({
    pageNo = 1,
    pageSize = 20,
    params: { name = '' },
  }) {
    return this.httpService
      .request(this.getCatalogTemplateRepositoriesEndpoint(), {
        method: 'GET',
        params: {
          page: pageNo,
          page_size: pageSize,
          name,
        },
      })
      .then((res: FetchDataResult<any>) => res);
  }

  getTemplates({
    pageNo = 1,
    pageSize = 20,
    params: { name = '', repository_uuid = '' },
  }) {
    return this.httpService
      .request(this.getCatalogTemplatesEndpoint(), {
        method: 'GET',
        params: {
          page: pageNo,
          page_size: pageSize,
          name,
          repository_uuid,
        },
      })
      .then((res: FetchDataResult<any>) => res);
  }

  deleteRepository(uuid: string) {
    return this.httpService.request(
      this.getCatalogTemplateRepositoryEndpoint(uuid),
      {
        method: 'DELETE',
      },
    );
  }

  cancelImportRepository(uuid: string) {
    return this.httpService.request(
      this.getCatalogTemplateRepositoryCancelEndpoint(uuid),
      {
        method: 'POST',
        body: {
          type: 'cancel_sync',
        },
      },
    );
  }
}
