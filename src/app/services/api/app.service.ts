import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import {
  K8sApplicationPayload,
  K8sResourceWithActions,
} from 'app/typings/backend-api';
import {
  DaemonSet,
  Deployment,
  KubernetesResource,
  ObjectMeta,
  Pod,
  StatefulSet,
} from 'app/typings/raw-k8s';
import { BatchReponse, ResourceList } from 'app_user/core/types';
import { EnvironmentService } from './environment.service';

export interface AppTopologyEdge {
  from: string;
  to: string;
  type: string;
}

export interface AppTopology {
  nodes: {
    [key: string]: KubernetesResource;
  };
  edges: AppTopologyEdge[];
}

export interface WorkloadStatus {
  pod_status?: {
    Failed: number;
    Running: number;
  };
  desired?: number;
  current?: number;
  status?: string;
  messages?: ApplicationStatusMessage[];
  pods?: {
    [key: string]: {
      message?: string;
      reason?: string;
      status?: string;
    };
  };
}

export interface WorkLoads {
  Deployment?: Deployment[];
  Daemonset?: DaemonSet[];
  Statefulset?: StatefulSet[];
}

export interface ApplicationStatus {
  app_status: string;
  messages: ApplicationStatusMessage[];
  workloads: {
    [key: string]: WorkloadStatus;
  };
}

export interface ApplicationStatusMessage {
  reason: string;
  message: string;
}

export interface ApplicationAddress {
  workloads?: {
    [key: string]: {
      [key: string]: WorkloadAddress[];
    };
  };
}

export interface WorkloadAddress {
  name?: string;
  protocol?: string;
  host?: string;
  _frontend_comments?: string;
  frontend?: number;
  _port_comments?: string;
  port?: number;
  url?: string;
}

enum diffKey {
  CREATE = 'create',
  UPDATE = 'update',
  DELETE = 'delete',
}

export type ApplicationHistoryDiff = {
  [key in diffKey]?: {
    kind?: string;
    name?: string;
  }[]
};

export interface ApplicationHistory extends KubernetesResource {
  metadata?: ObjectMeta;
  spec?: {
    revision?: number;
    yaml?: string;
    creationTimestamp?: string;
    user?: string;
    changeCause?: string;
    // compare with latest versions
    resourceDiffsWithLatest?: ApplicationHistoryDiff;
    resourceDiffs: ApplicationHistoryDiff;
    latestRevision?: number;
  };
}

@Injectable()
export class AppService {
  APPS_ENDPOINT: string;
  K8S_APPS_ENDPOINT: string;

  constructor(
    private httpService: HttpService,
    private envService: EnvironmentService,
    @Inject(ACCOUNT) account: Account,
  ) {
    const namespace = account.namespace;
    this.APPS_ENDPOINT = `/ajax/v1/applications/${namespace}/`;
    this.K8S_APPS_ENDPOINT = '/ajax/v2/apps/';
  }

  // ********** Legacy App API (v1) ********** //

  getApps(_params: any): Promise<any> {
    // TODO: delete me
    return Promise.resolve([]);
  }

  /**
   * CRD API
   * Get data from a new application API definition implemented
   * through the native Application CRD.
   */

  /**
   * Generate common prefix
   */
  getCRDEndPoint(clusterName: string) {
    return `/ajax/v2/kubernetes/clusters/${clusterName}/applications`;
  }

  /**
   * List applications in clsuter/namespace
   */
  getAppListCRD(params: {
    cluster_name: string;
    namespace?: string;
    params: any;
  }): Promise<ResourceList> {
    return this.httpService
      .request(
        `${this.getCRDEndPoint(params.cluster_name)}${
          params.namespace ? `/${params.namespace}/` : ''
        }`,
        {
          method: 'GET',
          params: params.params,
        },
      )
      .then((res: any) => {
        if (res.result) {
          return {
            results: res.result,
          };
        } else {
          return res;
        }
      });
  }

  /**
   * Get an application in namespace
   */
  getAppDetailCRD(params: {
    cluster_name: string;
    namespace: string;
    app: string;
  }): Promise<K8sResourceWithActions[]> {
    return this.httpService
      .request(
        `${this.getCRDEndPoint(params.cluster_name)}/${params.namespace}/${
          params.app
        }`,
        {
          method: 'GET',
        },
      )
      .then(({ result }) => result);
  }

  batchGetAppDetailCRD(params: {
    project: string;
    clusters: string[];
    namespace: string;
    name: string;
  }): Promise<{
    [clusterName: string]: {
      resource_actions: string[];
      kubernetes: any;
    }[];
  }> {
    const requests = params.clusters.reduce((requests, cluster: string) => {
      requests[cluster] = {
        url: `/v2/kubernetes/clusters/${cluster}/applications/${
          params.namespace
        }/${params.name}?project_name=${params.project}`,
      };
      return requests;
    }, {});
    return this.httpService
      .request({
        method: 'POST',
        url: '/ajax/v2/batch/',
        body: {
          common: {
            method: 'GET',
            header: {
              'Content-Type': 'application/json',
            },
          },
          requests,
        },
      })
      .then((res: BatchReponse) => {
        return Object.entries(res.responses).reduce(
          (obj, [clusterName, data]) => {
            if (data.code < 300) {
              const app: {
                resource_actions: string[];
                kubernetes: any;
              }[] = JSON.parse(data.body);
              obj[clusterName] = app;
            }
            return obj;
          },
          {},
        );
      })
      .catch(_e => null);
  }

  removeComponentCRD(params: {
    cluster: string;
    namespace: string;
    payload: any;
    app: string;
  }) {
    return this.httpService.request(
      `${this.getCRDEndPoint(params.cluster)}/${params.namespace}/${
        params.app
      }/remove`,
      {
        method: 'PUT',
        body: params.payload,
      },
    );
  }

  batchRemoveComponentCRD(params: {
    clusters: string[];
    namespace: string;
    project: string;
    payload: any;
    app: string;
  }) {
    const requests = params.clusters.reduce((requests, cluster: string) => {
      requests[cluster] = {
        url: `/v2/kubernetes/clusters/${cluster}/applications/${
          params.namespace
        }/${params.app}/remove?project_name=${params.project}`,
      };
      return requests;
    }, {});
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'PUT',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(params.payload),
        },
        requests,
      },
    });
  }

  createAppCRD(params: { cluster: string; payload: K8sApplicationPayload }) {
    return this.httpService.request(`${this.getCRDEndPoint(params.cluster)}`, {
      method: 'POST',
      body: params.payload,
    });
  }

  batchCreateAppCRD(params: {
    payload: K8sApplicationPayload;
    project: string;
    clusters: string[];
  }) {
    const requests = params.clusters.reduce((requests, cluster: string) => {
      requests[cluster] = {
        url: `/v2/kubernetes/clusters/${cluster}/applications?project_name=${
          params.project
        }`,
      };
      return requests;
    }, {});
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(params.payload),
        },
        requests,
      },
    });
  }

  updateAppCRD(params: { cluster: string; payload: K8sApplicationPayload }) {
    const name = params.payload.resource.name;
    const namespace = params.payload.resource.namespace;
    return this.httpService.request(
      `${this.getCRDEndPoint(params.cluster)}/${namespace}/${name}`,
      {
        method: 'PUT',
        body: {
          kubernetes: params.payload.kubernetes,
        },
      },
    );
  }

  batchUpdateAppCRD(params: {
    cluster: string;
    payload: K8sApplicationPayload;
    clusters?: string[];
    project?: string;
  }) {
    const name = params.payload.resource.name;
    const namespace = params.payload.resource.namespace;
    const requests = params.clusters.reduce((requests, cluster: string) => {
      requests[cluster] = {
        url: `/v2/kubernetes/clusters/${cluster}/applications/${namespace}/${name}?project_name=${
          params.project
        }`,
      };
      return requests;
    }, {});
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'PUT',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(params.payload),
        },
        requests,
      },
    });
  }

  deleteAppCRD(params: { cluster: string; namespace: string; app: string }) {
    return this.httpService.request(
      `${this.getCRDEndPoint(params.cluster)}/${params.namespace}/${
        params.app
      }`,
      {
        method: 'DELETE',
      },
    );
  }

  batchDeleteAppCRD(params: {
    clusters: string[];
    namespace: string;
    app: string;
    project: string;
  }) {
    const requests = params.clusters.reduce((requests, cluster: string) => {
      requests[cluster] = {
        url: `/v2/kubernetes/clusters/${cluster}/applications/${
          params.namespace
        }/${params.app}?project_name=${params.project}`,
      };
      return requests;
    }, {});
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests,
      },
    });
  }

  getResourcesInNamespace(params: {
    cluster: string;
    resource_type: string;
    namespace: string;
    name?: string; // filter by name
    labelSelector?: string; // filter by label
    fieldSelector?: string; // filter by field
    order_by?: string; // default metadata.name
    page?: number; // default 1
    page_size?: number; // default 20
  }) {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${params.cluster}/${params.resource_type}/${
        params.namespace
      }/`,
      {
        method: 'GET',
        params: {
          name: params.name || '',
          labelSelector: params.labelSelector || '',
          fieldSelector: params.fieldSelector || '',
          page: params.page || 1,
          page_size: params.page_size || 20,
        },
      },
    );
  }

  updateResource(params: {
    payload: any;
    cluster: string;
    resource_type: string;
    namespace: string;
    name: string;
  }) {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${params.cluster}/${params.resource_type}/${
        params.namespace
      }/${params.name}`,
      {
        method: 'PUT',
        body: params.payload,
      },
    );
  }

  deleteResource(params: {
    cluster: string;
    resource_type: string;
    namespace: string;
    name: string;
  }) {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${params.cluster}/${params.resource_type}/${
        params.namespace
      }/${params.name}`,
      {
        method: 'DELETE',
      },
    );
  }

  batchDeleteResource(params: {
    clusters: string[];
    resource_type: string;
    namespace: string;
    name: string;
    project: string;
  }) {
    const requests = params.clusters.reduce((requests, cluster: string) => {
      requests[cluster] = {
        url: `/v2/kubernetes/clusters/${cluster}/${params.resource_type}/${
          params.namespace
        }/${params.name}?project_name=${params.project}`,
      };
      return requests;
    }, {});
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests,
      },
    });
  }

  startApplication(params: {
    cluster: string;
    namespace: string;
    app: string;
  }) {
    return this.httpService.request(
      `${this.getCRDEndPoint(params.cluster)}/${params.namespace}/${
        params.app
      }/start`,
      {
        method: 'PUT',
      },
    );
  }

  stopApplication(params: { cluster: string; namespace: string; app: string }) {
    return this.httpService.request(
      `${this.getCRDEndPoint(params.cluster)}/${params.namespace}/${
        params.app
      }/stop`,
      {
        method: 'PUT',
      },
    );
  }

  getApplicationPods(params: {
    cluster: string;
    namespace: string;
    app: string;
  }): Promise<K8sResourceWithActions<Pod>> {
    return this.httpService.request(
      `${this.getCRDEndPoint(params.cluster)}/${params.namespace}/${
        params.app
      }/pods`,
      {
        method: 'GET',
      },
    );
  }

  /**
   * 获取应用以及应用下workload的状态集合
   * http://confluence.alaudatech.com/pages/viewpage.action?pageId=27178273
   */
  getApplicationStatus(params: {
    cluster: string;
    namespace: string;
    app: string;
  }): Promise<ApplicationStatus> {
    return this.httpService
      .request(
        `${this.getCRDEndPoint(params.cluster)}/${params.namespace}/${
          params.app
        }/status`,
        {
          method: 'GET',
        },
      )
      .catch(_err => null);
  }

  getApplicationAddress(params: {
    cluster: string;
    namespace: string;
    app: string;
  }): Promise<ApplicationAddress> {
    return this.httpService.request(
      `${this.getCRDEndPoint(params.cluster)}/${params.namespace}/${
        params.app
      }/address`,
      {
        method: 'GET',
      },
    );
  }

  getApplicationTopology(params: {
    cluster: string;
    namespace: string;
    name: string;
  }): Promise<AppTopology> {
    // /v2/kubernetes/clusters/ace/topology/alauda-system/application/jaeger
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${params.cluster}/topology/${
        params.namespace
      }/application/${params.name}`,
      {
        method: 'GET',
      },
    );
  }

  getWorkloads(params: {
    cluster: string;
    namespace: string;
  }): Promise<WorkLoads> {
    return this.httpService.request(
      `/ajax/v2/paas/clusters/${params.cluster}/namespaces/${
        params.namespace
      }/workloads/`,
      {
        method: 'GET',
      },
    );
  }

  getApplicationYaml(params: {
    cluster: string;
    namespace: string;
    app: string;
  }): Promise<string> {
    return this.httpService.request(
      `${this.getCRDEndPoint(params.cluster)}/${params.namespace}/${
        params.app
      }/yaml`,
      {
        method: 'GET',
        responseType: 'text',
      },
    );
  }

  getApplicationHistoryList(params: {
    namespace: string;
    cluster: string;
    app: string;
  }): Promise<K8sResourceWithActions<ApplicationHistory>[]> {
    return this.httpService
      .request(
        `/ajax/v2/kubernetes/clusters/${params.cluster}/applicationhistories/${
          params.namespace
        }/`,
        {
          method: 'GET',
          params: {
            labelSelector: `app.${
              this.envService.environments.label_base_domain
            }/name=${params.app}.${params.namespace}`,
          },
        },
      )
      .then(({ result }) => result);
  }

  getApplicationHistoryDetail(params: {
    cluster: string;
    namespace: string;
    name: string;
  }): Promise<K8sResourceWithActions<ApplicationHistory>> {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${params.cluster}/applicationhistories/${
        params.namespace
      }/${params.name}`,
      {
        method: 'GET',
      },
    );
  }

  rollback(params: {
    cluster: string;
    namespace: string;
    revision: number;
    app: string;
  }) {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${params.cluster}/applications/${
        params.namespace
      }/${params.app}/rollback`,
      {
        method: 'POST',
        body: {
          revision: params.revision,
        },
      },
    );
  }
}
