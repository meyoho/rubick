import { Inject, Injectable } from '@angular/core';

import { find, get } from 'lodash-es';

import { ImageRegistryService } from 'app/services/api/image-registry.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import { RcImageSelection } from 'app/typings/k8s-form-model';
@Injectable()
export class AppUtilitiesService {
  constructor(
    private roleUtilities: RoleUtilitiesService,
    private registryService: ImageRegistryService,
    @Inject(ACCOUNT) private account: Account,
  ) {}

  /**
   * App status: Running/Warning/Error/Stopped/Deploying
   * @param app
   */
  canStart(app: any) {
    return ['stopped'].includes(app.resource.status.toLowerCase());
  }

  canStop(app: any) {
    return ['running', 'warning', 'error'].includes(
      app.resource.status.toLowerCase(),
    );
  }

  canDelete(app: any) {
    return ['running', 'stopped', 'error', 'warning', 'deploying'].includes(
      app.resource.status.toLowerCase(),
    );
  }

  canUpdate(app: any) {
    return !['deploying'].includes(
      (app.status || app.resource.status).toLowerCase(),
    );
  }

  canRollback(app: any) {
    return !!get(app, 'resource.state.rollbackable');
  }

  /**
   * App permission: app:start
   * show / hide
   * @param service
   */
  showStart(app: any) {
    return this.roleUtilities.resourceHasPermission(
      app,
      'application',
      'start',
    );
  }

  showStop(app: any) {
    return this.roleUtilities.resourceHasPermission(app, 'application', 'stop');
  }

  showUpdate(app: any) {
    return this.roleUtilities.resourceHasPermission(
      app,
      'application',
      'update',
    );
  }

  showDelete(app: any) {
    return this.roleUtilities.resourceHasPermission(
      app,
      'application',
      'delete',
    );
  }

  showRollback(service: any) {
    return this.roleUtilities.resourceHasPermission(
      service,
      'application',
      'update',
    );
  }

  async getRepositoryParamsFromRawImageName(
    image_name: string,
  ): Promise<RcImageSelection> {
    const result: RcImageSelection = {
      is_public_registry: false,
      registry_name: '',
      registry_endpoint: '',
      project_name: '',
      repository_name: '',
      full_image_name: image_name,
    };
    if (image_name.indexOf('/') < 0) {
      image_name = `index.docker.io/library/${image_name}`;
    }
    const registries = await this.registryService.find();
    const fields = image_name.split('/');
    const [endpoint, ...parts] = fields;
    result.registry_endpoint = endpoint;
    const registry = find(registries, item => {
      return item.is_third
        ? fields[1] === 'public'
          ? item.is_public && item.endpoint === endpoint
          : !item.is_public && item.endpoint === endpoint
        : item.endpoint === endpoint;
    });
    if (registry) {
      result.is_third = !!registry.is_third;
      if (registry.is_public && fields[1] !== 'public' && !registry.is_third) {
        const [, namespace, repo_name] = fields;
        // repository is in public registry but not user namespace (eg: tutum, library), should get repository from thrid party API
        if (namespace !== this.account.namespace) {
          result.repository_name = parts.join('/');
          return result;
        }
        result.repository_name = repo_name;
      } else {
        if (fields.length === 2) {
          const [, repo_name] = fields;
          result.repository_name = repo_name;
        } else if (fields.length === 3) {
          const [, project_name, repo_name] = fields;
          result.project_name = project_name;
          result.repository_name = repo_name;
        } else if (fields.length === 4) {
          const [, , project_name, repo_name] = fields;
          result.project_name = project_name;
          result.repository_name = repo_name;
        }
      }
      result.is_public_registry = !!registry.is_public;
      result.registry_name = registry.name;
    } else {
      result.repository_name = parts.join('/');
      // result.full_image_name = `${result.registry_endpoint}/${
      //   result.repository_name
      // }`;
    }
    return result;
  }
}
