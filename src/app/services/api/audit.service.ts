import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

export interface AuditDetail {
  param: string;
}

export interface AuditParam {
  page: number;
  page_size: number;
  start_time: number;
  end_time: number;
  project_name?: string;
  fast_search?: string;
  user_name?: string;
  operation_type?: string;
  resource_name?: string;
  resource_type?: string;
  log_level?: number;
  operation_result?: string;
}

export interface Audit {
  time: number;
  user_name: string;
  operation_type: string;
  namespace: string;
  resource_type: string;
  resource_uuid: string;
  resource_name: string;
  log_level: number;
  operation_result?: string;
  source_ip: string;
  request_url: string;
  api_version: string;
  request_id: string;
  request_body: any;
  response_body: any;
  detail: AuditDetail;
}

export interface Audits {
  total_pages: number;
  page_size: number;
  total_items: number;
  results: Audit[];
}

export interface AuditTypes {
  code: number;
  result: string[];
}

@Injectable()
export class AuditService {
  AUDIT_ENDPOINT: string;

  constructor(
    @Inject(ACCOUNT) public account: Account,
    private httpService: HttpService,
  ) {
    this.AUDIT_ENDPOINT = `/ajax/v1/audits/${this.account.namespace}`;
  }

  getAudits(params: AuditParam) {
    return this.httpService.request<Audits>(this.AUDIT_ENDPOINT, {
      method: 'GET',
      params,
    });
  }

  getAuditTypes(params: any): Promise<string[]> {
    return this.httpService
      .request<AuditTypes>(`${this.AUDIT_ENDPOINT}/types`, {
        method: 'GET',
        params,
      })
      .then(({ result }) => result);
  }
}
