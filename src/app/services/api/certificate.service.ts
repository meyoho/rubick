import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

export interface Certificate {
  uuid: string;
  name: string;
  space_name: string;
  space_uuid: string;
  project_name: string;
  project_uuid: string;
  description: string;
  private_key: string;
  public_cert: string;
  is_used: boolean;
  status: string;
  service_count: number;
}

@Injectable()
export class CertificateService {
  private URL_PREFIX: string;

  constructor(
    @Inject(ACCOUNT) private account: Account,
    private http: HttpService,
  ) {
    this.URL_PREFIX = `/ajax/certificates/${this.account.namespace}`;
  }

  getCertificates(params?: {
    load_balancer_id?: string;
    page_size?: number;
    page?: number;
    project_name?: string;
  }): Promise<{
    count: number;
    next: null | string;
    num_pages: number;
    previous: null | string;
    page_size: number;
    results: Certificate[];
  }> {
    if (!params.page_size) {
      params.page_size = 50;
    }
    return this.http.request(this.URL_PREFIX, {
      method: 'get',
      params,
    });
  }
}
