import { Injectable } from '@angular/core';

import { keyBy, mapValues } from 'lodash-es';

import { FetchDataResult } from 'app/abstract/pagination-data';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { HttpService } from 'app/services/http.service';
import { BatchReponse } from 'app_user/core/types';

/**
 * @deprecated
 */
export interface K8sSecret {
  kubernetes?: {
    apiVersion: string;
    data: {
      [key: string]: string;
    };
    kind: 'Secret';
    type: string;
    metadata: {
      name: string;
      uid?: string;
      namespace?: string;
      resourceVersion?: string;
      annotations?: {
        [key: string]: string;
      };
    };
  };
  resource_actions?: string[];
}

/**
 * @deprecated
 */
export interface SecretOption {
  name: string;
  uuid: string;
  namespace: string;
  data: any;
}
export interface MirrorSecret {
  uuid: string;
  name: string;
  cluster_uuid: string;
  cluster_name: string;
  cluster_display_name: string;
  namespace_name: string;
}

/**
 * @deprecated
 */
@Injectable()
export class ConfigSecretService {
  CLUSTERS_ENDPOINT = '/ajax/v2/kubernetes/clusters/';

  constructor(
    private httpService: HttpService,
    private k8sResourceService: K8sResourceService,
  ) {}

  // ********** K8S Secret ********** //

  getK8sSecrets(params: {
    clusterId: string;
    namespace?: string;
    params?: { name?: string };
  }): Promise<K8sSecret[]>;
  getK8sSecrets(params: {
    clusterId: string;
    namespace?: string;
    params?: { page?: number; page_size?: number; name?: string };
  }): Promise<FetchDataResult<K8sSecret>>;
  getK8sSecrets({
    clusterId,
    namespace = '',
    params,
  }: {
    clusterId: string;
    namespace?: string;
    params?: { page?: number; page_size?: number; name?: string };
  }): Promise<K8sSecret[] | FetchDataResult<K8sSecret>> {
    return this.httpService
      .request(
        this.CLUSTERS_ENDPOINT +
          clusterId +
          '/secrets/' +
          (namespace ? namespace + '/' : ''),
        {
          method: 'GET',
          params,
          addNamespace: false,
        },
      )
      .then((res: any) => (res.results ? res : res.result));
  }

  getSecretOptions({
    clusterId,
    namespace,
    params,
  }: {
    clusterId: string;
    namespace?: string;
    params?: { name?: string };
  }): Promise<SecretOption[]> {
    return this.getK8sSecrets({
      clusterId,
      namespace,
      params,
    })
      .then(results =>
        results.map(result => ({
          name: result.kubernetes.metadata.name,
          uuid: result.kubernetes.metadata.uid,
          namespace: result.kubernetes.metadata.namespace,
          data: result.kubernetes.data,
        })),
      )
      .catch(() => []);
  }

  getK8sSecret({
    clusterId,
    namespace,
    name,
  }: {
    clusterId: string;
    namespace: string;
    name: string;
  }) {
    return this.httpService.request<K8sSecret>(
      this.CLUSTERS_ENDPOINT + clusterId + '/secrets/' + namespace + '/' + name,
      {
        method: 'GET',
      },
    );
  }

  async createK8sSecret(
    clusterId: string,
    secretInfo: K8sSecret['kubernetes'],
    clusters?: string[],
  ) {
    if (clusters && clusters.length) {
      const project_name = window.sessionStorage.getItem('project');
      const res: BatchReponse = await this.createK8sSecretBatch(
        secretInfo,
        clusters,
        project_name,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_secret_create',
        res,
        secretInfo.metadata.name,
      );
      return this.k8sResourceService.currentClusterResponseStatus(
        res,
        clusterId,
      );
    }
    return this.httpService.request<K8sSecret>(
      this.CLUSTERS_ENDPOINT + clusterId + '/secrets/',
      {
        method: 'POST',
        body: secretInfo,
        addNamespace: false,
      },
    );
  }

  createK8sSecretBatch(
    body: K8sSecret['kubernetes'],
    clusters: string[],
    project_name: string,
  ): Promise<BatchReponse> {
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/secrets?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body),
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  async updateK8sSecret(
    clusterId: string,
    secretInfo: K8sSecret,
    clusters?: string[],
  ) {
    if (clusters && clusters.length) {
      const res: BatchReponse = await this.updateSecretBatch(
        clusters,
        secretInfo,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_secret_update',
        res,
        secretInfo.kubernetes.metadata.name,
      );
      return this.k8sResourceService.currentClusterResponseStatus(
        res,
        clusterId,
      );
    }
    return this.updateSecret(clusterId, secretInfo);
  }

  updateSecret(clusterId: string, secret: K8sSecret) {
    return this.httpService.request({
      method: 'PUT',
      url:
        this.CLUSTERS_ENDPOINT +
        clusterId +
        '/secrets/' +
        secret.kubernetes.metadata.namespace +
        '/' +
        secret.kubernetes.metadata.name,
      body: secret.kubernetes,
      addNamespace: false,
    });
  }

  updateSecretBatch(
    clusters: string[],
    secret: K8sSecret,
  ): Promise<BatchReponse> {
    const project_name = window.sessionStorage.getItem('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/secrets/${
          secret.kubernetes.metadata.namespace
        }/${secret.kubernetes.metadata.name}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'PUT',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(secret.kubernetes),
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  deleteK8sSecret(clusterId: string, secret: K8sSecret) {
    return this.httpService.request(
      this.CLUSTERS_ENDPOINT +
        clusterId +
        '/secrets/' +
        secret.kubernetes.metadata.namespace +
        '/' +
        secret.kubernetes.metadata.name,
      {
        method: 'DELETE',
      },
    );
  }

  deleteSecretBatch(mirrorSecret: MirrorSecret[]): Promise<BatchReponse> {
    const project_name = window.sessionStorage.getItem('project');
    const requests = mirrorSecret.map((item: MirrorSecret) => {
      return {
        cluster: item.cluster_name,
        url: `/v2/kubernetes/clusters/${item.cluster_name}/secrets/${
          item.namespace_name
        }/${item.name}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      addNamespace: false,
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  async getMirrorSecrets(
    clusterName: string,
    namespace: string,
    name: string,
  ): Promise<any> {
    const mirrorSecret: MirrorSecret[] = [];
    const mirrorCluster = await this.k8sResourceService.getMirrorCluster(
      clusterName,
    );
    if (mirrorCluster.length) {
      const clusters = mirrorCluster.map((item: any) => {
        return item.name;
      });
      const res: BatchReponse = await this.k8sResourceService.getMirrorResources(
        clusters,
        namespace,
        'secrets',
        name,
      );
      Object.entries(res.responses).forEach(([id, data]) => {
        if (data.code < 300) {
          const body = JSON.parse(data.body);
          const cluster = mirrorCluster.find(cluster => {
            return cluster.name === id;
          });
          const metadata = body.kubernetes.metadata;
          mirrorSecret.push({
            uuid: metadata.uid,
            name: metadata.name,
            namespace_name: metadata.namespace,
            cluster_uuid: cluster ? cluster.id : '',
            cluster_name: cluster ? cluster.name : '',
            cluster_display_name: cluster ? cluster.display_name : '',
          });
        }
      });
    }
    return mirrorSecret;
  }
}
