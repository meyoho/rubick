import { Inject, Injectable } from '@angular/core';

import { head, mapValues } from 'lodash-es';
import { from as fromPromise, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { HttpService } from 'app/services/http.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, KubernetesResource, ObjectMeta } from 'app/typings';

export enum CredentialType {
  BasicAuth = 'kubernetes.io/basic-auth',
  DockerConfig = 'kubernetes.io/dockerconfigjson',
  OAuth2 = 'devops.alauda.io/oauth2',
  SSH = 'kubernetes.io/ssh-auth',
}

export interface CredentialIdentity {
  name: string;
  namespace?: string;
}

export interface Credential extends CredentialIdentity {
  displayName: string;
  description?: string;
  type: CredentialType;
  username?: string;
  password?: string;
  clientID?: string;
  clientSecret?: string;
  generic?: { [name: string]: any };
  dockerAddress?: string;
  dockerUsername?: string;
  dockerPassword?: string;
  dockerEmail?: string;
  sshPrivatekey?: string;
  ownerReferences: any[];
  accessToken?: string;
  creationTimestamp?: string;
  __orignal?: any;
}

export interface DevopsSecret extends KubernetesResource {
  apiVersion: string;
  kind: string;
  type: CredentialType;
  metadata: ObjectMeta;
  data?: DevopsSecretBasicData | DevopsSecretOauthData | DevopsSecretImageData;
  stringData?:
    | DevopsSecretBasicData
    | DevopsSecretOauthData
    | DevopsSecretImageData;
  [key: string]: any;
}

export interface DevopsSecretBasicData {
  username: string;
  password: string;
}

export interface DevopsSecretOauthData {
  clientID: string;
  clientSecret: string;
}

export interface DevopsSecretImageData {
  '.dockerconfigjson': string;
}

let ANNOTATION_DESCRIPTION: string,
  ANNOTATION_DISPLAY_NAME: string,
  ANNOTATION_PRODUCT: string;
const PRODUCT_NAME = 'Alauda Cloud Enterprise';

@Injectable()
export class CredentialApiService {
  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private httpService: HttpService,
  ) {
    this.LABEL_BASE_DOMAIN = this.environments.label_base_domain || 'alauda.io';
    ANNOTATION_DISPLAY_NAME = `${this.LABEL_BASE_DOMAIN}/displayName`;
    ANNOTATION_DESCRIPTION = `${this.LABEL_BASE_DOMAIN}/description`;
    ANNOTATION_PRODUCT = `${this.LABEL_BASE_DOMAIN}/product`;
  }

  SECRET_URL = '/ajax/v2/devops/namespace/secrets';
  LABEL_BASE_DOMAIN: string;

  find(
    query: {
      page?: number | string;
      page_size?: number;
      order_by?: string;
      secretType?: string;
      filterBy?: string;
      sortBy?: string;
      itemsPerPage?: string;
      space_name?: string;
    },
    project_name = '',
    space_name = '',
  ): Observable<{ items: Credential[]; length: number }> {
    const params =
      project_name && space_name
        ? {
            ...query,
            space_name,
          }
        : query;
    return fromPromise(
      this.httpService.request({
        url: this.SECRET_URL,
        method: 'GET',
        params,
      }),
    ).pipe(
      map((res: any) => {
        return {
          items: (res.results || res.result || []).map(toModel),
          errors: res.errors,
          length: res.count,
        };
      }),
    );
  }

  get(identity: CredentialIdentity, space_name?: string) {
    const params = space_name ? { space_name } : {};
    return fromPromise(
      this.httpService.request({
        url: `${this.SECRET_URL}/${identity.name}`,
        method: 'GET',
        params,
      }),
    ).pipe(map(toModel));
  }

  post(data: Credential, space_name?: string) {
    const params = space_name ? { space_name } : {};
    return fromPromise(
      this.httpService.request({
        url: this.SECRET_URL,
        method: 'POST',
        body: toResource(data),
        params,
      }),
    ).pipe(map(toModel));
  }

  put(data: Credential, space_name?: string) {
    const params = space_name ? { space_name } : {};
    return fromPromise(
      this.httpService.request({
        url: `${this.SECRET_URL}/${data.name}`,
        method: 'PUT',
        body: toResource(data),
        params,
      }),
    ).pipe(map(toModel));
  }

  delete(name: string, space_name?: string) {
    const params = space_name ? { space_name } : {};
    return fromPromise(
      this.httpService.request({
        url: `${this.SECRET_URL}/${name}`,
        method: 'DELETE',
        params,
      }),
    );
  }
}

function toModel(resource: DevopsSecret): Credential {
  const metadata = resource.metadata || {};
  const annotations = metadata.annotations || {};

  const secretFields: {
    [key: string]: string;
  } = getSecretData(resource);
  const secretProperties = secretFields;
  return {
    name: metadata.name,
    namespace: metadata.namespace,
    creationTimestamp: metadata.creationTimestamp,
    ownerReferences: metadata.ownerReferences || [],
    displayName: annotations[ANNOTATION_DISPLAY_NAME],
    description: annotations[ANNOTATION_DESCRIPTION],
    type: resource.type,
    ...secretProperties,
    __orignal: resource,
  };
}

function getSecretData(data: DevopsSecret) {
  switch (data.type) {
    case CredentialType.DockerConfig:
      return data.data
        ? parseDockerJson(JSON.parse(atob(data.data['.dockerconfigjson'])))
        : {};
    case CredentialType.BasicAuth:
      return { username: '', password: '' };
    case CredentialType.OAuth2:
      return { clientID: '', clientSecret: '' };
    case CredentialType.SSH:
      return { 'ssh-privatekey': '' };
    default:
      return {};
  }
}

function parseDockerJson(data: any) {
  const dockerAddress = head(Object.keys(data.auths || {}));
  return {
    dockerAddress,
    dockerUsername: '',
    dockerPassword: '',
    dockerEmail: '',
  };
}

function toResource(model: Credential): any {
  const orignal = model.__orignal || {};

  const data =
    model.type === CredentialType.BasicAuth
      ? {
          username: model.username,
          password: model.password,
        }
      : model.type === CredentialType.OAuth2
      ? {
          ...(orignal.data || {}),
          clientID: model.clientID,
          clientSecret: model.clientSecret,
        }
      : model.type === CredentialType.DockerConfig
      ? {
          '.dockerconfigjson': JSON.stringify({
            auths: {
              [model.dockerAddress]: {
                username: model.dockerUsername,
                password: model.dockerPassword,
                email: model.dockerEmail,
                auth: `${model.dockerUsername}:${model.dockerPassword}`,
              },
            },
          }),
        }
      : model.type === CredentialType.SSH
      ? {
          'ssh-privatekey': model.sshPrivatekey,
        }
      : mapValues(model.generic, value => value || '');

  return {
    apiVersion: 'v1',
    kind: 'Secret',
    metadata: {
      name: model.name,
      annotations: {
        [ANNOTATION_DISPLAY_NAME]: model.displayName,
        [ANNOTATION_DESCRIPTION]: model.description,
        [ANNOTATION_PRODUCT]: PRODUCT_NAME,
      },
      namespace: model.namespace,
    },
    type: model.type,
    stringData: data,
  };
}
