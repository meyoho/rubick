import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import { ResourceList } from 'app_user/core/types';

export interface Domain {
  domain_id?: string;
  domain: string;
  projects: string[];
  resource_actions?: string[];
}

@Injectable()
export class DomainService {
  CLUSTERS_ENDPOINT = '/ajax/v2/domains/';

  constructor(
    @Inject(ACCOUNT) public account: Account,
    private httpService: HttpService,
  ) {}

  getDomains(
    ignoreProject: boolean,
    params?: {
      page: number;
      page_size: number;
      search: string;
    },
  ): Promise<ResourceList | any> {
    const urlQuery = params
      ? `?page=${params.page}&page_size=${params.page_size}&search=${
          params.search
        }`
      : '';

    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${this.account.namespace}${urlQuery}`,
      {
        method: 'GET',
        ignoreProject,
      },
    );
  }

  createDomain(payload: Domain) {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${this.account.namespace}`,
      {
        method: 'POST',
        body: payload,
      },
    );
  }

  updateDomainBindProject(domain_id: string, projects: string[]) {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${this.account.namespace}/${domain_id}`,
      {
        method: 'PUT',
        body: projects,
      },
    );
  }

  deleteDomain(domain_id: string) {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${this.account.namespace}/${domain_id}`,
      {
        method: 'DELETE',
      },
    );
  }
}
