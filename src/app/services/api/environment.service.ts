import { Inject, Injectable } from '@angular/core';

import { get } from 'lodash-es';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT, ENVIRONMENTS } from 'app/shared/tokens';
import { Account, Environments } from 'app/typings';
export const QUOTA_TYPES = ['cpu', 'memory', 'storage', 'pvc_num', 'pods'];

export interface ConfigurationApi {
  type: string;
  id: number;
  name: string;
  namespace?: string;
  value?: string;
  display_name?: string;
  extra?: string;
}

@Injectable()
export class EnvironmentService {
  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) private account: Account,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  getDefaultQuota(type: 'namespace' | 'project') {
    let baseRate = 1;
    let transferType = type;
    try {
      const defaultQuota = JSON.parse(this.environments.default_quota);
      const newQuota = {};
      if (
        type === 'project' &&
        defaultQuota &&
        !defaultQuota['project'] &&
        defaultQuota['namespace']
      ) {
        transferType = 'namespace';
        baseRate = 2;
      }
      if (!defaultQuota || !defaultQuota[transferType]) {
        return null;
      }
      QUOTA_TYPES.forEach(quotaType => {
        newQuota[quotaType] =
          parseFloat(get(defaultQuota[transferType], quotaType)) * baseRate ||
          0;
      });
      return newQuota;
    } catch (e) {
      console.error(e);
      return null;
    }
  }

  beSameVendorCustomer(v: string) {
    return this.environments.vendor_customer === v;
  }

  getConfigurations(type: string = ''): Promise<ConfigurationApi[]> {
    return this.httpService
      .request(`ajax/v1/configuration/${this.account.namespace}`, {
        method: 'get',
        params: {
          type,
        },
        cache: true,
      })
      .then(({ result }) => result);
  }
}
