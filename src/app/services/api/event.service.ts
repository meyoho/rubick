import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import {
  EventQuery,
  EventsResult,
  K8sEventQuery,
  K8sEventsResult,
} from 'app2/features/event/event.types';

@Injectable()
export class EventService {
  EVENTS_URL: string;
  K8S_EVENT_URL: string;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) account: Account,
  ) {
    const namespace = account.namespace;
    this.EVENTS_URL = `/ajax/v1/events/${namespace}`;
    this.K8S_EVENT_URL = `/ajax/v2/kevents/`;
  }

  getEvents({
    start_time,
    end_time,
    pageno,
    size,
    event_types,
    event_type,
    event_pk,
    query_string = '',
  }: EventQuery): Promise<EventsResult> {
    let endpoint = this.EVENTS_URL;
    const params = {
      start_time,
      end_time,
      pageno,
      size,
      query_string,
    };
    if (event_types) {
      if (event_pk) {
        params['resource_type'] = event_types;
        params['resource_id'] = event_pk;
      } else {
        params['resource_type'] = event_types;
      }
    } else if (event_type) {
      if (event_pk) {
        endpoint = `${this.EVENTS_URL}/${event_type}/${event_pk}`;
      } else {
        endpoint = `${this.EVENTS_URL}/${event_type}/`;
      }
    }

    return this.httpService.request(endpoint, {
      method: 'GET',
      params,
      ignoreProject: true,
    });
  }

  getK8sEvents({
    start_time,
    end_time,
    cluster,
    name = '',
    kind = '',
    namespace = '',
    page = 1,
    page_size = 20,
  }: K8sEventQuery): Promise<K8sEventsResult> {
    return this.httpService.request(this.K8S_EVENT_URL, {
      method: 'GET',
      params: {
        start_time,
        end_time,
        cluster,
        name,
        kind,
        namespace,
        page,
        page_size,
      },
    });
  }
}
