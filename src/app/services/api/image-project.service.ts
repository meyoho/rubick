import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';

export interface ImageProject {
  resource_actions: string[];
  project_name: string;
  display_name?: string;
  name: string;
  repo_count: number;
  project_id: string;
  namespace: string;
  created_by: string;
  registry: {
    uuid: string;
    name: string;
    is_third?: boolean;
  };
}

@Injectable()
export class ImageProjectService {
  URL = `/ajax/registries/${this.account.namespace}`;

  constructor(
    private httpService: HttpService,
    private translate: TranslateService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  find(registry_name: string, is_third?: boolean): Promise<ImageProject[]> {
    const params = { is_third: !!is_third };
    return this.httpService
      .request({
        url: `${this.URL}/${registry_name}/projects`,
        method: 'GET',
        params,
      })
      .then(({ result }) => {
        const list = result.map((project: ImageProject) => ({
          display_name: project.project_name,
          ...project,
        }));
        if (!is_third) {
          return [
            {
              project_name: '@@shared',
              display_name: this.translate.get('default_project_name'),
            },
            ...list,
          ];
        } else {
          return list;
        }
      });
  }

  getProjects(
    registry_name: string,
    is_third?: boolean,
  ): Promise<ImageProject[]> {
    const params = { is_third: !!is_third };
    return this.httpService
      .request({
        url: `${this.URL}/${registry_name}/projects`,
        method: 'GET',
        params,
      })
      .then(({ result }) => result);
  }

  deleteProject(
    registry_name: string,
    project_name: string,
    is_third?: boolean,
  ): Promise<void> {
    const params = { is_third: !!is_third };

    return this.httpService.request({
      url: `${this.URL}/${registry_name}/projects/${project_name}`,
      method: 'DELETE',
      params,
    });
  }

  addProject(
    registry_name: string,
    project_name: string,
    is_third?: boolean,
  ): Promise<ImageProject> {
    const params = is_third ? { is_third: !!is_third } : {};
    return this.httpService.request({
      url: `${this.URL}/${registry_name}/projects`,
      method: 'POST',
      params,
      body: {
        name: project_name,
        is_third: !!is_third ? !!is_third : null,
      },
    });
  }
}
