import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import { Pagination } from 'app/typings/backend-api';

export interface ImageRepository {
  description: string;
  scan_enabled: boolean;
  tag_protected_count: number;
  updated_at: string;
  full_description: string;
  registry: {
    integration_id: string;
    endpoint: string;
    display_name: string;
    uuid: string;
    tag_protected_max_number: number;
    tag_max_number: number;
    is_public: boolean;
    name: string;
    is_third?: boolean;
  };
  download: number;
  is_public: boolean;
  icon: string;
  resource_actions: string[];
  pushed_at: string;
  uuid: string;
  name: string;
  created_at: string;
  pulled_at: string;
  namespace: string;
  upload: string;
  created_by: string;
  project: {
    project_name: string;
    project_id: string;
  };
  is_automated: boolean;
}

export interface ImageRepositoryTag {
  tag_name: string;
  level:
    | 'High'
    | 'Medium'
    | 'Low'
    | 'Negligible'
    | 'Unknown'
    | 'Failed'
    | 'Analyzing';
  summary?: {
    [key: string]: string;
  };
  created_at?: string;
  finished?: string;
  digest?: string;
  protected?: boolean;
}

export interface ImageRepositoryTagDetail {
  run_command?: string;
  entrypoint?: string;
  instance_ports?: number[];
  volumes?: string[];
  image_envvars?: {
    name: string;
    value: string;
  }[];
}

export interface ImageRepositoryTagReportSummary {
  total: number;
  fixable: number;
  detail: {
    High: number;
    Medium: number;
    Low: number;
    Negligible: number;
    Unknown: number;
    Security?: number;
  };
}

export interface ImageRepositoryTagReportDetail {
  cve_name?: string;
  level?: string;
  package_name: string;
  cur_version: string;
  secure_version?: string;
  layer_command: string;
  summary?: {
    count: number;
    level: string;
  };
  updated?: {
    count: number;
    level: string;
  };
}

export interface ImageRepositoryTagArtifacts {
  tag_name: string;
  build_id: string;
  build_at: string;
  artifacts_count: number;
  artifacts: ImageRepositoryTagArtifact[];
}

export interface ImageRepositoryTagArtifact {
  key: string;
  size: string;
  last_modified: string;
  path?: string;
}

@Injectable()
export class ImageRepositoryService {
  URL = `/ajax/registries/${this.account.namespace}`;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  private getUrlPrefix(registry_name: string, projectNameOrNamespace?: string) {
    const prefix = `${this.URL}/${registry_name}/`;
    return projectNameOrNamespace
      ? `${prefix}projects/${projectNameOrNamespace}/repositories`
      : `${prefix}repositories`;
  }

  /*
   * when registry without pagination params && is_third = true
   * @returns
   * {
       code: 200,
       result: [{
         "name": "busybox",
         "scan_enabled": true,
         "created_at": "2018-10-10T09:43:56.561795Z",
         "project": {
           "project_name": "alaudaorg",
           "project_id": 2
         },
         "registry": {
           "endpoint": "www.myalauda.com:31413",
           "display_name": "zhang-harbor",
           "uuid": "fdef72ab-4067-4601-a8c6-ea80325d9a07",
           "is_third": true,
           "is_public": false,
           "name": "zhang-harbor"
         },
         "download": 0,
         "description": ""
       }]
     }
   * */
  getRepositories(options: {
    registryName: string;
    projectName?: string;
    params?: {
      page?: number;
      page_size?: number;
      search?: string;
      is_third?: boolean;
      path?: string;
    };
  }) {
    return this.httpService
      .request({
        url: this.getUrlPrefix(options.registryName, options.projectName),
        method: 'GET',
        params: options.params || {},
      })
      .then((res: any) => res.result || res);
  }

  getRepository(
    registry_name: string,
    repository_name: string,
    project_name?: string,
    is_third?: boolean,
  ): Promise<ImageRepository> {
    const params = { is_third: !!is_third };

    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) + '/' + repository_name,
      method: 'GET',
      params,
    });
  }

  deleteRepository(
    registry_name: string,
    repository_name: string,
    project_name?: string,
    is_third?: boolean,
  ): Promise<void> {
    const params = { is_third: !!is_third };

    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) + '/' + repository_name,
      method: 'DELETE',
      params,
    });
  }

  addRepository(
    registry_name: string,
    repository: ImageRepository,
    project_name?: string,
  ): Promise<ImageRepository> {
    return this.httpService.request({
      url: this.getUrlPrefix(registry_name, project_name),
      method: 'POST',
      body: repository,
    });
  }

  updateRepository(
    registry_name: string,
    repository: ImageRepository,
    project_name?: string,
  ): Promise<void> {
    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) + '/' + repository.name,
      method: 'PUT',
      body: {
        description: repository.description,
        full_description: repository.full_description,
        is_public: repository.is_public,
        scan_enabled: repository.scan_enabled,
      },
    });
  }

  /*
    @returns {any[] | string[]}
    * eg:
    * params.is_third = true
    * [
        {
            "level": "Unknown",
            "created_at": "2016-06-23T23:23:37.198943461Z",
            "tag_name": "1.25.0",
            "summary": {
                "High": 0,
                "Fixable": 0
            },
            "finished": "2018-11-15T06:03:12.087208Z",
            "protected": false,
            "is_third": true,
            "digest": "c77a342d"
        }
    ]

    * params.is_third = false
    * ['tag1', 'tag2']
    * */
  getRepositoryTags(options: {
    registryName: string;
    projectName?: string;
    repositoryName?: string;
    params?: {
      page?: number;
      page_size?: number;
      view_type?: 'security' | 'detail';
      is_third?: boolean;
      path?: string;
    };
  }) {
    return this.httpService
      .request({
        url:
          this.getUrlPrefix(options.registryName, options.projectName) +
          '/' +
          options.repositoryName +
          '/tags',
        method: 'GET',
        params: options.params || {},
      })
      .then((res: any) => res.result || res)
      .catch(() => []);
  }

  getRepositoryTagDetail({
    registry_name,
    repository_name,
    project_name,
    tag_name,
    is_third = false,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
    is_third?: boolean;
  }): Promise<ImageRepositoryTagDetail> {
    return this.httpService
      .request({
        url:
          this.getUrlPrefix(registry_name, project_name) +
          `/${repository_name}/tags/${tag_name}`,
        method: 'GET',
        params: {
          is_third,
        },
      })
      .then((result: any) => {
        if (is_third) {
          // instance_ports
          const ports: string[] = result.ports || [];
          return {
            instance_ports: ports
              .map(port => parseInt(port, 10))
              .filter(port => !isNaN(port)),
          };
        } else {
          return this.getFormatRepositoryTagDetail(result.config);
        }
      })
      .catch(() => null);
  }

  deleteRepositoryTag({
    registry_name,
    repository_name,
    project_name,
    tag_name,
    is_third,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
    is_third?: boolean;
  }): Promise<void> {
    const params = { is_third: !!is_third };

    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) +
        '/' +
        repository_name +
        '/tags/' +
        tag_name,
      method: 'DELETE',
      params,
    });
  }

  getRepositoryTagReport(params: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
    view_type: 'cve' | 'package' | 'pipeline';
    action_type: 'summary';
    is_third?: boolean;
  }): Promise<ImageRepositoryTagReportSummary>;
  getRepositoryTagReport(params: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
    view_type: 'cve' | 'package' | 'pipeline';
    action_type: 'detail';
    page_size?: number;
    page?: number;
    is_third?: boolean;
  }): Promise<Pagination<ImageRepositoryTagReportDetail>>;
  getRepositoryTagReport({
    registry_name,
    repository_name,
    project_name,
    tag_name,
    view_type,
    action_type,
    page_size,
    page,
    is_third,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
    view_type: 'cve' | 'package' | 'pipeline';
    action_type: 'summary' | 'detail';
    page_size?: number;
    page?: number;
    is_third?: boolean;
  }): Promise<
    ImageRepositoryTagReportSummary | Pagination<ImageRepositoryTagReportDetail>
  > {
    const params = { is_third: !!is_third };

    return this.httpService.request({
      url:
        this.getUrlPrefix(registry_name, project_name) +
        '/' +
        repository_name +
        '/tags/' +
        tag_name +
        '/' +
        action_type,
      method: 'GET',
      params: {
        view_type,
        page_size,
        page,
        ...params,
      },
    });
  }

  // public repository

  getLegacyPublicRepositories(namespace: string) {
    return this.httpService
      .request({
        url: `/ajax/repositories/${namespace}`,
        method: 'GET',
      })
      .then(({ results }) => results)
      .catch(
        (): any[] => {
          return [];
        },
      );
  }

  getPublicRepositoryTags(image_name: string) {
    const [index, ...repo_names] = image_name.split('/');
    const repo_path = repo_names.join('/');
    if (!index || !repo_path) {
      return Promise.resolve([]);
    } else {
      return this.httpService
        .request({
          url: '/ajax/public_repository/tag/list',
          params: {
            index,
            repo_path,
          },
          method: 'GET',
        })
        .then(({ tags }: { tags: any }) => {
          return tags.map((item: any) => {
            return item.tag;
          });
        })
        .catch(() => []);
    }
  }

  getPublicRepositoryTagDetail({
    image_name,
    tag_name,
  }: {
    image_name: string;
    tag_name: string;
  }) {
    const [index, ...repo_names] = image_name.split('/');
    const repo_path = repo_names.join('/');
    if (!index || !repo_path) {
      return Promise.resolve(null);
    } else {
      return this.httpService
        .request({
          url: '/ajax/public_repository/tag',
          params: { index, repo_path, tag: tag_name },
          method: 'GET',
        })
        .then(({ tag: { config } }) =>
          this.getFormatRepositoryTagDetail(config),
        )
        .catch(() => null);
    }
  }

  private getFormatRepositoryTagDetail(
    tag_config: any,
  ): ImageRepositoryTagDetail {
    const config: ImageRepositoryTagDetail = {};
    if (tag_config.Cmd) {
      const run_commands: string[] = [];
      tag_config.Cmd.forEach((item: string) => {
        let cmd = item;
        if (cmd && cmd.split(' ').length > 1) {
          cmd = "'" + cmd + "'";
        }
        run_commands.push(cmd);
      });
      config.run_command = run_commands.join(' ');
    }
    if (tag_config.Entrypoint) {
      config.entrypoint = tag_config.Entrypoint.join(' ');
    }
    if (tag_config.ExposedPorts) {
      const ports = Object.keys(tag_config.ExposedPorts);
      config.instance_ports = ports.map((port: string) => {
        return parseInt(port, 10);
      });
    }
    if (tag_config.Volumes) {
      config.volumes = Object.keys(tag_config.Volumes);
    }
    config.image_envvars = [];
    if (tag_config.Env) {
      tag_config.Env.forEach((e: string) => {
        const arr = e.split('=');
        if (2 === arr.length) {
          config.image_envvars.push({
            name: arr[0],
            value: arr[1],
          });
        }
      });
    }
    return config;
  }

  getImageRepositoryTagArtifacts({
    registry_name,
    repository_name,
    project_name,
    tag_name,
  }: {
    registry_name: string;
    repository_name: string;
    project_name?: string;
    tag_name: string;
  }) {
    return this.httpService.request<ImageRepositoryTagArtifacts>({
      url:
        this.getUrlPrefix(registry_name, project_name) +
        '/' +
        repository_name +
        '/tags/' +
        tag_name +
        '/artifacts',
      method: 'GET',
    });
  }
}
