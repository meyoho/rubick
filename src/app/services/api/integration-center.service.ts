import { Inject, Injectable } from '@angular/core';

import { FetchDataResult } from 'app/abstract/pagination-data';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';

@Injectable()
export class IntegrationCenterService {
  constructor(
    @Inject(ACCOUNT) private account: Account,
    private httpService: HttpService,
    private translate: TranslateService,
  ) {}

  getIntegrationCenterCatalogEndpoint() {
    return '/ajax/integration-catalog';
  }

  getIntegrationCenterIntegrationsEndpoint() {
    return `/ajax/integrations/${this.account.namespace}/`;
  }

  getIntegrationCatalog() {
    return this.httpService.request({
      url: this.getIntegrationCenterCatalogEndpoint(),
      method: 'GET',
      params: {
        language: this.translate.currentLang,
      },
    });
  }

  getTypeByName(name: string) {
    return this.getIntegrationCatalog().then((catalog: any) => {
      return catalog.types.find((type: any) => type.name === name);
    });
  }

  getFamilies() {
    return this.getIntegrationCatalog().then(
      (catalog: any) => catalog.families,
    );
  }

  getTypes() {
    return this.getIntegrationCatalog().then((catalog: any) => catalog.types);
  }

  getIntegrationList({
    families,
    types,
    project_name,
    page = 1,
    page_size = 20,
  }: {
    families?: string;
    types?: string;
    project_name?: string;
    page?: number;
    page_size?: number;
  }) {
    return this.httpService
      .request({
        url: this.getIntegrationCenterIntegrationsEndpoint(),
        method: 'GET',
        params: {
          families,
          types,
          project_name,
          page,
          page_size,
        },
      })
      .then((res: FetchDataResult<any>) => res.results);
  }

  createIntegration(body: any) {
    return this.httpService.request({
      url: this.getIntegrationCenterIntegrationsEndpoint(),
      method: 'POST',
      body,
    });
  }

  getIntegrationById(id: string) {
    return this.httpService.request({
      url: this.getIntegrationCenterIntegrationsEndpoint() + id,
      method: 'GET',
      params: {
        project_name: this.account.namespace,
      },
    });
  }

  updateIntegration(body: any, id: string) {
    return this.httpService.request({
      url: this.getIntegrationCenterIntegrationsEndpoint() + id,
      method: 'PUT',
      body,
    });
  }

  deleteIntegrationById(id: string) {
    return this.httpService.request({
      url: this.getIntegrationCenterIntegrationsEndpoint() + id,
      method: 'DELETE',
    });
  }
}
