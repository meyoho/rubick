import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

export interface Integration {
  id: string;
  name: string;
  description: string;
  type: string;
  enabled: boolean;
  project_name: string;
  space_name: string;
  fields: { [key: string]: any };
}

@Injectable()
export class IntegrationService {
  INTEGRATIONS_URL: string;
  CATALOG_URL: string;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) private account: Account,
  ) {
    this.INTEGRATIONS_URL = `/ajax/v1/integrations/${this.account.namespace}/`;
    this.CATALOG_URL = '/ajax/integration-catalog';
  }

  getIntegrations(params: {
    families?: string;
    types?: string;
    project_name?: string;
    page?: number;
    page_size: number;
  }): Promise<Integration[]> {
    return this.httpService
      .request(this.INTEGRATIONS_URL, {
        method: 'GET',
        params,
      })
      .then(({ results }) => results);
  }

  getIntegration(ntegration_id: string) {
    return this.httpService.request({
      url: this.INTEGRATIONS_URL + ntegration_id,
      method: 'GET',
      params: {
        project_name: this.account.namespace,
      },
    });
  }
}
