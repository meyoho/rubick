import { Inject, Injectable } from '@angular/core';

import { omitBy } from 'lodash-es';

import { AccountService } from 'app/services/api/account.service';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

class JenkinsPipelines {
  constructor(private http: HttpService, private baseUrl: string) {}

  get(id: string): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/pipelines/${id}`,
      method: 'GET',
    });
  }

  find(params: any): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/pipelines`,
      method: 'GET',
      params,
    });
  }

  create(data: any) {
    return this.http.request({
      url: `${this.baseUrl}/pipelines`,
      method: 'POST',
      body: data,
    });
  }

  update(id: string, data: any) {
    return this.http.request({
      url: `${this.baseUrl}/pipelines/${id}`,
      method: 'PUT',
      body: data,
    });
  }

  delete(id: string, force: boolean = false) {
    return this.http.request({
      url: `${this.baseUrl}/pipelines/${id}`,
      method: 'DELETE',
      params: { force: force },
    });
  }
}

class JenkinsHistories {
  constructor(private http: HttpService, private baseUrl: string) {}

  get(id: string, params: { pipeline_uuid: string }) {
    return this.http.request({
      url: `${this.baseUrl}/history/${id}`,
      method: 'GET',
      params,
    });
  }

  find(params: {
    search?: string;
    pipeline_uuid: string;
    jenkins_integration_id: string;
    page: number;
    page_size: number;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history`,
      method: 'GET',
      params: omitBy(params, value => !value),
    });
  }

  start(pipeline_uuid: string, history_id?: string) {
    const body = history_id
      ? { pipeline_uuid, history_id, mode: 'replay' }
      : { pipeline_uuid };

    return this.http.request({
      url: `${this.baseUrl}/history`,
      method: 'POST',
      body,
    });
  }

  cancel(pipeline_uuid: string, history_id: string) {
    return this.http.request({
      url: `${this.baseUrl}/history/${history_id}/cancel`,
      method: 'PUT',
      params: { pipeline_uuid },
    });
  }

  delete(pipeline_uuid: string, history_id: string) {
    return this.http.request({
      url: `${this.baseUrl}/history/${history_id}`,
      method: 'DELETE',
      params: { pipeline_uuid },
    });
  }
}
class JenkinsStages {
  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: { history_id: string; pipeline_uuid: string }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages`,
      method: 'GET',
      params: { pipeline_uuid: params.pipeline_uuid },
    });
  }
}

class JenkinsSteps {
  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: {
    history_id: string;
    pipeline_uuid: string;
    stage_id: string;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages/${
        params.stage_id
      }/steps`,
      method: 'GET',
      params: { pipeline_uuid: params.pipeline_uuid },
    });
  }

  abort(params: {
    history_id: string;
    pipeline_uuid: string;
    stage_id: string;
    step_id: string;
    input_id: string;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages/${
        params.stage_id
      }/steps/${params.step_id}/abort`,
      method: 'PUT',
      params: { pipeline_uuid: params.pipeline_uuid },
      body: {
        input_id: params.input_id,
      },
    });
  }

  proceed(params: {
    history_id: string;
    pipeline_uuid: string;
    stage_id: string;
    step_id: string;
    input_id: string;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages/${
        params.stage_id
      }/steps/${params.step_id}/proceed`,
      method: 'PUT',
      params: { pipeline_uuid: params.pipeline_uuid },
      body: {
        input_id: params.input_id,
      },
    });
  }
}

class JenkinsLogs {
  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: {
    history_id: string;
    pipeline_uuid: string;
    start: number;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/logs`,
      method: 'GET',
      params: { pipeline_uuid: params.pipeline_uuid, start: params.start },
    });
  }
}

class JenkinsStepLogs {
  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: {
    history_id: string;
    pipeline_uuid: string;
    stage_id: string;
    step_id: string;
    start: number;
  }): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/stages/${
        params.stage_id
      }/steps/${params.step_id}/logs`,
      method: 'GET',
      params: { pipeline_uuid: params.pipeline_uuid, start: params.start },
    });
  }
}

class JenkinsTestResult {
  constructor(private http: HttpService, private baseUrl: string) {}

  get(params: { history_id: string; pipeline_uuid: string; status?: string }) {
    return this.http.request({
      url: `${this.baseUrl}/history/${params.history_id}/testresult`,
      method: 'GET',
      params,
    });
  }
}

class JenkinsCredentials {
  constructor(private http: HttpService, private baseUrl: string) {}

  get(id: string, jenkins_integration_id: string) {
    return this.http.request({
      url: `${this.baseUrl}/credentials/${id}`,
      method: 'GET',
      params: { jenkins_integration_id },
    });
  }

  create(data: any) {
    return this.http.request({
      url: `${this.baseUrl}/credentials`,
      method: 'POST',
      body: data,
    });
  }

  find({
    jenkins_integration_id,
  }: {
    jenkins_integration_id: string;
  }): Promise<any[]> {
    return this.http
      .request({
        url: `${this.baseUrl}/credentials`,
        method: 'GET',
        params: { jenkins_integration_id },
      })
      .then((response: any) => response.result);
  }
}

class JenkinsTemplates {
  constructor(private http: HttpService, private baseUrl: string) {}

  find(params: any): Promise<any[]> {
    return this.http
      .request({
        url: `${this.baseUrl}/templates`,
        method: 'GET',
        params: params,
      })
      .then(({ results }: any) => results);
  }

  get(uuid: string): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/templates/${uuid}`,
      method: 'GET',
    });
  }

  create(): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template`,
      method: 'POST',
    });
  }

  update(): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template`,
      method: 'POST',
    });
  }

  vars(data: any): Promise<any[]> {
    return this.http
      .request({
        url: `${this.baseUrl}/global_vars`,
        method: 'POST',
        body: data,
      })
      .then(({ result }: any) => result);
  }

  sources(params: any): Promise<any[]> {
    return this.http
      .request({
        url: `${this.baseUrl}/template_sources`,
        method: 'GET',
        params: params,
      })
      .then(({ result }) => result);
  }

  syncSettingsCreate(data: any): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template_sources`,
      method: 'POST',
      body: data,
    });
  }
  syncSettingsUpdate(data: any): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template_sources/${data.template_source_uuid}`,
      method: 'PUT',
      body: data,
    });
  }

  syncInfo(template_source_uuid: string): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template_sources/${template_source_uuid}`,
      method: 'GET',
    });
  }

  refresh(template_source_uuid: string): Promise<any> {
    return this.http.request({
      url: `${this.baseUrl}/template_sources/${template_source_uuid}/refresh`,
      method: 'PUT',
    });
  }

  previewJenkinsfile(
    template_uuid: string,
    data: any,
  ): Promise<{ jenkinsfile: string }> {
    return this.http.request({
      url: `${this.baseUrl}/templates/${template_uuid}/preview_jenkinsfile`,
      method: 'POST',
      body: data,
      addNamespace: true,
    });
  }
}

@Injectable()
export class JenkinsService {
  private BASE_URL = `/ajax/jenkins_pipelines/${this.account.namespace}`;

  pipelines = new JenkinsPipelines(this.http, this.BASE_URL);
  histories = new JenkinsHistories(this.http, this.BASE_URL);
  stages = new JenkinsStages(this.http, this.BASE_URL);
  logs = new JenkinsLogs(this.http, this.BASE_URL);
  steps = new JenkinsSteps(this.http, this.BASE_URL);
  stepLogs = new JenkinsStepLogs(this.http, this.BASE_URL);
  testResult = new JenkinsTestResult(this.http, this.BASE_URL);
  credentials = new JenkinsCredentials(this.http, this.BASE_URL);
  templates = new JenkinsTemplates(this.http, this.BASE_URL);

  public isUserView(): boolean {
    return this.accountService.isUserView();
  }

  constructor(
    private http: HttpService,
    @Inject(ACCOUNT) private account: Account,
    private accountService: AccountService,
  ) {}
}
