import { NotificationService } from '@alauda/ui';
import { Inject, Injectable } from '@angular/core';

import { keyBy, mapValues } from 'lodash-es';

import { MirrorCluster, RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { HttpService } from 'app/services/http.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import {
  Environments,
  K8sResourceWithActions,
  KubernetesResource,
  StringMap,
  TopologyResponse,
} from 'app/typings';
import { BatchReponse, ResourceList } from 'app_user/core/types';

export interface BatchRequest {
  cluster: string;
  payload: KubernetesResource;
}
export interface MirrorResource {
  uuid: string;
  name: string;
  cluster_uuid: string;
  cluster_name: string;
  cluster_display_name: string;
  namespace_name: string;
}

@Injectable()
export class K8sResourceService {
  CLUSTERS_ENDPOINT = '/ajax/v2/kubernetes/clusters/';
  CLUSTERS_ENDPOINT_SP = '/ajax-sp/v2/kubernetes/clusters/';
  CLUSTERS_ENDPOINT_MISC = '/ajax/v2/misc/clusters/';

  constructor(
    private httpService: HttpService,
    private regionService: RegionService,
    private notificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private translateService: TranslateService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  reourceBatchNotification(translate: string, res: BatchReponse, name: string) {
    Object.entries(res.responses).forEach(([key, data]) => {
      if (data.code >= 300) {
        const body = JSON.parse(data.body);
        const error = this.errorsToastService.parseErrors({
          errors: body.errors,
        });
        const title = this.translateService.get(`${translate}_error`, {
          name: name,
          clusterName: key,
        });
        this.notificationService.error({
          content: `${title}:${error.content}`,
        });
      } else {
        this.notificationService.success({
          content: this.translateService.get(`${translate}_success`, {
            name: name,
            clusterName: key,
          }),
        });
      }
    });
  }

  currentClusterResponseStatus(res: BatchReponse, cluster: string) {
    if (res.responses[cluster] && res.responses[cluster].code >= 300) {
      return Promise.reject();
    }
    return Promise.resolve();
  }

  async getMirrorCluster(clusterName: string): Promise<MirrorCluster[]> {
    let mirrorClusters: MirrorCluster[] = [];
    const cluster = await this.regionService.getCluster(clusterName);
    if (cluster.mirror && cluster.mirror.regions) {
      mirrorClusters = cluster.mirror.regions;
    }
    return mirrorClusters;
  }

  async getMirrorResources(
    clusters: string[],
    namespace: string,
    resourceType: string,
    resourceName: string,
  ): Promise<BatchReponse | any> {
    const project_name = window.sessionStorage.getItem('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/${resourceType}/${namespace}/${resourceName}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'GET',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, item => {
            return item.cluster;
          }),
          item => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  async getMirrorResourceList(
    clusterName: string,
    namespace: string,
    resourceType: string,
    resourceName: string,
  ): Promise<MirrorResource[]> {
    const mirrorResource: MirrorResource[] = [];
    const mirrorCluster = await this.getMirrorCluster(clusterName);
    if (mirrorCluster.length) {
      const clusters = mirrorCluster.map(item => {
        return item.name;
      });
      const res: BatchReponse = await this.getMirrorResources(
        clusters,
        namespace,
        resourceType,
        resourceName,
      );
      Object.entries(res.responses).forEach(([id, data]) => {
        if (data.code < 300) {
          const body = JSON.parse(data.body);
          const cluster = mirrorCluster.find(cluster => {
            return cluster.name === id;
          });
          const metadata = body.kubernetes.metadata;
          mirrorResource.push({
            uuid: metadata.uid,
            name: metadata.name,
            namespace_name: metadata.namespace,
            cluster_uuid: cluster ? cluster.id : '',
            cluster_name: cluster ? cluster.name : '',
            cluster_display_name: cluster ? cluster.display_name : '',
          });
        }
      });
    }
    return mirrorResource;
  }

  async getAllNamespaceResourceList(
    resourceType: string,
    params: {
      clusterName: string;
      namespaceList: string[];
    },
  ): Promise<BatchReponse | any> {
    const requests = params.namespaceList.map((namespace: string) => {
      return {
        namespace,
        url: `/v2/kubernetes/clusters/${
          params.clusterName
        }/${resourceType}/${namespace}/`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'GET',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, item => {
            return item.namespace;
          }),
          item => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  getK8sResources<T extends KubernetesResource = KubernetesResource>(
    resourceType: string,
    parmas: {
      clusterName: string;
      namespace?: string;
      name?: string;
      labelSelector?: string;
    },
  ): Promise<K8sResourceWithActions<T>[]> {
    return this.httpService
      .request(
        this.CLUSTERS_ENDPOINT +
          parmas.clusterName +
          `/${resourceType}/` +
          (parmas.namespace ? parmas.namespace + '/' : ''),
        {
          method: 'GET',
          params: {
            name: parmas.name,
            labelSelector: parmas.labelSelector,
          },
        },
      )
      .then((res: any) => res.result)
      .catch(() => []);
  }

  getK8sResourcesPaged(
    resourceType: string,
    parmas: {
      clusterName: string;
      namespace?: string;
      page?: number;
      page_size?: number;
      name?: string;
      labelSelector?: string;
      fieldSelector?: string;
    },
  ): Promise<ResourceList> {
    return this.httpService.request(
      this.CLUSTERS_ENDPOINT +
        parmas.clusterName +
        `/${resourceType}/` +
        (parmas.namespace ? parmas.namespace + '/' : ''),
      {
        method: 'GET',
        params: {
          name: parmas.name,
          page: parmas.page,
          page_size: parmas.page_size,
          labelSelector: parmas.labelSelector,
          fieldSelector: parmas.fieldSelector,
        },
      },
    );
  }

  getK8sReourceDetail<T extends KubernetesResource = KubernetesResource>(
    resourceType: string,
    parmas: {
      name: string;
      clusterName: string;
      namespace?: string;
    },
  ): Promise<K8sResourceWithActions<T>> {
    const namespacePart = parmas.namespace ? `${parmas.namespace}/` : '';
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${
        parmas.clusterName
      }/${resourceType}/${namespacePart}${parmas.name}`,
      {
        method: 'GET',
      },
    );
  }

  startWorkload(
    resourceType: string,
    params: {
      name: string;
      clusterName: string;
      namespace: string;
    },
  ) {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT_MISC}${
        params.clusterName
      }/workloads/${resourceType.toLocaleLowerCase()}s/${params.namespace}/${
        params.name
      }/start`,
      {
        method: 'PUT',
        ignoreProject: true,
      },
    );
  }

  stopWorkload(
    resourceType: string,
    params: {
      name: string;
      clusterName: string;
      namespace: string;
    },
  ) {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT_MISC}${
        params.clusterName
      }/workloads/${resourceType.toLocaleLowerCase()}s/${params.namespace}/${
        params.name
      }/stop`,
      {
        method: 'PUT',
        ignoreProject: true,
      },
    );
  }

  createK8sReource<T extends KubernetesResource>(
    clusterName: string,
    resourceType: string,
    payload: T,
  ): Promise<K8sResourceWithActions<T>> {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${clusterName}/${resourceType}/`,
      {
        method: 'POST',
        body: payload,
      },
    );
  }

  createK8sReourceBatch<T extends KubernetesResource>(
    clusters: string[],
    resourceType: string,
    payload: T,
  ): Promise<BatchReponse> {
    const project_name = window.sessionStorage.getItem('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/${resourceType}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(payload),
        },
        requests: mapValues(
          keyBy(requests, item => {
            return item.cluster;
          }),
          item => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  updateK8sReource<T extends KubernetesResource>(
    clusterName: string,
    resourceType: string,
    payload: T,
  ): Promise<K8sResourceWithActions<T>> {
    const namespacePart = payload.metadata.namespace
      ? `${payload.metadata.namespace}/`
      : '';
    return this.httpService.request({
      method: 'PUT',
      url: `${
        this.CLUSTERS_ENDPOINT
      }${clusterName}/${resourceType}/${namespacePart}${payload.metadata.name}`,
      body: payload,
    });
  }

  updateK8sReourceWithPatch<T extends KubernetesResource>(
    clusterName: string,
    resourceType: string,
    payload: T,
  ): Promise<BatchReponse> {
    const namespacePart = payload.metadata.namespace
      ? `${payload.metadata.namespace}/`
      : '';
    return this.httpService.request({
      method: 'PATCH',
      url: `${
        this.CLUSTERS_ENDPOINT_SP
      }${clusterName}/${resourceType}/${namespacePart}${payload.metadata.name}`,
      body: payload,
    });
  }

  updateK8sReourceBatch<T extends KubernetesResource>(
    clusters: string[],
    resourceType: string,
    payload: T,
  ): Promise<BatchReponse> {
    const project_name = window.sessionStorage.getItem('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/${resourceType}/${
          payload.metadata.namespace
        }/${payload.metadata.name}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'PUT',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(payload),
        },
        requests: mapValues(
          keyBy(requests, item => {
            return item.cluster;
          }),
          item => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  updateK8sReourceBatchWithMirrorBody(
    resourceType: string,
    batchRequest: BatchRequest[],
  ): Promise<BatchReponse> {
    const project_name = window.sessionStorage.getItem('project');
    const requests = batchRequest.map((req: BatchRequest) => {
      return {
        cluster: req.cluster,
        url: `/v2/kubernetes/clusters/${req.cluster}/${resourceType}/${
          req.payload.metadata.namespace
        }/${req.payload.metadata.name}?project_name=${project_name}`,
        payload: req.payload,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'PUT',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, item => {
            return item.cluster;
          }),
          item => {
            return {
              url: item.url,
              body: JSON.stringify(item.payload),
            };
          },
        ),
      },
    });
  }

  deleteK8sReource<T extends KubernetesResource>(
    clusterName: string,
    resourceType: string,
    payload: T,
  ) {
    const namespacePart = payload.metadata.namespace
      ? `${payload.metadata.namespace}/`
      : '';
    return this.httpService.request(
      `${
        this.CLUSTERS_ENDPOINT
      }${clusterName}/${resourceType}/${namespacePart}${payload.metadata.name}`,
      {
        method: 'DELETE',
      },
    );
  }

  deleteK8sReourceBatch<T extends KubernetesResource>(
    clusters: string[],
    resourceType: string,
    payload: T,
  ): Promise<BatchReponse> {
    const project_name = window.sessionStorage.getItem('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/${resourceType}/${
          payload.metadata.namespace
        }/${payload.metadata.name}?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, item => {
            return item.cluster;
          }),
          item => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  getK8sTopologyByResource(params: {
    cluster: string;
    namespace: string;
    name: string;
    kind: string;
  }): Promise<TopologyResponse> {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${params.cluster}/topology/${
        params.namespace
      }/${params.kind}/${params.name}`,
      {
        method: 'GET',
        ignoreProject: true,
      },
    );
  }

  createK8sReources<T extends KubernetesResource>(
    clusterName: string,
    clusters: string[],
    payload: T[],
  ) {
    if (clusters && clusters.length) {
      return this.createK8sReourcesBatch(clusters, payload);
    }
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT}${clusterName}/resources`,
      {
        method: 'POST',
        body: payload,
      },
    );
  }

  createK8sReourcesBatch<T extends KubernetesResource>(
    clusters: string[],
    payload: T[],
  ) {
    const project_name = window.sessionStorage.getItem('project');
    const requests = clusters.map((cluster: string) => {
      return {
        cluster,
        url: `/v2/kubernetes/clusters/${cluster}/resources?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(payload),
        },
        requests: mapValues(
          keyBy(requests, item => {
            return item.cluster;
          }),
          item => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  getUserAndBuiltInLabelsOrAnnotations(data: StringMap) {
    return Object.keys(data).reduce(
      (acc, key) => {
        return key.includes(this.environments.label_base_domain)
          ? {
              ...acc,
              builtIn: {
                ...acc.builtIn,
                [key]: data[key],
              },
            }
          : {
              ...acc,
              user: {
                ...acc.user,
                [key]: data[key],
              },
            };
      },
      {
        builtIn: {},
        user: {},
      },
    );
  }

  /**
   * misc API for kubernetes cluster
   * @docs http://internal-api-doc.alauda.cn/jakiro3.html#misc_clusters__cluster_name__clusters__action_type___resource_type___namespace___name__patch
   */
  updateResourcerLabelsOrAnnotations(params: {
    cluster: string;
    actionType: 'label' | 'annotate';
    resourceType: string;
    name: string;
    namespace?: string;
    payload: StringMap;
  }) {
    const url = params.namespace
      ? `${this.CLUSTERS_ENDPOINT_MISC}${params.cluster}/${params.actionType}/${
          params.resourceType
        }/${params.namespace}/${params.name}`
      : `${this.CLUSTERS_ENDPOINT_MISC}${params.cluster}/${params.actionType}/${
          params.resourceType
        }/${params.name}`;
    return this.httpService.request(url, {
      method: 'PATCH',
      body: params.payload,
    });
  }

  manualExecJob(params: { cluster: string; namespace: string; name: string }) {
    return this.httpService.request(
      `${this.CLUSTERS_ENDPOINT_MISC}${params.cluster}/manual/cronjobs/${
        params.namespace
      }/${params.name}`,
      {
        method: 'POST',
      },
    );
  }
}
