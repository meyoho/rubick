import { Inject, Injectable } from '@angular/core';

import { map, mergeWith, reduce } from 'lodash-es';
import moment from 'moment';

import { TIME_STAMP_OPTIONS } from 'app/features-shared/log/log-constant';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

export interface SearchParam {
  end_time: number;
  start_time: number;
  pageno: number;
  size: number;
  paths: string;
  read_log_source_uuid: string;
  clusters?: string;
  instances?: string;
  nodes?: string;
  query_string?: string;
  services?: string;
  allow_service_id?: boolean;
  mode?: string;
}

export interface Logs {
  logs: {
    cluster_name: string;
    instance_id: string;
    instance_id_full: string;
    message: string;
    nodes: string;
    paths: string;
    service_name: string;
    time: number;
  }[];
  total_items: number;
  total_page: number;
}

export interface Types {
  clusters: string[];
  services: string[];
  nodes: string[];
  paths: string[];
}

export interface AggregationsQueryParams {
  start_time: number;
  end_time: number;
  clusters?: string;
  instances?: string;
  nodes?: string;
  paths?: string;
  query_string?: string;
  services?: string;
}

export interface Aggregations {
  buckets: {
    count: number;
    time: number;
  }[];
}

export interface SavedSearch {
  created_at: number;
  created_by: string;
  display_name: string;
  end_time: number;
  name: string;
  project_name: string;
  project_uuid: string;
  query_conditions: {
    query_string: Array<string>;
  };
  resource_actions: Array<string>;
  space_name: string;
  space_uuid: string;
  start_time: number;
  updated_at: number;
  used_at: number;
  uuid: string;
}

export interface SavedSearchs {
  result: SavedSearch[];
}

export interface LogQueryCondition {
  display_name: string;
  end_time: number;
  name: string;
  namespace: string;
  query_conditions: {
    cluster: string;
    nodes: string;
    services: string;
    paths: string;
    query_string: string;
    instances: string;
  };
  space_name?: string;
  start_time: number;
  uuid?: string;
}

export interface QueryCondition {
  created_at: number;
  created_by: string;
  display_name: string;
  end_time: number;
  name: string;
  project_name: string;
  project_uuid: string;
  query_conditions: QueryConditionDetail;
  resource_actions: Array<string>;
  space_name: string;
  space_uuid: string;
  start_time: number;
  updated_at: number;
  used_at: number;
  uuid: string;
}

export interface QueryConditionService {
  app_name: string;
  kube_namespace: string;
  name: string;
  region_name: string;
  space_name: string;
  uuid: string;
}

export interface QueryConditionDetail {
  cluster?: Array<string>;
  nodes?: Array<string>;
  services?: Array<string | QueryConditionService>;
  paths?: Array<string>;
  query_string?: Array<string>;
  instances?: Array<string>;
}

export interface QueryConditions {
  results: QueryCondition[];
  count: number;
  num_pages: number;
  page_size: number;
}

export interface RangeDetail {
  tags: any[];
  timeRange: {
    start_time: number;
    end_time: number;
  };
  timeRangeType: string;
}

export interface Conditions {
  search: string;
  query_string: string;
}

export interface LogChartParam {
  totalCount: number;
  buckets: any[];
}

@Injectable()
export class LogService {
  LOG_V3_URL: string;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) account: Account,
  ) {
    const namespace = account.namespace;
    this.LOG_V3_URL = `/ajax/v3/logs/${namespace}/`;
  }

  logsSearch(params: SearchParam): Promise<Logs> {
    return this.httpService.request(this.LOG_V3_URL + 'search', {
      method: 'GET',
      params,
    });
  }

  getTypes(project_name?: string): Promise<Types> {
    return this.httpService.request(this.LOG_V3_URL + 'types', {
      method: 'GET',
      params: {
        projects: project_name,
      },
    });
  }

  getAggregations(params: AggregationsQueryParams): Promise<Aggregations> {
    return this.httpService.request(this.LOG_V3_URL + 'aggregations', {
      method: 'GET',
      params,
    });
  }

  queryConditionFilter(queryCondition: QueryConditionDetail) {
    if (queryCondition.services) {
      queryCondition.services = queryCondition.services.filter(
        (service: QueryConditionService) => {
          return service.uuid;
        },
      );
      if (queryCondition.services.length === 0) {
        delete queryCondition.services;
      }
    }
    return queryCondition;
  }

  generateQuerysFromTagObjects(tags: any) {
    const tagMap = map(tags, (tag: any) => {
      // from[{type:service,name:'a',uuid:'x'}, {type:node,name:'b'}, 'search string'] to [{services:a},{nodes:b},{search:c}]
      const param = {};
      if (typeof tag === 'string') {
        const regex = /(\w+): (.+)/;
        if (regex.test(tag)) {
          const regArr = regex.exec(tag);
          param[regArr[1] + 's'] = regArr[2];
        } else {
          param['search'] = tag;
        }
      } else {
        param[tag.type + 's'] = tag.name;
      }
      return param;
    });
    const querys = reduce(tagMap, (result, param) =>
      mergeWith(result, param, (pre, cur) => {
        // from [{services:a},{services:b},{clusters:c},{clusters:d},{search:e}] to {services:'a,b',clusters:'c,d',search:'e'}
        if (pre) {
          return [pre, cur].join(',');
        } else {
          return cur;
        }
      }),
    );
    return querys;
  }

  getTimeRangeByRangeType(type: string) {
    let start_time;
    const end_time = new Date().getTime();
    const offset =
      TIME_STAMP_OPTIONS.find(option => option.type === type).offset ||
      TIME_STAMP_OPTIONS[0]['offset'];
    if (offset > 7 * 24 * 3600 * 1000) {
      const endDate = new Date(end_time);
      start_time =
        new Date(
          endDate.getFullYear(),
          endDate.getMonth(),
          endDate.getDate(),
        ).getTime() -
        offset +
        24 * 3600 * 1000;
    } else {
      start_time = end_time - offset;
    }
    return {
      start_time,
      end_time,
    };
  }

  dateNumToStr(num: number) {
    return moment(num).format('YYYY-MM-DD HH:mm:ss');
  }

  dateStrToNum(str: string) {
    return moment(str, 'YYYY-MM-DD HH:mm:ss').valueOf();
  }
}
