import { Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';

export interface ChartData {
  name: string;
  series: { value: number; name: string }[];
}

export interface Metric {
  metric: {
    container_name?: string;
    pod_name?: string;
    __query_id__?: string;
    metric_name?: string;
    unit?: string;
    instance?: string;
  };
  values?: Array<string | number>[];
  value?: Array<number>;
}

export interface MonitorSelectionParams {
  mode: string;
  aggregator: string;
  start: number;
  end: number;
}

export interface MetricQuery {
  start?: number;
  end?: number;
  step?: number;
  time?: number;
  queries?: MetricQueries[];
}

export interface MetricQueries {
  aggregator: string;
  level?: string;
  range?: number;
  labels?: MetricLabels[];
  group_by?: string;
  id?: string;
}

export interface MetricLabels {
  type: string;
  name: string;
  value: string;
}

export interface YAxisScale {
  yScaleMin: number;
  yScaleMax: number;
}

export interface IndicatorType {
  kind: string;
  name: string;
  unit: string;
  string?: string;
  value_type?: string;
  type?: string;
}

@Injectable()
export class MetricService {
  METRICS_URL: string;

  constructor(private httpService: HttpService) {
    this.METRICS_URL = '/ajax/v1/metrics/';
  }

  getIndicators(cluster = 'global'): Promise<IndicatorType[]> {
    const endpoint = `${this.METRICS_URL}${cluster}/indicators`;
    return this.httpService
      .request(endpoint, {
        method: 'GET',
      })
      .then(({ result }) =>
        result.sort((a: IndicatorType, b: IndicatorType) =>
          ('' + a.name).localeCompare(b.name),
        ),
      );
  }

  queryMetric(cluster_name: string, params: MetricQuery): Promise<Metric[]> {
    return this.httpService
      .request(`${this.METRICS_URL}${cluster_name}/query`, {
        method: 'POST',
        body: params,
      })
      .then(({ result }) => result);
  }

  queryMetrics(cluster_name: string, params: MetricQuery): Promise<Metric[]> {
    return this.httpService
      .request(`${this.METRICS_URL}${cluster_name}/query_range`, {
        method: 'POST',
        body: params,
      })
      .then(({ result }) => result);
  }

  getPrometheusMetrics(cluster_name: string): Promise<any> {
    return this.httpService
      .request(
        `${this.METRICS_URL}${cluster_name}/prometheus/label/__name__/values`,
        {
          method: 'GET',
        },
      )
      .then(({ data }) => data);
  }

  getPrometheusMetricLabels(
    cluster_name: string,
    query: string,
  ): Promise<any[]> {
    return this.httpService
      .request(`${this.METRICS_URL}${cluster_name}/prometheus/query`, {
        method: 'GET',
        params: {
          query,
          time: Math.floor(Date.now() / 1000),
        },
      })
      .then(({ data }) => data.result)
      .catch(err => err);
  }

  fillUpResult(
    values: Array<number | string>[],
    end_time: number,
    step: number,
  ) {
    // fill in the chart when result is less than 30 points
    let dateNewest = end_time;
    const obj = {};
    for (let i = 0; i < 30; i++) {
      obj[(dateNewest -= step)] = '';
    }
    if (values.length > 0) {
      values.forEach((element: Array<number | string>) => {
        obj[element[0]] = element[1];
      });
    }
    return Object.keys(obj).map(function(key) {
      return [Number(key), obj[key]];
    });
  }
}
