import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';

export interface MetricQuery {
  agg: string;
  name: string;
  where?: object;
  by?: string[];
}

export interface MetricQueryParams {
  qs: MetricQuery[];
  start: number;
  end: number;
}

export interface Metric {
  metric_type: string;
  metric_name: string;
  unit: string;
  dps: object;
  tags: object;
}

@Injectable()
export class MonitorService {
  METRICS_URL: string;

  constructor(
    private httpService: HttpService,
    private translateService: TranslateService,
    @Inject(ACCOUNT) account: Account,
  ) {
    const namespace = account.namespace;
    this.METRICS_URL = `/ajax/v2/monitor/${namespace}/metrics/`;
  }

  private convertMetricQuery({
    agg,
    name,
    where = {},
    by = [],
  }: MetricQuery): string {
    const whereStr = Object.keys(where)
      .map(key => `${key}=${where[key]}`)
      .join(',');
    return `${agg}:${name}{${whereStr}}by{${by.join(',')}}`;
  }

  queryMetrics({ qs, start, end }: MetricQueryParams): Promise<Metric[]> {
    const qString = qs
      .map(q => `q=${encodeURIComponent(this.convertMetricQuery(q))}`)
      .join('&');
    const queryString = `?${qString}&start=${start}&end=${end}`;
    return this.httpService
      .request(this.METRICS_URL + 'query' + queryString, {
        method: 'GET',
      })
      .then(({ result }: { result: Metric[] }) => {
        if (result.length) {
          return result;
        } else {
          // tslint:disable-next-line
          throw 'Empty Response';
        }
      });
  }

  getPeriodOptions(): any[] {
    const hour = 60 * 60 * 1000;
    return [
      {
        title: this.translateService.get('chart_half_hour'),
        period: 0.5 * hour,
      },
      {
        title: this.translateService.get('chart_one_hour'),
        period: hour,
      },
      {
        title: this.translateService.get('chart_six_hour'),
        period: 6 * hour,
      },
      {
        title: this.translateService.get('chart_one_day'),
        period: 24 * hour,
      },
      {
        title: this.translateService.get('chart_three_day'),
        period: 3 * 24 * hour,
      },
      {
        title: this.translateService.get('chart_one_week'),
        period: 7 * 24 * hour,
      },
      {
        title: this.translateService.get('chart_two_week'),
        period: 14 * 24 * hour,
      },
      {
        title: this.translateService.get('custom_time_range'),
        period: -1,
      },
    ];
  }

  getMetrics() {
    return this.httpService
      .request(this.METRICS_URL, {
        method: 'GET',
        params: {
          simple: 'true',
        },
      })
      .then(({ result }) => result)
      .catch(() => []);
  }

  queryMetricsData({
    query_list,
    start,
    end,
    cache = false,
  }: {
    query_list: string[];
    start: number;
    end: number;
    cache: boolean;
  }) {
    const q = query_list
      .map((query: string) => {
        return `q=${encodeURIComponent(query)}`;
      })
      .join('&');
    const url = `${this.METRICS_URL}query?${q}&start=${start}&end=${end}`;
    return this.httpService.request(url, {
      method: 'GET',
      cache,
    });
  }

  getMetricsDetail({ metrics = '', cache = false }) {
    return this.httpService
      .request(this.METRICS_URL, {
        method: 'GET',
        params: {
          metrics,
          window: 86400,
        },
        cache,
      })
      .then(({ result }) => result[0])
      .catch(() => {
        return null;
      });
  }
}
