import { Injectable } from '@angular/core';

import { flattenDeep, isInteger, keys, max, min, uniqWith } from 'lodash-es';
import moment from 'moment';

import { MonitorService } from 'app/services/api/monitor.service';
import { TranslateService } from 'app/translate/translate.service';
import { BytesFormatterPipe } from 'app2/shared/pipes/bytes-formatter.pipe';

export const TRANSLATE_METRIC_TAG_KEYS = [
  'region_name',
  'app_name',
  'service_name',
  'instance_id',
  'host',
];

@Injectable()
export class MonitorUtilitiesService {
  filteredTagsKey = ['region_id'];

  constructor(
    private translate: TranslateService,
    private monitorService: MonitorService,
  ) {}

  getMonitorMetricOptions(metrics: any) {
    const options: any[] = [];
    metrics.forEach((item: string) => {
      options.push({
        // currently they have the same value
        metric_name: item,
        display_name: item,
        // tags: item.tags
      });
    });
    return options;
  }

  getMonitorSourceOptions(metric: any) {
    const options: any[] = [];
    const tags = metric.tags || {};
    for (const key in tags) {
      if (!['region_id', 'instance_id'].includes(key)) {
        const tagItem = tags[key];
        tagItem.forEach((item: string) => {
          const display_name = this.getMetricTagItemDisplayName(key, item);
          const display_key = TRANSLATE_METRIC_TAG_KEYS.includes(key)
            ? this.translate.get(`metric_tag_${key}`)
            : key;
          options.push({
            name: `${display_key}:${display_name}`,
            value: `${key}=${item}`,
          });
        });
      }
    }
    return options;
  }

  getMetricTagItemDisplayName(key: string, item: string) {
    let display_name = item;
    if (key === 'service_name') {
      const arr = item.split('.');
      if (arr.length === 3) {
        // space_name.app_name.service_name
        const [space_name, app_name, service_name] = arr;
        if (app_name === '#') {
          display_name = `${service_name} (${space_name})`;
        } else {
          display_name = `${app_name}.${service_name} (${space_name})`;
        }
      }
    } else if (key === 'app_name') {
      const arr = item.split('.');
      // TODO: space_name.#.app_name is not a valid pattern , wati for API udpate
      if (arr.length === 3) {
        // space_name.app_name
        const [space_name, , app_name] = arr;
        if (space_name === '#') {
          display_name = app_name;
        } else {
          display_name = `${app_name} (${space_name})`;
        }
      }
    } else if (key === 'instance_id') {
      display_name = item.length > 16 ? item.substring(0, 16) : item;
    }
    return display_name;
  }

  getMonitorGroupOption(metric: any, emptyIncluded = false) {
    const options = [];
    if (emptyIncluded) {
      options.push({
        name: this.translate.get('monitor_group_placeholder'),
        value: '',
      });
    }
    const tags = metric.tags || {};
    for (const key in tags) {
      if (!this.filteredTagsKey.includes(key)) {
        const display_name = TRANSLATE_METRIC_TAG_KEYS.includes(key)
          ? this.translate.get(`metric_tag_${key}`)
          : key;
        options.push({
          name: display_name,
          value: key,
        });
      }
    }
    return options;
  }

  getMonitorAggregatorOptions() {
    return [
      {
        name: this.translate.get('aggregator_avg'),
        value: 'avg',
      },
      {
        name: this.translate.get('aggregator_sum'),
        value: 'sum',
      },
      {
        name: this.translate.get('aggregator_min'),
        value: 'min',
      },
      {
        name: this.translate.get('aggregator_max'),
        value: 'max',
      },
    ];
  }

  getChartData({
    query_list,
    metrics,
    timeRange,
    seriesName = '',
    cache = false,
  }: {
    query_list?: string[];
    metrics?: any[];
    timeRange: any;
    seriesName?: string;
    cache?: boolean;
  }) {
    if (!query_list) {
      query_list = this.getMetricsQueryList(metrics);
    }
    return this.monitorService
      .queryMetricsData({
        query_list,
        start: timeRange.start,
        end: timeRange.end,
        cache,
      })
      .then(({ result }: any) => {
        if (result && result.length) {
          return this.parseMetricsToSeries({
            metrics: result,
            seriesName,
          });
        } else {
          return {
            data: [],
          };
        }
      })
      .catch(() => {
        return {
          data: [],
        };
      });
  }

  getMetricsQueryList(metrics: any[]) {
    // agg:metric_name{where1,where2}exclude{exclude1,exclude2}by{group_by}
    const query_list: string[] = [];
    metrics.forEach(item => {
      let q;
      if (item.exclude) {
        q = `${item.aggregator}:${item.metric}{${item.over || ''}}exclude{${
          item.exclude
        }}by{${item.group_by || ''}}`;
      } else {
        q = `${item.aggregator}:${item.metric}{${item.over ||
          ''}}by{${item.group_by || ''}}`;
      }
      query_list.push(q);
    });
    return query_list;
  }

  private parseMetricsToSeries({
    metrics,
    seriesName,
  }: {
    metrics: any[];
    seriesName: string;
  }) {
    const times = keys(metrics[0].dps);
    const data = metrics.map((result, index) => {
      let series: any[] = [];
      const dps = result.dps;
      series = times.map((time: string) => {
        return {
          value: dps[time] === null ? 0 : dps[time],
          name: time,
        };
      });

      const name = seriesName
        ? `${seriesName}.${index}`
        : this.getChartNameFromTags(result);
      return {
        name,
        tags: result.tags,
        unit: result.unit,
        metric_name: result.metric_name,
        series,
      };
    });
    const axisOptions = this.computeAxisOptions(data);
    return {
      data,
      ...axisOptions,
    };
  }

  private computeAxisOptions(data: any[]) {
    const ret: any = {};
    ret.xAxisTickFormatting = (tick: string) => {
      return moment(parseInt(tick, 10) * 1000).format('MM-DD HH:mm');
    };
    const { yScaleMin, yScaleMax } = this.getYAxisValueScale(data);
    ret.yScaleMin = yScaleMin;
    ret.yScaleMax = yScaleMax;
    const uniqData = uniqWith(data, (a: any, b: any) => {
      return a.unit === b.unit;
    });
    if (uniqData.length) {
      const unit = uniqData[0].unit;
      ret.yAxisTickFormatting = (tick: string) => {
        return this.pointFormatter(tick, unit, 2);
      };
    }
    return ret;
  }

  private pointFormatter(value: any, unit: string, precision = 4) {
    let data = value;
    // format data when unit is 'Percent' or 'Bytes'
    if (unit === 'Percent') {
      data = data === 0 ? 0 : (value * 100).toPrecision(precision + 1) + '%';
    } else if (unit === 'Percent__real') {
      data = data === 0 ? 0 : value.toPrecision(precision + 1) + '%';
    } else if (['Bytes', 'Bytes/Second'].includes(unit)) {
      const bytesFormatter = new BytesFormatterPipe();
      data = bytesFormatter.transform(value, precision - 1);
    } else {
      data = isInteger(value)
        ? value.toLocaleString()
        : value.toFixed(precision);
    }
    return data;
  }

  getYAxisValueScale(data: any[]) {
    const values: number[] = flattenDeep(
      data.map((item: any) => {
        return item.series.map((point: any) => Number(point.value));
      }),
    );
    const yScaleMin = min(values);
    const yScaleMax = max(values);
    const scale = { yScaleMin, yScaleMax };
    if (yScaleMin === 0) {
      scale.yScaleMin = 0;
    } else if (yScaleMin > 0) {
      scale.yScaleMin = yScaleMin * 0.8;
    } else {
      scale.yScaleMin = yScaleMin * 1.2;
    }
    if (yScaleMax === 0) {
      scale.yScaleMax = 1;
    } else if (yScaleMax > 0) {
      scale.yScaleMax = yScaleMax * 1.2;
    } else {
      scale.yScaleMax = yScaleMax * 0.8;
    }
    return scale;
  }

  private getChartNameFromTags(result: any) {
    const tags = result.tags;
    const name = Object.keys(tags)
      .filter(key => {
        return !this.filteredTagsKey.includes(key);
      })
      .sort(a => {
        if (a === 'instance_id') {
          return 1;
        }
      })
      .map(key => {
        const displayKey = TRANSLATE_METRIC_TAG_KEYS.includes(key)
          ? this.translate.get(`metric_tag_${key}`)
          : key;
        const tagDisplayName = this.getMetricTagItemDisplayName(key, tags[key]);
        return `${displayKey}=${tagDisplayName}`;
      })
      .join(',');
    return name;
  }
}
