import { Inject, Injectable } from '@angular/core';

import { keyBy, mapValues } from 'lodash-es';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT, ENVIRONMENTS } from 'app/shared/tokens';
import { Account, Environments } from 'app/typings';
import { Pagination } from 'app/typings/backend-api';
import { StringMap } from 'app/typings/raw-k8s';
import {
  LimitRange as K8sLimitRange,
  Namespace as K8sNamespace,
  ResourceQuota as K8sResourceQuota,
} from 'app/typings/raw-k8s';
import { BatchApi, BatchReponse } from 'app_user/core/types';
import {
  MixinQuota,
  PodQuota,
} from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';
import { Project } from './project.service';

export interface Namespace {
  kubernetes: K8sNamespace;
  resource_actions: string[];
}

export interface QuotaStatusModel {
  pods: string;
  'requests.cpu': string;
  'requests.storage': string;
  persistentvolumeclaims: string;
  'requests.memory': string;
}

export interface ResourceQuota {
  resource_actions: string[];
  kubernetes: {
    status: {
      hard: QuotaStatusModel;
      used: QuotaStatusModel;
    };
    kind: 'ResourceQuota';
    spec: {
      hard: QuotaStatusModel;
    };
    apiVersion: string;
    metadata: {
      creationTimestamp: string;
      name: string;
      resourceVersion: string;
      selfLink: string;
      uid: string;
    };
  };
}

export interface NamespaceOption {
  name: string;
  uuid: string;
}

export interface UserNamespace {
  name: string;
  uuid?: string;
  cluster_name: string;
  cluster_display_name?: string;
}

export interface LimitRange {
  default: {
    memory: string;
    cpu: string;
  };
  defaultRequest: {
    memory: string;
    cpu: string;
  };
  max: {
    cpu: string;
    memory: string;
  };
  type: string;
}

export interface CreateNamespaceRes {
  namespace?: K8sNamespace;
  resourcequota?: K8sResourceQuota;
  limitrange?: K8sLimitRange;
}

@Injectable()
export class NamespaceService {
  CLUSTERS_URL = '/ajax/v2/kubernetes/clusters/';
  private PASS_V2_URL = '/ajax/v2/paas/';
  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) private account: Account,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  getNamespace(cluster: string, name: string) {
    return this.httpService.request<Namespace>({
      method: 'GET',
      url: this.CLUSTERS_URL + cluster + '/namespaces/' + name,
    });
  }

  getNamespaces(
    cluster: string,
    params?: {
      project_name?: string;
      name?: string;
      page?: number;
      page_size?: number;
    },
  ): Promise<Namespace[] | Pagination<Namespace>> {
    return this.httpService
      .request({
        method: 'GET',
        url: this.CLUSTERS_URL + cluster + '/namespaces',
        params,
        ignoreProject: true,
      })
      .then((res: any) => (res.results ? res : res.result));
  }

  getNamespaceOptions(
    cluster: string,
    params?: {
      project_name?: string;
      name?: string;
      page?: number;
      page_size?: number;
    },
  ): Promise<NamespaceOption[]> {
    return this.getNamespaces(cluster, params)
      .then((results: Namespace[]) =>
        results.map(({ kubernetes: { metadata } }) => ({
          name: metadata.name,
          uuid: `${this.account.namespace}:${cluster}:${metadata.name}`,
        })),
      )
      .catch(() => []);
  }

  getNamespacesBatch(
    clusters: string[],
    project?: string,
  ): Promise<{
    [cluster: string]: Namespace[];
  }> {
    const requests = clusters.reduce((requests, cluster: string) => {
      requests[cluster] = {
        url: `/v2/kubernetes/clusters/${cluster}/namespaces?project_name=${project ||
          ''}`,
      };
      return requests;
    }, {});

    return this.httpService
      .request({
        method: 'POST',
        url: '/ajax/v2/batch/',
        body: {
          common: {
            method: 'GET',
            header: {
              'Content-Type': 'application/json',
            },
          },
          requests,
        },
      })
      .then((res: BatchReponse) => {
        return Object.entries(res.responses).reduce((obj, [cluster, data]) => {
          if (data.code < 300) {
            obj[cluster] = JSON.parse(data.body) as Namespace[];
          }
          return obj;
        }, {});
      })
      .catch(_e => null);
  }

  getBatchNamespaceOptions(
    clusters: {
      name: string;
      uuid: string;
      display_name: string;
    }[] = [],
    project_name: string,
  ): Promise<UserNamespace[]> {
    const requests = clusters.map(
      (cluster: { name: string; uuid: string; display_name: string }) => {
        return {
          clusterId: cluster.uuid,
          url: `/v2/kubernetes/clusters/${
            cluster.uuid
          }/namespaces?project_name=${project_name || ''}`,
        };
      },
    );
    return this.httpService
      .request({
        method: 'POST',
        url: '/ajax/v2/batch/',
        ignoreProject: true,
        body: {
          common: {
            method: 'GET',
          },
          // cluster_uuid : request object
          requests: mapValues(
            keyBy(requests, (item: any) => {
              return item.clusterId;
            }),
            (item: any) => {
              return {
                url: item.url,
              };
            },
          ),
        },
      })
      .then(
        (res: {
          responses: {
            [key: string]: {
              body: string;
              code: number;
            };
          };
        }) => {
          let namespaces: {
            name: string;
            uuid: string;
            cluster_name: string;
            cluster_uuid: string;
            cluster_display_name: string;
          }[] = [];
          Object.keys(res.responses).forEach((clusterId: string) => {
            const response: {
              body: string;
              code: number;
            } = res.responses[clusterId];
            if (response.code === 200) {
              const data = JSON.parse(response.body);
              namespaces = namespaces.concat(
                data.map((namespace: Namespace) => {
                  const cluster = clusters.find((c: { uuid: string }) => {
                    return c.uuid === clusterId;
                  });
                  return {
                    name: namespace.kubernetes.metadata.name,
                    uuid: `${this.account.namespace}:${cluster.name}:${
                      namespace.kubernetes.metadata.name
                    }`,
                    cluster_name: cluster.name,
                    cluster_uuid: cluster.uuid,
                    cluster_display_name: cluster.display_name,
                  };
                }),
              );
            }
          });
          return namespaces;
        },
      )
      .catch(() => []);
  }

  /**
   * @deprecated
   * 用k8s-service 方法替代
   */
  getNamespaceQuota(clusterId: string, namespace: string) {
    return this.httpService.request<ResourceQuota>({
      method: 'GET',
      url:
        this.CLUSTERS_URL +
        clusterId +
        '/resourcequotas/' +
        namespace +
        '/default',
    });
  }

  /**
   * @deprecated
   * 用k8s-service 方法替代
   */
  getNamespacePodQuota(clusterId: string, namespace: string) {
    return this.httpService.request<ResourceQuota>({
      method: 'GET',
      url:
        this.CLUSTERS_URL +
        clusterId +
        '/limitranges/' +
        namespace +
        '/default',
    });
  }

  createNamespace(clusterId: string, namespace: string) {
    return this.httpService.request<Namespace>({
      method: 'POST',
      url: this.CLUSTERS_URL + clusterId + '/namespaces',
      body: {
        apiVersion: 'v1',
        kind: 'Namespace',
        metadata: {
          name: namespace,
        },
      },
    });
  }

  deleteNamespace(clusterId: string, namespace: string) {
    return this.httpService.request<Pagination<Namespace>>({
      method: 'DELETE',
      url: this.CLUSTERS_URL + clusterId + '/namespaces/' + namespace,
    });
  }

  /** @deprecated 分步ns完全上线后删除 */
  deleteNamespaceBatch(
    data: {
      clusterName: string;
      namespace: string;
      projectName: string;
      clusterDisplayName: string;
    }[],
  ): Promise<BatchApi> {
    const requests = {};
    data.forEach(v => {
      const url = `/v2/kubernetes/clusters/${
        v.clusterName
      }/general-namespaces/${v.namespace}?project_name=${v.projectName}`;
      requests[v.clusterDisplayName] = {
        url,
      };
    });

    return this.httpService.request('/ajax/v2/batch/', {
      method: 'POST',
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests,
      },
    });
  }

  deleteNamespaceBatchPaas(
    data: {
      clusterName: string;
      namespace: string;
      projectName: string;
      clusterDisplayName: string;
    }[],
  ): Promise<BatchApi> {
    const requests = {};
    data.forEach(v => {
      const url = `/v2/paas/clusters/${v.clusterName}/namespaces/${
        v.namespace
      }?project_name=${v.projectName}`;
      requests[v.clusterDisplayName] = {
        url,
      };
    });

    return this.httpService.request('/ajax/v2/batch/', {
      method: 'POST',
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests,
      },
    });
  }

  createNamespaceBatch(
    arr: {
      clusterName: string;
      clusterDisplayName: string;
      namespace: string;
      namespaceLabels?: StringMap;
      namespaceAnnotations?: StringMap;
      project: Project;
      quotaConfig: any;
      podQuotaConfig: PodQuota;
    }[],
  ): Promise<BatchApi> {
    const requests = {};
    arr.forEach(c => {
      const url = `/v2/kubernetes/clusters/${
        c.clusterName
      }/general-namespaces?project_name=${c.project.name}`;
      requests[c.clusterDisplayName] = {
        url,
        body: JSON.stringify({
          namespace: {
            apiVersion: 'v1',
            kind: 'Namespace',
            metadata: {
              name: c.namespace,
              labels: c.namespaceLabels || {},
              annotations: c.namespaceAnnotations || {},
            },
          },
          resourcequota: {
            apiVersion: 'v1',
            kind: 'ResourceQuota',
            metadata: {
              name: 'default',
              namespace: c.namespace,
            },
            spec: {
              hard: Object.assign({}, c.quotaConfig, {
                'limits.cpu': c.quotaConfig['requests.cpu'],
                'limits.memory': c.quotaConfig['requests.memory'],
              }),
            },
          },
          limitrange: {
            apiVersion: 'v1',
            kind: 'LimitRange',
            metadata: {
              name: 'default',
              namespace: c.namespace,
            },
            spec: {
              limits: [
                {
                  default: {
                    memory: c.podQuotaConfig.pod_default_memory,
                    cpu: c.podQuotaConfig.pod_default_cpu,
                  },
                  defaultRequest: {
                    memory: c.podQuotaConfig.pod_default_request_memory,
                    cpu: c.podQuotaConfig.pod_default_request_cpu,
                  },
                  max: {
                    cpu: c.podQuotaConfig.pod_max_cpu,
                    memory: c.podQuotaConfig.pod_max_memory,
                  },
                  type: 'Container',
                },
              ],
            },
          },
        }),
      };
    });

    return this.httpService.request('/ajax/v2/batch/', {
      method: 'POST',
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests,
      },
    });
  }

  updateNamespaceQuotaBatch(
    arr: {
      clusterName: string;
      namespace: string;
      clusterDisplayName: string;
      projectName: string;
      quotaConfig: MixinQuota;
      podQuotaConfig: PodQuota;
    }[],
  ): Promise<BatchApi> {
    const requests = {};
    arr.forEach(c => {
      const url = `/v2/kubernetes/clusters/${
        c.clusterName
      }/general-namespaces/${c.namespace}?project_name=${c.projectName}`;
      requests[c.clusterDisplayName] = {
        url,
        body: JSON.stringify({
          namespace: {
            apiVersion: 'v1',
            kind: 'Namespace',
            metadata: {
              name: c.namespace,
            },
          },
          resourcequota: {
            apiVersion: 'v1',
            kind: 'ResourceQuota',
            metadata: {
              name: 'default',
              namespace: c.namespace,
            },
            spec: {
              hard: Object.assign({}, c.quotaConfig, {
                'limits.cpu': c.quotaConfig['requests.cpu'],
                'limits.memory': c.quotaConfig['requests.memory'],
              }),
            },
          },
          limitrange: {
            apiVersion: 'v1',
            kind: 'LimitRange',
            metadata: {
              name: 'default',
              namespace: c.namespace,
            },
            spec: {
              limits: [
                {
                  default: {
                    memory: c.podQuotaConfig.pod_default_memory,
                    cpu: c.podQuotaConfig.pod_default_cpu,
                  },
                  defaultRequest: {
                    memory: c.podQuotaConfig.pod_default_request_memory,
                    cpu: c.podQuotaConfig.pod_default_request_cpu,
                  },
                  max: {
                    cpu: c.podQuotaConfig.pod_max_cpu,
                    memory: c.podQuotaConfig.pod_max_memory,
                  },
                  type: 'Container',
                },
              ],
            },
          },
        }),
      };
    });

    return this.httpService.request('/ajax/v2/batch/', {
      method: 'POST',
      body: {
        common: {
          method: 'PUT',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests,
      },
    });
  }

  // create namespace in steps
  getNamespaceActions(): Promise<{ pipeline: string; actions: string[] }> {
    // return Promise.resolve({
    //   pipeline: 'default',
    //   actions: [
    //     'a_serviceaccount',
    //     'a_projectlabel',
    //     'resourcequota',
    //     'a_somestep1',
    //     'a_somestep2',
    //     'limitrange',
    //     'a_somestep3',
    //   ],
    // });
    return this.httpService.request(
      `${this.PASS_V2_URL}namespace-pipelines/default`,
      {
        method: 'GET',
        addNamespace: false,
      },
    );
  }

  createNamespacePaas(params: {
    cluster: string;
    namespace: string;
    payload: any;
  }): Promise<CreateNamespaceRes> {
    return this.httpService.request(
      `${this.PASS_V2_URL}clusters/${params.cluster}/namespaces/${
        params.namespace
      }`,
      {
        method: 'POST',
        addNamespace: false,
        body: params.payload,
      },
    );
  }

  batchCreateNamespacePaas(params: {
    project: string;
    namespace: string;
    resources: {
      cluster: string;
      payload: any;
    }[];
  }): Promise<BatchReponse> {
    const requests = params.resources.reduce((requests, cur) => {
      requests[cur.cluster] = {
        url: `/v2/paas/clusters/${cur.cluster}/namespaces/${
          params.namespace
        }?project_name=${params.project}`,
        body: JSON.stringify(cur.payload),
      };
      return requests;
    }, {});
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'POST',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests,
      },
    });
  }
}
