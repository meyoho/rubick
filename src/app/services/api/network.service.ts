import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import {
  Account,
  IPIP_MODE_TYPE,
  K8sResourceWithActions,
  LoadBalancerFormModel,
  ResourceType,
  Subnet,
  SubnetIp,
} from 'app/typings';
import { ResourceList } from 'app_user/core/types';
import { RoleUtilitiesService } from './role-utilities.service';

export enum LB_TYPE {
  nginx = 'nginx',
  SLB = 'slb',
  ELB = 'elb',
  CLB = 'clb',
}

export enum ALB_RESOURCE_TYPE {
  ALB = 'alaudaloadbalancer2',
  FRONTEND = 'frontends',
  RULE = 'rules',
}

export enum MATCH_TYPE {
  AND = 'AND',
  EXIST = 'Exist',
  EQ = 'Equal',
  IN = 'In',
  OR = 'OR',
  RANGE = 'Range',
  REGEX = 'RegEx',
  STARTS_WITH = 'StartsWith',
}

export const MATCH_TYPES = {
  Exist: 'EXIST',
  Equal: 'EQ',
  In: 'IN',
  Range: 'RANGE',
  RegEx: 'REGEX',
  StartsWith: 'STARTS_WITH',
};

export enum RULE_TYPE {
  HOST = 'HOST',
  URL = 'URL',
  SRC_IP = 'SRC_IP',
  HEADER = 'HEADER',
  COOKIE = 'COOKIE',
  PARAM = 'PARAM',
}

export enum CERT_STATUS {
  NORMAL = 'Normal',
}

export interface K8sSubnetPayload {
  name: string;
  cidr: string;
  gateway: string;
  block_size: number;
  ipip_mode: IPIP_MODE_TYPE;
  nat_outgoing: boolean;
}

export interface UpdateCalicoSubnet {
  ipip_mode: IPIP_MODE_TYPE;
  nat_outgoing: boolean;
}

export interface UpdateMacvlanSubnet {
  cidr: string;
  gateway: string;
}

export interface IpImportPayload {
  address_list?: string[];
  address_range?: {
    address_start: string;
    address_end: string;
  };
}

@Injectable()
export class NetworkService {
  private K8S_SUNNET_ENDPOINT = `/ajax/v2/subnet/clusters`;
  private ALB_ENPOINT = `/ajax/v2/loadbalancers`;

  constructor(
    private httpService: HttpService,
    private roleService: RoleUtilitiesService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  canAction(
    resource: any,
    action: string,
    resourceType: ResourceType = 'load_balancer',
  ): boolean {
    return this.roleService.resourceHasPermission(
      resource,
      resourceType,
      action,
    );
  }

  getK8sSubnets(
    cluster_id: string,
    params?: {
      page: number;
      page_size: number;
      name: string;
    },
  ): Promise<ResourceList> {
    return this.httpService.request({
      method: 'GET',
      params: {
        name: params.name,
        page: params.page,
        page_size: params.page_size,
      },
      url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}`,
    });
  }

  getK8sSubnet(
    cluster_id: string,
    subnet_name: string,
  ): Promise<K8sResourceWithActions<Subnet>[]> {
    return this.httpService.request({
      method: 'GET',
      url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}/${subnet_name}`,
    });
  }

  getK8sSubnetIps(
    cluster_id: string,
    subnet_name: string,
  ): Promise<K8sResourceWithActions<SubnetIp>[]> {
    return this.httpService
      .request({
        method: 'GET',
        url: `${
          this.K8S_SUNNET_ENDPOINT
        }/${cluster_id}/${subnet_name}/private_ips`,
      })
      .then(({ result }) => result);
  }

  deleteK8sSubnet(cluster_id: string, subnet_name: string) {
    return this.httpService.request({
      method: 'DELETE',
      url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}/${subnet_name}`,
    });
  }

  createK8sSubnet(cluster_id: string, data: K8sSubnetPayload) {
    return this.httpService.request({
      method: 'POST',
      url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}`,
      body: data,
    });
  }

  importK8sSubnetIp(
    cluster_id: string,
    subnet_name: string,
    data: IpImportPayload,
  ) {
    return this.httpService.request({
      method: 'POST',
      url: `${
        this.K8S_SUNNET_ENDPOINT
      }/${cluster_id}/${subnet_name}/private_ips`,
      body: data,
    });
  }

  deleteK8sSubnetIp(cluster_id: string, subnet_name: string, ip_name: string) {
    return this.httpService.request({
      method: 'DELETE',
      url: `${
        this.K8S_SUNNET_ENDPOINT
      }/${cluster_id}/${subnet_name}/private_ips/${ip_name}`,
    });
  }

  updateK8sSubnet(
    cluster_id: string,
    subnet_name: string,
    payload: UpdateCalicoSubnet | UpdateMacvlanSubnet,
  ) {
    return this.httpService.request({
      method: 'PUT',
      url: `${this.K8S_SUNNET_ENDPOINT}/${cluster_id}/${subnet_name}`,
      body: payload,
    });
  }

  createLoadbalancer(
    clusterName: string,
    namespace: string = 'alauda-system',
    payload: LoadBalancerFormModel,
  ) {
    return this.httpService.request({
      method: 'POST',
      url: `${this.ALB_ENPOINT}/${clusterName}/${namespace}`,
      body: payload,
    });
  }

  deleteLoadbalancer(
    clusterName: string,
    namespace: string = 'alauda-system',
    albName: string,
  ) {
    return this.httpService.request({
      method: 'DELETE',
      url: `${this.ALB_ENPOINT}/${clusterName}/${namespace}/${albName}`,
    });
  }
}
