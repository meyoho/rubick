import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

export interface NotificationSubscription {
  method: string;
  recipient: string;
  secret?: string;
  remark?: string;
}
export interface NotificationPayload {
  name: string;
  uuid?: string;
  space_name?: string;
  subscriptions: NotificationSubscription[];
}

export interface NotificationListItem {
  created_at: string;
  created_by: string;
  name: string;
  uuid: string;
  isDeleting?: boolean;
  resource_actions: string[];
}

export interface NotificationDetail extends NotificationListItem {
  subscriptions: NotificationSubscription[];
}

@Injectable()
export class NotificationService {
  NOTIFICATION_URL: string;
  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) account: Account,
  ) {
    this.NOTIFICATION_URL = `/ajax/notifications/${account.namespace}/`;
  }

  getNotifications(): Promise<Array<NotificationListItem>> {
    return this.httpService
      .request(this.NOTIFICATION_URL, {
        method: 'GET',
        ignoreProject: true,
      })
      .then(({ result }) => result);
  }

  getNotificationDetail(name: string): Promise<NotificationDetail> {
    return this.httpService.request(this.NOTIFICATION_URL + name, {
      method: 'GET',
      ignoreProject: true,
    });
  }

  createNotification(
    payload: NotificationPayload,
  ): Promise<NotificationDetail> {
    return this.httpService.request(this.NOTIFICATION_URL, {
      method: 'POST',
      body: payload,
      ignoreProject: true,
    });
  }

  updateNotification(
    payload: NotificationPayload,
  ): Promise<NotificationDetail> {
    const notification = payload.uuid;
    return this.httpService.request(this.NOTIFICATION_URL + notification, {
      method: 'PUT',
      body: payload,
      ignoreProject: true,
    });
  }

  deleteNotification(name: string) {
    return this.httpService.request(this.NOTIFICATION_URL + name, {
      method: 'DELETE',
      ignoreProject: true,
    });
  }
}
