import { NotificationService } from '@alauda/ui';
import { Inject, Injectable } from '@angular/core';

import { RcAccount } from 'app/services/api/account.service';
import { RcRole } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, Weblabs } from 'app/typings';

export interface User {
  username: string;
  type: string;
  created_at: string;
  updated_at: string;
  realname: string;
  department: string;
  position: string;
  mobile: string;
  email: string;
  office_address: string;
  landline_phone: string;
  extra_info: object;
  sub_type: string;
  is_valid: boolean;
}

export interface AccountPage {
  count: number;
  results: RcAccount[];
}

export interface Org {
  company: string;
  name: string;
  logo_file: string;
}

export interface LdapGroup {
  name: string;
  uuid: string;
  namespace?: string;
}
@Injectable()
export class OrgService {
  private URL_PREFIX: string;

  constructor(
    @Inject(ACCOUNT) public account: Account,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private http: HttpService,
  ) {
    this.URL_PREFIX = `/ajax/orgs/${account.namespace}`;
  }

  getOrg() {
    return this.http.request<Org>(this.URL_PREFIX);
  }

  /**
   * get user information
   */
  getUser(username?: string): Promise<User> {
    return this.http.request<User>(
      `${this.URL_PREFIX}/accounts/${username || this.account.username}`,
    );
  }

  updateUser(user: User) {
    return this.http.request(
      `${this.URL_PREFIX}/accounts/${user.username}/details`,
      {
        method: 'put',
        body: user,
      },
    );
  }

  updateOrg(name: string, company: string) {
    const url = `/ajax/orgs/${name}`;
    return this.http.request(url, {
      method: 'PUT',
      body: { name: name, company: company },
    });
  }

  //***  ldap start
  createOrgLdap(options: { config?: any; orgName?: string }) {
    return this.http
      .request(`/ajax/orgs/${options.orgName}/config/ldap`, {
        method: 'POST',
        body: options.config,
      })
      .then(res => {
        this.auiNotificationService.success({
          content: this.translate.get('org_ldap_create_success'),
        });
        return res;
      });
  }

  getOrgLdap({ cache = false } = {}) {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/config/ldap`,
      {
        method: 'GET',
        cache,
      },
    );
  }

  getOrgLdapSync() {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/config/ldap/sync`,
      {
        method: 'GET',
      },
    );
  }

  deleteOrgLdap(options: { orgName?: string }) {
    return this.http
      .request(`/ajax/orgs/${options.orgName}/config/ldap`, {
        method: 'DELETE',
      })
      .then(res => {
        this.auiNotificationService.success({
          content: this.translate.get('org_ldap_delete_success'),
        });
        return res;
      });
  }

  updateOrgLdap(options: { config?: any; orgName?: string }) {
    options.config.org_name = options.orgName;
    return this.http
      .request({
        method: 'PUT',
        url: `/ajax/orgs/${options.orgName}/config/ldap`,
        body: options.config,
      })
      .then(res => {
        this.auiNotificationService.success({
          content: this.translate.get('org_ldap_update_success'),
        });
        return res;
      });
  }

  triggerOrgLdapSync(options: { orgName?: string }) {
    return this.http
      .request({
        method: 'PUT',
        url: `/ajax/orgs/${options.orgName}/config/ldap/sync`,
      })
      .then(res => {
        this.auiNotificationService.success({
          content: this.translate.get('org_ldap_sync_trigger_success'),
        });
        return res;
      });
  }

  getOrgLdapInfo() {
    return this.http.request({
      method: 'GET',
      url: `/ajax/orgs/${this.account.namespace}/config/ldap/info`,
    });
  }

  deleteLdapAccounts(params: any) {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/config/ldap/sync`,
      {
        method: 'DELETE',
        params,
      },
    );
  }

  // *** ldap end

  listOrgAccounts({
    page = 1,
    page_size = 20,
    order_by = 'username',
    search = '',
    team_name_filter = '',
    assign = '',
    space_name = '',
    project_name = '',
    ldap_group_name = '',
  }) {
    const url = `/ajax/orgs/${this.account.namespace}/accounts/`;
    return this.http.request(url, {
      method: 'GET',
      params: {
        page,
        page_size,
        order_by,
        search,
        team_name_filter,
        assign,
        space_name,
        project_name,
        ldap_group_name,
      },
      ignoreProject: true,
    });
  }

  listOrgFilteredAccounts({
    org_name = '',
    page = 1,
    page_size = 20,
    order_by = 'username',
    search = '',
    team_name_filter = '',
    assign = '',
    invalid_ldap = '',
    action = 'get',
    project_name = '',
    ignoreProject = true,
    space_name = '',
  }) {
    const url = '/ajax/org/account/filtered_list';
    return this.http.request(url, {
      method: 'GET',
      params: {
        org_name,
        page,
        page_size,
        order_by,
        search,
        team_name_filter,
        assign,
        invalid_ldap,
        action,
        project_name,
        space_name,
      },
      ignoreProject,
      addNamespace: true,
    });
  }

  getAccounts({
    page = 1,
    page_size = 20,
    order_by = 'username',
    search = '',
    team_name_filter = '',
    assign = '',
    invalid_ldap = '',
    action = 'get',
    project_name = '',
    k8s_namespace_uuid = '',
    customer = '',
    ignoreProject = false,
  }): Promise<AccountPage> {
    const url = `/ajax/orgs/${this.account.namespace}/accounts/`;
    return this.http.request({
      url: url,
      method: 'GET',
      params: {
        page,
        page_size,
        order_by,
        search,
        team_name_filter,
        assign,
        invalid_ldap,
        action,
        project_name,
        k8s_namespace_uuid,
        customer,
      },
      ignoreProject,
    });
  }

  getOrgAccount({ username = '', cache = false }) {
    const url = `/ajax/orgs/${this.account.namespace}/accounts/${username}`;
    return this.http.request(url, {
      method: 'GET',
      cache,
    });
  }

  removeOrgAccount(data: any) {
    const url = `/ajax/orgs/${this.account.namespace}/accounts/${
      data.username
    }`;
    return this.http.request(url, {
      method: 'DELETE',
      params: data,
    });
  }

  getAccountRoles(
    username: string,
    params?: {
      project_name?: string;
      k8s_namespace_uuid?: string;
      space_name?: string;
    },
  ) {
    return this.http.request({
      method: 'GET',
      url: `/ajax/orgs/${this.account.namespace}/accounts/${username}/roles/`,
      params,
      ignoreProject: true,
    });
  }

  addAccountRoles(username: string, roles: any) {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/accounts/${username}/roles/`,
      {
        method: 'POST',
        body: roles,
      },
    );
  }

  removeAccountRoles(
    username: string,
    roles: any,
    params?: { [key: string]: any },
  ) {
    return this.http.request(
      `/ajax/orgs/${this.account.namespace}/accounts/${username}/roles/`,
      {
        method: 'DELETE',
        body: roles,
        params,
      },
    );
  }

  createRoleBasedAccounts(data: any) {
    return this.http.request(
      `/ajax/v1/orgs/${this.account.namespace}/accounts`,
      {
        method: 'POST',
        body: data,
      },
    );
  }

  //
  async getAllRolesOrUsers(type: 'role' | 'user'): Promise<Array<any>> {
    const pageSize = 50;
    let roles: Array<RcRole> = [];
    const reqUrlFun =
      type === 'role' ? this.getRoleBackUrl : this.getUerBackUrl;
    try {
      const firstPartData: {
        count?: number;
        results?: Array<RcRole>;
      } = await this.http.request(reqUrlFun.call(this, 1, pageSize));
      roles = roles.concat(firstPartData['results']);
      const total = Math.floor(firstPartData.count / pageSize);
      if (total) {
        const promiseArr = new Array(total).fill(0).map((_val, index) => {
          return this.http.request(reqUrlFun.call(this, index + 2, pageSize));
        });
        const retArr = await Promise.all(promiseArr);
        retArr.forEach(ret => {
          roles = roles.concat(ret['results']);
        });
      }
    } catch (e) {
      this.errorsToastService.error(e);
    } finally {
      return roles;
    }
  }

  getRoleBackUrl(page: number, pageSize: number = 50) {
    const currentProject = window.sessionStorage.getItem('project');
    let roleBackendUrl = `/ajax/roles/${
      this.account.namespace
    }/?page=${page}&page_size=${pageSize}&assign=true&namespace=${
      this.account.namespace
    }&order_by=created_at&action=view`;
    if (currentProject) {
      roleBackendUrl += `&project_name=${currentProject}`;
    }
    return roleBackendUrl;
  }

  getUerBackUrl(page: number = 1, pageSize: number = 50) {
    return `/ajax/org/account/filtered_list?page=${page}&page_size=${pageSize}&action=view&assign=true&order_by=username&namespace=${
      this.account.namespace
    }`;
  }

  getLdapGroups(): Promise<LdapGroup[]> {
    return this.http
      .request(`${this.URL_PREFIX}/groups/ldap`)
      .then(({ result }) => result);
  }
}
