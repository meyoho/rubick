// import { query } from '@angular/animations';
import { Injectable } from '@angular/core';

import { head } from 'lodash-es';
import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  ListResult,
  PipelineConfig,
  PipelineConfigModel,
  PipelineHistoryStep,
  PipelineParams,
  PipelineTemplate,
  PipelineTemplateResource,
  PipelineTemplateSync,
  PipelineTemplateSyncConfig,
  PipelineTemplateSyncResponse,
  TemplateCategory,
} from 'app/services/api/pipeline/pipeline-api.types';
import {
  mapFindRegistryResponseToList,
  toCategoryList,
  toClusterPipelineTemplateList,
  toCodeRepository,
  toJenkinsBinding,
  toPipelineConfig,
  toPipelineConfigList,
  toPipelineConfigModel,
  toPipelineConfigResource,
  toPipelineHistory,
  toPipelineTemplate,
  toPipelineTemplateList,
  toPipelineTemplateSync,
  toPipelineTemplateSyncSource,
  toToolBasic,
} from 'app/services/api/pipeline/utils';
import { HttpService } from 'app/services/http.service';

@Injectable()
export class PipelineApiService {
  static readonly PIPELINE_CONFIG_URL =
    '/ajax/v2/devops/namespace/pipelineconfigs';
  static readonly PIPELINE_URL = '/ajax/v2/devops/namespace/pipelines';
  static readonly PIPELINE_TEMPLATE_SYNC_URL =
    '/ajax/v2/devops/namespace/pipelinetemplatesyncs';
  static readonly PIPELINE_TEMPLATE_URL =
    '/ajax/v2/devops/namespace/pipelinetemplates';
  static readonly PIPELINE_CLUSTER_TEMPLATE_URL =
    '/ajax/v2/devops/cluster/clusterpipelinetemplates';
  static readonly PIPELINE_CATEGORIES_URL =
    '/ajax/v2/devops/namespace/pipelinetemplatecategories';

  constructor(private httpService: HttpService) {}

  // pipelineconfigs
  findPipelineConfigs(
    space: string,
    params: PipelineParams,
  ): Observable<ListResult<PipelineConfig>> {
    return from(
      this.httpService.request({
        url: `/ajax/v2/devops/namespace/pipelineconfig_with_pipeline`,
        method: 'GET',
        params: { ...params, space_name: space },
      }),
    ).pipe(map(toPipelineConfigList));
  }

  getPipelineConfig(name: string, space?: string): Observable<PipelineConfig> {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_CONFIG_URL}/${name}`,
        method: 'GET',
        params: { space_name: space || '' },
      }),
    ).pipe(map(toPipelineConfig));
  }

  deletePipelineConfig(space: string, name: string) {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_CONFIG_URL}/${name}`,
        method: 'DELETE',
        params: { space_name: space },
      }),
    );
  }

  createPipelineConfig(space: string, data: PipelineConfigModel) {
    return from(
      this.httpService.request({
        url: PipelineApiService.PIPELINE_CONFIG_URL,
        method: 'POST',
        body: toPipelineConfigResource(data),
        params: {
          space_name: space,
        },
      }),
    );
  }

  getPipelineConfigToModel(
    space: string,
    name: string,
  ): Observable<PipelineConfigModel> {
    return this.getPipelineConfig(name, space).pipe(map(toPipelineConfigModel));
  }

  updatePipelineConfig({
    name,
    data,
    space_name,
  }: {
    name: string;
    data: PipelineConfigModel;
    space_name: string;
  }) {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_CONFIG_URL}/${name}`,
        method: 'PUT',
        body: toPipelineConfigResource(data),
        params: { space_name },
      }),
    ).pipe(map(toPipelineConfig));
  }

  // pipelines
  getPipelineHistorySteps(space: string, name: string, stageId?: string) {
    return from(
      this.httpService.request<{ tasks: PipelineHistoryStep[] }>({
        url: `${PipelineApiService.PIPELINE_URL}/${name}/tasks`,
        method: 'GET',
        params: {
          stage: stageId,
          space_name: space,
        },
      }),
    );
  }

  triggerPipeline(space: string, name: string, parameters = {}) {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_CONFIG_URL}/${name}/trigger`,
        method: 'POST',
        body: parameters,
        params: {
          space_name: space,
        },
      }),
    );
  }

  previewPipelineJenkinsfile(
    space: string,
    templateName: string,
    kind: string,
    data?: { [key: string]: any },
  ) {
    return from(
      this.httpService.request({
        url: `${
          kind === 'pipelinetemplate'
            ? PipelineApiService.PIPELINE_TEMPLATE_URL
            : PipelineApiService.PIPELINE_CLUSTER_TEMPLATE_URL
        }/${templateName}/preview`,
        method: 'POST',
        params: {
          space_name: space,
        },
        body: data || {},
      }),
    );
  }

  getPipelineHistoryLog(
    space: string,
    name: string,
    params: { start?: number } = {
      start: 0,
    },
  ) {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_URL}/${name}/logs`,
        method: 'GET',
        params: {
          start: `${params.start}`,
          space_name: space,
        },
      }),
    );
  }

  getPipelineHistoryStepLog(
    space: string,
    name: string,
    params: { start?: number; stage?: string; step?: string } = {
      start: 0,
      stage: '',
      step: '',
    },
  ) {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_URL}/${name}/logs`,
        method: 'GET',
        params: {
          start: `${params.start}`,
          stage: params.stage,
          step: params.step,
          space_name: space,
        },
      }),
    );
  }

  getPipelineHistories(params?: any) {
    return from(
      this.httpService.request({
        url: PipelineApiService.PIPELINE_URL,
        method: 'GET',
        params: params,
      }),
    ).pipe(
      map((response: any) => ({
        total: response.count,
        histories: response.results.map(toPipelineHistory),
      })),
    );
  }

  deletePipeline(space: string, name: string) {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_URL}/${name}`,
        method: 'DELETE',
        params: {
          space_name: space,
        },
      }),
    );
  }

  abortPipeline(space: string, name: string) {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_URL}/${name}/abort`,
        method: 'PUT',
        params: {
          space_name: space,
        },
      }),
    );
  }

  // templates
  templateSetting(
    data: PipelineTemplateSyncConfig,
  ): Observable<PipelineTemplateSync> {
    return from(
      this.httpService.request<PipelineTemplateSyncResponse>({
        url: `${PipelineApiService.PIPELINE_TEMPLATE_SYNC_URL}`,
        method: 'POST',
        body: toPipelineTemplateSyncSource(data),
      }),
    ).pipe(map(toPipelineTemplateSync));
  }

  updateTemplateSetting(
    name: string,
    data: PipelineTemplateSyncConfig,
  ): Observable<PipelineTemplateSync> {
    return from(
      this.httpService.request<PipelineTemplateSyncResponse>({
        url: `${PipelineApiService.PIPELINE_TEMPLATE_SYNC_URL}/${name}`,
        method: 'PUT',
        body: toPipelineTemplateSyncSource(data),
      }),
    ).pipe(map(toPipelineTemplateSync));
  }

  templateSyncTrigger(name: string, data: PipelineTemplateSync) {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_TEMPLATE_SYNC_URL}/${name}`,
        method: 'PUT',
        body: data,
      }),
    ).pipe(map(toPipelineTemplateSync));
  }

  templateSyncDetail(): Observable<PipelineTemplateSync> {
    return from(
      this.httpService.request<PipelineTemplateSyncResponse>({
        url: `${PipelineApiService.PIPELINE_TEMPLATE_SYNC_URL}`,
        method: 'GET',
      }),
    ).pipe(
      map(({ result }: any) => head(result)),
      map(toPipelineTemplateSync),
    );
  }

  templateList(query?: {
    [key: string]: string;
  }): Observable<ListResult<PipelineTemplate>> {
    return from(
      this.httpService.request({
        url: PipelineApiService.PIPELINE_TEMPLATE_URL,
        method: 'GET',
        params: {
          ...query,
          labelSelector: 'alauda.io/latest=true',
        },
      }),
    ).pipe(map(toPipelineTemplateList));
  }

  templateDetail(name: string, space: string): Observable<PipelineTemplate> {
    return from(
      this.httpService.request<PipelineTemplateResource>({
        url: `${PipelineApiService.PIPELINE_TEMPLATE_URL}/${name}`,
        method: 'GET',
        params: {
          space_name: space,
        },
      }),
    ).pipe(map(toPipelineTemplate));
  }

  clusterTemplateList(query?: any): Observable<ListResult<PipelineTemplate>> {
    return from(
      this.httpService.request({
        url: PipelineApiService.PIPELINE_CLUSTER_TEMPLATE_URL,
        method: 'GET',
        params: {
          ...query,
          labelSelector: 'alauda.io/latest=true',
        },
      }),
    ).pipe(map(toClusterPipelineTemplateList));
  }

  clusterTemplateDetail(name: string): Observable<PipelineTemplate> {
    return from(
      this.httpService.request({
        url: `${PipelineApiService.PIPELINE_CLUSTER_TEMPLATE_URL}/${name}`,
        method: 'GET',
      }),
    ).pipe(map(toPipelineTemplate));
  }

  categories(): Observable<ListResult<TemplateCategory>> {
    return from(
      this.httpService.request({
        url: PipelineApiService.PIPELINE_CATEGORIES_URL,
        method: 'GET',
      }),
    ).pipe(map(toCategoryList));
  }

  cronCheck(space: string, jenkinsbinding: string, query: any) {
    return from(
      this.httpService.request({
        url: `/ajax/v2/devops/namespace/jenkinsbindings_croncheck/${jenkinsbinding}/croncheck`,
        method: 'GET',
        params: {
          ...query,
          space_name: space,
        },
      }),
    );
  }

  jenkinsBinding(name: string, space_name: string) {
    return this.httpService.request({
      url: `/ajax/v2/devops/namespace/jenkinsbindings/${name}`,
      method: 'GET',
      params: {
        space_name,
      },
    });
  }

  jenkinsBindings(space_name: string) {
    return from(
      this.httpService.request({
        url: '/ajax/v2/devops/namespace/jenkinsbindings',
        method: 'GET',
        params: {
          space_name,
        },
      }),
    ).pipe(map(toJenkinsBinding));
  }

  imageRepositories(space: string) {
    return from(
      this.httpService.request({
        url: '/ajax/v2/devops/namespace/imagerepositories',
        method: 'GET',
        params: {
          space_name: space,
        },
      }),
    ).pipe(map(mapFindRegistryResponseToList));
  }

  codeRepositories(space?: string) {
    return from(
      this.httpService.request({
        url: '/ajax/v2/devops/namespace/coderepositories',
        method: 'GET',
        params: { space_name: space },
      }),
    ).pipe(
      map((res: any) =>
        Object.assign({
          total: res.count,
          items: res.result.map(toCodeRepository),
        }),
      ),
    );
  }

  getBindingsByType(project: string, space: string, toolType: string) {
    return from(
      this.httpService.request({
        url: `/ajax/v2/devops/namespace/${toolType}`,
        method: 'GET',
        params: {
          project_name: project,
          space_name: space,
        },
      }),
    ).pipe(map(({ result }) => result.map(toToolBasic)));
  }

  getPipelineExportsOptions(tempName: string) {
    return from(
      this.httpService.request({
        url: `${
          PipelineApiService.PIPELINE_CLUSTER_TEMPLATE_URL
        }/${tempName}/exports`,
        method: 'GET',
        params: {
          formatvalue: '${%s}',
        },
      }),
    ).pipe(map((res: any) => res.values));
  }

  getPipeineCodeRepositoryBranchs(
    repository: string,
    project_name: string,
    space_name: string,
  ) {
    return from(
      this.httpService.request({
        url: `/ajax/v2/devops/namespace/coderepositories/${repository}/branches`,
        method: 'GET',
        params: {
          project_name,
          space_name,
        },
      }),
    );
  }
}
