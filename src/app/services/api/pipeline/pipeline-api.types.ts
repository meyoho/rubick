export interface ListResult<T> {
  total: number;
  items: Array<T>;
  errors: any[];
}

export interface Pagination<T> {
  items: T[];
  total: number;
  error?: boolean;
}

export interface PipelineParams {
  sortBy?: string;
  filterBy?: string;
  page?: string;
  itemsPerPage?: string;
}

export interface PipelineConfigListResponse {
  total: number;
  items: PipelineConfig[];
}

export interface PipelineIdentity {
  name: string;
  namespace: string;
}

export interface PipelineHistoryCause {
  type: 'manual' | 'cron' | 'codeChange';
  message: string;
}

export interface PipelineConfigStrategy {
  jenkins: { jenkinsfile?: string; jenkinsfilePath?: string };
  template?: PipelineTemplate;
}

export interface PipelineRepositorySource {
  git: {
    ref: string;
    uri: string;
  };
  path?: string;
  branch?: string;
  secret?: string;
  codeRepository: { name: string; ref: string };
}

export interface PipelineConfig extends PipelineIdentity {
  displayName: string;
  annotations: { [key: string]: string };
  labels: { [key: string]: string };
  application?: string;
  createdAt: string;
  histories: PipelineHistory[];
  jenkinsInstance: string;
  codeRepository: string;
  runPolicy: string;
  strategy: PipelineConfigStrategy;
  source: PipelineRepositorySource;
  parameters: TriggerPipelineParameter[];
  triggers: PipelineTrigger[];
  status?: any;
  templateVersion?: string;
  latestTemplateVersion?: string;
  upgradeTemplateName?: string;
  __original: any;
}

export interface PipelineTrigger {
  type: string;
  enabled: boolean;
  rule: string;
  schedule?: { weeks: string[]; times: string[] };
}

export interface PipelineHistory {
  name: string;
  createdAt: string;
  namespace: string;
  badges?: { text: string; link: string }[];
  pipeline: string;
  cause?: PipelineHistoryCause;
  status: {
    [key: string]: any;
  };
  jenkins: {
    [key: string]: any;
  };
  __original: any;
}

export interface PipelineHistoryLog {
  more: boolean;
  text: string;
  nextStart: number;
}

export interface PipelineHistoryStep {
  id: string;
  type: string;
  displayDescription: string;
  displayName: string;
  durationInMillis: number;
  input: any;
  result: string;
  state: string;
  startTime: string;
  edges: any;
  text: string;
  actions?: any[];
}

export interface PipelineConfigTriggerResponse {
  codeChange?: {
    enabled: boolean;
    periodicCheck: string;
  };
  cron?: {
    enabled: boolean;
    rule: string;
    schedule?: { weeks: string[]; times: string[] };
  };
  type: string;
}

export interface PipelineConfigResponse {
  metadata: {
    name: string;
    annotations?: any;
    namespace?: string;
    labels?: any;
    displayName?: string;
  };
  spec: {
    jenkinsBinding: {
      name: string;
    };
    runPolicy: string;
    source?: {
      git: {
        ref: string;
        uri: string;
      };
      secret: {
        name: string;
      };
    };
    strategy?: {
      jenkins: {
        jenkinsfile: string;
        jenkinsfilePath: string;
      };
    };
    triggers: PipelineConfigTriggerResponse[];
  };
  kind: string;
}

export interface PipelineConfigModel {
  template?: PipelineTemplate;
  basic: {
    name: string;
    display_name: string;
    jenkins_instance: string;
    source: string;
    run_policy: string;
  };
  jenkinsfile: {
    repo: { repo: string; secret: string };
    branch: string;
    path: string;
    secret?: string;
  };
  editor_script: {
    script: string;
  };
  triggers: {
    enabled: boolean;
    cron_string: string;
  }[];
  __original: any;
}

export interface PipelineTemplateSyncCondition {
  lastTransitionTime: string;
  lastUpdateTime: string;
  message: string;
  name: string;
  reason: string;
  status: string;
  target: string;
  type: string;
  previousVersion: string;
  version: string;
}

export interface PipelineTemplateSyncStatus {
  commitID?: string;
  conditions?: PipelineTemplateSyncCondition[];
  endTime?: string;
  message?: string;
  phase?: string;
  startTime?: string;
}

export interface PipelineTemplateSyncResponse {
  kind?: string;
  metadata: {
    [key: string]: any;
  };
  spec: {
    source?: {
      codeRepository?: { name: string; ref: string };
      git?: { ref: string; uri: string };
      secret?: { name: string };
    };
  };
  status?: PipelineTemplateSyncStatus;
}

export interface PipelineTemplateSyncConfig {
  codeRepository?: { name: string; ref: string };
  git?: { ref: string; uri: string };
  secret?: { name: string };
  status?: {
    phase: 'Draft' | 'Pending' | 'Error' | 'Ready';
  };
}

export interface PipelineTemplateSync {
  name: string;
  codeRepository?: { name: string; ref: string };
  git?: { ref: string; uri: string };
  secret?: { name: string };
  status: PipelineTemplateSyncStatus;
  codeRepositoryName?: string;
  branch?: string;
  gitUri?: string;
  secretName?: string;
  __original: any;
}

export interface PipelineTemplateStage {
  kind: string;
  name: string;
  tasks: [
    {
      agent: {
        label: string;
      };
      approve: {
        message: string;
        timeout: number;
      };
      environments: [
        {
          name: string;
          value: string;
        }
      ];
      kind: string;
      name: string;
      options: {
        timeout: number;
      };
      type: string;
    }
  ];
}

export interface PipelineTemplateResource {
  metadata: {
    [key: string]: any;
  };
  kind: string;
  spec: {
    agent: {
      label: string;
    };
    arguments: [
      {
        displayName: TemplateBasicDescription;
        items: TemplateArgumentItem[];
      }
    ];
    engine: string;
    parameters: [
      {
        description: string;
        name: string;
        type: string;
        value: string;
      }
    ];
    stages: PipelineTemplateStage[];
    withSCM: boolean;
    template?: {
      pipelineTemplateRef?: PipelineTemplateRef;
      values?: any;
    };
  };
}

export interface PipelineTemplate {
  name: string;
  kind: string;
  displayName: TemplateBasicDescription;
  labels?: { key: string; value: string }[];
  description: TemplateBasicDescription;
  version: string;
  latestVersion?: string;
  arguments: TemplateArgumentField[];
  stages: PipelineTemplateStage[];
  styleIcon: string;
  withSCM: boolean;
  official?: boolean;
  template?: {
    pipelineTemplateRef?: PipelineTemplateRef;
    values?: any;
  };
  __original: any;
}

export interface PipelineTemplateRef {
  name: string;
  namespace?: string;
  kind?: string;
}

export interface TemplateBasicDescription {
  'zh-CN': string;
  en: string;
  [key: string]: string;
}

export interface TemplateArgumentItem {
  name: string;
  schema: { type: string };
  binding: string[];
  display: {
    type: string;
    name: TemplateBasicDescription;
  };
  description: TemplateBasicDescription;
  required: boolean;
  default?: any;
  validation?: any;
  relation?: any;
  value?: any;
}

export interface TemplateArgumentField {
  displayName: TemplateBasicDescription;
  items: TemplateArgumentItem[];
}

export interface TriggerPipelineParameter {
  description: string;
  name: string;
  type: string;
  value: string;
}

export interface TemplateCategory {
  name: string;
}

export interface K8SMetaData {
  name: string;
  namespace?: string;
  description?: string;
  creationTimestamp?: string;
  labels?: { [key: string]: string };
  annotations?: { [key: string]: string };
}

export interface K8SResource {
  apiVersion?: string;
  kind?: string;
  metadata?: K8SMetaData;
  objectMeta?: K8SMetaData;
  spec: {
    [key: string]: any;
  };
  status?: {
    phase: string;
    message: string;
  };
}

export interface Registry {
  name: string;
  namespace: string;
  endpoint: string;
  link: string;
  type: string;
  image?: string;
  creationTimestamp: string;
  host: string;
  tags?: ImageTag[];
  status: {
    message: string;
    phase: string;
  };
  secretName?: string;
  secretNamespace?: string;
  __original: K8SResource;
}

export interface ImageTag {
  name: string;
  digest: string;
  createdAt: string;
  updatedAt: string;
  size?: string;
  level?: number;
  scanStatus?: ScanStatus;
  message?: string;
  author?: string;
  summary?: ScanSummary[];
}

export enum ScanStatus {
  NotScan = 'notScan',
  Pending = 'pending',
  Analyzing = 'analyzing',
  Finished = 'finished',
  Error = 'error',
  Running = 'running',
}

export interface ScanSummary {
  severity: number;
  count: number;
}

export interface CodeRepository {
  namespace: string;
  name: string;
  httpURL: string;
  sshURL: string;
  size: number;
  sizeHumanize: string;
  commit: string;
  commitID: string;
  type: string;
  fullName: string;
  ownerName: string;
  secret: { namespace?: string; name: string };
  status: {
    phase: string;
    message: string;
  };
}
