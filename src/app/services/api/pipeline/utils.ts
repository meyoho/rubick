import { find, get, last, map } from 'lodash-es';

import {
  CodeRepository,
  ImageTag,
  K8SResource,
  ListResult,
  PipelineConfig,
  PipelineConfigModel,
  PipelineConfigResponse,
  PipelineConfigTriggerResponse,
  PipelineHistory,
  PipelineTemplate,
  PipelineTemplateRef,
  PipelineTemplateResource,
  PipelineTemplateSync,
  PipelineTemplateSyncConfig,
  PipelineTemplateSyncResponse,
  PipelineTrigger,
  Registry,
  TemplateCategory,
} from 'app/services/api/pipeline';
import {
  ANNOTATION_CONFIG_TEMPLATE_LATEST_VERSION,
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
  ANNOTATION_PIPELINE_BADGES,
  ANNOTATION_PIPELINE_BUILD_URI,
  ANNOTATION_TEMPLATE_LATEST_VERSION,
  ANNOTATION_TEMPLATE_STYLE_ICON,
  ANNOTATION_TEMPLATE_VERSION,
} from 'app/utils/config';

export function toPipelineConfigList(
  response: any,
): ListResult<PipelineConfig> {
  return {
    total: get(response, 'count', 0),
    items: mapPipelinesResponse(response.results),
    errors: response.errors,
  };
}

export function mapPipelinesResponse(configs: any[]): PipelineConfig[] {
  return configs.map(toPipelineConfig);
}

export function toPipelineConfig(config: any): PipelineConfig {
  const metaKey = config.objectMeta ? 'objectMeta' : 'metadata';
  return {
    name: get(config, [metaKey, 'name'], ''),
    displayName: get(
      config,
      [metaKey, 'annotations', ANNOTATION_DISPLAY_NAME],
      '',
    ),
    labels: get(config, [metaKey, 'labels'], ''),
    annotations: get(config, [metaKey, 'annotations']),
    namespace: get(config, [metaKey, 'namespace'], ''),
    createdAt: get(config, [metaKey, 'creationTimestamp'], ''),
    application: get(config, [metaKey, 'labels', 'app'], ''),
    histories: (get(config, 'pipelines') || []).map(toPipelineHistory),
    jenkinsInstance: get(config, 'spec.jenkinsBinding.name', ''),
    runPolicy: get(config, 'spec.runPolicy'),
    codeRepository: get(config, 'spec.source.git.uri', ''),
    source: get(config, 'spec.source'),
    parameters: get(config, 'spec.parameters') || [],
    strategy: {
      ...get(config, 'spec.strategy'),
      template: toPipelineTemplate(get(config, 'spec.strategy.template')),
    },
    triggers: (get(config, 'spec.triggers') || []).map(mapPipelineTrigger),
    status: get(config, 'status'),
    latestTemplateVersion: get(
      config,
      ['metadata', 'annotations', ANNOTATION_CONFIG_TEMPLATE_LATEST_VERSION],
      '',
    ),
    templateVersion: get(config, ['metadata', 'labels', 'templateVersion'], ''),
    upgradeTemplateName: get(
      config,
      ['metadata', 'labels', 'templateName'],
      '',
    ),
    __original: config,
  };
}

export function mapPipelineTrigger(trigger: any): PipelineTrigger {
  const detail = trigger[trigger.type];
  return {
    type: trigger.type,
    enabled: detail.enabled,
    rule: detail.rule || detail.periodicCheck,
    schedule: detail.schedule,
  };
}

export function toPipelineHistory(history: any): PipelineHistory {
  const metaKey = history.objectMeta ? 'objectMeta' : 'metadata';
  const sourceStages = JSON.parse(get(history, 'status.jenkins.stages', '{}'));
  return {
    name: get(history, [metaKey, 'name'], ''),
    pipeline: get(history, 'spec.pipelineConfig.name', ''),
    badges: JSON.parse(
      get(history, [metaKey, 'annotations', ANNOTATION_PIPELINE_BADGES]) ||
        '[]',
    ),
    createdAt: get(history, [metaKey, 'creationTimestamp'], ''),
    namespace: get(history, [metaKey, 'namespace'], ''),
    cause: get(history, 'spec.cause', ''),
    status: get(history, 'status', {}),
    jenkins: {
      ...get(history, 'status.jenkins', {}),
      ...{
        stages: get(sourceStages, 'stages', []),
        startStageId: get(sourceStages, 'start_stage_id', ''),
      },
      jenkinsBuildUri: get(
        history,
        [metaKey, 'annotations', ANNOTATION_PIPELINE_BUILD_URI],
        '',
      ),
      jenkinsInstanceName: get(history, 'spec.jenkinsBinding.name', ''),
    },
    __original: history,
  };
}

export function toPipelineConfigResource(
  model: PipelineConfigModel,
): PipelineConfigResponse {
  let source: any;
  if (get(model, 'jenkinsfile.bindingRepository')) {
    source = {
      secret: {
        name: get(model, 'jenkinsfile.secret.name', ''),
        namespace: get(model, 'jenkinsfile.secret.namespace', ''),
      },
      codeRepository: {
        name: get(model, 'jenkinsfile.bindingRepository'),
        ref: get(model, 'jenkinsfile.branch', ''),
      },
    };
  } else {
    if (get(model, 'jenkinsfile.sourceType') === 'GIT') {
      source = {
        sourceType: get(model, 'jenkinsfile.sourceType'),
        git: {
          ref: get(model, 'jenkinsfile.branch', ''),
          uri: get(model, 'jenkinsfile.repo', ''),
        },
        secret: {
          name: get(model, 'jenkinsfile.secret.name', ''),
          namespace: get(model, 'jenkinsfile.secret.namespace', ''),
        },
        codeRepository: {
          name: get(model, 'jenkinsfile.bindingRepository'),
          ref: get(model, 'jenkinsfile.branch', ''),
        },
      };
    } else if (get(model, 'jenkinsfile.sourceType') === 'SVN') {
      source = {
        sourceType: get(model, 'jenkinsfile.sourceType'),
        svn: {
          uri: get(model, 'jenkinsfile.repo', ''),
        },
        secret: {
          name: get(model, 'jenkinsfile.secret.name', ''),
          namespace: get(model, 'jenkinsfile.secret.namespace', ''),
        },
        codeRepository: {
          name: get(model, 'jenkinsfile.bindingRepository'),
          ref: get(model, 'jenkinsfile.branch', ''),
        },
      };
    }
  }
  const result = {
    kind: 'PipelineConfig',
    apiVersion: 'devops.alauda.io/v1alpha1',
    metadata: {
      annotations: {
        ...get(model, '__original.metadata.annotations', {}),
        [ANNOTATION_DISPLAY_NAME]: get(model, 'basic.display_name', ''),
      },
      name: get(model, 'basic.name', ''),
      labels: {
        ...get(model, '__original.metadata.labels', {}),
        app: get(model, 'basic.app', ''),
      },
    },
    spec: {
      runPolicy: get(model, 'basic.run_policy'),
      jenkinsBinding: {
        name: get(model, 'basic.jenkins_instance', ''),
      },
      source: source,
      strategy: {
        jenkins: {
          jenkinsfile: get(model, 'editor_script.script', ''),
          jenkinsfilePath: get(model, 'jenkinsfile.path', ''),
        },
        template: get(model, 'template'),
      },
      template: getUpgradeTemplate(model),
      triggers: fromTrigger(model.triggers),
    },
  };
  // codeRepository cannot exist with git concurrently
  if (get(model, 'jenkinsfile.bindingRepository')) {
    delete result.spec.source.git;
  } else if (get(model, 'jenkinsfile.repo')) {
    delete result.spec.source.codeRepository;
  }
  if (get(model, 'editor_script.script')) {
    delete result.spec.source;
  }
  if (get(model, 'template')) {
    delete result.spec.source;
  }
  return result;
}

function getUpgradeTemplate(
  model: PipelineConfigModel,
): {
  pipelineTemplateRef?: PipelineTemplateRef;
  values?: any;
} {
  const originTemplateRef = get(
    model,
    '__original.spec.template.pipelineTemplateRef',
    {},
  );
  const latestTemplateVersion = get(
    model,
    [
      '__original',
      'metadata',
      'annotations',
      ANNOTATION_CONFIG_TEMPLATE_LATEST_VERSION,
    ],
    '',
  );
  const templateVersion = get(
    model,
    ['__original', 'metadata', 'labels', 'templateVersion'],
    '',
  );
  const upgradeTemplateName = get(
    model,
    ['__original', 'metadata', 'labels', 'templateName'],
    '',
  );

  if (latestTemplateVersion && latestTemplateVersion !== templateVersion) {
    originTemplateRef.name = upgradeTemplateName;
  }
  return { pipelineTemplateRef: originTemplateRef };
}

function fromTrigger(
  t: {
    enabled: boolean;
    cron_string: string;
    sourceType?: string;
    cron_object?: any;
  }[],
): PipelineConfigTriggerResponse[] {
  const triggers = [];
  const sourceType = get(t[1], 'sourceType');
  if (get(t, '[0].cron_string', '')) {
    triggers.push({
      codeChange: {
        enabled: t[0].enabled,
        periodicCheck: t[0].cron_string,
      },
      type: 'codeChange',
    });
  }
  if (
    get(t, '[1].cron_string', '') ||
    get(t, '[1].cron_object.times.length', 0)
  ) {
    triggers.push({
      cron: {
        enabled: t[1].enabled,
        rule: sourceType === 'input' ? get(t, '[1].cron_string', '') : '',
        schedule:
          sourceType === 'select'
            ? outputTriggerDays(get(t, '[1].cron_object', ''))
            : null,
      },
      type: 'cron',
    });
  }
  return triggers;
}

function outputTriggerDays(object: any) {
  if (!object) {
    return;
  }
  return {
    weeks: Object.keys(object.days).filter((key: string) => object.days[key]),
    times: object.times,
  };
}

function inputTriggerDays(weeks: string[], times: string[]) {
  if (!weeks || !times) {
    return '';
  }
  const days: any = {
    mon: false,
    tue: false,
    wed: false,
    thu: false,
    fri: false,
    sat: false,
    sun: false,
  };
  weeks.forEach((day: string) => {
    days[day] = true;
  });
  return {
    days: days,
    times: times,
  };
}

export function toPipelineConfigModel(
  pipelineConfig: PipelineConfig,
): PipelineConfigModel {
  const basic = {
    name: pipelineConfig.name,
    display_name: pipelineConfig.displayName,
    jenkins_instance: pipelineConfig.jenkinsInstance,
    source:
      get(pipelineConfig, 'source.git.uri', '') ||
      get(pipelineConfig, 'source.svn.uri', '') ||
      get(pipelineConfig, 'source.codeRepository.name', '')
        ? 'repo'
        : 'script',
    run_policy: pipelineConfig.runPolicy,
  };
  const jenkinsfile = {
    repo: {
      repo:
        get(pipelineConfig, 'source.git.uri', '') ||
        get(pipelineConfig, 'source.svn.uri', ''),
      secret: get(pipelineConfig, 'source.secret', {}),
      sourceType: get(pipelineConfig, 'source.sourceType', ''),
      bindingRepository: get(pipelineConfig, 'source.codeRepository.name'),
    },
    branch:
      get(pipelineConfig, 'source.git.ref', '') ||
      get(pipelineConfig, 'source.codeRepository.ref', ''),
    path: get(pipelineConfig, 'strategy.jenkins.jenkinsfilePath', ''),
  };
  const editor_script = {
    script: get(pipelineConfig, 'strategy.jenkins.jenkinsfile'),
  };
  const triggersSource = pipelineConfig.triggers;
  const codeChange = find(triggersSource, { type: 'codeChange' });
  const cron = find(triggersSource, { type: 'cron' });
  const sourceType = get(cron, 'schedule') ? 'select' : 'input';
  const triggers = [
    {
      enabled: get(codeChange, 'enabled', false),
      cron_string: get(codeChange, 'rule', ''),
    },
    {
      sourceType: sourceType,
      enabled: get(cron, 'enabled', false),
      cron_string: get(cron, 'rule', ''),
      cron_object: inputTriggerDays(
        get(cron, 'schedule.weeks', ''),
        get(cron, 'schedule.times', ''),
      ),
    },
  ];

  return {
    template: pipelineConfig.strategy.template,
    basic,
    jenkinsfile,
    editor_script,
    triggers,
    __original: pipelineConfig.__original,
  };
}

export function toPipelineTemplateSync(
  sync: PipelineTemplateSyncResponse,
): PipelineTemplateSync {
  if (!sync) {
    return;
  }
  return {
    name: get(sync, 'metadata.name', ''),
    ...get(sync, 'spec.source', ''),
    status: get(sync, 'status', ''),
    codeRepositoryName: get(sync, 'spec.source.codeRepository.name', ''),
    branch:
      get(sync, 'spec.source.codeRepository.ref', '') ||
      get(sync, 'spec.source.git.ref', ''),
    secretName: get(sync, 'spec.source.secret.name', ''),
    gitUri: get(sync, 'spec.source.git.uri', ''),
    __original: sync,
  };
}

export function toPipelineTemplate(
  template: PipelineTemplateResource,
): PipelineTemplate {
  if (!template) {
    return;
  }
  return {
    name: get(template, 'metadata.name', ''),
    kind: (get(template, 'kind') || '').toLowerCase(),
    styleIcon: get(
      template,
      ['metadata', 'annotations', `${ANNOTATION_TEMPLATE_STYLE_ICON}`],
      '',
    ),
    displayName: {
      en: get(
        template,
        ['metadata', 'annotations', `${ANNOTATION_DISPLAY_NAME}.en`],
        '',
      ),
      'zh-CN': get(
        template,
        ['metadata', 'annotations', `${ANNOTATION_DISPLAY_NAME}.zh-CN`],
        '',
      ),
    },
    labels: map(get(template, 'metadata.labels', {}), (value, key) => ({
      key,
      value,
    })),
    description: {
      en: get(
        template,
        ['metadata', 'annotations', `${ANNOTATION_DESCRIPTION}.en`],
        '',
      ),
      'zh-CN': get(
        template,
        ['metadata', 'annotations', `${ANNOTATION_DESCRIPTION}.zh-CN`],
        '',
      ),
    },
    version: get(
      template,
      ['metadata', 'annotations', ANNOTATION_TEMPLATE_VERSION],
      '',
    ),
    latestVersion: get(
      template,
      ['metadata', 'annotations', ANNOTATION_TEMPLATE_LATEST_VERSION],
      '',
    ),
    official: get(template, 'metadata.labels.source', '') !== 'customer',
    arguments: get(template, 'spec.arguments') || [],
    stages: get(template, 'spec.stages', []),
    withSCM: get(template, 'spec.withSCM', false),
    __original: template,
  };
}

export function toPipelineTemplateSyncSource(
  config: PipelineTemplateSyncConfig,
): PipelineTemplateSyncResponse {
  return {
    kind: 'PipelineTemplateSync',
    metadata: {
      name: 'TemplateSync',
      namespace: 'global-credentials',
    },
    spec: {
      source: config,
    },
    status: {
      phase: 'Pending',
    },
  };
}

export function templateStagesConvert(stages: any[]): any[] {
  let initID = 0;
  return stages
    .map((stage: any, stageIndex: number) => {
      let tasks = stage.tasks || [];
      const diagramStage: any = {
        id: ++initID,
        name: stage.name,
        edges: [],
      };
      if (tasks.length === 1) {
        diagramStage.name = tasks[0].name;
        tasks = [];
      } else if (tasks.length > 1) {
        tasks = tasks.map((task: any, taskIndex: number) => {
          const diagramTask: any = {
            id: ++initID,
            name: task.name,
            edges: [],
          };
          if (stages[stageIndex + 1]) {
            diagramTask.edges.push({
              id: initID + tasks.length - taskIndex,
              type: 'STAGE',
            });
          }
          return diagramTask;
        });
        diagramStage.edges = tasks.map((task: any) => ({
          id: task.id,
          type: 'PARALLEL',
        }));
      }
      if (stages[stageIndex + 1] && (!stage.tasks || stage.tasks.length <= 1)) {
        diagramStage.edges.push({ id: initID + 1, type: 'STAGE' });
      }
      return [diagramStage, ...tasks];
    })
    .reduce((accum, stageDiagramArray) => [...accum, ...stageDiagramArray], []);
}

export function toPipelineTemplateList(
  response: any,
): ListResult<PipelineTemplate> {
  return {
    total: get(response, 'count', 0),
    items: (get(response, 'result') || []).map(toPipelineTemplate),
    errors: response.errors,
  };
}

export function toClusterPipelineTemplateList(
  response: any,
): ListResult<PipelineTemplate> {
  return {
    total: get(response, 'count', 0),
    items: (get(response, 'result') || []).map(toPipelineTemplate),
    errors: response.errors,
  };
}

export function toCategoryList(response: any): ListResult<TemplateCategory> {
  return {
    total: get(response, 'items', '').length,
    items: get(response, 'items') || [],
    errors: response.errors,
  };
}

export function toJenkinsBinding(response: { result: any[] }) {
  return (
    response &&
    response.result.map(binding => ({
      name: get(binding, 'metadata.name', ''),
    }))
  );
}

export function mapFindRegistryResponseToList(res: any): ListResult<Registry> {
  return {
    total: get(res, 'count', 0),
    items: res.result.map(mapResourceToRegistryService),
    errors: res.errors,
  };
}

export function mapResourceToRegistryService(res: K8SResource): Registry {
  const metaKey = res.objectMeta ? 'objectMeta' : 'metadata';
  const meta = res.objectMeta || res.metadata;
  return {
    name: meta.name,
    namespace: meta.namespace,
    creationTimestamp: meta.creationTimestamp,
    image: get(res, 'spec.image', ''),
    endpoint: get(meta, 'annotations.imageRegistryEndpoint', ''),
    type: get(meta, 'annotations.imageRegistryType', ''),
    link: get(meta, 'annotations.imageRepositoryLink', ''),
    host: get(res, `${metaKey}.annotations.imageRegistryEndpoint`, ''),
    status: {
      phase: get(res, 'status.phase', ''),
      message: get(res, 'status.message', ''),
    },
    tags: (get(res, 'status.tags') || []).map(mapDataToRepoTag),
    secretName: get(meta, 'annotations.secretName', ''),
    secretNamespace: get(meta, 'annotations.secretNamespace', ''),
    __original: res,
  };
}

export function mapDataToRepoTag(data: any): ImageTag {
  const { created_at, ...rest } = data;
  return {
    ...rest,
    createdAt: created_at,
  };
}

export function toCodeRepository(resource: K8SResource): CodeRepository {
  return {
    namespace: get(resource, 'metadata.namespace', ''),
    name: get(resource, 'metadata.name', ''),
    httpURL: get(resource, 'spec.repository.htmlURL', ''),
    sshURL: get(resource, 'spec.repository.sshURL', ''),
    ownerName: get(resource, 'spec.repository.owner.name', ''),
    size: get(resource, 'spec.repository.size', ''),
    sizeHumanize: get(resource, 'spec.repository.sizeHumanize', ''),
    commit: get(resource, 'status.repository.latestCommit.commitAt', ''),
    commitID: get(resource, 'status.repository.latestCommit.commitID', ''),
    type: resource.spec.repository.codeRepoServiceType,
    fullName: resource.spec.repository.fullName,
    secret: {
      namespace: get(resource, 'metadata.annotations.secretNamespace', ''),
      name: get(resource, 'metadata.annotations.secretName', ''),
    },
    status: resource.status,
  };
}

export function splitImageAddress(imageAddress: string) {
  const lastBlock = last(imageAddress.split('/'));
  if (lastBlock.indexOf(':') >= 0) {
    return {
      address: imageAddress
        .split(':')
        .slice(0, -1)
        .join(':'),
      tag: lastBlock.split(':')[1],
    };
  } else {
    return { address: imageAddress, tag: '' };
  }
}

export function toToolBasic(tool: K8SResource) {
  return {
    name: get(tool, 'metadata.name', ''),
    namespace: get(tool, 'metadata.namespace', ''),
  };
}
