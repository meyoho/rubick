import { Injectable } from '@angular/core';

import { FetchDataResult } from 'app/abstract/pagination-data';
import { HttpService } from 'app/services/http.service';

export interface Pod {
  metadata: {
    annotations: any;
    creationTimestamp: string;
    generateName: string;
    labels: any;
    name: string;
    namespace: string;
    ownerReferences: {
      apiVersion: string;
      controller: boolean;
      kind: string;
      name: string;
      uid: string;
    }[];
    resourceVersion: string;
    selfLink: string;
    uid: string;
  };
  spec: {
    affinity: any;
    containers: {
      env: any;
      image: string;
      imagePullPolicy: string;
      name: string;
      resources: {
        requests?: {
          cpu: string;
          memory: string;
        };
        limits?: {
          cpu: string;
          memory: string;
        };
      };
      volumeMounts: {
        mountPath: string;
        name: string;
        readOnly: boolean;
      }[];
    }[];
    nodeName?: string;
  };
  status: {
    hostIP: string;
    podIP: string;
    phase: string;
    startTime: string;
  };
}

@Injectable()
export class PodService {
  K8S_PODS_ENDPOINT: string;

  constructor(private httpService: HttpService) {
    this.K8S_PODS_ENDPOINT = '/ajax/v2/pods/';
  }

  // ********** K8S Pod ********** //

  getK8sPods({
    pageNo = 1,
    pageSize = 20,
    params: { cluster = '', namespace = '', name = '' },
  }) {
    // console.log('fetch pods', 'pageNo: ', pageNo, 'pageSize: ', pageSize, 'search: ', search, 'label: ', label);
    return this.httpService
      .request(this.K8S_PODS_ENDPOINT, {
        method: 'GET',
        params: {
          cluster,
          namespace,
          name,
          page: pageNo,
          page_size: pageSize,
        },
      })
      .then((res: FetchDataResult<Pod>) => res);
  }

  deletePod({
    cluster_id,
    namespace,
    pod_name,
  }: {
    cluster_id: string;
    namespace: string;
    pod_name: string;
  }) {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${cluster_id}/pods/${namespace}/${pod_name}`,
      {
        method: 'DELETE',
      },
    );
  }
}
