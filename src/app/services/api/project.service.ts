import { Inject, Injectable } from '@angular/core';

import { Observable } from 'rxjs';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import { Quota } from 'app/typings/quota.types';
import { QuotaRange } from 'app_user/features/project/types';
import { Cluster } from './region.service';
import { Space } from './space.service';

export enum PorjectTabIndex {
  Quota = 0,
  Members = 1,
  Roles = 2,
  Audit = 3,
  Namespace = 4,
  Space = 5,
  ToolChain = 6,
  Credential = 7,
}

export interface Project {
  name: string;
  uuid: string;
  display_name: string;
  status: string;
  description: string;
  template_uuid: string;
  template: string;
  created_at: string;
  updated_at: string;
  created_by: string;
  service_type?: string[];
  clusters?: ClusterInProject[];
  admin_list?: Admin[];
  registry_uuid?: string;
  registry_endpoint?: string;
  registry_path_name?: string;
  is_system?: boolean;
}

export interface ClusterInProject {
  name: string;
  display_name: string;
  uuid: string;
  quota?: Quota;
  projectQuota?: Quota;
}

export interface Admin {
  name: string;
  realname: string;
}

export interface NamespaceSetting {
  quotas: {}[];
  name: string;
  cluster: NewCluster;
}

export interface NewCluster {
  name: string;
  display_name: string;
  origin_display_name?: string;
  uuid?: string;
  flag?: string;
  service_type?: string;
  regions?: {
    name: string;
    display_name: string;
    id?: string;
  }[];
  quota?: QuotaRange;
  quotaDisplay?: string;
}

export interface TransferedNewCluster {
  name: string;
  display_name: string;
  origin_display_name?: string;
  uuid?: string;
  regions?: {
    name: string;
    display_name: string;
    id?: string;
  }[];
  quota?: Quota;
}

export interface ProjectClustersQuotaApi {
  name: string;
  uuid: number;
  clusters: NewCluster[];
}

export interface NamespaceQuotaWithSpace {
  cluster_uuid: string;
  namespace_uuid: string;
  project_uuid: string;
  quota: {
    pvc_num: number;
    storage: number;
    memory: string;
    pods: string;
    cpu: string;
  };
  spaces?: Space[];
}

export interface AdminRoleApi {
  name: string;
  realname: string;
}

export interface ClusterInAvailabledResource extends Cluster {
  quota?: Quota;
}

export interface ProjectAvailableResourceApi {
  clusters: ClusterInAvailabledResource[];
}

interface NamespaceWithQuotaInfoApi {
  results: NamespaceWithQuotaInfo[];
  count?: number;
}
export interface NamespaceWithQuotaInfo {
  project_uuid: string;
  created_at: string;
  quota: Quota;
  spaces: Space[];
  updated_at: string;
  cluster_uuid: string;
  namespace_uuid: string;
}

interface SpaceWithNamespaceInfo {
  namespace_name?: string;
  namespace_uuid: string;
  space_name?: string;
  space_uuid: string;
}

export interface PreAddRegionData {
  name?: string;
  display_name?: string;
  id?: string;
  uuid?: string;
  service_type?: string;
  quota?: Quota;
  limitQuota?: Quota;
}

export interface PreSubmitData {
  name: string;
  uuid?: string;
  display_name?: string;
  description?: string;
  clusters?: PreAddRegionData[];
  registerPath?: String;
  env?: String;
  limitQuota?: Quota;
  project_uuid?: string;
  updated_at?: string;
  admin_list?: Admin[];
  service_type?: string;
}

export interface ProjectExtra {
  name: string;
  admin_list?: Admin[];
  clusters?: ClusterInProject[];
}

@Injectable()
export class ProjectService {
  private BASE_URL: string;
  private BASE_URL_V2: string;
  public project$: Observable<Project>;
  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) private account: Account,
  ) {
    this.BASE_URL = `/ajax/v1/projects/${account.namespace}/`;
    this.BASE_URL_V2 = `/ajax/v2/projects/${account.namespace}/`;
  }

  get() {
    return window.sessionStorage.getItem('project');
  }

  /**
   * Get project list.
   *
   * @param [Object] params
   *   - {boolean} verbose, default false, project clusters and admin_list information is not returned
   *   - {string} cluster_uuids, if verbose is true, cluster_uuids parameter has no effect
   */
  getProjects(
    params: {
      verbose?: boolean;
      page?: number;
      page_size?: number;
      search?: string;
      cluster_uuids?: string;
    } = {
      verbose: false,
    },
  ): Promise<{
    results?: Project[];
    count?: number;
    page_size?: number;
  }> {
    return this.httpService
      .request({
        url: this.BASE_URL_V2,
        method: 'GET',
        params,
        ignoreProject: true,
      })
      .then(res => {
        return res['result'] ? { results: res['result'] } : res;
      });
  }

  /**
   * Get project list with project clusters and admin_list information
   *
   * @param {string} project_names
   */
  getProjectsExtra(project_names: string): Promise<ProjectExtra[]> {
    return this.httpService
      .request({
        url: this.BASE_URL + 'extra',
        method: 'GET',
        params: {
          project_name: project_names,
        },
      })
      .then(res => res['results'] || [])
      .catch(() => null);
  }

  getProject(uuidOrName: string): Promise<Project> {
    return this.httpService
      .request({
        url: this.BASE_URL + uuidOrName,
        method: 'GET',
        ignoreProject: true,
      })
      .catch(() => null);
  }

  createProject(body: any) {
    // 占位
    if (!body.template) {
      body.template = 'empty-template';
    }
    return this.httpService.request(this.BASE_URL, {
      method: 'POST',
      body,
      ignoreProject: true,
    });
  }

  getProjectQuota(project_name: string, cluster_uuids: string = '') {
    return this.httpService.request({
      url: `${this.BASE_URL + project_name}/quota`,
      method: 'GET',
      params: {
        cluster_uuids,
      },
      ignoreProject: true,
    });
  }

  addProjectClusters(project_name: string, body: PreSubmitData) {
    return this.httpService.request(
      `${this.BASE_URL}${project_name}/clusters`,
      {
        method: 'POST',
        body,
        ignoreProject: true,
      },
    );
  }

  removeProjectClusters(project_name: string, clusters: string[]) {
    return this.httpService.request(
      `${this.BASE_URL}${project_name}/clusters`,
      {
        method: 'DELETE',
        body: {
          clusters,
        },
        ignoreProject: true,
      },
    );
  }

  updateProjectQuota(project_name: string, body: PreSubmitData) {
    return this.httpService.request({
      url: `${this.BASE_URL + project_name}/quota`,
      method: 'PUT',
      body,
      ignoreProject: true,
    });
  }

  updateProjectBasicInfo(uuid: string, body: any) {
    return this.httpService.request({
      url: this.BASE_URL + uuid,
      method: 'PUT',
      body,
      ignoreProject: true,
    });
  }

  deleteProject(uuid: string) {
    return this.httpService.request({
      url: this.BASE_URL + uuid,
      method: 'DELETE',
      ignoreProject: true,
    });
  }

  getProjectClustersQuota(uuid: string, cluster_uuids: string[] = []) {
    return this.httpService.request<ProjectClustersQuotaApi>({
      url: `${this.BASE_URL}${uuid}/quota`,
      method: 'GET',
      ignoreProject: true,
      params: {
        cluster_uuids,
      },
    });
  }

  getProjectNamespaceQuotas(
    project_name: string,
    cluster_uuid?: string,
  ): Promise<NamespaceQuotaWithSpace[]> {
    return this.httpService
      .request({
        url: `${
          this.BASE_URL
        }${project_name}/namespace?cluster_uuid=${cluster_uuid || ''}`,
        method: 'GET',
        ignoreProject: true,
      })
      .then(({ result }) => result);
  }

  getAdminRoles(params: {
    level?: 'platform' | 'project' | 'namespace' | 'space';
    project_name?: string;
    cluster_name?: string;
    k8s_namespace_name?: string;
    space_name?: string;
  }): Promise<AdminRoleApi[]> {
    return this.httpService
      .request(`/ajax/v1/role-admin/${this.account.namespace}/users`, {
        params,
        method: 'GET',
        ignoreProject: true,
      })
      .then(({ result }) => result);
  }

  getProjectAvailableResources(): Promise<ProjectAvailableResourceApi> {
    return this.httpService
      .request(`${this.BASE_URL}available-resources`)
      .then((result: any) => result);
  }

  getNamespacesWithSpaceInfo(data?: {
    name?: string;
    page_size?: number;
    page?: number;
    namepsace_name?: string;
    cluster_uuid?: string;
    space_name?: string;
  }): Promise<NamespaceWithQuotaInfoApi> {
    return this.httpService
      .request<NamespaceWithQuotaInfoApi>(
        `${this.BASE_URL}${this.get()}/namespace`,
        {
          method: 'GET',
          params: data,
        },
      )
      .then(res => {
        return res['result'] ? { results: res['result'] } : res;
      });
  }

  bindSpaceWithNamespace(data: SpaceWithNamespaceInfo[]) {
    return this.httpService.request({
      url: `${this.BASE_URL}${this.get()}/spaces-namespaces`,
      method: 'POST',
      body: {
        'spaces-namespaces': data,
      },
    });
  }

  unbindSpaceWithNamespace(data: SpaceWithNamespaceInfo[]) {
    return this.httpService.request({
      url: `${this.BASE_URL}${this.get()}/spaces-namespaces`,
      method: 'DELETE',
      body: {
        'spaces-namespaces': data,
      },
    });
  }
}
