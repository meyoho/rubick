import { Inject, Injectable } from '@angular/core';

import { AccountType } from 'app/services/api/account.service';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import { Account, Weblabs } from 'app/typings';
import {
  adaptLegacyPlatformRoleSchema,
  K8sTranslation,
  LegacyPlatformRoleSchema,
  LinkageMap,
  ResourceTypeFormModel,
  RoleApiGroup,
  RoleDetail,
  RolePermissionItem,
  RoleSchema,
  RoleTemplate,
  RoleTemplateDetail,
} from 'app2/features/rbac/backend-api';

@Injectable()
export class RBACService {
  private getRoleTemplateEndpoint(version = 'v1') {
    return `/ajax/${version}/role-templates/${this.account.namespace}/`;
  }

  private getRoleSchemaEndpoint(version = 'v1') {
    return `/ajax/${version}/role-schema/`;
  }

  private getRolesEndpoint(version = 'v1') {
    return `/ajax/${version}/roles/${this.account.namespace}/`;
  }
  private V3_URL = '/ajax/v3';

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) public account: Account,
    @Inject(WEBLABS) private weblabs: Weblabs,
  ) {}

  getTemplateList(
    /* pageNo: number, pageSize: number */ params: { level?: string } = null,
  ): Promise<RoleTemplate[]> {
    // ? V3 or V1 ?
    return this.httpService
      .request(this.getRoleTemplateEndpoint(), {
        method: 'GET',
        params,
      })
      .then(({ result }) => result);
  }

  getTemplateDetail(uuid: string): Promise<RoleTemplateDetail> {
    const version = this.weblabs.K8S_AUTH_ENABLED ? 'v3' : 'v1';
    return this.httpService.request(
      this.getRoleTemplateEndpoint(version) + uuid,
      {
        method: 'GET',
      },
    );
  }

  getTemplateScopes() {
    // ? V3 or V1 ?
    return this.httpService
      .request('/ajax/v1/role-templates-scopes/', {
        method: 'GET',
      })
      .then(({ result }) => result);
  }

  createTemplate(body: any, params?: any) {
    const version = this.weblabs.K8S_AUTH_ENABLED ? 'v3' : 'v1';
    return this.httpService.request(this.getRoleTemplateEndpoint(version), {
      method: 'POST',
      body,
      params,
    });
  }

  updateTemplate(body: any) {
    const version = this.weblabs.K8S_AUTH_ENABLED ? 'v3' : 'v1';
    return this.httpService.request(
      this.getRoleTemplateEndpoint(version) + body.uuid,
      {
        method: 'PUT',
        body,
      },
    );
  }

  deleteTemplate(uuid: string) {
    return this.httpService.request(this.getRoleTemplateEndpoint() + uuid, {
      method: 'DELETE',
    });
  }

  templateGenerate(uuid: string, params = { 'dry-run': true }) {
    // 该接口只有“V1”版本
    return this.httpService
      .request(this.getRoleTemplateEndpoint() + uuid + '/generate/', {
        method: 'POST',
        params,
      })
      .then(({ result }) => result);
  }

  getRoleSchema(): Promise<RoleSchema[]> {
    const version = this.weblabs.K8S_AUTH_ENABLED ? 'v3' : 'v1';
    return this.httpService
      .request(this.getRoleSchemaEndpoint(version), {
        method: 'GET',
      })
      .then(({ result }) => {
        if (version === 'v1') {
          result = (result as LegacyPlatformRoleSchema[]).map(item =>
            adaptLegacyPlatformRoleSchema(item),
          );
        }
        return result;
      });
  }

  getLinkageMap(): Promise<LinkageMap> {
    return this.httpService.request(
      `${this.V3_URL}/role-permission-map/${this.account.namespace}`,
      {
        method: 'GET',
      },
    );
  }

  getK8sSchemaTranslations(): Promise<K8sTranslation> {
    return this.httpService
      .request('/ajax/v3/role-k8s-schema-map', {
        method: 'GET',
      })
      .then(({ resource }) => resource);
  }

  getRoles(page_size: number = 20, page: number = 1, search: string = '') {
    // ? V3 or V1 ?
    // const version = this.weblabs.K8S_AUTH_ENABLED ? 'v3' : 'v1';
    return this.httpService.request(this.getRolesEndpoint(), {
      method: 'GET',
      params: { page_size, page, search },
      ignoreProject: true,
    });
  }

  getRole(role_name: string): Promise<RoleDetail> {
    const version = this.weblabs.K8S_AUTH_ENABLED ? 'v3' : 'v1';
    return this.httpService.request(
      this.getRolesEndpoint(version) + role_name,
      {
        method: 'GET',
      },
    );
  }

  getUsers(
    role_name: string,
    search: string = '',
    page: number = 1,
    page_size: number = 20,
  ) {
    return this.httpService.request(
      this.getRolesEndpoint() + role_name + '/users',
      {
        method: 'GET',
        params: { page, page_size, search },
      },
    );
  }

  deleteUser(role_name: string, body: any) {
    return this.httpService.request(
      this.getRolesEndpoint() + role_name + '/users',
      {
        method: 'DELETE',
        body,
      },
    );
  }

  assignUser(role_name: string, body: any) {
    return this.httpService.request(
      this.getRolesEndpoint() + role_name + '/users',
      {
        method: 'POST',
        body,
        ignoreProject: true,
      },
    );
  }

  addParent(role_name: string, body: any) {
    return this.httpService.request(
      this.getRolesEndpoint() + role_name + '/parents',
      {
        method: 'POST',
        body,
      },
    );
  }

  deleteParent(role_name: string, uuid: string) {
    return this.httpService.request(
      this.getRolesEndpoint() + role_name + `/parents/${uuid}`,
      {
        method: 'DELETE',
      },
    );
  }

  createRole(body: any, params?: any) {
    const version = this.weblabs.K8S_AUTH_ENABLED ? 'v3' : 'v1';
    return this.httpService.request(this.getRolesEndpoint(version), {
      method: 'POST',
      body,
      params,
      ignoreProject: true,
    });
  }

  updateRole(body: any, role_name: string) {
    const version = this.weblabs.K8S_AUTH_ENABLED ? 'v3' : 'v1';
    return this.httpService.request(
      this.getRolesEndpoint(version) + role_name,
      {
        method: 'PUT',
        body,
      },
    );
  }

  // 获取添加用户时可以选择的角色类型
  getValidRoleTypes({
    project_name = '',
    cluster_names = '',
    k8s_namespace_name = '',
    space_name = '',
    level = 'project',
    ignoreProject = true,
  }): Promise<AccountType[]> {
    return this.httpService
      .request({
        url: `/ajax/v1/role-types/${this.account.namespace}`,
        method: 'GET',
        params: {
          project_name,
          cluster_names,
          k8s_namespace_name,
          space_name,
          level,
        },
        ignoreProject,
      })
      .then(({ result }) => result);
  }

  getApiGroupList(): Promise<RoleApiGroup[]> {
    return this.httpService
      .request({
        url: `${this.V3_URL}/role-api-group`,
        method: 'GET',
      })
      .then(({ result }) => result);
  }

  createApiGroup(payload: { name: string }[]) {
    return this.httpService.request({
      url: `${this.V3_URL}/role-api-group`,
      method: 'POST',
      body: payload,
    });
  }

  updateApiGroup(name: string, payload: { name: string }) {
    return this.httpService.request({
      url: `${this.V3_URL}/role-api-group/${name}`,
      method: 'PUT',
      body: payload,
    });
  }

  deleteApiGroup(name: string) {
    return this.httpService.request({
      url: `${this.V3_URL}/role-api-group/${name}`,
      method: 'DELETE',
    });
  }

  getResourceType(
    apiGroupName: string,
    name: string,
  ): Promise<RolePermissionItem> {
    return this.httpService.request({
      url: `${this.V3_URL}/role-schema/${apiGroupName}/${name}`,
      method: 'GET',
    });
  }

  createResourceType(payload: ResourceTypeFormModel) {
    return this.httpService.request({
      url: `${this.V3_URL}/role-schema`,
      method: 'POST',
      body: payload,
    });
  }

  updateResourceType(payload: ResourceTypeFormModel) {
    return this.httpService.request({
      url: `${this.V3_URL}/role-schema/${payload.api_group}/${
        payload.resource_type
      }`,
      method: 'PUT',
      body: payload,
    });
  }

  deleteResourceType(apiGroupName: string, name: string) {
    return this.httpService.request({
      url: `${this.V3_URL}/role-schema/${apiGroupName}/${name}`,
      method: 'DELETE',
    });
  }
}
