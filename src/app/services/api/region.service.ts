import { Inject, Injectable } from '@angular/core';

import { isEqual } from 'lodash-es';
import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  first,
  map,
  publishReplay,
  refCount,
  retry,
} from 'rxjs/operators';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import * as clusterActions from 'app/store/actions/clusters.actions';
import { ClustersFacadeService } from 'app/store/facades/clusters.facade';
import { Account } from 'app/typings';

export enum RegionStatus {
  Running = 'RUNNING',
  Deploying = 'DEPLOYING',
  Preparing = 'PREPARING',
  Warning = 'WARNING',
  Error = 'ERROR',
  Critical = 'CRITICAL',
  Stopped = 'STOPPED',
}

export interface ClusterSelect {
  name: string;
  display_name: string;
}

export interface Mirror {
  id: string;
  name: string;
  display_name: string;
  description?: string;
  flag: string;
  regions: MirrorCluster[];
}

export interface MirrorCluster {
  name: string;
  display_name: string;
  id?: string;
}

export interface NodePayloadV2 {
  node_list: string[];
  ssh_port: number | '';
  ssh_username: string;
  ssh_password: string;
}

export enum ComponentStatus {
  Running = 'RUNNING',
  Error = 'ERROR',
  Unknwon = 'UNKNOWN',
}

export enum NodeState {
  Deploying = 'DEPLOYING',
  Running = 'RUNNING',
  Build = 'BUILD',
  Active = 'ACTIVE',
  Shutoff = 'SHUTOFF',
  ShuttingDown = 'SHUTTING_DOWN',
  Stopped = 'STOPPED',
  Stopping = 'STOPPING',
  Removed = 'REMOVED',
  Unknown = 'UNKNOWN',
  Offline = 'OFFLINE',
  Warning = 'WARNING',
  Error = 'ERROR',
  Critical = 'CRITICAL',
  Preparing = 'PREPARING',
  Draining = 'DRAINING',
}

export interface NodeVm {
  name: string;
  privateIp: string;
  labels: {
    [key: string]: string;
  };
  status: {
    state: 'Ready' | 'NotReady' | 'Warning';
    schedulable: boolean;
    taint: boolean;
  };
  cpu: number;
  memory: number | string;
  type: 'master' | 'node';
}

export interface ClusterAccessData {
  display_name: string;
  name: string;
  namespace: string;
  attr: {
    cluster: {
      nic: string;
    };
    docker: {
      path: string;
      version: string;
    };
    cloud: {
      name: string;
    };
    kubernetes: {
      type: string;
      endpoint: string;
      token: string;
      version: string;
      cni: {
        type: string;
      };
    };
  };
  features: {
    logs: {
      type: 'third-party';
      storage: {
        read_log_source: string;
        write_log_source: string;
      };
    };
    'service-catalog': {
      type: 'official';
    };
  };
}
export interface Cluster {
  display_name: string;
  name: string;
  namespace: string;
  created_at: string;
  container_manager: string;
  updated_at: string;
  platform_version: string;
  state: RegionStatus;
  env_uuid: string;
  type: string;
  id: string;
  attr: {
    cluser: {
      nic: string;
    };
    docker: {
      path: string;
      version: string;
    };
    cloud: {
      name: 'PRIVATE' | 'VMWARE';
    };
    kubernetes: {
      type: 'openshift' | 'original';
      endpoint: string;
      token: string;
      version: string;
      cni: {
        type: string;
        cidr: string;
        network_policy: string;
      };
    };
  };
  features: {
    logs: {
      type: 'official';
      storage: {
        read_log_source: string;
        write_log_source: string;
      };
    };
    ssh?: {
      type: string;
      integration_uuid: string;
    };
    customized?: {
      metric?: {
        grafana_admin_password: string;
        grafana_admin_user: string;
        grafana_url: string;
        name: string;
        namespace: string;
        prometheus_timeout: number;
        prometheus_url: string;
      };
    };
    'service-catalog': {
      type: 'official';
    };
  };
  mirror?: Mirror;
  resource_actions?: string[];
}

export interface ClusterNode {
  status: {
    volumes_attached: boolean;
    daemon_endpoints: {
      kubelet_endpoint: {
        port: number;
      };
    };
    capacity: {
      'alpha.kubernetes.io/nvidia-gpu': string;
      pods: string;
      cpu: string;
      memory: string;
    };
    addresses: Array<{
      type: string;
      address: string;
    }>;
    allocatable: {
      'alpha.kubernetes.io/nvidia-gpu': string;
      pods: string;
      cpu: string;
      memory: string;
    };
    images: Array<{
      size_bytes: number;
      names: string[];
    }>;
    node_info: {
      kube_proxy_version: string;
      operating_system: string;
      kernel_version: string;
      system_uuid: string;
      container_runtime_version: string;
      os_image: string;
      architecture: string;
      boot_id: string;
      machine_id: string;
      kubelet_version: string;
    };
    volumes_in_use: boolean;
    phase: string;
    conditions: Array<{
      last_heartbeat_time: string;
      status: string;
      last_transition_time: string;
      reason:
        | 'KubeletHasSufficientDisk'
        | 'KubeletHasSufficientMemory'
        | 'KubeletHasNoDiskPressure'
        | 'KubeletReady';
      message: string;
      type: 'OutOfDisk' | 'MemoryPressure' | 'DiskPressure' | 'Ready';
    }>;
  };
  kind: string;
  spec: {
    provider_id: string;
    taints: Array<{
      key: string;
      time_added: boolean;
      effect: string;
      value: string;
    }>;
    config_source: string;
    unschedulable: boolean;
    pod_cidr: string;
    external_id: string;
  };
  api_version: string;
  metadata: {
    name: string;
    owner_references: string;
    generation: string;
    namespace: string;
    labels: {
      'node-role.kubernetes.io/master': string;
      'kubernetes.io/hostname': string;
      'beta.kubernetes.io/os': string;
      'beta.kubernetes.io/arch': string;
    };
    generate_name: null;
    deletion_timestamp: null;
    cluster_name: null;
    finalizers: null;
    deletion_grace_period_seconds: null;
    initializers: null;
    self_link: string;
    resource_version: string;
    creation_timestamp: string;
    annotations: {
      'node.alpha.kubernetes.io/ttl': string;
      'flannel.alpha.coreos.com/public-ip': string;
      'flannel.alpha.coreos.com/backend-data': string;
      'flannel.alpha.coreos.com/kube-subnet-manager': string;
      'flannel.alpha.coreos.com/backend-type': string;
      'volumes.kubernetes.io/controller-managed-attach-detach': string;
    };
    uid: string;
  };
}

export interface ClusterNodes {
  kind: 'NodeList';
  api_version: 'v1';
  metadata: {
    self_link: string;
    resource_version: string;
  };
  items: ClusterNode[];
  resource_actions: string[];
}

export interface Label {
  key: string;
  value: string;
}

export interface NodeLabels {
  [key: string]: Label[];
}

export interface RegionFeature {
  config?: {
    type: 'official';
    application_uuid?: string;
    integration_uuid?: string;
  };
  new_version_avaliable: boolean;
  application_info?: {
    uuid: string;
    name: string;
    status: 'Running' | 'Warning' | 'Error' | 'Stopped' | 'Deploying';
  };
  integration_info?: {
    uuid: string;
    name: string;
    enabled: boolean;
  };
  template: {
    uuid: string;
    is_active: boolean;
    name: string;
    display_name: string;
    description: string;
    versions?: Array<{
      version_uuid: string;
      values_yaml_content: string;
    }>;
  };
}

export enum CniType {
  none = 'none',
  flannel = 'flannel',
  flannel_vxlan = 'flannel',
  flannel_host_gw = 'flannel',
  calico = 'calico',
  macVlan = 'macvlan',
}

export enum CniBackend {
  none = '',
  vxlan = 'vxlan',
  host_gw = 'host-gw',
}

export interface ClusterCreateModel {
  name: string;
  display_name: string;
  apiserver: string;
  cluster_type: {
    is_ha: boolean;
    loadbalancer: string;
  };
  masters: {
    ipaddress: string;
  }[];
  nodes: {
    ipaddress: string;
  }[];
  ssh: {
    type: 'secret' | 'password';
    name: string;
    secret: string;
    port: number | '';
  };
  cni: {
    type: CniType;
    backend: CniBackend;
    cidr: string;
    network_policy: string;
    macvlan_gateway?: string;
    macvlan_name?: string;
    macvlan_subnet_start?: string;
    macvlan_subnet_end?: string;
  };
}

export interface OverCommit {
  cpu: number;
  mem: number;
}

@Injectable()
export class RegionService {
  private REGIONS_V2_URL = `/ajax/v2/regions/${this.account.namespace}/`;
  private PASS_V2_URL = '/ajax/v2/paas/';
  private regionNameSubject: BehaviorSubject<string>;
  region$: Observable<Cluster>;
  regions$: Observable<Cluster[]>;
  // TODO: regionName$ 应该是 private 属性
  regionName$: Observable<string>;

  constructor(
    private httpService: HttpService,
    private clustersFacade: ClustersFacadeService,
    @Inject(ACCOUNT) private account: Account,
  ) {
    this.regionNameSubject = new BehaviorSubject<string>(
      window.sessionStorage.getItem('cluster'),
    );
    this.regions$ = this.clustersFacade.getClusters$();
    this.regionName$ = this.regionNameSubject.asObservable();

    this.region$ = combineLatest(this.regionName$, this.regions$).pipe(
      filter(([_, regions]) => !!regions),
      map(([regionName, regions]) =>
        regions.find(region => region.name === regionName),
      ),
      filter(c => !!c),
      distinctUntilChanged((region: Cluster, otherRegion: Cluster) =>
        isEqual(region, otherRegion),
      ),
      retry(10),
      publishReplay(1),
      refCount(),
    );
  }

  refetch() {
    this.clustersFacade.dispatch(new clusterActions.GetClusters());
  }

  setRegionByName(regionName: string) {
    if (window.sessionStorage.getItem('cluster') === regionName) {
      return;
    }
    window.sessionStorage.setItem('cluster', regionName);
    this.regionNameSubject.next(regionName);
  }

  getCurrentRegion(): Promise<Cluster> {
    return this.region$
      .pipe(
        filter(region => !!region),
        first(),
      )
      .toPromise();
  }

  /**
   * Folling methods refers to cluster API request
   */

  getRegions(): Promise<Cluster[]> {
    return this.httpService
      .request(this.REGIONS_V2_URL, {
        method: 'GET',
      })
      .then((resp: { result: Cluster[] }) => resp.result);
  }

  getClusterScripts(
    payload: ClusterCreateModel,
  ): Promise<{ commands: { install: string; uninstall: string } }> {
    return this.httpService.request(
      this.REGIONS_V2_URL + 'generate-installation-command',
      {
        method: 'POST',
        body: payload,
      },
    );
  }

  actionNodeV2(region: string, nodeName: string, action: string) {
    return this.httpService.request(
      `${this.REGIONS_V2_URL}${region}/nodes/${nodeName}/${action}`,
      {
        method: 'PUT',
      },
    );
  }

  deleteNodeV2(region: string, nodeName: string) {
    return this.httpService.request(
      `${this.REGIONS_V2_URL}${region}/nodes/${nodeName}`,
      {
        method: 'DELETE',
      },
    );
  }

  updateNodeLabelV2(
    region: string,
    nodeName: string,
    data: {
      labels: { [key: string]: string };
    },
  ) {
    return this.httpService.request(
      `${this.REGIONS_V2_URL}${region}/nodes/${nodeName}/labels`,
      {
        method: 'PUT',
        body: data,
      },
    );
  }

  getRegionLabelsV2(regionName: string): Promise<NodeLabels> {
    return this.httpService.request({
      method: 'GET',
      url: this.REGIONS_V2_URL + regionName + '/labels',
      addNamespace: true,
    });
  }

  /**
   *
   * @param nodeLabels
   * @param labelStrings
   * labelStrings 之间为 and 关系
   */
  checkNodeByLabels(nodeLabels: NodeLabels, labelStrings: Label[]) {
    if (!labelStrings || !nodeLabels) {
      return [];
    }
    const nodeLabelsArray = Object.entries(nodeLabels);
    const nodes: string[] = [];
    if (!labelStrings.length || !nodeLabelsArray.length) {
      return [];
    }
    nodeLabelsArray.forEach(
      ([node, labels]: [string, { key: string; value: string }[]]) => {
        const match = labelStrings.every(label => {
          return (
            labels.findIndex(
              t => t.key === label.key && t.value === label.value,
            ) >= 0
          );
        });
        if (match) {
          nodes.push(node);
        }
      },
    );
    return [...new Set(nodes)];
  }

  regionCheck(params: { endpoint: string; token: string }) {
    return this.httpService.request<{
      version: string;
    }>({
      method: 'POST',
      url: this.REGIONS_V2_URL + 'version-check',
      body: params,
    });
  }

  accessCluster(params: ClusterAccessData) {
    return this.httpService.request<Cluster>({
      method: 'POST',
      url: this.REGIONS_V2_URL,
      body: params,
    });
  }

  getCluster(name: string) {
    return this.httpService.request<Cluster>({
      method: 'GET',
      url: this.REGIONS_V2_URL + name,
    });
  }

  getClusterNodes(name: string) {
    return this.httpService.request<ClusterNodes>({
      method: 'GET',
      url: this.REGIONS_V2_URL + name + '/nodes',
    });
  }

  createClusterNodes(name: string, payload: NodePayloadV2) {
    return this.httpService.request<ClusterNodes>({
      method: 'POST',
      url: this.REGIONS_V2_URL + name + '/nodes',
      body: payload,
    });
  }

  updateCluster(name: string, payload: ClusterAccessData) {
    return this.httpService.request<Cluster>({
      method: 'PUT',
      url: this.REGIONS_V2_URL + name,
      params: {
        type: 'patch',
      },
      body: payload,
    });
  }

  deleteCluster(name: string, force?: boolean) {
    const url = force
      ? `${this.REGIONS_V2_URL}${name}?force=true`
      : `${this.REGIONS_V2_URL}${name}`;
    return this.httpService
      .request(url, {
        method: 'DELETE',
      })
      .then(() => name);
  }

  getRegionFeatures(regionName: string) {
    return this.httpService.request<{
      log: RegionFeature;
      metric: RegionFeature;
      registry: RegionFeature;
      ssh: RegionFeature;
    }>({
      method: 'GET',
      url: this.REGIONS_V2_URL + regionName + '/features',
    });
  }

  editRegionFeature(
    regionName: string,
    featureName: 'log' | 'metric' | 'registry' | 'ssh',
    feature: {
      config: {
        type: 'official' | 'prometheus';
        integration_uuid?: string;
        storage?: {
          read_log_source: 'default';
          write_log_source: 'default';
        };
      };
      values_yaml_content?: string;
    },
    isAddOrUpdate: boolean,
  ) {
    if (
      featureName === 'log' &&
      feature.config.type === 'official' &&
      !feature.config.storage
    ) {
      feature.config.storage = {
        read_log_source: 'default',
        write_log_source: 'default',
      };
    }
    return this.httpService.request<RegionFeature>({
      method: isAddOrUpdate ? 'POST' : 'PUT',
      url: this.REGIONS_V2_URL + regionName + '/features/' + featureName,
      body: feature,
    });
  }

  deleteRegionFeature(regionName: string, featureName: string) {
    return this.httpService.request({
      method: 'DELETE',
      url: this.REGIONS_V2_URL + regionName + '/features/' + featureName,
    });
  }

  getOverCommit(clusterName: string): Promise<OverCommit> {
    return this.httpService.request({
      method: 'GET',
      url: `${this.PASS_V2_URL}clusters/${clusterName}/info/resource-ratio`,
    });
  }

  updateOverCommit(clusterName: string, payload: OverCommit) {
    return this.httpService.request({
      method: 'PUT',
      url: `${this.PASS_V2_URL}clusters/${clusterName}/info/resource-ratio`,
      body: payload,
    });
  }
}
