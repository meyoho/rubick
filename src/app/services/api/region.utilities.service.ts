import { Injectable } from '@angular/core';

import { ClusterNode } from 'app/services/api/region.service';

export const ClusterStates = {
  Ready: 'Ready',
  NotReady: 'NotReady',
  Warning: 'Warning',
};

@Injectable()
export class RegionUtilitiesService {
  getNodeState(node: ClusterNode) {
    let ready: boolean;
    let warning: boolean;

    node.status.conditions.forEach(condition => {
      const status = condition.status.toLowerCase();
      if (condition.type === 'Ready') {
        ready = status === 'true';
      } else if (!warning) {
        warning = status === 'true';
      }
    });

    return warning ? 'Warning' : ready ? 'Ready' : 'NotReady';
  }
}
