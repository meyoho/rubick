import { Injectable } from '@angular/core';

import { from, Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { ImageTag, ListResult } from 'app/services/api/pipeline';
import { ImageRepository } from 'app/services/api/registry/registry-api.types';
import {
  mapFindRepositoriesResponseToList,
  mapResourceToRepository,
  mapResultToRepoTag,
} from 'app/services/api/registry/utils';
import { HttpService } from 'app/services/http.service';

const REPOSITORIES_URL = '/ajax/v2/devops/namespace/imagerepositories';

@Injectable()
export class RegistryApiService {
  constructor(private httpService: HttpService) {}

  findRepositoriesByProjectAndSpace(
    project: string,
    space: string,
    query: { [key: string]: string | number },
  ): Observable<ListResult<ImageRepository>> {
    query = {
      ...query,
      project_name: project,
      space_name: space,
    };
    return from(
      this.httpService.request(REPOSITORIES_URL, {
        method: 'GET',
        params: query,
      }),
    ).pipe(map(mapFindRepositoriesResponseToList));
  }

  getTags(
    project: string,
    space: string,
    name: string,
  ): Observable<ImageTag[]> {
    return from(
      this.httpService.request(`${REPOSITORIES_URL}/${name}/tags`, {
        method: 'GET',
        params: {
          project_name: project,
          space_name: space,
        },
      }),
    ).pipe(map(mapResultToRepoTag));
  }

  getRepository(
    project: string,
    space: string,
    name: string,
  ): Observable<ImageRepository> {
    return from(
      this.httpService.request(`${REPOSITORIES_URL}/${name}`, {
        method: 'GET',
        params: {
          project_name: project,
          space_name: space,
        },
      }),
    ).pipe(map(mapResourceToRepository));
  }

  triggerSecurityScan(
    project: string,
    space: string,
    repository: string,
    tag: string,
  ) {
    return from(
      this.httpService.request(`${REPOSITORIES_URL}/${repository}/security`, {
        method: 'POST',
        params: {
          project_name: project,
          space_name: space,
        },
        body: {
          tag,
          kind: 'ImageScanOptions',
        },
      }),
    );
  }
}
