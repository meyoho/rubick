import {
  ImageTag,
  K8SResource,
} from 'app/services/api/pipeline';
import { ResourceStatus } from 'app/services/api/tool-chain/tool-chain-api.types';

export interface ImageRepository {
  name: string;
  namespace: string;
  host: string;
  creationTimestamp: string;
  image: string;
  endpoint: string;
  type: string;
  link: string;
  tags: ImageTag[];
  status: ResourceStatus;
  secretName?: string;
  secretNamespace?: string;
  imageRegistry?: string;
  __original: K8SResource;
}

export interface CreateAppParams {
  repository_name: string;
  registry_endpoint: string;
  full_image_name?: string;
  secret: string;
  tag: string;
}
