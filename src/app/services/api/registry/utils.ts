import { get } from 'lodash-es';

import { ImageTag, K8SResource } from 'app/services/api/pipeline';
import { ImageRepository } from 'app/services/api/registry/registry-api.types';
import { ListResult } from 'app/services/api/tool-chain/tool-chain-api.types';

export function getFullImagePath(repo: ImageRepository) {
  return `${repo.endpoint}/${repo.image}`;
}

export function getFullTagPath(repo: ImageRepository, tag: ImageTag) {
  return `${getFullImagePath(repo)}:${tag.name}`;
}

export function getImagePathPrefix(repo: ImageRepository) {
  const fullPath = getFullImagePath(repo);
  return fullPath.slice(0, fullPath.lastIndexOf('/'));
}

export function getImagePathSuffix(repo: ImageRepository): string {
  const fullPath = getFullImagePath(repo);
  return fullPath.slice(fullPath.lastIndexOf('/'));
}

export function mapFindRepositoriesResponseToList(
  res: any,
): ListResult<ImageRepository> {
  return {
    total: get(res, 'count', 0),
    items: (res.results || res.result).map(mapResourceToRepository),
    errors: res.errors,
  };
}

export function mapResourceToRepository(res: K8SResource): ImageRepository {
  const meta = res.objectMeta || res.metadata;

  return {
    name: meta.name,
    namespace: meta.namespace,
    creationTimestamp: meta.creationTimestamp,
    image: get(res, 'spec.image', ''),
    endpoint: get(meta, 'annotations.imageRegistryEndpoint', ''),
    type: get(meta, 'annotations.imageRegistryType', ''),
    link: get(meta, 'annotations.imageRepositoryLink', ''),
    host: get(res, 'objectMeta.annotations.imageRegistryEndpoint', ''),
    status: {
      phase: get(res, 'status.phase', ''),
      message: get(res, 'status.message', ''),
    },
    tags: (get(res, 'status.tags') || []).map(mapDataToRepoTag),
    secretName: get(meta, 'annotations.secretName', ''),
    secretNamespace: get(meta, 'annotations.secretNamespace', ''),
    imageRegistry: get(meta, 'labels.imageRegistry', ''),
    __original: res,
  };
}

export function mapDataToRepoTag(data: any): ImageTag {
  const { created_at, updated_at, ...rest } = data;
  return {
    ...rest,
    createdAt: created_at,
    updatedAt: updated_at,
  };
}

export function mapResultToRepoTag(res: any): ImageTag[] {
  return res.tags.map(mapDataToRepoTag);
}
