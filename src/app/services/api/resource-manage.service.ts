import { NotificationService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { Params } from '@angular/router';

import { concat, get, includes, sortBy, uniqBy } from 'lodash-es';
import { BehaviorSubject, Observable, Subject } from 'rxjs';

import { FetchParams } from 'app/abstract';
import { AccountService } from 'app/services/api/account.service';
import { HttpService } from 'app/services/http.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, KubernetesResource } from 'app/typings';
import { ResourceList } from 'app_user/core/types';
import { RoleUtilitiesService } from './role-utilities.service';

export interface ResourceType {
  kind?: string;
  name?: string;
  namespaced?: boolean;
  verbs?: string[];
}

export interface ResourceManagementListFetchParams extends FetchParams {
  currentResourceType: ResourceType;
}

export interface Category {
  namespaced: ResourceType[];
  region: ResourceType[];
}

// TODO: delete
const FilterResourceKinds = [
  'configmaps',
  'cronjobs',
  'deployments',
  'endpoints',
  'events',
  'jobs',
  'persistentvolumeclaims',
  'pods',
  'replicasets',
  'replicationcontrollers',
  'routes',
  'secrets',
  'services',
  'serviceaccounts',
  'statefulsets',
  'roles',
  'rolebindings',
];

@Injectable()
export class RSRCManagementService {
  currentResourceType$: BehaviorSubject<ResourceType> = new BehaviorSubject<
    ResourceType
  >({ kind: '', name: '' });
  fetchParams$: Observable<ResourceManagementListFetchParams>;
  category$: Subject<Category> = new BehaviorSubject<Category>({
    namespaced: [],
    region: [],
  });
  currentResouceData$: Subject<
    K8sResourceWithActions<KubernetesResource>
  > = new BehaviorSubject<K8sResourceWithActions<KubernetesResource>>(null);

  clusterName: string;
  projectName: string;
  namespaceName: string;

  intialized = false;
  navLoading = false;
  createEnabled: boolean;

  constructor(
    private httpService: HttpService,
    private accountService: AccountService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
  ) {}

  isUserView(): boolean {
    return this.accountService.isUserView();
  }

  initScope(params: any) {
    this.resetScope();
    return this.isUserView()
      ? this.initUserViewScope(params)
      : this.initAdminViewScope(params);
  }

  private initAdminViewScope(scope: { cluster: string; namespace: string }) {
    this.clusterName = scope.cluster;
    this.namespaceName = scope.namespace;
    this.initCreateEnabled();
  }

  private initUserViewScope(scope: Params) {
    this.projectName = scope.project;
    this.clusterName = scope.cluster;
    this.namespaceName = scope.namespace;
    this.initCreateEnabled();
  }

  private resetScope() {
    this.projectName = '';
    this.clusterName = '';
    this.namespaceName = '';
  }

  private initCreateEnabled() {
    this.roleUtil
      .resourceTypeSupportPermissions(
        'k8s_others',
        {
          project_name: this.projectName || '',
          namespace_name: this.namespaceName || '',
          cluster_name: this.clusterName || '',
        },
        'create',
      )
      .then((res: boolean) => {
        this.createEnabled = res;
      });
  }

  initResourceTypeList$() {
    this.navLoading = true;
    return this.httpService
      .request(`/ajax/v2/misc/clusters/${this.clusterName}/resourcetypes`, {
        method: 'GET',
      })
      .then((res: any) => {
        this.navLoading = false;
        const rawCategory = res.result.reduce(this.reducer.bind(this), {
          namespaced: [],
          region: [],
        });
        const sortedCategory = this.sortCategory(rawCategory);
        if (!this.intialized && !this.currentResourceType$.getValue().kind) {
          this.setCurrentResourceType(sortedCategory.namespaced[0]);
          this.intialized = true;
        }
        this.category$.next(sortedCategory);
      })
      .catch(({ status, err }) => {
        this.navLoading = false;
        if (status === 403) {
          this.auiNotificationService.error({
            title: this.translate.get('permission_denied'),
          });
        } else {
          this.auiNotificationService.error(
            get(err, 'errors[0].message', this.translate.get('server_error')),
          );
        }
      });
  }

  private reducer(accum: { namespaced: any[]; region: any[] }, curr: any) {
    const allCategories = curr.resources.filter((item: any) => {
      return !includes(item.name, '/');
    });
    const namespacedKinds = allCategories
      .filter((item: ResourceType) =>
        this.isUserView() ? includes(FilterResourceKinds, item.name) : true,
      )
      .filter(
        (item: ResourceType) => item.namespaced && includes(item.verbs, 'get'),
      )
      .filter(
        (item: ResourceType) =>
          !includes(['applications', 'applicationhistories'], item.name),
      )
      .map((item: ResourceType) => {
        return {
          kind: item.kind,
          name: item.name,
          namespaced: item.namespaced,
          verbs: item.verbs,
        };
      });
    const regionKinds = allCategories
      .filter(
        (item: ResourceType) => !item.namespaced && includes(item.verbs, 'get'),
      )
      .map((item: ResourceType) => {
        return {
          kind: item.kind,
          name: item.name,
          namespaced: item.namespaced,
          verbs: item.verbs,
        };
      });
    return {
      namespaced: uniqBy(concat(accum.namespaced, namespacedKinds), 'kind'),
      region: uniqBy(concat(accum.region, regionKinds), 'kind'),
    };
  }

  /**
   * TODO: 资源类别排序，暂用首字母默认排序
   * @param rawCategory
   */
  private sortCategory(rawCategory: any) {
    return {
      namespaced: sortBy(rawCategory.namespaced, [
        (o: { kind: string; name: string }) => o.kind,
      ]),
      region: sortBy(rawCategory.region, [
        (o: { kind: string; name: string }) => o.kind,
      ]),
    };
  }

  setCurrentResourceType(newType: ResourceType): void {
    this.currentResourceType$.next(newType);
  }

  setCurrentResourceData(
    data: K8sResourceWithActions<KubernetesResource> | null,
  ) {
    this.currentResouceData$.next(data);
  }

  getResourceList(
    params: ResourceManagementListFetchParams,
  ): Promise<ResourceList> {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${this.clusterName}/${
        params.currentResourceType.name
      }${this.namespaceName ? `/${this.namespaceName}/` : ''}`,
      {
        method: 'GET',
        params: {
          name: params.search,
          page: params.pageParams.pageIndex,
          page_size: params.pageParams.pageSize,
        },
      },
    );
  }

  updateResource(params: {
    resourceTypeName: string;
    resourceName: string;
    data: KubernetesResource;
    namespace: string;
  }): Promise<any> {
    const customUrl = params.namespace
      ? `/${params.namespace}/${params.resourceName}`
      : `/${params.resourceName}`;
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${this.clusterName}/${
        params.resourceTypeName
      }${customUrl}`,
      {
        method: 'PUT',
        body: params.data,
      },
    );
  }

  createResource(params: { data: any[] }): Promise<any> {
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${this.clusterName}/resources`,
      {
        method: 'POST',
        body: params.data,
      },
    );
  }

  deleteResouce(params: {
    resourceTypeName: string;
    resourceName: string;
    namespace: string;
  }) {
    const customUrl = params.namespace
      ? `/${params.namespace}/${params.resourceName}`
      : `/${params.resourceName}`;
    return this.httpService.request(
      `/ajax/v2/kubernetes/clusters/${this.clusterName}/${
        params.resourceTypeName
      }${customUrl}`,
      {
        method: 'DELETE',
      },
    );
  }

  /**
   * Actions permission
   */
  hasPermission(resource_actions: string[], action: string): boolean {
    return resource_actions.some(permission =>
      includes(permission, `:${action}`),
    );
  }
}
