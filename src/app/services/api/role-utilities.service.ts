import { Inject, Injectable } from '@angular/core';

import { get, isArray } from 'lodash-es';

import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { ResourceType, Weblabs } from 'app/typings';
import { RoleService } from './role.service';

@Injectable()
export class RoleUtilitiesService {
  constructor(
    private roleService: RoleService,
    private translateService: TranslateService,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  getPermissionResourceDisplay(resource: string[]): string {
    if (!resource || !isArray(resource)) {
      return this.translateService.get('none');
    }
    return '<div>' + resource.join('</div><div>') + '</div>';
  }

  getPermissionActionsDisplay(actions: string[]): string {
    if (!actions || !isArray(actions)) {
      return this.translateService.get('none');
    }
    return '<div>' + actions.join('</div><div>') + '</div>';
  }

  /**
   * TODO: add types to constraint
   *
   * @param constraints
   * @returns {any}
   */
  getPermissionConstraintsDisplay(constraints: any[]): string {
    if (!constraints || !isArray(constraints) || !constraints.length) {
      return this.translateService.get('none');
    }
    const arr = constraints.reduce<string[]>((accum, constraintObject) => {
      const _str = Object.keys(constraintObject)
        .map(key => {
          return `<div>${key}:${constraintObject[key]}</div>`;
        })
        .join('');
      return [...accum, _str];
    }, []);

    return arr
      .map(block => {
        return `<div class="rb-permission-constraints-block">${block}</div>`;
      })
      .join('');
  }

  resourceHasPermission(
    resource_obj: { privilege?: string; resource_actions?: string[] },
    resource_type: ResourceType,
    action: string,
  ): boolean {
    if (!resource_obj) {
      return false;
    }
    if (action === 'permission') {
      return false;
    } else {
      const resource_actions: string[] = get(
        resource_obj,
        'resource_actions',
        [],
      );
      return resource_actions.includes(`${resource_type}:${action}`);
    }
  }

  /**
   * Checks resource actions within a context.
   * @param resource_type
   * @param context
   * @param action
   */
  resourceTypeSupportPermissions(
    resource_type: ResourceType,
    context?: any,
    action?: string,
  ): Promise<boolean>;
  resourceTypeSupportPermissions(
    resource_type: ResourceType,
    context: any,
    action: string[],
  ): Promise<boolean[]>;

  async resourceTypeSupportPermissions(
    resource_type: ResourceType,
    context = {},
    action: string | string[],
  ) {
    // Default to 'create'
    action = action || 'create';

    if (isArray(action)) {
      const permissions = await this.roleService.getContextPermissions(
        resource_type,
        context,
      );
      return action.map(item => {
        return permissions.includes(`${resource_type}:${item}`);
      });
    } else {
      return (await this.roleService.getContextPermissions(
        resource_type,
        context,
      )).includes(`${resource_type}:${action}`);
    }
  }

  /**
   * Checks resource types actions within a context.
   */
  resourceTypesPermissions(
    resource_types: ResourceType[],
    context = {},
  ): Promise<{ [resourceType: string]: string[] }> {
    return this.roleService.getPluralContextPermissions(
      resource_types,
      context,
    );
  }
}
