import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account, ResourceType } from 'app/typings';
import { ResourceResponse } from 'app/typings/resource-list';
import { ResourceList } from 'app_user/core/types';

export interface RoleType {
  role_uuid: string;
  role_name: string;
  template_display_name: string;
  template_name?: string;
  template_uuid: string;
  space_name?: string;
  space_uuid?: string;
}

export interface TemplateType {
  display_name: string;
  name: string;
  uuid: string;
}
export interface RcRole {
  name?: string;
  uuid?: string;
  namespace?: string;
  admin_role?: boolean;
  role_uuid?: string;
  template_uuid?: string;
}
export interface RoleSyncEventMeta {
  event_uuid: string;
  event_type: string;
  status: 'pending' | 'syncing' | 'failed';
  code: number;
  role_name: string;
}
export interface RoleSyncEvent {
  role_uuid: string;
  events: RoleSyncEventMeta[];
}
export interface RoleSyncStatics {
  template_uuid: string;
  admin_role: boolean;
  uuid: string;
  created_at: string;
  namespace: string;
  updated_at?: string;
  assigned_at?: string;
  name: string;
}

export interface RoleResourceList extends ResourceList {
  results: Role[];
  permissions: {
    [key: string]: string[];
  };
}

export interface Role {
  uuid: string;
  name: string;
  namespace: string;
  admin_role: boolean;
  created_at: string;
  updated_at: string;
  project_name: string;
  project_uuid: string;
  space_name: string;
  space_uuid: string;
  resource_actions: string[];
  permissions: string[];
  sync_status?: string;
}

export interface RoleUser {
  role_uuid: string;
  role_name: string;
  user: string;
  namespace: string;
  assigned_at: string;
  updated_at: string;
  project_name: string;
  project_uuid: string;
  space_name: string;
  space_uuid: string;
}
@Injectable()
export class RoleService {
  ENDPOINT_ROLE = '/ajax/permissions/' + this.account.namespace + '/';
  ROLES_NAMESPACE_PREFIX = `/ajax/roles/${this.account.namespace}/`;
  ROLES_V3_PREFIX = `/ajax/v3/`;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  getContextPermissions(
    resource_type: ResourceType,
    context?: any,
  ): Promise<string[]> {
    return this.httpService
      .request<{ result: string[] }>(this.ENDPOINT_ROLE + resource_type, {
        method: 'GET',
        params: context,
        cache: false,
        ignoreProject: !!(context && context.project_name),
      })
      .then(data => data.result || [])
      .catch(() => []);
  }

  getPluralContextPermissions(
    resource_types: ResourceType[],
    context?: any,
  ): Promise<{ [resourceType: string]: string[] }> {
    return this.httpService.request<{ [resourceType: string]: string[] }>(
      this.ENDPOINT_ROLE,
      {
        method: 'POST',
        params: context,
        body: resource_types,
        cache: false,
      },
    );
  }

  getRoleList({
    search = '',
    page = 1,
    page_size = 20,
    action = 'get',
    project_name = '',
    ignoreProject = true,
  }): Promise<RoleResourceList> {
    return this.httpService.request(
      `/ajax/v2/roles/${this.account.namespace}/`,
      {
        method: 'GET',
        params: {
          search,
          page,
          page_size,
          order_by: 'created_at',
          action,
          project_name,
        },
        ignoreProject,
      },
    );
  }

  createRole(body: any) {
    return this.httpService.request(this.ROLES_NAMESPACE_PREFIX, {
      method: 'POST',
      body,
    });
  }

  getRole(role_name: string) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/`,
      {
        method: 'GET',
      },
    );
  }

  deleteRole(role_name: string) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/`,
      {
        method: 'DELETE',
      },
    );
  }

  addPermissionOfRole(role_name: string, body: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/permissions/`,
      {
        method: 'POST',
        body,
      },
    );
  }

  updatePermissionOfRole(role_name: string, permission_uuid: any, body: any) {
    return this.httpService.request(
      `${
        this.ROLES_NAMESPACE_PREFIX
      }${role_name}/permissions/${permission_uuid}`,
      {
        method: 'PUT',
        body,
      },
    );
  }

  deletePermissionOfRole(role_name: string, permission_uuid: any) {
    return this.httpService.request(
      `${
        this.ROLES_NAMESPACE_PREFIX
      }${role_name}/permissions/${permission_uuid}`,
      {
        method: 'DELETE',
      },
    );
  }

  addParent(role_name: string, body: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/parents/`,
      {
        method: 'POST',
        body,
      },
    );
  }

  deleteParent(role_name: string, parent_uuid: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/parents/${parent_uuid}`,
      {
        method: 'DELETE',
      },
    );
  }

  getRoleSchemaList() {
    return this.httpService
      .request<{ result: any[] }>('/ajax/role-schema/', {
        method: 'GET',
      })
      .then(data => data.result || [])
      .catch(() => {
        return [];
      });
  }

  getRoleSchema(resource_type = '') {
    return this.httpService.request({
      method: 'GET',
      url: `/ajax/role-schema/${resource_type}`,
    });
  }

  listRoleUsers({
    role_name = '',
    page = 1,
    page_size = 20,
    search = '',
  }): Promise<ResourceList<RoleUser>> {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        method: 'GET',
        params: {
          page,
          page_size,
          search,
        },
      },
    );
  }

  addRoleUsers(role_name: string, users: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        method: 'POST',
        body: users,
      },
    );
  }

  addUsers(role_name: string, users: any, params?: { [key: string]: any }) {
    return this.httpService.request(
      `/ajax/v1/roles/${this.account.namespace}/${role_name}/users`,
      {
        method: 'POST',
        body: users,
        params,
        ignoreProject: true,
      },
    );
  }

  deleteRoleUsers(role_name: string, users: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        method: 'DELETE',
        // headers: {
        //   'Content-Type': 'Application/json',
        // },
        body: users,
      },
    );
  }

  createRoleUser(role_name: string, body: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        // headers: {
        //   'Content-Type': 'Application/json',
        // },
        method: 'POST',
        body,
      },
    );
  }

  deleteRoleUser(role_name: string, body: any) {
    return this.httpService.request(
      `${this.ROLES_NAMESPACE_PREFIX}${role_name}/users/`,
      {
        method: 'DELETE',
        body,
      },
    );
  }

  getRoleSyncStatics(): Promise<RoleSyncStatics[]> {
    return this.httpService
      .request(
        `${this.ROLES_V3_PREFIX}role-sync-statics/${this.account.namespace}`,
        {
          method: 'GET',
        },
      )
      .then((data: ResourceResponse<RoleSyncStatics>) => data.result || []);
  }

  getRoleSyncEvents(ids: string[]): Promise<RoleSyncEvent[]> {
    return this.httpService
      .request(
        `${this.ROLES_V3_PREFIX}role-sync-events/${this.account.namespace}`,
        {
          method: 'POST',
          body: ids,
        },
      )
      .then((data: ResourceResponse<RoleSyncEvent>) => data.result || []);
  }

  roleSyncRetry(ids: string[]) {
    return this.httpService.request(
      `${this.ROLES_V3_PREFIX}role-sync-retry/${this.account.namespace}`,
      {
        method: 'POST',
        body: ids,
      },
    );
  }
}
