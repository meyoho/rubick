import { Inject, Injectable } from '@angular/core';

import { get } from 'lodash-es';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

export interface Space {
  name: string;
  display_name?: string;
  project_uuid?: string;
  uuid?: string;
  created_at?: string;
  updated_at?: string;
  namespaces?: Namespace[];
  devops?: {
    namespace: {
      name: string;
    };
  };
}

export interface SpacePostApi {
  project_uuid: string;
  name: string;
  display_name: string;
  uuid: string;
  created_at: string;
  updated_at: string;
  failed_users: {
    username: string;
    code: string;
  }[];
}

export interface Namespace {
  cluster_uuid: string;
  cluster_name: string;
  uuid: string;
  name: string;
  created_at: string;
  updated_at: string;
}

@Injectable()
export class SpaceService {
  private BASE_URL: string;

  constructor(
    private httpService: HttpService,
    @Inject(ACCOUNT) private account: Account,
  ) {
    this.BASE_URL = `/ajax/v2/spaces/${this.account.namespace}/`;
  }

  createSpace(space: Space): Promise<SpacePostApi> {
    return this.httpService.request(this.BASE_URL, {
      method: 'POST',
      body: space,
    });
  }

  getSpaces(params?: {
    name?: string;
    page_size?: number;
    page?: number;
    project_name?: string;
  }): Promise<{
    results?: Space[];
    count?: number;
    page_size?: number;
  }> {
    return this.httpService
      .request({
        url: this.BASE_URL,
        method: 'GET',
        params,
        ignoreProject: !!get(params, 'project'),
      })
      .then(res => {
        return res['result'] ? { results: res['result'] } : res;
      });
  }

  updateSpace(space_name: string, display_name: string) {
    return this.httpService.request({
      url: `${this.BASE_URL}space/${space_name}`,
      body: {
        display_name,
      },
      method: 'PUT',
    });
  }

  deleteSpace(space_name: string) {
    return this.httpService.request({
      url: `${this.BASE_URL}space/${space_name}`,
      method: 'DELETE',
    });
  }

  getCurrentSpace(space_name: string): Promise<Space> {
    return this.httpService.request({
      url: `${this.BASE_URL}space/${space_name}`,
      method: 'GET',
    });
  }
}
