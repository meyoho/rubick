import { Inject, Injectable } from '@angular/core';

import { keyBy, mapValues } from 'lodash-es';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import { PersistentVolume, PersistentVolumeClaim } from 'app/typings/raw-k8s';
import { BatchReponse } from 'app_user/core/types';

/**
 * @deprecated
 */
export interface Pv {
  kubernetes: PersistentVolume;
  resource_actions: string[];
}

/**
 * @deprecated
 */
export interface Pvc {
  kubernetes: PersistentVolumeClaim;
  resource_actions: string[];
}

/**
 * @deprecated
 */
export interface StorageClass {
  kubernetes: {
    apiVersion: string;
    kind: 'StorageClass';
    metadata: {
      name: string;
      creationTimestamp: string;
      uid: string;
      annotations?: {
        'storageclass.kubernetes.io/is-default-class'?: 'true' | 'false';
      };
    };
    parameters: {
      resturl: string;
      volumetype: string;
    };
    provisioner: string;
    reclaimPolicy: string;
  };
  resource_actions: string[];
}

/**
 * @deprecated
 */
export interface KubernetesPv {
  kind: string;
  spec: {
    storageClassName?: string;
    accessModes: ['ReadWriteOnce' | 'ReadOnlyMany' | 'ReadWriteMany'];
    capacity: {
      storage: string;
    };
    persistentVolumeReclaimPolicy: 'Recycle' | 'Retain' | 'Delete';
  };
  apiVersion: string;
  metadata: {
    labels?: any;
    name: string;
    annotations: {
      [key: string]: string;
    };
  };
}
export interface MirrorPvc {
  uuid: string;
  name: string;
  cluster_uuid: string;
  cluster_name: string;
  cluster_display_name: string;
  namespace_name: string;
}

@Injectable()
export class StorageService {
  VOLUMES_URL: string;
  STORAGE_URL: string;
  SNAPSHOTS_URL: string;
  PV_URL: string;
  PVC_URL: string;
  K8S_URL: string;

  constructor(
    private httpService: HttpService,
    private k8sResourceService: K8sResourceService,
    @Inject(ACCOUNT) account: Account,
  ) {
    const namespace = account.namespace;
    this.STORAGE_URL = `/ajax/v1/storage/${namespace}/`;
    this.PV_URL = '/ajax/v2/persistentvolumes/';
    this.PVC_URL = '/ajax/v2/persistentvolumeclaims/';
    this.K8S_URL = '/ajax/v2/kubernetes/clusters/';
  }
  async getMirrorPvcs(
    clusterName: string,
    namespace: string,
    name: string,
  ): Promise<any> {
    const MirrorPvcs: MirrorPvc[] = [];
    const mirrorCluster = await this.k8sResourceService.getMirrorCluster(
      clusterName,
    );
    if (mirrorCluster.length) {
      const clusters = mirrorCluster.map((item: any) => {
        return item.name;
      });
      const res: BatchReponse = await this.k8sResourceService.getMirrorResources(
        clusters,
        namespace,
        'persistentvolumeclaims',
        name,
      );
      Object.entries(res.responses).forEach(([id, data]) => {
        if (data.code < 300) {
          const body = JSON.parse(data.body);
          const cluster = mirrorCluster.find(cluster => {
            return cluster.name === id;
          });
          const metadata = body.kubernetes.metadata;
          MirrorPvcs.push({
            uuid: metadata.uid,
            name: metadata.name,
            namespace_name: metadata.namespace,
            cluster_uuid: cluster ? cluster.id : '',
            cluster_name: cluster ? cluster.name : '',
            cluster_display_name: cluster ? cluster.display_name : '',
          });
        }
      });
    }
    return MirrorPvcs;
  }

  async deleteMirrorPvc(name: string, mirrorPvcs?: MirrorPvc[]) {
    if (mirrorPvcs && mirrorPvcs.length) {
      const res: BatchReponse = await this.deletePvcBatch(mirrorPvcs);
      this.k8sResourceService.reourceBatchNotification(
        'cluster_pvc_delete',
        res,
        name,
      );
      return;
    }
  }

  deletePvcBatch(mirrorPvcs: MirrorPvc[]): Promise<BatchReponse> {
    const project_name = window.sessionStorage.getItem('project');
    const requests = mirrorPvcs.map((item: MirrorPvc) => {
      return {
        cluster: item.cluster_name,
        url: `/v2/kubernetes/clusters/${
          item.cluster_name
        }/persistentvolumeclaims/${item.namespace_name}/${
          item.name
        }?project_name=${project_name}`,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  // StorageClass
  createStorageClass(cluster_id: string, payload: any): Promise<StorageClass> {
    return this.httpService
      .request(`${this.K8S_URL}${cluster_id}/storageclasses/`, {
        method: 'POST',
        body: payload,
        ignoreProject: true,
      })
      .then((ret: { result: StorageClass[] }) => {
        if (ret.result && ret.result.length) {
          return ret.result[0];
        }
      });
  }

  getStorageClasses({
    cluster_id,
    page = 1,
    page_size = 20,
    name = '',
  }: {
    cluster_id: string;
    page: number;
    page_size: number;
    name?: string;
  }): Promise<any> {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/storageclasses/`,
      {
        method: 'GET',
        params: {
          page,
          page_size,
          name,
        },
        ignoreProject: true,
      },
    );
  }

  getStorageClassDetail(cluster_id: string, name: string) {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/storageclasses/${name}`,
      {
        method: 'GET',
        ignoreProject: true,
      },
    );
  }

  updateStorageClass(cluster_id: string, name: string, payload: any) {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/storageclasses/${name}`,
      {
        method: 'PUT',
        body: payload,
        ignoreProject: true,
      },
    );
  }

  deleteStorageClass(cluster_id: string, name: string) {
    return this.httpService.request(
      `${this.K8S_URL}${cluster_id}/storageclasses/${name}`,
      {
        method: 'DELETE',
        ignoreProject: true,
      },
    );
  }
}
