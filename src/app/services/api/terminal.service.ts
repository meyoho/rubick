import { Injectable } from '@angular/core';

import { TerminalPageParams } from 'terminal/app/type';
@Injectable()
export class TerminalService {
  constructor() {}

  openTerminal(params: TerminalPageParams) {
    params.type = 'exec';
    const terminalurl = Object.entries(params).reduce(
      (urlParams, [key, value]) => {
        return `${urlParams}${key}=${value}&`;
      },
      '/terminal?',
    );

    this.openWindow(terminalurl);
  }

  openWebSSHTerminal(url: string, target: string) {
    const terminalurl = `/terminal?url=${encodeURIComponent(
      url,
    )}&target=${encodeURIComponent(target)}&type=webssh`;

    this.openWindow(terminalurl);
  }

  private openWindow(url: string) {
    // Following is to center the new window.
    const w = 800;
    const h = 600;

    const dualScreenLeft =
      window.screenLeft !== undefined ? window.screenLeft : window.screenX;
    const dualScreenTop =
      window.screenTop !== undefined ? window.screenTop : window.screenY;

    const width = window.innerWidth
      ? window.innerWidth
      : document.documentElement.clientWidth
      ? document.documentElement.clientWidth
      : screen.width;
    const height = window.innerHeight
      ? window.innerHeight
      : document.documentElement.clientHeight
      ? document.documentElement.clientHeight
      : screen.height;

    const left = width / 2 - w / 2 + dualScreenLeft;
    const top = height / 2 - h / 2 + dualScreenTop;

    window.open(
      url,
      '_blank',
      `width=${w},height=${h},resizable=yes,left=${left},top=${top}`,
    );
  }
}
