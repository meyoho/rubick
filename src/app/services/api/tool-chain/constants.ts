export const ANNOTATION_PREFIX = 'alauda.io';
export const API_GROUP = 'devops.alauda.io';
export const API_VERSION = 'v1alpha1';
export const API_GROUP_VERSION = `${API_GROUP}/${API_VERSION}`;
export const RESOURCE_CLASS = 'cluster';
export const TOOL_CHAIN_URL = '/ajax/v2/devops';
