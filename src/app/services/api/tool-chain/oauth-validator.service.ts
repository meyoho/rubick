import { Injectable, NgZone } from '@angular/core';

import { plural } from 'pluralize';
import { Observable, Subject } from 'rxjs';
import { take, tap } from 'rxjs/operators';

import {
  RESOURCE_CLASS,
  TOOL_CHAIN_URL,
} from 'app/services/api/tool-chain/constants';
import { HttpService } from 'app/services/http.service';

@Injectable()
export class OauthValidatorService {
  private secretValidateCode$ = new Subject<string>();

  constructor(private httpService: HttpService, private ngZone: NgZone) {
    (window as any).acceptCode = (code: string) => {
      this.ngZone.run(() => {
        this.secretValidateCode$.next(code);
      });
    };
  }

  validate(url: string): Observable<string> {
    const validateWindow = window.open(url, '_blank');
    return this.secretValidateCode$.pipe(
      take(1),
      tap(() => {
        validateWindow.close();
      }),
    );
  }

  callback(
    info: {
      name: string;
      kind: string;
      secretName: string;
      namespace: string;
    },
    code: string,
  ) {
    const kind = plural(info.kind.toLowerCase());
    const url = `${TOOL_CHAIN_URL}/${RESOURCE_CLASS}/${kind}/${
      info.name
    }/authorize`;
    return this.httpService.request(url, {
      method: 'POST',
      body: {
        kind: `${info.kind}AuthorizeCreate`,
        secretName: info.secretName,
        namespace: info.namespace,
        code,
        getAccessToken: true,
      },
    });
  }
}
