import { Injectable } from '@angular/core';

import { chunk } from 'lodash-es';
import { addIrregularRule, plural } from 'pluralize';
import { Observable } from 'rxjs';
import { fromPromise } from 'rxjs/internal-compatibility';
import { map } from 'rxjs/operators';

import { CredentialIdentity } from 'app/services/api/credential.service';
import {
  RESOURCE_CLASS,
  TOOL_CHAIN_URL,
} from 'app/services/api/tool-chain/constants';
import {
  AllocateInfo,
  ArtiFactDetail,
  ArtiFactDetailResponse,
  BlobItems,
  ListResult,
  NormalResponse,
  RepositoryItems,
  ResourceData,
  ToolBindingReplica,
  ToolBindings,
  ToolIntegrateParams,
  ToolKind,
  ToolProjectName,
  ToolService,
  ToolType,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  createResourceBody,
  getCreateArtifactRegistryBody,
  mapIntegrateConfigToK8SResource,
  mapResourceToAllocateInfo,
  mapResourceToRegistryService,
  mapToArtifactDetail,
  mapToRepositoryName,
  mapToToolBindings,
  mapToToolService,
  mapToToolType,
  sort,
} from 'app/services/api/tool-chain/utils';
import { HttpService } from 'app/services/http.service';
import { KubernetesResource } from 'app/typings';

addIrregularRule('jenkins', 'jenkinses');

@Injectable()
export class ToolChainApiService {
  constructor(private httpService: HttpService) {}

  getToolChains(): Promise<ToolType[]> {
    return this.httpService
      .request(`${TOOL_CHAIN_URL}/settings`, { method: 'GET' })
      .then((res: any) => (res.toolChains || []).map(mapToToolType))
      .catch(() => null);
  }

  findToolServices(
    type = '',
    subscription: any = null,
  ): Promise<ListResult<ToolService>> {
    return this.httpService
      .request(`${TOOL_CHAIN_URL}/toolchain`, {
        method: 'GET',
        params: {
          tool_type: type,
          subscription,
        },
      })
      .then((res: any) => ({
        total: res.listMeta.totalItems,
        items: (res.items || []).map(mapToToolService),
        errors: res.errors,
      }))
      .catch(() => null);
  }

  integrateTool(kind: ToolKind, data: ToolIntegrateParams) {
    return this.createService(kind, data);
  }

  deleteTool(kind: ToolKind, name: string): Promise<void> {
    return this.deleteService(kind, name);
  }

  updateTool(kind: ToolKind, config: ToolIntegrateParams) {
    return this.updateService(kind, config);
  }

  private createService(kind: ToolKind, data: ToolIntegrateParams) {
    const resource_class = RESOURCE_CLASS;
    const resource_type = plural(kind.toLowerCase());
    return this.httpService.request(
      `/ajax/v2/devops/${resource_class}/${resource_type}`,
      {
        method: 'POST',
        body: mapIntegrateConfigToK8SResource(kind, data),
      },
    );
  }

  private deleteService(kind: ToolKind, name: string): Promise<void> {
    const resource_class = RESOURCE_CLASS;
    const resource_type = plural(kind.toLowerCase());
    return this.httpService.request(
      `/ajax/v2/devops/${resource_class}/${resource_type}/${name}`,
      {
        method: 'DELETE',
      },
    );
  }

  private updateService(kind: ToolKind, config: ToolIntegrateParams) {
    const resource_class = RESOURCE_CLASS;
    const resource_type = plural(kind.toLowerCase());
    const name = config.name;
    return this.httpService.request(
      `/ajax/v2/devops/${resource_class}/${resource_type}/${name}`,
      {
        method: 'PUT',
        body: mapIntegrateConfigToK8SResource(kind, config),
      },
    );
  }

  getService(kind: ToolKind, name: string) {
    const resource_class = RESOURCE_CLASS;
    const resource_type = plural(kind.toLowerCase());
    return this.httpService
      .request(`/ajax/v2/devops/${resource_class}/${resource_type}/${name}`, {
        method: 'GET',
      })
      .then((res: any) => mapResourceToRegistryService(kind, res));
  }

  getAllocateByToolName(
    query: {
      page: number;
      page_size?: number;
      keyword?: string;
      order_by?: string;
    },
    toolName: string,
  ): Observable<{ items: AllocateInfo[]; length: number }> {
    const params = {
      ...query,
      labelSelector: `alauda.io/toolName=${toolName}`,
    };
    if (query.keyword) {
      params.labelSelector = `${params.labelSelector},alauda.io/project=${
        query.keyword
      }`;
    }
    return fromPromise(
      this.httpService.request(
        `/ajax/v2/devops/${RESOURCE_CLASS}/toolbindingreplicas`,
        {
          method: 'GET',
          params,
        },
      ),
    ).pipe(
      map((res: any) => {
        return {
          items: (res.results || []).map(mapResourceToAllocateInfo),
          length: res.count,
        };
      }),
    );
  }

  allocate(resource: ToolBindingReplica, project: string) {
    return this.httpService.request(
      '/ajax/v2/devops/namespace/toolbindingreplicas',
      {
        method: 'POST',
        params: {
          project_name: project,
        },
        body: resource,
      },
    );
  }

  deleteAllocate(item: AllocateInfo) {
    return this.httpService.request(
      `/ajax/v2/devops/namespace/toolbindingreplicas/${item.name}`,
      {
        method: 'DELETE',
        params: {
          project_name: item.project,
        },
      },
    );
  }

  updateAllocate(resource: ToolBindingReplica, project: string) {
    return this.httpService.request(
      `/ajax/v2/devops/namespace/toolbindingreplicas/${resource.metadata.name}`,
      {
        method: 'PUT',
        params: {
          project_name: project,
        },
        body: resource,
      },
    );
  }

  subscribeTool(payload: ToolBindingReplica) {
    const url = `${TOOL_CHAIN_URL}/namespace/toolbindingreplicas`;
    return this.httpService.request(url, {
      method: 'POST',
      body: payload,
    });
  }

  updateSubscription(payload: ToolBindingReplica) {
    const url = `${TOOL_CHAIN_URL}/namespace/toolbindingreplicas/${
      payload.metadata.name
    }`;
    return this.httpService
      .request(url, {
        method: 'PUT',
        body: payload,
      })
      .then((res: KubernetesResource) => {
        return mapResourceToAllocateInfo(res);
      });
  }

  getSubscriptions(params: { order_by: string }) {
    const url = `${TOOL_CHAIN_URL}/namespace/toolbindingreplicas`;
    return this.httpService
      .request(url, {
        method: 'GET',
        params,
      })
      .then((res: any) => {
        return (res.result || []).map(mapResourceToAllocateInfo);
      });
  }

  getSubscription(name: string) {
    const url = `${TOOL_CHAIN_URL}/namespace/toolbindingreplicas/${name}`;
    return this.httpService
      .request(url, {
        method: 'GET',
      })
      .then((res: any) => {
        return mapResourceToAllocateInfo(res);
      });
  }

  deleteSubscription(name: string) {
    const url = `${TOOL_CHAIN_URL}/namespace/toolbindingreplicas/${name}`;
    return this.httpService.request(url, {
      method: 'DELETE',
    });
  }

  getPrivateResources(name: string, project: string) {
    const url = `${TOOL_CHAIN_URL}/namespace/toolbindingreplicas/${name}/projects`;
    return this.httpService.request(url, {
      method: 'GET',
      params: {
        project_name: project,
      },
    });
  }

  /*
   * jira:
   * {
   *   name: 'test-name',
   *   kind: 'projectmanagement',
   * }
   *
   * gitlab:
   * {
   *   name: 'int-gitlab-ce',
   *   kind: 'codereposervice',
   *   secret: {
   *     secretname: 'int-gitlab-ce',
   *     namespace: 'global-credentials',
   *   }
   * }
   * */

  getResources(data: {
    name: string;
    kind: string;
    secret?: CredentialIdentity;
  }) {
    const resource_class = RESOURCE_CLASS;
    const resource_type = plural(data.kind.toLowerCase());

    const project_name = ToolProjectName[data.kind.toLowerCase()];
    // const url = `${TOOL_CHAIN_URL}/cluster/codereposervices/int-gitlab-ce/projects?secretname=int-gitlab-ce&namespace=global-credentials`
    const url = `${TOOL_CHAIN_URL}/${resource_class}/${resource_type}/${
      data.name
    }/${project_name}`;
    const params = {
      secretname: data.secret.name,
      namespace: data.secret.namespace,
      page: 1,
      pagesize: 1000,
    };
    return this.httpService.request(url, {
      method: 'GET',
      params,
      ignoreProject: true,
    });
  }

  /*
   * jira:
   * {
   *   name: '',
   *   kind: 'projectmanagement',
   *   body: {
   *   "apiVersion": "devops.alauda.io/v1alpha1",
   *   "kind": "CreateProjectOptions",
   *   "name": "xiaosu",
   *   "data": {
   *   "description": "edisonqt create a project",
   *   "lead": "admin",
   *   "key": "XIAOSU"
   *   }
   *   }
   * }
   *
   * gitlab:
   * {
   *   name: 'int-gitlab-ce',
   *   kind: 'codereposervices',
   *   body: {
   *   	"kind": "CreateProjectOptions",
   *   	"secretname": "int-gitlab-ce",
   *   	"namespace": "global-credentials",
   *   	"name": "a6-dev"
   *   }
   * }
   * */
  createResource(data: { resources: ResourceData; resourceName: string }) {
    const resource_class = RESOURCE_CLASS;
    const resource_type = plural(data.resources.kind.toLowerCase());
    const project_name = ToolProjectName[data.resources.kind.toLowerCase()]; // jira -> projectmanagement
    const url = `${TOOL_CHAIN_URL}/${resource_class}/${resource_type}/${
      data.resources.toolName
    }/${project_name}`;
    const params = data.resources.secretname
      ? {
          secretname: data.resources.secretname,
          namespace: data.resources.namespace,
        }
      : null;
    return this.httpService.request(url, {
      method: 'POST',
      body: createResourceBody({
        resources: data.resources,
        resourceName: data.resourceName,
      }),
      params,
      ignoreProject: true,
    });
  }

  getValidateStatus(info: {
    kind: string;
    name: string;
    secretName: string;
    namespace: string;
  }) {
    const kind = plural(info.kind.toLowerCase());
    const url = `${TOOL_CHAIN_URL}/${RESOURCE_CLASS}/${kind}/${
      info.name
    }/authorize`;
    return this.httpService.request(url, {
      method: 'GET',
      params: {
        secretName: info.secretName,
        namespace: info.namespace,
      },
    });
  }

  getToolBindings(
    projectName: string,
    spaceName: string,
    column: number,
  ): Promise<ToolBindings[][]> {
    const url = `${TOOL_CHAIN_URL}/namespace/toolbindings`;
    return this.httpService
      .request(url, {
        method: 'GET',
        params: {
          project_name: projectName,
          space_name: spaceName,
        },
      })
      .then((res: any) => (res.result || []).map(mapToToolBindings))
      .then((res: ToolBindings[]) => {
        const resFilter = res.filter(r => r.toolList.length > 0);
        return chunk(sort(resFilter), column);
      })
      .catch(() => null);
  }

  getArtifactBlobStore(managerName: string) {
    const url = `${TOOL_CHAIN_URL}/${RESOURCE_CLASS}/artifactregistrymanagers/${managerName}/blobstore`;
    return this.httpService
      .request(url, { method: 'GET' })
      .then((res: NormalResponse<BlobItems>) =>
        res.items.map(item => item.metadata.name),
      );
  }

  getArtifactList(managerName: string): Promise<ArtiFactDetail[]> {
    const url = `${TOOL_CHAIN_URL}/${RESOURCE_CLASS}/artifactregistries`;
    return this.httpService
      .request(url, {
        method: 'GET',
        params: {
          labelSelector: `alauda.io/artifactRegistryManager=${managerName}`,
        },
      })
      .then(({ result }) =>
        (result as ArtiFactDetailResponse[]).map(mapToArtifactDetail),
      );
  }

  getRepository(params: { managerName: string; type: string }) {
    const ArtifactType = params.type === 'Maven' ? 'Maven2' : params.type;
    const url = `${TOOL_CHAIN_URL}/${RESOURCE_CLASS}/artifactregistrymanagers/${
      params.managerName
    }/repository`;
    return this.httpService
      .request(url, {
        method: 'GET',
        params: {
          ArtifactType,
          IsFilterAR: true,
        },
      })
      .then((res: NormalResponse<RepositoryItems>) =>
        res.items.map(mapToRepositoryName),
      );
  }

  createArtifactRegistry<T>(params: { managerName: string; formData: T }) {
    const url = `${TOOL_CHAIN_URL}/${RESOURCE_CLASS}/artifactregistrymanagers/${
      params.managerName
    }/project`;
    return this.httpService.request(url, {
      method: 'POST',
      body: getCreateArtifactRegistryBody<T>(params.formData),
    });
  }
}
