import { CredentialType } from 'app/services/api/credential.service';
import { KubernetesResource, OwnerReference } from 'app/typings';

export interface ResourceStatus {
  phase: string;
  message: string;
}

export interface ResourceBinding {
  name: string;
  namespace: string;
  creationTimestamp: string;
  description: string;
  service: string;
  serviceType?: string;
  secret: string;
  secretType?: CredentialType;
  status: ResourceStatus;
  __original?: KubernetesResource;
}

export enum ToolKind {
  Jenkins = 'jenkins',
  CodeRepo = 'codereposervice',
  Registry = 'imageregistry',
  ArtifactRegistryManager = 'ArtifactRegistryManager',
  ArtifactRegistry = 'ArtifactRegistry',
}

export const ToolKindHump = {
  jenkins: 'Jenkins',
  codereposervice: 'CodeRepoService',
  imageregistry: 'ImageRegistry',
  projectmanagement: 'ProjectManagement',
  testtool: 'TestTool',
  codequalitytool: 'CodeQualityTool',
  documentmanagement: 'DocumentManagement',
  artifactRegistryManager: 'ArtifactRegistryManager',
  artifactRegistry: 'ArtifactRegistry',
};

export enum BindingKind {
  Jenkins = 'jenkinsbinding',
  CodeRepo = 'coderepobinding',
  Registry = 'imageregistrybinding',
}

export enum ToolProjectName {
  codereposervice = 'projects',
  imageregistry = 'projects',
  documentmanagement = 'projects',
  projectmanagement = 'remoteprojects',
}

export enum ToolResourceDisplayName {
  Gitlab = 'tool_chain_gitlab_resource',
  Github = 'tool_chain_github_resource',
  Bitbucket = 'tool_chain_bitbucket_resource',
  Gitee = 'tool_chain_gitee_resource',
  Jira = 'tool_chain_jira_resource',
  Taiga = 'tool_chain_taiga_resource',
  Confluence = 'tool_chain_confluence_resource',
  Harbor = 'tool_chain_harbor_resource',
  Nexus = 'tool_chain_nexus_resource',
  Maven2 = 'tool_chain_nexus_resource',
}

export interface ToolType {
  name: string;
  displayName: { [key: string]: string };
  enabled: boolean;
  items: Tool[];
  apiPath: string;
  subscribeMode?: boolean;
  projectName?: string;
}

export interface Tool {
  name: string;
  displayName: { [key: string]: string };
  toolType: string;
  shallow?: boolean;
  kind: ToolKind;
  type: string;
  host: string;
  html: string;
  public: boolean;
  enterprise: boolean;
  enabled: boolean;
  supportedSecretTypes?: SupportedSecretTypes[];
  roleSyncEnabled?: boolean;
}

export interface SupportedSecretTypes {
  type: string;
  description: { en: string; zh: string };
}

export interface ToolService {
  name: string;
  auth_type?: string;
  creationTimestamp?: string;
  toolType?: string;
  shallow?: boolean;
  subscription: boolean;
  integrateBy?: string;
  kind: ToolKind;
  type: string;
  host: string;
  html: string;
  visitHost: string;
  public: boolean;
  enterprise: boolean;
  status: ResourceStatus;
  __original: KubernetesResource;
  description?: string;
  selector?: {
    name: string;
    kind: string;
  };
  supportedSecretTypes?: SupportedSecretTypes[];
  roleSyncEnabled?: boolean;
  [key: string]: any;
}

export interface ToolBinding extends ResourceBinding {
  kind: BindingKind;
  tool: {
    toolType: string;
    type: string;
    kind: ToolKind;
    public: boolean;
    enterprise: boolean;
  };
}

export interface ListResult<T> {
  total: number;
  items: Array<T>;
  errors: any[];
}

export interface ToolIntegrateParams {
  name: string;
  apiHost: string;
  visitHost: string;
  allowSubscribe?: boolean;
  useSecret?: boolean;
  type?: string;
  public?: boolean;
  secret?: { [key: string]: string };
  resourceVersion?: string;
  secretType?: string;
  projectName?: string;
  artifactRegistryManager?: string;
  artifactRegistryArgs?: ArtifactRegistryArgs;
  ownerReferences?: OwnerReference[];
  roleSyncEnabled?: boolean;
}

export enum ShallowKind {
  ProjectManagment = 'projectmanagement',
  TestTool = 'testtool',
  CodeQualityTool = 'codequalitytool',
}

export interface ToolChainSecret {
  name: string;
  namespace: string;
}

export interface AllocateInfo {
  kind: string;
  auth_type?: string;
  toolItemType: string;
  toolItemKind: string;
  toolName: string;
  description: string;
  selector: {
    name: string;
    kind: string;
  };
  integrateBy: string;
  resources?: ToolResource[];
  secret?: {
    name: string;
    namespace: string;
  };
  name: string;
  namespace: string;
  creationTimestamp: string;
  type: string;
  host: string;
  visitHost: string;
  status: {
    phase: string;
    message: string;
  };
  bindings: AllocatedSpace[];
  project: string;
  createdMode: string;
  // toolItemProject: string; // integrate by
  __original: any;
}

export interface DevopsSpace {
  name?: string;
  displayName?: string;
  devopsNamespace?: string;
}
export interface AllocatedSpace {
  name: string;
  type?: string;
  allocatedSpaces: DevopsSpace[];
  allocatedToAll: boolean;
  enterprise?: string;
}

// tool chain allocate, update form model
export interface AllocateFormModel {
  tenant: {
    name: string;
  };
  resource?: { name: string; type?: string }[];
  name: string;
  description: string;
  selector: { name: string; kind: string };
  integrateBy: string;
  registry?: string;
  __original: any;
}

export interface ResourceDataOptions {
  mode: string;
  hideOptions: boolean;
}

export interface ResourceData {
  tenantName: string;
  lead?: string;
  toolName: string;
  type: string;
  kind: string;
  secretname?: string;
  namespace?: string;
  resourceNameList: string[];
  options: ResourceDataOptions;
}

// 工具的资源
export interface ToolResource {
  name: string;
  type?: string; // codeRepoService
}

export interface AuthorizeInfo {
  authorizeUrl: string;
  redirectUrl: string;
  kind: string;
  apiVersion: string;
  gotAccessToken: boolean;
}

export interface ToolBindingReplica extends KubernetesResource {
  spec: ToolBindingReplicaSpec;
  status?: {
    phase: string;
    conditions?: any[];
    lastAttempt?: string;
    lastUpdated?: string;
  };
}

export interface ToolBindingReplicaSpec {
  selector: {
    kind: string;
    name: string;
  };
  secret?: {
    namespace: string;
    name: string;
  };
}

// Harbor
export interface ToolBindingReplicaImageRegistrySpec
  extends ToolBindingReplicaSpec {
  ImageRegistry: {
    repoInfo: {
      repositories: string[];
    };
    template?: {
      bindings: ToolBindingReplicaImageRegistryBindings[];
    };
  };
}

export interface ToolBindingReplicaImageRegistryBindings {
  selector: {
    namespaces?: string[];
    matchExpressions?: any[];
  };
  spec: {
    imageRegistry: {
      name: '';
    };
    repoInfo: {
      repositories: string[];
    };
  };
}

// Jira, Taiga
export interface ToolBindingReplicaProjectManagementSpec
  extends ToolBindingReplicaSpec {
  projectManagement: {
    projects: { name: string }[];
    template?: {
      bindings: ToolBindingReplicaProjectManagementBindings[];
    };
  };
}

export interface ToolBindingReplicaProjectManagementBindings {
  selector: {
    matchExpressions?: any[];
    namespaces?: string[];
  };
  spec: {
    projectManagement: {
      name: ''; //空值 required
    };
    projectManagementProjectInfos: { name: string }[];
  };
}

// Gitlab
export interface ToolBindingReplicaCodeRepoServiceSpec
  extends ToolBindingReplicaSpec {
  codeRepoService: {
    owners: { name: string; type: string; id?: string }[];
    template?: {
      bindings: ToolBindingReplicaCodeRepoServiceBindings[];
    };
  };
}

export interface ToolBindingReplicaCodeRepoServiceBindings {
  selector: {
    matchExpressions?: any[];
    namespaces?: string[];
  };
  spec: {
    codeRepoService: {
      name: string;
    };
    account: {
      owners: { name: string; type: string }[];
    };
  };
}

// Jenkins
export interface ToolBindingReplicaJenkinsSpec extends ToolBindingReplicaSpec {
  continuousIntegration: {
    template: {
      bindings: ToolBindingReplicaJenkinsBindings[];
    };
  };
}

export interface ToolBindingReplicaJenkinsBindings {
  selector: {
    matchExpressions: any[];
  };
  spec: {
    jenkins: {
      name: string;
    };
  };
}

// Confluence
export interface ToolBindingReplicaDocumentManagementSpec
  extends ToolBindingReplicaSpec {
  documentManagement: {
    spaces: { name: string }[];
    template?: {
      bindings: ToolBindingReplicaDocumentManagementBindings[];
    };
  };
}

export interface ToolBindingReplicaDocumentManagementBindings {
  selector: {
    namespaces?: string[];
    matchExpressions?: any[];
  };
  spec: {
    documentManagement: {
      name: string;
    };
    documentManagementSpaceRefs: { name: string }[];
    secret?: { name: string; namespace: string };
  };
}

// Sornaqube
export interface ToolBindingReplicaCodeQualityToolSpec
  extends ToolBindingReplicaSpec {
  codeQualityTool: {
    template: {
      bindings: ToolBindingReplicaCodeQualityToolBindings[];
    };
  };
}

export interface ToolBindingReplicaCodeQualityToolBindings {
  selector: {
    matchExpressions: any[];
  };
  spec: {
    codeQualityTool: {
      name: string;
    };
  };
}

// ArtifactRegistry
export interface ToolBindingReplicaArtifactRegistrySpec
  extends ToolBindingReplicaSpec {
  artifactRegistry: {
    template: {
      bindings: ToolBindingReplicaArtifactRegistryBindings[];
    };
  };
}

export interface ToolBindingReplicaArtifactRegistryBindings {
  selector: {
    matchExpressions: any[];
  };
  spec: {
    artifactRegistry: {
      name: string;
    };
  };
}

export enum ToolBindingKind {
  CodeRepo = 'CodeRepoBinding',
}

export interface ToolBindings {
  index: number;
  displayKind: {
    en: string;
    zh: string;
  };
  toolList: ToolList[];
}

export interface ToolList {
  name: string;
  address: string;
  enterprise: boolean;
  type: string;
}

export interface NormalResponse<T> {
  items: Array<T>;
  kind: string;
  apiVersion: string;
  metadata: { [key: string]: string };
}

export interface BlobItems {
  Type: string;
  Name: string;
  metadata: { [key: string]: string };
}

export interface RepositoryItems {
  status: { [key: string]: string };
  spec: { [key: string]: string };
  metadata: { [key: string]: string };
}

export interface ArtiFactDetailResponse {
  status: { [key: string]: string };
  kind: string;
  spec: { [key: string]: string };
  apiVersion: string;
  metadata: { [key: string]: string };
}

export interface ArtiFactDetail {
  subscription: boolean | string;
  artifactType: string;
  artifactName: string;
  accessUrl: string;
  kind: string;
  integrateBy: string;
  status: ResourceStatus;
}

export interface ArtifactRegistryArgs {
  artifactRegistryName: string;
  artifactType: string;
  blobStore: string;
  public: string;
  type: string;
  versionPolicy: string;
}
