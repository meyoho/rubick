import { find, get, startCase } from 'lodash-es';

import {
  Credential,
  CredentialType,
} from 'app/services/api/credential.service';
import {
  ANNOTATION_PREFIX,
  API_GROUP_VERSION,
} from 'app/services/api/tool-chain/constants';
import {
  AllocatedSpace,
  AllocateFormModel,
  AllocateInfo,
  ArtiFactDetailResponse,
  ArtifactRegistryArgs,
  DevopsSpace,
  RepositoryItems,
  ResourceData,
  ShallowKind,
  ToolBindingKind,
  ToolBindingReplica,
  ToolBindingReplicaArtifactRegistrySpec,
  ToolBindingReplicaCodeQualityToolSpec,
  ToolBindingReplicaCodeRepoServiceSpec,
  ToolBindingReplicaImageRegistrySpec,
  ToolBindingReplicaJenkinsSpec,
  ToolBindingReplicaProjectManagementSpec,
  ToolBindings,
  ToolChainSecret,
  ToolIntegrateParams,
  ToolKind,
  ToolKindHump,
  ToolList,
  ToolResource,
  ToolResourceDisplayName,
  ToolService,
  ToolType,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import { KubernetesResource, OwnerReference } from 'app/typings';
import { CreateArtifactRegistryForm } from 'app2/features/tool-chain/create-repository-dialog/create-repository-dialog.component';
import { TenantCreateArtifactRegistryForm } from 'app_user/features/project/tool-chain/create-repository-dialog/create-repository-dialog.component';

export function mapToToolType(type: ToolType) {
  return {
    ...type,
    items: (type.items || [])
      .map(item => ({
        ...item,
        type: item.type || item.kind,
        toolType: type.name,
        shallow: Object.values(ShallowKind).includes(item.kind),
        supportedSecretTypes: item.supportedSecretTypes || [],
        roleSyncEnabled: item.roleSyncEnabled || false,
        enterprise: item.enterprise || false,
      }))
      .filter(
        item => item.supportedSecretTypes && item.supportedSecretTypes.length,
      ),
  };
}

const ENTERPRISE_KINDS = [ToolKind.CodeRepo];
export function mapToToolService(ins: any): ToolService {
  const kind = get(ins, 'kind', '');
  const isPublic = get(ins, 'spec.public', false);
  const isEnterprise =
    ENTERPRISE_KINDS.includes(kind.toLocaleLowerCase()) && !isPublic;

  return {
    kind,
    auth_type: get(
      ins,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/secretType`],
      '',
    ),
    name: get(ins, 'metadata.name', ''),
    creationTimestamp: get(ins, 'metadata.creationTimestamp', ''),
    toolType: get(
      ins,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolType`],
      '',
    ),
    shallow:
      get(
        ins,
        ['metadata', 'annotations', `${ANNOTATION_PREFIX}/shallow`],
        'false',
      ) === 'true',
    subscription:
      get(
        ins,
        ['metadata', 'annotations', `${ANNOTATION_PREFIX}/subscription`],
        'true',
      ) === 'true',
    integrateBy: get(
      ins,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolItemProject`],
      '',
    ),
    visitHost: get(
      ins,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/createAppUrl`],
      '',
    ),
    type: get(ins, 'spec.type', startCase(kind)),
    host: get(ins, 'spec.http.host', ''),
    html: get(ins, 'spec.http.html', ''),
    public: isPublic,
    enterprise: isEnterprise,
    status: {
      phase: get(ins, 'status.phase', '') || get(ins, 'status.status', ''),
      message: get(ins, 'status.message', ''),
    },
    secret: get(ins, 'spec.secret', {
      name: '',
      apiTokenKey: '',
    }),
    roleSyncEnabled:
      get(
        ins,
        ['metadata', 'annotations', `${ANNOTATION_PREFIX}/role.sync`],
        'false',
      ) === 'true',
    artifactRegistryManager: get(
      ins,
      ['metadata', 'labels', `${ANNOTATION_PREFIX}/artifactRegistryManager`],
      '',
    ),
    // description: get(ins, ['metadata', 'annotations', `${ANNOTATION_PREFIX}/description`], ''),
    __original: ins,
  };
}

export function mapIntegrateConfigToK8SResource(
  kind: ToolKind,
  data: ToolIntegrateParams,
): KubernetesResource {
  let result: KubernetesResource = {
    kind: ToolKindHump[kind] || kind,
    spec: {
      http: {
        host: data.apiHost,
        accessUrl: data.visitHost,
      },
      type: data.type,
      public: data.public,
    },
    apiVersion: API_GROUP_VERSION,
    metadata: {
      resourceVersion: data.resourceVersion,
      name: data.name,
      annotations: {
        'alauda.io/subscription': data.allowSubscribe.toString(),
        'alauda.io/createAppUrl': data.visitHost,
        'alauda.io/toolItemPublic': data.public.toString(),
        'alauda.io/toolItemProject': data.projectName || '',
        'alauda.io/role.sync': data.roleSyncEnabled.toString(),
      },
      labels: {
        'alauda.io/subscription': data.allowSubscribe.toString(),
      },
    },
  };
  if (data.useSecret) {
    Object.assign(result.spec, {
      secret: {
        namespace: data.secret.namespace,
        name: data.secret.name,
      },
    });
    result.metadata.annotations['alauda.io/secretType'] = data.secretType;
  }
  result = addArtifactRegistryPayload(
    result,
    data.artifactRegistryManager,
    data.artifactRegistryArgs,
    data.ownerReferences,
  );
  return result;
}

function addArtifactRegistryPayload(
  result: KubernetesResource,
  managerName: string,
  args: ArtifactRegistryArgs,
  ownerReferences: OwnerReference[],
) {
  if (result.kind === 'ArtifactRegistry') {
    result.metadata.labels['alauda.io/artifactRegistryManager'] = managerName;
    Object.assign(result.spec, {
      artifactRegistryArgs: args,
      artifactRegistryName: args.artifactRegistryName || '',
    });
    Object.assign(result.metadata, {
      ownerReferences,
    });
  }
  return result;
}

export function mapResourceToRegistryService(
  kind: ToolKind,
  resource: KubernetesResource,
) {
  const isPublic = get(resource, 'spec.public', false);
  const isEnterprise =
    ENTERPRISE_KINDS.includes(<ToolKind>(kind as string).toLocaleLowerCase()) &&
    !isPublic;
  return {
    kind,
    auth_type: get(
      resource,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/secretType`],
      '',
    ),
    secret: get(resource, ['spec', 'secret'], {
      name: '',
      namespace: '',
    }),
    subscription:
      get(
        resource,
        ['metadata', 'annotations', `${ANNOTATION_PREFIX}/subscription`],
        '',
      ) === 'true',
    name: get(resource, ['metadata', 'name'], ''),
    integrateBy: get(
      resource,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolItemProject`],
      '',
    ),
    projectName: get(
      resource,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolItemProject`],
      '',
    ),
    creationTimestamp: get(resource, ['metadata', 'creationTimestamp'], ''),
    type: get(resource, 'spec.type', kind.toLowerCase()),
    host: get(resource, 'spec.http.host', ''),
    visitHost: get(resource, ['spec', 'http', 'accessUrl'], ''),
    html: get(resource, 'spec.http.html', ''),
    public: isPublic,
    enterprise: isEnterprise,
    status: {
      phase: get(resource, 'status.phase', ''),
      message: get(resource, 'status.message', ''),
      conditions: get(resource, 'status.conditions', null),
    },
    resourceVersion: get(resource, ['metadata', 'resourceVersion'], ''),
    versionPolicy: get(
      resource,
      ['spec', 'artifactRegistryArgs', 'versionPolicy'],
      '',
    ),
    artifactRegistryManager: get(
      resource,
      ['metadata', 'labels', `${ANNOTATION_PREFIX}/artifactRegistryManager`],
      '',
    ),
    artifactRegistryArgs: get(
      resource,
      ['spec', 'artifactRegistryArgs'],
      {} as ArtifactRegistryArgs,
    ),
    roleSyncEnabled:
      get(
        resource,
        ['metadata', 'annotations', `${ANNOTATION_PREFIX}/role.sync`],
        'false',
      ) === 'true',
    ownerReferences: get(
      resource,
      ['metadata', 'ownerReferences'],
      [] as OwnerReference[],
    ),
    __original: resource,
  };
}

function getResourceList(resource: KubernetesResource): ToolResource[] {
  const kind = get(resource, [
    'metadata',
    'labels',
    `${ANNOTATION_PREFIX}/toolItemKind`,
  ]);
  switch (kind) {
    case 'ImageRegistry': {
      const projects = get(
        resource,
        ['spec', 'imageRegistry', 'repoInfo', 'repositories'],
        [],
      );
      return projects.map((p: string) => {
        return { name: p };
      });
    }
    case 'ProjectManagement': {
      const projects = get(
        resource,
        ['spec', 'projectManagement', 'projects'],
        [],
      );
      return projects.map((p: { name: string; id: string }) => {
        return { name: p.name };
      });
    }
    case 'DocumentManagement': {
      const project = get(
        resource,
        ['spec', 'documentManagement', 'spaces'],
        [],
      );
      return project;
    }
    case 'CodeRepoService': {
      const groups = get(resource, ['spec', 'codeRepoService', 'owners'], []);
      return groups.map((g: { name: string; id: string; type: string }) => {
        return { name: g.name, type: g.type };
      });
    }
    case 'ArtifactRegistry': {
      const registryName = get(resource, ['spec', 'selector', 'name'], '');
      return [{ name: registryName }];
    }
  }
}

export function mapFormModelToToolBindingReplica(
  formData: AllocateFormModel,
  spaceEnabled: boolean,
): ToolBindingReplica {
  const result = {
    apiVersion: API_GROUP_VERSION,
    kind: 'ToolBindingReplica',
    metadata: {
      name: formData.name,
      namespace: '',
      annotations: {
        'alauda.io/project': formData.tenant.name,
        'alauda.io/description': formData.description || '',
        'alauda.io/createdMode': 'assignment',
        'alauda.io/toolItemProject': formData.integrateBy || '',
      },
      labels: {
        'alauda.io/project': formData.tenant.name,
      },
    },
    spec: {
      selector: {
        name: formData.selector.name, // eg: jira-test
        kind: formData.selector.kind, // eg: ProjectManagement
      },
      ...formData.__original.spec,
    },
  };
  const allocateInfo = mapResourceToAllocateInfo(result);
  allocateInfo.resources = formData.resource;
  return assignToolBindingReplicaSpec(result, allocateInfo, spaceEnabled);
}

function getImageRegistryTemplateBinding(
  resource: AllocateInfo,
  spaceEnabled: boolean,
) {
  const repositories = resource.resources.map(item => item.name);
  const bindings = resource.resources.map(item => {
    return {
      selector: {
        matchExpressions: getMatchExpressions(resource.project),
      },
      spec: {
        imageRegistry: {
          name: '', //空值 required
        },
        repoInfo: {
          repositories: [item.name],
        },
      },
    };
  });
  const template = get(
    resource,
    ['__original', 'spec', 'imageRegistry', 'template'],
    null,
  );
  return {
    imageRegistry: {
      repoInfo: {
        repositories: repositories,
      },
      template: spaceEnabled
        ? template
        : {
            bindings,
          },
    },
    selector: resource.selector,
  };
}
function getProjectManagementTemplateBinding(
  resource: AllocateInfo,
  spaceEnabled: boolean,
) {
  const resources = resource.resources.map((item: any) => {
    return { name: item.name };
  });
  const bindings = resource.resources.map(item => {
    return {
      selector: {
        matchExpressions: getMatchExpressions(resource.project),
      },
      spec: {
        projectManagement: {
          name: '', //空值 required
        },
        projectManagementProjectInfos: [
          {
            name: item.name,
          },
        ],
      },
    };
  });
  const template = get(
    resource,
    ['__original', 'spec', 'projectManagement', 'template'],
    null,
  );

  return {
    projectManagement: {
      projects: resources,
      template: spaceEnabled
        ? template
        : {
            bindings,
          },
    },
    selector: resource.selector,
  };
}
function getDocumentManagementTemplateBinding(
  resource: AllocateInfo,
  spaceEnabled: boolean,
) {
  const resources = resource.resources.map((item: any) => {
    return { name: item.name };
  });
  const bindings = resources.map(item => {
    return {
      selector: {
        matchExpressions: getMatchExpressions(resource.project),
      },
      spec: {
        docmentManagement: {
          name: '', //空值 required
        },
        documentManagementSpaceRefs: [
          {
            name: item.name,
          },
        ],
      },
    };
  });
  const template = get(
    resource,
    ['__original', 'spec', 'documentManagement', 'template'],
    null,
  );

  return {
    documentManagement: {
      spaces: resources,
      template: spaceEnabled
        ? template
        : {
            bindings,
          },
    },
    selector: resource.selector,
  };
}
function getCodeRepoServiceTemplateBinding(
  resource: AllocateInfo,
  spaceEnabled: boolean,
) {
  const resources = resource.resources.map((item: any) => {
    return {
      name: item.name,
      type: item.type,
    };
  });
  const bindings = resource.resources.map(item => {
    return {
      selector: {
        matchExpressions: getMatchExpressions(resource.project),
      },
      spec: {
        codeRepoService: {
          name: '',
        },
        account: {
          owners: [
            {
              type: item.type,
              name: item.name,
            },
          ],
        },
      },
    };
  });
  const template = get(
    resource,
    ['__original', 'spec', 'codeRepoService', 'template'],
    null,
  );

  return {
    codeRepoService: {
      owners: resources,
      template: spaceEnabled
        ? template
        : {
            bindings,
          },
    },
    selector: resource.selector,
  };
}
function getJenkinsTemplateBinding(resource: AllocateInfo) {
  return {
    continuousIntegration: {
      template: {
        bindings: [
          {
            selector: {
              matchExpressions: getMatchExpressions(resource.project),
            },
            spec: {
              jenkins: {
                name: '',
              },
            },
          },
        ],
      },
    },
    selector: resource.selector,
  };
}
function getCodeQualityToolTemplateBinding(resource: AllocateInfo) {
  return {
    codeQualityTool: {
      template: {
        bindings: [
          {
            selector: {
              matchExpressions: getMatchExpressions(resource.project),
            },
            spec: {
              codeQualityTool: {
                name: '',
              },
            },
          },
        ],
      },
    },
    selector: resource.selector,
  };
}

function getArtifactRegistryTemplateBinding(
  resource: AllocateInfo,
  spaceEnabled: boolean,
) {
  const bindings = resource.resources.map(() => {
    return {
      selector: {
        matchExpressions: getMatchExpressions(resource.project),
      },
      spec: {
        artifactRegistry: {
          name: '',
        },
      },
    };
  });
  const template = get(
    resource,
    ['__original', 'spec', 'artifactRegistry', 'template'],
    null,
  );
  return {
    artifactRegistry: {
      template: spaceEnabled
        ? template
        : {
            bindings,
          },
    },
    selector: resource.selector,
  };
}

export function getResourceSpec(
  resource: AllocateInfo,
  spaceEnabled: boolean,
):
  | ToolBindingReplicaArtifactRegistrySpec
  | ToolBindingReplicaImageRegistrySpec
  | ToolBindingReplicaProjectManagementSpec
  | ToolBindingReplicaCodeRepoServiceSpec
  | ToolBindingReplicaJenkinsSpec
  | ToolBindingReplicaCodeQualityToolSpec {
  let spec: any;
  switch (resource.selector.kind) {
    case 'ImageRegistry':
      spec = getImageRegistryTemplateBinding(resource, spaceEnabled);
      break;
    case 'ProjectManagement':
      spec = getProjectManagementTemplateBinding(resource, spaceEnabled);
      break;
    case 'DocumentManagement':
      spec = getDocumentManagementTemplateBinding(resource, spaceEnabled);
      break;
    case 'CodeRepoService':
      spec = getCodeRepoServiceTemplateBinding(resource, spaceEnabled);
      break;
    case 'Jenkins':
      spec = getJenkinsTemplateBinding(resource);
      break;
    case 'CodeQualityTool':
      spec = getCodeQualityToolTemplateBinding(resource);
      break;
    case 'ArtifactRegistry':
      spec = getArtifactRegistryTemplateBinding(resource, spaceEnabled);
      break;
  }
  return spec;
}

export function assignToolBindingReplicaSpec(
  result: KubernetesResource,
  resource: AllocateInfo,
  spaceEnabled: boolean,
): ToolBindingReplica {
  const spec = getResourceSpec(resource, spaceEnabled);
  return Object.assign(result, { spec });
}

export function createResourceBody(data: {
  resources: ResourceData;
  resourceName: string;
}) {
  switch (data.resources.kind) {
    case 'ProjectManagement':
    case 'DocumentManagement': {
      const projectName = getProjectName(data.resources, data.resourceName);
      return {
        apiVersion: 'devops.alauda.io/v1alpha1',
        kind: 'CreateProjectOptions',
        name: projectName,
        data: {
          description: `${data.resources.tenantName} create ${projectName}`,
          key: data.resourceName.toUpperCase(),
        },
        secretname: data.resources.secretname,
        namespace: data.resources.namespace,
      };
    }
    case 'CodeRepoService':
    case 'ImageRegistry': {
      const projectName = getProjectName(data.resources, data.resourceName);
      return {
        kind: 'CreateProjectOptions',
        secretname: data.resources.secretname,
        namespace: data.resources.namespace,
        name: projectName,
      };
    }
  }
}

function getProjectName(resource: ResourceData, name: string) {
  if (name === resource.tenantName) {
    return name;
  }
  return `${resource.tenantName}-${name}`;
}

export function resourceSwitcher(type: string): boolean {
  const needResource = [
    'Jira',
    'Taiga',
    'Harbor',
    'Gitlab',
    'Gitee',
    'Github',
    'Bitbucket',
    'Confluence',
    'Maven2',
  ];
  return needResource.includes(type);
}

export function canCreateResource(type: string): boolean {
  const canCreateResource = [
    'Jira',
    'Taiga',
    'Harbor',
    'Gitlab',
    'Gitee',
    'Confluence',
  ];
  return canCreateResource.includes(type);
}

function projectManagementAllocateBindings(resource: KubernetesResource) {
  const projectResource = get(
    resource,
    ['spec', 'projectManagement', 'projects'],
    [],
  );
  const template = get(resource, ['spec', 'projectManagement', 'template']);
  const bindings = get(template, ['bindings'], []);
  return projectResource.map((item: { id: string; name: string }) => {
    const found = find(bindings, o => {
      const refs = get(o, ['spec', 'projectManagementProjectInfos'], []);
      return refs.length && refs[0].name === item.name;
    });
    if (!template || !found) {
      return {
        name: item.name,
        allocatedSpaces: [] as any[],
        allocatedToAll: false,
      };
    } else {
      const allocatedToAll = !!get(found, ['selector', 'matchExpressions']);
      return {
        name: get(found, ['spec', 'projectManagementProjectInfos'])[0].name,
        allocatedSpaces: allocatedToAll
          ? []
          : get(found, ['selector', 'namespaces']).map(
              mapDevopsNamespacesToDevopsSpace,
            ),
        allocatedToAll,
      };
    }
  });
}

function codeRepoServiceAllocateBindings(resource: KubernetesResource) {
  const codeResource = get(resource, ['spec', 'codeRepoService', 'owners'], []);
  const template = get(resource, ['spec', 'codeRepoService', 'template']);
  const bindings = get(template, ['bindings'], []);
  return codeResource.map(
    (item: { type: string; id?: string; name: string }) => {
      const found = find(bindings, o => {
        const refs = get(o, ['spec', 'account', 'owners'], []);
        return refs.length && refs[0].name === item.name;
      });
      if (!template || !found) {
        return {
          name: item.name,
          type: item.type,
          allocatedSpaces: [] as any[],
          allocatedToAll: false,
        };
      } else {
        const allocatedToAll = !!get(found, ['selector', 'matchExpressions']);
        const owners = get(found, ['spec', 'account', 'owners']);
        return {
          name: owners[0].name,
          type: owners[0].type,
          allocatedSpaces: allocatedToAll
            ? []
            : get(found, ['selector', 'namespaces']).map(
                mapDevopsNamespacesToDevopsSpace,
              ),
          allocatedToAll,
        };
      }
    },
  );
}

function imageRegistryAllocateBindings(resource: KubernetesResource) {
  const imageResource = get(
    resource,
    ['spec', 'imageRegistry', 'repoInfo', 'repositories'],
    [],
  );
  const template = get(resource, ['spec', 'imageRegistry', 'template']);
  const bindings = get(template, ['bindings'], []);

  return imageResource.map((item: string) => {
    const found = find(bindings, o => {
      const refs = get(o, ['spec', 'repoInfo', 'repositories'], []);
      return refs.length && refs[0] === item;
    });
    if (!template || !found) {
      return {
        name: item,
        allocatedSpaces: [] as any[],
        allocatedToAll: false,
      };
    } else {
      const allocatedToAll = !!get(found, ['selector', 'matchExpressions']);
      return {
        name: get(found, ['spec', 'repoInfo', 'repositories'])[0],
        allocatedSpaces: allocatedToAll
          ? []
          : get(found, ['selector', 'namespaces']).map(
              mapDevopsNamespacesToDevopsSpace,
            ),
        allocatedToAll,
      };
    }
  });
}

function mapDevopsNamespacesToDevopsSpace(namespace: string): DevopsSpace {
  return {
    devopsNamespace: namespace,
  };
}
function getDevopsNamespaceFromDevopsSpace(space: DevopsSpace): string {
  return space.devopsNamespace;
}

// Confluence
function documentmanagementBindings(resource: KubernetesResource) {
  const docResource = get(
    resource,
    ['spec', 'documentManagement', 'spaces'],
    [],
  );
  const template = get(resource, ['spec', 'documentManagement', 'template']);
  const bindings = get(template, ['bindings'], []);
  return docResource.map((item: { name: string }) => {
    const found = find(bindings, o => {
      const refs = get(o, ['spec', 'documentManagementSpaceRefs'], []);
      return refs.length && refs[0].name === item.name;
    });
    if (!template || !found) {
      return {
        name: item.name,
        allocatedSpaces: [] as any[],
        allocatedToAll: false,
      };
    } else {
      const allocatedToAll = !!get(found, ['selector', 'matchExpressions']);
      return {
        name: get(found, ['spec', 'documentManagementSpaceRefs'])[0].name,
        allocatedSpaces: allocatedToAll
          ? []
          : get(found, ['selector', 'namespaces']).map(
              mapDevopsNamespacesToDevopsSpace,
            ),
        allocatedToAll,
      };
    }
  });
}

function artifactRegistryBindings(resource: KubernetesResource) {
  const template = get(resource, ['spec', 'artifactRegistry', 'template']);
  const bindings = get(template, ['bindings'], []);
  const allocatedToAll = !!get(bindings[0], ['selector', 'matchExpressions']);
  const result = {
    name: get(resource, ['spec', 'selector', 'name'], ''),
    allocatedSpaces: allocatedToAll
      ? []
      : get(bindings[0], ['selector', 'namespaces'], []).map(
          mapDevopsNamespacesToDevopsSpace,
        ),
    allocatedToAll,
  };
  return [result];
}

export function getResourceAllocatedBindings(resource: KubernetesResource) {
  const kind = get(resource, ['spec', 'selector', 'kind'], '');
  let bindings = [];
  switch (kind) {
    case 'ProjectManagement':
      bindings = projectManagementAllocateBindings(resource);
      break;
    case 'CodeRepoService':
      bindings = codeRepoServiceAllocateBindings(resource);
      break;
    case 'ImageRegistry':
      bindings = imageRegistryAllocateBindings(resource);
      break;
    case 'DocumentManagement':
      bindings = documentmanagementBindings(resource);
      break;
    case 'ArtifactRegistry':
      bindings = artifactRegistryBindings(resource);
      break;
  }
  return bindings;
}

export function mapResourceToAllocateInfo(
  resource: KubernetesResource,
): AllocateInfo {
  const kind = get(resource, 'kind', '');
  return {
    kind,
    auth_type: get(
      resource,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/secretType`],
      '',
    ),
    toolItemType: get(resource, [
      'metadata',
      'labels',
      `${ANNOTATION_PREFIX}/toolItemType`,
    ]),
    toolItemKind: get(resource, [
      'metadata',
      'labels',
      `${ANNOTATION_PREFIX}/toolItemKind`,
    ]),
    toolName: get(resource, [
      'metadata',
      'labels',
      `${ANNOTATION_PREFIX}/toolName`,
    ]),
    integrateBy: get(
      resource,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolItemProject`],
      '',
    ),
    description: get(
      resource,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/description`],
      '',
    ),
    selector: get(resource, ['spec', 'selector']),
    secret: get(resource, ['spec', 'secret'], {}),
    name: get(resource, ['metadata', 'name'], ''),
    namespace: get(resource, ['metadata', 'namespace'], ''),
    creationTimestamp: get(resource, ['metadata', 'creationTimestamp'], ''),
    type: get(resource, 'spec.type', kind.toLowerCase()),
    host: get(
      resource,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolHttpHost`],
      '',
    ),
    visitHost: get(
      resource,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolAccessUrl`],
      '',
    ),
    status: {
      phase: get(resource, 'status.phase', ''),
      message: get(resource, 'status.message', ''),
    },
    project: get(
      resource,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/project`], //tenant
      '',
    ),
    bindings: getResourceAllocatedBindings(resource),
    resources: getResourceList(resource),
    createdMode: get(resource, [
      'metadata',
      'annotations',
      `${ANNOTATION_PREFIX}/createdMode`,
    ]),
    __original: resource,
  };
}

export function getMatchExpressions(project: string) {
  return [
    {
      key: 'alauda.io/project',
      values: [project],
      operator: 'In',
    },
    {
      key: 'alauda.io/subProject',
      operator: 'Exists',
    },
  ];
}

function projectManagementBinding(
  resource: AllocateInfo,
  spaces: AllocatedSpace[],
) {
  const projects = spaces.map((item: AllocatedSpace) => {
    return { name: item.name };
  });
  const bindings = spaces.map((item: AllocatedSpace) => {
    const result = {
      selector: {},
      spec: {
        projectManagement: {
          name: '', //空值 required
        },
        projectManagementProjectInfos: [
          {
            name: item.name,
          },
        ],
      },
    };
    if (item.allocatedToAll) {
      Object.assign(result.selector, {
        matchExpressions: getMatchExpressions(resource.project),
      });
    } else {
      Object.assign(result.selector, {
        namespaces: item.allocatedSpaces.map(getDevopsNamespaceFromDevopsSpace),
      });
    }
    return result;
  });
  return {
    projectManagement: {
      projects: projects,
      template: {
        bindings,
      },
    },
  };
}

function documentManagementBinding(
  resource: AllocateInfo,
  spaces: AllocatedSpace[],
) {
  const projects = spaces.map((item: AllocatedSpace) => {
    return { name: item.name };
  });
  const bindings = spaces.map((item: AllocatedSpace) => {
    const result = {
      selector: {},
      spec: {
        docmentManagement: {
          name: '', //空值 required
        },
        documentManagementSpaceRefs: [
          {
            name: item.name,
          },
        ],
      },
    };
    if (item.allocatedToAll) {
      Object.assign(result.selector, {
        matchExpressions: getMatchExpressions(resource.project),
      });
    } else {
      Object.assign(result.selector, {
        namespaces: item.allocatedSpaces.map(getDevopsNamespaceFromDevopsSpace),
      });
    }
    return result;
  });
  return {
    documentManagement: {
      spaces: projects,
      template: {
        bindings,
      },
    },
  };
}

function codeRepoServiceBinding(
  resource: AllocateInfo,
  spaces: AllocatedSpace[],
) {
  const owners = spaces.map((item: AllocatedSpace) => {
    return {
      name: item.name,
      type: item.type,
    };
  });
  const bindings = spaces.map((item: AllocatedSpace) => {
    const result = {
      selector: {},
      spec: {
        codeRepoService: {
          name: '',
        },
        account: {
          owners: [
            {
              type: item.type,
              name: item.name,
            },
          ],
        },
      },
    };
    if (item.allocatedToAll) {
      Object.assign(result.selector, {
        matchExpressions: getMatchExpressions(resource.project),
      });
    } else {
      Object.assign(result.selector, {
        namespaces: item.allocatedSpaces.map(getDevopsNamespaceFromDevopsSpace),
      });
    }
    return result;
  });
  return {
    secret: resource.secret,
    codeRepoService: {
      owners,
      template: {
        bindings,
      },
    },
  };
}

function imageRegistryBinding(
  resource: AllocateInfo,
  spaces: AllocatedSpace[],
) {
  const repositories = spaces.map((item: AllocatedSpace) => item.name);
  const bindings = spaces.map((item: AllocatedSpace) => {
    const result = {
      selector: {},
      spec: {
        imageRegistry: {
          name: '', //空值 required
        },
        repoInfo: {
          repositories: [item.name],
        },
      },
    };
    if (item.allocatedToAll) {
      Object.assign(result.selector, {
        matchExpressions: getMatchExpressions(resource.project),
      });
    } else {
      Object.assign(result.selector, {
        namespaces: item.allocatedSpaces.map(getDevopsNamespaceFromDevopsSpace),
      });
    }
    return result;
  });
  return {
    imageRegistry: {
      repoInfo: {
        repositories,
      },
      template: {
        bindings,
      },
    },
  };
}

function artifactRegistryBinding(
  resource: AllocateInfo,
  spaces: AllocatedSpace[],
  spaceEnabled: boolean,
) {
  const bindings = spaces.map((item: AllocatedSpace) => {
    const result = {
      selector: {},
      spec: {
        artifactRegistry: {
          name: item.name,
        },
      },
    };
    if (item.allocatedToAll || !spaceEnabled) {
      Object.assign(result.selector, {
        matchExpressions: getMatchExpressions(resource.project),
      });
    } else {
      Object.assign(result.selector, {
        namespaces: item.allocatedSpaces.map(getDevopsNamespaceFromDevopsSpace),
      });
    }
    return result;
  });
  return {
    artifactRegistry: {
      template: {
        bindings,
      },
    },
  };
}

export function resourceBindingParamsHandler(
  resource: AllocateInfo,
  spaces: AllocatedSpace[],
  spaceEnabled: boolean,
) {
  const kind = resource.selector.kind;
  let result = {};
  switch (kind) {
    case 'ProjectManagement':
      result = projectManagementBinding(resource, spaces);
      break;
    case 'DocumentManagement':
      result = documentManagementBinding(resource, spaces);
      break;
    case 'CodeRepoService':
      result = codeRepoServiceBinding(resource, spaces);
      break;
    case 'ImageRegistry':
      result = imageRegistryBinding(resource, spaces);
      break;
    case 'ArtifactRegistry':
      result = artifactRegistryBinding(resource, spaces, spaceEnabled);
      break;
  }
  return result;
}

// allocate resource to spaces
export function mapToResourceBindingBody(
  resource: AllocateInfo,
  spaces: AllocatedSpace[],
  spaceEnabled: boolean,
) {
  const params = resourceBindingParamsHandler(resource, spaces, spaceEnabled);
  const result = {
    apiVersion: API_GROUP_VERSION,
    kind: 'ToolBindingReplica',
    metadata: {
      name: resource.name,
      namespace: resource.namespace,
      annotations: {
        'alauda.io/description': resource.description,
        'alauda.io/project': resource.project,
        'alauda.io/createdMode': resource.createdMode,
      },
      labels: {
        'alauda.io/project': resource.project,
      },
    },
    spec: {
      selector: resource.selector,
      secret: resource.secret,
    },
  };
  Object.assign(result.spec, params);
  return result;
}

export function allocatedDialogMode(info: any) {
  if (info.createdMode === 'subscription') {
    return 'create';
  }
  return 'normal';
}

export function canViewAllocateDialog(
  createdMode: string,
  spaceEnabled: boolean,
) {
  return !(createdMode === 'assignment' && !spaceEnabled);
}

export function isGlobalSecret(secret?: ToolChainSecret) {
  return secret && secret.namespace === 'global-credentials';
}

export function getToolChainSecretSourceType(secret?: ToolChainSecret) {
  if (!secret || !secret.namespace) {
    return '';
  }
  if (isGlobalSecret(secret)) {
    return 'platform';
  }
  return 'tenant';
}

export function mapCredentialToToolChainSecret(
  credentail: Credential,
): ToolChainSecret {
  return {
    name: credentail.name,
    namespace: credentail.namespace,
  };
}

export function mapToToolBindings(res: any): ToolBindings {
  return {
    index: get(res, ['metadata', 'annotations', 'index'], 0),
    displayKind: get(res, ['metadata', 'annotations'], { en: '', zh: '' }),
    toolList: (res.items || []).map(mapToToolList),
  };
}

function mapToToolList(items: []): ToolList {
  const ENTERPRISE_KINDS = [ToolBindingKind.CodeRepo];
  const kind = get(items, 'kind', '');
  const isPublic =
    get(items, ['metadata', 'labels', 'codeRepoServicePublic'], 'true') ===
    'true';
  const isEnterprise = ENTERPRISE_KINDS.includes(kind) && !isPublic;
  return {
    name: get(items, ['metadata', 'name'], ''),
    address: get(
      items,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolAccessUrl`],
      '',
    ),
    enterprise: isEnterprise,
    type: get(
      items,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolItemType`],
      '',
    ),
  };
}

export function sort(array: ToolBindings[]): ToolBindings[] {
  return array.sort((a, b) => {
    return a.index - b.index;
  });
}

export function getToolResourceDisplayName(tool: string) {
  return ToolResourceDisplayName[tool];
}

export function mapToArtifactDetail(artifact: ArtiFactDetailResponse) {
  return {
    subscription: get(artifact, ['spec', 'public'], true),
    artifactType: get(artifact, ['spec', 'type'], ''),
    artifactName: get(artifact, ['metadata', 'name'], ''),
    accessUrl: get(artifact, ['spec', 'http', 'accessUrl'], ''),
    kind: get(artifact, 'kind', ''),
    integrateBy: get(
      artifact,
      ['metadata', 'annotations', `${ANNOTATION_PREFIX}/toolItemProject`],
      '',
    ),
    status: {
      phase:
        get(artifact, 'status.phase', '') || get(artifact, 'status.status', ''),
      message: get(artifact, 'status.message', ''),
    },
  };
}

export function mapToRepositoryName(repoitem: RepositoryItems) {
  return get(repoitem, ['spec', 'artifactRegistryName'], '');
}

export function getCreateArtifactRegistryBody<
  T extends TenantCreateArtifactRegistryForm | CreateArtifactRegistryForm | any
>(formData: T) {
  return {
    metadata: {
      annotations: {
        'alauda.io/subscription':
          (formData.allowSubscribe && formData.allowSubscribe.toString()) ||
          'true',
        'alauda.io/toolItemProject': formData.projectName || '',
        'alauda.io/secretType': formData.authType || CredentialType.BasicAuth,
        'alauda.io/toolType': 'artifactRegistry',
      },
    },
    apiVersion: 'devops.alauda.io/v1alpha1',
    kind: 'CreateProjectOptions',
    secretname:
      (formData.useSecret && formData.secret && formData.secret.name) || '',
    namespace:
      (formData.useSecret && formData.secret && formData.secret.namespace) ||
      '',
    name: formData.repoName || '',
    isRemote: 'false',
    data: {
      artifactRegistryName: formData.repoNameSelect || '',
      artifactType:
        formData.repoType === 'Maven' ? 'Maven2' : formData.repoType,
      public:
        (formData.allowSubscribe && formData.allowSubscribe.toString()) ||
        'true',
      type: 'hosted',
      versionPolicy: formData.versionPolicy || '',
      blobStore: formData.fileStorageLocation || '',
    },
  };
}
