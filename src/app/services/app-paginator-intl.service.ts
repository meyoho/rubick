import { Injectable } from '@angular/core';

import { Subject } from 'rxjs';

import { TranslateService } from 'app/translate/translate.service';

@Injectable()
export class AppPaginatorIntl {
  readonly changes = new Subject<void>();

  get itemsPerPageLabel() {
    return this.translate.get('paginator_page_items');
  }

  get jumperLabelPrefix() {
    return this.translate.get('pagination_goto');
  }

  get jumperLabelSuffix() {
    return this.translate.get('pagination_page');
  }

  getTotalLabel = (length: number) =>
    this.translate.get('paginator_total_records', { length });

  constructor(private translate: TranslateService) {
    this.translate.currentLang$.subscribe(() => this.changes.next());
  }
}
