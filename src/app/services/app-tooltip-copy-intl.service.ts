import { Injectable } from '@angular/core';

import { TranslateService } from 'app/translate/translate.service';

@Injectable()
export class AppTooltipCopyIntl {
  constructor(private translate: TranslateService) {}

  get copyTip() {
    return this.translate.get('tooltip_copy_click_to_copy');
  }

  get copySuccessTip() {
    return this.translate.get('tooltip_copy_succeeded');
  }

  get copyFailTip() {
    return this.translate.get('tooltip_copy_faield');
  }
}
