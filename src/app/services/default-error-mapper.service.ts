import { Injectable } from '@angular/core';

import { TranslateService } from 'app/translate/translate.service';
import { ErrorMapperInterface } from 'app2/shared/types';

@Injectable()
export class DefaultErrorMapperService implements ErrorMapperInterface {
  constructor(private translate: TranslateService) {}

  map(key: string, error: any): string {
    switch (key) {
      case 'required':
        return this.translate.get('field_required');
      case 'pattern':
        return this.translate.get('invalid_pattern');
      case 'minlength':
        return this.translate.get('warning_min_length', {
          length: error.requiredLength,
        });
      case 'maxlength':
        return this.translate.get('warning_max_length', {
          length: error.requiredLength,
        });
      case 'min':
        return this.translate.get('warning_min', {
          value: error.min,
        });
      case 'max':
        return this.translate.get('warning_max', {
          value: error.max,
        });
      case 'number':
        return this.translate.get('invalid_number');
      case 'option_not_found':
        return this.translate.get('option_not_found');
      case 'dropdown_min_length':
        return this.translate.get('dropdown_min_length', {
          length: error.minLength,
        });
      case 'dropdown_max_length':
        return this.translate.get('dropdown_max_length', {
          length: error.maxLength,
        });
      default:
        return '';
    }
  }
}
