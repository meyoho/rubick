import { ImageIntl } from '@alauda/common-snippet';
import { Injectable } from '@angular/core';

import { map, publishReplay, refCount } from 'rxjs/operators';

import { TranslateService } from 'app/translate/translate.service';

@Injectable({
  providedIn: 'root',
})
export class ImageIntlService extends ImageIntl {
  constructor(private translate: TranslateService) {
    super();
    this.translate.currentLang$
      .pipe(
        map((lang: string) => (lang === 'en' ? 'en' : 'zh-CN')),
        publishReplay(1),
        refCount(),
      )
      .subscribe((currentLang: string) => {
        this.changes.next(currentLang);
        this.setImageIntl();
      });
  }

  setImageIntl() {
    this.pipelineSelect = this.translate.get('pipeline_select');
    this.pipelineSelectImage = this.translate.get('pipeline_select_image');
    this.pipelineMethod = this.translate.get('pipeline_method');
    this.pipelineInput = this.translate.get('pipeline_input');
    this.pipelineConfirm = this.translate.get('pipeline_confirm');
    this.cancel = this.translate.get('cancel');
    this.pipelineImageRepository = this.translate.get(
      'pipeline_image_repository',
    );
    this.pipelineSelectOrInputImageTag = this.translate.get(
      'pipeline_select_or_input_image_tag',
    );
    this.pipelineImageRepositoryAddress = this.translate.get(
      'pipeline_image_repository_addr',
    );
    this.pipelineInputRegistryAddressTip = this.translate.get(
      'input_registry_address_tips',
    );
    this.pipelineDefaultRegistryTips = this.translate.get(
      'default_registry_tips',
    );
    this.pipelineSecret = this.translate.get('pipeline_secret');
    this.noData = this.translate.get('no_data');
    this.pipelineSecretAdd = this.translate.get('pipeline_secret_add');
    this.pipelineImage = this.translate.get('pipeline_image');
    this.pipelineNew = this.translate.get('pipeline_new');
    this.pipelineRepositoryAddress = this.translate.get(
      'pipeline_repository_address',
    );
    this.pipelineTemplateVersionChange = this.translate.get(
      'pipeline_template_version_change',
    );
    this.pipelineAdd = this.translate.get('pipeline_add');
  }
}
