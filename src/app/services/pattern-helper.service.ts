import { Injectable } from '@angular/core';

import { TranslateService } from 'app/translate/translate.service';

export interface PwdPattern {
  min?: number;
  errorMin?: string;
  max?: number;
  errorMax?: string;
  pattern?: RegExp;
  errorPattern?: string;
  strength?: number;
  errorStrength?: string;
}

@Injectable()
export class PatternHelperService {
  constructor(private translate: TranslateService) {}

  getPasswordPattern(): PwdPattern {
    return {
      min: 6,
      errorMin: this.translate.get('warning_min_length', {
        length: 6,
      }),
      max: 64,
      errorMax: this.translate.get('warning_max_length', {
        length: 64,
      }),
      errorPattern: '',
      errorStrength: this.translate.get('password_too_weak'),
      strength: 45,
    };
  }

  getChangePasswordErrorMapper() {
    const passwordPattern: PwdPattern = this.getPasswordPattern();
    return {
      map: (key: string, _error: any, control: any) => {
        let translateKey = '';
        if (key === 'required') {
          translateKey =
            control.name === 'old_password'
              ? 'old_password_not_empty'
              : 'password_not_empty';
        } else {
          switch (key) {
            case 'same_password':
              translateKey = 'password_not_same';
              break;
            case 'minlength':
              translateKey = passwordPattern.errorMin;
              break;
            case 'maxlength':
              translateKey = passwordPattern.errorMax;
              break;
            case 'pattern':
              translateKey = passwordPattern.errorPattern;
              break;
            case 'strength':
              translateKey = passwordPattern.errorStrength;
              break;
          }
        }
        return this.translate.get(translateKey);
      },
    };
  }

  getPasswordStrength(pass: string) {
    let score = 0;
    if (!pass) {
      return score;
    }
    // award every unique letter until 5 repetitions
    const letters = {};
    for (let i = 0; i < pass.length; i++) {
      letters[pass[i]] = (letters[pass[i]] || 0) + 1;
      score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    const variations = {
      digits: /\d/.test(pass),
      lower: /[a-z]/.test(pass),
      upper: /[A-Z]/.test(pass),
      nonWords: /\W/.test(pass),
    };

    let variationCount = 0;
    for (const check of Object.keys(variations)) {
      variationCount += variations[check] === true ? 1 : 0;
    }
    score += (variationCount - 1) * 10;
    return +score;
  }
}
