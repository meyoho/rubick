import { Injectable } from '@angular/core';

import { get } from 'lodash-es';
import { Observable, ReplaySubject } from 'rxjs';
import { pluck, publishReplay, refCount, startWith } from 'rxjs/operators';

import {
  K8sResourceWithActions,
  KubernetesResource,
  ResourceType,
  StringMap,
} from 'app/typings';
import { RoleService } from './api/role.service';

export interface ResourcePermissions {
  [key: string]: string[];
}

/**
 * 在k8资源列表页面使用，用来支持 LIST API返回数据中不包含 resource_actions 字段的情况
 * 需要在component metadata里(providers)声明组件作用域的provider
 * component 初始化时需要通过 initContext 方法正确设置资源类型和上下文参数
 * sendRequest 方法需要根据获取权限的 API 决定资源名称参数
 */
@Injectable()
export class ResourceListPermissionsService {
  private resourcePermissions: ResourcePermissions;
  private resourceType: string;
  private resourceNameParamKey: string;
  private resourceContext: StringMap;

  private permissionsSubject$: ReplaySubject<
    ResourcePermissions
  > = new ReplaySubject();
  public permissions$: Observable<ResourcePermissions>;

  constructor(private roleService: RoleService) {
    this.resourcePermissions = {};
    this.permissions$ = this.permissionsSubject$.asObservable().pipe(
      startWith({}),
      publishReplay(1),
      refCount(),
    );
  }

  private getResourceType(resource: KubernetesResource): ResourceType {
    return (!!this.resourceType
      ? this.resourceType
      : resource.kind.toLowerCase()) as ResourceType;
  }

  private getResourceIdentifier(resource: KubernetesResource): string {
    return get(resource, 'metadata.name');
  }

  /**
   * 获取该资源对应的权限信息
   * resourceNameParamKey 的值需要参考api: http://internal-api-doc.alauda.cn/jakiro.html#permissions__namespace___resource_type__get
   * 根据资源类型决定；
   * resourceNameParamKey 参数如果为空，表示 该资源类型使用API，暂不支持资源名称作为上下文
   * @param resource
   */
  public async sendRequest(resource: K8sResourceWithActions) {
    try {
      const context = this.resourceNameParamKey
        ? Object.assign(this.resourceContext, {
            [this.resourceNameParamKey]: this.getResourceIdentifier(
              resource.kubernetes,
            ),
          })
        : this.resourceContext;
      const permissions = await this.roleService.getContextPermissions(
        this.getResourceType(resource.kubernetes),
        context,
      );
      if (permissions && permissions.length) {
        this.resourcePermissions = {
          ...this.resourcePermissions,
          [this.getResourceIdentifier(resource.kubernetes)]: permissions,
        };
        this.permissionsSubject$.next(this.resourcePermissions);
      }
    } catch (_e) {
      console.log(_e);
    }
  }

  public getByResource$(resource: K8sResourceWithActions) {
    const key = this.getResourceIdentifier(resource.kubernetes);
    return this.permissions$.pipe(pluck(key));
  }

  public checkByAction(action: string, resource_actions: string[]) {
    if (!resource_actions || !resource_actions.length) {
      return false;
    }
    return resource_actions
      .map((resource_action: string) => {
        return resource_action.split(':')[1];
      })
      .includes(action);
  }

  /**
   * 设置资源类型、API context参数
   * 在每一个注入该Service的组件中，需要手动调用此方法。否则将无法正常工作。
   * @param resourceType 若不设置，默认或使用 KubernetesResource 的 kind 字段
   * @param resourceNameParamKey 发送 API 请求时对应资源名称的参数名称，若不设置，API可能不支持此类资源获取单个资源的权限
   * @param context
   */
  initContext(params: {
    resourceType?: string;
    resourceNameParamKey?: string;
    context: StringMap;
  }) {
    this.resourceType = params.resourceType;
    this.resourceNameParamKey = params.resourceNameParamKey;
    this.resourceContext = params.context;
  }
}
