import { Location } from '@angular/common';
import { Inject, Injectable } from '@angular/core';
import {
  ActivatedRouteSnapshot,
  NavigationEnd,
  PRIMARY_OUTLET,
  Route,
  Router,
  RouterState,
  UrlSegment,
  UrlTree,
} from '@angular/router';

import { Observable } from 'rxjs';
import { filter, map, startWith } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';
import { getLeafActivatedRouteSnapshot } from 'app/utils/router-state';

/* tslint:disable-next-line */
type Params = { [name: string]: any };

type Commands = string | { [key: string]: string };

/**
 * Unified Rubick Router state consisted with basic router state info.
 */
export class RcRouterStateSnapshot {
  /**
   * A string representing the state in UI-Router style, i.e.: foo.bar
   */
  name: string;

  /**
   * Params data associated with this route state, combined with path/matrix/query parameters
   */
  params: Params;

  /**
   * Url associated with this state.
   */
  url: string;

  /**
   * Original route state object
   */
  config: Route;

  /**
   * The associate data object.
   */
  data: any;
}

const HISTORY_SAFE_LENGTH = 20;

export interface RouterSnaphotPathAndParams {
  path: string;
  params: {
    [key: string]: string;
  };
}

/**
 * Define useful router related functions.
 */
@Injectable({
  providedIn: 'root',
})
export class RouterUtilService {
  private history: string[] = [];
  routerSnapshotPathAndParams$: Observable<RouterSnaphotPathAndParams[]>;
  constructor(
    private router: Router,
    private location: Location,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {
    this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .subscribe(({ urlAfterRedirects }: NavigationEnd) => {
        this.updateRouterHistory(urlAfterRedirects);
      });
    this.routerSnapshotPathAndParams$ = this.router.events
      .pipe(filter(event => event instanceof NavigationEnd))
      .pipe(map(() => this.getRoutrStatePathAndParams()));

    if (this.environments.embedded_mode && window.parent) {
      // 在嵌入模式下，路由变化需要向父级window发送信息
      this.routerSnapshotPathAndParams$.subscribe(pathAndParams => {
        window.parent.postMessage(
          {
            pathAndParams,
          },
          '*',
        );
      });
    }
  }

  public getHistory(): string[] {
    return this.history;
  }

  public getPreviousUrl(): string {
    return this.history[this.history.length - 2] || '/console';
  }

  public goBack() {
    this.router.navigateByUrl(this.getPreviousUrl());
  }

  public getLinkUrl(commands: string | Commands[], queryParams?: Params) {
    if (!Array.isArray(commands)) {
      commands = [commands];
    }
    return this.location.prepareExternalUrl(
      this.router.serializeUrl(
        this.router.createUrlTree(commands, {
          queryParams,
        }),
      ),
    );
  }

  private updateRouterHistory(urlAfterRedirects: string) {
    if (this.history.length >= HISTORY_SAFE_LENGTH) {
      this.history.shift();
    }
    this.history = [...this.history, urlAfterRedirects];
  }

  getRoutrStatePathAndParams(): RouterSnaphotPathAndParams[] {
    const leafActivatedRouteSnapshot = getLeafActivatedRouteSnapshot(
      this.router.routerState,
    );
    return leafActivatedRouteSnapshot.pathFromRoot.reduce(
      (pathAndParams, interSnapshot: ActivatedRouteSnapshot) => {
        if (interSnapshot.url.length) {
          pathAndParams.push({
            path: interSnapshot.url[0].path,
            params: interSnapshot.params,
          });
        }
        return pathAndParams;
      },
      [],
    );
  }

  createUrlTreeWithClusterParam(cluster: string, url?: string): UrlTree {
    const tree = this.router.parseUrl(url);
    const segments = tree.root.children[PRIMARY_OUTLET].segments;
    segments.forEach((segment: UrlSegment) => {
      if (segment.path === 'admin') {
        segment.parameters['cluster'] = cluster;
      }
    });
    return tree;
  }

  /**
   * @deprecated
   * @returns {Observable<RcRouterStateSnapshot>}
   */
  protected getRouterState(): Observable<RcRouterStateSnapshot> {
    return this.router.events.pipe(
      filter(event => event instanceof NavigationEnd),
      startWith(null),
      map(() => this.router.routerState),
      map((state: RouterState) => {
        const leafActivatedRouteSnapshot = getLeafActivatedRouteSnapshot(state);

        // Combine both path/matrix params with query params
        const params = Object.assign(
          {},
          leafActivatedRouteSnapshot.params,
          leafActivatedRouteSnapshot.queryParams,
        );
        const stateName = leafActivatedRouteSnapshot.pathFromRoot
          .map(interSnapshot => interSnapshot.url)
          .filter(urlSegments => urlSegments.length)
          // Note: for ActivatedRoute, we have an assumption that only the first segment is used for the state name
          .map(urlSegments => urlSegments[0])
          .join('.');

        const routeConfig = leafActivatedRouteSnapshot.routeConfig;

        return {
          params,
          name: stateName,
          url: this.location.prepareExternalUrl(state.snapshot.url),
          config: routeConfig,
          data: leafActivatedRouteSnapshot.data,
        };
      }),
      filter(state => !!state.name),
    );
  }
}
