import { Injectable } from '@angular/core';

import * as distanceInWordsStrict from 'date-fns/distance_in_words_strict';
import * as format from 'date-fns/format';
import * as zh_cn from 'date-fns/locale/zh_cn/index.js';

import { HttpService } from 'app/services/http.service';
import { TranslateService } from 'app/translate/translate.service';

const milisecondsInAWeek = 1000 * 60 * 60 * 24 * 7;

@Injectable()
export class TimeService {
  _configPath = '/ajax/current_time';
  currentTime: any;
  _initTime: any;
  translatedUnits: { [key: string]: string[] };

  constructor(
    private http: HttpService,
    private readonly translate: TranslateService,
  ) {
    this.getAppConfig();
  }

  getAppConfig() {
    this._initTime = new Date().getTime();
    this.http.request(this._configPath).then((time: { serverTime: number }) => {
      if (time && time.serverTime) {
        const elapsed = new Date().getTime() - this._initTime;
        return new Date(time.serverTime + elapsed);
      } else {
        return new Date();
      }
    });
  }

  transformRelative(value: string, ...args: any[]): string {
    if (value == null) {
      return '-';
    }
    // Current server time.
    const serverTime = this.currentTime;

    // Current and given times in miliseconds.
    const currentTime = this.getCurrentTime_(serverTime);
    const givenTime = new Date(value).getTime();
    if (currentTime - givenTime > milisecondsInAWeek) {
      return format(value, 'YYYY-MM-DD');
    } else {
      return distanceInWordsStrict(currentTime, givenTime, {
        addSuffix: args[0],
        locale: this.translate.currentLang === 'zh_cn' ? zh_cn : 'en',
      });
    }
  }

  transform(value: Date | string | number): string {
    if (value === null) {
      return '-';
    }

    return format(value, 'YYYY-MM-DD HH:mm:ss');
  }

  /**
   * Returns current time. If appConfig.serverTime is provided then it will be returned, otherwise
   * current client time will be used.
   */
  private getCurrentTime_(serverTime: Date): number {
    return serverTime ? serverTime.getTime() : new Date().getTime();
  }
}
