import { NavItemConfig } from '@alauda/ui';
import { TemplatePortal } from '@angular/cdk/portal';
import { Inject, Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';

/**
 * Provides a holder for UI templates to be used globally.
 */
export class TemplateHolder {
  private templatePortalSubject = new BehaviorSubject<TemplatePortal<any>>(
    undefined,
  );

  get templatePortal$() {
    return this.templatePortalSubject.pipe(debounceTime(0));
  }

  setTemplatePortal(templatePortal: TemplatePortal<any>) {
    this.templatePortalSubject.next(templatePortal);
  }
}

export enum TemplateHolderType {
  PageHeaderContent = 'PageHeaderContent',
}

export interface ActiveNavItem {
  label: string;
  routerLink?: string | any[];
}

/**
 * Acts as a general ui state store
 */
@Injectable()
export class UiStateService {
  private templateHolders = new Map<TemplateHolderType, TemplateHolder>();
  activeItemInfo$ = new BehaviorSubject<NavItemConfig[]>([]);
  userViewProjectBadgeDisplayState$ = new BehaviorSubject<boolean>(true);
  logoMainSrc = '/static/images/logo/logo-main.svg';
  logoMainMiniSrc = '/static/images/logo/logo-main-mini.svg'; // 暂时没用到

  constructor(@Inject(ENVIRONMENTS) private environments: Environments) {
    // Override logo src if there is an environment variable been set.
    const logoSrcMap =
      this.environments.overridden_logo_sources &&
      this.environments.overridden_logo_sources
        .split(',')
        .reduce((accum, logoSrcEnv) => {
          const splitIndex = logoSrcEnv.indexOf(':');
          const key = logoSrcEnv.substr(0, splitIndex);
          accum[key] = logoSrcEnv.substr(splitIndex + 1);
          return accum;
        }, {});

    if (logoSrcMap) {
      this.logoMainSrc = logoSrcMap['logo-main'] || this.logoMainSrc;
      this.logoMainMiniSrc =
        logoSrcMap['logo-main-mini'] || this.logoMainMiniSrc;
    }
  }

  private regiserTemplateHolder(id: TemplateHolderType) {
    if (this.templateHolders.has(id)) {
      throw new Error(`Template holder for ${id} has already registered!`);
    }
    this.templateHolders.set(id, new TemplateHolder());
  }

  // Will init template holder if not initialzed yet
  getTemplateHolder(id: TemplateHolderType) {
    if (!this.templateHolders.has(id)) {
      this.regiserTemplateHolder(id);
    }

    return this.templateHolders.get(id);
  }

  /**
   * NOTE: only set active item info in ConsoleComponent
   */
  setActiveItemInfo(activeNavItemInfo: NavItemConfig[]) {
    this.activeItemInfo$.next(activeNavItemInfo);
  }

  setProjectBadgeState(target: boolean) {
    this.userViewProjectBadgeDisplayState$.next(target);
  }
}
