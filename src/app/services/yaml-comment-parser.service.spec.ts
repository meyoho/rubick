import { TestBed, inject } from '@angular/core/testing';
import {
  YamlCommentParserService,
  YamlEntry,
} from 'app/services/yaml-comment-parser.service';

describe('YamlCommentParserService', () => {
  let ycps: YamlCommentParserService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [YamlCommentParserService],
    });
  });

  // Inject required services:
  beforeEach(inject(
    [YamlCommentParserService],
    (y: YamlCommentParserService) => {
      ycps = y;
    },
  ));

  it('should transform comment only yaml', () => {
    const output = ycps.parse('# asdf');
    expect([]).toEqual(output);
  });

  it('should transform array value with object', () => {
    const output = ycps.parse(`
a:
- foo: 1
  bar: 2
- foo: 3
  bar: [1, 2, 3]`);
    const expected: YamlEntry[] = [
      {
        paths: ['a'],
        type: 'array',
        value: '[{"foo":1,"bar":2},{"foo":3,"bar":[1,2,3]}]',
        preComment: '',
        inlineComment: '',
        afterContent: '',
      },
    ];
    expect(expected).toEqual(output);
  });

  it('should transform simple map 1', () => {
    const input = `
# Test pre-comment-1
key0: 123 # Test inline-comment-1
# Test pre-comment-2
key1: '123' # Test inline-comment-2
key2: 12.3
key3: false
# Test after-comment`;
    const expected: YamlEntry[] = [
      {
        paths: ['key0'],
        type: 'number',
        value: '123',
        preComment: '# Test pre-comment-1',
        inlineComment: '# Test inline-comment-1',
        afterContent: undefined,
      },
      {
        paths: ['key1'],
        type: 'string',
        value: '123',
        preComment: '# Test pre-comment-2',
        inlineComment: '# Test inline-comment-2',
        afterContent: undefined,
      },
      {
        paths: ['key2'],
        type: 'number',
        value: '12.3',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['key3'],
        type: 'boolean',
        value: 'false',
        preComment: '',
        inlineComment: '',
        afterContent: '\n# Test after-comment',
      },
    ];

    const output = ycps.parse(input);

    // console.log(JSON.stringify(output));
    expect(expected).toEqual(output);
  });

  it('should transform simple map 2', () => {
    const input = `
# Test Pre Comment 0
simpleKey: # Test Key-Only Inline Comment 0
  # Test Pre Comment 1
  simpleKey2.3: 123 # Test Inline Comment 0
  simpleKey3:
    simpleKey4: 567 # Test Inline Comment 2
simpleKey5: 789`;
    const expected: YamlEntry[] = [
      {
        paths: ['simpleKey', 'simpleKey2.3'],
        type: 'number',
        value: '123',
        preComment:
          '# Test Pre Comment 0\n# Test Key-Only Inline Comment 0\n# Test Pre Comment 1',
        inlineComment: '# Test Inline Comment 0',
        afterContent: undefined,
      },
      {
        paths: ['simpleKey', 'simpleKey3', 'simpleKey4'],
        type: 'number',
        value: '567',
        preComment: '',
        inlineComment: '# Test Inline Comment 2',
        afterContent: undefined,
      },
      {
        paths: ['simpleKey5'],
        type: 'number',
        value: '789',
        preComment: '',
        inlineComment: '',
        afterContent: '',
      },
    ];
    const output = ycps.parse(input);
    expect(expected).toEqual(output);
  });

  it('should transform simple array', () => {
    const input = `
# Test Comment
simpleKey:
  - aaaa
  - bbbb # 456
bar:
  - a
# sad
foo:
  - b`;
    const expected: YamlEntry[] = [
      {
        inlineComment: '# 456',
        paths: ['simpleKey'],
        type: 'array',
        preComment: '# Test Comment',
        value: '["aaaa","bbbb"]',
        afterContent: undefined,
      },
      {
        inlineComment: '',
        paths: ['bar'],
        type: 'array',
        preComment: '',
        value: '["a"]',
        afterContent: undefined,
      },
      {
        inlineComment: '',
        paths: ['foo'],
        type: 'array',
        preComment: '# sad',
        value: '["b"]',
        afterContent: '',
      },
    ];
    const output = ycps.parse(input);
    expect(expected).toEqual(output);
  });

  it('should transform full yaml', () => {
    const input = `
    # Default values for jenkins.
    # This is a YAML-formatted file.
    # Declare name/value pairs to be passed into your templates.
    # name: value

    Master:
      Name: jenkins-master
      Image: "jenkinsci/jenkins"
      ImageTag: "2.67"
      ImagePullPolicy: "Always"
      Component: "jenkins-master"
      UseSecurity: true
      AdminUser: admin
    # AdminPassword: <defaults to random>
      Cpu: "200m"
      Memory: "256Mi"
    # Set min/max heap here if needed with:
    # JavaOpts: "-Xms512m -Xmx512m"
    # JenkinsOpts: ""
    # JenkinsUriPrefix: "/jenkins"
      ServicePort: 8080
    # For minikube, set this to NodePort, elsewhere use LoadBalancer
    # Use ClusterIP if your setup includes ingress controller
      ServiceType: LoadBalancer
    # Master Service annotations
      ServiceAnnotations: {}
        #   service.beta.kubernetes.io/aws-load-balancer-backend-protocol: https
    # Used to create Ingress record (should used with ServiceType: ClusterIP)
    # HostName: jenkins.cluster.local
    # NodePort: <to set explicitly, choose port between 30000-32767
      ContainerPort: 8080
      SlaveListenerPort: 50000
      LoadBalancerSourceRanges:
      - 0.0.0.0/0
    # Optionally assign a known public LB IP
    # LoadBalancerIP: 1.2.3.4
    # Optionally configure a JMX port
    # requires additional JavaOpts, ie
    # JavaOpts: >
    #   -Dcom.sun.management.jmxremote.port=4000
    #   -Dcom.sun.management.jmxremote.authenticate=false
    #   -Dcom.sun.management.jmxremote.ssl=false
    # JMXPort: 4000
    # List of plugins to be install during Jenkins master start
      InstallPlugins:
          - kubernetes:0.11
          - workflow-aggregator:2.5
          - workflow-job:2.13
          - credentials-binding:1.12
          - git:3.4.0
    # Used to approve a list of groovy functions in pipelines used the script-security plugin. Can be viewed under /scriptApproval
      # ScriptApproval:
      #   - "method groovy.json.JsonSlurperClassic parseText java.lang.String"
      #   - "new groovy.json.JsonSlurperClassic"
    # List of groovy init scripts to be executed during Jenkins master start
      InitScripts:
    #  - |
    #    print 'adding global pipeline libraries, register properties, bootstrap jobs...'
      CustomConfigMap: false
    # Node labels and tolerations for pod assignment
    # ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#nodeselector
    # ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#taints-and-tolerations-beta-feature
      NodeSelector: {}
      Tolerations: {}

      Ingress:
        Annotations:
          # kubernetes.io/ingress.class: nginx
          # kubernetes.io/tls-acme: "true"

        TLS:
          # - secretName: jenkins.cluster.local
          #   hosts:
          #     - jenkins.cluster.local

    Agent:
      Enabled: true
      Image: jenkinsci/jnlp-slave
      ImageTag: 2.62
      Component: "jenkins-slave"
      Privileged: false
      Cpu: "200m"
      Memory: "256Mi"
      # You may want to change this to true while testing a new image
      AlwaysPullImage: false
      # You can define the volumes that you want to mount for this container
      # Allowed types are: ConfigMap, EmptyDir, HostPath, Nfs, Pod, Secret
      # Configure the attributes as they appear in the corresponding Java class for that type
      # https://github.com/jenkinsci/kubernetes-plugin/tree/master/src/main/java/org/csanchez/jenkins/plugins/kubernetes/volumes
      volumes:
      # - type: Secret
      #   secretName: mysecret
      #   mountPath: /var/myapp/mysecret
      NodeSelector: {}
      # Key Value selectors. Ex:
      # jenkins-agent: v1

    Persistence:
      Enabled: true
      ## A manually managed Persistent Volume and Claim
      ## Requires Persistence.Enabled: true
      ## If defined, PVC must be created manually before volume will be bound
      # ExistingClaim:

      ## jenkins data Persistent Volume Storage Class
      ## If defined, storageClassName: <storageClass>
      ## If set to "-", storageClassName: "", which disables dynamic provisioning
      ## If undefined (the default) or set to null, no storageClassName spec is
      ##   set, choosing the default provisioner.  (gp2 on AWS, standard on
      ##   GKE, AWS & OpenStack)
      ##
      # StorageClass: "-"

      AccessMode: ReadWriteOnce
      Size: 8Gi
      volumes:
    #  - name: nothing
    #    emptyDir: {}
      mounts:
    #  - mountPath: /var/nothing
    #    name: nothing
    #    readOnly: true

    NetworkPolicy:
      # Enable creation of NetworkPolicy resources.
      Enabled: false
      # For Kubernetes v1.4, v1.5 and v1.6, use 'extensions/v1beta1'
      # For Kubernetes v1.7, use 'networking.k8s.io/v1'
      ApiVersion: extensions/v1beta1

    ## Install Default RBAC roles and bindings
    rbac:
      install: false
      serviceAccountName: default
      # RBAC api version (currently either v1beta1 or v1alpha1)
      apiVersion: v1beta1
      # Cluster role reference
      roleRef: cluster-admin
    `;

    const output = ycps.parse(input);
    const expected: YamlEntry[] = [
      {
        paths: ['Master', 'Name'],
        value: 'jenkins-master',
        type: 'string',
        preComment:
          '# Default values for jenkins.\n# This is a YAML-formatted file.\n# Declare name/value pairs to be passed into your templates.\n# name: value',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'Image'],
        value: 'jenkinsci/jenkins',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'ImageTag'],
        value: '2.67',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'ImagePullPolicy'],
        value: 'Always',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'Component'],
        value: 'jenkins-master',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'UseSecurity'],
        value: 'true',
        type: 'boolean',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'AdminUser'],
        value: 'admin',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'Cpu'],
        value: '200m',
        type: 'string',
        preComment: '# AdminPassword: <defaults to random>',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'Memory'],
        value: '256Mi',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'ServicePort'],
        value: '8080',
        type: 'number',
        preComment:
          '# Set min/max heap here if needed with:\n# JavaOpts: "-Xms512m -Xmx512m"\n# JenkinsOpts: ""\n# JenkinsUriPrefix: "/jenkins"',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'ServiceType'],
        value: 'LoadBalancer',
        type: 'string',
        preComment:
          '# For minikube, set this to NodePort, elsewhere use LoadBalancer\n# Use ClusterIP if your setup includes ingress controller',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'ServiceAnnotations'],
        value: '{}',
        type: 'object',
        preComment: '# Master Service annotations',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'ContainerPort'],
        value: '8080',
        type: 'number',
        preComment:
          '#   service.beta.kubernetes.io/aws-load-balancer-backend-protocol: https\n# Used to create Ingress record (should used with ServiceType: ClusterIP)\n# HostName: jenkins.cluster.local\n# NodePort: <to set explicitly, choose port between 30000-32767',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'SlaveListenerPort'],
        value: '50000',
        type: 'number',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'LoadBalancerSourceRanges'],
        value: '["0.0.0.0/0"]',
        type: 'array',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'InstallPlugins'],
        value:
          '["kubernetes:0.11","workflow-aggregator:2.5","workflow-job:2.13","credentials-binding:1.12","git:3.4.0"]',
        type: 'array',
        preComment:
          '# Optionally assign a known public LB IP\n# LoadBalancerIP: 1.2.3.4\n# Optionally configure a JMX port\n# requires additional JavaOpts, ie\n# JavaOpts: >\n#   -Dcom.sun.management.jmxremote.port=4000\n#   -Dcom.sun.management.jmxremote.authenticate=false\n#   -Dcom.sun.management.jmxremote.ssl=false\n# JMXPort: 4000\n# List of plugins to be install during Jenkins master start',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'InitScripts'],
        value: '',
        type: 'string',
        preComment:
          '# Used to approve a list of groovy functions in pipelines used the script-security plugin. Can be viewed under /scriptApproval\n# ScriptApproval:\n#   - "method groovy.json.JsonSlurperClassic parseText java.lang.String"\n#   - "new groovy.json.JsonSlurperClassic"\n# List of groovy init scripts to be executed during Jenkins master start',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'CustomConfigMap'],
        value: 'false',
        type: 'boolean',
        preComment:
          "#  - |\n#    print 'adding global pipeline libraries, register properties, bootstrap jobs...'",
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'NodeSelector'],
        value: '{}',
        type: 'object',
        preComment:
          '# Node labels and tolerations for pod assignment\n# ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#nodeselector\n# ref: https://kubernetes.io/docs/concepts/configuration/assign-pod-node/#taints-and-tolerations-beta-feature',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'Tolerations'],
        value: '{}',
        type: 'object',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'Ingress', 'Annotations'],
        value: '',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Master', 'Ingress', 'TLS'],
        value: '',
        type: 'string',
        preComment:
          '# kubernetes.io/ingress.class: nginx\n# kubernetes.io/tls-acme: "true"',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'Enabled'],
        value: 'true',
        type: 'boolean',
        preComment:
          '# - secretName: jenkins.cluster.local\n#   hosts:\n#     - jenkins.cluster.local',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'Image'],
        value: 'jenkinsci/jnlp-slave',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'ImageTag'],
        value: '2.62',
        type: 'number',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'Component'],
        value: 'jenkins-slave',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'Privileged'],
        value: 'false',
        type: 'boolean',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'Cpu'],
        value: '200m',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'Memory'],
        value: '256Mi',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'AlwaysPullImage'],
        value: 'false',
        type: 'boolean',
        preComment:
          '# You may want to change this to true while testing a new image',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'volumes'],
        value: '',
        type: 'string',
        preComment:
          '# You can define the volumes that you want to mount for this container\n# Allowed types are: ConfigMap, EmptyDir, HostPath, Nfs, Pod, Secret\n# Configure the attributes as they appear in the corresponding Java class for that type\n# https://github.com/jenkinsci/kubernetes-plugin/tree/master/src/main/java/org/csanchez/jenkins/plugins/kubernetes/volumes',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Agent', 'NodeSelector'],
        value: '{}',
        type: 'object',
        preComment:
          '# - type: Secret\n#   secretName: mysecret\n#   mountPath: /var/myapp/mysecret',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Persistence', 'Enabled'],
        value: 'true',
        type: 'boolean',
        preComment: '# Key Value selectors. Ex:\n# jenkins-agent: v1',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Persistence', 'AccessMode'],
        value: 'ReadWriteOnce',
        type: 'string',
        preComment:
          '## A manually managed Persistent Volume and Claim\n## Requires Persistence.Enabled: true\n## If defined, PVC must be created manually before volume will be bound\n# ExistingClaim:\n## jenkins data Persistent Volume Storage Class\n## If defined, storageClassName: <storageClass>\n## If set to "-", storageClassName: "", which disables dynamic provisioning\n## If undefined (the default) or set to null, no storageClassName spec is\n##   set, choosing the default provisioner.  (gp2 on AWS, standard on\n##   GKE, AWS & OpenStack)\n##\n# StorageClass: "-"',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Persistence', 'Size'],
        value: '8Gi',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Persistence', 'volumes'],
        value: '',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['Persistence', 'mounts'],
        value: '',
        type: 'string',
        preComment: '#  - name: nothing\n#    emptyDir: {}',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['NetworkPolicy', 'Enabled'],
        value: 'false',
        type: 'boolean',
        preComment:
          '#  - mountPath: /var/nothing\n#    name: nothing\n#    readOnly: true\n# Enable creation of NetworkPolicy resources.',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['NetworkPolicy', 'ApiVersion'],
        value: 'extensions/v1beta1',
        type: 'string',
        preComment:
          "# For Kubernetes v1.4, v1.5 and v1.6, use 'extensions/v1beta1'\n# For Kubernetes v1.7, use 'networking.k8s.io/v1'",
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['rbac', 'install'],
        value: 'false',
        type: 'boolean',
        preComment: '## Install Default RBAC roles and bindings',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['rbac', 'serviceAccountName'],
        value: 'default',
        type: 'string',
        preComment: '',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['rbac', 'apiVersion'],
        value: 'v1beta1',
        type: 'string',
        preComment: '# RBAC api version (currently either v1beta1 or v1alpha1)',
        inlineComment: '',
        afterContent: undefined,
      },
      {
        paths: ['rbac', 'roleRef'],
        value: 'cluster-admin',
        type: 'string',
        preComment: '# Cluster role reference',
        inlineComment: '',
        afterContent: '\n    ',
      },
    ];

    expect(expected).toEqual(output);
  });
});
