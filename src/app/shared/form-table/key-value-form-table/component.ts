import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormArray, FormControl, ValidatorFn } from '@angular/forms';

import { sortBy } from 'lodash-es';

import { BaseStringMapFormComponent } from 'app/abstract';
import { ErrorMapper } from 'app2/shared/types';

@Component({
  selector: 'rc-key-value-form-table',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class KeyValueFormTableComponent extends BaseStringMapFormComponent
  implements OnInit {
  @Input()
  valiadator: {
    key: ValidatorFn[];
    value: ValidatorFn[];
  } = {
    key: [],
    value: [],
  };
  @Input()
  errorMapper: {
    key: ErrorMapper;
    value: ErrorMapper;
  } = {
    key: {},
    value: {},
  };
  @Input() readonlyKeys: string[] = [];
  form: FormArray;
  constructor(injector: Injector) {
    super(injector);
  }

  adaptResourceModel(resource: { [key: string]: string }) {
    let newFormModel = Object.entries(resource || {});
    // 排序，先按字母排序，再把 readonly 的放前面
    newFormModel = sortBy(newFormModel, (arr: [string, string]) => {
      return arr[0];
    });
    newFormModel.sort((arr1: [string, string], arr2: [string, string]) => {
      const flag1 = this.readonlyKeys.includes(arr1[0]);
      const flag2 = this.readonlyKeys.includes(arr2[0]);
      if ((flag1 && flag2) || (!flag1 && !flag2)) {
        return 0;
      } else {
        return flag1 ? -1 : 1;
      }
    });
    if (newFormModel.length === 0) {
      newFormModel = this.getDefaultFormModel();
    }

    return newFormModel;
  }

  rowBackgroundColorFn(row: FormControl) {
    if (row.invalid) {
      return '#f9f2f4';
    } else {
      return '';
    }
  }

  getKeyValidators() {
    return (this.valiadator && this.valiadator.key) || [];
  }

  getValueValidators() {
    return (this.valiadator && this.valiadator.value) || [];
  }

  isLabelReadonly(index: number) {
    const formArray = this.form.controls[index] as FormArray;
    const isValid = this.form.controls[index].valid;
    return isValid && this.readonlyKeys.includes(formArray.controls[0].value);
  }
}
