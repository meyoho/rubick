import { FormModule as AuiFormModule, IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { SharedModule } from 'app/shared/shared.module';

import {
  ArrayFormTableComponent,
  ArrayFormTableFooterDirective,
  ArrayFormTableHeaderDirective,
  ArrayFormTableRowControlDirective,
  ArrayFormTableRowDirective,
  ArrayFormTableZeroStateDirective,
} from './array-form-table/component';
import { KeyValueFormTableComponent } from './key-value-form-table/component';
import { StringArrayFormTableComponent } from './string-array-form-table/component';

const NG_MODULES = [CommonModule, FormsModule, ReactiveFormsModule];
const AUI_MODUELS = [AuiFormModule, IconModule];

const COMPONENTS = [
  ArrayFormTableComponent,
  ArrayFormTableFooterDirective,
  ArrayFormTableHeaderDirective,
  ArrayFormTableRowControlDirective,
  ArrayFormTableRowDirective,
  ArrayFormTableZeroStateDirective,
  KeyValueFormTableComponent,
  StringArrayFormTableComponent,
];

@NgModule({
  // TODO: SharedModule 需要精简
  imports: [...NG_MODULES, ...AUI_MODUELS, SharedModule],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
})
export class FormTableModule {}
