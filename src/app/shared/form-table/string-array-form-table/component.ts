import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormArray, FormControl, ValidatorFn } from '@angular/forms';

import { StringMap } from 'app/typings/raw-k8s';
import { BaseResourceFormComponent } from 'ng-resource-form-util';

@Component({
  selector: 'rc-string-array-form-table',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class StringArrayFormTableComponent
  extends BaseResourceFormComponent<string[]>
  implements OnInit {
  @Input()
  resourceName: string;
  @Input() validators: ValidatorFn[] = [];
  @Input() errorMapper: StringMap = {};
  form: FormArray;
  constructor(injector: Injector) {
    super(injector);
  }

  getResourceMergeStrategy() {
    return false;
  }

  createForm() {
    return this.fb.array([]);
  }

  getDefaultFormModel(): string[] {
    return [];
  }

  add(index = this.form.length) {
    this.form.insert(index, this.getOnFormArrayResizeFn()());
    this.cdr.markForCheck();
  }

  remove(index: number) {
    this.form.removeAt(index);
    this.cdr.markForCheck();
  }

  rowBackgroundColorFn(row: FormControl) {
    if (row.invalid) {
      return '#f9f2f4';
    } else {
      return '';
    }
  }

  getOnFormArrayResizeFn() {
    return () => this.fb.control('', this.validators);
  }
}
