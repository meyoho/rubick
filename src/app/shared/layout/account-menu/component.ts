import { DialogService } from '@alauda/ui';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { AccountService } from 'app/services/api/account.service';
import { OrgService, User } from 'app/services/api/org.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { AppState } from 'app/store';
import { LogoutAction } from 'app/store/actions';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { isUserView } from 'app/utils/page';

@Component({
  selector: 'rc-account-menu',
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
})
export class AccountMenuComponent implements OnInit {
  @Input() showViewSwitch = true;
  operationViewEnabled: boolean;
  language: string;
  user: User;
  isThirdPartyUser: boolean;
  isUserView: boolean;
  isSubAccount: boolean;

  constructor(
    @Inject(ACCOUNT) public account: Account,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private orgService: OrgService,
    private error: ErrorsToastService,
    public accountService: AccountService,
    private router: Router,
    private store: Store<AppState>,
  ) {
    this.isUserView = isUserView();
  }

  async ngOnInit() {
    this.language = this.translateService.otherLang;
    this.isSubAccount = !!this.account.username;
    try {
      const [profile, operationViewEnabled] = await Promise.all([
        this.accountService.getAuthProfile(),
        this.roleUtil.resourceTypeSupportPermissions(
          'operation_view',
          {},
          'get',
        ),
      ]);
      this.operationViewEnabled = operationViewEnabled;
      this.accountService.avatarPath$$.next(profile.logo_file);

      if (this.account.username) {
        const user = await this.orgService.getUser();
        this.isThirdPartyUser =
          user.sub_type !== 'ldap' && user.type !== 'organizations.LDAPAccount';
      }
    } catch (e) {
      this.error.error(e);
    }
  }

  switchToConsole() {
    this.router.navigate(['/console/admin']);
  }

  async switchToUserView() {
    this.router.navigate(['/console/user']);
  }

  changeLanguage() {
    const preLang = this.translateService.currentLang;
    this.translateService.changeLanguage();
    this.language = preLang;
  }

  logout() {
    this.dialogService
      .confirm({
        title: this.translateService.get('logout_confirm'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      })
      .then(async () => {
        this.accountService.logout();
        this.store.dispatch(new LogoutAction());
      })
      .catch(() => {});
  }

  get icon() {
    return `basic:switch_to_${
      this.translateService.currentLang === 'zh_cn' ? 'english' : 'chinese'
    }`;
  }

  gotoUserCenter() {
    this.router.navigateByUrl(
      `/console/user-center/detail?username=${this.account.username}`,
    );
  }
}
