import { Component, Inject, OnInit } from '@angular/core';

import { AccountService } from 'app/services/api/account.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';

@Component({
  selector: 'rc-amp-entry',
  templateUrl: './amp-entry.component.html',
  styleUrls: ['./amp-entry.component.scss'],
})
export class AmpEntryComponent implements OnInit {
  ampUrl: string;

  constructor(
    private accountService: AccountService,
    private translate: TranslateService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  ngOnInit() {
    this.ampUrl = this.environments.api_market_url;
  }

  async toAmp() {
    const tokens = await this.accountService.getAmpIdTokens();
    window.location.href = `${this.environments.api_market_url}?locale=${
      this.translate.currentLang
    }&auth_url=${window.location.origin}/ap/logout&id_token=${
      tokens.kubernetes_id_token
    }&ace_token=${tokens.platform_token}`;
  }
}
