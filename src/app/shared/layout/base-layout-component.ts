import { NavGroupConfig, NavItemConfig } from '@alauda/ui';
import { TemplatePortal } from '@angular/cdk/portal';
import { HttpClient } from '@angular/common/http';
import { Injector, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import {
  ActivatedRoute,
  NavigationEnd,
  ParamMap,
  Router,
} from '@angular/router';

import { combineLatest, Observable, of, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { RoleService } from 'app/services/api/role.service';
import {
  TemplateHolderType,
  UiStateService,
} from 'app/services/ui-state.service';
import { ENVIRONMENTS, WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments, ResourceType, Weblabs } from 'app/typings';

export abstract class BaseLayoutComponent implements OnInit, OnDestroy {
  http: HttpClient;
  router: Router;
  title: Title;
  activatedRoute: ActivatedRoute;
  translate: TranslateService;
  uiState: UiStateService;
  roleService: RoleService;
  environments: Environments;
  weblabs: Weblabs;

  // Default nav config
  userDocsUrl: string;
  ampUrl: string;
  navConfig$: Observable<NavGroupConfig[]>;
  destroy$ = new Subject<void>();
  pageHeaderContentTemplate$: Observable<TemplatePortal<any>>;
  activatedKey$: Observable<string>;

  constructor(injector: Injector) {
    this.http = injector.get(HttpClient);
    this.router = injector.get(Router);
    this.title = injector.get(Title);
    this.translate = injector.get(TranslateService);
    this.activatedRoute = injector.get(ActivatedRoute);
    this.uiState = injector.get(UiStateService);
    this.roleService = injector.get(RoleService);
    this.environments = injector.get(ENVIRONMENTS);
    this.weblabs = injector.get(WEBLABS);
  }

  ngOnInit() {
    this.userDocsUrl = this.environments.user_docs_url;
    this.ampUrl = this.environments.api_market_url;
    this.pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
      TemplateHolderType.PageHeaderContent,
    ).templatePortal$;

    this.uiState.activeItemInfo$
      .pipe(
        filter(activeItemInfo => activeItemInfo && activeItemInfo.length > 0),
        map(activeItemInfo => activeItemInfo[0].label),
        switchMap(primaryItemName => this.translate.stream(primaryItemName)),
        takeUntil(this.destroy$),
      )
      .subscribe(title => this.title.setTitle(title));
  }

  ngOnDestroy() {
    this.destroy$.next();
  }

  abstract handleActivatedItemChange(config: NavItemConfig): void;

  protected initNavConfig(
    navConfigType: string,
    buildUrl: (segment: string, paramMap?: ParamMap) => string,
  ) {
    const rawNavConfig$ = this.http
      .get<{ nav_config: AppNavConfig }>(`ajax-sp/nav-config/${navConfigType}/`)
      .pipe(map(nav => nav.nav_config));

    // FIXME: 请求用户对所有资源的操作权限时，暂不考虑上权限下文。
    // const navContext$ = this.activatedRoute.params.pipe(
    //   map(({ project, cluster, namespace }) => ({
    //     project_name: project,
    //     cluster_name: cluster,
    //     namespace_name: namespace,
    //   })),
    // );

    // 旧的实现会在导航链接不显示的时候跳到首页；
    // 为了减少实现复杂度，不在此处做页面跳转，而考虑做在路由的 Guard 里。
    this.navConfig$ = rawNavConfig$.pipe(
      switchMap(navConfig => {
        const resourceTypes = getNavConfigResourceTypes(navConfig);

        // Skip api call if no resource type defined in nav config.
        const permissions$ =
          resourceTypes.length === 0
            ? of({})
            : this.http.post<ResourcePermissions>(
                this.roleService.ENDPOINT_ROLE,
                resourceTypes,
                {
                  headers: {
                    'RUBICK-AJAX-REQUEST': 'true',
                  },
                },
              );

        return permissions$.pipe(
          map(permissions =>
            filterNavConfigByPermissions(navConfig, permissions),
          ),
        );
      }),
      map(config => filterNavConfigByWeblab(config.categories, this.weblabs)),
      switchMap(raw => this.mapToNavConfig(raw)),
      publishReplay(1),
      refCount(),
    );

    this.activatedKey$ = combineLatest(
      this.navConfig$.pipe(map(config => this.flatNavConfig(config))),
      this.router.events.pipe(
        filter(event => event instanceof NavigationEnd),
        map((event: NavigationEnd) => event.url),
        startWith(this.router.url),
        distinctUntilChanged(),
        switchMap(() => this.activatedRoute.paramMap),
      ),
    ).pipe(
      map(([items, paramMap]) => {
        return items.find(item => {
          return this.router.isActive(
            buildUrl(item.left.routerLink, paramMap),
            false,
          );
        });
      }),
      filter(config => !!config),
      tap(config => {
        this.uiState.setActiveItemInfo(config.path);
      }),
      map(config => config.left.key),
      distinctUntilChanged(),
      publishReplay(1),
      refCount(),
    );
  }

  protected flatNavConfig(
    config: NavGroupConfig[],
  ): { path: NavItemConfig[]; left: NavItemConfig }[] {
    const pickChildren = (
      item: NavItemConfig,
      path: NavItemConfig[] = [],
    ): { path: NavItemConfig[]; left: NavItemConfig }[] => {
      return item.children
        ? item.children.reduce(
            (prev, curr) => prev.concat(pickChildren(curr, path.concat(item))),
            [],
          )
        : [{ path: path.concat(item), left: item }];
    };
    return config.reduce(
      (prev, curr) =>
        prev.concat(curr.items.reduce((p, c) => p.concat(pickChildren(c)), [])),
      [],
    );
  }

  private mapToNavConfig(
    items: AppNavItemConfig[],
  ): Observable<NavGroupConfig[]> {
    return this.translate.currentLang$.pipe(
      map(() => {
        return items.map(category => ({
          title: this.translate.get(category.name),
          items: category.children.map(item => this.mapToNavItemConfig(item)),
        }));
      }),
    );
  }

  private mapToNavItemConfig(config: AppNavItemConfig): NavItemConfig {
    return {
      ...config,
      label: this.translate.get(config.name),
      key: config.routerLink && config.routerLink.join('/'),
      routerLink: config.routerLink && config.routerLink.join('/'),
      children:
        config.children &&
        config.children.map(item => this.mapToNavItemConfig(item)),
    };
  }
}

export interface PermissionsContainer {
  permissions?: string[];
  children?: PermissionsContainer[];
  categories?: PermissionsContainer[];
}

export interface ResourcePermissions {
  [resourceType: string]: string[];
}

export function getNavConfigResourceTypes(
  navConfig: AppNavConfig,
): ResourceType[] {
  return unique(
    getAllNavItemPermissions(navConfig).map(
      permission => permission.split(':')[0].trim() as ResourceType,
    ),
  );
}

export function filterNavConfigByWeblab(
  navConfig: AppNavItemConfig[],
  weblabs: Weblabs,
): AppNavItemConfig[] {
  return navConfig.reduce((prev, curr) => {
    if (weblabs[curr.weblab] === false) {
      return prev;
    } else if (curr.children) {
      return prev.concat({
        ...curr,
        children: filterNavConfigByWeblab(curr.children, weblabs),
      });
    } else {
      return prev.concat(curr);
    }
  }, []);
}

export function filterNavConfigByPermissions(
  navConfig: AppNavConfig,
  // Ref: RoleUtiles.resourceTypesPermissions
  resourcePermissions: ResourcePermissions,
): AppNavConfig {
  const flatResourcePermissions = unique(
    Object.values(resourcePermissions).flat(),
  );

  function permissionCheck(item: { permissions?: string[] }) {
    return (
      !item.permissions ||
      item.permissions.some(permission =>
        flatResourcePermissions.includes(permission.trim()),
      )
    );
  }

  // 可以通过递归解决，不过。。算了
  const newNavConfig = {
    categories: navConfig.categories
      .filter(category => permissionCheck(category))
      .map(category => {
        const newCategory = { ...category };
        if (newCategory.children) {
          newCategory.children = newCategory.children
            .filter(primaryItem => permissionCheck(primaryItem))
            .map(primaryItem => {
              const newPrimaryItem = { ...primaryItem };
              if (newPrimaryItem.children) {
                newPrimaryItem.children = newPrimaryItem.children.filter(item =>
                  permissionCheck(item),
                );
              }
              return newPrimaryItem;
            });
        }
        return newCategory;
      }),
  };

  return newNavConfig;
}

function getAllNavItemPermissions(navItem: PermissionsContainer): string[] {
  const childrenPermissions = (
    navItem.children ||
    navItem.categories ||
    []
  ).flatMap(item => getAllNavItemPermissions(item));
  return [...getNavItemPermissions(navItem), ...childrenPermissions];
}

function getNavItemPermissions(item: { permissions?: string[] }): string[] {
  return item.permissions || [];
}

function unique<T>(input: T[]) {
  return Array.from(new Set(input));
}

export interface AppNavItemConfig {
  name: string;
  permissions?: string[];
  weblab?: string;
  children?: AppNavItemConfig[];
  routerLink?: string[];
  icon?: string;
}

export interface AppNavConfig {
  categories: AppNavItemConfig[];
  // TODO: more attributes can be added here:
}
