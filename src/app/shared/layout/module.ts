import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { AccountMenuComponent } from './account-menu/component';
import { AmpEntryComponent } from './amp-entry/amp-entry.component';
import { ProjectBadgeComponent } from './project-badge/component';
import { ProjectBadgeTooltipComponent } from './project-badge/tooltip';
import { UpdatePasswordComponent } from './update-password/component';

const COMPONENTS = [
  AccountMenuComponent,
  UpdatePasswordComponent,
  ProjectBadgeComponent,
  ProjectBadgeTooltipComponent,
  AmpEntryComponent,
];

@NgModule({
  imports: [SharedModule],
  exports: [...COMPONENTS],
  declarations: [...COMPONENTS],
  entryComponents: [UpdatePasswordComponent],
})
export class SharedLayoutModule {}
