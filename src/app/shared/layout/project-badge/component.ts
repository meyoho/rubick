import { TooltipDirective } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { OnDestroy } from '@angular/core';
import { PRIMARY_OUTLET, Router, UrlSegment } from '@angular/router';

import { sortBy } from 'lodash-es';
import { Observable, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { Project } from 'app/services/api/project.service';
import { Space } from 'app/services/api/space.service';
import { ENVIRONMENTS, WEBLABS } from 'app/shared/tokens';
import * as spaceActions from 'app/store/actions/spaces.actions';
import { ProjectsFacadeService, SpacesFacadeService } from 'app/store/facades';
import { Environments, Weblabs } from 'app/typings';

@Component({
  selector: 'rc-project-badge',
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
})
export class ProjectBadgeComponent implements OnInit, OnDestroy {
  @ViewChild('projectTooltip', { read: TooltipDirective })
  projectTooltip: TooltipDirective;

  projects$: Observable<Project[]>;
  currentProject$: Observable<Project>;

  onDestroy$ = new Subject<void>();

  constructor(
    private spacesFacade: SpacesFacadeService,
    private projectsFacade: ProjectsFacadeService,
    @Inject(WEBLABS) private weblabs: Weblabs,
    @Inject(ENVIRONMENTS) private environments: Environments,
    private router: Router,
  ) {}

  ngOnInit() {
    this.currentProject$ = this.projectsFacade.currentProject$;

    this.projects$ = this.projectsFacade.projects$.pipe(
      map((projects: Project[]) => {
        return sortBy(projects, project => project.name);
      }),
    );
    this.projects$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((projects: Project[]) => {
        if (!!this.environments.embedded_mode) {
          return;
        }
        // check if project name in url really exists
        const projectNameInUrl = this.getProjectNameInUrl();
        if (projects.length) {
          if (projectNameInUrl) {
            const project = projects.find(
              item => item.name === projectNameInUrl,
            );
            if (!project) {
              this.redirectToOverview(projects[0].name);
            }
          } else if (!projectNameInUrl) {
            this.redirectToOverview(projects[0].name);
          }
        } else {
          // API request failed, do nothing...
        }
      });

    this.currentProject$
      .pipe(
        takeUntil(this.onDestroy$),
        distinctUntilChanged((p, q) => {
          return p.name === q.name;
        }),
        tap((project: Project) => {
          this.spacesFacade.dispatch(
            new spaceActions.GetSpacesByProject({
              project_name: project.name,
            }),
          );
        }),
        switchMap((project: Project) => {
          return this.spacesFacade.getSpacesByProjectName$(project.name);
        }),
      )
      .subscribe((spaces: Space[]) => {
        // 如果 GROUP_ENABLED 没有打开，把当前 project 下第一个 space 设置为 store 里的 currentSpace
        if (!this.weblabs.GROUP_ENABLED) {
          const space = spaces[0];
          this.spacesFacade.dispatch(
            new spaceActions.SetCurrentSpace(space ? space.uuid : null),
          );
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  onSelect(name: string) {
    this.projectTooltip.disposeTooltip();
    this.router.navigate([
      '/console/user/project',
      {
        project: name,
      },
    ]);
  }

  private getProjectNameInUrl() {
    const url = this.router.url;
    const tree = this.router.parseUrl(url);
    const segments = tree.root.children[PRIMARY_OUTLET].segments;
    const segmentWithProjectParam = segments.find((segment: UrlSegment) => {
      const parameters = segment.parameters;
      return !!parameters.project;
    });
    return segmentWithProjectParam
      ? segmentWithProjectParam.parameters.project
      : null;
  }

  private redirectToOverview(project?: string) {
    this.router.navigate(['/console/user/project', { project }]);
  }
}
