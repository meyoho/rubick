import {
  ChangeDetectionStrategy,
  Component,
  ElementRef,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import {
  AfterViewInit,
  EventEmitter,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import {
  BehaviorSubject,
  combineLatest,
  Observable,
  ReplaySubject,
} from 'rxjs';
import {
  debounceTime,
  map,
  publishReplay,
  refCount,
  startWith,
} from 'rxjs/operators';

import { Project } from 'app/services/api/project.service';

@Component({
  selector: 'rc-project-badge-tooltip',
  templateUrl: 'tooltip.html',
  styleUrls: ['./tooltip.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectBadgeTooltipComponent implements OnChanges, AfterViewInit {
  @Input() projects: Project[];
  @Input() selectedProjectName: string;
  @Output() onSelect = new EventEmitter<string>();
  @ViewChild('filterInput', { read: ElementRef })
  filterInput: ElementRef;

  private projects$: ReplaySubject<Project[]> = new ReplaySubject();
  filteredProjects$: Observable<Project[]>;
  filterName$: BehaviorSubject<string> = new BehaviorSubject<string>('');
  filterName: string;

  constructor() {
    this.filteredProjects$ = combineLatest(
      this.filterName$.pipe(
        debounceTime(200),
        startWith(''),
      ),
      this.projects$,
    ).pipe(
      map(([name, projects]) => {
        name = name.trim();
        return name
          ? projects.filter((project: Project) => {
              const projectName = project.display_name || project.name;
              return projectName.toLowerCase().includes(name.toLowerCase());
            })
          : projects;
      }),
      publishReplay(1),
      refCount(),
    );
  }

  ngOnChanges({ projects }: SimpleChanges) {
    if (projects && projects.currentValue) {
      this.projects$.next(projects.currentValue);
    }
  }

  ngAfterViewInit() {
    if (this.filterInput) {
      this.filterInput.nativeElement.focus();
    }
  }

  selectProject(project: Project) {
    this.onSelect.emit(project.name);
  }

  trackByProjectId(_index: number, project: Project) {
    return project.uuid;
  }
}
