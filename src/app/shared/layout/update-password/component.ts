import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { AccountService } from 'app/services/api/account.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import {
  PatternHelperService,
  PwdPattern,
} from 'app/services/pattern-helper.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class UpdatePasswordComponent implements OnInit {
  pwdLoading = false;
  submitting = false;
  username: string;
  isSubAccount = false;
  formModel = { password: '', confirmPassword: '', oldPassword: '' };
  pwdPattern: PwdPattern;
  pwdVisible = false;
  @ViewChild('form')
  form: NgForm;

  constructor(
    private accountService: AccountService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private errorHandle: ErrorsToastService,
    private patternHelper: PatternHelperService,
    @Inject(ENVIRONMENTS) public environments: Environments,
    @Inject(DIALOG_DATA) public dialogData: { username: string },
    private dialogRef: DialogRef<any>,
  ) {}

  ngOnInit() {
    this.username = this.dialogData.username || '';
    this.isSubAccount = this.accountService.isSubAccount();
    this.pwdPattern = this.patternHelper.getPasswordPattern();
  }

  async saveNewPassword() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const params: any = {
      username: this.username || undefined,
      password: this.form.value.password,
    };
    if ((!this.isSubAccount && !this.username) || this.isSubAccount) {
      params.old_password = this.form.value.old_password;
    }
    this.submitting = true;

    try {
      if (this.username) {
        await this.accountService.updateSubAccountPassword(params);
      } else {
        await this.accountService.updateProfile(params as any);
      }
      this.auiNotificationService.success({
        content: this.translate.get('update_password_success'),
      });
      this.dialogRef.close();
    } catch (e) {
      this.errorHandle.error(e);
    } finally {
      this.submitting = false;
    }
  }

  get cPwdPattern() {
    return this.formModel.password === this.formModel.confirmPassword
      ? /\.*/
      : /\.{,0}/;
  }
}
