import { NgModule } from '@angular/core';

import { BytesFormatterPipe } from 'app2/shared/pipes/bytes-formatter.pipe';
import { CaptalizeAllPipe } from 'app2/shared/pipes/capitalize-all.pipe';
import { CrontabNextPipe } from 'app2/shared/pipes/crontab-next.pipe';
import { CrontabParsePipe } from 'app2/shared/pipes/crontab.pipe';
import { DurationToConsumedPipe } from 'app2/shared/pipes/duration-to-consumed.pipe';
import { FieldNotAvailablePipe } from 'app2/shared/pipes/field-not-available.pipe';
import { FitMemPipe } from 'app2/shared/pipes/fit-mem.pipe';
import { FormatMs } from 'app2/shared/pipes/format-ms.pipe';
import { FormatUtcStrPipe } from 'app2/shared/pipes/format-utc-str.pipe';
import { LimitToPipe } from 'app2/shared/pipes/limit-to.pipe';
import { MarkdownToHtmlPipe } from 'app2/shared/pipes/markdown-to-html.pipe';
import { MarkedPipe } from 'app2/shared/pipes/marked.pipe';
import { MsToConsumedPipe } from 'app2/shared/pipes/ms-to-consumed.pipe';
import { ObjectToMapPipe } from 'app2/shared/pipes/object-to-map.pipe';
import { RelativeTimePipe } from 'app2/shared/pipes/relative-time.pipe';
import { SvgTranslatePipe } from 'app2/shared/pipes/svg-translate.pipe';
import { ThousandUnitPipe } from 'app2/shared/pipes/thousand-unit.pipe';
import { TrustHtmlPipe } from 'app2/shared/pipes/trust-html.pipe';
import { TrustStylePipe } from 'app2/shared/pipes/trust-style.pipe';
import { TrustUrlPipe } from 'app2/shared/pipes/trust-url.pipe';
import { UrlProxy } from 'app2/shared/pipes/url-proxy.pipe';
import { UtcToConsumedPipe } from 'app2/shared/pipes/utc-to-consumed.pipe';
import { PurePipe } from './pipes/pure.pipe';

const pipes = [
  TrustHtmlPipe,
  FormatUtcStrPipe,
  UtcToConsumedPipe,
  FormatMs,
  MsToConsumedPipe,
  DurationToConsumedPipe,
  ObjectToMapPipe,
  TrustUrlPipe,
  TrustStylePipe,
  ThousandUnitPipe,
  FitMemPipe,
  SvgTranslatePipe,
  FieldNotAvailablePipe,
  LimitToPipe,
  BytesFormatterPipe,
  MarkedPipe,
  CaptalizeAllPipe,
  MarkdownToHtmlPipe,
  CrontabNextPipe,
  CrontabParsePipe,
  UrlProxy,
  RelativeTimePipe,
  PurePipe,
];

@NgModule({
  declarations: pipes,
  exports: pipes,
})
export class PipesModule {}
