import { CodeEditorModule } from '@alauda/code-editor';
import {
  AccordionModule,
  AutocompleteModule,
  BreadcrumbModule,
  ButtonModule,
  CardModule,
  CheckboxModule,
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  StatusBarModule,
  SwitchModule,
  TableModule as AuiTableModule,
  TableOfContensModaule,
  TabsModule,
  TagModule,
  TooltipModule as AuiTooltipModule,
} from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FlexLayoutModule } from '@angular/flex-layout';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';

import { HighchartsChartModule } from 'highcharts-angular';

import { PageHeaderContentDirective } from 'app/shared/directives/page-header-content.directive';
import { TranslateModule } from 'app/translate/translate.module';
import { ProjectAccountsListComponent } from 'app2/features/projects/accounts-list/accounts-list.component';
import { ProjectPieChartComponent } from 'app2/features/projects/pie/project-pie-chart.component';
import { ProjectRoleListComponent } from 'app2/features/projects/role-list/project-role-list.component';
import { DynamicFormFieldComponent } from 'app2/shared/components/dynamic-form/dynamic-form-field.component';
import { DynamicFormComponent } from 'app2/shared/components/dynamic-form/dynamic-form.component';
import { FlexCellComponent } from 'app2/shared/components/flex-list/flex-cell/flex-cell.component';
import { FlexListComponent } from 'app2/shared/components/flex-list/flex-list.component';
import { ForceDeleteComponent } from 'app2/shared/components/force-delete/component';
import { LoadingMaskComponent } from 'app2/shared/components/loading-mask/loading-mask.component';
import { MenuTriggerModule } from 'app2/shared/components/menu-trigger/menu-trigger.module';
import { PasswordModule } from 'app2/shared/components/password';
import { PasswordStrengthLabelComponent } from 'app2/shared/components/password-strength-label/password-strength-label.component';
import { PasswordStrengthComponent } from 'app2/shared/components/password-strength/password-strength.component';
import { StatusBadgeComponent } from 'app2/shared/components/status-badge/status-badge.component';
import {
  TabNavDirective,
  VerticalTabComponent,
  VerticalTabsComponent,
} from 'app2/shared/components/tabs/vertical-tabs.component';
import { TagsLabelComponent } from 'app2/shared/components/tags-label/tags-label.component';
import { YamlCommentFormComponent } from 'app2/shared/components/yaml-comment-form/yaml-comment-form.component';
import { AsyncDataDirective } from 'app2/shared/directives/async-data/async-data.directive';
import { CalendarDirective } from 'app2/shared/directives/calendar/calendar.directive';
import { FileReaderDirective } from 'app2/shared/directives/file-reader/file-reader.directive';
import {
  FormDirective,
  RcFormGroupDirective,
} from 'app2/shared/directives/form/form.directive';
import { LoadingMaskDirective } from 'app2/shared/directives/loading-mask/loading-mask.directive';
import { RegionBadgeOptionDirective } from 'app2/shared/directives/region-badge-option/region-badge-option.directive';
import { TooltipModule } from 'app2/shared/directives/tooltip/tooltip.module';
import { TrimDirective } from 'app2/shared/directives/trim/trim.directive';
import { AuiCodeEditorHelperDirective } from 'app2/shared/directives/utilities/aui-code-editor.directive';
import { VisibilityDirective } from 'app2/shared/directives/utilities/visibility.directive';
import { CronValidatorDirective } from 'app2/shared/directives/validators/cron.directive';
import {
  MaxValidatorDirective,
  MinValidatorDirective,
} from 'app2/shared/directives/validators/min-max.directive';
import { ActionsToTranslateKeysPipe } from 'app2/shared/pipes/actions-to-translate-key.pipe';
import {
  PipelineApplicationContainerComponent,
  PipelineApplicationContainerValidatorDirective,
  PipelineDyformCredentialComponent,
  PipelineDyformEnvComponent,
  PipelineDyformrRepositoryComponent,
  PipelineDyfromConfigMapComponent,
} from 'app_user/features/jenkins/components/pipeline-dynamic-form';
import { DefaultBreadcrumbComponent } from 'app_user/shared/components/breadcrumb/default-breadcrumb.component';
import { FoldableBlockComponent } from 'app_user/shared/components/foldable-block/component';
import { MirrorClusterSelectComponent } from 'app_user/shared/components/mirror-clutser-select/mirror-cluster-select.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import { PasswordInputComponent } from 'app_user/shared/components/password-input/component';
import { StatusIconComponent } from 'app_user/shared/components/status-icon/status-icon.component';
import { StepsProcessComponent } from 'app_user/shared/components/steps-process/steps-process.component';
import { ViewResourceYamlComponent } from 'app_user/shared/components/view-resource-yaml/view-resource-yaml.component';
import { ZeroStateComponent } from 'app_user/shared/components/zero-state/zero-state.component';
import { PipesModule } from './pipes.module';

const NG_MODULES = [
  CommonModule,
  FormsModule,
  ReactiveFormsModule,
  RouterModule,
];

const THIRD_PARTY_MODULES = [
  FlexLayoutModule,
  HighchartsChartModule,
  PortalModule,
  PasswordModule,
];

const AUI_MODULES = [
  AccordionModule,
  AuiTableModule,
  AutocompleteModule,
  BreadcrumbModule,
  ButtonModule,
  CardModule,
  CheckboxModule,
  CodeEditorModule,
  DialogModule,
  DropdownModule,
  FormModule,
  IconModule,
  InlineAlertModule,
  InputModule,
  PaginatorModule,
  RadioModule,
  SelectModule,
  SortModule,
  StatusBarModule,
  SwitchModule,
  TabsModule,
  TagModule,
  TableOfContensModaule,
  AuiTooltipModule,
];

const SHARED_MODUELS = [TooltipModule];

const EXPORTABLE_MODULES = [
  PipesModule,
  TranslateModule,
  MenuTriggerModule,
  ...NG_MODULES,
  ...THIRD_PARTY_MODULES,
  ...AUI_MODULES,
  ...SHARED_MODUELS,
];

const SHARED_DIRECTIVES = [
  AuiCodeEditorHelperDirective,
  CalendarDirective,
  AsyncDataDirective,
  CronValidatorDirective,
  FileReaderDirective,
  FormDirective,
  LoadingMaskDirective,
  MaxValidatorDirective,
  MinValidatorDirective,
  PageHeaderContentDirective,
  RcFormGroupDirective,
  RegionBadgeOptionDirective,
  TrimDirective,
  VisibilityDirective,
  ActionsToTranslateKeysPipe,
];

const SHARED_COMPONENTS = [
  DefaultBreadcrumbComponent,
  DynamicFormComponent,
  DynamicFormFieldComponent,
  FlexCellComponent,
  FlexListComponent,
  FoldableBlockComponent,
  LoadingMaskComponent,
  MirrorClusterSelectComponent,
  MirrorResourceDeleteConfirmComponent,
  PasswordStrengthComponent,
  PasswordStrengthLabelComponent,
  PipelineApplicationContainerComponent,
  PipelineApplicationContainerValidatorDirective,
  PipelineDyformCredentialComponent,
  PipelineDyformEnvComponent,
  PipelineDyformrRepositoryComponent,
  PipelineDyfromConfigMapComponent,
  ProjectAccountsListComponent,
  ProjectPieChartComponent,
  ProjectRoleListComponent,
  StatusBadgeComponent,
  ForceDeleteComponent,
  PasswordInputComponent,
  // ********** app_user shared components **********//
  StatusIconComponent,
  StepsProcessComponent,
  TabNavDirective,
  TagsLabelComponent,
  VerticalTabComponent,
  VerticalTabsComponent,
  ViewResourceYamlComponent,
  YamlCommentFormComponent,
  ZeroStateComponent,
];

const ENTRY_COMPONENTS = [
  LoadingMaskComponent,
  MirrorResourceDeleteConfirmComponent,
  ViewResourceYamlComponent,
  ForceDeleteComponent,
];

/**
 *  Entry shared module containers source with app2 and app_user
 */
@NgModule({
  imports: [...EXPORTABLE_MODULES],
  exports: [...EXPORTABLE_MODULES, ...SHARED_DIRECTIVES, ...SHARED_COMPONENTS],
  declarations: [...SHARED_DIRECTIVES, ...SHARED_COMPONENTS],
  entryComponents: [...ENTRY_COMPONENTS],
})
export class SharedModule {}
