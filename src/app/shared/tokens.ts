/**
 * Make weblabs or other non class ts interfaces to be injectable
 */
import { InjectionToken } from '@angular/core';

import { Account, Environments, I18nManifest, Weblabs } from '../typings';

export const WEBLABS = new InjectionToken<Weblabs>('WEBLABS');
export const ENVIRONMENTS = new InjectionToken<Environments>('ENVIRONMENTS');
export const ACCOUNT = new InjectionToken<Account>('ACCOUNT');
export const TRANSLATIONS = new InjectionToken<Weblabs>('TRANSLATIONS');
export const I18NMANIFEST = new InjectionToken<I18nManifest>('I18NMANIFEST');
