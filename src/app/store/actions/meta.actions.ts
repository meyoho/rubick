// metadata reducer releated Action

import { Action } from '@ngrx/store';

export const BATCH = `[global] BATCH`;
export const LOGOUT = `[global] LOGOUT`;

export class BatchAction implements Action {
  public readonly type = BATCH;
  constructor(public payload: Action[]) {}
}

export class LogoutAction implements Action {
  public readonly type = LOGOUT;
  constructor() {}
}

export type MetaActions = BatchAction | LogoutAction;
