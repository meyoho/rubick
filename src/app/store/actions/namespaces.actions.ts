import { Action } from '@ngrx/store';

import { Namespace } from 'app/services/api/namespace.service';
import { ErrorResponse } from 'app/services/http.service';

export const GET_CURRENT_NAMESPACES = '[Namespace] get current';
export const GET_NAMESPACES_BATCH_BY_PROJECT =
  '[Namespace] get batch by project';
export const GET_NAMESPACES_BATCH_BY_PROJECT_SUCC =
  '[Namespace] get batch by project success';
export const GET_NAMESPACES_BATCH_BY_PROJECT_FAIL =
  '[Namespace] get batch by project fail';
export const GET_NAMESPACES_BY_CLUSTER_AND_PROJECT =
  '[Namespace] get by cluster and project';
export const GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_SUCC =
  '[Namespace] get by cluster and project success';
export const GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_FAIL =
  '[Namespace] get by cluster and project failed';

export class GetCurrentNamespaces implements Action {
  readonly type = GET_CURRENT_NAMESPACES;
  constructor() {}
}

export class GetNamespacesBatchByProject implements Action {
  readonly type = GET_NAMESPACES_BATCH_BY_PROJECT;
  constructor(public payload: { clusters: string[]; project: string }) {}
}

export class GetNamespacesBatchByProjectSucc implements Action {
  readonly type = GET_NAMESPACES_BATCH_BY_PROJECT_SUCC;
  constructor(
    public payload: {
      project: string;
      data: {
        [cluster: string]: Namespace[];
      };
    },
  ) {}
}

export class GetNamespacesBatchByProjectFail implements Action {
  readonly type = GET_NAMESPACES_BATCH_BY_PROJECT_FAIL;
  constructor(public payload: ErrorResponse) {}
}

export class GetNamespacesByClusterAndProject implements Action {
  readonly type = GET_NAMESPACES_BY_CLUSTER_AND_PROJECT;
  constructor(public payload: { project: string; cluster: string }) {}
}

export class GetNamespacesByClusterAndProjectSucc implements Action {
  readonly type = GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_SUCC;
  constructor(
    public payload: {
      project: string;
      cluster: string;
      namespaces: Namespace[];
    },
  ) {}
}

export class GetNamespacesByClusterAndProjectFail implements Action {
  readonly type = GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_FAIL;
  constructor(public payload: ErrorResponse) {}
}

export type NamespaceAction =
  | GetCurrentNamespaces
  | GetNamespacesBatchByProject
  | GetNamespacesBatchByProjectSucc
  | GetNamespacesBatchByProjectFail
  | GetNamespacesByClusterAndProject
  | GetNamespacesByClusterAndProjectSucc
  | GetNamespacesByClusterAndProjectFail;
