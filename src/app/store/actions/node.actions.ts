import { Action } from '@ngrx/store';

import { ClusterNode } from 'app/services/api/region.service';
import { ErrorResponse } from 'app/services/http.service';

export const GET_NODES_BY_REGION = '[Node] get by region';
export const GET_NODES_BY_REGION_SUCC = '[Node] get by region success';
export const GET_NODES_BY_REGION_FAIL = '[Node] get by region failed';

export class GetNodesByRegion implements Action {
  readonly type = GET_NODES_BY_REGION;
  constructor(
    public payload: string | { regionName: string; isK8sV4?: boolean },
  ) {}
}

export class GetNodesByRegionSucc implements Action {
  readonly type = GET_NODES_BY_REGION_SUCC;
  constructor(public payload: { regionName: string; nodes: ClusterNode[] }) {}
}

export class GetNodesByRegionFail implements Action {
  readonly type = GET_NODES_BY_REGION_FAIL;
  constructor(public payload: { regionName: string; error: ErrorResponse }) {}
}

export type NodeAction =
  | GetNodesByRegion
  | GetNodesByRegionSucc
  | GetNodesByRegionFail;
