import { Action } from '@ngrx/store';

import { Space } from 'app/services/api/space.service';
import { ErrorResponse } from 'app/services/http.service';

export const GET_CURRENT_SPACE = '[Space] get current space';
export const SET_CURRENT_SPACE = '[Space] set current space';
export const GET_CURRENT_SPACES = '[Space] get current spaces';
export const GET_SPACES_BY_PROJECT = '[Space] get spaces by project';
export const GET_SPACES_BY_PROJECT_SUCC = '[Space] get spaces by project succ';
export const GET_SPACES_BY_PROJECT_FAIL = '[Space] get spaces by project fail';

export class SetCurrentSpace implements Action {
  readonly type = SET_CURRENT_SPACE;
  // payload is space uuid
  constructor(public payload: string) {}
}

export class GetSpacesByProject implements Action {
  readonly type = GET_SPACES_BY_PROJECT;
  constructor(public payload: { project_name: string }) {}
}
export class GetSpacesByProjectSucc implements Action {
  readonly type = GET_SPACES_BY_PROJECT_SUCC;
  constructor(public payload: { project_name: string; spaces: Space[] }) {}
}
export class GetSpacesByProjectFail implements Action {
  readonly type = GET_SPACES_BY_PROJECT_FAIL;
  constructor(public payload: ErrorResponse) {}
}

export type SpaceAction =
  | SetCurrentSpace
  | GetSpacesByProject
  | GetSpacesByProjectSucc
  | GetSpacesByProjectFail;
