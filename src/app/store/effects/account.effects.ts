import { Inject, Injectable } from '@angular/core';
import { Effect } from '@ngrx/effects';
import { Action } from '@ngrx/store';

import { Observable, of } from 'rxjs';

import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import * as accountActions from '../actions/account.actions';

@Injectable()
export class AccountEffects {
  @Effect()
  setAccount$: Observable<Action> = of(
    new accountActions.SetAccount(this.account),
  );

  constructor(@Inject(ACCOUNT) private account: Account) {}
}
