import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { from, of } from 'rxjs';
import { catchError, map, mergeMap, pluck, switchMap } from 'rxjs/operators';

import {
  Cluster,
  ClusterAccessData,
  RegionService,
} from 'app/services/api/region.service';
import * as clusterActions from '../actions/clusters.actions';

@Injectable()
export class ClusterEffects {
  constructor(
    private actions$: Actions,
    private regionService: RegionService,
  ) {}

  @Effect()
  getClusters$ = this.actions$.pipe(
    ofType(clusterActions.GET_CLUSTERS),
    switchMap(() => {
      return from(this.regionService.getRegions()).pipe(
        map(
          (clusters: Cluster[]) =>
            new clusterActions.GetClustersSuccess(clusters),
        ),
        catchError(error => of(new clusterActions.GetClustersFail(error))),
      );
    }),
  );

  @Effect()
  getByName$ = this.actions$.pipe(
    ofType(clusterActions.GET_CLUSTER_BY_NAME),
    pluck('payload'),
    mergeMap((name: string) =>
      from(this.regionService.getCluster(name)).pipe(
        map(cluster => new clusterActions.GetClusterByNameSuccess(cluster)),
        catchError(() => of(new clusterActions.GetClusterByNameFail(name))),
      ),
    ),
  );

  @Effect()
  accessCluster$ = this.actions$.pipe(
    ofType(clusterActions.CREATE_CLUSTER),
    pluck('payload'),
    switchMap((payload: ClusterAccessData) => {
      return from(this.regionService.accessCluster(payload)).pipe(
        map(cluster => new clusterActions.CreateClusterSuccess(cluster)),
        catchError(rawError =>
          of(new clusterActions.CreateClusterFail(rawError)),
        ),
      );
    }),
  );

  @Effect()
  deleteCluster$ = this.actions$.pipe(
    ofType(clusterActions.DELETE_CLUSTER_BY_NAME),
    pluck('payload'),
    switchMap((payload: { name: string; force: boolean }) => {
      return from(
        this.regionService.deleteCluster(payload.name, payload.force),
      ).pipe(
        map(cluster => new clusterActions.DeleteClusterByNameSuccess(cluster)),
        catchError(rawError =>
          of(
            new clusterActions.DeleteClusterByNameFail({
              error: rawError,
              force: payload.force,
            }),
          ),
        ),
      );
    }),
  );
}
