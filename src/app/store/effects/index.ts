import { AccountEffects } from './account.effects';
import { ClusterEffects } from './clusters.effects';
import { NamespacesEffects } from './namespaces.effects';
import { NodeEffects } from './node.effects';
import { ProjectEffects } from './projects.effects';
import { SpacesEffects } from './spaces.effects';

export const effects: any[] = [
  AccountEffects,
  ProjectEffects,
  ClusterEffects,
  NodeEffects,
  NamespacesEffects,
  SpacesEffects,
];

export * from './account.effects';
export * from './projects.effects';
export * from './clusters.effects';
export * from './node.effects';
export * from './namespaces.effects';
export * from './spaces.effects';
