import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { from, of } from 'rxjs';
import { catchError, groupBy, map, mergeMap, switchMap } from 'rxjs/operators';

import {
  Namespace,
  NamespaceService,
} from 'app/services/api/namespace.service';
import * as namespaceActions from '../actions/namespaces.actions';

@Injectable()
export class NamespacesEffects {
  constructor(
    private actions$: Actions,
    private namespaceService: NamespaceService,
  ) {}

  @Effect()
  getNamespacesByCluster$ = this.actions$.pipe(
    ofType(namespaceActions.GET_NAMESPACES_BY_CLUSTER_AND_PROJECT),
    map(
      (action: namespaceActions.GetNamespacesByClusterAndProject) =>
        action.payload,
    ),
    groupBy(cluster => cluster),
    mergeMap(group => {
      return group.pipe(
        switchMap(({ cluster, project }) => {
          return from(
            this.namespaceService.getNamespaces(cluster, {
              project_name: project,
            }),
          ).pipe(
            map(
              (namespaces: Namespace[]) =>
                new namespaceActions.GetNamespacesByClusterAndProjectSucc({
                  project,
                  cluster,
                  namespaces,
                }),
            ),
            catchError(error =>
              of(
                new namespaceActions.GetNamespacesByClusterAndProjectFail(
                  error,
                ),
              ),
            ),
          );
        }),
      );
    }),
  );

  @Effect()
  getNamespacesBatch$ = this.actions$.pipe(
    ofType(namespaceActions.GET_NAMESPACES_BATCH_BY_PROJECT),
    map(
      (action: namespaceActions.GetNamespacesBatchByProject) => action.payload,
    ),
    switchMap(({ clusters, project }) => {
      return from(
        this.namespaceService.getNamespacesBatch(clusters, project),
      ).pipe(
        map(
          result =>
            new namespaceActions.GetNamespacesBatchByProjectSucc({
              project,
              data: result,
            }),
        ),
        catchError(error =>
          of(new namespaceActions.GetNamespacesBatchByProjectFail(error)),
        ),
      );
    }),
  );
}
