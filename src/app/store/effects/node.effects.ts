import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { from, of } from 'rxjs';
import { catchError, map, mergeMap, pluck } from 'rxjs/operators';

import { RegionService } from 'app/services/api/region.service';
import * as nodeActions from '../actions/node.actions';

@Injectable()
export class NodeEffects {
  @Effect()
  getByRegion$ = this.actions.pipe(
    ofType(nodeActions.GET_NODES_BY_REGION),
    pluck('payload'),
    mergeMap((params: string | { regionName: string; isK8sV4?: boolean }) => {
      let regionName: string;

      if (typeof params === 'string') {
        regionName = params;
      } else {
        regionName = params.regionName;
      }

      return from(
        this.regionService
          .getClusterNodes(regionName)
          .then(({ items }) => items),
      ).pipe(
        map(
          nodes => new nodeActions.GetNodesByRegionSucc({ regionName, nodes }),
        ),
        catchError(error =>
          of(new nodeActions.GetNodesByRegionFail({ regionName, error })),
        ),
      );
    }),
  );

  constructor(private actions: Actions, private regionService: RegionService) {}
}
