import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { from, of } from 'rxjs';
import { catchError, map, pluck, switchMap, tap } from 'rxjs/operators';

import { Project } from 'app/services/api/project.service';
import { ProjectService } from 'app/services/api/project.service';
import * as projectActions from '../actions/projects.actions';
import { SyncProjectCookie } from '../actions/projects.actions';

@Injectable()
export class ProjectEffects {
  constructor(
    private actions$: Actions,
    private projectService: ProjectService,
  ) {}

  @Effect()
  loadProjects$ = this.actions$.pipe(
    ofType(projectActions.LOAD_PROJECTS),
    switchMap(() => {
      return from(
        this.projectService.getProjects({
          page_size: 1000,
        }),
      ).pipe(
        pluck('results'),
        map(
          (projects: Project[]) =>
            new projectActions.LoadProjectsSuccess(projects),
        ),
        catchError(error => of(new projectActions.LoadProjectsFail(error))),
      );
    }),
  );

  @Effect()
  loadCurrentProject$ = this.actions$.pipe(
    ofType(projectActions.LOAD_PROJECT_DETAIL),
    map((action: projectActions.LoadProjectDetail) => action.payload),
    switchMap((payload: string) => {
      return from(this.projectService.getProject(payload)).pipe(
        map((project: Project) =>
          !!project
            ? new projectActions.LoadProjectDetailSuccess(project)
            : new projectActions.LoadProjectDetailFail(null),
        ),
      );
    }),
  );

  // TODO: 暂时用不到
  // @Effect()
  // loadProjectNamespaces$ = this.actions$
  //   .pipe(ofType(projectActions.LOAD_PROJECT_DETAIL_SUCCESS))
  //   .pipe(
  //     map((action: projectActions.LoadProjectDetailSuccess) => action.payload),
  //     flatMap((project: Project) => {
  //       const clusters = project.clusters || []; // project下没有关联cluster时，该字段不存在
  //       return clusters.map(({ name }) => {
  //         return new fromStore.GetNamespacesByClusterAndProject({
  //           project: project.name,
  //           cluster: name,
  //         });
  //       });
  //     }),
  //   );

  @Effect({ dispatch: false })
  syncProjectCookie$ = this.actions$.pipe(
    ofType(projectActions.SYNC_PROJECT_COOKIE),
    tap(({ payload }: SyncProjectCookie) => {
      window.sessionStorage.setItem('project', payload);
    }),
  );
}
