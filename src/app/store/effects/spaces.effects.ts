import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';

import { from, of } from 'rxjs';
import { catchError, map, switchMap, tap } from 'rxjs/operators';

import { SpaceService } from 'app/services/api/space.service';
import * as spaceActions from '../actions/spaces.actions';

@Injectable()
export class SpacesEffects {
  constructor(private actions$: Actions, private spaceService: SpaceService) {}

  @Effect({ dispatch: false })
  setCurrentSpace$ = this.actions$.pipe(
    ofType(spaceActions.SET_CURRENT_SPACE),
    tap(({ payload }: spaceActions.SetCurrentSpace) => {
      payload
        ? window.sessionStorage.setItem('space', payload)
        : window.sessionStorage.removeItem('space');
    }),
  );

  @Effect()
  getSpaceByProject$ = this.actions$.pipe(
    ofType(spaceActions.GET_SPACES_BY_PROJECT),
    map((action: spaceActions.GetSpacesByProject) => action.payload),
    switchMap(({ project_name }) => {
      return from(this.spaceService.getSpaces({ project_name })).pipe(
        map(
          ({ results }) =>
            new spaceActions.GetSpacesByProjectSucc({
              project_name: project_name,
              spaces: results,
            }),
        ),
        catchError(error => of(new spaceActions.GetSpacesByProjectFail(error))),
      );
    }),
  );
}
