import { Injectable } from '@angular/core';
import { select, Store } from '@ngrx/store';

import { filter, publishReplay, refCount } from 'rxjs/operators';

import { AppState } from 'app/store';
import { ClusterAction } from 'app/store/actions/clusters.actions';
import * as fromCluster from 'app/store/selectors/clusters.selectors';

@Injectable({
  providedIn: 'root',
})
export class ClustersFacadeService {
  constructor(private store: Store<AppState>) {}

  getClusters$() {
    return this.store.pipe(
      select(fromCluster.getAllClusters),
      filter(r => !!r),
      publishReplay(1),
      refCount(),
    );
  }

  dispatch(action: ClusterAction) {
    this.store.dispatch(action);
  }
}
