import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { AppState } from 'app/store';
import { NamespaceAction } from 'app/store/actions/namespaces.actions';
import * as fromNamespace from 'app/store/selectors/namespaces.selectors';
import { filter, publishReplay, refCount } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class NamespacesFacadeService {
  constructor(private store: Store<AppState>) {}

  getNamespaces$() {
    return this.store.pipe(
      select(fromNamespace.getNamespaces),
      filter(r => !!r),
      publishReplay(1),
      refCount(),
    );
  }

  getNamespacesByClusterAndProject$(cluster: string, project: string) {
    return this.store.pipe(
      select(fromNamespace.getNamespacesByClusterAndProject, {
        cluster,
        project,
      }),
      filter(r => !!r),
      publishReplay(1),
      refCount(),
    );
  }

  dispatch(action: NamespaceAction) {
    this.store.dispatch(action);
  }
}
