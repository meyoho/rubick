import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Project } from 'app/services/api/project.service';
import { AppState } from 'app/store';
import { ProjectAction } from 'app/store/actions/projects.actions';
import * as fromProject from 'app/store/selectors/projects.selectors';
import { Observable } from 'rxjs';
import { filter, publishReplay, refCount } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class ProjectsFacadeService {
  projects$: Observable<Project[]>;
  currentProject$: Observable<Project>;
  currentProjectDetail$: Observable<Project>;

  constructor(private store: Store<AppState>) {
    this.projects$ = this.store.select(fromProject.getAllProjects);
    this.currentProject$ = this.store.pipe(
      select(fromProject.getCurrentProject),
      filter(project => !!project),
      publishReplay(1),
      refCount(),
    );
    this.currentProjectDetail$ = this.store.pipe(
      select(fromProject.getCurrentProjectDetail),
      filter(project => !!project),
      publishReplay(1),
      refCount(),
    );
  }

  dispatch(action: ProjectAction) {
    this.store.dispatch(action);
  }
}
