import { Injectable } from '@angular/core';
import { Store, select } from '@ngrx/store';
import { Space } from 'app/services/api/space.service';
import { AppState } from 'app/store';
import { SpaceAction } from 'app/store/actions/spaces.actions';
import * as fromSpaces from 'app/store/selectors/spaces.selectors';
import { Observable } from 'rxjs';
import { filter, publishReplay, refCount } from 'rxjs/operators';

@Injectable({
  providedIn: 'root',
})
export class SpacesFacadeService {
  currentSpace$: Observable<Space>;
  private spaces$ByProjectName: {
    [project: string]: Observable<Space[]>;
  } = {};

  constructor(private store: Store<AppState>) {
    this.currentSpace$ = this.store.select(fromSpaces.getCurrentSpace).pipe(
      publishReplay(1),
      refCount(),
    );
  }

  getSpacesByProjectName$(project: string): Observable<Space[]> {
    if (!this.spaces$ByProjectName[project]) {
      this.spaces$ByProjectName[project] = this.store.pipe(
        select(fromSpaces.getSpacesByProjectName(project)),
        filter(v => !!v),
        publishReplay(1),
        refCount(),
      );
    }
    return this.spaces$ByProjectName[project];
  }

  dispatch(action: SpaceAction) {
    this.store.dispatch(action);
  }
}
