import * as fromAccount from '../actions/account.actions';

export interface AccountState {
  namespace: string;
  username: string;
}

const initialState: AccountState = { namespace: null, username: null };

export function reducer(
  state: AccountState = initialState,
  action: fromAccount.AccountActions,
): AccountState {
  switch (action.type) {
    case fromAccount.SET_ACCOUNT:
      return action.payload;
  }
  return state;
}
