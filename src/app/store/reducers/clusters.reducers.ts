import { Cluster } from 'app/services/api/region.service';
import * as fromCluster from '../actions/clusters.actions';

export interface ClusterState {
  entities: { [name: string]: Cluster };
  loaded: boolean;
  loading: boolean;
}

export const initialState: ClusterState = {
  entities: {},
  loaded: false,
  loading: false,
};

export function reducer(
  state = initialState,
  action: fromCluster.ClusterAction,
) {
  switch (action.type) {
    case fromCluster.GET_CLUSTERS: {
      return {
        ...state,
        loading: true,
      };
    }
    case fromCluster.GET_CLUSTERS_SUCCESS: {
      const clusters = action.payload;
      const entities = clusters.reduce(
        (entities: { [name: string]: Cluster }, cluster: Cluster) => {
          return {
            ...entities,
            [cluster.name]: cluster,
          };
        },
        {
          ...state.entities,
        },
      );
      return {
        ...state,
        loading: false,
        loaded: true,
        entities,
      };
    }
    case fromCluster.GET_CLUSTERS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
      };
    }
    case fromCluster.GET_CLUSTER_BY_NAME_SUCCESS:
    case fromCluster.CREATE_CLUSTER_SUCCESS:
      const cluster = action.payload;
      const newEntities = {
        ...state.entities,
        [cluster.name]: cluster,
      };
      return {
        ...state,
        entities: newEntities,
      };
    case fromCluster.DELETE_CLUSTER_BY_NAME_SUCCESS:
      const clusterName = action.payload;
      const { [clusterName]: removed, ...entities } = state.entities;
      return {
        ...state,
        entities,
      };
  }

  return state;
}

export const getClusterEntities = (state: ClusterState) => state.entities;
export const getClusterLoading = (state: ClusterState) => state.loading;
export const getClusterLoaded = (state: ClusterState) => state.loaded;
