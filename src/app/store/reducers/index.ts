import {
  ActivatedRouteSnapshot,
  Params,
  RouterStateSnapshot,
} from '@angular/router';
import * as fromRouter from '@ngrx/router-store';
import {
  ActionReducer,
  ActionReducerMap,
  createFeatureSelector,
  MetaReducer,
} from '@ngrx/store';

import * as metaActions from '../actions/meta.actions';
import * as fromAccount from './account.reducers';
import * as fromCluster from './clusters.reducers';
import * as fromNamespace from './namespaces.reducers';
import * as fromNode from './nodes.reducers';
import * as fromProject from './projects.reducers';
import * as fromSpaces from './spaces.reducers';

export interface RouterStateUrl {
  url: string;
  params: Params;
  queryParams: Params;
  // we need project name ...
  projectName: string;
  clusterName: string;
}

export interface AppState {
  router?: fromRouter.RouterReducerState<RouterStateUrl>;
  account?: fromAccount.AccountState;
  projects?: fromProject.ProjectState;
  clusters?: fromCluster.ClusterState;
  nodes?: fromNode.NodeState;
  namespaces?: fromNamespace.NamespaceState;
  spaces?: fromSpaces.SpaceState;
}

export const reducers: ActionReducerMap<AppState> = {
  router: fromRouter.routerReducer,
  account: fromAccount.reducer,
  projects: fromProject.reducer,
  clusters: fromCluster.reducer,
  nodes: fromNode.reducer,
  namespaces: fromNamespace.reducer,
  spaces: fromSpaces.reducer,
};

export const getRouterState = createFeatureSelector<
  fromRouter.RouterReducerState<RouterStateUrl>
>('router');

export const getProjectsState = createFeatureSelector<
  AppState,
  fromProject.ProjectState
>('projects');

export const getClustersState = createFeatureSelector<
  AppState,
  fromCluster.ClusterState
>('clusters');

export const getNodesState = createFeatureSelector<
  AppState,
  fromNode.NodeState
>('nodes');

export const getNamespacesState = createFeatureSelector<
  AppState,
  fromNamespace.NamespaceState
>('namespaces');

export const getSpacesState = createFeatureSelector<
  AppState,
  fromSpaces.SpaceState
>('spaces');

export function batch(
  reducer: ActionReducer<AppState>,
): ActionReducer<AppState> {
  return function(state: AppState, action: any): AppState {
    if ((<metaActions.BatchAction>action).type === metaActions.BATCH) {
      return action.payload.reduce(reducer, state);
    }
    return reducer(state, action);
  };
}

export function logout(
  reducer: ActionReducer<AppState>,
): ActionReducer<AppState> {
  return function(state: AppState, action: any) {
    if ((<metaActions.LogoutAction>action).type === metaActions.LOGOUT) {
      return Object.assign(
        {},
        Object.keys(reducers).reduce((_state, key) => {
          _state[key] = reducers[key](undefined, action);
          return _state;
        }, {}),
      );
    }
    return reducer(state, action);
  };
}

export const metaReducers: MetaReducer<AppState>[] = [batch, logout];

export class CustomSerializer
  implements fromRouter.RouterStateSerializer<RouterStateUrl> {
  serialize(routerState: RouterStateSnapshot): RouterStateUrl {
    const { url } = routerState;
    const { queryParams } = routerState.root;
    let projectName: string;
    let clusterName: string;
    let state: ActivatedRouteSnapshot = routerState.root;
    while (state.firstChild) {
      if (!projectName && !!state.params['project']) {
        projectName = state.params['project'];
        clusterName = state.params['cluster'];
      }
      state = state.firstChild;
    }
    const { params } = state;

    return { url, queryParams, params, projectName, clusterName };
  }
}
