import { Namespace } from 'app/services/api/namespace.service';
import * as namespaceActions from '../actions/namespaces.actions';

export interface NamespaceEntities {
  [name: string]: Namespace;
}

export interface ProjectNamespaceMappings {
  [project: string]: {
    entities: NamespaceEntities;
  };
}

export interface ClusterProjectMappings {
  [cluster: string]: ProjectNamespaceMappings;
}

export interface NamespaceState {
  mappings: ClusterProjectMappings;
  loading: boolean;
}

const initialState: NamespaceState = {
  mappings: {},
  loading: false,
};

function getNamespacesEntities(namespaces: Namespace[]): NamespaceEntities {
  return namespaces.reduce(
    (entities: { [name: string]: Namespace }, namespace: Namespace) => {
      return {
        ...entities,
        [namespace.kubernetes.metadata.name]: namespace,
      };
    },
    {},
  );
}

export function reducer(
  state: NamespaceState = initialState,
  action: namespaceActions.NamespaceAction,
) {
  switch (action.type) {
    case namespaceActions.GET_CURRENT_NAMESPACES:
      return {
        ...state,
        mappings: {
          ...state.mappings,
        },
      };
    case namespaceActions.GET_NAMESPACES_BATCH_BY_PROJECT: {
      return {
        ...state,
        loading: true,
      };
    }
    /**
      payload: {
        project: string;
        data: {
          [cluster: string]: Namespace[];
        };
      }
     */
    case namespaceActions.GET_NAMESPACES_BATCH_BY_PROJECT_SUCC: {
      const result = (action as namespaceActions.GetNamespacesBatchByProjectSucc)
        .payload;
      const data = result.data;
      const project = result.project;
      const mappings = Object.keys(data).reduce(
        (_mappings: ClusterProjectMappings, cluster: string) => {
          const entities = getNamespacesEntities(data[cluster]);
          _mappings[cluster] = {
            ..._mappings[cluster],
            [project]: {
              entities,
            },
          };
          return _mappings;
        },
        { ...state.mappings },
      );
      return {
        ...state,
        mappings,
        loading: false,
      };
    }
    case namespaceActions.GET_NAMESPACES_BATCH_BY_PROJECT_FAIL: {
      return {
        ...state,
        loading: false,
      };
    }

    case namespaceActions.GET_NAMESPACES_BY_CLUSTER_AND_PROJECT: {
      return {
        ...state,
        loading: true,
      };
    }

    /**
      payload: {
        project: string;
        cluster: string;
        namespaces: Namespace[];
      }
     */
    case namespaceActions.GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_SUCC: {
      const {
        project,
        cluster,
        namespaces,
      } = (action as namespaceActions.GetNamespacesByClusterAndProjectSucc).payload;
      const mappings = {
        ...state.mappings,
        [cluster]: {
          ...state.mappings[cluster],
          [project]: {
            entities: getNamespacesEntities(namespaces),
          },
        },
      };
      return {
        ...state,
        mappings,
        loading: false,
      };
    }

    case namespaceActions.GET_NAMESPACES_BY_CLUSTER_AND_PROJECT_FAIL: {
      return {
        ...state,
        loading: false,
      };
    }
  }
  return state;
}

export const getClusterProjectMappings = (state: NamespaceState) =>
  state.mappings;
export const getNamespaceLoading = (state: NamespaceState) => state.loading;
