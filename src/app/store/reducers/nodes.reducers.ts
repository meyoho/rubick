import { ClusterNode } from 'app/services/api/region.service';
import * as fromNode from '../actions/node.actions';

interface Entities {
  [ip: string]: ClusterNode;
}

export interface NodeState {
  [regionName: string]: {
    entities: Entities;
    ips: string[];
  };
}

const initialState: NodeState = {};

function getPrivateIp(node: ClusterNode) {
  return node.status.addresses.find(addr => addr.type === 'InternalIP').address;
}

function nodes2State(
  state: NodeState,
  region: string,
  nodes: Array<ClusterNode>,
): NodeState {
  const ips = nodes.map(node => getPrivateIp(node));
  const entities: Entities = {};
  nodes.forEach(node => {
    entities[getPrivateIp(node)] = node;
  });
  return {
    ...state,
    [region]: {
      ips,
      entities,
    },
  };
}

export function reducer(
  state: NodeState = initialState,
  action: fromNode.NodeAction,
) {
  switch (action.type) {
    case fromNode.GET_NODES_BY_REGION_SUCC:
      const {
        regionName,
        nodes,
      } = (action as fromNode.GetNodesByRegionSucc).payload;
      return nodes2State(state, regionName, nodes);
  }
  return state;
}
