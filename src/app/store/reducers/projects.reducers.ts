import { Project } from 'app/services/api/project.service';
import * as fromProject from '../actions/projects.actions';

export interface ProjectState {
  entities: { [name: string]: Project };
  entitiesDetail: { [name: string]: Project };
  loaded: boolean;
  loading: boolean;
}

export const initialState: ProjectState = {
  entities: {},
  entitiesDetail: {},
  loaded: false,
  loading: false,
};

export function reducer(
  state = initialState,
  action: fromProject.ProjectAction,
) {
  switch (action.type) {
    case fromProject.LOAD_PROJECTS: {
      return {
        ...state,
        loading: true,
      };
    }
    case fromProject.LOAD_PROJECTS_SUCCESS: {
      const projects = action.payload;
      const entities = projects.reduce(
        (entities: { [name: string]: Project }, project: Project) => {
          return {
            ...entities,
            [project.name]: project,
          };
        },
        {
          // ...state.entities, // TODO: 合并project reducer 到root模块后，增加deleteProject相关的effect，这里可以恢复。
        },
      );
      return {
        ...state,
        loading: false,
        loaded: true,
        entities,
      };
    }
    case fromProject.LOAD_PROJECTS_FAIL: {
      return {
        ...state,
        loading: false,
        loaded: false,
      };
    }
    case fromProject.LOAD_PROJECT_DETAIL_SUCCESS: {
      const project = action.payload;
      const entitiesDetail = {
        ...state.entitiesDetail,
        [project.name]: project,
      };
      return {
        ...state,
        entitiesDetail,
      };
    }
    case fromProject.CREATE_PROJECT_SUCCESS: {
      const project = action.payload;
      const entities = {
        ...state.entities,
        [project.name]: project,
      };
      return {
        ...state,
        entities,
      };
    }

    case fromProject.DELETE_PROJECT_SUCCESS: {
      const projectName = action.payload;
      const { [projectName]: removed1, ...entities } = state.entities;
      const {
        [projectName]: removed2,
        ...entitiesDetail
      } = state.entitiesDetail;
      return {
        ...state,
        entities,
        entitiesDetail,
      };
    }
  }

  return state;
}

export const getProjectEntities = (state: ProjectState) => state.entities;
export const getProjectEntitiesDetail = (state: ProjectState) =>
  state.entitiesDetail;
export const getProjectLoading = (state: ProjectState) => state.loading;
export const getProjectLoaded = (state: ProjectState) => state.loaded;
