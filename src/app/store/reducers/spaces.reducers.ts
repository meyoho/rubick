import { Space } from 'app/services/api/space.service';
import * as spaceActions from '../actions/spaces.actions';

export interface ProjectSpaceMapping {
  [project: string]: Space[];
}

export interface SpaceState {
  mappings: ProjectSpaceMapping;
  loading: boolean;
  currentSpaceUuid: string;
}

const initialState: SpaceState = {
  mappings: {},
  loading: false,
  currentSpaceUuid: null,
};

export function reducer(
  state: SpaceState = initialState,
  action: spaceActions.SpaceAction,
) {
  switch (action.type) {
    case spaceActions.SET_CURRENT_SPACE:
      const uuid = (action as spaceActions.SetCurrentSpace).payload;
      return {
        ...state,
        currentSpaceUuid: uuid,
      };
    case spaceActions.GET_SPACES_BY_PROJECT:
      return {
        ...state,
        loading: true,
      };
    case spaceActions.GET_SPACES_BY_PROJECT_FAIL:
      return {
        ...state,
        loading: false,
      };
    case spaceActions.GET_SPACES_BY_PROJECT_SUCC: {
      const {
        project_name,
        spaces,
      } = (action as spaceActions.GetSpacesByProjectSucc).payload;
      return {
        ...state,
        mappings: {
          ...state.mappings,
          [project_name]: spaces,
        },
      };
    }
  }
  return state;
}

export const getProjectSpacesMapping = (state: SpaceState) => state.mappings;
export const getSpacesLoading = (state: SpaceState) => state.loading;
export const getCurrentSpaceUuid = (state: SpaceState) =>
  state.currentSpaceUuid;
