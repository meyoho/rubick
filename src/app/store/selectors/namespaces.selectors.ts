import { createSelector } from '@ngrx/store';

import { get } from 'lodash-es';

import { Namespace } from 'app/services/api/namespace.service';
import * as fromFeature from '../reducers';
import * as fromNamespace from '../reducers/namespaces.reducers';

interface NamespaceEntitiy {
  cluster: string;
  project: string;
  namespace: Namespace;
}

export const getClusterProjectMappings = createSelector(
  fromFeature.getNamespacesState,
  fromNamespace.getClusterProjectMappings,
);

export const getNamespaces = createSelector(
  getClusterProjectMappings,
  (mappings: fromNamespace.ClusterProjectMappings) => {
    return Object.keys(mappings).reduce((list, cluster: string) => {
      const clusterProjectMappings = mappings[cluster];
      const projectNamespacesList = Object.keys(clusterProjectMappings).reduce(
        (_list, project: string) => {
          return [
            ..._list,
            ...Object.values(clusterProjectMappings[project].entities).map(
              namespace => {
                return { namespace, project, cluster };
              },
            ),
          ];
        },
        [],
      );
      return [...list, ...projectNamespacesList];
    }, []) as NamespaceEntitiy[];
  },
);

export const getNamespacesByClusterAndProject = () => {
  return createSelector(
    getClusterProjectMappings,
    (
      mappings: fromNamespace.ClusterProjectMappings,
      props: { cluster: string; project: string },
    ) => {
      const entities = get(mappings, `${props.cluster}.${props.project}`, {});
      return Object.values(entities);
    },
  );
};

export const getNamespacesMappingLoading = createSelector(
  fromFeature.getNamespacesState,
  fromNamespace.getNamespaceLoading,
);
