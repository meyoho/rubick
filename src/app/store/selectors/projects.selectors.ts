import { createSelector } from '@ngrx/store';

import { get } from 'lodash-es';

import { Project } from 'app/services/api/project.service';
import * as fromFeature from '../reducers';
import * as fromProject from '../reducers/projects.reducers';

export const getProjectsEntities = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectEntities,
);

export const getProjectsEntitiesDetail = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectEntitiesDetail,
);

export const getAllProjects = createSelector(
  getProjectsEntities,
  entities => {
    return Object.keys(entities).map(name => entities[name]);
  },
);

export const getAllProjectsDetail = createSelector(
  getProjectsEntitiesDetail,
  entitiesDetail => {
    return Object.keys(entitiesDetail).map(name => entitiesDetail[name]);
  },
);

export const getCurrentProject = createSelector(
  getProjectsEntities,
  fromFeature.getRouterState,
  (entities, router): Project => {
    const projectName = router
      ? get(
          router,
          'state.projectName',
          window.sessionStorage.getItem('project'),
        )
      : window.sessionStorage.getItem('project');
    return entities[projectName];
  },
);

export const getCurrentProjectDetail = createSelector(
  getProjectsEntitiesDetail,
  fromFeature.getRouterState,
  (entitiesDetail, router): Project => {
    const projectName = router
      ? get(
          router,
          'state.projectName',
          window.sessionStorage.getItem('project'),
        )
      : window.sessionStorage.getItem('project');
    return entitiesDetail[projectName];
  },
);

export const getProjectsLoading = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectLoading,
);
export const getProjectsLoaded = createSelector(
  fromFeature.getProjectsState,
  fromProject.getProjectLoaded,
);
