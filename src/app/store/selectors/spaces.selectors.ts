import { createSelector } from '@ngrx/store';

import { flatten } from 'lodash-es';

import { Space } from 'app/services/api/space.service';
import * as fromFeature from '../reducers';
import * as fromSpace from '../reducers/spaces.reducers';

export const getProjectSpacesMapping = createSelector(
  fromFeature.getSpacesState,
  fromSpace.getProjectSpacesMapping,
);

export const getCurrentSpaceUuid = createSelector(
  fromFeature.getSpacesState,
  fromSpace.getCurrentSpaceUuid,
);

export const getSpacesByProjectName = (projectName: string) => {
  return createSelector(
    getProjectSpacesMapping,
    (mapping): Space[] => {
      return mapping[projectName];
    },
  );
};

export const getSpaces = createSelector(
  getProjectSpacesMapping,
  mappings => {
    return flatten(Object.values(mappings));
  },
);

export const getCurrentSpace = createSelector(
  getSpaces,
  getCurrentSpaceUuid,
  (spaces, uuid) => {
    if (!uuid) {
      uuid = window.sessionStorage.getItem('space');
    }
    return spaces.find(space => space.uuid === uuid);
  },
);

export const getSpacesLoading = createSelector(
  fromFeature.getSpacesState,
  fromSpace.getSpacesLoading,
);
