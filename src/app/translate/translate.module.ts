import { NgModule } from '@angular/core';
import {
  TranslateLoader,
  TranslateModule as NgxTranslateModule,
} from '@ngx-translate/core';

import { Observable, of } from 'rxjs';

import { getGlobal } from 'app/app-global';
import { TRANSLATIONS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';

/**
 * Translate module using ngx-translate.
 *
 */
export class CoreTranslateLoader implements TranslateLoader {
  getTranslation(): Observable<any> {
    return of(getGlobal(TRANSLATIONS));
  }
}
/**
 * Wraps translate module for ease of use. This is to be added to shared module.
 */
@NgModule({
  exports: [NgxTranslateModule],
})
export class TranslateModule {}

@NgModule({
  imports: [
    NgxTranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useClass: CoreTranslateLoader,
      },
    }),
  ],
  providers: [TranslateService],
  exports: [NgxTranslateModule],
})
export class GlobalTranslateModule {}
