import { KubernetesResource } from 'app/typings/raw-k8s';
import {
  Application,
  DaemonSet,
  Deployment,
  HorizontalPodAutoscaler,
  Service,
  StatefulSet,
} from 'app/typings/raw-k8s';

export type K8sComponent = PodController | HorizontalPodAutoscaler | Service;

export type PodController = Deployment | DaemonSet | StatefulSet;

export interface K8sResourceWithActions<
  T extends KubernetesResource = KubernetesResource
> {
  kubernetes: T;
  resource_actions: string[];
}

export interface K8sApplicationPayload {
  resource?: {
    name: string;
    namespace: string;
    displayName?: string;
    createMethod?: string; // UI/YAML
    owners?: {
      name: string;
      phone: string;
    }[];
  };
  crd?: Application;
  kubernetes: KubernetesResource[];
}

export interface Pagination<T> {
  count: number;
  page_size: number;
  num_pages: number;
  page: number;
  results: T[];
}

export interface TopologyResponse {
  nodes: {
    [key: string]: KubernetesResource;
  };
  edges: {
    type: string;
    from: string;
    to: string;
  }[];
}
