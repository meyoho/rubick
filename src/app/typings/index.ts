export * from './backend-api';
export * from './k8s-form-model';
export * from './raw-k8s';
export * from './status';
export * from './vendor-customer.types';
export * from './chart.types';

// Defines core types here

import { VendorCustomer } from './vendor-customer.types';

export interface WorkspaceBaseParams {
  project: string;
  cluster?: string;
  namespace?: string;
}

// type for ACCOUNT token
export declare interface Account {
  namespace: string;
  username: string;
}

/**
 * All weblabs used in Rubick
 */
export interface Weblabs {
  HUATAI_ENABLED?: boolean;
  FEDERATION_ENABLED?: boolean;
  DEVOPS_INTEGRATION?: boolean;
  JENKINS_PIPELINE_ENABLED?: boolean;
  GROUP_ENABLED?: boolean;

  // K8S Roles and related features, #DEV-9065
  K8S_AUTH_ENABLED?: boolean;
}

export interface I18nManifest {
  en: { [key: string]: string };
  zh_cn: { [key: string]: string };
}

export interface Environments {
  debug?: boolean;
  alauda_image_index?: string;
  is_private_deploy_enabled?: boolean;
  cluster_create_cloud_type?: string;
  user_docs_url?: string;
  api_market_url?: string;
  predefined_login_organization?: string;
  overridden_logo_sources?: string;
  vendor_customer?: VendorCustomer;
  outsourcing_image_index?: string;
  default_quota?: string;
  label_base_domain: string;
  auto_logout_latency: number;
  browser_download_url?: string;
  background_image: string;
  staged_namespace_create?: boolean;
  ajax_request_timeout?: number;
  landing_bg_url?: string;
  embedded_mode?: boolean;
  docker_versions?: string;
  log_ttl?: string;
  event_ttl?: string;
}

export type Weblab = keyof Weblabs;

export type Environment = keyof Environments;

/**
 * Console related status.
 *
 * All feature related status should be mapped into one of the categories
 */
export enum UnifiedStatus {
  SUCCESS = 'SUCCESS',
  ERROR = 'ERROR',
  PENDING = 'PENDING',
  IN_PROGRESS = 'IN_PROGRESS',
  INACTIVE = 'INACTIVE',
}

/**
 * Resource types
 */
export type ResourceType =
  | 'alarm'
  | 'alert_template'
  | 'application'
  | 'application_template'
  | 'audit'
  | 'build_config'
  | 'build_history'
  | 'cloud_account'
  | 'cloud_instance'
  | 'cluster'
  | 'cluster_node'
  | 'namespace'
  | 'configuration_file'
  | 'dashboard'
  | 'dashboard_panel'
  | 'env_file'
  | 'events'
  | 'k8s_networkpolicies'
  | 'ip'
  | 'subnet'
  | 'load_balancer'
  | 'notification'
  | 'organization'
  | 'pipeline_config'
  | 'pipeline_history'
  | 'pipeline_task'
  | 'registry'
  | 'registry_project'
  | 'repository'
  | 'role'
  | 'service'
  | 'space'
  | 'subaccount'
  | 'sync_config'
  | 'sync_history'
  | 'project'
  | 'project_template'
  | 'private_ip'
  | 'certificate'
  | 'job_config'
  | 'log_filter'
  | 'log_alarm'
  | 'integration'
  | 'configmap'
  | 'jenkins_pipeline'
  | 'jenkins_pipeline_history'
  | 'jenkins_pipeline_template'
  | 'jenkins_pipeline_template_repository'
  | 'jenkins_credential'
  | 'helm_template'
  | 'helm_template_repo'
  | 'public_helm_template'
  | 'public_helm_template_repo'
  | 'persistentvolume'
  | 'persistentvolumeclaim'
  | 'role_template'
  | 'pipeline_template'
  | 'k8s_storageclasses'
  | 'operation_view'
  | 'event'
  | 'k8s_others'
  | 'k8s_resourcequotas'
  | 'domain'
  | 'secret'
  | 'devops_secret'
  | 'devops_tool'
  | 'devops_tool_projects'
  | 'devops_toolbindingreplica'
  | 'devops_pipeline_template'
  | 'devops_pipelineconfig';
