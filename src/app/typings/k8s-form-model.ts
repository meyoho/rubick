import { PodController } from 'app/typings/backend-api';
import {
  DaemonSetSpec,
  DeploymentSpec,
  IngressBackend,
  IngressRule,
  JobSpec,
  LabelSelector,
  PodAffinityTerm,
  PodSpecAffinity,
  Probe,
  StatefulSetSpec,
  StringMap,
  WeightedPodAffinityTerm,
} from 'app/typings/raw-k8s';
import {
  Application,
  Container,
  DaemonSetUpdateStrategy,
  DeploymentStrategy,
  HorizontalPodAutoscaler,
  KubernetesResource,
  Service,
  StatefulSetUpdateStrategy,
} from 'app/typings/raw-k8s';

export const LOG_PATH_KEY = '__FILE_LOG_PATH__';
export const EXCLUDE_LOG_PATH_KEY = '__EXCLUDE_LOG_PATH__';

export declare interface RcImageSelection {
  is_public_registry?: boolean;
  registry_name?: string;
  registry_endpoint?: string;
  project_name?: string;
  repository_name?: string;
  full_image_name?: string;
  tag?: string;
  is_third?: boolean;
}

export interface ApplicationFormModel {
  metadata?: ApplicationMetaDataFormModel;
  crd?: Application; //  TODO: remove after when PUT API updates
  clusters?: string[];
  components?: ComponentFormModel[];
  services?: Service[];
  resources?: KubernetesResource[];
}

export interface ApplicationMetaDataFormModel {
  name?: string;
  displayName?: string;
  namespace?: string;
}

export interface ComponentFormModel {
  controller?: PodController;
  autoscaler?: HorizontalPodAutoscaler;
}

export interface PodControllerFormModel extends KubernetesResource {
  suffixName?: string; // UI 输入的后半部分名字
  spec?: DeploymentSpec | StatefulSetSpec | DaemonSetSpec;
  network?: PodNetworkFormModel;
}

export interface PodSpecFormModel {
  containers?: ContainerFormModel[];
  hostNetwork?: boolean;
  nodeSelector?: StringMap;
  affinity?: PodSpecAffinity;
}

export interface PodNetworkFormModel {
  // 子网的annotation需要设置 PodTemplateSpec.metadata.annotation
  //  subnet.alauda.io/ipAddrs: "192.168.10.100,192.168.10.111" # 可选，当需要指定 ip 时，填入所期望的 ip，不填则自动分配
  //  subnet.alauda.io/name: <subnet_name>  # macvlan/ipvlan 情况下使用的子网名
  hostNetwork?: boolean;
  type?: PodNetworkTypeEnum;
  subnetName?: string;
  subnetIpAddrs?: string[]; // 暂不支持指定IP地址
  cidrBlocks?: string[]; // calico annotation 需要用到: https://docs.projectcalico.org/v3.1/reference/cni-plugin/configuration
}

export interface ContainerFormModel extends Container {
  volumes?: VolumeFormModel[];
  probes?: {
    livenessProbe?: Probe;
    readinessProbe?: Probe;
  };
  logPaths?: string[];
  excludeLogPaths?: string[];
}

export interface PodAffinityFormModel {
  type: string; // required, preferred
  weight: number;
  topologyKey: string;
  labelSelector: LabelSelector;
}

export type AffinityTerm = WeightedPodAffinityTerm | PodAffinityTerm;

export const PodAffinityType = {
  required: 'requiredDuringSchedulingIgnoredDuringExecution',
  preferred: 'preferredDuringSchedulingIgnoredDuringExecution',
};

export interface ResourceRequirementsFormModel {
  limits?: {
    cpu?:
      | {
          value: string;
          unit: string;
        }
      | string;
    memory?: {
      value: string;
      unit: string;
    };
  };
  requests?: {
    cpu?: {
      value: string;
      unit: string;
    };
    memory?: {
      value: string;
      unit: string;
    };
  };
}

export interface VolumeFormModel {
  type?: string;
  name?: string;
  hostPath?: {
    path: string;
    mountPath: string;
  };
  persistentVolumeClaim?: {
    claimName: string;
    mountPath: string;
    subPath: string;
  };
  configMap?: {
    name: string;
    mountPath?: string;
    subPath?: string;
    hasRefKeys?: boolean; // 不同于volumes->configMap->items的写法，这里configMap不包含items，而是在volumeMounts中定义每个挂载的subPath，subPath的值对应configMap的key
    refKeys?: {
      key: string; // volumeMount中的subPath
      path: string; // volumeMount中的mountPath
    }[];
  };
  // 其他UI 暂不支持的volumes定义
  other?: {
    yaml: string;
    mountPath?: string;
    subPath?: string;
  };
}

export type StrategyFormModel =
  | DeploymentStrategy
  | DaemonSetUpdateStrategy
  | StatefulSetUpdateStrategy;

export interface ServiceFormModel extends Service {
  ports?: ServicePortFormModel[]; // formModel 中的ports，除了Service的ports配置外还附带alb信息
  type?: ServiceTypeEnum;
  podControllerName?: string;
}

export interface ServicePortFormModel {
  port: number | '';
  targetPort?: number | string;
  albName?: string;
  albPort?: number;
  albProtocol?: 'tcp' | 'http';
}

export enum ServiceTypeEnum {
  ClusterIP = 'ClusterIP',
  Headless = 'Headless',
  NodePort = 'NodePort',
}

export const ServiceTypeDisplay = {
  ClusterIP: 'HTTP (ClusterIP)',
  NodePort: 'TCP (NodePort)',
  Headless: 'Headless',
};

export enum PodNetworkTypeEnum {
  host = 'Host',
  cluster = 'Cluster',
}

/**
 * @deprecated
 * TODO: 用 VolumeFormModel 替代相关实现
 */
export interface ContainerVolumeViewModel {
  _uniqueName?: string;
  index?: number;
  name?: string;
  type?: 'host-path' | 'volume' | 'pvc' | 'configmap';
  containerPath: string;
  subPath?: string;
  // host-path
  hostPath?: string;
  // pvc
  pvcName?: string;
  // volume
  volumeName?: string;
  driverName?: string;
  driverVolumeId?: string;
  // configmap
  configMapName?: string;
  configMapUuid?: string;
  configMapKeyRef?: boolean;
  configMapKeyMap?: {
    index: number;
    key: string;
    path: string;
  }[];
}

export interface LoadBalancerFormModel {
  loadbalancerName: string;
  address: string;
  replicas: number;
  nodeSelector: StringMap;
}
export interface RuleIndicator {
  type: string; // RULE_TYPE
  values: string[][];
  key: string;
}

export interface IngressRuleFormModel extends IngressRule {
  secretName?: string;
}
export interface IngressSpecFormModel {
  backend?: IngressBackend;
  rules?: IngressRuleFormModel[];
}
export interface IngressFormModel extends KubernetesResource {
  spec?: IngressSpecFormModel;
}

export interface JobSpecFormModel extends JobSpec {
  jobType?: string;
  startingDeadlineSeconds?: number | '';
}
