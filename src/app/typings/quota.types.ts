export interface Quota {
  cpu?: number;
  memory?: number;
  pvc_num?: number;
  pods?: number;
  storage?: number;
}

export enum QuotaUnit {
  pvc_num = 'unit_ge',
  storage = 'unit_gb',
  memory = 'unit_gb',
  pods = 'unit_ge',
  cpu = 'unit_core',
}
