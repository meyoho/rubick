export interface StringMap {
  [key: string]: string;
}

// TypeMeta describes an individual object in an API response or request
// with strings representing the type of the object and its API schema version.
// Structures that are versioned or persisted should inline TypeMeta.
export interface TypeMeta {
  kind?: string;
  apiVersion?: string;
}

export interface OwnerReference {
  apiVersion: string;
  kind: string;
  name: string;
  uid: string;
  controller?: boolean;
  blockOwnerDeletion?: boolean;
}

export interface CrossVersionObjectReference {
  apiVersion: string;
  kind: string;
  name: string;
}

// ObjectMeta is metadata that all persisted resources must have, which includes all objects
export interface ObjectMeta {
  name?: string;
  namespace?: string;
  labels?: StringMap;
  annotations?: StringMap;
  selfLink?: string;
  uid?: string;
  creationTimestamp?: string;
  ownerReferences?: OwnerReference[];
  resourceVersion?: string;
}

export interface KubernetesResource extends TypeMeta {
  metadata?: ObjectMeta;
  status?: any;
  spec?: any;
}

export interface List extends TypeMeta {
  items?: KubernetesResource[];
}

export interface LabelSelector {
  matchLabels?: StringMap;
  matchExpressions?: LabelSelectorRequirement[];
}

export interface LabelSelectorRequirement {
  key: string;
  operator: string;
  values: string[];
}

export interface HorizontalPodAutoscaler extends KubernetesResource {
  spec: HorizontalPodAutoscalerSpec;
  status?: any;
}

export interface HorizontalPodAutoscalerSpec {
  maxReplicas?: number;
  minReplicas?: number;
  scaleTargetRef?: CrossVersionObjectReference;
}

/**
 * Workload API
 */
export interface Deployment extends KubernetesResource {
  spec?: DeploymentSpec;
  status?: any;
}

// DeploymentSpec is the specification of the desired behavior of the Deployment.
export interface DeploymentSpec {
  replicas?: number;
  selector?: LabelSelector;
  template?: PodTemplateSpec;
  strategy?: DeploymentStrategy;
  minReadySeconds?: number;
  revisionHistoryLimit?: number;
}

export interface DaemonSet extends KubernetesResource {
  spec?: DaemonSetSpec;
  status?: any;
}

export interface DaemonSetSpec {
  selector?: LabelSelector;
  template?: PodTemplateSpec;
  updateStrategy?: DaemonSetUpdateStrategy;
  minReadySeconds?: number;
  revisionHistoryLimit?: number;
}

export interface StatefulSet extends KubernetesResource {
  spec?: StatefulSetSpec;
  status?: any;
}

export interface StatefulSetSpec {
  replicas?: number;
  selector?: LabelSelector;
  template?: PodTemplateSpec;

  // serviceName is the name of the service that governs this StatefulSet.
  // This service must exist before the StatefulSet, and is responsible for
  // the network identity of the set. Pods get DNS/hostnames that follow the
  // pattern: pod-specific-string.serviceName.default.svc.cluster.local
  // where "pod-specific-string" is managed by the StatefulSet controller.
  serviceName?: string;
  updateStrategy?: StatefulSetUpdateStrategy;
  revisionHistoryLimit?: number;
}

export type DeploymentStrategyType = 'RollingUpdate' | 'Recreate';
export type DaemonSetUpdateStrategyType = 'RollingUpdate' | 'OnDelete';
export type StatefulSetUpdateStrategyType = 'RollingUpdate' | 'OnDelete';

export interface DeploymentStrategy {
  type?: DeploymentStrategyType;
  rollingUpdate?: RollingUpdateDeployment;
}

export interface DaemonSetUpdateStrategy {
  type?: DaemonSetUpdateStrategyType;
  rollingUpdate?: RollingUpdateDaemonSet;
}

export interface StatefulSetUpdateStrategy {
  type?: StatefulSetUpdateStrategyType;
  rollingUpdate?: RollingUpdateStatefulSetStrategy;
}

export interface RollingUpdateDeployment {
  maxUnavailable?: string | number;
  maxSurge?: string | number;
}

export interface RollingUpdateDaemonSet {
  maxUnavailable?: string | number;
}

// Partition indicates the ordinal at which the StatefulSet should be
// partitioned.
// Default value is 0.
export interface RollingUpdateStatefulSetStrategy {
  partition?: string | number;
}

// TODO: fill this in if required
// tslint:disable-next-line:no-empty-interface
export interface DeploymentStatus {}

export interface PodTemplateSpec {
  metadata?: ObjectMeta;
  spec?: PodSpec;
}

export interface PodSpec {
  containers: Container[];
  volumes?: Volume[];
  nodeSelector?: StringMap;
  hostNetwork?: boolean;
  affinity?: PodSpecAffinity;
  restartPolicy?: string;
}

export interface PodSpecAffinity {
  podAffinity?: Affinity;
  podAntiAffinity?: Affinity;
  nodeAffinity?: Affinity;
}

export interface Affinity {
  requiredDuringSchedulingIgnoredDuringExecution?: PodAffinityTerm[];
  preferredDuringSchedulingIgnoredDuringExecution?: WeightedPodAffinityTerm[];
}

export interface WeightedPodAffinityTerm {
  podAffinityTerm: PodAffinityTerm;
  weight: number;
}

export interface PodAffinityTerm {
  labelSelector: LabelSelector;
  topologyKey: string;
  namespaces?: string[];
}

export interface Pod extends KubernetesResource {
  spec?: PodSpec;
  status?: PodStatus;
}

export interface PodStatus {
  conditions?: PodCondition[];
  containerStatuses?: ContainerStatus[];
  hostIP?: string;
  initContainerStatuses?: ContainerStatus[];
  message?: string;
  nominatedNodeName?: string;
  phase?: string;
  podIP?: string;
  qosClass?: string;
  reason?: string;
  startTime?: string;
}

export interface PodCondition {
  lastProbeTime?: string;
  lastTransitionTime?: string;
  message?: string;
  reason?: string;
  status?: string;
  type?: string;
}

export interface ContainerStatus {
  containerID?: string;
  image?: string;
  imageID?: string;
  lastState?: ContainerState;
  name?: string;
  ready?: boolean;
  restartCount?: number;
  state?: ContainerState;
}

export interface ContainerState {
  running?: ContainerStateRunning;
  terminated?: ContainerStateTerminated;
  waiting?: ContainerStateWaiting;
}

export interface ContainerStateRunning {
  startedAt?: string;
}

export interface ContainerStateTerminated {
  containerID?: string;
  exitCode?: number;
  finishedAt?: string;
  message?: string;
  reason?: string;
  signal?: number;
  startedAt?: string;
}

export interface ContainerStateWaiting {
  message?: string;
  reason?: string;
}

export interface Volume {
  name: string;
  configMap?: {
    name: string;
    items?: string;
  };
  secret?: {
    secretName: string;
    items?: string;
  };
  persistentVolumeClaim?: {
    claimName: string;
  };
  hostPath?: {
    path: string;
  };
  // other volumes
}

/**
 * Config And Storage API
 */

export interface PersistentVolumeClaim extends KubernetesResource {
  apiVersion: string;
  kind: string;
  spec: PersistentVolumeClaimSpec;
  status?: {
    phase: string;
  };
}

export interface PersistentVolumeClaimSpecMeta {
  storageClassName?: string;
  volumeName?: string;
  resources: {
    requests: { storage: string };
  };
  selector?: {
    matchLabels: StringMap;
  };
}

export interface PersistentVolumeClaimSpec
  extends PersistentVolumeClaimSpecMeta {
  accessModes: string[];
}

export interface PersistentVolume extends KubernetesResource {
  apiVersion: string;
  kind: string;
  spec: PersistentVolumeSpec;
  status?: {
    phase: string;
  };
}

export interface PersistentVolumeSpecMeta {
  capacity: {
    storage: string;
  };
  claimRef?: {
    apiVersion: string;
    kind: 'PersistentVolumeClaim';
    name: string;
    namespace: string;
    uid: string;
  };
  persistentVolumeReclaimPolicy: string; //'Recycle' | 'Retain' | 'Delete';
  hostPath?: {
    path: string;
  };
  nfs?: {
    server: string;
    path: string;
  };
}

export interface PersistentVolumeSpec extends PersistentVolumeSpecMeta {
  accessModes: string[]; //['ReadWriteOnce' | 'ReadOnlyMany' | 'ReadWriteMany'];
}

export interface VolumeMount {
  name: string;
  mountPath?: string;
  readOnly?: boolean;
  subPath?: string;
  mountPropagation?: string;
}

// TODO: fill this in if required
// tslint:disable-next-line:no-empty-interface
export interface VolumeSource {}

export interface KeyToPath {
  key?: string;
  path?: string;
}

export interface ResourceRequirements {
  limits?: {
    cpu?: string;
    memory?: string;
  };
  requests?: {
    cpu?: string;
    memory?: string;
  };
}

export interface Container {
  name?: string;
  image?: string;
  command?: string[];
  args?: string[];
  env?: EnvVar[];
  envFrom?: EnvFromSource[];
  workingDir?: string;
  ports?: ContainerPort[];
  resources?: ResourceRequirements;
  volumeMounts?: any;
  livenessProbe?: Probe;
  readinessProbe?: Probe;
}

export interface ContainerPort {
  name?: string;
  hostPort?: number;
  containerPort?: number;
  protocol?: string;
  hostIP?: string;
}

export interface EnvVarSource {
  configMapKeyRef?: ConfigMapKeyRef;
  secretKeyRef?: SecretKeyRef;
  fieldRef?: FieldRef;
}

export interface EnvVar {
  name: string;
  value?: string;
  valueFrom?: EnvVarSource;
}

export interface EnvFromSource {
  prefix?: string;
  configMapRef?: ConfigMapRef;
  secretRef?: SecretRef;
}

export interface Probe {
  exec?: ExecAction;
  failureThreshold?: number;
  httpGet?: HTTPGetAction;
  initialDelaySeconds?: number;
  periodSeconds?: number;
  successThreshold?: number;
  tcpSocket?: TCPSocketAction;
  timeoutSeconds?: number;
}

export interface ExecAction {
  command?: string[];
}

export interface HTTPGetAction {
  host?: string;
  httpHeaders?: HTTPHeader[];
  path: string;
  port?: number | string;
  scheme?: string;
}

export interface HTTPHeader {
  name?: string;
  value?: string;
}

export interface TCPSocketAction {
  host?: string;
  port?: number | string;
}

// tslint:disable-next-line:no-empty-interface
export interface Namespace extends KubernetesResource {
  status?: NamespaceStatus;
}

export interface NamespaceStatus {
  phase?: string;
}

export interface LocalObjectReference {
  name: string;
}

export interface ConfigMapKeyRef extends LocalObjectReference {
  key: string;
  optional?: boolean;
}

export interface SecretKeyRef extends LocalObjectReference {
  key: string;
  optional?: boolean;
}

export interface FieldRef extends LocalObjectReference {
  fieldPath: string;
  apiVersion?: string;
}

export interface ConfigMapRef extends LocalObjectReference {
  optional?: boolean;
}

export interface SecretRef extends LocalObjectReference {
  optional?: boolean;
}

export interface IngressSpec {
  backend?: IngressBackend;
  rules?: IngressRule[];
  tls?: IngressTLS[];
}

export interface IngressRule {
  host: string;
  domain?: string;
  http: HTTPIngressRuleValue;
}

export interface HTTPIngressRuleValue {
  paths?: HTTPIngressPath[];
}

export interface HTTPIngressPath {
  path: string;
  backend: IngressBackend;
}

export interface IngressBackend {
  serviceName?: string;
  servicePort?: string | number;
}

export interface IngressTLS {
  hosts?: string[];
  secretName?: string;
}

export interface IngressStatus {
  loadBalancer?: LoadBalancerStatus;
}

export interface LoadBalancerStatus {
  ingress?: LoadBalancerIngress[];
}

export interface LoadBalancerIngress {
  ip: string;
  hostname: string;
}

export interface LimitRange extends KubernetesResource {
  spec?: LimitRangeSpec;
}

export type LimitType = 'Container' | 'Pod' | 'PersistentVolumeClaim';

export interface LimitRangeItem {
  type: LimitType;
  min?: StringMap;
  max?: StringMap;
  default?: StringMap;
  defaultRequest?: StringMap;
  maxLimitRequestRatio?: StringMap;
}

export interface LimitRangeSpec extends KubernetesResource {
  limits: LimitRangeItem[];
}

export type ResourceQuotaScope =
  | 'Terminating'
  | 'NotTerminating'
  | 'BestEffort'
  | 'NotBestEffort';

export interface ResourceQuota extends KubernetesResource {
  spec?: ResourceQuotaSpec;
}

export interface ResourceQuotaSpec {
  hard?: StringMap;
  scopes?: ResourceQuotaScope[];
}

export enum PodStatusEnum {
  Failed = 'Failed',
  Succeeded = 'Succeeded',
  Running = 'Running',
  Pending = 'Pending',
  Completed = 'Completed',
  ContainerCreating = 'ContainerCreating',
  PodInitializing = 'PodInitializing',
  Terminating = 'Terminating',
  Initing = 'Initing',
}

export interface Service extends KubernetesResource {
  spec?: ServiceSpec;
  status?: any;
}

export interface ServiceSpec {
  clusterIP?: string;
  externalIPs?: string[];
  externalName?: string;
  ports?: ServicePort[];
  selector?: StringMap;
  type?: string;
}

export interface ServicePort {
  name?: string;
  nodePort?: number;
  port: number | '';
  protocol: string;
  targetPort: number | string;
}

export interface ConfigMap extends KubernetesResource {
  data?: StringMap;
}

export interface Secret extends KubernetesResource {
  data?: StringMap;
}

export interface AlternateBackend {
  kind: string;
  name: string;
  weight: number;
}

export interface RouteSpec {
  host: string;
  path: string;
  port: {
    targetPort: string;
  };
  to: AlternateBackend;
  wildcardPolicy: string | 'None';
  alternateBackends?: AlternateBackend[];
}

export interface Route extends KubernetesResource {
  spec?: RouteSpec;
}

export interface Application extends KubernetesResource {
  spec: ApplicationSpec;
}

export interface ApplicationSpec {
  componentKinds: GroupKind[];
  assemblyPhase?: ApplicationStatusEnum;
  selector?: {
    [key: string]: string;
  };
  descriptor?: any;
}

export interface GroupKind {
  kind: string;
  group: string;
}

export const DeploymentTypeMeta: TypeMeta = {
  apiVersion: 'extensions/v1beta1',
  kind: 'Deployment',
};

export const DaemonSetTypeMeta: TypeMeta = {
  apiVersion: 'extensions/v1beta1',
  kind: 'DaemonSet',
};

export const StatefulSetTypeMeta: TypeMeta = {
  apiVersion: 'apps/v1beta1',
  kind: 'StatefulSet',
};

export const NamespaceTypeMeta: TypeMeta = {
  apiVersion: 'v1',
  kind: 'Namespace',
};

export const PodControllerTypeMeta: TypeMeta = {
  apiVersion: 'extensions/v1beta1',
};

export const LimitRangeTypeMeta: TypeMeta = {
  apiVersion: 'v1',
  kind: 'LimitRange',
};

export const ResourceQuotaTypeMeta: TypeMeta = {
  apiVersion: 'v1',
  kind: 'ResourceQuota',
};

export const ConfigMapTypeMeta: TypeMeta = {
  apiVersion: 'v1',
  kind: 'ConfigMap',
};

export const SecretTypeMeta: TypeMeta = {
  apiVersion: 'v1',
  kind: 'Secret',
};

export const ServiceTypeMeta: TypeMeta = {
  apiVersion: 'v1',
  kind: 'Service',
};

export const HorizontalPodAutoscalerMeta: TypeMeta = {
  apiVersion: 'autoscaling/v1',
  kind: 'HorizontalPodAutoscaler',
};

export const ListMeta: TypeMeta = {
  apiVersion: 'v1',
  kind: 'List',
};

export enum ApplicationStatusEnum {
  Failed = 'Failed',
  Succeeded = 'Succeeded',
  Pending = 'Pending',
  Running = 'Running',
  PartialRunning = 'PartialRunning',
  Stopped = 'Stopped',
  Empty = 'Empty',
}

export const ServiceSpecTypeMap = {
  ClusterIP: 'HTTP',
  NodePort: 'TCP',
};

export interface Ingress extends KubernetesResource {
  spec?: IngressSpec;
}

export interface LoadBalancerSpec {
  address: string;
  type: string;
  domains: string[];
}

export interface LoadBalancer extends KubernetesResource {
  spec?: LoadBalancerSpec;
}

export interface RuleService {
  name: string;
  namespace: string;
  port: number | '';
  weight: number;
}

export interface ServiceGroup {
  services: RuleService[];
  session_affinity_attribute: string;
  session_affinity_policy: string;
}

export interface FrontendSpec {
  port?: number | '';
  protocol?: string;
  serviceGroup?: ServiceGroup;
  certificate_name?: string;
}

// http://confluence.alaudatech.com/pages/viewpage.action?pageId=27177567
// 后端对于 Frontend 的资源定义
export interface Frontend extends KubernetesResource {
  spec?: FrontendSpec;
}

export interface RuleSpec {
  priority: number;
  domain: string;
  url: string;
  dsl: string;
  description: string;
  certificate_name?: string;
  source?: {
    name: string;
    namespace: string;
    type: string;
  };
  serviceGroup: ServiceGroup;
  rewrite_target?: string;
}

export interface Rule extends KubernetesResource {
  spec?: RuleSpec;
}

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#jobspec-v1-batch
export interface JobSpec {
  activeDeadlineSeconds?: number | '';
  backoffLimit?: number | '';
  completions?: number | '';
  manualSelector?: boolean;
  parallelism?: number | '';
  selector?: LabelSelector;
  template?: PodTemplateSpec;
}

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#jobtemplatespec-v1beta1-batch
export interface JobTemplateSpec {
  metadata?: ObjectMeta;
  spec?: JobSpec;
}

// https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#cronjobspec-v1beta1-batch
export interface CronJobSpec {
  concurrencyPolicy?: string;
  failedJobsHistoryLimit?: number | '';
  schedule?: string;
  startingDeadlineSeconds?: number | '';
  successfulJobsHistoryLimit?: number | '';
  suspend?: boolean;
  jobTemplate?: JobTemplateSpec;
}

export interface ObjectReference {
  apiVersion: string;
  fieldPath: string;
  kind: string;
  name: string;
  namespace: string;
  resourceVersion: string;
  uid: string;
}

export interface CronJobStatus {
  active?: ObjectReference[];
  lastScheduleTime: string;
}

export interface CronJob extends KubernetesResource {
  spec?: CronJobSpec;
  status?: CronJobStatus;
}

export interface JobCondition {
  lastProbeTime: string;
  lastTransitionTime: string;
  message: string;
  reason: string;
  status: string;
  type: string;
}

export interface JobStatus {
  active: number | '';
  completionTime: string;
  conditions: JobCondition[];
  startTime: string;
  succeeded: number;
  failed: number;
}

export interface Job extends KubernetesResource {
  spec?: JobSpec;
  status?: JobStatus;
}

export enum IPIP_MODE_TYPE {
  Never = 'Never',
  Always = 'Always',
  CrossSubnet = 'CrossSubnet',
}

export interface SubnetSpec {
  project_name: string;
  namesapce: string;
  subnet_name: string;
  create_at: string;
  update_at: string;
  create_by: string;
  cidr_block: string;
  gateway: string;
  ipip_mode?: IPIP_MODE_TYPE;
  nat_outgoing?: boolean;
}

export interface Subnet extends KubernetesResource {
  spec?: SubnetSpec;
}

export interface SubnetIpSpec {
  subnet_name: string;
  fresh: boolean;
  used: boolean;
  is_required: boolean;
  service_name: string;
  service_id: string;
}

export interface SubnetIp extends KubernetesResource {
  spec?: SubnetIpSpec;
}

export interface IPBlock {
  cidr: string;
  except: string[];
}

export interface NetworkPolicyPort {
  port: number;
  protocol: string;
}

export interface NetworkPolicyPeer {
  ipBlock?: IPBlock;
  namespaceSelector?: LabelSelector;
  podSelector?: LabelSelector;
}

export interface NetworkPolicyEgressRule {
  ports?: NetworkPolicyPort[];
  to?: NetworkPolicyPeer[];
}

export interface NetworkPolicyIngressRule {
  from?: NetworkPolicyPeer[];
  ports?: NetworkPolicyPort[];
}

export interface NetworkPolicySpec {
  egress?: NetworkPolicyEgressRule[];
  ingress?: NetworkPolicyIngressRule[];
  podSelector?: LabelSelector;
  policyTypes?: string[];
}

export interface NetworkPolicy extends KubernetesResource {
  spec?: NetworkPolicySpec;
}

export interface Secret extends KubernetesResource {
  data?: StringMap;
}
