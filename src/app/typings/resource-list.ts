// TODO: migrate from app_user/core/types

export interface ResourceResponse<T = any> {
  result?: T[];
  code?: number;
}
