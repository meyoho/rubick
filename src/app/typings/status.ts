export enum GenericStatus {
  Success = 'success',
  Succeeded = 'success',
  Running = 'running',
  Pending = 'pending',
  Failed = 'error',
  Error = 'error',
  Unknown = 'unknown',
  Bound = 'bound',
  Warning = 'warning',
  Terminating = 'terminating',
  Stopped = 'stopped',
  Suspended = 'suspended',
}

export const GenericStatusList = [
  'success',
  'running',
  'pending',
  'error',
  'unknown',
  'bound',
  'warning',
  'suspended',
  'terminating',
  'stopped',
];
