/**
 * Vendor customers
 */
export enum VendorCustomer {
  PLATFORM = 'PLATFORM',
  ZYRF = 'ZYRF',
}
