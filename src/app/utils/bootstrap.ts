import { template } from 'lodash-es';
import moment from 'moment';

import { getGlobal, setGlobal } from 'app/app-global';
import {
  ACCOUNT,
  ENVIRONMENTS,
  I18NMANIFEST,
  TRANSLATIONS,
  WEBLABS,
} from 'app/shared/tokens';
import { Environments, Weblabs } from 'app/typings';

const weblabsURL = '/ajax-sp/account/weblabs';
const environmentsURL = '/ajax-sp/global/environments';
const initI18nManifestUrl = '/static/i18n/manifest.json';
const customLocaleSuffix = 'custom.json';
const customAssetsDir = '/static/custom';
const HEADER_AJAX_REQUEST = 'RUBICK-AJAX-REQUEST';
const DEFAULT_TRANS_TEMPLATE = {
  project: {
    zh: '项目',
    en: 'Project',
  },
  namespace: {
    zh: '命名空间',
    en: 'Namespace',
  },
  space: {
    zh: '组',
    en: 'Space',
  },
};
enum TRANSLATION_API_KEYWORD {
  'zh_cn' = 'zh',
  'en' = 'en',
}
const defaultTranslationsMap = {},
  customTranslationsMap = {};

export const ajax = <T>(
  url: string,
  isJson = true,
  isRubickAjax = false,
): Promise<T> =>
  new Promise((resolve, reject) => {
    const request = new XMLHttpRequest();
    request.onreadystatechange = () => {
      if (request.readyState === XMLHttpRequest.DONE) {
        const response = request.responseText;
        if (request.status === 200) {
          if (isJson) {
            try {
              return resolve(JSON.parse(response));
            } catch (e) {}
          }
          resolve(response as any);
        } else {
          reject(response);
        }
      }
    };
    request.onerror = reject;
    request.open('GET', url);
    if (isRubickAjax) {
      request.setRequestHeader(HEADER_AJAX_REQUEST, 'true');
    }
    request.send();
  });

export function getLocale() {
  const locale =
    localStorage.getItem('alauda_locale') || navigator.language || 'en';
  // 注意这里没有用startsWith('zh'), 是考虑繁体中文用户可能会更倾向于用英语
  // 默认语言为en, 当用户浏览器语言为zh-cn或者zh时，改为使用zh_cn。
  return ['zh_cn', 'zh-cn', 'zh'].includes(locale.toLowerCase())
    ? 'zh_cn'
    : 'en';
}

const getText = (id: string) => document.getElementById(id).innerHTML.trim();

export function initAccount() {
  return {
    namespace: getText('cp-namespace'),
    username: getText('console-username'),
  };
}

export async function initTranslations(
  privateDeployEnabled: boolean,
  namespace?: string,
) {
  // 不同标准定义locale的字符串形式不同，
  // 为了统一成目前项目中使用 zh_cn 和 en 两种locale, 需要做一次简单的映射。
  const locale = getLocale();
  localStorage.setItem('alauda_locale', locale);

  //:yinmock
  // Configure Moment locale
  moment.locale(locale === 'zh_cn' ? 'zh-cn' : 'en');
  const localKey = TRANSLATION_API_KEYWORD[locale];

  // Load translation file from server and register as a new constant
  if (!defaultTranslationsMap[locale]) {
    const [defaultTranslations, customTranslations] = await Promise.all([
      ajax(getGlobal(I18NMANIFEST)[locale], false).catch(() => null),
      privateDeployEnabled &&
        ajax(`${customAssetsDir}/${locale}.${customLocaleSuffix}`, false).catch(
          () => null,
        ),
    ]);
    defaultTranslationsMap[locale] = defaultTranslations;
    customTranslationsMap[locale] = customTranslations;
  }
  let defaultTranslationsJson = {},
    customTranslationsJson = {},
    translationTemplateInputValue = Object.keys(DEFAULT_TRANS_TEMPLATE).reduce(
      (t: { [key: string]: string }, c: string) => {
        t[c] = DEFAULT_TRANS_TEMPLATE[c][localKey];
        return t;
      },
      {},
    );

  // http://confluence.alaudatech.com/pages/viewpage.action?pageId=35687660
  /**
   {
      "value": "{\"zh\": \"项目\", \"en\": \"project\"}",
      "type": "translation",
      "name": "project",
      "extra": ""
    }
   */
  if (!!namespace) {
    try {
      const translationConfiguration = await ajax(
        `ajax/v1/configuration/${namespace}?type=translation`,
        true,
        true,
      ).then(({ result }) => result);
      translationTemplateInputValue = Object.assign(
        {},
        translationTemplateInputValue,
        translationConfiguration.reduce(
          (
            t: { [key: string]: string },
            c: {
              name: string;
              value: string;
            },
          ) => {
            t[c.name] = JSON.parse(c.value)[localKey];
            return t;
          },
          {},
        ),
      );
    } catch (e) {
      console.error('Rubick configuration translation init failed:', e);
    }
  }

  try {
    defaultTranslationsJson = JSON.parse(
      template(defaultTranslationsMap[locale] as string)(
        translationTemplateInputValue,
      ) || null,
    );
    customTranslationsJson = JSON.parse(
      template(customTranslationsMap[locale] as string)(
        translationTemplateInputValue,
      ) || null,
    );
  } catch (e) {
    console.error('Rubick translation init failed:', e);
  }

  return { ...defaultTranslationsJson, ...customTranslationsJson };
}

export async function initWeblabs(namespace: string) {
  const { feature_flags } = await ajax<{ feature_flags: Weblabs }>(
    weblabsURL + '?namespace=' + namespace,
  );
  const weblabs = {};
  // in legacy code, some weblabs are still retrieved by their lowercase name
  Object.keys(feature_flags).forEach(key => {
    weblabs[key] = weblabs[key.toLowerCase()] = feature_flags[key];
  });
  return weblabs;
}

export const initEnvironments = () => ajax<Environments>(environmentsURL);

export const initI18nManifest = () =>
  ajax(initI18nManifestUrl + '?_=' + Date.now());

export const initStateInjectToken = async () => {
  const account = initAccount();
  if (account.namespace) {
    const weblabs = await initWeblabs(account.namespace);
    setGlobal(ACCOUNT, account);
    setGlobal(WEBLABS, weblabs);
  }
  const translations = await initTranslations(
    getGlobal(ENVIRONMENTS).is_private_deploy_enabled,
    account.namespace,
  );
  setGlobal(TRANSLATIONS, translations);
};
