export function transportCode(code: string): void {
  if (window.opener && window.opener.acceptCode) {
    window.opener.acceptCode(code);
  }
}
