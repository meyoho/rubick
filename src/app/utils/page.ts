export function isUserView() {
  return location.pathname.startsWith('/console/user');
}

export function isAdminView() {
  return location.pathname.startsWith('/console/admin');
}
