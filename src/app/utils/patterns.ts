export const RESOURCE_NAME_BASE = {
  pattern: /^[a-zA-Z]([-a-zA-Z0-9]*[a-zA-Z0-9])?$/,
  tip: 'regexp_tip_resource_name_base',
};

export const K8S_RESOURCE_NAME_BASE = {
  pattern: /^[a-z0-9]([-a-z0-9]*[a-z0-9])?$/,
  tip: 'regexp_tip_k8s_resource_name_base',
};

export const K8S_CONFIGMAP_KEY = {
  pattern: /^[-_a-zA-Z0-9][-._a-zA-Z0-9]*$/,
  tip: 'regexp_tip_k8s_resource_key',
};

export const K8S_SECRET_KEY = {
  pattern: /^[-_a-zA-Z0-9][-._a-zA-Z0-9]*$/,
  tip: 'regexp_tip_k8s_resource_key',
};

export const K8S_ENV_VARIABLE_NAME = {
  pattern: /^[_a-zA-Z][_a-zA-Z0-9]*$/,
  tip: 'regexp_tip_k8s_env_variable_name',
};

export const K8S_SERVICE_NAME = {
  pattern: /^[a-z]([-a-z0-9]*[a-z0-9])?$/,
  tip: 'regexp_tip_k8s_service_name',
};

export const K8S_VOLUME_MOUNT_SUB_PATH = {
  pattern: /^([a-zA-Z0-9_.][a-zA-Z0-9._-]*\/?)*$/,
  tip: 'regexp_tip_sub_path',
};

export const RBAC_CONSTRAINT_PATTERN = {
  pattern: /^[a-z0-9-_\.]*$/,
  tip: 'rbac_constraint_error',
};

export const RBAC_USERNAME_PATTERN = {
  pattern: /^[a-z][A-Za-z0-9-]{2,31}$/,
  tip: 'rbac_username_placeholder',
};

export const RBAC_PASSWORD_PATTERN = {
  pattern: /^(?![\d]+$)(?![a-zA-Z]+$)(?![^\da-zA-Z]+$).{6,64}$/,
  tip: 'rbac_password_placeholder',
};

export const ACCOUNT_ENTERPRISE_NAME_PATTERN = {
  pattern: /^[\s\S]{0,50}$/,
  tip: 'account_enterprise_name_error',
};

export const ACCOUNT_MOBILE_PATTERN = {
  pattern: /^[1]([3-9])[0-9]{9}$/,
  tip: 'account_mobile_error',
};

export const ACCOUNT_EMAIL_PATTERN = {
  pattern: /^[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?\.)+[a-zA-Z0-9](?:[a-zA-Z0-9-]*[a-zA-Z0-9])?$/,
  tip: 'account_email_error',
};

export const DOMAIN_NAME = {
  pattern: /^[a-z0-9]([-a-z0-9]*[a-z0-9])?(\.[a-z0-9]([-a-z0-9]*[a-z0-9])?)*$/,
  tip: 'regexp_tip_domain_name',
};

export const INT_PATTERN = {
  pattern: /^-?\d+$/,
  tip: 'integer_pattern',
};

export const POSITIVE_INT_PATTERN = {
  pattern: /^[1-9]\d*$/,
  tip: 'positive_integer_pattern',
};

export const NATURAL_NUMBER_PATTERN = {
  pattern: /^[0-9]\d*$/,
  tip: 'natural_number_pattern',
};

export const REGISTRY_PATH = {
  pattern: /^[a-z0-9]([\.\_\-a-z0-9]*[a-z0-9])?$/,
  tip: 'registry_path_rule',
};

export const PROJECT_PATTERN = {
  pattern: /^[a-z][a-z0-9-_\.]*$/,
  tip: 'project_name_placeholder',
};

export const INGRESS_PATH_PATTERN = {
  pattern: /^\/.*/,
  tip: 'ingress_path_pattern',
};

export const INGRESS_DOMAIN_PATTERN = {
  pattern: /^((?!\*).)*$/,
  tip: 'ingress_domain_pattern',
};

export const SPACE_PATTERN = {
  pattern: /^[a-z][a-z0-9\.\_\-]*$/,
  tip: 'space_name_placeholder',
};

export const IP_ADDRESS_PATTERN = {
  pattern: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
  tip: 'regexp_tip_ip_address',
};

export const IP_ADDRESS_PORT_PATTERN = {
  pattern: /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])(:[0-9]+)?$/,
  tip: 'regexp_tip_ip_address',
};

export const IP_ADDRESS_HOSTNAME_PATTERN = {
  pattern: /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])\.){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])$|^(([a-zA-Z]|[a-zA-Z][a-zA-Z0-9\-]*[a-zA-Z0-9])\.)*([A-Za-z]|[A-Za-z][A-Za-z0-9\-]*[A-Za-z0-9])$/,
  tip: 'regexp_tip_ip_address_or_hostname',
};

export const CIDR_PATTERN = {
  pattern: /^([0-9]{1,3}\.){3}[0-9]{1,3}(\/([0-9]|[1-2][0-9]|3[0-2]))?$/,
  tip: 'regexp_cidr_pattern',
};

export const K8S_RESOURCE_LABEL_KEY_NAME_PATTERN = {
  pattern: /^(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])?$/,
  tip: 'regexp_tip_resource_label_key_name',
};

export const K8S_RESOURCE_LABEL_KEY_DOMAIN_PATTERN = {
  pattern: /^[a-z0-9][-a-z0-9]{0,62}(\.[a-z0-9][-a-z0-9]{0,62})+$/,
  tip: 'regexp_tip_resource_label_key_domain',
};

export const K8S_RESOURCE_LABEL_VALUE_PATTERN = {
  pattern: /^(([A-Za-z0-9][-A-Za-z0-9_.]*)?[A-Za-z0-9])?$/,
  tip: 'regexp_tip_resource_label_value',
};

export const ALARM_RESOURCE_PATTERN = {
  pattern: /^[A-Za-z][A-Za-z0-9._-]*$/,
  tip: 'regexp_tip_alert_name',
};

export const ALARM_LABEL_KEY_PATTERN = {
  pattern: /^[a-zA-Z_][a-zA-Z0-9_]*$/,
  tip: 'regexp_tip_alert_label',
};

export const ALARM_THRESHOLD_PATTERN = {
  pattern: /^(?!0\d)\d{0,17}(\.\d{1,4})?$/,
  tip: 'regexp_tip_alert_threshold',
};

export const ALARM_METRIC_PATTERN = {
  pattern: /^[\x20-\x7e]*$/,
  tip: 'regexp_tip_alert_metric',
};
