import { ActivatedRouteSnapshot, RouterState } from '@angular/router';

/**
 * Helper function to get the leaf ActivatedRouteSnapshot of a RouterState
 * @param routerState
 * @returns {ActivatedRouteSnapshot}
 */
export function getLeafActivatedRouteSnapshot(
  routerState: RouterState,
): ActivatedRouteSnapshot {
  let leafActivatedRouteSnapshot = routerState.root.snapshot;
  while (leafActivatedRouteSnapshot.firstChild) {
    leafActivatedRouteSnapshot = leafActivatedRouteSnapshot.firstChild;
  }
  return leafActivatedRouteSnapshot;
}
