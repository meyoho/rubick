import {
  UrlSegment,
  UrlSegmentGroup,
  UrlSerializer,
  UrlTree,
} from '@angular/router';

export function getUrlParams(urlSerializer: UrlSerializer) {
  const url = window.location.href;
  const baseUrl = window.location.origin;
  const normalizedUrl = stripBaseHref(baseUrl, url);
  const urlTree = urlSerializer.parse(normalizedUrl);
  return getAllParams(urlTree);
}

function stripBaseHref(baseHref: string, url: string): string {
  return baseHref && url.startsWith(baseHref)
    ? url.substring(baseHref.length)
    : url;
}

function getAllParams(urlTree: UrlTree) {
  const segments: UrlSegment[] = mapChildrenIntoArray(urlTree.root).reduce(
    (accum, cur) => [...accum, ...cur.segments],
    [],
  );
  return segments.reduce(
    (accum, segment) => ({ ...accum, ...segment.parameters }),
    { ...urlTree.queryParams },
  );
}

function mapChildrenIntoArray(segment: UrlSegmentGroup) {
  let res = [segment];

  if (segment.hasChildren()) {
    Object.keys(segment.children).forEach(key => {
      res = [...mapChildrenIntoArray(segment.children[key])];
    });
  }

  return res;
}
