import { AbstractControl, ValidationErrors } from '@angular/forms';

import {
  K8S_RESOURCE_LABEL_KEY_DOMAIN_PATTERN,
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
} from './patterns';

export function k8sResourceLabelKeyValidator(
  control: AbstractControl,
): ValidationErrors | null {
  if (!control.value) {
    return null;
  }

  const keyPartition = control.value.split('/');
  if (keyPartition.length > 2) {
    return { pattern: true };
  }
  const checkDomain =
    keyPartition.length === 2
      ? K8S_RESOURCE_LABEL_KEY_DOMAIN_PATTERN.pattern.test(keyPartition[0])
      : true;
  const checkName =
    keyPartition.length === 1
      ? K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.pattern.test(keyPartition[0])
      : K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.pattern.test(keyPartition[1]);
  if (!checkDomain) {
    return { domainPattern: true };
  }
  if (!checkName) {
    return { namePattern: true };
  }

  return null;
}
