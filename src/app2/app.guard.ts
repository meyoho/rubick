import { NotificationService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';

@Injectable()
export class AppGuard implements CanActivate {
  constructor(
    private router: Router,
    private roleUtil: RoleUtilitiesService,
    private translate: TranslateService,
    private notificationService: NotificationService,
  ) {}

  canActivate(): Promise<boolean> | boolean {
    return this.roleUtil
      .resourceTypeSupportPermissions('operation_view', {}, 'get')
      .then(res => {
        if (!res) {
          if (this.router.routerState.snapshot.url.startsWith('/consle/user')) {
            this.notificationService.warning(
              this.translate.get('no_view_premission'),
            );
          } else {
            this.router.navigate(['/console/user']);
          }
        }
        return res;
      })
      .catch(() => false);
  }
}
