import { NgModule } from '@angular/core';
import { ClusterNameGuard } from 'app/guards/cluster-name-guard';
import { SharedModule } from 'app/shared/shared.module';
import { AppGuard } from 'app2/app.guard';
import { AppRoutingModule } from 'app2/app.routing.module';
import { LayoutModule } from 'app2/layout/layout.module';

import { HomeComponent } from './home.component';

@NgModule({
  declarations: [HomeComponent],
  imports: [SharedModule, LayoutModule, AppRoutingModule],
  providers: [AppGuard, ClusterNameGuard],
})
export class AppModule {
  constructor() {}
}
