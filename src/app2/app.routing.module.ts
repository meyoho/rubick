import { Component, NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ClusterNameGuard } from 'app/guards/cluster-name-guard';
import { AppGuard } from 'app2/app.guard';
import { LayoutComponent } from 'app2/layout/layout.component';
import { HomeComponent } from './home.component';

@Component({
  template: '',
})
export class EmptyComponent {}

/**
 * Define base level App base routes.
 */
export const appBaseRoutes: Routes = [
  {
    path: '',
    component: LayoutComponent,
    canActivate: [AppGuard],
    children: [
      {
        path: '',
        redirectTo: 'home',
        pathMatch: 'full',
      },
      {
        path: 'home',
        component: HomeComponent,
      },
      {
        path: 'empty-route',
        component: EmptyComponent,
      },
      // Lazy loading feature modules:
      {
        path: 'resource_management',
        canActivate: [ClusterNameGuard],
        loadChildren: './features/resource-manage/module#ResourceManageModule',
      },
      {
        path: 'app-catalog',
        loadChildren:
          './features/app-catalog/app-catalog.module#AppCatalogModule',
      },
      {
        path: 'image',
        loadChildren: './features/image/image.module#ImageModule',
      },
      {
        path: 'tool_chain',
        loadChildren: './features/tool-chain/tool-chain.module#ToolChainModule',
      },
      {
        path: 'event',
        canActivate: [ClusterNameGuard],
        loadChildren: './features/event/event.module#EventModule',
      },
      {
        path: 'log',
        loadChildren: './features/log/log.module#LogModule',
      },
      {
        path: 'notification',
        loadChildren:
          './features/notification/notification.module#NotificationModule',
      },
      {
        path: 'subnet',
        canActivate: [ClusterNameGuard],
        loadChildren: './features/network/subnet.module#SubnetModule',
      },
      {
        path: 'load_balancer',
        canActivate: [ClusterNameGuard],
        loadChildren:
          './features/network/loadbalancer.module#LoadbalancerModule',
      },
      {
        path: 'cluster',
        loadChildren: './features/cluster/cluster.module#ClusterModule',
      },
      {
        path: 'rbac',
        loadChildren: './features/rbac/rbac.module#RBACModule',
      },
      {
        path: 'ldap',
        loadChildren: './features/ldap/ldap.module#LDAPModule',
      },
      {
        path: 'resource-type-manage',
        loadChildren:
          './features/resource-type-manage/module#ReourceTypeManageModule',
      },
      {
        path: 'role-template',
        loadChildren: './features/rbac/role-template.module#RoleTemplateModule',
      },
      {
        path: 'storage',
        canActivate: [ClusterNameGuard],
        loadChildren: './features/storage/storage.module#StorageModule',
      },
      {
        path: 'alarm',
        canActivate: [ClusterNameGuard],
        loadChildren: './features/alarm/alarm.module#AlarmModule',
      },
      {
        path: 'monitor',
        canActivate: [ClusterNameGuard],
        loadChildren: './features/monitor/monitor.module#MonitorModule',
      },
      {
        path: 'jenkins',
        loadChildren: './features/jenkins/jenkins.module#JenkinsModule',
      },
      {
        path: 'pipelines',
        loadChildren: './features/pipelines/pipelines.module#PipelinesModule',
      },
      {
        path: 'credential',
        loadChildren:
          './features/credential/credential.module#CredentialModule',
      },
      {
        path: 'integration_center',
        loadChildren:
          './features/integration-center/integration-center.module#IntegrationCenterModule',
      },
      {
        path: 'projects',
        loadChildren: './features/projects/projects.module#ProjectsModule',
      },
      {
        path: 'domain',
        loadChildren: './features/network/domain/domain.module#DomainModule',
      },
      {
        path: 'audit',
        loadChildren: './features/audit/audit.module#AuditModule',
      },
      {
        path: 'auth-redirect',
        loadChildren:
          './features/auth-redirect/auth-redirect.module#AuthRedirectModule',
      },
      {
        path: '**',
        component: HomeComponent,
      },
    ],
  },
];

// Refer to https://angular.io/docs/ts/latest/guide/router.html
// see why we may want to use a module to define
@NgModule({
  imports: [RouterModule.forChild(appBaseRoutes)],
  exports: [RouterModule],
  declarations: [EmptyComponent],
})
export class AppRoutingModule {
  constructor() {}
}
