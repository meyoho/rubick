import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { AccountService } from 'app/services/api/account.service';
import { NotificationService } from 'app/services/api/notification.service';

@Component({
  selector: 'rc-alarm-action',
  styleUrls: ['alarm-action.component.scss'],
  templateUrl: './alarm-action.component.html',
})
export class AlarmActionComponent implements OnInit {
  @Output()
  alarmActionAdd = new EventEmitter<any[]>();
  @Input()
  actions: any[];
  @Input()
  componentName: string;
  @Input()
  kind: string;

  get showScaling() {
    return this.isUserView && this.kind !== 'DaemonSet';
  }
  isUserView = false;
  notifications: {
    uuid: string;
    name: string;
  }[];
  trackFn = (val: any = {}) => {
    return val.name;
  };
  filterFn = (filter: string, option: any) =>
    option.value.name.includes(filter);

  constructor(
    private notificationService: NotificationService,
    private accountService: AccountService,
  ) {}

  async ngOnInit() {
    this.isUserView = this.accountService.isUserView();
    this.notifications = await this.notificationService.getNotifications();
  }

  addAlarmAction() {
    this.actions.push({
      action_type: '',
    });
    this.alarmActionAdd.emit(this.actions);
  }

  removeActionResource(index: number) {
    this.actions.splice(index, 1);
    this.alarmActionAdd.emit(this.actions);
  }
}
