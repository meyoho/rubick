import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AlarmTemplateDetailComponent } from './alarm-template/detail/alarm-template-detail.component';
import { AlarmTemplateFormComponent } from './alarm-template/form/alarm-template-form.component';
import { AlarmTemplateComponent } from './alarm-template/list/component';
import { AlarmDashboardComponent } from './dashboard/alarm-dashboard.component';
import { PrometheusDetailComponent } from './prometheus-alarm/detail/component';
import { PrometheusAlarmFormComponent } from './prometheus-alarm/form/component';

const alarmRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: AlarmDashboardComponent,
  },
  {
    path: 'detail/:cluster/:namespace/:resource/:group/:name',
    component: PrometheusDetailComponent,
  },
  {
    path: 'create/:cluster/:namespace',
    component: PrometheusAlarmFormComponent,
  },
  {
    path: 'update/:cluster/:namespace/:resource/:group/:name',
    component: PrometheusAlarmFormComponent,
  },
  {
    path: 'templates',
    component: AlarmTemplateComponent,
  },
  {
    path: 'templates/create',
    component: AlarmTemplateFormComponent,
  },
  {
    path: 'templates/update/:name',
    component: AlarmTemplateFormComponent,
  },
  {
    path: 'templates/detail/:name',
    component: AlarmTemplateDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(alarmRoutes)],
  exports: [RouterModule],
})
export class AlarmRoutingModule {}
