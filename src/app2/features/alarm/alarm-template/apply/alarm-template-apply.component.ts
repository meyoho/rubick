import { DIALOG_DATA, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { AlarmService, AlarmTemplate } from 'app/services/api/alarm.service';
import { IndicatorType } from 'app/services/api/metric.service';
import { RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import {
  getRule,
  getRulesAction,
  parseAlertList,
} from 'app2/features/alarm/alarm.util';

enum TemplateKind {
  Cluster,
  Node,
}

@Component({
  selector: 'rc-alarm-template-apply',
  templateUrl: 'alarm-template-apply.component.html',
  styleUrls: ['alarm-template-apply.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmTemplateApplyComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;
  @Output()
  close: EventEmitter<boolean> = new EventEmitter();

  loading = true;
  initialized = false;
  submitting = false;

  clusters: Array<string> = [];
  nodes: Array<string> = [];
  metric_type: IndicatorType[];
  clusterNodePremission: boolean;
  viewModel: {
    template?: AlarmTemplate;
    kind?: string;
    name?: string;
  } = {};
  templatesGroup: AlarmTemplate[][];
  parseAlertList = parseAlertList;

  getRule = getRule;
  getRulesAction = getRulesAction;

  constructor(
    @Inject(DIALOG_DATA)
    private modalData: {
      metric_type: IndicatorType[];
    },
    private cdr: ChangeDetectorRef,
    private alarmService: AlarmService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private region: RegionService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  ngOnInit() {
    this.region.getCurrentRegion().then(async cluster => {
      this.clusters = [cluster.name];
      this.metric_type = this.modalData.metric_type;
      try {
        const alarmTemplates = await this.alarmService.getAlertTemplates({
          page: 1,
          page_size: 999,
        });
        this.templatesGroup = [[], []];
        alarmTemplates.results.forEach(el => {
          if (el.kind === 'cluster') {
            this.templatesGroup[TemplateKind.Cluster].push(el);
          } else if (el.kind === 'node') {
            this.templatesGroup[TemplateKind.Node].push(el);
          }
        });
        this.clusterNodePremission = await this.roleUtil.resourceTypeSupportPermissions(
          'cluster_node',
          {},
          'list',
        );
        if (this.clusterNodePremission) {
          const { items } = await this.region.getClusterNodes(cluster.name);
          this.nodes = items.map(
            node =>
              node.status.addresses.find(addr => addr.type === 'InternalIP')
                .address,
          );
        }
      } finally {
        this.cdr.markForCheck();
        this.loading = false;
        this.initialized = true;
      }
    });
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    try {
      this.submitting = true;
      await this.alarmService.applyAlertTemplate(this.viewModel.template.name, {
        cluster: {
          name: this.clusters[0],
        },
        metric: {
          queries: [
            {
              labels: [
                {
                  name: 'name',
                  value: this.viewModel.name,
                },
                {
                  name: 'kind',
                  value: this.viewModel.template.kind,
                },
              ],
            },
          ],
        },
      });
      this.close.next(true);
      this.auiNotificationService.success(
        this.translate.get('alarm_template_apply_success'),
      );
    } catch (rejection) {
      this.submitting = false;
      this.errorsToastService.error(rejection);
      this.cdr.markForCheck();
    }
  }

  cancel() {
    this.close.next(false);
  }

  onTemplateSelect(_template: AlarmTemplate) {
    this.viewModel.name = null;
    this.cdr.markForCheck();
  }
}
