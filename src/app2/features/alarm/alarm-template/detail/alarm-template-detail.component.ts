import { DialogService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, of, Subject } from 'rxjs';
import {
  catchError,
  first,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import { AlarmService, AlarmTemplate } from 'app/services/api/alarm.service';
import { IndicatorType, MetricService } from 'app/services/api/metric.service';
import { RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import {
  getRule,
  getRulesAction,
  parseAlertList,
} from 'app2/features/alarm/alarm.util';
import { delay } from 'app2/utils';

@Component({
  selector: 'rc-alarm-template-detail',
  templateUrl: 'alarm-template-detail.component.html',
  styleUrls: ['alarm-template-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmTemplateDetailComponent implements OnInit, OnDestroy {
  cluster: string;
  name: string;
  data: AlarmTemplate;
  initialized = false;

  metric_type: IndicatorType[];
  parseAlertList = parseAlertList;
  columns = ['name', 'rule', 'level', 'action'];
  onDestroy$ = new Subject<void>();
  canUpdateAlarmTemplate: boolean;
  canDeleteAlarmTemplate: boolean;

  getRule = getRule;
  getRulesAction = getRulesAction;

  constructor(
    private router: Router,
    private dialogService: DialogService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private errorsToastService: ErrorsToastService,
    private regionService: RegionService,
    private alarmService: AlarmService,
    private metricService: MetricService,
    public translate: TranslateService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  async ngOnInit() {
    [
      this.canUpdateAlarmTemplate,
      this.canDeleteAlarmTemplate,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'alert_template',
      {},
      ['update', 'delete'],
    );
    this.route.params
      .pipe(
        takeUntil(this.onDestroy$),
        first(),
      )
      .subscribe(params => {
        this.name = params['name'];
        if (this.name) {
          this.alarmService
            .getAlertTemplate(this.name)
            .then(data => {
              this.data = data;
            })
            .catch(err => {
              this.errorsToastService.error(err);
            })
            .finally(() => {
              this.initialized = true;
              this.cdr.markForCheck();
            });
        } else {
          this.initialized = true;
          this.cdr.markForCheck();
        }
      });

    this.regionService.region$
      .pipe(
        switchMap(cluster => {
          if (!cluster) {
            return of([]);
          }
          this.cluster = cluster.name;
          return this.getIndicators(cluster.name).pipe(
            catchError(error => of(error)),
          );
        }),
        publishReplay(1),
        refCount(),
      )
      .subscribe(metric_type => {
        this.metric_type = metric_type;
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(index: number) {
    return index;
  }

  getIndicators(clusterName: string): Observable<any> {
    return from(this.metricService.getIndicators(clusterName));
  }

  updateAlarmTemplate(alarm: any) {
    this.router.navigate(['/console/admin/alarm/templates/update', alarm.name]);
  }

  async deleteAlarmTemplate(alarm: AlarmTemplate) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_alarm_template_title'),
        content: this.translate.get('delete_alarm_template_content', {
          alarm_name: alarm.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      await this.alarmService.deleteAlertTemplate(alarm.name);
      this.auiNotificationService.success(
        this.translate.get('alarm_template_delete_success'),
      );
      await delay(500);
      this.router.navigate(['/console/admin/alarm/templates']);
    } catch (rejection) {
      this.errorsToastService.error(rejection);
    }
  }
}
