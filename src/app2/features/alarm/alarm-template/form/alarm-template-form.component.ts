import { NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { AlarmService, AlarmTemplate } from 'app/services/api/alarm.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { ALARM_RESOURCE_PATTERN } from 'app/utils/patterns';
import { hasDuplicateBy } from 'app2/features/alarm/alarm.util';

@Component({
  selector: 'rc-alarm-template-form',
  templateUrl: 'alarm-template-form.component.html',
  styleUrls: ['alarm-template-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmTemplateFormComponent implements OnInit, OnDestroy {
  @ViewChild('form')
  form: NgForm;

  cluster: string;
  name: string;
  data: AlarmTemplate;
  formSubmitText = 'create';
  initialized = false;
  submitting = false;
  resourceNameReg = ALARM_RESOURCE_PATTERN;

  viewModel: AlarmTemplate = {
    name: '',
    kind: 'cluster',
    description: '',
    template: [],
  };
  onDestroy$ = new Subject<void>();

  constructor(
    private cdr: ChangeDetectorRef,
    private router: Router,
    private route: ActivatedRoute,
    private location: Location,
    private alarmService: AlarmService,
    private translate: TranslateService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  ngOnInit() {
    this.route.params
      .pipe(
        takeUntil(this.onDestroy$),
        first(),
      )
      .subscribe(params => {
        this.name = params['name'];
        if (this.name) {
          this.formSubmitText = 'update';
          this.alarmService
            .getAlertTemplate(this.name)
            .then(data => {
              this.data = data;
              data.template = data.template.map((item: any) => {
                let metric_name = '';
                let expr = '';
                item.metric.queries[0].labels.forEach(
                  (label: { name: string; value: string }) => {
                    if (label.name === '__name__') {
                      metric_name = label.value;
                    }
                    if (label.name === 'expr') {
                      expr = label.value;
                    }
                  },
                );
                return {
                  ...item,
                  metric_name,
                  expr,
                };
              });
              this.viewModel = data;
            })
            .finally(() => {
              this.initialized = true;
              this.cdr.markForCheck();
            });
        } else {
          this.initialized = true;
          this.cdr.markForCheck();
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (this.viewModel.template.length === 0) {
      this.auiNotificationService.error(
        this.translate.get('alarm_template_rules_error'),
      );
      return;
    }
    const duplicate = hasDuplicateBy(this.viewModel.template, 'name');
    if (duplicate) {
      this.auiNotificationService.error(
        this.translate.get('alarm_template_rules_duplicate_name', {
          name: duplicate,
        }),
      );
      return;
    }
    const body = this.viewModel;
    if (this.data) {
      try {
        this.submitting = true;
        await this.alarmService.updateAlertTemplate(this.name, body);
        this.location.back();
        this.auiNotificationService.success(
          this.translate.get('alarm_template_update_success'),
        );
      } catch (rejection) {
        this.submitting = false;
        this.errorsToastService.error(rejection);
        this.cdr.markForCheck();
      }
    } else {
      try {
        this.submitting = true;
        await this.alarmService.createAlertTemplate(body);
        this.router.navigate([
          '/console/admin/alarm/templates/detail',
          body.name,
        ]);
        this.auiNotificationService.success(
          this.translate.get('alarm_template_create_success'),
        );
      } catch (rejection) {
        this.submitting = false;
        this.errorsToastService.error(rejection);
        this.cdr.markForCheck();
      }
    }
  }

  cancel() {
    this.location.back();
  }

  shouldDisableKind(kind: string) {
    if (this.data && this.data.kind !== kind) {
      return true;
    } else if (this.viewModel.template.length && this.viewModel.kind !== kind) {
      return true;
    }
    return false;
  }
}
