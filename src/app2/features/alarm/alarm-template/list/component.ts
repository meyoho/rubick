import { DialogService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, of, Subject } from 'rxjs';
import {
  catchError,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import {
  Alarm,
  AlarmService,
  AlarmTemplate,
} from 'app/services/api/alarm.service';
import { IndicatorType, MetricService } from 'app/services/api/metric.service';
import { RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import {
  getRule,
  getRulesAction,
  parseAlertList,
} from 'app2/features/alarm/alarm.util';
import { delay } from 'app2/utils';
import { PageParams, ResourceList } from 'app_user/core/types';

@Component({
  selector: 'rc-alarm-template-list',
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmTemplateComponent
  extends BaseResourceListComponent<AlarmTemplate>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  columns = ['name', 'type', 'rules', 'description', 'create_time', 'action'];
  cluster: string;
  metric_type: IndicatorType[];
  parseAlertList = parseAlertList;
  keyword: string;

  initialized: boolean;
  canCreateAlarmTemplate: boolean;

  getRule = getRule;
  getRulesAction = getRulesAction;

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    @Inject(ACCOUNT) public account: Account,
    private alarmService: AlarmService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private regionService: RegionService,
    private auiNotificationService: NotificationService,
    private metricService: MetricService,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    const [
      canCreateAlarmTemplate,
      canViewAlarmTemplate,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'alert_template',
      {},
      ['create', 'list'],
    );
    this.canCreateAlarmTemplate = canCreateAlarmTemplate;
    if (!canViewAlarmTemplate) {
      this.auiNotificationService.error(
        this.translate.get('permission_denied'),
      );
    }

    this.regionService.region$
      .pipe(
        takeUntil(this.onDestroy$),
        switchMap(cluster => {
          if (!cluster) {
            return of([]);
          }
          this.cluster = cluster.name;
          return this.getIndicators(cluster.name).pipe(
            catchError(error => of(error)),
          );
        }),
        publishReplay(1),
        refCount(),
      )
      .subscribe(metric_type => {
        this.metric_type = metric_type;
      });

    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.alarmService.getAlertTemplates({
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
        name: params.search,
      }),
    );
  }

  getIndicators(clusterName: string): Observable<any> {
    return from(this.metricService.getIndicators(clusterName));
  }

  trackByFn(index: number) {
    return index;
  }

  pageChanged(pageEvent: PageParams) {
    this.onPageEvent({
      pageIndex: ++pageEvent.pageIndex,
      pageSize: pageEvent.pageSize,
    });
  }

  createAlarmTemplate() {
    this.router.navigate(['/console/admin/alarm/templates/create']);
  }

  updateAlarmTemplate(alarm: any) {
    this.router.navigate(['/console/admin/alarm/templates/update', alarm.name]);
  }

  async deleteAlarmTemplate(alarm: AlarmTemplate) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_alarm_template_title'),
        content: this.translate.get('delete_alarm_template_content', {
          alarm_name: alarm.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      await this.alarmService.deleteAlertTemplate(alarm.name);
      this.auiNotificationService.success(
        this.translate.get('alarm_template_delete_success'),
      );
      await delay(500);
      this.onUpdate(null);
    } catch (rejection) {
      this.errorsToastService.error(rejection);
    }
  }

  parseRules(template: Alarm[]) {
    return parseAlertList(template, '', this.metric_type, this.cluster);
  }

  hasPermission(item: AlarmTemplate, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'alert_template', action);
  }
}
