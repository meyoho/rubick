import { DIALOG_DATA } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm, Validators } from '@angular/forms';

import { Subject } from 'rxjs';
import { debounceTime, distinctUntilChanged, takeUntil } from 'rxjs/operators';

import {
  AGGREGATORS,
  LEVELS,
  UNITS,
} from 'app/features-shared/app/utils/monitor';
import { Alarm } from 'app/services/api/alarm.service';
import {
  IndicatorType,
  MetricQueries,
  MetricQuery,
  MetricService,
} from 'app/services/api/metric.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { formatNumber } from 'app/utils/format-number';
import {
  ALARM_LABEL_KEY_PATTERN,
  ALARM_METRIC_PATTERN,
  ALARM_RESOURCE_PATTERN,
  ALARM_THRESHOLD_PATTERN,
} from 'app/utils/patterns';
import { getUnitType, parseAlertList } from 'app2/features/alarm/alarm.util';

const ALARM_TIME_STAMP_OPTIONS = [1, 2, 3, 5, 10, 30].map(el => ({
  type: el,
  offset: el * 60,
}));

interface Notifications {
  uuid: string;
  name: string;
}

interface AlarmActions {
  action_type: string;
  notifications?: Notifications[];
}

@Component({
  selector: 'rc-alarm-template-rule-form',
  templateUrl: 'alarm-template-rule-form.component.html',
  styleUrls: ['alarm-template-rule-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmTemplateRuleFormComponent implements OnInit, OnDestroy {
  @ViewChild('form')
  form: NgForm;
  @Output()
  close: EventEmitter<boolean | Object> = new EventEmitter();

  cluster: string;
  metric_mode = 'default';
  prometheus_metrics: string[];
  function_names: string[];
  prometheus_suggestion: string[];
  function_names_suggestion: string[];
  metric_type: IndicatorType[];
  metrics: IndicatorType[] = [];
  formSubmitText = 'add';
  unitName = '%';
  alertMetric: MetricQuery;
  initialized = false;
  metricReg = ALARM_METRIC_PATTERN;
  resourceNameReg = ALARM_RESOURCE_PATTERN;
  thresholdReg = ALARM_THRESHOLD_PATTERN;
  metricMapper = {
    pattern: this.translate.get(ALARM_METRIC_PATTERN.tip),
    metric: this.translate.get(ALARM_METRIC_PATTERN.tip),
  };
  labelValidator = {
    key: [Validators.pattern(ALARM_LABEL_KEY_PATTERN.pattern)],
  };
  labelErrorMapper = {
    key: {
      pattern: this.translate.get(ALARM_LABEL_KEY_PATTERN.tip),
    },
  };
  alertNameMapper = {
    pattern: this.translate.get(ALARM_RESOURCE_PATTERN.tip),
    duplicate: this.translate.get('alert_name_already_existed'),
  };
  folded = true;

  aggregators = AGGREGATORS;
  levels = LEVELS;
  unit = '';
  units = UNITS;
  timeStampOptions = ALARM_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type + this.translate.get('minutes'),
    };
  });
  compareOptions = ['>', '>=', '==', '<=', '<', '!='];
  actions: AlarmActions[] = [];
  labels: Object = {};
  annotations: Object = {};
  data_type = false;
  viewModel: {
    name?: string;
    metric?: string;
    timeStampOption?: number;
    compare?: string;
    threshold?: any;
    level?: string;
    aggregator?: string;
    wait?: number;
    unit?: string;
    metric_expression?: string;
  } = {};
  expressionChanged$: Subject<string> = new Subject<string>();
  onDestroy$ = new Subject<void>();

  constructor(
    @Inject(DIALOG_DATA)
    private modalData: {
      cluster: string;
      kind: string;
      prometheus_metrics: string[];
      function_names: string[];
      metric_type: IndicatorType[];
      data: Alarm;
    },
    private cdr: ChangeDetectorRef,
    private metricService: MetricService,
    private translate: TranslateService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  async ngOnInit() {
    this.expressionChanged$
      .pipe(
        takeUntil(this.onDestroy$),
        debounceTime(300),
        distinctUntilChanged(),
      )
      .subscribe(expression => {
        this.predictExpression(expression);
      });
    this.cluster = this.modalData.cluster;
    this.metric_type = this.modalData.metric_type;
    this.prometheus_metrics = this.modalData.prometheus_metrics;
    this.function_names = this.modalData.function_names;
    this.filterMetrics(this.modalData.kind);
    this.initViewModel();
    this.cdr.markForCheck();
    this.initialized = true;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  private initViewModel() {
    this.viewModel.compare = this.compareOptions[0];
    this.viewModel.level = this.levels[2];
    this.viewModel.wait = this.timeStampOptions[0].offset;
    if (this.modalData.data) {
      this.formSubmitText = 'edit';
      const data = parseAlertList(
        [this.modalData.data],
        '',
        this.metric_type,
        this.cluster,
      )[0];
      this.viewModel.name = data.name;
      this.viewModel.compare = data.compare;
      if (data.metric_name === 'custom') {
        this.metric_mode = 'custom';
        this.viewModel.metric_expression = data.expr;
        this.viewModel.unit = data.unit;
      } else {
        this.metric_mode = 'default';
        this.viewModel.metric = data.metric_name;
      }
      this.viewModel.wait = data.wait;
      if (data.notifications) {
        this.actions.push({
          action_type: 'notification',
          notifications: data.notifications,
        });
      }
      if (data.scale_up) {
        this.actions.push({
          action_type: 'scale_up',
        });
      }
      if (data.scale_down) {
        this.actions.push({
          action_type: 'scale_down',
        });
      }
      this.viewModel.level = data.severity || data.labels.severity;
      this.labels = data.labels;
      this.annotations = data.annotations;
      if (data.metric && data.metric.queries && data.metric.queries[0]) {
        const aggregator = data.metric.queries[0].aggregator;
        if (aggregator !== undefined && aggregator !== '') {
          this.data_type = true;
        }
        this.viewModel.aggregator = aggregator;
        this.viewModel.timeStampOption = data.metric.queries[0].range;
      }
      if (data.unit === '%') {
        this.viewModel.threshold = formatNumber(data.threshold * 100, [
          1,
          0,
          4,
        ]);
      } else {
        this.viewModel.threshold = data.threshold;
        this.unitName = this.translate.get(data.unit);
      }
    } else {
      this.onMetricSelect(this.viewModel.metric);
    }
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (this.metric_mode === 'custom') {
      this.unitName = this.viewModel.unit;
    }
    const query: MetricQueries = {
      aggregator: this.viewModel.aggregator,
      range: this.viewModel.timeStampOption,
      labels: [],
    };
    if (this.metric_mode === 'custom') {
      query.labels = [
        {
          type: 'EQUAL',
          name: '__name__',
          value: 'custom',
        },
        {
          type: 'EQUAL',
          name: 'expr',
          value: this.viewModel.metric_expression,
        },
      ];
    } else {
      query.labels = [
        {
          type: 'EQUAL',
          name: '__name__',
          value: this.viewModel.metric,
        },
      ];
    }
    const body: Alarm = {
      name: this.viewModel.name,
      metric: {
        queries: [query],
      },
      compare: this.viewModel.compare,
      threshold: 0,
      wait: this.viewModel.wait,
      expr: this.viewModel.metric_expression,
    };
    if (this.metric_mode === 'custom') {
      body.unit = this.viewModel.unit;
      body.metric_name = this.metric_mode;
    } else {
      body.metric_name = this.viewModel.metric;
    }
    if (this.percentFlag()) {
      body.threshold = (this.viewModel.threshold * 10000) / 1000000;
    } else {
      body.threshold = this.viewModel.threshold / 1;
    }
    const notificationActions = this.actions.filter(el => {
      return el.action_type === 'notification';
    });
    if (notificationActions.length > 0) {
      let notifications: Array<Notifications> = [];
      notificationActions.forEach(el => {
        if (el.notifications) {
          notifications = notifications.concat(el.notifications);
        }
      });
      body.notifications = notifications.filter(
        (notification, index, self) =>
          self.findIndex(t => t.uuid === notification.uuid) === index,
      );
    }
    body.labels = {
      ...this.labels,
      severity: this.viewModel.level,
    };
    body.annotations = this.annotations;
    this.close.next(body);
  }

  cancel() {
    this.close.next(false);
  }

  onAlarmActionAdd(actions: AlarmActions[]) {
    this.actions = actions;
  }

  onMetricSelect(metric: string) {
    const unit = this.getUnitType(metric);
    if (!unit) {
      this.unitName = '';
    } else {
      this.unitName = this.translate.get(unit);
    }
  }

  // 动态更新自动补全，见http://confluence.alaudatech.com/pages/viewpage.action?pageId=36864640 最下方的辅助输入API
  private predictExpression(exp: string) {
    const found = exp.match(/([\d\w\-])+$/g);
    if (found && found[0]) {
      const match = found[0].toLowerCase();
      // 监控指标补全
      this.prometheus_suggestion = this.prometheus_metrics
        .filter(el => el.startsWith(match))
        .map(el => {
          return exp + el.split(found[0])[1];
        });
      // 函数名补全
      this.function_names_suggestion = this.function_names
        .filter(el => el.startsWith(match))
        .map(el => {
          return exp + el.split(found[0])[1];
        });
      this.cdr.markForCheck();
    }
    // label的可用值补全
    let foundLabel = exp.match(/([\d\w\-])+=([\d\w\-])+$/g);
    if (foundLabel && foundLabel[0]) {
      foundLabel = foundLabel[0].split('=');
      this.metricService
        .getPrometheusMetricLabels(this.cluster, foundLabel[0])
        .then(labels => {
          this.prometheus_suggestion = labels
            .filter(el => {
              return el['value'][1].startsWith(foundLabel[1]);
            })
            .map(el => {
              return exp + el['value'][1].split(foundLabel[1])[1];
            });
          this.cdr.markForCheck();
        });
    }
  }

  onExpressionInput(event: Event) {
    this.expressionChanged$.next(event.target['value']);
  }

  onCustomUnitSelect(unit: string) {
    this.unitName = unit;
  }

  private filterMetrics(kind: string) {
    this.metrics = this.metric_type.filter(el => {
      return el.kind === kind && el.type === 'metric';
    });
    this.viewModel.metric = this.metrics[0].name;
  }

  onMetricModeSelect(metric_mode: string) {
    if (metric_mode === 'custom') {
      this.data_type = false;
    }
  }

  onDataTypeSelect(data_type: string) {
    if (data_type) {
      this.viewModel.timeStampOption = this.timeStampOptions[0].offset;
      this.viewModel.aggregator = this.aggregators[0].key;
    } else {
      this.viewModel.timeStampOption = 0;
      this.viewModel.aggregator = '';
    }
  }

  private getUnitType(metric: string) {
    return getUnitType(metric, this.metric_type);
  }

  private percentFlag() {
    if (this.metric_mode === 'custom') {
      if (this.viewModel.unit === '%') {
        return true;
      }
    } else if (this.getUnitType(this.viewModel.metric) === '%') {
      return true;
    }
    return false;
  }
}
