import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { filter } from 'rxjs/operators';

import { AlarmService } from 'app/services/api/alarm.service';
import { AlarmTemplateRule } from 'app/services/api/alarm.service';
import { IndicatorType, MetricService } from 'app/services/api/metric.service';
import { RegionService } from 'app/services/api/region.service';
import { TranslateService } from 'app/translate/translate.service';
import { AlarmTemplateRuleFormComponent } from 'app2/features/alarm/alarm-template/rule-form/alarm-template-rule-form.component';
import { getRule } from 'app2/features/alarm/alarm.util';

@Component({
  selector: 'rc-alarm-template-rule-list',
  templateUrl: './alarm-template-rule-list.component.html',
  styleUrls: ['./alarm-template-rule-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => AlarmTemplateRuleListComponent),
      multi: true,
    },
  ],
})
export class AlarmTemplateRuleListComponent
  implements OnInit, ControlValueAccessor {
  @Input()
  kind: string;
  templates: AlarmTemplateRule[];
  clusterName: string;
  prometheus_metrics: string[];
  function_names: string[];
  default_labels: string[];
  metric_type: IndicatorType[];
  onChange = (_templates: AlarmTemplateRule[]) => {};

  getRule = getRule;

  constructor(
    private cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private alarmService: AlarmService,
    private metricService: MetricService,
    private region: RegionService,
    public translate: TranslateService,
  ) {}

  ngOnInit() {
    this.region.region$
      .pipe(filter(region => !!region))
      .subscribe(async cluster => {
        this.clusterName = cluster.name;
        const [
          metric_type,
          prometheus_metrics,
          function_names,
          default_labels,
        ] = await Promise.all([
          this.metricService.getIndicators(this.clusterName),
          this.metricService.getPrometheusMetrics(this.clusterName),
          this.alarmService.getPrometheusFunctions(),
          this.alarmService.getAlertConfig(),
        ]);
        this.metric_type = metric_type;
        this.prometheus_metrics = prometheus_metrics || [];
        this.function_names = function_names || [];
        this.default_labels = default_labels || [];
        this.cdr.markForCheck();
      });
  }

  add() {
    const modelRef = this.dialogService.open(AlarmTemplateRuleFormComponent, {
      data: {
        cluster: this.clusterName,
        kind: this.kind,
        metric_type: this.metric_type,
        prometheus_metrics: this.prometheus_metrics,
        function_names: this.function_names,
        default_labels: this.default_labels,
      },
      size: DialogSize.Large,
    });
    modelRef.componentInstance.close.subscribe((el: AlarmTemplateRule) => {
      if (el) {
        this.templates.push(el);
        this.onChange(this.templates);
      }
      modelRef.close();
    });
  }

  edit(index: number) {
    const modelRef = this.dialogService.open(AlarmTemplateRuleFormComponent, {
      data: {
        cluster: this.clusterName,
        kind: this.kind,
        metric_type: this.metric_type,
        prometheus_metrics: this.prometheus_metrics,
        function_names: this.function_names,
        default_labels: this.default_labels,
        data: this.templates[index],
      },
      size: DialogSize.Large,
    });
    modelRef.componentInstance.close.subscribe((el: AlarmTemplateRule) => {
      if (el) {
        this.templates[index] = el;
        this.onChange(this.templates);
      }
      modelRef.close();
    });
  }

  remove(index: number) {
    this.templates.splice(index, 1);
    this.onChange(this.templates);
  }

  writeValue(templates: AlarmTemplateRule[]): void {
    if (templates && this.templates !== templates) {
      this.templates = templates;
    }
  }

  registerOnChange(fn: (templates: AlarmTemplateRule[]) => void): void {
    this.onChange = fn;
  }

  registerOnTouched(_fn: () => void): void {}
}
