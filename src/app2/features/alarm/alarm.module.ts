import { NgModule } from '@angular/core';

import { AlarmSharedModule } from 'app/features-shared/alarm/shared.module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { AlarmRoutingModule } from 'app2/features/alarm/alarm-routing.module';
import { AlarmTemplateApplyComponent } from './alarm-template/apply/alarm-template-apply.component';
import { AlarmTemplateDetailComponent } from './alarm-template/detail/alarm-template-detail.component';
import { AlarmTemplateFormComponent } from './alarm-template/form/alarm-template-form.component';
import { AlarmTemplateComponent } from './alarm-template/list/component';
import { AlarmTemplateRuleFormComponent } from './alarm-template/rule-form/alarm-template-rule-form.component';
import { AlarmTemplateRuleListComponent } from './alarm-template/rule-list/alarm-template-rule-list.component';
import { AlarmDashboardComponent } from './dashboard/alarm-dashboard.component';
import { PrometheusDetailComponent } from './prometheus-alarm/detail/component';
import { PrometheusAlarmFormComponent } from './prometheus-alarm/form/component';
import { PrometheusAlarmComponent } from './prometheus-alarm/list/component';

@NgModule({
  imports: [
    SharedModule,
    AlarmRoutingModule,
    AlarmSharedModule,
    EventSharedModule,
    FormTableModule,
  ],
  declarations: [
    AlarmDashboardComponent,
    AlarmTemplateComponent,
    AlarmTemplateDetailComponent,
    AlarmTemplateFormComponent,
    AlarmTemplateRuleFormComponent,
    AlarmTemplateApplyComponent,
    AlarmTemplateRuleListComponent,
    PrometheusAlarmComponent,
    PrometheusDetailComponent,
    PrometheusAlarmFormComponent,
  ],
  entryComponents: [
    AlarmTemplateRuleFormComponent,
    AlarmTemplateApplyComponent,
  ],
})
export class AlarmModule {}
