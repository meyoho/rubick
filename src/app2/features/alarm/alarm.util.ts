import { Alarm, AlarmDetail } from 'app/services/api/alarm.service';
import { IndicatorType } from 'app/services/api/metric.service';
import { formatNumber } from 'app/utils/format-number';

export function getUnit(item: Alarm, metric_type: IndicatorType[]) {
  // 如果是自定义告警，返回自定义的单位
  if (item.metric_name === 'custom') {
    return item.unit;
  }
  // 如果不是自定义告警，返回与告警指标匹配的单位
  const found = metric_type.find(el => {
    return el.name === item.metric_name;
  });
  return found ? found.unit : undefined;
}

export function getThreshold(item: Alarm, metric_type: IndicatorType[] = []) {
  const found = metric_type.find(el => {
    return el.name === item.metric_name;
  });
  if ((found && found.unit === '%') || (item.unit && item.unit === '%')) {
    return item.threshold * 100;
  }
  return item.threshold;
}

export function getUnitType(metric: string, metric_type: IndicatorType[] = []) {
  const found = metric_type.find(el => {
    return el.name === metric;
  });
  if (found) {
    return found.unit;
  }
  return undefined;
}

export function getRulesAction(item: Alarm) {
  const alarm_actions: string[] = [];
  if (item.notifications) {
    alarm_actions.push('notification');
  }
  if (item.scale_up || item.scale_down) {
    alarm_actions.push('scaling');
  }
  const action = alarm_actions
    .map(el => {
      return this.translate.get(el);
    })
    .join(' ');
  if (action) {
    return action;
  }
  return '-';
}

export function getRule(item: Alarm, metric_type: IndicatorType[] = []) {
  let metric = item.metric_name;
  if (item.metric_name === 'workload.log.keyword.count') {
    return this.translate.get('alarm_log_list_rule', {
      time: item.metric.queries[0].range / 60,
      query: item.query,
      compare: item.compare,
      threshold: formatNumber(getThreshold(item, metric_type), [1, 0, 4]),
    });
  }
  if (item.metric_name === 'custom') {
    if (item.expr.length <= 100) {
      metric = item.expr;
    } else {
      metric = `${item.expr.slice(0, 50)}...${item.expr.slice(-50)}`;
    }
  }
  return `${metric} ${item.compare} ${formatNumber(
    getThreshold(item, metric_type),
    [1, 0, 4],
  )}${this.translate.get(getUnit(item, metric_type))} ${this.translate.get(
    'alarm_wait_time_last',
    { time: item.wait / 60 },
  )}`;
}

/**
 * 在告警中，图表有灰色背景，增加网格线
 */
export function parseChartOptions(chartOption: any) {
  chartOption.chart.plotBackgroundColor = '#FCFCFC';
  chartOption.yAxis['gridLineWidth'] = 1;
  chartOption.xAxis['gridLineWidth'] = 1;
  chartOption.yAxis['tickPosition'] = 'inside';
}

/**
 * 解析后端返回的告警，补全参数用于列表和详情页。
 * @param list API返回的告警列表
 * @param search 根据告警名称过滤
 * @param METRICS_TYPE 预设的指标数组，包含指标名称、类型、查询表达式和单位
 * @param cluster_name 在用于补全集群名称
 */
export function parseAlertList(
  list: Alarm[],
  search: string,
  METRICS_TYPE: IndicatorType[] = [],
  cluster_name?: string,
): Alarm[] {
  if (!list) {
    return [];
  }
  list = list.map((item: Alarm) => {
    let display_name = '';
    let metric_name = '';
    let metric_tooltip = '';
    let namespace = '';
    let type = '';
    let unit = '';
    let expr = '';
    let kind = '';
    let query = '';
    // 在告警中，metric.queries里有且仅有一个元素
    item.metric.queries[0].labels.forEach(
      (label: { name: string; value: string }) => {
        // http://confluence.alaudatech.com/pages/viewpage.action?pageId=36864640
        // __name__表示监控指标的名字, eg: workload.cpu.utilization/custom/workload.log.keyword.count
        // 非自定义告警时，指标单位需要通过METRICS_TYPE的键值对解析出来
        switch (label.name) {
          case '__name__':
            metric_name = label.value;
            const found = METRICS_TYPE.find(el => {
              return el.name === label.value;
            });
            if (found) {
              unit = found.unit;
            }
            break;
          // kind可以是Cluster、Node、Deployment、DaemonSet和StatefulSet
          case 'kind':
            kind = label.value;
            break;
          // 非自定义告警时，name表示告警对象的名字
          // 自定义告警时，name为空
          case 'name':
            display_name = label.value;
            metric_tooltip = label.value;
            break;
          // 非自定义告警且是组件告警时，namespace表示组件所在命名空间的名字
          case 'namespace':
            namespace = label.value;
            break;
          // expr表示prometheus查询表达式的名字，仅在自定义告警时生效
          case 'expr':
            expr = label.value;
            break;
          // query表示日志查询条件，在日志告警起作用
          case 'query':
            query = label.value;
            break;
        }
      },
    );
    // http://confluence.alaudatech.com/pages/viewpage.action?pageId=27171757
    // item.resource_name遵循链接文档的规则，可以通过前缀判断是集群/主机/组件的告警
    // metric_tooltip是拼装好的告警资源，用于展示在告警列表中
    // type可以直接通过kind判断，kind的首字母均为小写
    switch (kind.toLocaleLowerCase()) {
      case 'node':
        type = 'node';
        break;
      case 'cluster':
        type = 'cluster';
        metric_tooltip = cluster_name;
        break;
      case 'deployment':
      case 'daemonset':
      case 'statefulset':
        type = 'workload';
        break;
      case 'other':
        type = 'other';
        metric_tooltip = '-';
        break;
      default:
        type = 'unknown';
        break;
    }
    // 所有自定义告警的阈值单位都直接取unit字段
    if (metric_name === 'custom') {
      unit = item.unit;
    }
    // 组件的告警时，告警资源会写成[命名空间.组件名称]的形式
    if (namespace) {
      metric_tooltip = `${namespace}.${metric_tooltip}`;
    }
    return {
      namespace,
      ...item,
      display_name,
      metric_name,
      metric_tooltip,
      kind,
      type,
      unit,
      expr,
      query,
    };
  });
  if (search) {
    list = list.filter((item: Alarm) => {
      return item.name.includes(search.trim());
    });
  }
  return list;
}

export function parseLabels(item: AlarmDetail) {
  if (item.labels) {
    item.labelsParsed = Object.entries(item.labels).map(el => ({
      key: el[0],
      value: el[1],
    }));
  } else {
    item.labelsParsed = [];
  }
  if (item.annotations) {
    item.annotationsParsed = Object.entries(item.annotations).map(el => ({
      key: el[0],
      value: el[1],
    }));
  } else {
    item.annotationsParsed = [];
  }
  return item;
}

export function capitalizeFirstLetter(string: string) {
  return string.charAt(0).toUpperCase() + string.slice(1);
}

/**
 * 判断一个数组中的某个key的值是否重复
 * @param arr 一个对象数组
 * @param key 需要判断的key
 * @return 如重复，则返回重复的值，否则返回false
 */
export function hasDuplicateBy(arr: Array<any>, key: string) {
  const itemSeen = new Set();
  if (
    arr.some(item => {
      return itemSeen.size === itemSeen.add(item[key]).size;
    })
  ) {
    return itemSeen.values().next().value;
  } else {
    return false;
  }
}
