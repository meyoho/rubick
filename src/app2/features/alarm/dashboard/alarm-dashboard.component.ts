import { Component, OnInit } from '@angular/core';

import { get } from 'lodash-es';

import { IntegrationCenterService } from 'app/services/api/integration-center.service';
import { RegionService } from 'app/services/api/region.service';
import { Cluster } from 'app/services/api/region.service';

@Component({
  templateUrl: './alarm-dashboard.component.html',
  styleUrls: ['./alarm-dashboard.component.scss'],
})
export class AlarmDashboardComponent implements OnInit {
  loading = true;
  metricDocked: boolean;
  namespace: string;
  regionBadgeOption = {
    onChange: async (region: Cluster) => {
      const customized = get(region, 'features.customized');
      if (customized) {
        this.namespace = get(region, 'features.customized.metric.namespace');
        this.metricDocked = true;
      } else {
        if (!get(region, 'features.metric.type')) {
          this.metricDocked = false;
        } else {
          this.namespace = await this.getPrometheusNamespace(
            get(region, 'features.metric.integration_uuid'),
          );
          this.metricDocked = true;
        }
      }
      return true;
    },
  };

  constructor(
    private regionService: RegionService,
    private integrationCenterService: IntegrationCenterService,
  ) {}

  ngOnInit() {
    this.regionService
      .getCurrentRegion()
      .then(async res => {
        const customized = get(res, 'features.customized');
        if (customized) {
          this.namespace = get(res, 'features.customized.metric.namespace');
          this.metricDocked = true;
          return;
        }
        const type = get(res, 'features.metric.type');
        if (!type) {
          this.metricDocked = false;
        } else {
          this.namespace = await this.getPrometheusNamespace(
            get(res, 'features.metric.integration_uuid'),
          );
          this.metricDocked = true;
        }
      })
      .catch(() => {
        this.metricDocked = false;
      })
      .finally(() => {
        this.loading = false;
      });
  }

  private async getPrometheusNamespace(integrationUuid: string) {
    return await this.integrationCenterService
      .getIntegrationById(integrationUuid)
      .then(metric_setting => {
        return get(metric_setting, 'fields.namespace');
      });
  }
}
