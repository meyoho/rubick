import {
  DialogService,
  MessageService,
  NotificationService as AuiNotificationService,
} from '@alauda/ui';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as Highcharts from 'highcharts';
import { cloneDeep } from 'lodash-es';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { BaseTimeSelectComponent } from 'app/features-shared/alarm/base-time-select.component';
import {
  MetricNumericOptions,
  MetricPercentOptions,
} from 'app/features-shared/app/utils/monitor';
import { Alarm, AlarmService } from 'app/services/api/alarm.service';
import { LogService } from 'app/services/api/log.service';
import {
  IndicatorType,
  Metric,
  MetricService,
} from 'app/services/api/metric.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  getThreshold,
  getUnit,
  getUnitType,
  parseAlertList,
  parseChartOptions,
  parseLabels,
} from 'app2/features/alarm/alarm.util';
require('highcharts/modules/no-data-to-display')(Highcharts);

@Component({
  templateUrl: './template.html',
  styleUrls: ['styles.scss'],
})
export class PrometheusDetailComponent extends BaseTimeSelectComponent
  implements OnInit {
  loading = false;
  paramsSubscription: Subscription;
  cluster = '';
  namespace = '';
  resource = '';
  group = '';
  name = '';
  metric_type: IndicatorType[];
  alarm: Alarm;
  alarm_actions: any[] = [];
  canDelete = false;
  canUpdate = false;
  isDeleting = false;
  columns = ['action_type', 'name'];
  advanced_columns = ['key', 'value'];
  Highcharts = Highcharts;
  chartPercentOptions: Highcharts.Options = cloneDeep(MetricPercentOptions);
  chartNumericOptions: Highcharts.Options = cloneDeep(MetricNumericOptions);
  constructor(
    private cdr: ChangeDetectorRef,
    private route: ActivatedRoute,
    private alarmService: AlarmService,
    private dialogService: DialogService,
    private errorToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private metricService: MetricService,
    private auiNotificationService: AuiNotificationService,
    private translate: TranslateService,
    private router: Router,
    logService: LogService,
    auiMessageService: MessageService,
  ) {
    super(logService, auiMessageService, translate);
  }

  trackByFn(i: number) {
    return i;
  }

  async ngOnInit() {
    this.paramsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.cluster = params.cluster;
        this.resource = params.resource;
        this.group = params.group;
        this.namespace = params.namespace;
        this.name = params['name'];
        this.metric_type = await this.metricService.getIndicators(this.cluster);
        await this.getAlarm();
        parseChartOptions(this.chartPercentOptions);
        parseChartOptions(this.chartNumericOptions);
        const threshold = this.getThreshold(this.alarm);
        this.addPlotLines(this.chartPercentOptions, threshold);
        this.addPlotLines(this.chartNumericOptions, threshold);
        const unit = this.getUnit(this.alarm);
        this.chartNumericOptions.tooltip.valueSuffix = this.translate.get(unit);
        this.resetTimeRange();
        this.loadCharts();
      });
  }

  getUnit(item: Alarm) {
    return getUnit(item, this.metric_type);
  }

  getThreshold(item: Alarm) {
    return getThreshold(item, this.metric_type);
  }

  private getUnitType(metric: string) {
    return getUnitType(metric, this.metric_type);
  }

  private percentFlag() {
    if (this.alarm.metric_name === 'custom') {
      if (this.alarm.unit === '%') {
        return true;
      }
    } else if (this.getUnitType(this.alarm.metric_name) === '%') {
      return true;
    }
    return false;
  }

  async getAlarm() {
    this.loading = true;
    try {
      const alarm = await this.alarmService.getAlert(
        this.cluster,
        this.namespace,
        this.resource,
        this.group,
        this.name,
      );
      if (alarm.notifications) {
        this.alarm_actions.push({
          action_type: 'notification',
          notifications: alarm.notifications,
        });
      }
      if (alarm.scale_up) {
        this.alarm_actions.push({
          action_type: 'scaling',
          name: 'scaling_out',
        });
      }
      if (alarm.scale_down) {
        this.alarm_actions.push({
          action_type: 'scaling',
          name: 'scaling_in',
        });
      }
      this.alarm = parseLabels(
        parseAlertList([alarm], '', this.metric_type, this.cluster)[0],
      );
      [
        this.canUpdate,
        this.canDelete,
      ] = await this.roleUtil.resourceTypeSupportPermissions('alarm', {}, [
        'update',
        'delete',
      ]);
    } catch (err) {
      if (err.status === 404) {
        this.auiNotificationService.error(
          this.translate.get('alarm_not_exist'),
        );
      } else {
        this.errorToastService.error(err);
      }
      this.router.navigate(['/console/admin/alarm']);
    }
    this.loading = false;
  }

  update() {
    this.router.navigate([
      '/console/admin/alarm/update',
      this.cluster,
      this.namespace,
      this.resource,
      this.group,
      this.name,
    ]);
  }

  async delete() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: this.alarm.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch {
      return;
    }
    this.isDeleting = true;
    try {
      await this.alarmService.deleteAlert(
        this.cluster,
        this.namespace,
        this.resource,
        this.group,
        this.name,
      );
      this.auiNotificationService.success(this.translate.get('delete_success'));
      this.router.navigate(['/console/admin/alarm']);
    } catch ({ errors }) {}
  }

  loadCharts() {
    const args = {
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    };
    // http://jira.alaudatech.com/browse/DEV-12815
    let start = 0;
    if (args.end - args.start < 900) {
      this.step = 30;
      start = args.start;
    } else {
      this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
      start = args.start + this.step;
    }
    this.end_time = args.end;
    this.chartLoading = true;
    this.chartPercentOptions.series = [];
    this.chartNumericOptions.series = [];
    this.metricService
      .queryMetrics(this.cluster, {
        start,
        end: args.end,
        queries: this.alarm.metric.queries,
        step: this.step,
      })
      .then(
        result => {
          if (result) {
            if (this.percentFlag()) {
              this.chartPercentOptions['series'] = this.parseMetricsResponse(
                result,
                100,
              );
              this.chartNumericOptions.series = [];
            } else {
              this.chartNumericOptions['series'] = this.parseMetricsResponse(
                result,
              );
              this.chartPercentOptions.series = [];
            }
            this.chartLoading = false;
            this.cdr.markForCheck();
          }
        },
        error => {
          this.chartLoading = false;
          this.cdr.markForCheck();
          throw error;
        },
      );
  }

  private parseMetricsResponse(metric: Metric[], multiplier?: number) {
    return metric.map(el => {
      if (el.values.length < 30) {
        el.values = this.metricService.fillUpResult(
          el.values,
          this.end_time,
          this.step,
        );
      }
      let name = '';
      if (this.alarm.metric_name === 'custom') {
        if (Object.entries(el.metric).length) {
          name = Object.entries(el.metric)
            .map(el => `${el[0]}: ${el[1]}`)
            .join('<br>');
        }
      } else if (el.metric.instance) {
        name = el.metric.instance;
      } else {
        name = this.alarm.display_name;
      }
      return {
        name,
        data: el.values.map((value: Array<number | string>) => {
          let y = null;
          if (value[1] !== '+Inf' && value[1] !== '') {
            y = Number(value[1]);
            if (multiplier) {
              y = y * 100;
            }
          }
          return {
            x: Number(value[0]) * 1000,
            y,
          };
        }),
      };
    });
  }

  private addPlotLines(chartOption: any, value: number) {
    chartOption.yAxis['plotLines'] = [
      {
        color: '#ed615f',
        dashStyle: 'dash',
        width: 2,
        value,
        zIndex: 3,
      },
    ];
  }
}
