import { NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import * as Highcharts from 'highcharts';
import { debounceTime, filter, takeUntil } from 'rxjs/operators';

import { BaseAlarmFormComponent } from 'app/features-shared/alarm/base-alarm-form.component';
import { AlarmMetric, AlarmService } from 'app/services/api/alarm.service';
import { IndicatorType, MetricService } from 'app/services/api/metric.service';
import { RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { parseAlertList } from 'app2/features/alarm/alarm.util';
require('highcharts/modules/no-data-to-display')(Highcharts);

@Component({
  selector: 'rc-prometheus-alarm-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrometheusAlarmFormComponent extends BaseAlarmFormComponent
  implements OnInit, OnDestroy {
  metrics: IndicatorType[] = [];
  namespace: string;
  group_name: string;

  nodes: Array<{
    key: string;
  }> = [];
  clusterNodePremission: boolean;

  constructor(
    @Inject(ACCOUNT) public account: Account,
    alarmService: AlarmService,
    auiNotificationService: NotificationService,
    cdr: ChangeDetectorRef,
    fb: FormBuilder,
    metricService: MetricService,
    route: ActivatedRoute,
    translate: TranslateService,
    private errorsToastService: ErrorsToastService,
    private location: Location,
    private region: RegionService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
  ) {
    super(
      cdr,
      fb,
      metricService,
      translate,
      auiNotificationService,
      alarmService,
    );
    this.cluster_name = route.snapshot.params.cluster;
    this.resource_name = route.snapshot.params.resource;
    this.group_name = route.snapshot.params.group;
    this.namespace = route.snapshot.params.namespace;
    this.name = route.snapshot.params.name;
  }

  async ngOnInit() {
    super.ngOnInit();
    this.form
      .get('alarm_metric')
      .valueChanges.pipe(
        takeUntil(this.onDestroy$),
        debounceTime(500),
        filter(value => !!value),
      )
      .subscribe((alarm_metric: AlarmMetric) => {
        if (alarm_metric.unit) {
          this.unit_name = this.translate.get(alarm_metric.unit);
          this.chartNumericOptions.tooltip.valueSuffix = this.unit_name;
        } else {
          const unit = this.getUnitType(alarm_metric.metric_name);
          if (!unit) {
            this.unit_name = '';
            this.chartNumericOptions.tooltip.valueDecimals = 2;
            this.chartNumericOptions.tooltip.valueSuffix = '';
          } else {
            this.unit_name = this.translate.get(unit);
            this.chartNumericOptions.tooltip.valueDecimals = 4;
            this.chartNumericOptions.tooltip.valueSuffix = this.unit_name;
          }
        }
        if (
          !alarm_metric.metric_name &&
          (alarm_metric.expr === '' || alarm_metric.expr === undefined)
        ) {
          return;
        }
        super.loadChart(
          alarm_metric.resource_name,
          this.cluster_name,
          alarm_metric,
        );
      });
    this.form.patchValue({
      cluster: this.cluster_name,
    });
    const [metric_type, clusterNodePremission] = await Promise.all([
      this.metricService.getIndicators(this.cluster_name),
      this.roleUtil.resourceTypeSupportPermissions('cluster_node', {}, 'list'),
    ]);
    this.metric_type = metric_type;
    this.clusterNodePremission = clusterNodePremission;
    if (this.clusterNodePremission) {
      const { items } = await this.region.getClusterNodes(this.cluster_name);
      this.nodes = items.map(node => ({
        key: node.status.addresses.find(addr => addr.type === 'InternalIP')
          .address,
      }));
    }
    if (this.name) {
      this.formSubmitText = 'update';
      const alarm = await this.alarmService.getAlert(
        this.cluster_name,
        this.namespace,
        this.resource_name,
        this.group_name,
        this.name,
      );
      this.data = parseAlertList(
        [alarm],
        '',
        this.metric_type,
        this.cluster_name,
      )[0];
    }
    super.initViewModel();
    this.cdr.markForCheck();
    this.initialized = true;
  }

  async onSubmit() {
    this.alarmForm.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const body = super.generatePayload();
    if (this.data) {
      delete body.name;
      try {
        this.submitting = true;
        await this.alarmService.updateAlert(
          this.cluster_name,
          this.namespace,
          this.data.resource_name,
          this.data.group_name,
          this.data.name,
          body,
        );
        this.location.back();
        this.auiNotificationService.success(
          this.translate.get('alarm_update_success'),
        );
      } catch (rejection) {
        this.submitting = false;
        this.errorsToastService.error(rejection);
        this.cdr.markForCheck();
      }
    } else {
      try {
        this.submitting = true;
        const res = await this.alarmService.createAlert(
          this.cluster_name,
          body,
        );
        this.router.navigate([
          '/console/admin/alarm/detail',
          this.cluster_name,
          res['namespace'],
          res['resource_name'],
          res['group_name'],
          body.name,
        ]);
        this.auiNotificationService.success(
          this.translate.get('alarm_create_success'),
        );
      } catch (rejection) {
        this.submitting = false;
        this.errorsToastService.error(rejection);
        this.cdr.markForCheck();
      }
    }
  }

  cancel() {
    this.location.back();
  }
}
