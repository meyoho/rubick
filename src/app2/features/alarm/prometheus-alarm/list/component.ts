import {
  DialogService,
  DialogSize,
  NotificationService,
  PageEvent,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';

import { debounce } from 'lodash-es';
import {
  BehaviorSubject,
  combineLatest,
  from,
  Observable,
  of,
  Subject,
} from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  publishReplay,
  refCount,
  scan,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { Alarm, AlarmService } from 'app/services/api/alarm.service';
import { IndicatorType, MetricService } from 'app/services/api/metric.service';
import { RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { AlarmTemplateApplyComponent } from 'app2/features/alarm/alarm-template/apply/alarm-template-apply.component';
import { getRule, parseAlertList } from 'app2/features/alarm/alarm.util';
import { delay } from 'app2/utils';
import { ResourceList } from 'app_user/core/types';

@Component({
  selector: 'rc-prometheus-list',
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PrometheusAlarmComponent implements OnInit, OnDestroy {
  @Input()
  namespace: string;
  private onDestroy$ = new Subject<void>();
  search$ = new BehaviorSubject<string>(null);
  paginationEvent$ = new BehaviorSubject<Partial<PageEvent>>({});
  pagination$: Observable<PageEvent>;
  updated$ = new BehaviorSubject(null);
  list$: Observable<ResourceList>;
  filteredList$: Observable<Alarm[]>;
  indicators$: Observable<IndicatorType[]>;
  columns = [
    'name',
    'metric',
    'alarm_type',
    'threshold',
    'level',
    'status',
    'action',
  ];

  initialized: boolean;
  canCreateAlarm: boolean;
  canUpdateAlarm: boolean;
  canDeleteAlarm: boolean;
  clusterName: string;
  metric_type: IndicatorType[];
  fetching = false;
  searching = false;
  showZeroState = true;

  protected poll$ = new BehaviorSubject(null);
  pollInterval = 60000;
  protected debouncedUpdate: () => void;

  getRule = getRule;

  constructor(
    @Inject(ACCOUNT) public account: Account,
    public cdr: ChangeDetectorRef,
    private router: Router,
    private alarmService: AlarmService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private metricService: MetricService,
    private regionService: RegionService,
  ) {
    this.debouncedUpdate = debounce(() => {
      this.poll$.next(null);
    }, this.pollInterval);
  }

  map(value: any): any[] {
    return value.result;
  }

  onUpdate(item: any) {
    this.updated$.next(item);
  }

  async ngOnInit() {
    [
      this.canCreateAlarm,
      this.canUpdateAlarm,
      this.canDeleteAlarm,
    ] = await this.roleUtil.resourceTypeSupportPermissions('alarm', {}, [
      'create',
      'update',
      'delete',
    ]);

    this.pagination$ = this.paginationEvent$.pipe(
      scan((prev, changes) => ({ ...prev, ...changes }), {
        pageSize: 10,
        pageIndex: 0,
        previousPageIndex: 0,
        length: 0,
      }),
      publishReplay(1),
      refCount(),
    );

    this.list$ = combineLatest(
      this.regionService.region$.pipe(filter(region => !!region)),
      this.pagination$,
      this.search$.pipe(startWith('')),
      this.poll$,
      this.updated$,
    ).pipe(
      debounceTime(100),
      tap(() => {
        this.fetching = true;
        this.cdr.markForCheck();
      }),
      switchMap(([cluster, pagination, search]) => {
        return this.fetchResources(cluster.name, pagination, search).pipe(
          catchError(error => of(error)),
        );
      }),
      tap(result => {
        this.fetching = false;
        this.searching = false;
        if (!result.results) {
          this.showZeroState = true;
        } else {
          this.showZeroState = result.results.length === 0;
          if (result.results.length === 0) {
            if (result.num_pages) {
              this.pageChanged({ pageIndex: result.num_pages - 1 });
            }
          }
        }
        this.cdr.markForCheck();
        this.debouncedUpdate();
      }),
      publishReplay(1),
      refCount(),
    );

    this.indicators$ = this.regionService.region$.pipe(
      switchMap(cluster =>
        this.getIndicators(cluster.name).pipe(catchError(error => of(error))),
      ),
      publishReplay(1),
      refCount(),
    );

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe((list: ResourceList) => {
        if (list.results) {
          this.initialized = true;
        }
      });

    this.filteredList$ = combineLatest(this.list$, this.indicators$)
      .pipe(
        map(([list, metric_type]) => {
          this.metric_type = metric_type;
          return parseAlertList(
            list.results,
            null,
            metric_type,
            this.clusterName,
          );
        }),
      )
      .pipe(
        catchError(err => {
          throw err;
          return [];
        }),
      );

    this.poll$.next(null);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  fetchResources(
    clusterName: string,
    pagination: PageEvent,
    name: string,
  ): Observable<any> {
    this.clusterName = clusterName;
    const { pageIndex, pageSize } = pagination;
    return from(
      this.alarmService.getAlertsByLabelSelector(
        this.clusterName,
        'cluster',
        this.clusterName,
        this.namespace,
        {
          page: pageIndex + 1,
          page_size: pageSize,
          name,
        },
      ),
    );
  }

  getIndicators(clusterName: string): Observable<any> {
    return from(this.metricService.getIndicators(clusterName));
  }

  trackByFn(index: number) {
    return index;
  }

  createAlarm() {
    this.router.navigate([
      '/console/admin/alarm/create',
      this.clusterName,
      this.namespace,
    ]);
  }

  createAlarmTemplate() {
    const modelRef = this.dialogService.open(AlarmTemplateApplyComponent, {
      data: {
        metric_type: this.metric_type,
      },
      size: DialogSize.Large,
    });
    modelRef.componentInstance.close.subscribe(async () => {
      modelRef.close();
      await delay(500);
      this.onUpdate(null);
    });
  }

  async updateAlarm(alarm: any) {
    this.router.navigate([
      '/console/admin/alarm/update',
      this.clusterName,
      this.namespace,
      alarm.resource_name,
      alarm.group_name,
      alarm.name,
    ]);
  }

  async deleteAlarm(alarm: Alarm) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: alarm.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      await this.alarmService.deleteAlert(
        this.clusterName,
        this.namespace,
        alarm.resource_name,
        alarm.group_name,
        alarm.name,
      );
      this.auiNotificationService.success(
        this.translate.get('alarm_delete_success'),
      );
      await delay(500);
      this.onUpdate(null);
    } catch (rejection) {
      this.errorsToastService.error(rejection);
    }
  }

  onSearchChanged(search: string) {
    this.pageChanged({ pageIndex: 0 });
    this.search$.next(search);
  }

  pageChanged(pageEvent: Partial<PageEvent>) {
    this.paginationEvent$.next(pageEvent);
  }
}
