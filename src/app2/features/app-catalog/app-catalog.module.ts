import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';

import { SharedModule } from 'app/shared/shared.module';
import { AppCatalogRoutingModule } from 'app2/features/app-catalog/app-catalog-routing.module';
import { AppCatalogTemplateEmptyViewComponent } from 'app2/features/app-catalog/components/empty-view/app-catalog-template-empty-view.component';
import { AppCatalogTemplateRepoImportDialogComponent } from 'app2/features/app-catalog/components/import-dialog/app-catalog-template-repo-import-dialog.component';
import { AppCatalogTemplateRepositoryImportCircleComponent } from 'app2/features/app-catalog/components/import-progress/app-catalog-template-repo-import-circle.component';
import { AppCatalogTemplateRepoImportProgressComponent } from 'app2/features/app-catalog/components/import-progress/app-catalog-template-repo-import-progress.component';
import { AppCatalogTemplateListPageComponent } from 'app2/features/app-catalog/components/pages/app-catalog-template-list-page.component';
import { AppCatalogRepoListComponent } from 'app2/features/app-catalog/components/repository/app-catalog-repo-list.component';
import { AppCatalogTemplateCardComponent } from 'app2/features/app-catalog/components/template-card/app-catalog-template-card.component';
import { AppCatalogCatalogTemplateEffects } from 'app2/features/app-catalog/effects/catalog-template';

import { FEATURE_NAME, reducers } from './reducers';

@NgModule({
  imports: [
    SharedModule,
    AppCatalogRoutingModule,
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([AppCatalogCatalogTemplateEffects]),
  ],
  declarations: [
    AppCatalogTemplateListPageComponent,
    AppCatalogTemplateRepoImportProgressComponent,
    AppCatalogTemplateRepoImportDialogComponent,
    AppCatalogTemplateRepositoryImportCircleComponent,
    AppCatalogTemplateEmptyViewComponent,
    AppCatalogTemplateCardComponent,
    AppCatalogRepoListComponent,
  ],
  exports: [],
  entryComponents: [AppCatalogTemplateRepoImportDialogComponent],
})
export class AppCatalogModule {}
