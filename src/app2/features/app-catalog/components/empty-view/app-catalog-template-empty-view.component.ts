import { Component, Input, OnInit } from '@angular/core';

import { Store } from '@ngrx/store';

import {
  AppCatalogTemplateRepository,
  RepositoryStatus,
  RepositorySteps,
  VcsType,
} from 'app/services/api/app-catalog.service';
import { ImportDialogOpenAction } from 'app2/features/app-catalog/actions/catalog-template';
import * as fromAppCatalog from 'app2/features/app-catalog/reducers';

@Component({
  selector: 'rc-app-catalog-template-empty-view',
  templateUrl: 'app-catalog-template-empty-view.component.html',
  styleUrls: ['app-catalog-template-empty-view.component.scss'],
})
export class AppCatalogTemplateEmptyViewComponent implements OnInit {
  @Input()
  permission: boolean;
  newRepository: AppCatalogTemplateRepository;

  constructor(private store: Store<fromAppCatalog.State>) {}

  ngOnInit() {
    this.newRepository = {
      branch: '',
      has_template: false,
      name: '',
      display_name: '',
      namespace: '',
      path: '',
      resource_actions: [],
      status: RepositoryStatus.Success,
      step_at: RepositorySteps.Complete,
      templates: [],
      type: VcsType.Git,
      url: '',
      username: '',
      uuid: '',
    };
  }

  async importDialogOpen() {
    this.store.dispatch(new ImportDialogOpenAction(this.newRepository));
  }
}
