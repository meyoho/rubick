import { DIALOG_DATA, DialogRef, DialogService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { MessageService, NotificationService } from '@alauda/ui';
import {
  AppCatalogService,
  AppCatalogTemplateRepository,
  VcsType,
} from 'app/services/api/app-catalog.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';
import { RepoLoadAction } from 'app2/features/app-catalog/actions/catalog-template';
import * as fromAppCatalog from 'app2/features/app-catalog/reducers';

@Component({
  selector: 'rc-app-catalog-template-repo-import-dialog',
  templateUrl: 'app-catalog-template-repo-import-dialog.component.html',
  styleUrls: ['app-catalog-template-repo-import-dialog.component.scss'],
})
export class AppCatalogTemplateRepoImportDialogComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;
  errorState: boolean;
  submitting = false;
  repo_uuid: string;
  VcsType = VcsType;
  viewModel = {
    name: '',
    display_name: '',
    type: VcsType.Git,
    url: '',
    path: '',
    branch: '',
    username: '',
    password: '',
  };
  resourceNameReg = K8S_RESOURCE_NAME_BASE;

  constructor(
    private appCatalog: AppCatalogService,
    private translate: TranslateService,
    private dialogRef: DialogRef,
    private dialog: DialogService,
    private store: Store<fromAppCatalog.State>,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private router: Router,
    @Inject(DIALOG_DATA)
    public dialogData: {
      title: string;
      payload: AppCatalogTemplateRepository;
    },
  ) {}

  ngOnInit(): void {
    this.repo_uuid = this.dialogData.payload && this.dialogData.payload.uuid;
    if (this.isEditModel) {
      this.initViewModel();
    }
  }

  get isEditModel() {
    return !!this.dialogData.payload.name;
  }

  initViewModel() {
    const model = this.dialogData.payload;
    this.viewModel = {
      name: model.name,
      display_name: model.display_name,
      type: model.type,
      url: model.url,
      path: model.path,
      branch: model.branch || '',
      username: model.username || '',
      password: model.password || '',
    };
  }

  async cancel() {
    if (this.form.touched) {
      try {
        await this.dialog.confirm({
          title: this.translate.get('app_catalog_cancel_import_dialog_confirm'),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
        });
      } catch (e) {
        return false;
      }
    }
    this.dialogRef.close();
  }

  async submit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const model = this.form.value;

    if (this.dialogData.payload.uuid) {
      try {
        await this.dialog.confirm({
          title: this.translate.get('app_catalog_import_template_confirm'),
          content: this.translate.get(
            'app_catalog_import_template_confirm_content',
          ),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
        });
      } catch (e) {
        return false;
      }
    }

    this.submitting = true;
    if (this.repo_uuid) {
      try {
        const res = await this.appCatalog.importTemplateRepository(
          model,
          this.repo_uuid,
        );
        this.dialogRef.close(res);
        this.auiMessageService.success({
          content: this.translate.get(
            'app_catalog_update_repo_trigger_success',
            { repo_name: res.display_name ? res.display_name : res.name },
          ),
        });
        this.router.navigateByUrl(
          `/console/admin/app-catalog/repository/repo_detail/${res.uuid}`,
        );
        this.store.dispatch(new RepoLoadAction(this.repo_uuid));
      } catch (err) {
        if (err.errors && err.errors[0].code === 'Invalid_remote_code_repo') {
          const message = this.translate.get('app_catalog_cant_checkout_code');
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: message,
          });
        } else if (
          err.errors &&
          err.errors[0].code === 'Invalid_remote_code_repo_branch'
        ) {
          const message = this.translate.get('app_catalog_invalid_branch');
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: message,
          });
        } else {
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: err.errors[0].message,
          });
        }
      }
    } else {
      try {
        const res = await this.appCatalog.importTemplateRepository(model);
        this.dialogRef.close(res);
        this.auiMessageService.success({
          content: this.translate.get(
            'app_catalog_import_repo_trigger_success',
            { repo_name: res.display_name ? res.display_name : res.name },
          ),
        });
        this.store.dispatch(new RepoLoadAction(res.uuid));
        this.router.navigateByUrl(
          `/console/admin/app-catalog/repository/repo_detail/${res.uuid}`,
        );
      } catch (err) {
        if (err.errors && err.errors[0].code === 'Invalid_remote_code_repo') {
          const message = this.translate.get('app_catalog_cant_checkout_code');
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: message,
          });
        } else if (
          err.errors &&
          err.errors[0].code === 'Invalid_remote_code_repo_branch'
        ) {
          const message = this.translate.get('app_catalog_invalid_branch');
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: message,
          });
        } else {
          this.auiNotificationService.error({
            duration: 0,
            title: this.translate.get('app_catalog_failed_to_import'),
            content: err.errors[0].message,
          });
        }
      }
    }
    this.submitting = false;
  }
}
