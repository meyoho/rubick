import { DialogService } from '@alauda/ui';
import { animate, style, transition, trigger } from '@angular/animations';
import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';
import { Store } from '@ngrx/store';

import {
  AppCatalogService,
  AppCatalogTemplateRepository,
  RepositoryStatus,
  RepositorySteps,
} from 'app/services/api/app-catalog.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { RepoLoadAction } from 'app2/features/app-catalog/actions/catalog-template';
import { StepStatus } from 'app2/features/app-catalog/components/import-progress/app-catalog-template-repo-import-circle.component';
import { AppCatalogTemplateListPageComponent } from 'app2/features/app-catalog/components/pages/app-catalog-template-list-page.component';
import * as fromAppCatalog from 'app2/features/app-catalog/reducers';

interface StepInfo {
  label: string;
  status: StepStatus;
  step: RepositorySteps;
}

@Component({
  selector: 'rc-app-catalog-template-repo-import-progress',
  templateUrl: 'app-catalog-template-repo-import-progress.component.html',
  styleUrls: ['app-catalog-template-repo-import-progress.component.scss'],
  animations: [
    trigger('opacityInOut', [
      transition(':enter', [
        style({ opacity: '0' }),
        animate('.2s', style({ opacity: '1' })),
      ]),
      transition(':leave', animate('.2s', style({ opacity: '0' }))),
    ]),
  ],
})
export class AppCatalogTemplateRepoImportProgressComponent
  implements OnChanges {
  readonly RepositoryStatus = RepositoryStatus;
  @Input()
  repository: AppCatalogTemplateRepository;
  @Input()
  permission: boolean;
  @Input()
  loading: boolean;

  steps: StepInfo[] = [];
  currentStep: RepositorySteps = null;
  stepHintMessage = '';

  constructor(
    private translate: TranslateService,
    private listPage: AppCatalogTemplateListPageComponent,
    private dialogService: DialogService,
    private appCatalog: AppCatalogService,
    private store: Store<fromAppCatalog.State>,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnChanges({ repository }: SimpleChanges): void {
    if (repository && repository.currentValue) {
      const { steps, currentStep, message } = this.parseSteps(
        repository.currentValue,
      );
      this.steps = steps;
      this.currentStep = currentStep;
      this.stepHintMessage = message;
    }
  }

  circleTrackBy(index: number) {
    return index;
  }

  private parseSteps(
    repository: AppCatalogTemplateRepository,
  ): { steps: StepInfo[]; currentStep: RepositorySteps; message: string } {
    const step0: StepInfo = {
      label: this.translate.get('app_catalog_download_code'),
      status: StepStatus.InActive,
      step: RepositorySteps.DownloadCode,
    };
    const step1: StepInfo = {
      label: this.translate.get('app_catalog_parse_code'),
      status: StepStatus.InActive,
      step: RepositorySteps.ParseCode,
    };
    const step2: StepInfo = {
      label: this.translate.get('complete'),
      status: StepStatus.InActive,
      step: RepositorySteps.Complete,
    };

    const steps = [step0, step1, step2];

    function mapRepoStatusToStepStatus(
      repoStatus: RepositoryStatus,
    ): StepStatus {
      if (repoStatus === RepositoryStatus.Fail) {
        return StepStatus.Failed;
      } else if (repoStatus === RepositoryStatus.InProgress) {
        return StepStatus.InProgress;
      } else {
        return StepStatus.Success;
      }
    }

    const currentIndex = steps.findIndex(
      step => step.step === repository.step_at,
    );
    const currentStep = steps[currentIndex].step;
    steps
      .filter((_, i) => i < currentIndex)
      .forEach(step => (step.status = StepStatus.Success));
    steps[currentIndex].status = mapRepoStatusToStepStatus(repository.status);

    let message = '';
    if (repository.status === RepositoryStatus.InProgress) {
      message = this.translate.get('app_catalog_import_in_progress_hint');
    } else if (repository.status === RepositoryStatus.Success) {
      message = this.translate.get('app_catalog_import_success_hint');
    } else {
      const errorMessageCodes = {
        [RepositorySteps.DownloadCode]:
          'app_catalog_import_error_download_code_hint',
        [RepositorySteps.ParseCode]: 'app_catalog_import_error_parse_code_hint',
        [RepositorySteps.Complete]: 'app_catalog_import_error_general_hint',
      };
      message = this.translate.get(errorMessageCodes[currentStep]);
    }
    return { steps, currentStep, message };
  }

  retryClicked() {
    this.listPage.syncTemplate(true);
  }

  async cancelClicked() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('app_catalog_import_cancel_confirm', {
          repo_name: this.repository.display_name
            ? this.repository.display_name
            : this.repository.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return false;
    }

    try {
      await this.appCatalog.cancelImportRepository(this.repository.uuid);
      this.store.dispatch(new RepoLoadAction(this.repository.uuid));
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }
}
