import { DialogService } from '@alauda/ui';
import { MessageService, NotificationService } from '@alauda/ui';
import {
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { debounce, pickBy, values } from 'lodash-es';
import { Observable, Subscription } from 'rxjs';
import { distinctUntilChanged, filter, first, map } from 'rxjs/operators';

import {
  AppCatalogService,
  AppCatalogTemplate,
  AppCatalogTemplateRepository,
  RepositoryStatus,
} from 'app/services/api/app-catalog.service';
import { RoleService } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  ImportDialogOpenAction,
  ListPageModeChangeAction,
  ListPageViewMode,
  RepoLoadAction,
  RepoRefreshAction,
} from 'app2/features/app-catalog/actions/catalog-template';
import * as fromAppCatalog from 'app2/features/app-catalog/reducers';
import * as catalogTemplate from 'app2/features/app-catalog/reducers/catalog-template';

@Component({
  templateUrl: 'app-catalog-template-list-page.component.html',
  styleUrls: ['app-catalog-template-list-page.component.scss'],
})
export class AppCatalogTemplateListPageComponent implements OnInit, OnDestroy {
  static readonly POLLING_INTERVAL = 3000;
  readonly ListPageViewMode = ListPageViewMode;
  pageState$: Observable<catalogTemplate.TemplateListState>;
  repoLoading$: Observable<boolean>;
  repository$: Observable<AppCatalogTemplateRepository>;
  templates$: Observable<AppCatalogTemplate[]>;
  selectedTemplate$: Observable<AppCatalogTemplate>;
  pageLoading$: Observable<boolean>;
  allAppsNumber$: Observable<number>;
  selectedTemplateName$: Observable<string>;
  searchQuery$: Observable<string>;
  refreshLoading$: Observable<boolean>;
  permission: boolean;
  applicationPermission = false;
  templateLoading = false;
  permissionLoading = false;
  selectedTemplateIndex = -1;
  routeParamsSubscription: Subscription;
  uuid: string;
  pageMode: string;
  templates: AppCatalogTemplate[];
  displayTemplates: AppCatalogTemplate[];

  @ViewChild('retryTemplate')
  private retryTemplate: TemplateRef<any>;
  @ViewChild('dialogTemplate')
  private dialogTemplate: TemplateRef<any>;

  private repoSub: Subscription;
  private storeSub: Subscription;
  repository: AppCatalogTemplateRepository;
  private pollTimer: any;

  private changeRouteParams = debounce(
    (
      mode: ListPageViewMode,
      templateId: string = '',
      searchQuery: string = '',
    ) => {
      const queryParams = {
        mode,
        template_id: templateId,
        search: searchQuery,
      };
      this.router.navigate([], {
        replaceUrl: true,
        queryParams: pickBy(queryParams, value => !!value),
      });
    },
  );

  constructor(
    private store: Store<fromAppCatalog.State>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private translate: TranslateService,
    private dialog: DialogService,
    private roleService: RoleService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private appCatalog: AppCatalogService,
    private errorsToastService: ErrorsToastService,
    private route: ActivatedRoute,
  ) {}

  async ngOnInit() {
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.uuid = params['uuid'];
      });

    this.permissionLoading = true;
    this.roleService
      .getPluralContextPermissions(['helm_template_repo', 'application'])
      .then(res => {
        this.permission =
          res.helm_template_repo.indexOf('helm_template_repo:manage') >= 0;
        this.applicationPermission =
          res.application.indexOf('application:create') >= 0;
      })
      .catch(err => {
        this.permission = false;
        this.errorsToastService.error(err);
      })
      .then(() => {
        this.permissionLoading = false;
      });

    this.refetchRepositoryData();
    this.refetchTemplateData();

    // Stores:
    this.pageState$ = this.store.select(
      fromAppCatalog.getTemplateListPageState,
    );
    this.repoLoading$ = this.store.select(
      fromAppCatalog.getTemplateListLoadingState,
    );
    this.repository$ = this.store.select(fromAppCatalog.getRepositoryState);
    this.templates$ = this.store.select(fromAppCatalog.getTemplatesState);

    this.pageLoading$ = this.pageState$.pipe(
      map(state => state.loading && !state.repository),
    );

    this.refreshLoading$ = this.store
      .select(fromAppCatalog.getRefreshLoadingState)
      .pipe(map(state => state));

    this.selectedTemplate$ = this.pageState$.pipe(
      map(state => {
        if (state.repository && state.selectedTemplateId) {
          return state.repository.templates.find(
            template => template.uuid === state.selectedTemplateId,
          );
        }
      }),
    );

    this.selectedTemplateName$ = this.selectedTemplate$.pipe(
      map(template => {
        return template
          ? template.display_name
          : this.translate.get('app_catalog_all_template_apps');
      }),
    );

    this.searchQuery$ = this.pageState$.pipe(map(state => state.searchQuery));

    this.allAppsNumber$ = this.pageState$.pipe(
      map(state => {
        if (state.repository) {
          return state.repository.templates.reduce(
            (accum, template) => accum + template.installed_app_num,
            0,
          );
        } else {
          return 0;
        }
      }),
    );

    this.repoSub = this.repository$
      .pipe(filter(repository => !!repository))
      .subscribe(repository => {
        this.repository = repository;
        if (repository.status === RepositoryStatus.InProgress) {
          this.startPolling();
        } else {
          this.stopPolling();
        }
      });

    this.storeSub = this.pageState$
      .pipe(distinctUntilChanged())
      .subscribe(state => {
        if (this.pageMode === 'IMPORT' && state.mode === 'TEMPLATES') {
          this.refetchTemplateData();
        } else if (state.mode === 'IMPORT') {
          this.pageMode = state.mode;
        } else if (state.mode === 'TEMPLATES') {
          this.pageMode = 'TEMPLATES';
        }
        this.checkRepoStateAndNavigate(state);
      });

    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.handleQueryParamsChange(queryParams as { mode: ListPageViewMode });
      if (queryParams.import_dialog) {
        this.store.dispatch(new ImportDialogOpenAction(this.repository));
      }
    });
  }

  ngOnDestroy(): void {
    this.storeSub.unsubscribe();
    this.repoSub.unsubscribe();
    this.routeParamsSubscription.unsubscribe();
    this.stopPolling();
  }

  refetchRepositoryData() {
    this.store.dispatch(new RepoLoadAction(this.uuid));
  }

  async importDialogOpen() {
    this.store.dispatch(new ImportDialogOpenAction(this.repository));
  }

  async refetchTemplateData() {
    this.templateLoading = true;
    try {
      const res = await this.appCatalog.getTemplates({
        pageNo: 1,
        pageSize: 999,
        params: {
          repository_uuid: this.uuid,
        },
      });
      this.templates = res.results;
      this.displayTemplates = res.results;
    } catch (errors) {
      this.templates = [];
      this.displayTemplates = [];
    }
    this.templateLoading = false;
  }

  async syncTemplate(isRetry = false) {
    if (!isRetry) {
      try {
        await this.dialog.confirm({
          title: this.translate.get('app_catalog_sync_template_confirm'),
          content: this.translate.get(
            'app_catalog_sync_template_confirm_content',
          ),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
        });
      } catch (e) {
        return false;
      }
    }

    try {
      this.store.dispatch(
        new RepoRefreshAction(this.repository.uuid, error => {
          if (
            error.errors &&
            error.errors[0].code === 'Invalid_remote_code_repo'
          ) {
            const message = this.translate.get(
              'app_catalog_cant_checkout_code',
            );
            this.auiNotificationService.error({
              contentRef: this.dialogTemplate,
              duration: 0,
              title: this.translate.get('app_catalog_failed_to_sync'),
              context: {
                message: message,
              },
            });
          } else if (
            error.errors &&
            error.errors[0].code === 'Invalid_remote_code_repo_branch'
          ) {
            const message = this.translate.get('app_catalog_invalid_branch');
            this.auiNotificationService.error({
              contentRef: this.dialogTemplate,
              duration: 0,
              title: this.translate.get('app_catalog_failed_to_sync'),
              context: {
                message: message,
              },
            });
          } else {
            const message = this.translate.get('app_catalog_sync_failed');
            this.auiNotificationService.error({
              contentRef: this.retryTemplate,
              duration: 0,
              title: this.translate.get('app_catalog_failed_to_sync'),
              context: {
                message: message,
              },
            });
          }
        }),
      );
    } catch (err) {}
  }

  createAppClicked(template: AppCatalogTemplate) {
    this.router.navigate([
      `/console/admin/app-catalog/repository/app-create/${template.uuid}`,
    ]);
  }

  /**
   * Check the given state and navigate route if necessary.
   */
  private checkRepoStateAndNavigate(state: catalogTemplate.TemplateListState) {
    if (state.mode === ListPageViewMode.Templates) {
      // Load failed / empty repository, go to empty view
      if (
        state.repository &&
        state.repository.status !== RepositoryStatus.Success
      ) {
        this.changeRouteParams(ListPageViewMode.Import, '');
      }
    }

    if (state.repository && ListPageViewMode.Import === state.mode) {
      if (state.repository.status === RepositoryStatus.Success) {
        this.changeRouteParams(ListPageViewMode.Templates, '');
      }
    }
  }

  private async handleQueryParamsChange({ mode }: { mode: ListPageViewMode }) {
    const state = await this.pageState$.pipe(first()).toPromise();

    this.checkRepoStateAndNavigate(state);

    if (!values(ListPageViewMode).includes(mode)) {
      return this.changeRouteParams(ListPageViewMode.Templates);
    }

    if (mode !== state.mode) {
      this.store.dispatch(new ListPageModeChangeAction(mode));
    }
  }

  private startPolling() {
    if (!this.pollTimer) {
      this.pollTimer = setInterval(() => {
        if (this.pollTimer) {
          this.refetchRepositoryData();
        }
      }, AppCatalogTemplateListPageComponent.POLLING_INTERVAL);
    }
  }

  private stopPolling() {
    if (this.pollTimer) {
      clearInterval(this.pollTimer);
      this.pollTimer = null;
    }
  }

  searchChanged(keyWord: string) {
    this.displayTemplates = this.templates.filter(({ name, display_name }) => {
      const keyWordRegExp = new RegExp(keyWord, 'i');

      return keyWordRegExp.test(name) || keyWordRegExp.test(display_name);
    });
  }

  async deleteRepo() {
    try {
      await this.dialog.confirm({
        title: this.translate.get('app_catalog_delete_repo_title', {
          repo_name: this.repository.display_name
            ? this.repository.display_name
            : this.repository.name,
        }),
        content: this.translate.get('app_catalog_delete_repo_content'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (error) {
      return false;
    }
    try {
      await this.appCatalog.deleteRepository(this.repository.uuid);
      this.auiMessageService.success({
        content: this.translate.get('app_catalog_delete_repo_trigger_success', {
          repo_name: this.repository.display_name
            ? this.repository.display_name
            : this.repository.name,
        }),
      });
      this.router.navigateByUrl(
        `/console/admin/app-catalog/repository/repo_list`,
      );
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }
}
