import { DialogService } from '@alauda/ui';
import { MessageService, NotificationService, PageEvent } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { some } from 'lodash-es';
import { Observable, Subscription } from 'rxjs';
import { map } from 'rxjs/operators';

import {
  Pagination,
  PaginationDataWrapper,
} from 'app/abstract/pagination-data';
import {
  AppCatalogService,
  AppCatalogTemplateRepository,
  RepositoryStatus,
  RepositorySteps,
  VcsType,
} from 'app/services/api/app-catalog.service';
import { RoleService } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  ImportDialogOpenAction,
  RepoRefreshAction,
} from 'app2/features/app-catalog/actions/catalog-template';
import * as fromAppCatalog from 'app2/features/app-catalog/reducers';

@Component({
  templateUrl: './app-catalog-repo-list.component.html',
  styleUrls: ['./app-catalog-repo-list.component.scss'],
})
export class AppCatalogRepoListComponent implements OnInit, OnDestroy {
  refreshLoading$: Observable<boolean>;
  showNotification = true;
  paginationDataWrapper: PaginationDataWrapper<any>;
  paginationDataSubscription: Subscription;
  refreshSubscription: Subscription;
  errorSubscription: Subscription;
  appCreateEnabled: boolean;
  pageSize = 20;
  count = 0;
  templateRepositoryItems: Array<any> = [];
  permission = false;
  initialized = false;
  actionLoading = {
    uuid: '',
    loading: false,
  };
  queryString = '';
  searching = false;
  private pollingTimer: any = null;
  private destroyed = false;
  syncRepository: AppCatalogTemplateRepository;
  newRepository: AppCatalogTemplateRepository;

  @ViewChild('retryTemplate')
  private retryTemplate: TemplateRef<any>;
  @ViewChild('dialogTemplate')
  private dialogTemplate: TemplateRef<any>;

  constructor(
    private store: Store<fromAppCatalog.State>,
    private appCatalog: AppCatalogService,
    private changeDetectorRef: ChangeDetectorRef,
    private router: Router,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private roleService: RoleService,
    private errorsToastService: ErrorsToastService,
  ) {
    const fetchRequest = (pageNo: number, pageSize: number, params: any) => {
      return this.appCatalog.getTemplateRepositoryList({
        pageNo,
        pageSize,
        params,
      });
    };
    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchRequest,
      this.pageSize,
    );
  }

  ngOnInit() {
    this.newRepository = {
      branch: '',
      has_template: false,
      name: '',
      display_name: '',
      namespace: '',
      path: '',
      resource_actions: [],
      status: RepositoryStatus.Success,
      step_at: RepositorySteps.Complete,
      templates: [],
      type: VcsType.Git,
      url: '',
      username: '',
      uuid: '',
    };
    this.roleService
      .getPluralContextPermissions(['helm_template_repo'])
      .then(res => {
        this.permission =
          res.helm_template_repo.indexOf('helm_template_repo:manage') >= 0;
      })
      .catch(err => {
        this.permission = false;
        this.errorsToastService.error(err);
      });
    this.refreshLoading$ = this.store
      .select(fromAppCatalog.getRefreshLoadingState)
      .pipe(map(state => state));
    this.refreshSubscription = this.refreshLoading$.subscribe(loading => {
      this.actionLoading.loading = loading;
      if (!loading && this.initialized) {
        this.refetch();
        this.actionLoading.uuid = '';
      }
    });
    this.errorSubscription = this.paginationDataWrapper.loadError.subscribe(
      err => {
        this.errorsToastService.error(err);
      },
    );
    this.paginationDataSubscription = this.paginationDataWrapper.pagination.subscribe(
      (paginationData: Pagination<any>) => {
        this.searching = false;
        if (!paginationData.loadError) {
          this.count = paginationData.count;
          this.templateRepositoryItems = paginationData.results;
          this.initialized = true;
          if (!this.destroyed) {
            this.changeDetectorRef.detectChanges();
          }
        }
        this.resetPollingTimer();
      },
    );
    this.refetch();
  }

  ngOnDestroy() {
    this.destroyed = true;
    if (this.paginationDataSubscription) {
      this.paginationDataSubscription.unsubscribe();
    }
    if (this.refreshSubscription) {
      this.refreshSubscription.unsubscribe();
    }
    if (this.errorSubscription) {
      this.errorSubscription.unsubscribe();
    }
    this.auiNotificationService.removeAll();
    clearTimeout(this.pollingTimer);
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  get empty() {
    return (
      !this.templateRepositoryItems || !this.templateRepositoryItems.length
    );
  }

  get currentPage() {
    return this.paginationDataWrapper.pageNo;
  }

  refetch() {
    return this.paginationDataWrapper.refetch();
  }

  searchChanged(queryString: string) {
    this.searching = true;
    this.queryString = queryString;
    this.paginationDataWrapper.setParams({
      name: queryString,
    });
  }

  pageChanged(PageEvent: Partial<PageEvent>) {
    const { pageIndex, pageSize } = PageEvent;
    this.paginationDataWrapper.refetch(pageIndex + 1, {}, pageSize);
    this.pageSize = pageSize;
  }

  viewRepo(uuid: string) {
    this.router.navigateByUrl(
      `/console/admin/app-catalog/repository/repo_detail/${uuid}`,
    );
  }

  closeNoti() {
    this.showNotification = false;
  }

  showNoti() {
    this.showNotification = true;
  }

  async syncTemplate(repo: AppCatalogTemplateRepository) {
    this.syncRepository = repo;
    try {
      await this.dialogService.confirm({
        title: this.translate.get('app_catalog_sync_template_confirm'),
        content: this.translate.get(
          'app_catalog_sync_template_confirm_content',
        ),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return false;
    }

    this.auiNotificationService.removeAll();
    this.actionLoading.uuid = repo.uuid;
    try {
      this.store.dispatch(
        new RepoRefreshAction(repo.uuid, error => {
          if (
            error.errors &&
            error.errors[0].code === 'Invalid_remote_code_repo'
          ) {
            const message = this.translate.get(
              'app_catalog_cant_checkout_code',
            );
            this.auiNotificationService.error({
              contentRef: this.dialogTemplate,
              duration: 0,
              title: this.translate.get('app_catalog_failed_to_sync'),
              context: {
                message: message,
              },
            });
          } else if (
            error.errors &&
            error.errors[0].code === 'Invalid_remote_code_repo_branch'
          ) {
            const message = this.translate.get('app_catalog_invalid_branch');
            this.auiNotificationService.error({
              contentRef: this.dialogTemplate,
              duration: 0,
              title: this.translate.get('app_catalog_failed_to_sync'),
              context: {
                message: message,
              },
            });
          } else {
            const message = this.translate.get('app_catalog_sync_failed');
            this.auiNotificationService.error({
              contentRef: this.retryTemplate,
              duration: 0,
              title: this.translate.get('app_catalog_failed_to_sync'),
              context: {
                message: message,
              },
            });
          }
        }),
      );
    } catch (err) {}
  }

  createRepo() {
    this.store.dispatch(new ImportDialogOpenAction(this.newRepository));
  }

  updateRepo(repo: AppCatalogTemplateRepository) {
    this.store.dispatch(new ImportDialogOpenAction(repo));
  }

  async deleteRepo(repo: AppCatalogTemplateRepository) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('app_catalog_delete_repo_title', {
          repo_name: repo.display_name ? repo.display_name : repo.name,
        }),
        content: this.translate.get('app_catalog_delete_repo_content'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (error) {
      return false;
    }
    this.actionLoading.loading = true;
    this.actionLoading.uuid = repo.uuid;
    try {
      await this.appCatalog.deleteRepository(repo.uuid);
      this.auiMessageService.success({
        content: this.translate.get('app_catalog_delete_repo_trigger_success', {
          repo_name: repo.display_name ? repo.display_name : repo.name,
        }),
      });
      this.refetch();
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.actionLoading.loading = false;
    this.actionLoading.uuid = '';
  }

  trackByFn(index: number) {
    return index;
  }

  /**
   * paginationData 返回后重置下次轮询时间 若有同步中的模板仓库，等待时间为3s
   */
  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.destroyed) {
      return;
    }
    const hasDeployingApps = some(
      this.templateRepositoryItems,
      (repo: AppCatalogTemplateRepository) => {
        return repo.status.toLowerCase() === 'in_progress';
      },
    );
    const waitTime = hasDeployingApps ? 3000 : 10000;
    this.pollingTimer = setTimeout(() => {
      this.refetch();
    }, waitTime);
  }
}
