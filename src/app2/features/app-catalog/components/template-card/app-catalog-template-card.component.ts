import { Component, Input, OnInit } from '@angular/core';

import { AppCatalogTemplate } from 'app/services/api/app-catalog.service';
import { TranslateService } from 'app/translate/translate.service';
import { PlatFormUtilService } from 'app_user/features/app-platform/platform-util.service';

@Component({
  selector: 'rc-app-catalog-template-card',
  templateUrl: 'app-catalog-template-card.component.html',
  styleUrls: ['app-catalog-template-card.component.scss'],
})
export class AppCatalogTemplateCardComponent implements OnInit {
  @Input()
  template: AppCatalogTemplate;
  @Input()
  showRepoName = false;
  @Input()
  applicationPermission: boolean;

  warm_tips = '';

  constructor(
    private translate: TranslateService,
    public platformService: PlatFormUtilService,
  ) {}

  ngOnInit() {
    if (this.template.resource_actions.indexOf('helm_template:use') < 0) {
      this.warm_tips = this.translate.get(
        'app_catalog_no_permission_create_app',
      );
    }
  }
}
