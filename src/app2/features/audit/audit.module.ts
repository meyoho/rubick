import { NgModule } from '@angular/core';
import { AuditSharedModule } from 'app/features-shared/audit/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { AuditRoutingModule } from 'app2/features/audit/audit.routing.module';

@NgModule({
  imports: [AuditSharedModule, SharedModule, AuditRoutingModule],
})
export class AuditModule {}
