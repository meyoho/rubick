import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import moment from 'moment';

import { AccountService } from 'app/services/api/account.service';
import { Audit, AuditService } from 'app/services/api/audit.service';
import { LogService } from 'app/services/api/log.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { viewActions } from 'app/utils/code-editor-config';
import {
  LOGLEVELS,
  NAMESPACE_RESOURCE_TYPES,
  OPERATIONTYPES,
  PROJECT_RESOURCE_TYPES,
  RESOURCE_TYPES,
} from 'app2/features/audit/audit.constant';

@Component({
  selector: 'rc-audit-list',
  templateUrl: 'audit-list.component.html',
  styleUrls: ['audit-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AuditListComponent implements OnInit, OnDestroy {
  @ViewChild(NgForm)
  advancedForm: NgForm;

  @Input()
  k8sNamespaceName: string;

  @Input()
  projectName: string;

  @Input()
  clusterName: string;
  isUserView = false;
  current_time = moment();
  start_time = this.current_time.clone().subtract(1, 'hours');
  queryDates = {
    start_time: this.start_time.valueOf(),
    end_time: this.current_time.valueOf(),
  };
  queryDatesShown = {
    start_time: this.logService.dateNumToStr(this.queryDates.start_time),
    end_time: this.logService.dateNumToStr(this.queryDates.end_time),
  };
  dateTimeOptions = {
    maxDate: this.current_time
      .clone()
      .endOf('day')
      .valueOf(),
    minDate: this.current_time
      .clone()
      .subtract(89, 'days')
      .startOf('day')
      .valueOf(),
    enableTime: true,
    enableSeconds: true,
    time_24hr: true,
  };
  operationTypes = OPERATIONTYPES;
  resourceTypes = RESOURCE_TYPES;
  resourceTypesAll = '';
  logLevels = LOGLEVELS;

  usernames: Array<string> = [];
  resourceNames: Array<string> = [];
  userName = '_audit_all';
  operationType = '_audit_all';
  resourceName = '_audit_all';
  resourceType = '';
  logLevel = '_audit_all';
  audits: Audit[] = [];
  actionsConfigView = viewActions;
  audit_detail = {};
  expandedAudits: any = {};
  pagination = {
    count: 0,
    size: 20,
    current: 1,
    pages: 0,
  };
  initialized: boolean;
  loading: boolean;
  advanced_search = false;
  keyword: string;
  private intervalTimer: number;

  constructor(
    public cdr: ChangeDetectorRef,
    private logService: LogService,
    private auiMessageService: MessageService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private auditService: AuditService,
    private accountService: AccountService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  async ngOnInit() {
    this.initTimer();
    this.isUserView = this.accountService.isUserView();
    if (this.k8sNamespaceName) {
      this.resourceTypes = NAMESPACE_RESOURCE_TYPES;
    } else if (this.projectName) {
      this.resourceTypes = PROJECT_RESOURCE_TYPES;
    }
    this.resourceTypesAll = this.resourceTypes.join(',');
    this.resourceType = this.resourceTypesAll;
    await this.fetchAuditTypes();
    await this.fetchAudits();
    this.initialized = true;
  }

  ngOnDestroy() {
    this.clearTimeout();
  }

  initTimer() {
    this.intervalTimer = window.setInterval(() => {
      if (this.advancedForm && this.advancedForm.dirty) {
        this.clearTimeout();
        return;
      }
      const current_time = moment();
      const start_time = moment()
        .clone()
        .subtract(1, 'hours');
      this.queryDates = {
        start_time: start_time.valueOf(),
        end_time: current_time.valueOf(),
      };
      this.fillCalendar(this.queryDates.start_time, this.queryDates.end_time);
      this.refetch();
    }, 60000);
  }

  clearTimeout() {
    window.clearInterval(this.intervalTimer);
  }

  currentPageChange(_currentPage: number) {
    this.clearTimeout();
    this.fetchAudits(this.keyword);
  }

  pageSizeChange(size: number) {
    this.clearTimeout();
    if (Math.ceil(this.pagination.count / size) < this.pagination.pages) {
      this.pagination.current = 1;
    }
    this.fetchAudits(this.keyword);
  }

  onSelect() {
    if (this.checkTimeRange()) {
      this.clearTimeout();
      this.pagination.current = 1;
      this.fetchAudits();
    }
  }

  fastSearch(fast_search: string) {
    if (this.checkTimeRange()) {
      this.clearTimeout();
      this.fetchAudits(fast_search);
    }
  }

  async fetchAuditTypes() {
    try {
      const params = {
        start_time: this.queryDates.start_time / 1000,
        end_time: this.queryDates.end_time / 1000,
        k8s_namespace_name: this.k8sNamespaceName,
        resource_type: this.resourceType,
      };
      if (this.projectName && !this.isUserView) {
        params['project_name'] = this.projectName;
      }
      if (this.clusterName) {
        params['cluster_name'] = this.clusterName;
      }
      const [usernames, resourceNames] = await Promise.all([
        this.auditService.getAuditTypes({
          field: 'username',
          ...params,
        }),
        this.auditService.getAuditTypes({
          field: 'resource_name',
          ...params,
        }),
      ]);
      this.usernames = usernames;
      this.resourceNames = resourceNames;
    } catch (error) {
      this.usernames = [];
      this.resourceNames = [];
    }
  }

  async fetchAudits(fast_search?: string) {
    this.loading = true;
    try {
      const query_params = {
        start_time: this.queryDates.start_time / 1000,
        end_time: this.queryDates.end_time / 1000,
        k8s_namespace_name: this.k8sNamespaceName,
        page: this.pagination.current,
        page_size: this.pagination.size,
      };
      if (!this.advanced_search) {
        query_params['fast_search'] = fast_search;
        if (this.isUserView) {
          query_params['resource_type'] = this.resourceType;
        }
      } else {
        query_params['user_name'] = this.getParam(this.userName);
        query_params['operation_type'] = this.getParam(this.operationType);
        query_params['resource_name'] = this.getParam(this.resourceName);
        query_params['resource_type'] = this.resourceType;
        query_params['log_level'] = this.getParam(this.logLevel);
      }
      if (this.projectName && !this.isUserView) {
        query_params['project_name'] = this.projectName;
      }
      if (this.clusterName) {
        query_params['cluster_name'] = this.clusterName;
      }
      const audits = await this.auditService.getAudits(query_params);
      this.audits = audits.results;
      this.pagination.count =
        audits.total_items > 10000 ? 10000 : audits.total_items;
      this.pagination.pages = Math.ceil(
        this.pagination.count / this.pagination.size,
      );
    } catch (error) {
      this.audits = [];
    } finally {
      this.loading = false;
      this.cdr.markForCheck();
    }
  }

  showDetailTemplate(audit: Audit, template: TemplateRef<any>) {
    try {
      audit.request_body = JSON.parse(audit.request_body);
      audit.response_body = JSON.parse(audit.response_body);
    } catch (err) {}
    this.audit_detail = audit;
    this.dialogService.open(template, {
      size: DialogSize.Big,
    });
  }

  private fillCalendar(start_time: number, end_time: number) {
    this.queryDatesShown.start_time = this.logService.dateNumToStr(start_time);
    this.queryDatesShown.end_time = this.logService.dateNumToStr(end_time);
  }

  async onStartTimeSelect(event: any) {
    this.clearTimeout();
    this.queryDates.start_time = this.logService.dateStrToNum(event);
    await this.refetch();
  }

  async onEndTimeSelect(event: any) {
    this.clearTimeout();
    this.queryDates.end_time = this.logService.dateStrToNum(event);
    await this.refetch();
  }

  async refetch() {
    if (this.checkTimeRange()) {
      this.pagination.current = 1;
      await this.fetchAuditTypes();
      await this.fetchAudits(this.keyword);
    }
  }

  checkTimeRange() {
    if (this.queryDates.end_time <= this.queryDates.start_time) {
      this.auiMessageService.warning({
        content: this.translateService.get('metric_timerange_invalid'),
      });
      return false;
    }
    return true;
  }

  toggleSearch() {
    if (this.advanced_search) {
      this.resetAdvancedSearch();
    } else {
      this.keyword = '';
    }
    this.advanced_search = !this.advanced_search;
  }

  resetAdvancedSearch() {
    this.userName = '_audit_all';
    this.operationType = '_audit_all';
    this.resourceName = '_audit_all';
    this.resourceType = this.resourceTypesAll;
    this.logLevel = '_audit_all';
  }

  private getParam(param: string) {
    return param === '_audit_all' ? '' : param;
  }

  getResourceTypeTranslateKey(resourceType: string) {
    return resourceType === 'service' ? 'kube_service' : resourceType;
  }

  getOperationTypeTranslateKey(operationType: string) {
    return operationType === 'login' ? 'audit_login' : operationType;
  }

  clickRow(alarm: any) {
    this.expandedAudits[alarm.event_id] = !this.expandedAudits[alarm.event_id];
  }
}
