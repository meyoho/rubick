import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AuthRedirectRoutingModule } from 'app2/features/auth-redirect/auth-redirect.routing.module';
import { AuthRedirectComponent } from 'app2/features/auth-redirect/components/auth-redirect.component';

@NgModule({
  imports: [SharedModule, AuthRedirectRoutingModule],
  declarations: [AuthRedirectComponent],
})
export class AuthRedirectModule {}
