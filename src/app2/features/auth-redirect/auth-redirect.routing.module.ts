import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthRedirectComponent } from 'app2/features/auth-redirect/components/auth-redirect.component';

const routes: Routes = [
  {
    path: ':clientName',
    component: AuthRedirectComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AuthRedirectRoutingModule {}
