import { NotificationService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AccountService } from 'app/services/api/account.service';
import { TranslateService } from 'app/translate/translate.service';
import { combineLatest } from 'rxjs';
import { first } from 'rxjs/operators';

@Component({
  template: '',
})
export class AuthRedirectComponent implements OnInit {
  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private accountService: AccountService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    combineLatest(this.route.paramMap, this.route.queryParamMap)
      .pipe(first())
      .subscribe(async ([params, queryParams]) => {
        const codeClientName = params.get('clientName');
        const code = queryParams.get('code');
        const state = queryParams.get('state');

        const payload = {
          codeClientName,
          state,
          code,
        };
        try {
          await this.accountService.linkPrivateBuildCodeClient(payload);
          this.auiNotificationService.success({
            title: this.translate.get('oauth_binding_success'),
          });
          this.router.navigate(['/console/admin/user/detail']);
        } catch (e) {
          this.auiNotificationService.error({
            title: this.translate.get('oauth_binding_failed'),
          });
          this.router.navigate(['/console/admin/user/detail']);
        }
      });
  }
}
