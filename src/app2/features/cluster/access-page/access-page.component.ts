import { Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { merge } from 'rxjs';
import { map, pluck, publishReplay, refCount, tap } from 'rxjs/operators';

import {
  ClusterAccessData,
  RegionService,
} from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ErrorResponse } from 'app/services/http.service';
import { ACCOUNT, ENVIRONMENTS } from 'app/shared/tokens';
import { AppState } from 'app/store';
import * as clusterActions from 'app/store/actions/clusters.actions';
import { Account, Environments } from 'app/typings';
import {
  IP_ADDRESS_PORT_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from 'app/utils/patterns';

const CLUSTER_TYPES = [
  {
    display: 'OpenShift',
    value: 'openshift',
  },
  {
    display: 'Original kubernetes',
    value: 'original',
  },
];

const CLOUD_HOST_TYPES = [
  {
    display: 'Private',
    value: 'PRIVATE',
  },
  {
    display: 'VMWare',
    value: 'VMWARE',
  },
];

const DOCKER_VERSIONS = ['1.12.6'];

const NETWORK_MODES = ['ovs', 'flannel', 'calico', 'macvlan'];

const PROTOCOL_PATTERN = /^http:\/\/|^https:\/\//g;

@Component({
  templateUrl: './access-page.component.html',
  styleUrls: ['access-page.component.scss'],
})
export class AccessPageComponent implements OnInit, OnDestroy {
  @ViewChild('form')
  ngForm: NgForm;

  CLUSTER_TYPES = CLUSTER_TYPES;
  CLOUD_HOST_TYPES = CLOUD_HOST_TYPES;
  DOCKER_VERSIONS = DOCKER_VERSIONS;
  NETWORK_MODES = NETWORK_MODES;
  baseRSReg = K8S_RESOURCE_NAME_BASE;
  apiServerReg = IP_ADDRESS_PORT_PATTERN;

  apiserver_protocol = 'https://';
  apiserver_address = '';
  endpoint: string;
  token: string;
  k8sVersion: string;

  name: string;
  displayName: string;

  clusterType = CLUSTER_TYPES[0].value;
  cloudHostType = CLOUD_HOST_TYPES[0].value;
  dockerVersion = DOCKER_VERSIONS[0];
  dockerPath = '/var/lib/docker';
  networkMode = NETWORK_MODES[0];
  nicName = 'eth0';

  loading: boolean;
  isNextStep: boolean;

  loading$ = merge(
    this.actions.pipe(ofType(clusterActions.CREATE_CLUSTER)),
    this.actions
      .pipe(ofType(clusterActions.CREATE_CLUSTER_FAIL))
      .pipe(
        tap((res: { rawError: ErrorResponse }) =>
          this.errorsToast.error(res.rawError),
        ),
      ),
    this.actions
      .pipe(ofType(clusterActions.CREATE_CLUSTER_SUCCESS))
      .pipe(
        tap((_action: clusterActions.CreateClusterSuccess) =>
          this.router.navigate(['/console/admin/cluster/detail/', this.name]),
        ),
      ),
  )
    .pipe(
      pluck('type'),
      map(type => type === clusterActions.CREATE_CLUSTER),
      publishReplay(1),
      refCount(),
    )
    .subscribe((loading: boolean) => (this.loading = loading));

  constructor(
    @Inject(ACCOUNT) private account: Account,
    @Inject(ENVIRONMENTS) private env: Environments,
    private router: Router,
    private location: Location,
    private region: RegionService,
    private errorsToast: ErrorsToastService,
    private store: Store<AppState>,
    private actions: Actions,
  ) {}

  ngOnInit() {
    if (this.env.docker_versions) {
      this.DOCKER_VERSIONS = this.env.docker_versions.split(',');
      this.dockerVersion = this.DOCKER_VERSIONS[0];
    }
  }

  ngOnDestroy() {
    this.loading$.unsubscribe();
  }

  onApiServerInput(event: KeyboardEvent) {
    const inputValue = (<HTMLInputElement>event.target).value;
    const match = inputValue.match(PROTOCOL_PATTERN);
    if (match) {
      this.apiserver_protocol = match[0];
    }
    this.apiserver_address = inputValue.replace(PROTOCOL_PATTERN, '');
  }

  cancel() {
    this.location.back();
  }

  async confirm() {
    this.ngForm.onSubmit(null);
    if (this.ngForm.invalid) {
      return;
    }

    if (this.isNextStep) {
      const params: ClusterAccessData = {
        name: this.name,
        display_name: this.displayName,
        namespace: this.account.namespace,
        attr: {
          cluster: {
            nic: this.nicName,
          },
          docker: {
            path: this.dockerPath,
            version: this.dockerVersion,
          },
          cloud: {
            name: this.cloudHostType,
          },
          kubernetes: {
            type: this.clusterType,
            endpoint: this.apiserver_protocol + this.apiserver_address,
            token: this.token,
            version: this.k8sVersion,
            cni: {
              type: this.networkMode,
            },
          },
        },
        features: {
          logs: {
            type: 'third-party',
            storage: {
              read_log_source: 'default',
              write_log_source: 'default',
            },
          },
          'service-catalog': {
            type: 'official',
          },
        },
      };

      this.store.dispatch(new clusterActions.CreateCluster(params));
    } else {
      this.loading = true;

      try {
        const { version } = await this.region.regionCheck({
          endpoint: this.apiserver_protocol + this.apiserver_address,
          token: this.token,
        });

        this.k8sVersion = version;
      } catch (e) {
        this.errorsToast.error(e);
        return;
      } finally {
        this.loading = false;
      }

      const { apiserver_protocol, apiserver_address, token } = this;

      this.isNextStep = true;

      this.ngForm.resetForm();

      this.apiserver_protocol = apiserver_protocol;
      this.apiserver_address = apiserver_address;
      this.token = token;
    }
  }
}
