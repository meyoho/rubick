import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';

import { AccessPageComponent } from './access-page.component';

@NgModule({
  imports: [SharedModule],
  declarations: [AccessPageComponent],
})
export class AccessPageModule {}
