import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { AccessPageModule } from 'app2/features/cluster/access-page/access-page.module';
import { ClusterRoutingModule } from 'app2/features/cluster/cluster.routing.module';
import { CreatePageModule } from 'app2/features/cluster/create-page/create-page.module';
import { DetailModule } from 'app2/features/cluster/detail/detail.module';
import { NodePodsUpdateComponent } from 'app2/features/cluster/detail/node-pods-update/node-pods-update.component';
import { ListPageModule } from 'app2/features/cluster/list-page/list-page.module';
import { MirrorPageModule } from 'app2/features/cluster/mirror-page/mirror-page.module';

@NgModule({
  declarations: [NodePodsUpdateComponent],
  imports: [
    SharedModule,
    ClusterRoutingModule,
    ListPageModule,
    MirrorPageModule,
    AccessPageModule,
    DetailModule,
    CreatePageModule,
  ],
  exports: [ListPageModule, CreatePageModule],
})
export class ClusterModule {}
