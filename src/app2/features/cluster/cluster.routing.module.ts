import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccessPageComponent } from 'app2/features/cluster/access-page/access-page.component';
import { CreatePageComponent } from 'app2/features/cluster/create-page/create-page.component';
import { DetailComponent } from 'app2/features/cluster/detail/detail.component';
import { FeaturesComponent } from 'app2/features/cluster/detail/features/features.component';
import { NodeDetailComponent } from 'app2/features/cluster/detail/node-detail/node-detail.component';
import { NodePodsUpdateComponent } from 'app2/features/cluster/detail/node-pods-update/node-pods-update.component';
import { ListPageComponent } from 'app2/features/cluster/list-page/list-page.component';
import { MirrorDetailComponent } from 'app2/features/cluster/mirror-page/mirror-detail.component';
import { MirrorEditComponent } from 'app2/features/cluster/mirror-page/mirror-edit.component';
import { MirrorPageComponent } from 'app2/features/cluster/mirror-page/mirror-page.component';

const clusterRoutes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: ListPageComponent,
  },
  {
    path: 'create',
    component: CreatePageComponent,
  },
  {
    path: 'mirror',
    component: MirrorPageComponent,
    pathMatch: 'full',
  },
  {
    path: 'mirror/detail/:name',
    component: MirrorDetailComponent,
  },
  {
    path: 'mirror/create',
    component: MirrorEditComponent,
  },
  {
    path: 'mirror/update/:name',
    component: MirrorEditComponent,
  },
  {
    path: 'access',
    component: AccessPageComponent,
  },
  {
    path: 'detail/:name',
    component: DetailComponent,
  },
  {
    path: 'node_detail/:clusterName/:privateIp',
    component: NodeDetailComponent,
  },
  {
    path: 'node_detail/:clusterName/:privateIp/update/:namespace/:name',
    component: NodePodsUpdateComponent,
  },
  {
    path: ':regionName/features',
    component: FeaturesComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(clusterRoutes)],
  exports: [RouterModule],
})
export class ClusterRoutingModule {}
