import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import clipboard from 'clipboard-polyfill';

import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: 'cluster-script-dialog.component.html',
  styleUrls: ['cluster-script-dialog.component.scss'],
})
export class ClusterScriptDialogComponent implements OnInit {
  nodeList: {
    ipaddress: string;
  }[];

  script: string;

  constructor(
    @Inject(DIALOG_DATA)
    private modalData: {
      nodeList: {
        ipaddress: string;
      }[];
      script: string;
    },
    private dialogRef: DialogRef,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private router: Router,
  ) {}

  ngOnInit() {
    this.script = this.modalData.script;
    this.nodeList = this.modalData.nodeList;
  }

  cancel() {
    this.dialogRef.close();
  }

  copyScript() {
    clipboard
      .writeText(this.script)
      .then(() => {
        this.notificationService.success(
          this.translateService.get('copy_clipboard_success'),
        );
        this.dialogRef.close();
        this.router.navigateByUrl(`/console/admin/cluster/list`);
      })
      .catch(() => {
        this.notificationService.error(
          this.translateService.get('copy_clipboard_fail'),
        );
      });
  }
}
