import { DialogService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute } from '@angular/router';

import {
  ClusterCreateModel,
  CniBackend,
  CniType,
  RegionService,
} from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import {
  CIDR_PATTERN,
  IP_ADDRESS_HOSTNAME_PATTERN,
  IP_ADDRESS_PORT_PATTERN,
  K8S_RESOURCE_NAME_BASE,
} from 'app/utils/patterns';
import { ClusterScriptDialogComponent } from 'app2/features/cluster/create-page/cluster-script-dialog/cluster-script-dialog.component';
import { ClusterMasterFieldsetComponent } from 'app2/features/cluster/create-page/fieldset/cluster-master-fieldset.component';

const PROTOCOL_PATTERN = /^http:\/\/|^https:\/\//g;

@Component({
  templateUrl: 'create-page.component.html',
  styleUrls: ['create-page.component.scss'],
})
export class CreatePageComponent implements OnInit {
  baseRSReg = K8S_RESOURCE_NAME_BASE;
  cidrReg = CIDR_PATTERN;
  apiServerReg = IP_ADDRESS_PORT_PATTERN;
  loadBalancerReg = IP_ADDRESS_HOSTNAME_PATTERN;
  initialized: boolean;
  submitting: boolean;
  disableClusterName = false;

  clusterCreatemodel: ClusterCreateModel = {
    name: '',
    display_name: '',
    apiserver: '',
    cluster_type: {
      is_ha: false,
      loadbalancer: '',
    },
    masters: [],
    nodes: [],
    ssh: {
      type: 'password',
      name: '',
      secret: '',
      port: '',
    },
    cni: {
      type: CniType.none,
      backend: CniBackend.none,
      cidr: '',
      network_policy: '',
      macvlan_gateway: '',
      macvlan_name: 'default',
      macvlan_subnet_start: '',
      macvlan_subnet_end: '',
    },
  };
  indexId: 0;
  apiserver_protocol = 'https://';
  apiserver_address = '';
  masterList = [
    {
      ipaddress: '',
      compute: false,
    },
  ];
  nodeList: {
    ipaddress: string;
  }[] = [];
  cniType: CniType = CniType.none;

  @ViewChild('Form')
  form: NgForm;

  @ViewChild('clusterMasterForm')
  clusterMasterForm: ClusterMasterFieldsetComponent;

  constructor(
    private regionService: RegionService,
    private dialogService: DialogService,
    private location: Location,
    private errorsToastService: ErrorsToastService,
    private route: ActivatedRoute,
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe(params => {
      const name = params.get('name');
      const display_name = params.get('display_name');
      if (name && display_name) {
        this.clusterCreatemodel.name = name;
        this.clusterCreatemodel.display_name = display_name;
        this.disableClusterName = true;
      }
      this.initialized = true;
    });
  }

  clusterTypeChange(isHa: boolean) {
    this.masterList = [
      {
        ipaddress: '',
        compute: false,
      },
    ];
    if (isHa) {
      this.clusterMasterForm.add();
    }
  }

  onApiServerInput(event: KeyboardEvent) {
    const inputValue = (<HTMLInputElement>event.target).value;
    const match = inputValue.match(PROTOCOL_PATTERN);
    if (match) {
      this.apiserver_protocol = match[0];
    }
    this.apiserver_address = inputValue.replace(PROTOCOL_PATTERN, '');
  }

  async confirm() {
    this.submitting = true;
    this.form.onSubmit(null);
    if (this.form.invalid) {
      this.submitting = false;
      return;
    }
    this.clusterCreatemodel.nodes = this.nodeList;
    this.clusterCreatemodel.masters = this.masterList.map(item => {
      if (item.compute) {
        this.clusterCreatemodel.nodes.push({
          ipaddress: item.ipaddress,
        });
      }
      return {
        ipaddress: item.ipaddress,
      };
    });
    if (['flannel_vxlan', 'flannel_host_gw'].includes(this.cniType)) {
      this.clusterCreatemodel.cni.type = CniType.flannel;
      this.clusterCreatemodel.cni.backend =
        CniBackend[this.cniType.substring(8)];
    } else {
      this.clusterCreatemodel.cni.type = this.cniType;
    }
    this.clusterCreatemodel.apiserver = this.apiserver_address
      ? this.apiserver_protocol + this.apiserver_address
      : '';
    try {
      const res = await this.regionService.getClusterScripts(
        this.clusterCreatemodel,
      );
      this.dialogService.open(ClusterScriptDialogComponent, {
        data: {
          nodeList: this.clusterCreatemodel.nodes,
          script: res.commands.install,
        },
      });
    } catch (e) {
      this.errorsToastService.error(e);
    } finally {
      this.submitting = false;
    }
  }

  cancel() {
    this.location.back();
  }
}
