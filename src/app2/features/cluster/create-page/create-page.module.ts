import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { ClusterScriptDialogComponent } from 'app2/features/cluster/create-page/cluster-script-dialog/cluster-script-dialog.component';
import { CreatePageComponent } from 'app2/features/cluster/create-page/create-page.component';
import { ClusterMasterFieldsetComponent } from 'app2/features/cluster/create-page/fieldset/cluster-master-fieldset.component';
import { ClusterNodeFieldsetComponent } from 'app2/features/cluster/create-page/fieldset/cluster-node-fieldset.component';

@NgModule({
  imports: [FormTableModule, SharedModule, RouterModule],
  declarations: [
    CreatePageComponent,
    ClusterMasterFieldsetComponent,
    ClusterNodeFieldsetComponent,
    ClusterScriptDialogComponent,
  ],
  entryComponents: [ClusterScriptDialogComponent],
})
export class CreatePageModule {}
