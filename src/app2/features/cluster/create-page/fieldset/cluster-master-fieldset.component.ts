import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { IP_ADDRESS_PATTERN } from 'app/utils/patterns';

export interface ClusterMaster {
  value: string;
  compute: boolean;
}

@Component({
  selector: 'rc-cluster-master-fieldset',
  templateUrl: './cluster-master-fieldset.component.html',
  styleUrls: ['./cluster-master-fieldset.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterMasterFieldsetComponent extends BaseResourceFormArrayComponent<
  ClusterMaster
> {
  @Input()
  isHa: boolean;
  ipReg = IP_ADDRESS_PATTERN;

  constructor(injector: Injector) {
    super(injector);
  }

  getOnFormArrayResizeFn() {
    return () =>
      this.fb.group({
        ipaddress: [
          '',
          [Validators.required, Validators.pattern(this.ipReg.pattern)],
        ],
        compute: [false],
      });
  }

  add(index = this.form.controls.length) {
    super.add(index);
    super.add(index);
  }

  trackByFn(index: number) {
    return index;
  }
}
