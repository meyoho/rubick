import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { IP_ADDRESS_PATTERN } from 'app/utils/patterns';
@Component({
  selector: 'rc-cluster-node-fieldset',
  templateUrl: './cluster-node-fieldset.component.html',
  styleUrls: ['./cluster-node-fieldset.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterNodeFieldsetComponent extends BaseResourceFormArrayComponent<{
  ipaddress: string;
}> {
  ipReg = IP_ADDRESS_PATTERN;
  constructor(injector: Injector) {
    super(injector);
  }

  getOnFormArrayResizeFn() {
    return () =>
      this.fb.group({
        ipaddress: [
          '',
          [Validators.required, Validators.pattern(this.ipReg.pattern)],
        ],
      });
  }
}
