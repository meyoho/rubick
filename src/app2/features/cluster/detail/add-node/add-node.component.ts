import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Subject } from 'rxjs';

import { NodePayloadV2, RegionService } from 'app/services/api/region.service';

@Component({
  templateUrl: 'add-node.component.html',
  styleUrls: ['add-node.component.scss'],
})
export class AddNodeComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  @Output()
  close = new EventEmitter();
  submitting = true;
  addNode: NodePayloadV2 = {
    node_list: [],
    ssh_port: '',
    ssh_username: '',
    ssh_password: '',
  };
  constructor(
    private regionService: RegionService,
    @Inject(DIALOG_DATA)
    private modalData: {
      cluster: string;
    },
  ) {}

  ngOnInit() {}

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  async cancel() {
    this.close.next(null);
  }

  async confirm(formRef: NgForm) {
    formRef.onSubmit(null);
    if (formRef.form.invalid) {
      return;
    }
    this.submitting = true;
    try {
      await this.regionService.createClusterNodes(
        this.modalData.cluster,
        this.addNode,
      );
      this.close.next(true);
    } catch (err) {
    } finally {
      this.submitting = false;
    }
  }
}
