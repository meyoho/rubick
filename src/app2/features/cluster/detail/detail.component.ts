import { DialogService, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { get } from 'lodash-es';
import { Subject } from 'rxjs';
import { first, pluck, takeUntil } from 'rxjs/operators';

import {
  Cluster,
  OverCommit,
  RegionFeature,
  RegionService,
  RegionStatus,
} from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ErrorResponse } from 'app/services/http.service';
import { ENVIRONMENTS, WEBLABS } from 'app/shared/tokens';
import { AppState } from 'app/store';
import * as clusterActions from 'app/store/actions/clusters.actions';
import { TranslateService } from 'app/translate/translate.service';
import { Environments, Weblabs } from 'app/typings';
import { AddNodeComponent } from 'app2/features/cluster/detail/add-node/add-node.component';
import { UpdateOverCommitComponent } from 'app2/features/cluster/detail/update-over-commit/component';
import { UpdateTokenComponent } from 'app2/features/cluster/detail/update-token/component';
import { ForceDeleteComponent } from 'app2/shared/components/force-delete/component';
import { getRegionFeatureStatus } from '../utils';

@Component({
  templateUrl: './detail.component.html',
  styleUrls: ['./detail.component.scss', '../common-badge.component.scss'],
})
export class DetailComponent implements OnInit, OnDestroy {
  clusterName: string;
  cluster: Cluster;
  overCommit: OverCommit = {
    cpu: 0,
    mem: 0,
  };
  columns = ['member', 'create_time'];
  regionFeatures?: {
    log: RegionFeature;
    metric: RegionFeature;
  };
  customizedMetric: boolean;
  hasCreateNodePermission: boolean;

  getRegionFeatureStatus = getRegionFeatureStatus;

  loading: boolean;
  private onDestroy$ = new Subject<void>();

  get autoScalingEnabled() {
    return get(this.cluster, 'features.node.features', []).includes(
      'auto-scaling',
    );
  }

  get cloudUpdate() {
    return (
      this.cluster &&
      this.cluster.state !== RegionStatus.Deploying &&
      this.cluster.state !== RegionStatus.Preparing
    );
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private region: RegionService,
    private errorsToast: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private translate: TranslateService,
    private store: Store<AppState>,
    private actions: Actions,
    private location: Location,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  ngOnInit() {
    this.roleUtil
      .resourceTypeSupportPermissions('cluster_node')
      .then(
        hasCreateNodePermission =>
          (this.hasCreateNodePermission = hasCreateNodePermission),
      );
    this.actions
      .pipe(ofType(clusterActions.DELETE_CLUSTER_BY_NAME_SUCCESS))
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.loading = false;
        this.router.navigateByUrl('/console/admin/cluster/list', {
          replaceUrl: true,
        });
      });

    this.actions
      .pipe(ofType(clusterActions.DELETE_CLUSTER_BY_NAME_FAIL))
      .pipe(
        takeUntil(this.onDestroy$),
        pluck('payload'),
      )
      .subscribe((res: { error: ErrorResponse; force: boolean }) => {
        this.loading = false;
        if (res.force) {
          this.notificationService.error(
            this.translate.get('force_delete_cluster_failed_tip'),
          );
        } else {
          this.deleteClusterForce();
        }
      });
    this.route.paramMap.subscribe(params => {
      this.clusterName = params.get('name');
      this.region.setRegionByName(this.clusterName);
      this.fetchCluster();
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(index: number) {
    return index;
  }

  async fetchCluster() {
    this.loading = true;

    try {
      const overCommit = await this.region.getOverCommit(this.clusterName);
      this.overCommit = overCommit;
    } catch (e) {
      this.errorsToast.error(e);
    }

    try {
      const [cluster, regionFeatures, customizedMetric] = await Promise.all([
        this.region.getCluster(this.clusterName),
        this.region.getRegionFeatures(this.clusterName).catch(e => {
          this.errorsToast.error(e);
          return { log: null, metric: null };
        }),
        this.region
          .getCurrentRegion()
          .then(res => {
            if (get(res, 'features.customized.metric')) {
              return true;
            }
            return false;
          })
          .catch(error => {
            this.errorsToast.error(error);
            return false;
          }),
      ]);

      this.customizedMetric = customizedMetric;
      this.regionFeatures = regionFeatures;
      this.cluster = cluster;
    } catch (e) {
      this.errorsToast.error(e);
      return this.location.back();
    } finally {
      this.loading = false;
    }
  }

  shouldDisplay(action: string) {
    return this.roleUtil.resourceHasPermission(this.cluster, 'cluster', action);
  }

  async deleteCluster() {
    const name = this.clusterName;

    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('confirm_delete_cluster', {
          name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.loading = true;
    this.store.dispatch(
      new clusterActions.DeleteClusterByName({
        name: this.clusterName,
      }),
    );
  }

  async deleteClusterForce() {
    const modelRef = await this.dialogService.open(ForceDeleteComponent, {
      data: {
        title: this.translate.get('delete_cluster_failed'),
        content: this.translate.get('delete_cluster_failed_content'),
        name: this.clusterName,
      },
    });
    modelRef.componentInstance.close.pipe(first()).subscribe((res: Boolean) => {
      modelRef.close();
      if (res) {
        this.loading = true;
        this.store.dispatch(
          new clusterActions.DeleteClusterByName({
            name: this.clusterName,
            force: true,
          }),
        );
      }
    });
  }

  async addNode() {
    const modelRef = await this.dialogService.open(AddNodeComponent, {
      data: {
        cluster: this.clusterName,
      },
    });
    modelRef.componentInstance.close.pipe(first()).subscribe((res: Boolean) => {
      modelRef.close();
      if (res) {
        this.fetchCluster();
        this.notificationService.success({
          title: this.translate.get('success'),
          content: this.translate.get('add_cluster_node_success_msg'),
        });
      }
    });
  }

  async updateOverCommit() {
    const modelRef = await this.dialogService.open(UpdateOverCommitComponent, {
      data: {
        cluster: this.clusterName,
        overCommit: this.overCommit,
      },
    });
    modelRef.componentInstance.close.pipe(first()).subscribe((res: Boolean) => {
      modelRef.close();
      if (res) {
        this.fetchCluster();
        this.notificationService.success({
          title: this.translate.get('success'),
          content: this.translate.get('update_over_commit_success'),
        });
      }
    });
  }

  showOverCommit(value: number) {
    return value ? value : this.translate.get('region_container_manager_NONE');
  }

  async updateToken() {
    const modelRef = await this.dialogService.open(UpdateTokenComponent, {
      data: {
        cluster: this.cluster,
        clusterName: this.clusterName,
      },
    });
    modelRef
      .afterClosed()
      .pipe(first())
      .subscribe((res: Boolean) => {
        modelRef.close();
        if (res) {
          this.fetchCluster();
          this.notificationService.success({
            title: this.translate.get('success'),
            content: this.translate.get('update_token_success'),
          });
        }
      });
  }
}
