import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ProjectSharedModule } from 'app/features-shared/project/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { AddNodeComponent } from 'app2/features/cluster/detail/add-node/add-node.component';
import { ClusterChartComponent } from 'app2/features/cluster/detail/monitor/chart/component';
import { ClusterMonitorComponent } from 'app2/features/cluster/detail/monitor/component';
import { NodeDetailComponent } from 'app2/features/cluster/detail/node-detail/node-detail.component';
import { NodePodsComponent } from 'app2/features/cluster/detail/node-pods/node-pods.component';
import { UpdateNodeLabelsComponent } from 'app2/features/cluster/detail/update-node-labels/update-node-labels.component';
import { UpdateNodePodsLabelsComponent } from 'app2/features/cluster/detail/update-node-pods-labels/update-node-pods-labels.component';
import { UpdateOverCommitComponent } from 'app2/features/cluster/detail/update-over-commit/component';
import { UpdateTokenComponent } from 'app2/features/cluster/detail/update-token/component';
import { WebSSHComponent } from 'app2/features/cluster/detail/web-ssh/component';
import { DetailComponent } from './detail.component';
import { EditFeatureComponent } from './edit-feature/edit-feature.component';
import { FeaturesComponent } from './features/features.component';
import { ClusterNodesComponent } from './nodes/cluster-nodes.component';
import { WebSSHService } from './webssh.service';

@NgModule({
  imports: [SharedModule, FormTableModule, RouterModule, ProjectSharedModule],
  declarations: [
    DetailComponent,
    ClusterMonitorComponent,
    ClusterChartComponent,
    NodePodsComponent,
    ClusterNodesComponent,
    FeaturesComponent,
    EditFeatureComponent,
    NodeDetailComponent,
    UpdateNodeLabelsComponent,
    UpdateNodePodsLabelsComponent,
    UpdateTokenComponent,
    AddNodeComponent,
    UpdateOverCommitComponent,
    WebSSHComponent,
  ],
  entryComponents: [
    EditFeatureComponent,
    UpdateNodeLabelsComponent,
    UpdateNodePodsLabelsComponent,
    UpdateTokenComponent,
    AddNodeComponent,
    UpdateOverCommitComponent,
    WebSSHComponent,
  ],
  providers: [WebSSHService],
})
export class DetailModule {}
