import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { get } from 'lodash-es';

import {
  Integration,
  IntegrationService,
} from 'app/services/api/integration.service';
import {
  Cluster,
  RegionFeature,
  RegionService,
} from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { getRegionFeatureStatus } from 'app2/features/cluster/utils';
import { EditFeatureComponent } from '../edit-feature/edit-feature.component';

@Component({
  templateUrl: './features.component.html',
  styleUrls: ['./features.component.scss', '../../common-badge.component.scss'],
})
export class FeaturesComponent implements OnInit, OnDestroy {
  @ViewChild('dockThirdPartyTemp')
  dockThirdPartyTemp: TemplateRef<any>;
  @ViewChild('dockSSHTemp')
  dockSSHTemp: TemplateRef<any>;

  getRegionFeatureStatus = getRegionFeatureStatus;

  regionName: string;

  regionFeatures: {
    log: RegionFeature;
    metric: RegionFeature;
    registry: RegionFeature;
    ssh: RegionFeature;
  };

  checkingForUpdates: boolean;

  timeoutId: number;

  integrations: Integration[];
  integrationId: string;
  isAddOrUpdate: boolean;

  checkForUpdatesEnabled: boolean;
  clusterEditEnabled: boolean;

  constructor(
    private route: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private location: Location,
    private dialog: DialogService,
    private regionService: RegionService,
    private integration: IntegrationService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private errorsToast: ErrorsToastService,
    private roleUtilitiesService: RoleUtilitiesService,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(async () => {
      await this.refetch();
      this.cdr.detectChanges();
    });
  }

  private async fetch() {
    this.regionName = this.route.snapshot.paramMap.get('regionName');

    this.regionService.setRegionByName(this.regionName);
    const region: Cluster = await this.regionService.getCurrentRegion();
    this.clusterEditEnabled = this.roleUtilitiesService.resourceHasPermission(
      region,
      'cluster',
      'update',
    );

    try {
      this.regionFeatures = await this.regionService.getRegionFeatures(
        this.regionName,
      );
    } catch (e) {
      this.errorsToast.error(e);
      if (!this.regionFeatures && e.status === 404) {
        return this.location.back();
      }
    }
  }

  async refetch(immediate = true) {
    clearTimeout(this.timeoutId);

    if (immediate) {
      await this.fetch();
    }

    if (
      !this.regionFeatures ||
      ['deploying', 'unknown'].includes(
        this.getRegionFeatureStatus(this.regionFeatures.log),
      )
    ) {
      this.timeoutId = window.setTimeout(() => this.refetch(), 5000);
    }
  }

  ngOnDestroy() {
    clearTimeout(this.timeoutId);
  }

  async checkForUpdates() {
    // TODO: waiting for backend
  }

  async dockThirdParty(types: string, isAddOrUpdate = false) {
    this.isAddOrUpdate = isAddOrUpdate;

    this.integrations = await this.integration.getIntegrations({
      families: 'Features',
      types,
      page_size: 1000,
    });

    if (types === 'PrometheusOperator') {
      this.integrationId = get(
        this.regionFeatures.metric,
        'config.integration_uuid',
      );
      this.dialog.open(this.dockThirdPartyTemp);
    } else if (types === 'Ssh') {
      this.integrationId = get(
        this.regionFeatures.ssh,
        'config.integration_uuid',
      );
      this.dialog.open(this.dockSSHTemp);
    }
  }

  closeModal() {
    this.dialog.closeAll();
  }

  async editRegionFeature({
    featureName,
    type,
    yamlContent,
    isAddOrUpdate,
  }: {
    featureName: 'log' | 'metric' | 'registry';
    type: 'official' | 'prometheus';
    yamlContent?: string;
    isAddOrUpdate?: boolean;
  }) {
    const isOfficial = type === 'official';

    if (!isOfficial && !this.integrationId) {
      return;
    }

    try {
      this.regionFeatures[
        featureName
      ] = await this.regionService.editRegionFeature(
        this.regionName,
        featureName,
        {
          config: {
            type,
            integration_uuid: isOfficial ? undefined : this.integrationId,
          },
          values_yaml_content: yamlContent,
        },
        isAddOrUpdate,
      );
      this.refetch(true);
    } catch (e) {
      return this.errorsToast.error(e);
    }

    this.closeModal();
  }

  editOfficialFeature(
    featureName: 'log' | 'metric' | 'registry',
    isAddOrUpdate?: boolean,
  ) {
    const yamlConfig = get(
      this.regionFeatures[featureName],
      'template.versions[0].values_yaml_content',
    );
    if (yamlConfig) {
      const dialogRef = this.dialog.open(EditFeatureComponent, {
        data: this.regionFeatures[featureName],
        size: DialogSize.Large,
      });

      dialogRef.componentInstance.confirmed.subscribe(
        async (yamlContent: string) => {
          dialogRef.componentInstance.loading = true;

          await this.editRegionFeature({
            featureName,
            type: 'official',
            yamlContent,
            isAddOrUpdate,
          });

          dialogRef.componentInstance.loading = false;

          this.refetch(false);
        },
      );
    } else {
      this.auiNotificationService.error(
        this.translate.get('region_feature_not_configured'),
      );
    }
  }

  async deleteFeature(featureName: 'log' | 'metric' | 'registry') {
    try {
      await this.dialog.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('confirm_delete_region_feature'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      await this.regionService.deleteRegionFeature(
        this.regionName,
        featureName,
      );
    } catch (e) {
      return this.errorsToast.error(e);
    }

    this.regionFeatures[featureName].config = null;
  }
}
