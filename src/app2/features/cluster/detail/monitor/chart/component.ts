import { Component, Input } from '@angular/core';

import * as Highcharts from 'highcharts';
import { cloneDeep } from 'lodash-es';
require('highcharts/modules/no-data-to-display')(Highcharts);

@Component({
  selector: 'rc-cluster-chart',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class ClusterChartComponent {
  @Input()
  metricOptions: Highcharts.Options;
  @Input()
  chartTitle: string;
  @Input()
  chartLoading = false;

  chartOptions: Highcharts.Options;
  Highcharts = Highcharts;

  @Input()
  set series(series: Highcharts.IndividualSeriesOptions[]) {
    if (!this.chartOptions) {
      this.chartOptions = cloneDeep(this.metricOptions);
    }
    if (series && series.length) {
      this.chartOptions.series = series;
    }
  }

  constructor() {}
}
