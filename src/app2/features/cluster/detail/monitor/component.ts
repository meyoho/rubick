import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';

import * as Highcharts from 'highcharts';
import { cloneDeep } from 'lodash-es';
import { BehaviorSubject, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { BaseTimeSelectComponent } from 'app/features-shared/alarm/base-time-select.component';
import {
  AGGREGATORS,
  MetricNumericOptions,
  MetricPercentOptions,
  TIME_STAMP_OPTIONS,
} from 'app/features-shared/app/utils/monitor';
import { LogService } from 'app/services/api/log.service';
import {
  Metric,
  MetricQueries,
  MetricQuery,
  MetricService,
} from 'app/services/api/metric.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
const uuidv4 = require('uuid/v4');

const QUERY_TIME_STAMP_OPTIONS = TIME_STAMP_OPTIONS.concat([
  {
    type: 'custom_time_range',
    offset: 0,
  },
]);

@Component({
  selector: 'rc-cluster-monitor',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterMonitorComponent extends BaseTimeSelectComponent
  implements OnInit, OnDestroy {
  @Input()
  privateIp: string;
  @Input()
  clusterName: string;
  private onDestroy$ = new Subject<void>();
  private intervalTimer: number;
  private loadMetrics$ = new BehaviorSubject(null);

  aggregators = AGGREGATORS.map(aggregator => {
    return {
      key: aggregator.key,
      name: aggregator.name,
    };
  });
  timeStampOptions = QUERY_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type,
    };
  });
  selectedAggregator = this.aggregators[0].key;
  cpuChartSeries: Highcharts.IndividualSeriesOptions[];
  memChartSeries: Highcharts.IndividualSeriesOptions[];
  loadChartSeries: Highcharts.IndividualSeriesOptions[];
  diskChartSeries: Highcharts.IndividualSeriesOptions[];
  diskRWChartSeries: Highcharts.IndividualSeriesOptions[];
  networkChartSeries: Highcharts.IndividualSeriesOptions[];
  metricPercentOptions = cloneDeep(MetricPercentOptions);
  metricNumericOptions = cloneDeep(MetricNumericOptions);
  systemLoadOptions = cloneDeep(MetricNumericOptions);
  metric_name = {
    'cpu.utilization': {
      chart: 'cpuChartSeries',
      unit: '%',
    },
    'memory.utilization': {
      chart: 'memChartSeries',
      unit: '%',
    },
    'load.1': {
      chart: 'loadChartSeries',
      unit: '',
    },
    'load.5': {
      chart: 'loadChartSeries',
      unit: '',
    },
    'load.15': {
      chart: 'loadChartSeries',
      unit: '',
    },
    'disk.utilization': {
      chart: 'diskChartSeries',
      unit: '%',
    },
    'disk.read.bytes': {
      chart: 'diskRWChartSeries',
      unit: 'byte/second',
    },
    'disk.written.bytes': {
      chart: 'diskRWChartSeries',
      unit: 'byte/second',
    },
    'network.receive_bytes': {
      chart: 'networkChartSeries',
      unit: 'byte/second',
    },
    'network.transmit_bytes': {
      chart: 'networkChartSeries',
      unit: 'byte/second',
    },
  };
  chart_name = {
    cpuChartSeries: ['cpu.utilization'],
    memChartSeries: ['memory.utilization'],
    loadChartSeries: ['load.1', 'load.5', 'load.15'],
    diskChartSeries: ['disk.utilization'],
    diskRWChartSeries: ['disk.written.bytes', 'disk.read.bytes'],
    networkChartSeries: ['network.receive_bytes', 'network.transmit_bytes'],
  };
  metrics = [
    'cpu.utilization',
    'memory.utilization',
    'load.1',
    'load.5',
    'load.15',
    'disk.utilization',
    'disk.read.bytes',
    'disk.written.bytes',
    'network.receive_bytes',
    'network.transmit_bytes',
  ];

  constructor(
    private cdr: ChangeDetectorRef,
    private metricService: MetricService,
    logService: LogService,
    auiMessageService: MessageService,
    translateService: TranslateService,
    @Inject(ACCOUNT) public account: Account,
  ) {
    super(logService, auiMessageService, translateService);
  }

  async ngOnInit() {
    this.initTimer();
    Highcharts.setOptions({
      lang: {
        noData: this.translateService.get('no_data'),
      },
    });
    if (!this.privateIp) {
      this.metrics = ['cpu.utilization', 'memory.utilization'];
    }
    this.metricNumericOptions.tooltip[
      'valueSuffix'
    ] = this.translateService.get('byte/second');
    this.systemLoadOptions.tooltip.valueDecimals = 2;
    this.queryMetrics();
  }

  ngOnDestroy() {
    window.clearInterval(this.intervalTimer);
    this.onDestroy$.next();
  }

  initTimer() {
    this.intervalTimer = window.setInterval(() => {
      if (!this.chartLoading) {
        if (this.selectedTimeStampOption !== 'custom_time_range') {
          this.resetTimeRange();
        }
        if (this.checkTimeRange()) {
          this.loadCharts();
        }
      }
    }, 60000);
  }

  loadCharts() {
    this.chartLoading = true;
    this.cdr.markForCheck();
    this.loadMetrics$.next({
      aggregator: this.selectedAggregator,
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    });
  }

  private generateQueries(
    query: MetricQuery,
    aggregator: string,
    metricTypes: Array<string>,
  ) {
    const queries = metricTypes.map(metricType => {
      const uuid = uuidv4();
      this.metric_name[metricType]['__query_id__'] = uuid;
      const metricQuery: MetricQueries = {
        aggregator: aggregator,
        id: uuid,
      };
      if (this.privateIp) {
        metricQuery.labels = [
          {
            type: 'EQUAL',
            name: '__name__',
            value: `node.${metricType}`,
          },
          {
            type: 'EQUAL',
            name: 'name',
            value: this.privateIp,
          },
          {
            type: 'EQUAL',
            name: 'kind',
            value: 'Node',
          },
        ];
      } else {
        metricQuery.labels = [
          {
            type: 'EQUAL',
            name: '__name__',
            value: `cluster.${metricType}`,
          },
          {
            type: 'EQUAL',
            name: 'kind',
            value: 'Cluster',
          },
        ];
      }
      return metricQuery;
    });
    return this.metricService.queryMetrics(this.clusterName, {
      ...query,
      queries,
    });
  }

  private parseMetricsResponse(metric: Metric[]) {
    return metric.map(el => {
      if (this.step > 30 && el.values.length < 30) {
        el.values = this.metricService.fillUpResult(
          el.values,
          this.end_time,
          this.step,
        );
      }
      return {
        name: el.metric.metric_name,
        data: el.values.map((value: Array<number | string>) => {
          let y = null;
          if (value[1] !== '+Inf' && value[1] !== '') {
            y = Number(value[1]);
            if (el.metric.unit === '%') {
              y = y * 100;
            }
          }
          return {
            x: Number(value[0]) * 1000,
            y,
          };
        }),
      };
    });
  }

  private queryMetrics() {
    this.loadMetrics$.pipe(takeUntil(this.onDestroy$)).subscribe(args => {
      if (args === null) {
        return;
      }
      // http://jira.alaudatech.com/browse/DEV-12815
      let start = 0;
      if (args.end - args.start < 900) {
        this.step = 30;
        start = args.start;
      } else {
        this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
        start = args.start + this.step;
      }
      this.end_time = args.end;
      const query = {
        start,
        end: args.end,
        step: this.step,
      };
      this.generateQueries(query, args.aggregator, this.metrics)
        .then(results => {
          let metricPrefix = 'cluster';
          if (this.privateIp) {
            metricPrefix = 'node';
          }
          /*
           * 对于一个metric_name，results中可能包含一个或多个__query_id__与之对应的结果
           * 下面是把__query_id__相同的results合并到一个数组并保存在metric[1]['values']中
           */
          Object.entries(this.metric_name).forEach(metric => {
            metric[1]['values'] = [];
            results.forEach(result => {
              if (result.metric.__query_id__ === metric[1]['__query_id__']) {
                metric[1]['metric'] = {
                  ...result.metric,
                  metric_name: `${metricPrefix}.${metric[0]}`,
                  unit: metric[1].unit,
                };
                metric[1]['values'] = metric[1]['values'].concat(result.values);
              }
            });
            /*
             * metric[1]['values']是包含多个Metric的数组
             * 一个Metric是一个包含两个元素的数组：[1552626688, "1.6875"]
             * 第一个元素表示时间，第二个元素表示数值
             * 下面是对Metric值按照时间排序，再去除时间相同的Metric
             */
            metric[1]['values'] = metric[1]['values']
              .sort((a: Array<any>, b: Array<any>) => {
                return a[0] - b[0];
              })
              .filter((item: Array<any>, pos: number, ary: Array<any>) => {
                return !pos || item[0] !== ary[pos - 1][0];
              });
          });
          Object.entries(this.chart_name).forEach(chart => {
            this[chart[0]] = this.parseMetricsResponse(
              chart[1].map(metric => this.metric_name[metric]),
            );
          });
        })
        .catch(error => {
          throw error;
        })
        .finally(() => {
          this.chartLoading = false;
          this.cdr.markForCheck();
        });
    });

    this.resetTimeRange();
    this.loadCharts();
  }
}
