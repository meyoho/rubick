import { DialogService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { get } from 'lodash-es';

import { IntegrationCenterService } from 'app/services/api/integration-center.service';
import {
  NodeVm,
  RegionFeature,
  RegionService,
} from 'app/services/api/region.service';
import { RegionUtilitiesService } from 'app/services/api/region.utilities.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { UpdateNodeLabelsComponent } from 'app2/features/cluster/detail/update-node-labels/update-node-labels.component';
import { WebSSHComponent } from 'app2/features/cluster/detail/web-ssh/component';
import {
  formatCommonUnit,
  formatNumUnit,
  getRegionFeatureStatus,
} from 'app2/features/cluster/utils';

interface NodeVmDetail extends NodeVm {
  hostname: string;
  create_time: number;
}

@Component({
  selector: 'rc-node-detail',
  templateUrl: 'node-detail.component.html',
  styleUrls: ['node-detail.component.scss'],
})
export class NodeDetailComponent implements OnInit {
  clusterName: string;
  filterIp = '';
  showMonitorTab = false;
  showSSHMenu = false;
  nodeVm: NodeVmDetail;
  nodeCanUpdate = false;
  nodeCanDelete = false;
  regionFeatures?: {
    log: RegionFeature;
    metric: RegionFeature;
    ssh: RegionFeature;
  };
  customizedMetric: boolean;
  getRegionFeatureStatus = getRegionFeatureStatus;

  constructor(
    private region: RegionService,
    private regionUtilities: RegionUtilitiesService,
    private integrationCenterService: IntegrationCenterService,
    private roleUtil: RoleUtilitiesService,
    private route: ActivatedRoute,
    private router: Router,
    private errorsToast: ErrorsToastService,
    private dialogService: DialogService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(
      async params => {
        this.clusterName = params.get('clusterName');
        this.filterIp = params.get('privateIp');
        const [customizedMetric] = await Promise.all([
          this.region
            .getCurrentRegion()
            .then(res => {
              if (get(res, 'features.customized.metric')) {
                return true;
              }
              return false;
            })
            .catch(() => {
              return false;
            }),
          this.getNode(),
          this.fetchRegionFeatures(),
        ]);
        this.customizedMetric = customizedMetric;
      },
      e => {
        this.errorsToast.error(e);
      },
    );
  }

  async getNode() {
    const res = await this.region.getClusterNodes(this.clusterName);
    this.nodeCanUpdate = this.roleUtil.resourceHasPermission(
      res,
      'cluster_node',
      'update',
    );
    this.nodeCanDelete = this.roleUtil.resourceHasPermission(
      res,
      'cluster_node',
      'delete',
    );
    const nodes = res.items.filter(node =>
      node.status.addresses
        .find(addr => addr.type === 'InternalIP')
        .address.includes(this.filterIp),
    );
    const node = nodes[0];
    this.nodeVm = {
      name: node.metadata.name,
      privateIp: node.status.addresses.find(addr => addr.type === 'InternalIP')
        .address,
      hostname: node.status.addresses.find(addr => addr.type === 'Hostname')
        .address,
      create_time: Date.parse(node.metadata.creation_timestamp),
      labels: node.metadata.labels,
      status: {
        state: this.regionUtilities.getNodeState(node),
        schedulable: !node.spec.unschedulable,
        taint: node.spec.taints && !!node.spec.taints.length,
      },
      cpu: formatNumUnit(node.status.allocatable.cpu),
      memory: formatCommonUnit(node.status.allocatable.memory, true),
      type:
        'node-role.kubernetes.io/master' in node.metadata.labels
          ? 'master'
          : 'node',
    };
  }

  async fetchRegionFeatures() {
    try {
      const regionFeatures = await this.region.getRegionFeatures(
        this.clusterName,
      );

      this.regionFeatures = regionFeatures;
      this.showMonitorTab =
        this.regionFeatures &&
        this.getRegionFeatureStatus(regionFeatures.metric) === 'docked';
      this.showSSHMenu =
        this.regionFeatures &&
        this.getRegionFeatureStatus(regionFeatures.ssh) === 'docked';
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  webssh(privateIp: string) {
    const integrationUuid = get(
      this.regionFeatures,
      'ssh.config.integration_uuid',
    );
    this.integrationCenterService
      .getIntegrationById(integrationUuid)
      .then(setting => {
        const modelRef = this.dialogService.open(WebSSHComponent, {
          data: {
            node: this.nodeVm.name,
            endpoint: get(setting, 'fields.endpoint'),
            remote_url: privateIp,
          },
        });
        modelRef.componentInstance.close.subscribe(() => {
          modelRef.close();
        });
      });
  }

  async deleteNode(nodeName: string) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('region_node_delete'),
        content: this.translateService.get(
          `region_node_maintain_delete_confirm_message`,
        ),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      await this.region.deleteNodeV2(this.clusterName, nodeName);
      this.router.navigate([
        '/console/admin/cluster/detail/' + this.clusterName,
      ]);
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  updateNodeTag(node: NodeVm) {
    const dialogRef = this.dialogService.open(UpdateNodeLabelsComponent, {
      data: {
        cluster: this.clusterName,
        node,
      },
    });

    dialogRef.componentInstance.close.subscribe((updated: boolean) => {
      dialogRef.close();
      if (updated) {
        this.getNode();
      }
    });
  }

  async doAction(node: NodeVm, action: string) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('region_node_' + action),
        content: this.translateService.get(
          `region_node_maintain_${action}_confirm_message`,
        ),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      if (action === 'delete') {
        await this.region.deleteNodeV2(this.clusterName, node.name);
      } else {
        await this.region.actionNodeV2(this.clusterName, node.name, action);
      }
      this.getNode();
    } catch (e) {
      this.errorsToast.error(e);
    }
  }
}
