import { ConfirmType, DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { KubernetesResource } from 'app/typings/raw-k8s';
import { updateActions } from 'app/utils/code-editor-config';
import { safeDump, safeLoad } from 'js-yaml';

interface Pod {
  kubernetes?: KubernetesResource;
  resource_actions?: string[];
}

@Component({
  templateUrl: './node-pods-update.component.html',
  styleUrls: ['./node-pods-update.component.scss'],
})
export class NodePodsUpdateComponent implements OnInit {
  edtiorActions = updateActions;
  editorOptions = {
    language: 'yaml',
    readOnly: false,
    renderLineHighlight: 'none',
  };
  yamlInputValue = '';
  originalYaml = '';
  clusterName: string;
  privateIp: string;
  resourceData: Pod;
  updatePayload: Pod['kubernetes'];

  @ViewChild('form')
  form: NgForm;

  constructor(
    private router: Router,
    private k8sResourceService: K8sResourceService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private route: ActivatedRoute,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnInit(): void {
    this.route.paramMap.subscribe(async params => {
      this.clusterName = params.get('clusterName');
      this.privateIp = params.get('privateIp');
      this.k8sResourceService
        .getK8sResources('pods', {
          clusterName: this.clusterName,
          namespace: params.get('namespace'),
          name: params.get('name'),
        })
        .then(result => {
          this.resourceData = result[0];
          this.yamlInputValue = safeDump(result[0].kubernetes, {
            lineWidth: 9999,
          });
          this.originalYaml = this.yamlInputValue;
        });
    });
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    try {
      this.updatePayload = safeLoad(this.yamlInputValue);
    } catch (error) {
      this.auiNotificationService.error(this.translate.get('yaml_format_hint'));
      return;
    }

    this.dialogService
      .confirm({
        title: this.translate.get('confirm_update_resource_content'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Primary,
        beforeConfirm: this.updateResource(),
      })
      .then(() => {
        this.router.navigate(['../../../'], {
          relativeTo: this.route,
        });
      })
      .catch(() => null);
  }

  cancel() {
    if (this.yamlInputValue) {
      this.dialogService
        .confirm({
          title: this.translate.get('cancel_resource_update_content'),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
          confirmType: ConfirmType.Warning,
        })
        .then(() => {
          this.router.navigate(['../../../'], {
            relativeTo: this.route,
          });
        })
        .catch(() => null);
    } else {
      this.router.navigate(['../../../'], {
        relativeTo: this.route,
      });
    }
  }

  private updateResource() {
    return (resolve: () => void, reject: () => void) => {
      this.k8sResourceService
        .updateK8sReource(this.clusterName, 'pods', this.updatePayload)
        .then(() => resolve())
        .catch(err => {
          reject();
          this.errorsToastService.error(err);
        });
    };
  }
}
