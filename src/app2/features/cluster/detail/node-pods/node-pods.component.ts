import { ConfirmType, DialogService, NotificationService } from '@alauda/ui';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions } from 'app/typings';
import { KubernetesResource } from 'app/typings/raw-k8s';
import { GenericStatus } from 'app/typings/status';
import { UpdateNodePodsLabelsComponent } from 'app2/features/cluster/detail/update-node-pods-labels/update-node-pods-labels.component';

interface Pod {
  kubernetes?: KubernetesResource;
  resource_actions?: string[];
}

enum Status {
  Failed = 'Failed',
  Succeeded = 'Succeeded',
  Running = 'Running',
  Pending = 'Pending',
  Unknown = 'Unknown',
}

@Component({
  selector: 'rc-node-pods',
  templateUrl: 'node-pods.component.html',
  styleUrls: ['./node-pods.component.scss'],
})
export class NodePodsComponent implements OnInit {
  @Input()
  clusterName: string;
  @Input()
  nodeName: string;

  loading = false;
  initialized = false;
  pagination = {
    count: 0,
    size: 10,
    current: 1,
    pages: 0,
  };
  columnDef = [
    'name',
    'labels',
    'status',
    'reload_times',
    'namespace',
    'created_at',
    'action',
  ];
  pods: Array<any>;

  constructor(
    private k8sResourceService: K8sResourceService,
    private roleUtil: RoleUtilitiesService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
    private dialogService: DialogService,
  ) {}

  ngOnInit() {
    this.loadPods();
  }

  async loadPods() {
    try {
      const result = await this.k8sResourceService.getK8sResourcesPaged(
        'pods',
        {
          clusterName: this.clusterName,
          fieldSelector: `spec.nodeName=${this.nodeName}`,
          page: this.pagination.current,
          page_size: this.pagination.size,
        },
      );
      this.pods = result.results;
      this.pagination.count = result.count;
      this.pagination.pages = result.num_pages;
    } catch (e) {
      this.pods = [];
    } finally {
      this.initialized = true;
    }
  }

  getPodStatus(status: Status) {
    switch (status) {
      case Status.Succeeded:
        return GenericStatus.Success;
      case Status.Pending:
        return GenericStatus.Pending;
      case Status.Failed:
        return GenericStatus.Error;
      case Status.Running:
        return GenericStatus.Success;
      default:
        return GenericStatus.Unknown;
    }
  }

  currentPageChange(_currentPage: number) {
    this.loadPods();
  }

  pageSizeChange(size: number) {
    if (Math.ceil(this.pagination.count / size) < this.pagination.pages) {
      this.pagination.current = 1;
    }
    this.loadPods();
  }

  async updateAction(item: Pod) {
    this.router.navigate(
      [
        `./update/${item.kubernetes.metadata.namespace}/${item.kubernetes.metadata.name}`,
      ],
      {
        relativeTo: this.activatedRoute,
      },
    );
  }

  updateLabelAction(item: { kubernetes: any; resource_actions: any }) {
    const dialogRef = this.dialogService.open(UpdateNodePodsLabelsComponent, {
      data: {
        kubernetes: item.kubernetes,
        clusterName: this.clusterName,
      },
    });
    dialogRef.afterClosed().subscribe(result => !!result && this.loadPods());
  }

  deleteAction(item: K8sResourceWithActions<KubernetesResource>) {
    this.dialogService
      .confirm({
        title: this.translateService.get('confirm_delete_resource_content', {
          kind: 'pod',
          name: item.kubernetes.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
        confirmType: ConfirmType.Danger,
        beforeConfirm: (resolve, reject) => {
          this.k8sResourceService
            .deleteK8sReource(this.clusterName, 'pods', item.kubernetes)
            .then(() => {
              resolve();
              this.auiNotificationService.success(
                this.translateService.get('delete_success'),
              );
            })
            .catch(err => {
              reject();
              this.errorsToastService.error(err);
            });
        },
      })
      .then(() => {
        this.loadPods();
      })
      .catch(() => null);
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'service', action);
  }
}
