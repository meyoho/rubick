import { DialogService, PageEvent } from '@alauda/ui';
import { Component, Input, OnInit } from '@angular/core';

import { get } from 'lodash-es';
import { BehaviorSubject, combineLatest, ReplaySubject } from 'rxjs';
import { debounceTime, map, publishReplay, refCount } from 'rxjs/operators';

import { IntegrationCenterService } from 'app/services/api/integration-center.service';
import {
  ClusterNode,
  ClusterNodes,
  NodeVm,
  RegionFeature,
  RegionService,
} from 'app/services/api/region.service';
import { RegionUtilitiesService } from 'app/services/api/region.utilities.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { UpdateNodeLabelsComponent } from 'app2/features/cluster/detail/update-node-labels/update-node-labels.component';
import { WebSSHComponent } from 'app2/features/cluster/detail/web-ssh/component';
import { formatCommonUnit, formatNumUnit } from 'app2/features/cluster/utils';
import { getRegionFeatureStatus } from '../../utils';

@Component({
  selector: 'rc-cluster-nodes',
  templateUrl: './cluster-nodes.component.html',
  styleUrls: ['./cluster-nodes.component.scss'],
})
export class ClusterNodesComponent implements OnInit {
  @Input()
  clusterName: string;

  columns = [
    'name',
    'privateIp',
    'nodeType',
    'nodeTags',
    'status',
    'cpu',
    'memory',
    'action',
  ];

  filterIp = '';
  filterIp$ = new BehaviorSubject<string>(this.filterIp);
  nodeVms$ = new ReplaySubject<NodeVm[]>();
  pageEvent$ = new BehaviorSubject<PageEvent>({
    pageIndex: 0,
    pageSize: 10,
    previousPageIndex: 0,
    length: 0,
  });
  nodeCanUpdate = false;
  nodeCanDelete = false;
  regionFeatures: {
    ssh: RegionFeature;
  };
  getRegionFeatureStatus = getRegionFeatureStatus;

  filteredNodes$ = combineLatest(this.filterIp$, this.nodeVms$).pipe(
    debounceTime(100),
    map(([filterIp, nodeVms]) => {
      filterIp = filterIp.trim();
      return filterIp
        ? nodeVms.filter(nodeVm => nodeVm.privateIp.includes(filterIp))
        : nodeVms;
    }),
    publishReplay(),
    refCount(),
  );

  filertedAndPagedNodes$ = combineLatest(
    this.filteredNodes$,
    this.pageEvent$,
  ).pipe(
    map(([filteredNodes, pageEvent]) => {
      pageEvent.pageIndex =
        pageEvent.pageSize > pageEvent.length ? 0 : pageEvent.pageIndex;
      return filteredNodes.slice(
        pageEvent.pageIndex * pageEvent.pageSize,
        (pageEvent.pageIndex + 1) * pageEvent.pageSize,
      );
    }),
    publishReplay(),
    refCount(),
  );

  constructor(
    private regionService: RegionService,
    private regionUtilities: RegionUtilitiesService,
    private integrationCenterService: IntegrationCenterService,
    private errorsToast: ErrorsToastService,
    private translateService: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private dialogService: DialogService,
  ) {}

  ngOnInit() {
    this.getNodes();
    this.getRegionFeature();
  }

  async getRegionFeature() {
    try {
      this.regionFeatures = await this.regionService.getRegionFeatures(
        this.clusterName,
      );
    } catch (e) {
      this.regionFeatures = { ssh: null };
      this.errorsToast.error(e);
    }
  }

  webssh(nodeVm: NodeVm) {
    const integrationUuid = get(
      this.regionFeatures,
      'ssh.config.integration_uuid',
    );
    this.integrationCenterService
      .getIntegrationById(integrationUuid)
      .then(setting => {
        const modelRef = this.dialogService.open(WebSSHComponent, {
          data: {
            node: nodeVm.name,
            endpoint: get(setting, 'fields.endpoint'),
            remote_url: nodeVm.privateIp,
          },
        });
        modelRef.componentInstance.close.subscribe(() => {
          modelRef.close();
        });
      });
  }

  async getNodes() {
    let nodes: ClusterNode[];

    try {
      const res: ClusterNodes = await this.regionService.getClusterNodes(
        this.clusterName,
      );
      nodes = res.items;
      this.nodeCanUpdate = this.roleUtil.resourceHasPermission(
        res,
        'cluster_node',
        'update',
      );
      this.nodeCanDelete = this.roleUtil.resourceHasPermission(
        res,
        'cluster_node',
        'delete',
      );
    } catch (e) {
      this.errorsToast.error(e);
      this.nodeVms$.next([]);
      return;
    }

    this.nodeVms$.next(
      nodes.map(
        node =>
          ({
            name: node.metadata.name,
            privateIp: node.status.addresses.find(
              addr => addr.type === 'InternalIP',
            ).address,
            labels: node.metadata.labels,
            status: {
              state: this.regionUtilities.getNodeState(node),
              schedulable: !node.spec.unschedulable,
              taint: node.spec.taints && !!node.spec.taints.length,
            },
            cpu: formatNumUnit(node.status.allocatable.cpu),
            memory: formatCommonUnit(node.status.allocatable.memory, true),
            type:
              'node-role.kubernetes.io/master' in node.metadata.labels
                ? 'master'
                : 'node',
          } as NodeVm),
      ),
    );
  }

  getLabels(labels: { [key: string]: string }) {
    return Object.entries(labels).reduce(
      (prev, [name, value]) => {
        prev.push({ name, value });
        return prev;
      },
      [] as Array<{
        name: string;
        value: string;
      }>,
    );
  }

  updateNodeTag(node: NodeVm) {
    const dialogRef = this.dialogService.open(UpdateNodeLabelsComponent, {
      data: {
        cluster: this.clusterName,
        node,
      },
    });

    dialogRef.componentInstance.close.subscribe((updated: boolean) => {
      dialogRef.close();
      if (updated) {
        this.getNodes();
      }
    });
  }

  async doAction(node: NodeVm, action: string) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('region_node_' + action),
        content: this.translateService.get(
          `region_node_maintain_${action}_confirm_message`,
        ),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      if (action === 'delete') {
        await this.regionService.deleteNodeV2(this.clusterName, node.name);
      } else {
        await this.regionService.actionNodeV2(
          this.clusterName,
          node.name,
          action,
        );
      }
      this.getNodes();
    } catch (e) {
      this.errorsToast.error(e);
    }
  }
}
