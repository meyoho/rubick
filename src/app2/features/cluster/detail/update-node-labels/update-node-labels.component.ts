import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, ViewChild } from '@angular/core';
import { NgForm, Validators } from '@angular/forms';

import { cloneDeep, mergeWith } from 'lodash-es';

import { NodeVm, RegionService } from 'app/services/api/region.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  K8S_RESOURCE_LABEL_KEY_DOMAIN_PATTERN,
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
} from 'app/utils/patterns';
import { k8sResourceLabelKeyValidator } from 'app/utils/validator';

@Component({
  templateUrl: 'update-node-labels.component.html',
})
export class UpdateNodeLabelsComponent {
  labels: { [key: string]: string };
  close = new EventEmitter();
  submitting = false;
  @ViewChild('form')
  form: NgForm;
  labelValidator = {
    key: [k8sResourceLabelKeyValidator, Validators.maxLength(63)],
    value: [
      Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern),
      Validators.maxLength(63),
    ],
  };
  labelErrorMapper = {
    key: {
      pattern: this.translate.get('invalid_pattern'),
      namePattern: this.translate.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip),
      domainPattern: this.translate.get(
        K8S_RESOURCE_LABEL_KEY_DOMAIN_PATTERN.tip,
      ),
    },
    value: {
      pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
    },
  };

  constructor(
    @Inject(DIALOG_DATA) public modalData: { node: NodeVm; cluster: string },
    private regionService: RegionService,
    private translate: TranslateService,
  ) {
    this.labels = cloneDeep(modalData.node.labels);
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    // todo: 推进后端优化 api 后可以删除 mergeWith
    const labels = mergeWith(this.labels, this.modalData.node.labels, obj => {
      if (obj === undefined) {
        return null;
      } else {
        return obj;
      }
    });
    try {
      await this.regionService.updateNodeLabelV2(
        this.modalData.cluster,
        this.modalData.node.name,
        { labels },
      );
      this.close.next(true);
    } catch (err) {
    } finally {
      this.submitting = false;
    }
  }

  cancel() {
    this.close.next();
  }
}
