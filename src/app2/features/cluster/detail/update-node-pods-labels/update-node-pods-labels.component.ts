import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject, ViewChild } from '@angular/core';
import { NgForm, Validators } from '@angular/forms';

import { cloneDeep } from 'lodash-es';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { TranslateService } from 'app/translate/translate.service';
import { KubernetesResource, StringMap } from 'app/typings/raw-k8s';
import {
  K8S_RESOURCE_LABEL_KEY_DOMAIN_PATTERN,
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
} from 'app/utils/patterns';
import { k8sResourceLabelKeyValidator } from 'app/utils/validator';

@Component({
  templateUrl: 'update-node-pods-labels.component.html',
})
export class UpdateNodePodsLabelsComponent {
  labels: StringMap;
  @ViewChild('form')
  form: NgForm;
  submitting = false;
  labelValidator = {
    key: [k8sResourceLabelKeyValidator],
    value: [Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern)],
  };
  labelErrorMapper = {
    key: {
      pattern: this.translate.get('invalid_pattern'),
      namePattern: this.translate.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip),
      domainPattern: this.translate.get(
        K8S_RESOURCE_LABEL_KEY_DOMAIN_PATTERN.tip,
      ),
    },
    value: {
      pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
    },
  };

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: { kubernetes: KubernetesResource; clusterName: string },
    private dialogRef: DialogRef,
    private k8sResourceService: K8sResourceService,
    private translate: TranslateService,
  ) {
    this.labels = cloneDeep(modalData.kubernetes.metadata.labels);
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    try {
      this.submitting = true;
      this.modalData.kubernetes.metadata.labels = this.labels;
      await this.k8sResourceService.updateK8sReource(
        this.modalData.clusterName,
        'pods',
        this.modalData.kubernetes,
      );
      this.dialogRef.close(true);
    } catch (err) {
    } finally {
      this.submitting = false;
    }
  }
}
