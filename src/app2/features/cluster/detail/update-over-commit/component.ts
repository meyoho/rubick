import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { OverCommit, RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { POSITIVE_INT_PATTERN } from 'app/utils/patterns';

interface OverCommitForm extends OverCommit {
  cpuEnabled: boolean;
  memEnabled: boolean;
}

@Component({
  templateUrl: 'template.html',
})
export class UpdateOverCommitComponent implements OnInit {
  @Output()
  close: EventEmitter<boolean> = new EventEmitter();
  confirming: boolean;
  model: OverCommitForm;

  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: {
      cluster: string;
      overCommit: OverCommit;
    },
    private errorsToastService: ErrorsToastService,
    private regionService: RegionService,
  ) {
    this.model = {
      cpu: this.modalData.overCommit.cpu || 1,
      mem: this.modalData.overCommit.mem || 1,
      cpuEnabled: !!this.modalData.overCommit.cpu,
      memEnabled: !!this.modalData.overCommit.mem,
    };
  }

  async ngOnInit() {}

  async submitForm(formRef: NgForm) {
    if (formRef.form.invalid) {
      return;
    }
    this.confirming = true;
    const payload = {
      cpu: this.model.cpuEnabled ? +this.model.cpu : 0,
      mem: this.model.memEnabled ? +this.model.mem : 0,
    };
    try {
      await this.regionService.updateOverCommit(
        this.modalData.cluster,
        payload,
      );
      this.close.next(true);
    } catch (e) {
      this.errorsToastService.error(e);
      this.close.next(false);
    } finally {
      this.confirming = false;
    }
  }
}
