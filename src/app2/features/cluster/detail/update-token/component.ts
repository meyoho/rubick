import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import {
  ClusterAccessData,
  RegionService,
} from 'app/services/api/region.service';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateTokenComponent {
  @ViewChild('form')
  form: NgForm;

  submitting = false;
  token: string;

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: { cluster: ClusterAccessData; clusterName: string },
    private region: RegionService,
    private dialogRef: DialogRef,
    private cdr: ChangeDetectorRef,
  ) {}

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    this.modalData.cluster.attr.kubernetes.token = this.token;
    this.region
      .updateCluster(this.modalData.clusterName, this.modalData.cluster)
      .then(() => {
        this.dialogRef.close(true);
      })
      .finally(() => {
        this.submitting = false;
        this.cdr.markForCheck();
      });
  }

  cancel() {
    this.dialogRef.close(false);
  }
}
