import { DIALOG_DATA, NotificationService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { TerminalService } from 'app/services/api/terminal.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { IP_ADDRESS_PATTERN } from 'app/utils/patterns';

import { WebSSHService } from '../webssh.service';

interface WebSSHForm {
  node_address: string;
  port: string;
  username: string;
  password: string;
}

@Component({
  templateUrl: 'template.html',
})
export class WebSSHComponent {
  @ViewChild('form')
  form: NgForm;
  @Output()
  close: EventEmitter<boolean> = new EventEmitter();
  confirming: boolean;
  model: WebSSHForm;
  node: string;

  IP_ADDRESS_PATTERN = IP_ADDRESS_PATTERN;

  constructor(
    @Inject(DIALOG_DATA)
    private modalData: {
      node: string;
      endpoint: string;
      remote_url: string;
    },
    private errorToastService: ErrorsToastService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private terminalService: TerminalService,
    private webSSHService: WebSSHService,
  ) {
    this.node = this.modalData.node;
    this.model = {
      node_address: this.modalData.remote_url || '',
      port: '22',
      username: '',
      password: '',
    };
  }

  async submitForm(formRef: NgForm) {
    this.form.onSubmit(null);
    if (formRef.form.invalid) {
      return;
    }
    this.confirming = true;
    this.webSSHService
      .getWebSocketEndpoint(
        // 判断当前的http协议是否加密，如果加密，请求使用https
        (window.location.protocol === 'https:' ? 'https://' : 'http://') +
          `${this.modalData.endpoint}/v1/console/login?node_address=${
            this.model.node_address
          }&username=${this.model.username}&password=${
            this.model.password
          }&port=${this.model.port}`,
      )
      .then(data => {
        this.confirming = false;
        if (data['ok'] === true) {
          this.close.next();
          this.terminalService.openWebSSHTerminal(
            data['data'],
            this.modalData.endpoint,
          );
        } else if (data['ok'] === false) {
          this.auiNotificationService.error(
            this.translate.get('webssh_login_error'),
          );
        } else {
          this.auiNotificationService.error(
            this.translate.get('webssh_conf_error'),
          );
        }
      })
      .catch(err => {
        this.confirming = false;
        this.errorToastService.error(err);
      });
  }
}
