import { Injectable } from '@angular/core';
import { HttpService } from 'app/services/http.service';

@Injectable()
export class WebSSHService {
  WEB_SSH_URL: string;

  constructor(private httpService: HttpService) {
    this.WEB_SSH_URL = '/ajax-sp/webssh';
  }

  getWebSocketEndpoint(url: string): Promise<any> {
    return this.httpService.request(this.WEB_SSH_URL, {
      method: 'GET',
      params: {
        url,
      },
    });
  }
}
