import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
} from '@angular/core';
import { Store } from '@ngrx/store';

import { get } from 'lodash-es';
import { BehaviorSubject, combineLatest } from 'rxjs';
import { filter, map, publishReplay, refCount, tap } from 'rxjs/operators';

import {
  Metric,
  MetricQueries,
  MetricService,
} from 'app/services/api/metric.service';
import {
  Cluster,
  ClusterNode,
  RegionStatus,
} from 'app/services/api/region.service';
import {
  ClusterStates,
  RegionUtilitiesService,
} from 'app/services/api/region.utilities.service';
import * as fromStore from 'app/store';
const uuidv4 = require('uuid/v4');


const CLUSTER_STATUS_MAP = {
  running: {
    status: 'running',
    icon: 'check_s',
    color: '#1bb393',
  },
  deploying: {
    status: 'deploying',
    icon: 'basic:sync',
    color: '#009ce3',
  },
  error: {
    status: 'error',
    icon: 'basic:exclamation',
    color: '#f34235',
  },
};

@Component({
  selector: 'rc-cluster-card',
  templateUrl: './cluster-card.component.html',
  styleUrls: ['./cluster-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ClusterCardComponent implements OnDestroy {
  private _cluster: Cluster;
  private intervalTimer: number;
  cpu_utilization: string;
  mem_utilization: string;
  metric_name = {
    'cpu.utilization': {
      __query_id__: '',
    },
    'memory.utilization': {
      __query_id__: '',
    },
  };

  get cluster() {
    return this._cluster;
  }

  @Input()
  set cluster(cluster) {
    this._cluster = cluster;
    this.cluster$.next(cluster);
  }

  cluster$ = new BehaviorSubject(this.cluster);

  ngOnDestroy() {
    window.clearInterval(this.intervalTimer);
  }

  clusterStatus$ = this.cluster$.pipe(
    map(cluster => {
      let status = 'error';

      switch (cluster.state) {
        case RegionStatus.Running:
        case RegionStatus.Deploying:
          status = this.cluster.state.toLowerCase();
          break;
      }

      return CLUSTER_STATUS_MAP[status];
    }),
  );

  clusterNodes$ = combineLatest(
    this.store.select(fromStore.getNodesState),
    this.cluster$.pipe(
      tap(cluster =>
        this.store.dispatch(
          new fromStore.GetNodesByRegion({
            regionName: cluster.name,
            isK8sV4: true,
          }),
        ),
      ),
    ),
  ).pipe(
    map(([state, region]) => state[region.name]),
    filter(data => !!data),
    map(state => state.ips.map(ip => state.entities[ip])),
    publishReplay(1),
    refCount(),
  );

  masterNodes$ = this.clusterNodes$.pipe(
    map(nodes =>
      nodes.filter(node => {
        return 'node-role.kubernetes.io/master' in node.metadata.labels;
      }),
    ),
  );

  unnormalNodes$ = this.clusterNodes$.pipe(
    map(nodes => nodes.filter(this.isUnnormalNode)),
  );

  isUnnormalNode = (node: ClusterNode) => {
    return [ClusterStates.NotReady, ClusterStates.Warning].includes(
      this.regionUtilities.getNodeState(node),
    );
  };

  private queryClusterMetric(clusterName: string) {
    const queries = ['cpu.utilization', 'memory.utilization'].map(
      metricType => {
        const uuid = uuidv4();
        this.metric_name[metricType].__query_id__ = uuid;
        const metricQuery: MetricQueries = {
          aggregator: 'avg',
          id: uuid,
        };
        metricQuery.labels = [
          {
            type: 'EQUAL',
            name: '__name__',
            value: `cluster.${metricType}`,
          },
          {
            type: 'EQUAL',
            name: 'kind',
            value: 'Cluster',
          },
        ];
        return metricQuery;
      },
    );
    this.metricService
      .queryMetric(clusterName, {
        time: Math.floor(Date.now() / 1000),
        queries,
      })
      .then(results => {
        results.forEach(result => {
          if (
            result.metric.__query_id__ ===
            this.metric_name['cpu.utilization'].__query_id__
          ) {
            this.cpu_utilization = this.parseMetricsResponse(result);
          } else if (
            result.metric.__query_id__ ===
            this.metric_name['memory.utilization'].__query_id__
          ) {
            this.mem_utilization = this.parseMetricsResponse(result);
          }
        });
      })
      .catch(() => {
        this.cpu_utilization = null;
        this.mem_utilization = null;
      })
      .finally(() => {
        this.cdr.markForCheck();
      });
  }

  private parseMetricsResponse(metric: Metric) {
    return (metric.value[1] * 100).toFixed(2);
  }

  constructor(
    private cdr: ChangeDetectorRef,
    private store: Store<fromStore.AppState>,
    private regionUtilities: RegionUtilitiesService,
    private metricService: MetricService,
  ) {
    this.cluster$.subscribe(cluster => {
      if (cluster && get(cluster, 'features.metric.type') === 'prometheus') {
        if (!this.intervalTimer) {
          this.intervalTimer = window.setInterval(
            () => this.queryClusterMetric(cluster.name),
            60000,
          );
          this.queryClusterMetric(cluster.name);
        }
      }
    });
  }
}
