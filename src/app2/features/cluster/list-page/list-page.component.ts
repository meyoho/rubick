import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';

import { BehaviorSubject, combineLatest, Observable } from 'rxjs';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { Cluster } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ENVIRONMENTS, WEBLABS } from 'app/shared/tokens';
import { AppState } from 'app/store';
import * as clusterActions from 'app/store/actions/clusters.actions';
import * as clusterSelectors from 'app/store/selectors/clusters.selectors';
import { Environments, Weblabs } from 'app/typings';

@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
})
export class ListPageComponent implements OnInit, OnDestroy {
  filter$ = new BehaviorSubject('');

  clusters$: Observable<Array<Cluster>>;

  hasCreatePermission = false;
  private intervalTimer: number;

  constructor(
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ENVIRONMENTS) public environments: Environments,
    private store: Store<AppState>,
    private roleUtilities: RoleUtilitiesService,
  ) {}

  async ngOnInit() {
    this.initTimer();
    this.clusters$ = combineLatest(
      this.store.select(clusterSelectors.getAllClusters),
      this.filter$.pipe(
        map(filter => filter.trim().toLowerCase()),
        distinctUntilChanged(),
      ),
    ).pipe(
      map(([clusters, filter]) =>
        filter
          ? clusters.filter(({ display_name }) => display_name.includes(filter))
          : clusters,
      ),
    );
    this.hasCreatePermission = await this.roleUtilities.resourceTypeSupportPermissions(
      'cluster',
    );
  }

  ngOnDestroy() {
    this.clearTimeout();
  }

  refreshRegions() {
    this.store.dispatch(new clusterActions.GetClusters());
  }

  private initTimer() {
    this.intervalTimer = window.setInterval(() => {
      this.refreshRegions();
    }, 60000);
  }

  private clearTimeout() {
    window.clearInterval(this.intervalTimer);
  }
}
