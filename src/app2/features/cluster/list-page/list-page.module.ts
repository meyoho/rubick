import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { ClusterCardComponent } from './cluster-card/cluster-card.component';
import { ClusterListComponent } from './cluster-list/cluster-list.component';
import { ListPageComponent } from './list-page.component';

@NgModule({
  imports: [SharedModule, RouterModule],
  declarations: [ListPageComponent, ClusterListComponent, ClusterCardComponent],
  exports: [ClusterListComponent, ClusterCardComponent],
})
export class ListPageModule {}
