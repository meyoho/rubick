import { DialogService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { first } from 'rxjs/operators';

import { RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import * as clusterActions from 'app/store/actions/clusters.actions';
import { TranslateService } from 'app/translate/translate.service';
import {
  Mirror,
  MirrorService,
} from 'app2/features/cluster/mirror-page/mirror.service';

@Component({
  templateUrl: './mirror-detail.component.html',
  styleUrls: ['./mirror-detail.component.scss'],
})
export class MirrorDetailComponent {
  name: string;

  loading: boolean;
  initialized: boolean;
  mirror: Mirror;

  columns = ['region_name', 'create_time'];

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private mirrorService: MirrorService,
    private roleUtilities: RoleUtilitiesService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private errorsToast: ErrorsToastService,
    private regionService: RegionService,
    private actions: Actions,
    private location: Location,
  ) {
    this.route.paramMap.subscribe(params => {
      this.name = params.get('name');
      this.lodaData();
    });
  }

  async lodaData() {
    try {
      this.mirror = await this.mirrorService.getMirror(this.name);
      this.initialized = true;
    } catch (e) {
      this.errorsToast.error(e);
      if (!this.initialized && e.status === 404) {
        this.location.back();
      }
    }
  }

  showButton(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.mirror,
      'cluster',
      action,
    );
  }

  async deleteMirror() {
    const { name } = this.mirror;

    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('confirm_delete_mirror_cluster', {
          name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.loading = true;

    try {
      await this.mirrorService.deleteMirror(name);
    } catch (e) {
      return this.errorsToast.error(e);
    } finally {
      this.loading = false;
    }

    this.regionService.refetch();

    this.actions
      .pipe(ofType(clusterActions.GET_CLUSTERS_SUCCESS))
      .pipe(first())
      .subscribe(() => {
        this.router.navigate(['/console/admin/cluster/mirror']);
      });
  }
}
