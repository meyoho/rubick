import { NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { AppState } from 'app/store';
import * as clusterActions from 'app/store/actions/clusters.actions';
import * as clusterSelectors from 'app/store/selectors/clusters.selectors';
import { TranslateService } from 'app/translate/translate.service';
import {
  Mirror,
  MirrorService,
} from 'app2/features/cluster/mirror-page/mirror.service';
import { delay } from 'app2/utils';
import { Observable, Subject, combineLatest } from 'rxjs';
import { first, map } from 'rxjs/operators';

export const MIRROR_COLORS = [
  '#37d9f0',
  '#4da8ee',
  '#aa17d0',
  '#82ccd2',
  '#89b0cd',
  '#9389b0',
  '#def3f3',
  '#cde0ef',
  '#dfdbec',
  '#00a3af',
  '#3876a6',
  '#765c83',
];

@Component({
  templateUrl: './mirror-edit.component.html',
  styleUrls: ['./mirror-edit.component.scss'],
})
export class MirrorEditComponent implements OnInit {
  isUpdating: boolean;

  name: string;
  allColors = MIRROR_COLORS;
  mirror = {} as Mirror;
  disabledColors: string[] = [];
  loading: boolean;
  namePattern = {
    pattern: '^[a-z][a-z0-9_]*$',
    tip: 'mirror_cluster_name_tip',
  };

  mirror$ = new Subject<Mirror>();
  regions$: Observable<Cluster[]>;

  constructor(
    private router: Router,
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private location: Location,
    private mirrorService: MirrorService,
    private auiNotificationService: NotificationService,
    private errorsToast: ErrorsToastService,
    private region: RegionService,
    private translate: TranslateService,
    private actions: Actions,
  ) {}

  ngOnInit() {
    this.route.paramMap.subscribe(async params => {
      this.name = params.get('name');
      this.isUpdating = !!this.name;

      if (this.isUpdating) {
        this.mirror = await this.mirrorService.getMirror(this.name);
      } else {
        this.mirror = {
          regions: [],
          flag: MIRROR_COLORS[0],
        } as Mirror;
        await delay(0);
      }

      this.mirror$.next(this.mirror);
    });

    this.regions$ = combineLatest(
      this.store.select(clusterSelectors.getAllClusters) as Observable<
        Cluster[]
      >,
      this.mirror$,
    ).pipe(
      map(([regions, mirror]) => {
        const result: Cluster[] = [];
        regions.forEach(region => {
          if (region.platform_version !== 'v4') {
            return;
          }

          if (
            'mirror' in region &&
            region.mirror &&
            region.mirror.name &&
            (!this.isUpdating || mirror.flag !== region.mirror.flag)
          ) {
            this.disabledColors.push(region.mirror.flag);
          }

          if (
            !('mirror' in region && region.mirror && region.mirror.name) ||
            region.mirror.name === mirror.name
          ) {
            result.push(region as Cluster);
          }
        });

        if (
          !this.isUpdating &&
          this.disabledColors.includes(this.mirror.flag)
        ) {
          this.mirror.flag = this.allColors.find(
            color => !this.disabledColors.includes(color),
          );
        }
        return result;
      }),
    );
  }

  cancel() {
    this.location.back();
  }

  trackFn(item: Cluster) {
    return item && item.id;
  }

  async confirm(ngForm: NgForm) {
    if (ngForm.invalid) {
      return;
    }

    if (this.mirror.regions.length < 2) {
      return this.auiNotificationService.error(
        this.translate.get('mirror_regions_limit'),
      );
    }

    this.loading = true;

    try {
      await this.mirrorService[
        this.isUpdating ? 'updateMirror' : 'createMirror'
      ](this.mirror);
    } catch (e) {
      this.errorsToast.error(e);
      this.loading = false;
      return;
    }

    this.region.refetch();

    this.actions
      .pipe(ofType(clusterActions.GET_CLUSTERS_SUCCESS))
      .pipe(first())
      .subscribe(() => {
        this.loading = false;
        this.router.navigate([
          '/console/admin/cluster/mirror/detail',
          this.mirror.name,
        ]);
      });
  }
}
