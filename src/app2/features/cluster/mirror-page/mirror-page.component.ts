import { DialogService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { Actions, ofType } from '@ngrx/effects';
import { Observable, Subject, combineLatest, from } from 'rxjs';
import { catchError, first, map, startWith } from 'rxjs/operators';

import { RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import * as clusterActions from 'app/store/actions/clusters.actions';
import { TranslateService } from 'app/translate/translate.service';

import { Mirror as _Mirror, MirrorService } from './mirror.service';

interface Mirror extends _Mirror {
  displayedRegions?: _Mirror['regions'];
}

@Component({
  templateUrl: './mirror-page.component.html',
  styleUrls: ['./mirror-page.component.scss'],
})
export class MirrorPageComponent implements OnInit {
  columns = ['name', 'member', 'create_time', 'action'];
  loading: boolean;
  hasCreatePermission: boolean;

  mirrors$: Observable<Mirror[]>;
  keyword$ = new Subject<string>();

  constructor(
    private mirror: MirrorService,
    private roleUtilities: RoleUtilitiesService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private errorsToast: ErrorsToastService,
    private regionService: RegionService,
    private actions: Actions,
  ) {}

  trackByFn(mirror: Mirror) {
    return mirror.name;
  }

  async ngOnInit() {
    this.mirrors$ = combineLatest(
      from(
        this.mirror.getMirrors().then(mirrors =>
          mirrors.map(mirror =>
            Object.assign(mirror, {
              displayedRegions: mirror.regions.slice(0, 3),
            }),
          ),
        ),
      ),
      this.keyword$.pipe(startWith(null)),
    ).pipe(
      map(([mirrors, keyword]) => {
        keyword = keyword && keyword.trim();
        return keyword
          ? mirrors.filter(
              ({ name, display_name }) =>
                name.includes(keyword) || display_name.includes(keyword),
            )
          : mirrors;
      }),
      catchError(() => []),
    );

    this.hasCreatePermission = await this.roleUtilities.resourceTypeSupportPermissions(
      'cluster',
    );
  }

  isButtonDisabled(mirror: Mirror, action: string) {
    return !this.roleUtilities.resourceHasPermission(mirror, 'cluster', action);
  }

  async deleteMirror(mirror: Mirror) {
    const { name } = mirror;
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('confirm_delete_mirror_cluster', {
          name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.loading = true;

    try {
      await this.mirror.deleteMirror(name);
    } catch (e) {
      this.errorsToast.error(e);
      this.loading = false;
      return;
    }

    this.mirrors$ = this.mirrors$.pipe(
      map(mirrors => mirrors.filter(({ name }) => name !== mirror.name)),
    );

    this.regionService.refetch();

    this.actions
      .pipe(ofType(clusterActions.GET_CLUSTERS_SUCCESS))
      .pipe(first())
      .subscribe(() => {
        this.loading = false;
      });
  }
}
