import { Inject, Injectable } from '@angular/core';

import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

export interface Mirror {
  name: string;
  display_name: string;
  description: string;
  flag: string;
  created_at: string;
  regions: Array<{
    name: string;
    display_name?: string;
    id: string;
    created_at?: string;
  }>;
  resource_actions: string[];
}

@Injectable()
export class MirrorService {
  MIRRORS_URL = '/ajax/v2/mirrors/' + this.account.namespace + '/';

  constructor(
    @Inject(ACCOUNT) private account: Account,
    private http: HttpService,
  ) {}

  getMirrors(): Promise<Mirror[]> {
    return this.http.request(this.MIRRORS_URL).then(({ result }) => result);
  }

  getMirror(name: string) {
    return this.http.request<Mirror>(this.MIRRORS_URL + name);
  }

  createMirror(mirror: Mirror) {
    return this.http.request<Mirror>({
      method: 'POST',
      url: this.MIRRORS_URL,
      body: mirror,
    });
  }

  updateMirror(mirror: Mirror) {
    return this.http.request<Mirror>({
      method: 'PUT',
      url: this.MIRRORS_URL + mirror.name,
      body: mirror,
    });
  }

  deleteMirror(name: string) {
    return this.http.request<Mirror>({
      method: 'DELETE',
      url: this.MIRRORS_URL + name,
    });
  }
}
