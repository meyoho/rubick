import { NodeState, RegionStatus } from 'app/services/api/region.service';

export * from './status';
export * from './unit';

export function parseRegionStateColor(state: RegionStatus): string {
  switch (state) {
    case RegionStatus.Running:
      return 'green';
    case RegionStatus.Warning:
      return 'yellow';
    case RegionStatus.Error:
    case RegionStatus.Critical:
      return 'red';
    case RegionStatus.Deploying:
    case RegionStatus.Preparing:
      return 'blue';
    case RegionStatus.Stopped:
    default:
      return 'gray';
  }
}

export function parseNodeStateColor(state: NodeState) {
  switch (state) {
    case NodeState.Deploying:
    case NodeState.ShuttingDown:
    case NodeState.Stopping:
    case NodeState.Preparing:
    case NodeState.Draining:
      return 'blue';
    case NodeState.Running:
      return 'green';
    case NodeState.Warning:
      return 'yellow';
    case NodeState.Error:
    case NodeState.Critical:
      return 'red';
    default:
      return 'gray';
  }
}

export const CLUSTER_DEPLOY_TEMPLATES: {
  [key: string]: {
    features: string[];
    optionalFeatures: string[];
  };
} = {
  poc: {
    features: [
      'load_balancer', // Will be mapped to haproxy or nginx when submitting
      'exec',
      'volume',
      'chronos',
      'pipeline',
      'registry',
    ],
    optionalFeatures: ['tunnel'],
  },
  prod: {
    features: ['chronos', 'pipeline', 'registry'],
    optionalFeatures: [
      'load_balancer', // Will be mapped to haproxy or nginx when submitting
      'exec',
      'volume',
      'tunnel',
    ],
  },
};
