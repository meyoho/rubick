import { RegionFeature } from 'app/services/api/region.service';

export type RegionFeatureStatus =
  | 'not_added'
  | 'running'
  | 'error'
  | 'deploying'
  | 'pending'
  | 'docked'
  | 'unknown';

export const getRegionFeatureStatus = (
  regionFeature: RegionFeature,
  customizedMetric = false,
): RegionFeatureStatus => {
  if (customizedMetric) {
    return 'docked';
  }

  if (!regionFeature || !regionFeature.config || !regionFeature.config.type) {
    return 'not_added';
  }

  if (regionFeature.config.type !== 'official') {
    return 'docked';
  }

  if (
    !regionFeature.application_info ||
    !regionFeature.application_info.status
  ) {
    return 'unknown';
  }

  if (
    ['Running', 'Deploying', 'Pending'].includes(
      regionFeature.application_info.status,
    )
  ) {
    return regionFeature.application_info.status.toLowerCase() as RegionFeatureStatus;
  }

  return 'error';
};
