export const CORE_UNITS = ['m', '', 'k', 'M', 'G', 'T', 'P', 'E'];

export const formatNumUnit = (coreStr: string) => {
  if (coreStr == null) {
    return null;
  }
  coreStr = coreStr.trim();
  const num = +coreStr.replace(/[mkMGTPE]$/, '');
  if (num === 0) {
    return 0;
  }
  if (Number.isNaN(num)) {
    return null;
  }
  const lastChar = coreStr[coreStr.length - 1];
  let unitIndex = CORE_UNITS.indexOf(lastChar);
  unitIndex = unitIndex === -1 ? 0 : unitIndex - 1;
  return num * Math.pow(1000, unitIndex);
};

function formatCommonUnit(value: string): string;
function formatCommonUnit(
  value: string,
  fixed?: boolean,
  base?: number,
): string | number;
function formatCommonUnit(value: string = '', fixed?: boolean, base?: number) {
  value = value.replace(/i$/, '');

  let number = parseFloat(value);
  if (!base) {
    base = 1024;
  }

  if (!number) {
    return 0;
  }

  switch (value[value.length - 1]) {
    case 'M':
      number = number / base;
      break;
    case 'Mi':
      number = number / 1024;
      break;
    case 'm':
      number = number / 1000 / 1024 / 1024 / 1024;
      break;
    case 'G':
      break;
    case 'Gi':
      number = (number / 1000) * 1024;
      break;
    case 'K':
      number = number / base / base;
      break;
    case 'Ki':
    case 'k':
      number = number / 1024 / 1024;
      break;
    case 'T':
      number = number * base;
      break;
    case 'Ti':
      number = number * 1024;
      break;
    case 'P':
      number = number * base * base;
      break;
    case 'Pi':
      number = number * 1024 * 1024;
      break;
    case 'E':
      number = number * base * base * base;
      break;
    case 'Ei':
      number = number * 1024 * 1024 * 1024;
      break;
    default:
      number = number / 1024 / 1024 / 1024;
      break;
  }

  if (!fixed) {
    return number;
  }

  return number
    .toFixed(2)
    .replace(/0+$/, '')
    .replace(/\.$/, '');
}

function formatK8sCommonUnit(value: string): string;
function formatK8sCommonUnit(value: string, fixed: boolean): string | number;
function formatK8sCommonUnit(value: string, fixed?: boolean) {
  return formatCommonUnit(value, fixed, 1000);
}

export { formatCommonUnit, formatK8sCommonUnit };
