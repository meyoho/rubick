import { NgModule } from '@angular/core';
import { CredentialSharedModule } from 'app/features-shared/credential';
import { SharedModule } from 'app/shared/shared.module';

import { CredentialRoutingModule } from './credential.routing.module';

@NgModule({
  imports: [SharedModule, CredentialRoutingModule, CredentialSharedModule],
  providers: [],
})
export class CredentialModule {}
