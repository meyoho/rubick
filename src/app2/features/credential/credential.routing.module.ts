import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CredentialDetailPageComponent } from 'app/features-shared/credential/components/detail-page/detail-page.component';
import { CredentialListPageComponent } from 'app/features-shared/credential/components/list-page/list-page.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: CredentialListPageComponent,
  },
  {
    path: 'detail/:name',
    component: CredentialDetailPageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class CredentialRoutingModule {}
