import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';

import { get } from 'lodash-es';
import { Subscription } from 'rxjs';

import { AccountService } from 'app/services/api/account.service';
import { RegionService } from 'app/services/api/region.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Application } from 'app/typings';
import { Environments } from 'app/typings';

@Component({
  selector: 'rc-event-dashboard',
  templateUrl: './event.component.html',
  styleUrls: ['./event.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class EventComponent implements OnInit, OnDestroy {
  @Input()
  appData: Application;
  /*
   * app core
   * clusterName和namespaceName直接传入
   * app core的uid和name从appData中传入
   */
  @Input()
  cluster: string;

  @Input()
  namespace: string;
  /*
   * 用于rc-event-list
   * 如果uuid有值，查询这个应用/组件的平台事件
   */
  uuid: string;

  /*
   * 用于rc-k8s-event-list
   * 查询k8s事件
   */
  @Input()
  appFilters: string;
  @Input()
  kind: string;
  @Input()
  embedded = false;
  @Input()
  names: string;

  type: string;

  currentEventType = 'platformEvents';

  private regionSub = Subscription.EMPTY;

  ngOnInit() {
    /*
     * app core
     */
    if (this.isUserView || get(this.appData, 'kubernetes.metadata.uid')) {
      this.uuid = get(this.appData, 'kubernetes.metadata.uid');
      this.type = get(this.appData, 'kubernetes.kind');
      // 非app core创建的应用的uuid从labels里获取
      if (!this.uuid) {
        const labels = get(this.appData, 'kubernetes.metadata.labels');
        if (labels) {
          this.uuid = labels[`app.${this.env.label_base_domain}/uuid`] || '';
        }
      }
    } else {
      this.uuid = get(this.appData, 'resource.uuid');
      this.regionSub = this.regionService.region$.subscribe(region => {
        if (!region) {
          return;
        }
        // FIXME:
        // 重新渲染事件列表，使得集群切换可以生效：
        this.cluster = null;
        this.cdr.markForCheck();
        setTimeout(() => {
          this.cluster = region.name;
          this.cdr.markForCheck();
        });
      });
    }
  }

  ngOnDestroy(): void {
    this.regionSub.unsubscribe();
  }

  typeSelect(eventType: string) {
    this.currentEventType = eventType;
  }

  get isUserView() {
    return this.account.isUserView();
  }

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    private regionService: RegionService,
    private cdr: ChangeDetectorRef,
    private account: AccountService,
  ) {}
}
