import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { EventComponent } from 'app2/features/event/dashboard/event.component';

const eventRoutes: Routes = [
  {
    path: '',
    component: EventComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(eventRoutes)],
  exports: [RouterModule],
})
export class EventRoutingModule {}
