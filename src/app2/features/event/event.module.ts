import { NgModule } from '@angular/core';

import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { EventRoutingModule } from 'app2/features/event/event-routing.module';

@NgModule({
  imports: [SharedModule, EventSharedModule, EventRoutingModule],
})
export class EventModule {}
