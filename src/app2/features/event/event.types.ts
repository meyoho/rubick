export interface Event {
  detail: any;
  /**
   * event type:
   *
   * 0: success
   * 1: warning
   * 2: error
   */
  log_level: number;
  namespace: string;
  resource_id: string;
  resource_name: string;
  resource_type: string;
  template_id: string;
  time: number;
  /**
   * Combined message of other event properties
   */
  message?: string;
  /**
   * Stringified event detail
   */
  jsonDetail?: string;
  /**
   * Show jsonDetail or not, default false
   */
  expanded?: boolean;
  /**
   * Time string with format 'YYYY-MM-DD HH:mm:ss', generated from time property
   */
  formattedTime?: string;
}

/**
 * Events query
 */
export interface EventQuery {
  start_time: number;
  end_time: number;
  pageno: number;
  size: number;
  event_types?: string;
  event_type?: string;
  event_pk?: string;
  query_string?: string;
}

/**
 * Event result
 */
export interface EventsResult {
  total_items: number;
  total_pages: number;
  results: [Event];
}

/**
 * K8s event
 *
 * @export
 * @interface K8sEvent
 */
export interface K8sEvent {
  count: number;
  firstTimestamp: string;
  lastTimestamp: string;
  source: {
    component: string;
  };
  reason: string;
  cluster_name: string;
  cluster_uuid: string;
  type: string;
  message: string;
  involvedObject: {
    kind: string;
    name: string;
    namespace?: string;
    apiVersion: string;
    resourceVersion: string;
    uid: string;
  };
  metadata: {
    uid: string;
    name: string;
    namespace?: string;
    resourceVersion: string;
    creationTimestamp: string;
    selfLink: string;
  };
}

/**
 * K8s event query
 *
 * @export
 * @interface K8sEventQuery
 */
export interface K8sEventQuery {
  start_time: number;
  end_time: number;
  cluster: string;
  kind?: string;
  name?: string;
  namespace?: string;
  page?: number;
  page_size?: number;
  filters?: string;
}

export interface K8sEventsResult {
  count: number;
  num_pages: number;
  page_size: number;
  next: string;
  previous: string;
  total_items: number;
  results: [K8sEvent];
}
