import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  TemplateRef,
} from '@angular/core';

import { find, map, mergeWith, reduce } from 'lodash-es';
import moment from 'moment';
import { Subject } from 'rxjs';

import { AccountService } from 'app/services/api/account.service';
import { EventService } from 'app/services/api/event.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app/services/api/namespace.service';
import { RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';
import {
  K8sEvent,
  K8sEventQuery,
  K8sEventsResult,
} from 'app2/features/event/event.types';
import { RESOURCE_TYPES, TIMESTAMP_OPTIONS } from './k8s-event.constant';

interface TimeRangeOption {
  name: string;
  type: string;
  offset: number;
}
@Component({
  selector: 'rc-k8s-event-list',
  templateUrl: './k8s-event-list.component.html',
  styleUrls: ['./k8s-event-list.component.scss'],
})
export class K8sEventListComponent implements OnInit, OnDestroy {
  @Input()
  kind: string;
  @Input()
  names: string;
  @Input()
  namespace: string;
  @Input()
  cluster: string;
  @Input()
  appFilters: string;
  @Input()
  embedded = false;
  private onDestroy$ = new Subject<void>();
  loading = false;
  loadingSuggestions = false;
  initialized = false;
  eventQuery: K8sEventQuery;
  region: any;
  timeRangeOptions: TimeRangeOption[] = TIMESTAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: this.translate.get(option.type),
    };
  });
  timeRangeOption = 'last_30_minites';
  timeRangeOutOfDate = false;
  currentTime = moment();
  startTime = this.currentTime
    .clone()
    .startOf('minutes')
    .subtract(30, 'minutes');
  endTime = this.currentTime.clone();
  queryDatesShown = {
    startTime: this.dateFormat(this.startTime.valueOf()),
    endTime: this.dateFormat(this.endTime.valueOf()),
  };
  calendarOptions = {
    maxDate: this.currentTime
      .clone()
      .endOf('day')
      .valueOf(),
    minDate: this.currentTime
      .clone()
      .startOf('day')
      .subtract(6, 'days')
      .valueOf(),
    enableTime: true,
    enableSeconds: true,
  };
  events: K8sEvent[] = [];
  columns: string[] = [
    'namespace',
    'type',
    'resource_name',
    'time_range',
    'count',
    'reason',
    'message',
    'action',
  ];
  pagination = {
    count: 0,
    size: 20,
    current: 1,
    pages: 0,
  };
  clusterNamspaces: string[] = [];
  resourceTypes: string[] = [];
  suggestions: string[] = [];
  conditions: string[] = [];
  queryCondition = {};
  queryDate: {
    start_time: number;
    end_time: number;
  };
  event_detail = {};
  private intervalTimer: number;
  // @ViewChild('conditionsInput') conditionsInput: TagsInputComponent;
  constructor(
    private eventService: EventService,
    private errorToast: ErrorsToastService,
    private regionService: RegionService,
    private translate: TranslateService,
    private namespaceService: NamespaceService,
    private dialogService: DialogService,
    private auiNotificationService: NotificationService,
    private accountService: AccountService,
    private cdr: ChangeDetectorRef,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}

  async ngOnInit() {
    this.initTimer();
    if (this.isUserView) {
      this.columns.splice(0, 1);
    }

    if (this.cluster) {
      this.region = {
        name: this.cluster,
      };
    } else {
      this.region = await this.regionService.getCurrentRegion();
    }
    this.initQueryTimeRange();
    this.initSuggestions();
    this.fetchEvents();
    this.initialized = true;
  }

  get isUserView() {
    return this.accountService.isUserView();
  }

  private initQueryTimeRange() {
    if (this.env.event_ttl) {
      this.calendarOptions.minDate = this.currentTime
        .clone()
        .startOf('day')
        .subtract(parseInt(this.env.event_ttl, 10) - 1, 'days')
        .valueOf();
    }
  }

  async initSuggestions() {
    this.resourceTypes = RESOURCE_TYPES.map((type: string) => {
      return `kind: ${type}`;
    });
    try {
      this.loadingSuggestions = true;
      const namespaces = await this.namespaceService.getNamespaceOptions(
        this.region.name,
      );
      this.clusterNamspaces = namespaces.map((namespace: NamespaceOption) => {
        return `namespace: ${namespace.name}`;
      });
    } catch (error) {
      this.clusterNamspaces = [];
    } finally {
      this.suggestions = [...this.clusterNamspaces, ...this.resourceTypes];
      this.loadingSuggestions = false;
      this.cdr.markForCheck();
    }
  }

  async fetchEvents() {
    this.queryDate = {
      start_time: this.startTime.valueOf() / 1000,
      end_time: this.endTime.valueOf() / 1000,
    };

    if (!this.checkQueryDates()) {
      return;
    }
    const conditions = this.generateCondition();
    if (this.kind) {
      conditions.kind = conditions.kind
        ? `${conditions.kind},${this.kind}`
        : this.kind;
    }
    if (this.names) {
      conditions.name = conditions.name
        ? `${conditions.name},${this.names}`
        : this.names;
    }
    if (this.namespace) {
      conditions.namespace = conditions.namespace
        ? `${conditions.namespace},${this.namespace}`
        : this.namespace;
    }
    this.eventQuery = {
      cluster: this.region.name,
      ...this.queryDate,
      ...conditions,
      page: this.pagination.current,
      page_size: this.pagination.size,
      filters: this.appFilters,
    } as K8sEventQuery;
    this.loading = true;
    try {
      const eventsResult: K8sEventsResult = await this.eventService.getK8sEvents(
        this.eventQuery,
      );
      this.events = eventsResult.results;
      this.pagination.count =
        eventsResult.total_items > 10000 ? 10000 : eventsResult.total_items;
      this.pagination.pages = eventsResult.num_pages;
    } catch (error) {
      this.errorToast.error(error);
    } finally {
      this.loading = false;
      this.cdr.markForCheck();
    }
  }

  trackByFn(_index: number, item: K8sEvent) {
    return item.metadata.uid;
  }

  onTimeRangeChanged(value: string) {
    this.clearTimeout();
    const option: TimeRangeOption = find(this.timeRangeOptions, {
      type: value,
    });
    if (option.type !== 'custom_time_range') {
      this.resetTimeRange();
      this.pagination.current = 1;
      this.fetchEvents();
    }
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.clearTimeout();
  }

  initTimer() {
    this.intervalTimer = window.setInterval(() => {
      if (this.conditions.length) {
        this.clearTimeout();
        return;
      }
      if (this.timeRangeOption !== 'custom_time_range') {
        const timeRange = TIMESTAMP_OPTIONS.find(
          option => option.type === this.timeRangeOption,
        );
        this.endTime = moment();
        this.startTime = moment()
          .clone()
          .subtract(timeRange.offset);
        this.fillCalendar(this.startTime.valueOf(), this.endTime.valueOf());
        this.fetchEvents();
      }
    }, 60000);
  }

  clearTimeout() {
    window.clearInterval(this.intervalTimer);
  }

  private resetTimeRange() {
    const timeRange = TIMESTAMP_OPTIONS.find(
      option => option.type === this.timeRangeOption,
    );
    this.endTime = moment();
    this.startTime = this.endTime
      .clone()
      .startOf('seconds')
      .subtract(timeRange.offset);
    this.fillCalendar(this.startTime.valueOf(), this.endTime.valueOf());
  }

  private fillCalendar(start_time: number, end_time: number) {
    this.queryDatesShown.startTime = this.dateFormat(start_time);
    this.queryDatesShown.endTime = this.dateFormat(end_time);
  }

  onStartTimeSelect(time: string) {
    this.startTime = moment(time, 'YYYY-MM-DD HH:mm:ss');
    this.pagination.current = 1;
    this.fetchEvents();
  }

  onEndTimeSelect(time: string) {
    this.endTime = moment(time, 'YYYY-MM-DD HH:mm:ss');
    this.pagination.current = 1;
    this.fetchEvents();
  }

  setConditions(_conditions: string[]) {
    this.clearTimeout();
    this.pagination.current = 1;
    this.fetchEvents();
  }

  private checkQueryDates() {
    if (!this.queryDate['start_time'] || !this.queryDate['end_time']) {
      this.auiNotificationService.warning(
        this.translate.get('log_query_timerange_required'),
      );
      return false;
    }
    if (
      this.timeRangeOption === 'custom_time_range' &&
      this.queryDate['start_time'] >= this.queryDate['end_time']
    ) {
      this.auiNotificationService.warning(
        this.translate.get('log_query_timerange_warning'),
      );
      return false;
    }
    return true;
  }

  private generateCondition(): {
    kind?: string;
    namespace?: string;
    name?: string;
  } {
    const conditionMap = map(this.conditions, (condition: string) => {
      const param = {};
      const match = condition.match(/^(.+):\s(.+)$/);
      if (match && match[0]) {
        const value = (match[2] || '').trim();
        param[match[1]] = value;
      } else {
        param['name'] = condition;
      }
      return param;
    });
    const ret = reduce(conditionMap, (result, param) =>
      mergeWith(result, param, (pre, cur) => {
        if (pre) {
          return [pre, cur].join(',');
        } else {
          return cur;
        }
      }),
    );
    return ret ? ret : {};
  }

  currentPageChange(_currentPage: number) {
    this.clearTimeout();
    this.fetchEvents();
  }

  pageSizeChange(size: number) {
    this.clearTimeout();
    if (Math.ceil(this.pagination.count / size) < this.pagination.pages) {
      this.pagination.current = 1;
    }
    this.fetchEvents();
  }

  showDetailTemplate(item: K8sEvent, template: TemplateRef<any>) {
    this.event_detail = item;
    this.dialogService.open(template, {
      size: DialogSize.Big,
    });
  }

  private dateFormat(stamp: number) {
    return moment(stamp).format('YYYY-MM-DD HH:mm:ss');
  }
}
