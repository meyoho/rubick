import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';

import moment from 'moment';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AccountService } from 'app/services/api/account.service';
import { EventService } from 'app/services/api/event.service';
import { LoggerUtilitiesService } from 'app/services/logger.service';
import { RouterUtilService } from 'app/services/router-util.service';
import { TranslateService } from 'app/translate/translate.service';
import { Event } from 'app2/features/event/event.types';

interface Params {
  [name: string]: any;
}

@Component({
  selector: 'rc-event-list',
  templateUrl: './event-list.component.html',
  styleUrls: ['./event-list.component.scss'],
})
export class EventListComponent implements OnInit, OnDestroy {
  @Input()
  type: string; // 1. resource type: 'service'  2. multiple resource types: 'service,region,node'
  @Input()
  primaryKey: string; // resource uuid
  @Input()
  polling = true;
  @Input()
  pollingInterval: number = 30 * 1000;
  loading = false;
  pagination = {
    count: 0,
    size: 20,
    current: 1,
    pages: 0,
  };
  keyword = '';
  events: Array<any> = [];
  expandedEvents: Array<any> = [];
  eventStateMap: any = {
    '0': 'success',
    '1': 'warning',
    '2': 'error',
  };

  notFoundTips = '';

  destroyed = false;
  onDestroy$ = new Subject<void>();

  pollingTimer: any;

  constructor(
    private accountService: AccountService,
    private eventService: EventService,
    private routerUtil: RouterUtilService,
    private logger: LoggerUtilitiesService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
  ) {
    this.notFoundTips = this.translate.get('event_not_found_tips');
  }

  ngOnInit() {
    this.loadEvents();
    // refetch on language change DEV-15671
    this.translate.currentLang$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(() => this.loadEvents());
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.onDestroy$.next();
    this.destroyed = true;
  }

  private resetPollingTimer() {
    clearTimeout(this.pollingTimer);
    if (this.keyword) {
      return;
    }
    this.pollingTimer = setTimeout(() => {
      if (this.pagination.current === 1) {
        this.loadEvents();
      }
    }, this.pollingInterval);
  }

  /**
   * Search handler
   *
   * @memberof EventListComponent
   */
  search(keyword: string): void {
    clearTimeout(this.pollingTimer);
    this.keyword = keyword;
    // reset current page
    this.pagination.current = 1;
    this.loadEvents();
  }

  currentPageChange(_currentPage: number) {
    clearTimeout(this.pollingTimer);
    this.loadEvents();
  }

  pageSizeChange(size: number) {
    clearTimeout(this.pollingTimer);
    if (Math.ceil(this.pagination.count / size) < this.pagination.pages) {
      this.pagination.current = 1;
    }
    this.loadEvents();
  }

  /**
   * Load events
   */
  async loadEvents() {
    // init query
    if (this.loading) {
      return;
    }
    this.loading = true;
    const now = moment();
    const start = now
      .clone()
      .startOf('day')
      .subtract(29, 'days');
    const query = {
      start_time: start.valueOf() / 1000,
      end_time: now.valueOf() / 1000,
      pageno: this.pagination.current,
      size: this.pagination.size,
    };
    if (this.primaryKey) {
      query['event_pk'] = this.primaryKey;
    }
    if (this.type) {
      const types = this.type.split(',');
      const typeParamName = types.length > 1 ? 'event_types' : 'event_type';
      query[typeParamName] = this.type;
    }
    if (this.keyword) {
      query['query_string'] = this.keyword;
    }
    // cache current events state
    this.expandedEvents = this.events.length
      ? this.events.filter(event => event.expanded)
      : [];

    // start query events
    try {
      if (this.polling) {
        this.resetPollingTimer();
      }
      const data = await this.eventService.getEvents(query);
      this.pagination.count =
        data.total_items > 10000 ? 10000 : data.total_items;
      this.pagination.pages = Math.ceil(
        this.pagination.count / this.pagination.size,
      );
      this.events = this.handleEvents(data.results || []);
    } catch (err) {
      this.logger.error(err);
    } finally {
      this.loading = false;
      if (!this.destroyed) {
        this.cdr.detectChanges();
      }
    }
  }

  /**
   * Handle loaded events
   */
  handleEvents(events: Array<any>) {
    return events.map((event: Event) => {
      // detail text
      event.jsonDetail = JSON.stringify(event, null, 4);
      // set expand state
      event.expanded = this.expandedEvents.find(expandedEvent => {
        return (
          expandedEvent.resource_id === event.resource_id &&
          expandedEvent.time === event.time
        );
      });
      // event time
      event.formattedTime = moment(event.time * 1000).format(
        'YYYY-MM-DD HH:mm:ss',
      );
      // event message
      this.generateMessage(event);
      return event;
    });
  }

  href(stateName: string, params?: Params) {
    return this.routerUtil.getLinkUrl(stateName.split('.').join('/'), params);
  }

  /**
   * Generate message
   * @param event
   */
  generateMessage(event: Event) {
    let message = '';
    let resource = '';
    let parentResource = '';
    const detail = event.detail;
    const stateItem = this.getStateItem(event);
    const href = stateItem.routerLink
      ? this.routerUtil.getLinkUrl(stateItem.routerLink, stateItem.params)
      : this.href(stateItem.name);
    let link = `<a href=${href}>${event.resource_name}</a>`;
    if (this.accountService.isUserView() || href === window.location.pathname) {
      link = `<span>${event.resource_name}</span>`;
    }
    if (event.resource_type === 'rule') {
      event.resource_type = `k8s_event_access_${event.resource_type}`;
    }
    /*
     * @TODO
     * For now, only four types of template_id exists on staging:
     * generic, sub_resource,  service_status and generic-svoa
     * Are the other types deprecated?
     */
    switch (event.template_id) {
      case 'generic':
        if (
          [
            'alert_template',
            'certificate',
            'configmap',
            'customresourcedefinition',
            'deployment',
            'domain',
            'endpoints',
            'frontend',
            'jenkins',
            'jenkins_pipeline',
            'k8s_event_access_rule',
            'ldap_config',
            'ldap_sync',
            'pod',
            'priv_regis_proj',
            'project_template',
            'resource_ratio',
            'subnet',
            'svc_binding',
            'toolbindingreplica',
            'service',
          ].includes(event.resource_type)
        ) {
          link = `<span>${event.resource_name}</span>`;
        }
        message += this.generateSubMessage(
          this.mixTerm(event.detail.operator),
          'event_' + event.detail.operation,
          event.resource_type,
          link,
        );
        break;
      case 'sub_resource':
        if (event.detail.parent) {
          resource = this.getResourceLink(event);
          parentResource = this.getResourceLink(event.detail.parent, true);
          message += this.generateSubMessage(
            this.mixTerm(event.detail.operator),
            `event_${event.detail.operation}`,
            event.detail.parent.resource_type,
            parentResource,
            this.translate.get('event_of'),
            event.resource_type,
            resource,
          );
        }
        break;
      case 'repo_tag':
        message += this.generateSubMessage(
          this.mixTerm(event.detail.operator),
          'event_' + event.detail.operation,
          event.resource_type,
          link + this.translate.get('event_of'),
          event.detail.sub_resource_type,
          `<span>${event.detail.sub_resource_name}</span>`,
        );
        break;
      case 'node':
        message += this.generateSubMessage(
          this.mixTerm(event.detail.operator),
          'event_' + event.detail.operation,
          event.resource_type,
          event.namespace +
            '/' +
            event.detail.region_display_name +
            this.translate.get('event_of'),
          link,
        );
        break;
      case 'result':
        message += this.generateSubMessage(
          event.resource_type,
          link,
          event.detail.operation,
          event.detail.status,
        );
        break;
      case 'service_status':
      case 'region_status':
        if (event.resource_type === 'ldap_sync') {
          link = `<span>${event.resource_name}</span>`;
        }
        message += this.generateSubMessage(
          event.resource_type,
          link,
          'entered',
          `status_${event.detail.status}`,
          'status',
        );
        break;
      case 'app_task_result':
        const links: string[] = [];
        let linkStr = '';
        event.detail[`sub_resource_id`]
          .split(',')
          .forEach((e: string, i: number) => {
            // if there's not any service
            if (
              event.detail.operation === 'create' &&
              event.detail.status === 'failure'
            ) {
              links.push(e);
            } else {
              const href = this.href(
                'console.app_service.service.service_detail',
                {
                  service_name: e,
                },
              );
              links.push(`
              <a href="${href}">${
                event.detail.sub_resource_name.split(',')[i]
              }</a>
            `);
            }
          });
        linkStr = links.join(',&nbsp;');
        message += this.generateSubMessage(
          `event_${event.resource_type}`,
          link,
          'event_of',
          event.detail.sub_resource_type,
          linkStr,
          `event_result_${event.detail.operation}`,
          `status_${event.detail.status}`,
        );
        break;
      case 'permission-change':
        message += this.generateSubMessage(
          this.mixTerm(detail.operator),
          'event_' + detail.operation,
          detail.object_type,
          detail['object'],
          'event_for',
          event.resource_type,
          link + this.translate.get('event_of'),
          detail.sub_attr_type,
          detail.sub_attr_name,
        );
        break;
      case 'team-update':
        message += this.generateSubMessage(
          this.mixTerm(detail.operator),
          'event_' + detail.operation,
          event.resource_type,
          link + this.translate.get('event_of'),
          detail.sub_attr_type,
          'event_to',
          detail.sub_attr_value,
        );
        break;
      case 'generic-svoa':
        if (event.resource_type === 'role') {
          link = event.resource_name;
        } else {
        }
        message += this.generateSubMessage(
          this.mixTerm(detail.operator),
          'event_' + detail.operation,
          event.resource_type,
          link + this.translate.get('event_of'),
          detail.attribute_type,
          detail.attribute,
        );
        break;
      case 'registry-gc':
        message += this.translate.get('event_registry_gc', {
          href,
          resource_name: event.resource_name,
        });
        break;
      case 'registry-gc-completed':
        message += this.translate.get('event_registry_gc_completed', {
          href,
          resource_name: event.resource_name,
        });
        break;
      default:
        break;
    }
    event.message = message;
  }

  generateSubMessage(...terms: Array<any>) {
    const str = terms
      .map(term => {
        if (term === 'service') {
          term = 'k8s_service';
        }
        return /<a/.test(term) || !term
          ? term
          : this.translate.get(term.toLowerCase());
      })
      .join(' ');
    // remove space between two double-byte characters.
    return str.replace(/([^\x00-\xff]) ([^\x00-\xff])/g, '$1$2');
  }

  /**
   * Prevent term from being translated in generateSubMessage
   *
   * @param string
   * @returns {string}
   * @memberof EventListComponent
   */
  mixTerm(term: string) {
    return `<span>${term}</span>`;
  }

  getResourceLink(event: Event, isParent = false): string {
    if (!event) {
      return;
    }
    switch (event.resource_type) {
      case 'sync_regis_conf':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/console/admin/image/sync-center/detail/' + event.resource_id,
        )}">${event.resource_name}</a>`;
      case 'sync_regis_history':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/console/admin/image/sync-history/detail/' + event.resource_id,
        )}">${event.resource_id}</a>`;
      case 'permission':
        return '';
      case 'role':
        return `<a href="${this.routerUtil.getLinkUrl(
          '/console/admin/rbac/roles/detail',
          {
            role_name: event.resource_id,
          },
        )}">${event.resource_name}</a>`;
      default:
        return isParent ? event.resource_name : event.resource_id;
    }
    return '';
  }

  /**
   * Get state info: state name and state params
   *
   * @param {*} event
   * @returns
   * @memberof EventListComponent
   */
  getStateItem(event: Event) {
    const name = 'console.admin.home';
    let routerLink;
    let params = null;
    switch (event.resource_type.toLowerCase()) {
      case 'application':
        routerLink = [
          '/console',
          'user',
          'workspace',
          {
            project: event.detail.project_name,
            cluster: event.detail.region_name,
            namespace: event.detail.k8s_namespace,
          },
          'app',
          'detail',
          event.resource_name,
        ];
        break;
      case 'alb2':
        routerLink = [
          '/console',
          'admin',
          {
            cluster: event.detail.region_name,
          },
          'load_balancer',
          'detail',
          event.detail.k8s_namespace,
          event.resource_name,
        ];
        break;
      case 'alarm':
        routerLink = [
          '/console',
          'admin',
          {
            cluster: event.detail.cluster_name,
          },
          'alarm',
          'detail',
          event.detail.cluster_name,
          event.detail.namespace,
          event.detail.resource_name,
          event.detail.group_name,
          event.resource_name,
        ];
        break;
      case 'mirror':
        routerLink = `/console/admin/cluster/mirror/detail/${event.resource_name}`;
        break;
      case 'namespace':
        routerLink = [
          '/console',
          'user',
          'project',
          {
            project: event.detail.project_name,
          },
          'namespace',
          'detail',
          event.detail.region_name,
          event.resource_name,
        ];
        break;
      case 'resourcequota':
        routerLink = [
          '/console',
          'user',
          'project',
          {
            project: event.detail.project_name,
          },
          'namespace',
          'detail',
          event.detail.region_name,
          event.detail.k8s_namespace,
        ];
        break;
      case 'node':
        routerLink = `/console/admin/cluster/node_detail/${event.detail.region_name}/${event.resource_name}`;
        break;
      case 'notification':
        routerLink = `/console/admin/notification/detail/${event.resource_id}`;
        break;
      case 'repository':
        routerLink = '/console/admin/image/repository/detail';
        params = {
          repositoryName: event.resource_name,
          registryName: event.detail.registry_name,
        };
        break;
      case 'repo':
        routerLink = '/console/admin/image/repository/detail';
        params = {
          repo_ns: event.namespace,
          repositoryName: event.resource_name,
        };
        break;
      case 'priv_registry':
        routerLink = '/console/admin/image/repository';
        params = {
          registryName: event.resource_name,
        };
        break;
      case 'priv_regis_repo':
        routerLink = '/console/admin/image/repository/detail';
        params = {
          repositoryName: event.resource_name,
          registryName: event.detail.registry_name,
          projectName: event.detail.project,
        };
        break;
      case 'sync_regis_conf':
        routerLink =
          '/console/admin/image/sync-center/detail/' + event.resource_id;
        break;
      case 'sync_regis_history':
        routerLink =
          '/console/admin/image/sync-history/detail/' + event.resource_id;
        break;
      case 'build':
        routerLink = '/console/admin/image/build/detail/' + event.resource_id;
        break;
      case 'team':
        routerLink = '/console/admin/org/team';
        params = {
          orgName: event.namespace,
          teamName: event.resource_name,
        };
        break;
      case 'organization':
        routerLink = '/console/admin/user/detail';
        break;
      case 'region':
        routerLink = `/console/admin/cluster/detail/${event.resource_name}`;
        break;
      case 'feature':
        routerLink = '/console/admin/cluster/detail';
        params = {
          name: event.detail.region_name,
        };
        break;
      case 'role':
        routerLink = '/console/admin/rbac/roles/detail';
        params = {
          role_name: event.resource_name,
        };
        break;
      case 'project':
        routerLink = `/console/admin/projects/detail/${event.resource_name}`;
        params = {
          uuid: event.resource_id,
        };
        break;
      case 'lb':
      case 'load_balancer':
        routerLink = `/console/admin/load_balancer/detail/${event.resource_name}`;
        break;
      case 'integration':
        routerLink = `/console/admin/integration_center/detail/${event.resource_id}`;
        break;
      case 'networkpolicy':
        routerLink = [
          '/console',
          'user',
          'workspace',
          {
            project: event.detail.project_name,
            cluster: event.detail.region_name,
            namespace: event.detail.k8s_namespace,
          },
          'network_policy',
          'detail',
          event.resource_name,
        ];
        break;
      case 'pv':
      case 'persistentvolume':
        routerLink = [
          '/console',
          'admin',
          {
            cluster: event.detail.region_name,
          },
          'storage',
          'pv',
          'detail',
          event.resource_name,
        ];
        break;
      case 'pvc':
      case 'persistentvolumeclaim':
        routerLink = [
          '/console',
          'user',
          'workspace',
          {
            project: event.detail.project_name,
            cluster: event.detail.region_name,
            namespace: event.detail.k8s_namespace,
          },
          'pvc',
          'detail',
          event.resource_name,
        ];
        break;
      case 'helm_template_repo':
        routerLink = `/console/admin/app-catalog/repository/repo_detail/${event.resource_id}`;
        break;
      case 'jenkins_pipeline_template_source':
        routerLink = `/console/admin/jenkins/templates`;
        break;
      case 'storageclass':
        routerLink = [
          '/console',
          'admin',
          {
            cluster: event.detail.region_name,
          },
          'storage',
          'storageclass',
          'detail',
          event.resource_name,
        ];
        break;
      case 'secret':
        routerLink = `/console/admin/credential/detail/${event.resource_name}`;
        break;
      case 'route':
      case 'ingress':
        routerLink = [
          '/console',
          'user',
          'workspace',
          {
            project: event.detail.project_name,
            cluster: event.detail.region_name,
            namespace: event.detail.k8s_namespace,
          },
          event.resource_type,
          'detail',
          event.resource_name,
        ];
        break;
      case 'cronjob':
        routerLink = [
          '/console',
          'user',
          'workspace',
          {
            project: event.detail.project_name,
            cluster: event.detail.region_name,
            namespace: event.detail.k8s_namespace,
          },
          'cron_job',
          'detail',
          event.resource_name,
        ];
        break;
    }
    return {
      name,
      routerLink,
      params,
    };
  }

  /**
   * Show or hide event's detail
   *
   * @param {*} event
   * @memberof EventListComponent
   */
  expandEvent(event: any, e: any): void {
    if (e.target.tagName === 'A') {
      return;
    }
    event.expanded = !event.expanded;
  }
}
