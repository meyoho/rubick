import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';

import { ImageRoutingModule } from './image.routing.module';
import { ImageRepositoryCardComponent } from './repository/card/image-repository-card.component';
import { ImageRepositoryTagLevelLabelComponent } from './repository/common/image-repository-tag-level-label.component';
import { ImageRepositoryCreateComponent } from './repository/create/image-repository-create.component';
import { ImageRepositoryDetailComponent } from './repository/detail/image-repository-detail.component';
import { ImageRepositoryHelpComponent } from './repository/help/image-repository-help.component';
import { ImageRepositoryListComponent } from './repository/list/image-repository-list.component';
import { ImageRepositoryAddProjectComponent } from './repository/projects/image-repository-add-project.component';
import { ImageRepositoryProjectsComponent } from './repository/projects/image-repository-projects.component';
import { ImageRepositoryTagsComponent } from './repository/tags/image-repository-tags.component';

@NgModule({
  imports: [SharedModule, ImageRoutingModule],
  declarations: [
    ImageRepositoryListComponent,
    ImageRepositoryProjectsComponent,
    ImageRepositoryAddProjectComponent,
    ImageRepositoryCardComponent,
    ImageRepositoryCreateComponent,
    ImageRepositoryDetailComponent,
    ImageRepositoryTagLevelLabelComponent,
    ImageRepositoryTagsComponent,
    ImageRepositoryHelpComponent,
  ],
  entryComponents: [ImageRepositoryAddProjectComponent],
})
export class ImageModule {}
