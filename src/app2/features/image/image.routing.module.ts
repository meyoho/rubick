import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ImageRepositoryCreateComponent } from './repository/create/image-repository-create.component';
import { ImageRepositoryDetailComponent } from './repository/detail/image-repository-detail.component';
import { ImageRepositoryHelpComponent } from './repository/help/image-repository-help.component';
import { ImageRepositoryListComponent } from './repository/list/image-repository-list.component';

const routes: Routes = [
  {
    path: 'repository',
    component: ImageRepositoryListComponent,
  },
  {
    path: 'repository/create',
    component: ImageRepositoryCreateComponent,
    pathMatch: 'full',
  },
  {
    path: 'repository/edit',
    component: ImageRepositoryCreateComponent,
    pathMatch: 'full',
  },
  {
    path: 'repository/detail',
    component: ImageRepositoryDetailComponent,
  },
  {
    path: 'repository/help',
    component: ImageRepositoryHelpComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ImageRoutingModule {}
