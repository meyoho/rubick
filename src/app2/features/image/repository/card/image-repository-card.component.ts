import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  forwardRef,
} from '@angular/core';

import { ImageRepository } from 'app/services/api/image-repository.service';
import { WEBLABS } from 'app/shared/tokens';
import { Weblabs } from 'app/typings';

import { ImageRepositoryProjectsComponent } from '../projects/image-repository-projects.component';

@Component({
  selector: 'rc-image-repository-card',
  templateUrl: './image-repository-card.component.html',
  styleUrls: ['./image-repository-card.component.scss'],
})
export class ImageRepositoryCardComponent implements OnInit {
  @Input()
  repository: ImageRepository;
  @Output()
  deleteRepository: EventEmitter<{
    before: () => void;
    after: () => void;
  }> = new EventEmitter();

  isThird: boolean;
  weblabs: Weblabs;
  $projects: ImageRepositoryProjectsComponent;

  deleting: boolean;

  constructor(
    @Inject(WEBLABS) weblabs: Weblabs,
    @Inject(forwardRef(() => ImageRepositoryProjectsComponent))
    $projects: ImageRepositoryProjectsComponent,
  ) {
    this.weblabs = weblabs;
    this.$projects = $projects;
  }

  ngOnInit() {
    this.isThird = this.repository.registry.is_third;
  }

  deleteRepo(e: Event) {
    e.stopPropagation();
    if (this.deleting) {
      return;
    }
    this.deleteRepository.emit({
      before: () => {
        this.deleting = true;
      },
      after: () => {
        this.deleting = false;
      },
    });
  }
}
