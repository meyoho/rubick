import { Component, Input } from '@angular/core';

@Component({
  selector: 'rc-image-repository-tag-level-label',
  templateUrl: './image-repository-tag-level-label.component.html',
  styleUrls: ['./image-repository-tag-level-label.component.scss'],
})
export class ImageRepositoryTagLevelLabelComponent {
  @Input()
  level: string;
  @Input()
  count: number;
  @Input()
  icon: boolean;
}
