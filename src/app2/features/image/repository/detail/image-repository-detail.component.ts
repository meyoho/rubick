import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { ConfirmType, DialogService, NotificationService } from '@alauda/ui';
import { ImageRegistryService } from 'app/services/api/image-registry.service';
import {
  ImageRepository,
  ImageRepositoryService,
} from 'app/services/api/image-repository.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';

@Component({
  templateUrl: './image-repository-detail.component.html',
  styleUrls: ['./image-repository-detail.component.scss'],
})
export class ImageRepositoryDetailComponent implements OnInit {
  registryName: string;
  repositoryName: string;
  projectName: string;
  isThird: boolean;

  initialized: boolean;
  repository: ImageRepository = {} as ImageRepository;
  imageAddress: string;
  deleting: boolean;

  tagsCount: number;
  get dockerPullTip() {
    return `${this.translate.get('tooltip_copy_click_to_copy')} "docker pull ${
      this.imageAddress
    }"`;
  }

  get imageAddressTip() {
    return (
      this.translate.get('tooltip_copy_click_to_copy') + ' ' + this.imageAddress
    );
  }

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: DialogService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private roleUtilities: RoleUtilitiesService,
    private imageRegistry: ImageRegistryService,
    private imageRepository: ImageRepositoryService,
  ) {}

  ngOnInit() {
    this.route.queryParamMap.subscribe(async params => {
      this.registryName = params.get('registryName');
      this.repositoryName = params.get('repositoryName');
      this.projectName = params.get('projectName');
      this.isThird = params.get('isThird') === 'true';

      if (this.repositoryName && this.repositoryName.includes('/')) {
        const registry = await this.imageRegistry.getRegistry(
          this.registryName,
        );
        if (!registry.is_public) {
          const [projectName, repositoryName] = this.repositoryName.split('/');
          return this.router.navigate(
            ['/console/admin/image/repository/detail'],
            {
              queryParams: {
                registryName: this.registryName,
                repositoryName,
                projectName,
                isThird: this.isThird,
              },
              replaceUrl: true,
            },
          );
        }
      }

      this.initialized = false;

      try {
        this.repository = await this.imageRepository.getRepository(
          this.registryName,
          this.repositoryName,
          this.projectName,
          this.isThird,
        );
      } catch (e) {
        if (!this.initialized && e.status !== 403) {
          this.auiNotificationService.warning({
            content: this.translate.get('image_repository_not_exist'),
          });
        }

        this.router.navigateByUrl('/console/admin/image/repository', {
          replaceUrl: true,
        });
      }

      this.imageAddress = this.getImageAddress(this.repository);

      this.initialized = true;
    });
  }

  buttonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.repository,
      'repository',
      action,
    );
  }

  async deleteRepository() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content:
          this.translate.get('delete_confirm') +
          this.translate.get('repository') +
          ' ' +
          this.repositoryName +
          ' ?',
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Danger,
      });
    } catch (e) {
      return;
    }

    this.deleting = true;

    try {
      await this.imageRepository.deleteRepository(
        this.registryName,
        this.repositoryName,
        this.projectName,
      );

      this.auiNotificationService.success({
        content: this.translate.get('repo_delete_success'),
      });

      this.router.navigateByUrl('/console/admin/image/repository', {
        replaceUrl: true,
      });
    } finally {
      this.deleting = false;
    }
  }

  private getImageAddress(
    repository: ImageRepository & { repo_path?: string },
  ) {
    if (repository.registry) {
      const registry = repository.registry;
      const project = repository.project;
      if (registry.is_public) {
        return `${repository.registry.endpoint}/${repository.namespace}/${
          repository.name
        }`;
      } else {
        if (project) {
          return `${repository.registry.endpoint}/${project.project_name}/${
            repository.name
          }`;
        } else {
          return `${repository.registry.endpoint}/${repository.name}`;
        }
      }
    } else {
      return `${this.environments.alauda_image_index}/${repository.repo_path}`;
    }
  }
}
