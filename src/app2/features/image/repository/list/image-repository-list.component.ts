import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { pipe, sortBy } from 'ramda';

import {
  ImageRegistry,
  ImageRegistryService,
} from 'app/services/api/image-registry.service';
import { getCookie, setCookie } from 'app/utils/cookie';

const REGISTRY_COOKIE = 'registry';

@Component({
  templateUrl: './image-repository-list.component.html',
})
export class ImageRepositoryListComponent implements OnInit {
  registries: ImageRegistry[];
  selectedIndex: number;

  constructor(
    private imageRegistry: ImageRegistryService,
    private route: ActivatedRoute,
  ) {}

  async ngOnInit() {
    const registries = await this.imageRegistry.find();
    // // FIXME: typescript升级到 3.4.x后可以去掉泛型
    const sortFn = pipe<ImageRegistry[], ImageRegistry[], ImageRegistry[]>(
      sortBy((registry: ImageRegistry) =>
        (registry.display_name || registry.name).toLowerCase(),
      ),
      sortBy((registry: ImageRegistry) => registry.is_public),
    );

    this.registries = sortFn(registries);
    this.registries = registries;

    const registryName =
      this.route.snapshot.queryParams.registryName ||
      getCookie(REGISTRY_COOKIE);

    const selectedIndex = this.registries.findIndex(
      ({ name }) => name === registryName,
    );

    this.selectedIndex = selectedIndex === -1 ? 0 : selectedIndex;
  }

  onSelectedIndexChange(selectedIndex: number) {
    this.selectedIndex = selectedIndex;
    setCookie(REGISTRY_COOKIE, this.registries[selectedIndex].name);
  }
}
