import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { ImageProjectService } from 'app/services/api/image-project.service';
import { ImageRegistry } from 'app/services/api/image-registry.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';

@Component({
  templateUrl: './image-repository-add-project.component.html',
})
export class ImageRepositoryAddProjectComponent {
  @Input()
  registry: ImageRegistry;
  @Output()
  confirmed: EventEmitter<boolean> = new EventEmitter();

  @ViewChild('form')
  form: NgForm;

  name: string;
  loading: boolean;

  constructor(
    @Inject(DIALOG_DATA)
    private data: {
      registry: ImageRegistry;
    },
    private imageProject: ImageProjectService,
    private errorsToast: ErrorsToastService,
  ) {}

  async addProject() {
    if (this.form.invalid) {
      return;
    }
    this.loading = true;
    try {
      await this.imageProject.addProject(
        this.data.registry.name,
        this.name,
        this.data.registry.is_third,
      );
    } catch (e) {
      return this.errorsToast.error(e);
    } finally {
      this.loading = false;
    }
    this.confirmed.next(true);
  }
}
