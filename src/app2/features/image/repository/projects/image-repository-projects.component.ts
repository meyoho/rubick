import {
  ConfirmType,
  DialogService,
  DialogSize,
  NotificationService,
  PageEvent,
} from '@alauda/ui';
import { Component, Inject, Input, OnInit } from '@angular/core';

import { get } from 'lodash-es';

import {
  ImageProject as _ImageProject,
  ImageProjectService,
} from 'app/services/api/image-project.service';
import { ImageRegistry } from 'app/services/api/image-registry.service';
import {
  ImageRepository,
  ImageRepositoryService,
} from 'app/services/api/image-repository.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';
import { ImageRepositoryAddProjectComponent } from 'app2/features/image/repository/projects/image-repository-add-project.component';

type ImageProject = _ImageProject & { is_default?: boolean };

@Component({
  selector: 'rc-image-repository-projects',
  templateUrl: './image-repository-projects.component.html',
  styleUrls: ['./image-repository-projects.component.scss'],
})
export class ImageRepositoryProjectsComponent implements OnInit {
  @Input()
  registry: ImageRegistry;

  weblabs: Weblabs;

  projectCreateDisplay: boolean;
  repositoryCreateEnabled: boolean;
  repositoryLoading: boolean;
  isThird: boolean;

  defaultProject = {
    project_name: this.translate.get('default_project_name'),
    is_default: true,
  } as ImageProject;

  currentProject: ImageProject;
  projects: ImageProject[];
  repositories: ImageRepository[] = [];

  count = 0;
  page = 0;
  pageSize = 20;
  search: string;

  constructor(
    @Inject(WEBLABS) weblabs: Weblabs,
    private roleUtilities: RoleUtilitiesService,
    private imageRepository: ImageRepositoryService,
    private imageProject: ImageProjectService,
    private translate: TranslateService,
    private dialogService: DialogService,
    private auiNotificationService: NotificationService,
  ) {
    this.weblabs = weblabs;
  }

  async ngOnInit() {
    const registry_name = this.registry.name;
    this.isThird = this.registry.is_third;

    let projects: ImageProject[] = [];

    if (!this.registry.is_public) {
      projects = await this.getProjects(this.isThird);
      this.projectCreateDisplay = this.isThird
        ? true
        : await this.roleUtilities.resourceTypeSupportPermissions(
            'registry_project',
            {
              registry_name,
            },
          );
    }

    this.projects = this.isThird
      ? projects
      : [this.defaultProject].concat(projects);
    if (this.projects.length > 0) {
      this.selectProject(this.projects[0]);
    }
  }

  trackByUuid(repository: ImageRepository) {
    return repository.uuid;
  }

  registryButtonDisplayExpr(action: string) {
    return this.roleUtilities.resourceHasPermission(
      this.registry,
      'registry',
      action,
    );
  }

  repositoryButtonDisplayExpr(repository: ImageRepository, action: string) {
    return this.roleUtilities.resourceHasPermission(
      repository,
      'repository',
      action,
    );
  }

  projectButtonDisplayExpr(project: ImageProject, action: string) {
    return this.roleUtilities.resourceHasPermission(
      project,
      'registry_project',
      action,
    );
  }

  getProjects(is_third?: boolean) {
    return this.imageProject.getProjects(this.registry.name, is_third);
  }

  async selectProject(project: ImageProject) {
    if (this.repositoryLoading || this.currentProject === project) {
      return;
    }

    this.currentProject = project;
    const registry_project = this.currentProject.is_default
      ? ''
      : this.currentProject.project_name;

    this.repositoryCreateEnabled = this.isThird
      ? false
      : await this.roleUtilities.resourceTypeSupportPermissions('repository', {
          registry_name: this.registry.name,
          registry_project,
        });

    this.getRepositories();
  }

  async getRepositories(pageEvent?: Partial<PageEvent>) {
    this.repositoryLoading = true;
    let { pageIndex = this.page } = pageEvent || {};
    const { pageSize = this.pageSize } = pageEvent || {};

    if (pageSize !== this.pageSize || (this.search && !pageEvent)) {
      pageIndex = 0;
    }
    const project = this.currentProject;
    const is_third_project = get(project, 'registry.is_third') || null;

    try {
      const {
        count,
        num_pages,
        results,
      } = await this.imageRepository.getRepositories({
        registryName: this.registry.name,
        projectName: project.is_default ? null : project.project_name,
        params: {
          page: pageIndex + 1,
          page_size: pageSize,
          search: this.search,
          is_third: is_third_project,
        },
      });

      if (num_pages && pageIndex + 1 > num_pages) {
        this.page = 0;
        this.getRepositories();
        return;
      }

      this.page = pageIndex;
      this.pageSize = pageSize;
      this.count = count;
      this.repositories = results;
    } catch (e) {}

    this.repositoryLoading = false;
  }

  async addProject() {
    const modalRef = await this.dialogService.open(
      ImageRepositoryAddProjectComponent,
      {
        size: DialogSize.Big,
        data: {
          registry: this.registry,
        },
      },
    );

    modalRef.componentInstance.confirmed.subscribe(
      async (isConfirm: boolean) => {
        modalRef.close(isConfirm);

        if (!isConfirm) {
          return;
        }

        const projects = await this.getProjects(this.isThird);
        this.projects = this.isThird
          ? projects
          : [this.defaultProject].concat(projects);
        this.selectProject(this.projects[0]);
      },
    );
  }

  async deleteProject(project: ImageProject) {
    if (project.repo_count > 0) {
      return this.auiNotificationService.warning({
        content: this.translate.get('deleted_project_not_empty'),
      });
    }

    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: `${this.translate.get('delete')}${this.translate.get(
          'repository_project',
        )} ${project.project_name}?`,
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Danger,
      });
    } catch (e) {
      return;
    }

    try {
      await this.imageProject.deleteProject(
        this.registry.name,
        project.project_name,
        this.isThird,
      );
      this.projects = this.projects.filter(
        ({ project_name }) => project_name !== project.project_name,
      );
      this.auiNotificationService.success({
        content: this.translate.get('delete_success'),
      });
    } catch (e) {
      this.auiNotificationService.warning({
        content: this.translate.get('delete_failed'),
      });
    } finally {
      this.selectProject(this.projects[0]);
    }
  }

  async deleteRepository(
    repository: ImageRepository,
    {
      before,
      after,
    }: {
      before: () => void;
      after: () => void;
    },
  ) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: `${this.translate.get('delete')}${this.translate.get(
          'repository',
        )} ${repository.name}?`,
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Danger,
      });
    } catch (e) {
      return;
    }

    before();

    try {
      await this.imageRepository.deleteRepository(
        this.registry.name,
        repository.name,
        this.currentProject.is_default
          ? null
          : this.currentProject.project_name,
      );
    } catch (e) {
      return this.auiNotificationService.warning({
        content: this.translate.get('repo_delete_failed'),
      });
    }

    const { project } = repository;

    if (!project) {
      this.removeRepository(repository);
    } else if (project.project_name === this.currentProject.project_name) {
      this.removeRepository(repository);
      this.currentProject.repo_count--;
    }

    this.auiNotificationService.success({
      content: this.translate.get('repo_delete_success'),
    });

    after();
  }

  removeRepository(repository: ImageRepository) {
    this.repositories = this.repositories.filter(
      ({ name }) => name !== repository.name,
    );
  }
}
