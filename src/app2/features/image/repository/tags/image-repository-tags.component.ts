import {
  ConfirmType,
  DialogService,
  NotificationService,
  PageEvent,
} from '@alauda/ui';
import {
  Component,
  forwardRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';

import { get, sortBy } from 'lodash-es';

import {
  ImageRepository,
  ImageRepositoryService,
  ImageRepositoryTag as _ImageRepositoryTag,
} from 'app/services/api/image-repository.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { ImageRepositoryDetailComponent } from 'app2/features/image/repository/detail/image-repository-detail.component';
import { delay } from 'app2/utils';

interface ImageRepositoryTag extends _ImageRepositoryTag {
  summaryList?: Array<{
    level: string;
    count: string;
  }>;
}

@Component({
  selector: 'rc-image-repository-tags',
  templateUrl: './image-repository-tags.component.html',
  styleUrls: ['./image-repository-tags.component.scss'],
})
export class ImageRepositoryTagsComponent implements OnInit, OnDestroy {
  @Input()
  repository: ImageRepository;

  count: number;
  page = 0;
  pageSize = 20;

  initialized: boolean;
  destroyed: boolean;
  loading: boolean;
  tags: ImageRepositoryTag[];
  isThird: boolean;

  columns: string[];
  constructor(
    @Inject(forwardRef(() => ImageRepositoryDetailComponent))
    private $parent: ImageRepositoryDetailComponent,
    private imageRepository: ImageRepositoryService,
    private translate: TranslateService,
    private errorService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
  ) {}

  async ngOnInit() {
    this.isThird = this.repository.registry.is_third;

    this.startPolling();
    const columns: string[] = ['image_tag', 'created_at'];
    this.columns = this.isThird ? columns.concat(['action']) : columns;
  }

  ngOnDestroy() {
    this.destroyed = true;
    this.dialogService.closeAll();
  }

  async startPolling() {
    try {
      await this.getTags();
    } catch (e) {}

    await delay(30000);

    if (!this.destroyed) {
      this.startPolling();
    }
  }

  async getTags(pageEvent?: Partial<PageEvent>) {
    let { pageIndex = this.page } = pageEvent || {};
    const { pageSize = this.pageSize } = pageEvent || {};

    if (pageSize !== this.pageSize) {
      pageIndex = 0;
    }
    const { registry, project } = this.repository;
    this.page = pageIndex;
    this.pageSize = pageSize;
    try {
      this.loading = true;

      const third_parmas = this.isThird
        ? {
            is_third: this.isThird,
          }
        : {
            page_size: this.pageSize,
            page: this.page + 1,
          };

      let count,
        tags: ImageRepositoryTag[] = [];
      const tagsResult: any = await this.imageRepository.getRepositoryTags({
        registryName: registry.name,
        projectName: project && project.project_name,
        repositoryName: this.repository.name,
        params: {
          view_type: 'detail',
          ...third_parmas,
        },
      });
      if (Array.isArray(tagsResult)) {
        tags = tagsResult;
      } else {
        count = get(tagsResult, 'count');
        tags = get(tagsResult, 'results');
      }

      this.count = count || 0;

      tags.forEach(tag => {
        const summaryList = Object.keys(tag.summary).map(key => ({
          level: key,
          count: tag.summary[key],
        }));
        (tag as ImageRepositoryTag).summaryList = sortBy(summaryList, item =>
          item.level === 'Fixable' ? 1 : -1,
        );
      });

      this.tags = tags;
    } catch (e) {
      this.tags = [];
    }

    this.initialized = true;
    this.loading = false;
  }

  async deleteTag(tag: ImageRepositoryTag) {
    const tagName = tag.tag_name;

    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content:
          this.translate.get('delete_confirm') +
          this.translate.get('image_tag') +
          ' ' +
          tagName +
          ' ?',
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Danger,
      });
    } catch (e) {
      return;
    }

    const { repository } = this;
    try {
      await this.imageRepository.deleteRepositoryTag({
        registry_name: repository.registry.name,
        repository_name: repository.name,
        project_name: repository.project && repository.project.project_name,
        tag_name: tagName,
        is_third: this.isThird,
      });
      this.tags = this.tags.filter(({ tag_name }) => tag_name !== tagName);
      this.$parent.tagsCount = this.tags.length;

      this.auiNotificationService.success({
        content: this.translate.get('repo_tag_delete_success'),
      });
    } catch (e) {
      this.errorService.error(e);
    }
  }

  trackByFn(_index: number) {
    return _index;
  }
}
