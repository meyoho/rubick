import { DialogService } from '@alauda/ui';
import { MessageService } from '@alauda/ui';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { cloneDeep } from 'lodash-es';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { IntegrationCenterService } from 'app/services/api/integration-center.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { IntegrationUtilitiesService } from '../integration-center.utilities.service';

@Component({
  templateUrl: './integration-center-detail.component.html',
  styleUrls: ['./integration-center-detail.component.scss'],
})
export class IntegrationCenterDetailComponent implements OnInit, OnDestroy {
  private routeParamsSubscription: Subscription;
  integrationData: any;
  configFields: any;
  typeConfig: any;
  uuid: string;
  loading = false;

  constructor(
    public integrationUtilities: IntegrationUtilitiesService,
    private route: ActivatedRoute,
    private integrationCenterService: IntegrationCenterService,
    private errorsToastService: ErrorsToastService,
    private translate: TranslateService,
    private auiMessageService: MessageService,
    private dialogService: DialogService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.routeParamsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.uuid = params['uuid'];
        await this.refetch();
      });
  }

  ngOnDestroy() {
    this.routeParamsSubscription.unsubscribe();
  }

  async refetch() {
    let catalog: any;
    const p1 = this.integrationCenterService.getIntegrationById(this.uuid);
    const p2 = this.integrationCenterService.getIntegrationCatalog();
    this.loading = true;
    try {
      [this.integrationData, catalog] = await Promise.all([p1, p2]);
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.loading = false;

    this.typeConfig = catalog.types.find(
      (type: any) => type.name === this.integrationData.type,
    );
    const family = this.typeConfig.families.join(',');
    this.integrationData.integration_family = this.translate.get(
      'integration_family_name_' + family,
    );
    this.configFields = this.buildConfigInfoFields(
      this.integrationData.fields,
      this.typeConfig,
    );

    const typesMap = {};
    catalog.types.forEach((type: any) => {
      typesMap[type.name] = type;
    });
    this.integrationData.logo = typesMap[this.integrationData.type].logo;
  }

  buildConfigInfoFields(fields: any, typeConfig: any) {
    const result: any[] = [];
    Object.entries(typeConfig.schema)
      .sort(([, item1], [, item2]) => {
        return item1['order'] - item2['order'];
      })
      .forEach(([key, item]) => {
        if (item['display'] === false) {
          return;
        }
        const value = item['type'] === 'password' ? '********' : fields[key];
        result.push({
          name: key,
          display_text: item['display_text'],
          value,
        });
      });
    return result;
  }

  isLink(value: string) {
    return /^(http(s?):\/\/.+)/.test(value);
  }

  getImg(type: string) {
    return `/static/images/integration-logo/${type}.png`;
  }

  async switchStatus() {
    try {
      const updateBody = cloneDeep(this.integrationData);
      updateBody.enabled = !this.integrationData.enabled;
      await this.integrationCenterService.updateIntegration(
        updateBody,
        this.uuid,
      );
      this.auiMessageService.success({
        content: this.translate.get(
          this.integrationData.enabled
            ? 'integration_switch_off_success_title'
            : 'integration_switch_on_success_title',
        ),
      });
      this.refetch();
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  async deleteIntegration() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('integration_delete_config_hint', {
          name: this.integrationData.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return false;
    }
    try {
      await this.integrationCenterService.deleteIntegrationById(this.uuid);
      this.router.navigateByUrl('/console/admin/integration_center/list');
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  updateIntegration() {
    this.router.navigateByUrl(
      `/console/admin/integration_center/update/${this.uuid}`,
    );
  }
}
