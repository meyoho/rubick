import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { IntegrationCenterCreateComponent } from './create/integration-center-create.component';
import { IntegrationCenterDetailComponent } from './detail/integration-center-detail.component';
import { IntegrationCenterListComponent } from './list/integration-center-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: IntegrationCenterListComponent,
  },
  {
    path: 'detail/:uuid',
    component: IntegrationCenterDetailComponent,
  },
  {
    path: 'create/:typeName',
    component: IntegrationCenterCreateComponent,
  },
  {
    path: 'update/:integrationId',
    component: IntegrationCenterCreateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class IntegrationCenterRoutingModule {}
