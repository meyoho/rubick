import { DialogService } from '@alauda/ui';
import { Component, Input, OnChanges } from '@angular/core';
import { Router } from '@angular/router';
import { TranslateService } from 'app/translate/translate.service';

import { SelectIntegrationTypeDialogComponent } from '../../select-integration-type-dialog/select-integration-type.component';

@Component({
  selector: 'rc-integration-center-list-cards',
  templateUrl: './integration-center-list-cards.component.html',
  styleUrls: ['./integration-center-list-cards.component.scss'],
})
export class IntegrationCenterCardsComponent implements OnChanges {
  @Input()
  familyName: any;
  @Input()
  integrations: any;
  @Input()
  couldCreate: any;
  @Input()
  catalog: any;
  keyWord = '';
  showIntegrations: any;

  constructor(
    private router: Router,
    private dialog: DialogService,
    private translate: TranslateService,
  ) {}

  ngOnChanges() {
    this.showIntegrations = this.integrations;
  }

  getDetailHref(id: string) {
    this.router.navigateByUrl(`/console/admin/integration_center/detail/${id}`);
  }

  getImg(type: string) {
    return `/static/images/integration-logo/${type}.png`;
  }

  filterIntegrations() {
    this.showIntegrations = this.integrations.filter((integration: any) => {
      return integration.name.indexOf(this.keyWord.toLowerCase()) >= 0;
    });
  }

  searchChanged(queryString: string) {
    this.keyWord = queryString;
    this.filterIntegrations();
  }

  addIntegration() {
    this.dialog.open(SelectIntegrationTypeDialogComponent, {
      data: {
        title: this.translate.get('integration_add_integration'),
        payload: {
          catalog: this.catalog,
          familyName: this.familyName,
        },
      },
    });
  }
}
