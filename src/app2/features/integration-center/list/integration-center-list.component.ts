import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';

import { IntegrationCenterService } from 'app/services/api/integration-center.service';
import { RoleService } from 'app/services/api/role.service';

@Component({
  templateUrl: './integration-center-list.component.html',
  styleUrls: ['./integration-center-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IntegrationCenterListComponent implements OnInit {
  pageSize = 200;
  count = 0;

  families: any;
  selectedFamily: any;
  catalog: any;
  createPermission: boolean;
  familiesMap = {};
  initialized = false;

  constructor(
    private integrationCenterService: IntegrationCenterService,
    private roleService: RoleService,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    let integrations: any, permission: any;
    const p1 = this.integrationCenterService.getIntegrationCatalog();
    const p2 = this.integrationCenterService.getIntegrationList({
      page_size: this.pageSize,
    });
    const p3 = this.roleService.getPluralContextPermissions(['integration']);

    try {
      [this.catalog, integrations, permission] = await Promise.all([
        p1,
        p2,
        p3,
      ]);
    } catch (errs) {
      return;
    } finally {
      this.initialized = true;
    }

    this.createPermission =
      permission.integration.indexOf('integration:create') >= 0;
    this.families = this.catalog.families;

    this.families.forEach((family: any, index: number) => {
      this.familiesMap[family] = {
        integrations: [],
        name: family,
        index,
      };
    });

    const typesMap = {};
    this.catalog.types.forEach((type: any) => {
      typesMap[type.name] = type;
    });

    integrations.forEach((integration: any) => {
      if (!typesMap[integration.type]) {
        return;
      }
      integration.logo = typesMap[integration.type].logo;
      integration.typeDisplayText = typesMap[integration.type].display_text;
      typesMap[integration.type].families.forEach((family: any) => {
        this.familiesMap[family].integrations.push(integration);
      });
    });

    this.handleFamilySelected(0);
  }

  handleFamilySelected(index: number) {
    this.selectedFamily = this.familiesMap[this.families[index]];
    this.cdr.detectChanges();
  }
}
