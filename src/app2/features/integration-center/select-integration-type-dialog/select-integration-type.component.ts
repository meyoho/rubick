import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './select-integration-type.component.html',
  styleUrls: ['./select-integration-type.component.scss'],
})
export class SelectIntegrationTypeDialogComponent implements OnInit {
  catalog: any;
  families: any;
  allTypes: any;
  types: any;
  selectedFamily: any;
  loading = false;

  constructor(
    private translate: TranslateService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
    private dialogRef: DialogRef,
    @Inject(DIALOG_DATA)
    public dialogData: {
      title: string;
      payload: {
        catalog: any;
        familyName: string;
      };
    },
  ) {}

  private complete() {
    this.dialogRef.close();
  }

  ngOnInit() {
    this.catalog = this.dialogData.payload.catalog;
    this.loading = true;
    try {
      this.families = [
        {
          displayName: this.translate.get('all'),
          name: '_all',
        },
      ];
      this.catalog.families.forEach((family: any) => {
        this.families.push({
          displayName: this.translate.get('integration_family_name_' + family),
          name: family,
        });
      });
      this.allTypes = this.catalog.types;
      this.selectedFamily = this.dialogData.payload.familyName;
      this.onFamilyChanged(this.selectedFamily);
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.loading = false;
  }

  onFamilyChanged(familyName: string) {
    if (familyName === '_all') {
      this.types = this.allTypes;
    } else {
      this.types = this.allTypes.filter((type: any) =>
        type.families.includes(familyName),
      );
    }
  }

  onSelected(typeName: string) {
    this.complete();
    this.router.navigateByUrl(
      `/console/admin/integration_center/create/${typeName}`,
    );
  }
}
