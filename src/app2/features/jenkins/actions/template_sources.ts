import { Action } from '@ngrx/store';

import { createTypes } from '../utils/types';

enum ActionTypes {
  Find = 'Find',
  FindComplete = 'FindComplete',
  Get = 'Get',
}
export const types = createTypes(
  '@jenkinsTemplate.template_sources',
  ActionTypes,
);

export class Find implements Action {
  public readonly type = types.Find;
  constructor(public with_public: boolean = true) {}
}

export class Get implements Action {
  public readonly type = types.Get;
  constructor(public with_public: boolean = true) {}
}

export class FindComplete implements Action {
  public readonly type = types.FindComplete;
  constructor(public items: any[]) {}
}

export type All = Get | Find | FindComplete;
