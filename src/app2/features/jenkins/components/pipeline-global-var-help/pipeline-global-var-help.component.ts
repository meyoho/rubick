import { DIALOG_DATA } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';

import { JenkinsService } from 'app/services/api/jenkins.service';
import { TranslateService } from 'app/translate/translate.service';

import { VARIABLE_HELP_CN, VARIABLE_HELP_EN } from './pipeline-global-var-help';

@Component({
  templateUrl: './pipeline-global-var-help.component.html',
  styleUrls: ['./pipeline-global-var-help.component.scss'],
})
export class PipelineGlobalVarHelpComponent implements OnInit {
  env_vars: any[];
  language: string;
  HELP_CONTENT: string;

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      sourceInfo: any;
    },
    private jenkinsService: JenkinsService,
    private translate: TranslateService,
  ) {
    this.language = this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
  }
  ngOnInit() {
    this.jenkinsService.templates
      .vars(this.dialogData.sourceInfo)
      .then((result: any[]) => {
        this.env_vars = result;
      });
    this.HELP_CONTENT =
      this.language === 'en' ? VARIABLE_HELP_EN : VARIABLE_HELP_CN;
  }

  targetLanguageString(target: any) {
    return (target && target[this.language]) || '';
  }
}
