import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  HostBinding,
  Input,
  Output,
} from '@angular/core';

import { DiagramConfig } from '../pipeline-history-diagram.component';
import { Node } from '../types';

@Component({
  // tslint:disable-next-line:component-selector
  selector: '[rc-jenkins-pipeline-history-diagram-node]',
  styleUrls: ['node.component.scss'],
  templateUrl: 'node.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryDiagramNodeComponent {
  @Input()
  node: Node;
  @Input()
  entities: any;
  @Input()
  selected: string;
  @Input()
  isBranch: boolean;
  @Input()
  diagramConfig: DiagramConfig;
  @Output()
  selectedChange = new EventEmitter<string>();

  @HostBinding('attr.transform')
  get transform() {
    return this.isBranch
      ? `translate(0, ${this.node.y * this.diagramConfig.NODE_DISTANCE_Y})`
      : `translate(${this.node.x * this.diagramConfig.NODE_DISTANCE_X},${this
          .node.y * this.diagramConfig.NODE_DISTANCE_Y})`;
  }

  get text() {
    if (['begin', 'end'].includes(this.node.type)) {
      return '';
    }
    if (this.diagramConfig.NODE_DISTANCE_Y < 60) {
      let name: string = this.entities[this.node.id].name;
      name = name.length > 20 ? name.substring(0, 12) + '...' : name;
      return name;
    } else {
      return this.entities[this.node.id].name;
    }
  }

  get status() {
    if (['begin', 'end'].includes(this.node.type)) {
      return '';
    }

    return ['FINISHED', 'NOT_BUILT'].includes(
      this.entities[this.node.id].status,
    )
      ? this.entities[this.node.id].result
      : this.entities[this.node.id].status;
  }

  get radius() {
    return ['begin', 'end'].includes(this.node.type)
      ? this.diagramConfig.END_RADIUS
      : this.diagramConfig.NODE_RADIUS;
  }

  get icon() {
    switch (this.status) {
      case 'PAUSED':
        return '';
      case 'SUCCESS':
        return '';
      case 'FAILURE':
        return '';
      default:
        return '';
    }
  }

  branchTracker(_index: number, branch: Node) {
    return branch.id;
  }

  onClick(id: string): void {
    if (!id) {
      return;
    }

    this.selectedChange.next(id);
  }
}
