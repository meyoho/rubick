import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';

import { extend, flatMap, head, last } from 'lodash-es';

import {
  END_RADIUS,
  NODE_DISTANCE_X,
  NODE_DISTANCE_Y,
  NODE_RADIUS,
  PADDING,
  TEXT_OFFSET,
} from './constants';
import { DiagramState, Link, Node } from './types';

export interface DiagramConfig {
  NODE_RADIUS?: number;
  END_RADIUS?: number;
  NODE_DISTANCE_X?: number;
  NODE_DISTANCE_Y?: number;
  PADDING?: number;
  TEXT_OFFSET?: number;
}

export const DefaultConfig: DiagramConfig = {
  NODE_RADIUS: NODE_RADIUS,
  END_RADIUS: END_RADIUS,
  NODE_DISTANCE_X: NODE_DISTANCE_X,
  NODE_DISTANCE_Y: NODE_DISTANCE_Y,
  PADDING: PADDING,
  TEXT_OFFSET: TEXT_OFFSET,
};

@Component({
  selector: 'rc-jenkins-pipeline-history-diagram',
  styleUrls: ['pipeline-history-diagram.component.scss'],
  templateUrl: 'pipeline-history-diagram.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryDiagramComponent implements OnChanges, OnInit {
  @Input()
  stages: any[];
  @Input()
  start: string;
  @Input()
  selected: string;
  @Input()
  diagramConfig: DiagramConfig = DefaultConfig;
  @Output()
  selectedChange = new EventEmitter<string>();
  nodes: Node[];
  links: Link[];
  entities: {
    [id: string]: any;
  };

  get width() {
    if (!this.nodes || !this.nodes.length) {
      return 0;
    }
    return (
      this.diagramConfig.PADDING * 2 +
      (this.nodes.length - 1) * this.diagramConfig.NODE_DISTANCE_X
    );
  }

  get height() {
    if (!this.nodes || !this.nodes.length) {
      return 0;
    }
    const maxBranchCount = Math.max(
      ...this.nodes.map(node => (node.children && node.children.length) || 1),
    );
    return (
      this.diagramConfig.PADDING * 2 +
      (maxBranchCount - 1) * this.diagramConfig.NODE_DISTANCE_Y +
      24
    );
  }

  get transform() {
    return `translate(${this.diagramConfig.PADDING},${this.diagramConfig
      .PADDING - this.diagramConfig.TEXT_OFFSET})`;
  }

  ngOnChanges(changes: SimpleChanges) {
    if (
      changes.stages &&
      changes.stages.currentValue &&
      changes.stages.currentValue !== changes.stages.previousValue
    ) {
      this.resetState();
    }
  }

  ngOnInit() {
    this.diagramConfig = extend({}, DefaultConfig, this.diagramConfig);
  }

  linkTracker(_index: number, link: Link) {
    const [from, to] = link;
    return `${from.id},${to.id}`;
  }

  nodeTracker(_index: number, node: Node) {
    return node.id || `@@${node.type}`;
  }

  resetState() {
    if (!this.stages || !this.stages.length) {
      this.nodes = [];
      this.links = [];
      this.entities = {};
      return;
    }
    this.entities = this.stages.reduce(
      (prev, item) => ({
        ...prev,
        [item.id]: item,
      }),
      {},
    );
    const begin: Node = { type: 'begin', x: 0, y: 0 };
    const { nodes, links } = this.travel(
      { nodes: [begin], links: [] },
      this.start || head(this.stages).id,
    );
    this.nodes = nodes;
    this.links = links;
  }

  travel({ nodes, links }: DiagramState, id: string): DiagramState {
    const stage: Node = id
      ? this.populate(id, nodes.length)
      : { type: 'end', x: nodes.length, y: 0 };
    const prev = last(nodes);
    const newState = {
      nodes: [...nodes, stage],
      links: [
        ...links,
        ...createLinks(
          !prev.children ? [prev] : [],
          !stage.children ? [stage] : [],
        ),
        ...createLinks(prev.children || [], [stage]),
        ...createLinks([prev], stage.children || []),
      ],
    };
    return stage.type === 'end'
      ? newState
      : this.travel(newState, this.next(id));
  }

  next(id: string): string {
    const entity = this.entities[id];
    if (entity.edges.length === 0) {
      return null;
    }
    const edge: any = head(entity.edges);
    if (entity.edges.length === 1) {
      return edge.id;
    }
    return this.next(edge.id);
  }

  populate(id: string, x: number): Node {
    const entity = this.entities[id];
    const children =
      entity.edges.length > 1
        ? entity.edges.map(
            (edge: any, y: number): Node => ({
              type: 'branch',
              id: edge.id,
              x,
              y,
            }),
          )
        : null;
    return {
      type: 'stage',
      id,
      x,
      y: 0,
      children,
    };
  }
}

function createLinks(froms: Node[], tos: Node[]): Link[] {
  return flatMap(froms, from => tos.map((to): Link => [from, to]));
}
