export interface Node {
  type: 'begin' | 'end' | 'stage' | 'branch';
  x: number;
  y: number;
  id?: string;
  children?: Node[];
}

export type Link = [Node, Node];

export interface DiagramState {
  nodes: Node[];
  links: Link[];
}
