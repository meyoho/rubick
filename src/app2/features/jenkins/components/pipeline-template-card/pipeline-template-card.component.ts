import { DialogService, DialogSize } from '@alauda/ui';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { extend, get } from 'lodash-es';

import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';
import { templateStagesConvert } from 'app_user/features/jenkins/services/pipeline-template.service'; //todo: ??
import { DefaultConfig, DiagramConfig } from '../pipeline-history-diagram';
import { PipelineTemplateInfoComponent } from '../pipeline-template-info';

const CardDiagramConfig: DiagramConfig = {};
Object.keys(DefaultConfig).forEach((key: string) => {
  CardDiagramConfig[key] = DefaultConfig[key] * 0.5;
});

export enum DisplayType {
  List,
  Dialog,
}

extend(CardDiagramConfig, { NODE_DISTANCE_X: 80, TEXT_OFFSET: -16 });
@Component({
  selector: 'rc-pipeline-template-card',
  templateUrl: './pipeline-template-card.component.html',
  styleUrls: ['./pipeline-template-card.component.scss'],
})
export class PipelineTemplateCardComponent implements OnInit {
  BRIEF_DESC_NAME = `${this.env.label_base_domain}/readme`;
  VERSION_NAME = `${this.env.label_base_domain}/version`;
  diagramConfig: DiagramConfig = CardDiagramConfig;
  tags: any[] = [];
  language: string;
  @Input()
  canCreatePipeline: boolean;
  @Input()
  template: any;
  @Input()
  type: number;
  @Input()
  routeParams: any;

  constructor(
    private dialogService: DialogService,
    private roleUtilities: RoleUtilitiesService,
    private translate: TranslateService,
    private router: Router,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    this.language = this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
  }

  ngOnInit() {
    this.template = mapperToPageModal(this.template);
    Object.keys(this.template.labels).forEach((key: string) => {
      this.tags.push({ key: key, value: this.template.labels[key] });
    });
  }

  shouldShowDetail() {
    return this.roleUtilities.resourceHasPermission(
      this.template,
      'jenkins_pipeline_template',
      'get',
    );
  }

  showHelp() {
    if (this.type === DisplayType.Dialog || !this.shouldShowDetail()) {
      return;
    }
    this.dialogService.open(PipelineTemplateInfoComponent, {
      size: DialogSize.Large,
      data: {
        template: this.template,
        title: this.template.display_name[`${this.language}`],
      },
    });
  }
  async createPipeline() {
    const status = get(this.template, 'status.phase', false);
    if (status) {
      try {
        this.dialogService.closeAll();
        await this.dialogService
          .confirm({
            title: this.translate.get(
              'jenkins_template_create_pipeline_unstable_title',
            ),
            content: this.translate.get(
              'jenkins_template_create_pipeline_unstable_tips',
            ),
            confirmText: this.translate.get('jenkins_pipeline_create'),
            cancelText: this.translate.get('cancel'),
          })
          .then(() => {});
      } catch (err) {
        return;
      }
    }
    this.dialogService.closeAll();
    this.router.navigate([
      '/console/user',
      'workspace',
      this.routeParams,
      'jenkins',
      'pipelines',
      'templates',
      'template_create_pipeline',
      this.template.uuid,
    ]);
  }

  showRelatedPipelines() {
    this.router.navigate(['/console/admin/jenkins/pipelines'], {
      queryParams: {
        search: '',
        uuid: this.template.uuid,
      },
    });
  }

  get briefDes(): string {
    return this.template.annotations[
      `${this.BRIEF_DESC_NAME}.${this.language}`
    ];
  }

  get displayName(): string {
    return get(this.template, `display_name.${this.language}`, '');
  }

  get version() {
    const an = get(this.template, `annotations`, {});
    return an[`${this.VERSION_NAME}`];
  }
}

export function mapperToPageModal(template: any) {
  const {
    name,
    uuid,
    source,
    display_name,
    definition: {
      metadata: { annotations, labels },
      spec: { stages },
      status,
    },
    resource_actions,
  } = template;
  return {
    name,
    uuid,
    display_name,
    status,
    labels,
    source,
    stages: templateStagesConvert(stages),
    arguments: get(template, 'definition.spec.arguments'),
    annotations,
    resource_actions,
  };
}
