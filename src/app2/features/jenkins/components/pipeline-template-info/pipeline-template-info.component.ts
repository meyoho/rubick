import { DIALOG_DATA } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';
import { Template } from 'app_user/features/jenkins/services/pipeline-template.service';

@Component({
  templateUrl: './pipeline-template-info.component.html',
  styleUrls: ['./pipeline-template-info.component.scss'],
})
export class PipelineTemplateInfoComponent {
  DESCRIPTION_NAME = `${this.env.label_base_domain}/description`;
  BRIEF_DESC_NAME = `${this.env.label_base_domain}/readme`;
  template: Template;
  data: any;
  language: string;
  tags: any[] = [];
  diagramConfig = {};
  title = '';

  constructor(
    @Inject(ENVIRONMENTS) private env: Environments,
    @Inject(DIALOG_DATA)
    public dialogData: {
      title: string;
      template: any;
      type?: any; //todo: boolean ? string ?
    },
    private translate: TranslateService,
  ) {
    this.template = dialogData.template;
    this.title = this.dialogData.title;
    Object.keys(this.template.labels || {}).forEach((key: string) => {
      this.tags.push({ key: key, value: this.template.labels[key] });
    });
    this.language = this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
  }

  targetLanguageString(target: any) {
    return (target && target[this.language]) || '';
  }

  get description() {
    return this.template.annotations[
      `${this.DESCRIPTION_NAME}.${this.language}`
    ];
  }

  get briefDes() {
    return this.template.annotations[
      `${this.BRIEF_DESC_NAME}.${this.language}`
    ];
  }
}
