import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import { NotificationService } from '@alauda/ui';
import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { Store } from '@ngrx/store';

import { find, get } from 'lodash-es';
import {
  BehaviorSubject,
  from,
  interval,
  merge,
  Subject,
  Subscription,
} from 'rxjs';
import {
  debounceTime,
  distinctUntilChanged,
  first,
  map,
  skipWhile,
  switchMap,
} from 'rxjs/operators';

import { JenkinsService } from 'app/services/api/jenkins.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { JenkinsMessageService } from 'app_user/features/jenkins/services/jenkins-message.service';
import * as sourceActions from '../../actions/template_sources';
import { PipelineCustomTemplateManComponent } from '../../components/pipeline-custom-template-man';
import { PipelineTemplateSyncReportComponent } from '../../components/pipeline-template-sync-report';
import { PipelineTemplateSyncSettingsComponent } from '../../components/pipeline-template-sync-settings';
import { selectTemplateSourcesListItems, State } from '../../reducers';
import { PipelineTemplateListComponent } from './pipeline-template-list.component';

const TEMPLATE_REPO_RESOURCE_NAME = 'jenkins_pipeline_template_repository';

@Component({
  templateUrl: './pipeline-template-list-container.component.html',
  styleUrls: ['./pipeline-template-list-container.component.scss'],
})
export class PipelineTemplateListContainerComponent
  implements OnInit, OnDestroy {
  @ViewChild(PipelineTemplateListComponent)
  child: PipelineTemplateListComponent;

  @ViewChild('syncDetailTemplate')
  alertTemplate: TemplateRef<any>;

  syncReportRef: DialogRef<PipelineTemplateSyncReportComponent>;

  actions: any = {
    main: {
      type: 'sync',
      processing_text: 'jenkins_template_sync_doing',
      text: 'jenkins_template_sync',
      fn: this.sync.bind(this),
    },
    others: [
      {
        type: 'setting',
        text: 'jenkins_template_sync_repo_settings',
        disabled: false,
        fn: this.syncSettings.bind(this),
      },
      {
        type: 'status',
        text: 'jenkins_template_repo_sync_detail',
        disabled: true,
        fn: this.syncReport.bind(this),
      },
      {
        text: 'jenkins_custom_template_introduction',
        disabled: false,
        fn: this.man.bind(this),
      },
    ],
  };

  sources$ = this.store
    .select(selectTemplateSourcesListItems)
    .pipe(distinctUntilChanged());
  source: any;
  subs: Subscription[] = [];
  sourcesSub: Subscription;
  status$ = interval(10000);
  statusSub: Subscription;
  status: any;
  syncStatus: string;
  startSync$: Subject<any> = new Subject();
  sync$: Subject<any> = new Subject();
  canViewRepo = false;
  canManageRepo = false;
  statusInit = false;

  constructor(
    private dialogService: DialogService,
    private translate: TranslateService,
    private store: Store<State>,
    private jenkins: JenkinsService,
    private message: JenkinsMessageService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    @Inject(ACCOUNT) private account: Account,
  ) {}

  async ngOnInit() {
    this.subs.push(
      this.sources$
        .pipe(
          map(
            (source: any) => source && source.filter((s: any) => !s.is_public),
          ),
        )
        .subscribe((s: any[]) => {
          this.source = s.shift();
          if (this.source) {
            this.startSync$.next();
          }
        }),
    );
    this.subs.push(
      merge(this.status$.pipe(skipWhile(() => !this.source)), this.startSync$)
        .pipe(switchMap(() => from(this.refresh())))
        .subscribe((statusDetail: any) => {
          if (!statusDetail) {
            return;
          }
          this.status = statusDetail;
          if (get(this.syncReportRef, 'componentInstance.status$', '')) {
            this.syncReportRef.componentInstance.status$.next(statusDetail);
          }
          const stAction: any = find(this.actions.others, { type: 'status' });
          const settingAction: any = find(this.actions.others, {
            type: 'setting',
          });
          if (stAction) {
            stAction.disabled = !statusDetail;
          }
          const currentStatus: string = get(statusDetail, 'last_job.status');
          if (currentStatus === 'INPROGRESS' && settingAction) {
            settingAction.disabled = true;
          } else if (settingAction) {
            settingAction.disabled = false;
          }
          if (
            currentStatus === 'FAIL' &&
            get(statusDetail, 'last_job.step_at') === 'BEGIN'
          ) {
            this.auiNotificationService.error({
              title: this.translate.get('jenkins_template_sync_failed'),
              content: this.translate.get(
                'jenkins_template_sync_failed_reason_at_start',
              ),
              duration: 0,
            });
          }
          const createBy = get(statusDetail, 'last_job.created_by');
          if (
            this.statusInit &&
            this.syncStatus !== currentStatus &&
            (this.account.username === createBy ||
              this.account.namespace === createBy)
          ) {
            if (currentStatus === 'SUCCESS') {
              this.auiNotificationService.info({
                contentRef: this.alertTemplate,
                title: this.translate.get('jenkins_template_sync_finished'),
                content: this.translate.get(
                  'jenkins_template_sync_finished_description',
                  this.getSyncCounter(statusDetail),
                ),
                duration: 0,
              });
              this.child.setQuery();
            }
            if (currentStatus === 'FAIL') {
              const lastJob: any = get(statusDetail, 'last_job');
              if (lastJob.step_at === 'CLONE') {
                this.auiNotificationService.error({
                  contentRef: this.alertTemplate,
                  title: this.translate.get('jenkins_template_sync_failed'),
                  content: this.translate.get(
                    'jenkins_template_sync_failed_reason_is_clone',
                  ),
                  duration: 0,
                });
              } else if (lastJob.step_at === 'IMPORT') {
                this.auiNotificationService.error({
                  contentRef: this.alertTemplate,
                  title: this.translate.get('jenkins_template_sync_failed'),
                  content: this.translate.get(
                    'jenkins_template_sync_failed_reason_is_import',
                  ),
                  duration: 0,
                });
              } else {
                this.message.error(get(lastJob, 'errors[0]'));
              }
            }
          }
          this.syncStatus = currentStatus;
          this.statusInit = true;
        }),
    );
    this.subs.push(
      this.sync$.pipe(debounceTime(500)).subscribe(() => {
        this.doSync();
      }),
    );

    const ty = await this.roleUtil.resourceTypesPermissions([
      TEMPLATE_REPO_RESOURCE_NAME,
    ]);
    this.canManageRepo = ty[TEMPLATE_REPO_RESOURCE_NAME].includes(
      `${TEMPLATE_REPO_RESOURCE_NAME}:manage`,
    );
    this.canViewRepo = ty[TEMPLATE_REPO_RESOURCE_NAME].includes(
      `${TEMPLATE_REPO_RESOURCE_NAME}:list`,
    );
    if (!this.canManageRepo) {
      this.actions.others.shift();
      this.actions.main = this.actions.others.shift();
    }
  }

  ngOnDestroy() {
    this.subs.forEach((sub: Subscription) => sub.unsubscribe());
  }

  sync() {
    this.sync$.next();
  }

  doSync() {
    if (!this.source) {
      this.dialogService
        .confirm({
          title: this.translate.get('jenkins_template_source_unset'),
          content: this.translate.get('jenkins_template_source_unset_tips'),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
        })
        .catch(() => {});
    } else {
      this.syncStatus = '';
      this.jenkins.templates
        .refresh(this.source.uuid)
        .then(() => {
          this.showSyncingAlert();
        })
        .catch(errors => {
          this.message.warning(get(errors, 'errors[0]'));
        })
        .then(() => {
          this.startSync$.next();
        });
    }
  }

  syncSettings() {
    let model = {};
    if (this.status) {
      const { uuid, url, type, branch, username, password } = this.status;
      model = {
        uuid,
        url,
        type: type ? type : 'GIT',
        branch,
        username,
        password,
      };
    }
    const dialogRef = this.dialogService.open(
      PipelineTemplateSyncSettingsComponent,
      {
        size: DialogSize.Large,
        data: {
          model,
        },
      },
    );
    dialogRef.componentInstance.finished
      .pipe(first())
      .subscribe((id?: string) => {
        this.store.dispatch(new sourceActions.Find());
        if (!!id) {
          if (!this.source) {
            this.source = {};
          }
          this.source.uuid = id;
        }
        this.showSyncingAlert();
        this.startSync$.next();
      });
  }

  syncReport() {
    if (this.syncReportRef && this.syncReportRef.componentInstance) {
      return;
    }
    this.syncReportRef = this.dialogService.open(
      PipelineTemplateSyncReportComponent,
      {
        size: DialogSize.Large,
        data: {
          status$: new BehaviorSubject(this.status),
        },
      },
    );
    this.syncReportRef.afterClosed().subscribe(() => {
      this.syncReportRef = null;
    });
  }

  man() {
    this.dialogService.open(PipelineCustomTemplateManComponent, {
      size: DialogSize.Large,
    });
  }

  refresh(): Promise<any> {
    return this.source && this.source.uuid
      ? this.jenkins.templates.syncInfo(this.source.uuid)
      : Promise.resolve(null);
  }

  private showSyncingAlert() {
    this.auiNotificationService.info({
      title: this.translate.get('jenkins_template_is_syncing'),
    });
  }

  private getSyncCounter(status: any) {
    const re = {
      success: 0,
      fail: 0,
      ban: 0,
    };
    const items: any[] = get(status, 'last_job.report.items', []);
    items.forEach(item => {
      if (item.status === 'SUCCESS') {
        re.success++;
      } else if (item.status === 'FAIL') {
        re.fail++;
      }
      if (item.action === 'SKIP') {
        re.ban++;
      }
    });
    re.success = re.success - re.ban;
    return re;
  }
}
