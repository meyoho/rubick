import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from 'app/shared/shared.module';
import { PipelineCustomTemplateManComponent } from './components/pipeline-custom-template-man';
import { PipelineTemplateSyncReportComponent } from './components/pipeline-template-sync-report';
import { PipelineTemplateSyncSettingsComponent } from './components/pipeline-template-sync-settings';
import { PipelineTemplateListContainerComponent } from './containers/pipeline-template-list/pipeline-template-list-container.component';
import { JenkinsSharedModule } from 'app2/features/lazy/jenkins.shared.module';

import { TemplateEffects } from './effects/template';
import { TemplateSourcesEffects } from './effects/template_sources';
import { JenkinsRoutingModule } from './jenkins.routing.module';
import { reducers } from './reducers';

const components = [
  PipelineTemplateListContainerComponent,
  PipelineTemplateSyncSettingsComponent,
  PipelineTemplateSyncReportComponent,
  PipelineCustomTemplateManComponent,
];

const entryComponents = [
  PipelineTemplateSyncSettingsComponent,
  PipelineTemplateSyncReportComponent,
  PipelineCustomTemplateManComponent,
];

@NgModule({
  imports: [
    SharedModule,
    JenkinsSharedModule,
    JenkinsRoutingModule,
    StoreModule.forFeature('jenkinsTemplate', reducers),
    EffectsModule.forFeature([TemplateEffects, TemplateSourcesEffects]),
  ],
  exports: components,
  declarations: components,
  providers: [],
  entryComponents: entryComponents,
})
export class JenkinsModule {}
