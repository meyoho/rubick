import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PipelineTemplateListContainerComponent } from './containers/pipeline-template-list/pipeline-template-list-container.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'templates',
    pathMatch: 'full',
  },
  {
    path: 'templates',
    component: PipelineTemplateListContainerComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JenkinsRoutingModule {}
