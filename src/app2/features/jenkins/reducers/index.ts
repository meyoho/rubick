import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from 'app/store';

import * as fromTemplateList from './template-list';
import * as fromTemplateSourcesList from './template-sources';

export interface JenkinsTemplateState {
  templateList: fromTemplateList.State;
  templateSourcesList: fromTemplateSourcesList.State;
}

export interface State extends fromRoot.AppState {
  jenkinsTemplate: JenkinsTemplateState;
}

export const reducers = {
  templateList: fromTemplateList.reducer,
  templateSourcesList: fromTemplateSourcesList.reducer,
};

const selectJenkinsFeature = createFeatureSelector<JenkinsTemplateState>(
  'jenkinsTemplate',
);

export const selectTemplateList = createSelector(
  selectJenkinsFeature,
  (state: JenkinsTemplateState) => state.templateList,
);

export const selectTemplateSourcesList = createSelector(
  selectJenkinsFeature,
  (state: JenkinsTemplateState) => state.templateSourcesList,
);

// template list
export const selectTemplateListItems = createSelector(
  selectTemplateList,
  fromTemplateList.selectItems,
);

// template sources
export const selectTemplateSourcesListItems = createSelector(
  selectTemplateSourcesList,
  fromTemplateSourcesList.selectItems,
);
