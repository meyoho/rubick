import * as accountActions from 'app/store/actions/account.actions';
import * as actions from '../actions/template_sources';

export interface State {
  with_public: boolean;
  items: any[];
}

export const initialState: State = {
  with_public: true,
  items: [],
};

export function reducer(
  state = initialState,
  action: actions.All | accountActions.SetAccount,
) {
  if (action.type === accountActions.SET_ACCOUNT) {
    return initialState;
  }
  switch (action.type) {
    case actions.types.Get:
      return items(state, action);
    case actions.types.Find:
      return find(state, action);
    case actions.types.FindComplete:
      return FindComplete(state, action);
    default:
      return state;
  }
}

export const selectItems = (state: State) => state.items;

function items(_state: State, _action: actions.Get) {}

function find(state: State, _action: actions.Find) {
  return {
    ...state,
  };
}

function FindComplete(state: State, action: actions.FindComplete) {
  return {
    ...state,
    items: action.items,
  };
}
