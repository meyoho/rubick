export type ReturnTypes<EnumTypes> = { [key in keyof EnumTypes]: key };

export function createTypes<EnumTypes>(
  prefix: string,
  enumTypes: EnumTypes,
): ReturnTypes<EnumTypes> {
  return new Proxy(enumTypes as any, {
    get(_target, property: any) {
      return prefix + '/' + property;
    },
  });
}
