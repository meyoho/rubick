import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';

import { PipelineGlobalVarHelpComponent } from 'app2/features/jenkins/components/pipeline-global-var-help';

import {
  PipelineHistoryDiagramComponent,
  PipelineHistoryDiagramLinkComponent,
  PipelineHistoryDiagramNodeComponent,
} from 'app2/features/jenkins/components/pipeline-history-diagram';

import { PipelineTemplateInfoComponent } from 'app2/features/jenkins/components/pipeline-template-info';

import { IntegrationService } from 'app/services/api/integration.service';
import { SonarQubeService } from 'app/services/api/integration.sonar-qube.service';
import { PrivateBuildCodeClientService } from 'app/services/api/private-build-code-client.service';
import { RegionUtilitiesService } from 'app/services/api/region.utilities.service';
import { PipelineTemplateCardComponent } from 'app2/features/jenkins/components/pipeline-template-card';
import { PipelineTemplateListComponent } from 'app2/features/jenkins/containers/pipeline-template-list/pipeline-template-list.component';
import { JenkinsMessageService } from 'app_user/features/jenkins/services/jenkins-message.service';

const components = [
  // 流水线diagram 模板列表card 执行历史tab
  PipelineHistoryDiagramComponent,
  PipelineHistoryDiagramLinkComponent,
  PipelineHistoryDiagramNodeComponent,

  PipelineTemplateInfoComponent,

  PipelineGlobalVarHelpComponent,

  PipelineTemplateListComponent, // 模板创建时选择
  PipelineTemplateCardComponent,
];
const entryComponents = [
  PipelineTemplateInfoComponent,
  PipelineGlobalVarHelpComponent,
  PipelineTemplateListComponent,
  PipelineTemplateCardComponent,
];

const providers = [
  JenkinsMessageService,
  IntegrationService,
  SonarQubeService,
  RegionUtilitiesService,
  PrivateBuildCodeClientService,
];
@NgModule({
  imports: [RouterModule, SharedModule],
  exports: components,
  declarations: components,
  providers: providers,
  entryComponents: entryComponents,
})
export class JenkinsSharedModule {}
