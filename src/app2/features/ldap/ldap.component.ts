import { Component, Inject, OnInit } from '@angular/core';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';

@Component({
  templateUrl: './ldap.component.html',
  styleUrls: ['./ldap.component.scss'],
})
export class LDAPComponent implements OnInit {
  orgName: string;

  constructor(@Inject(ACCOUNT) private account: Account) {}

  ngOnInit() {
    this.orgName = this.account.namespace;
  }
}
