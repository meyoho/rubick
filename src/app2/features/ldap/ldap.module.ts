import { NgModule } from '@angular/core';

import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { LDAPComponent } from 'app2/features/ldap/ldap.component';
import { LDAPRoutingModule } from 'app2/features/ldap/ldap.routing.module';
import { OrgLdapDialogComponent } from 'app2/features/ldap/org-ldap-dialog.component';
import { OrgLdapComponent } from 'app2/features/ldap/org-ldap.component';

@NgModule({
  imports: [LDAPRoutingModule, SharedModule, EventSharedModule],
  declarations: [LDAPComponent, OrgLdapComponent, OrgLdapDialogComponent],
  entryComponents: [OrgLdapDialogComponent],
})
export class LDAPModule {}
