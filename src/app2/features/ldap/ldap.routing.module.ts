import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { LDAPComponent } from 'app2/features/ldap/ldap.component';

const routes: Routes = [
  {
    path: '',
    component: LDAPComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LDAPRoutingModule {}
