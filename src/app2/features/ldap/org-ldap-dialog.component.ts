import { DIALOG_DATA } from '@alauda/ui';
import { Component, Inject } from '@angular/core';

@Component({
  templateUrl: './org-ldap-dialog.component.html',
  styleUrls: ['./org-ldap-dialog.component.scss'],
})
export class OrgLdapDialogComponent {
  helpLink: string;

  constructor(
    @Inject(DIALOG_DATA)
    private dialogData: {
      helpLink: string;
    },
  ) {
    this.helpLink = this.dialogData.helpLink;
  }
}
