import { DialogService } from '@alauda/ui';
import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import marked from 'marked';

import { OrgService } from 'app/services/api/org.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, Weblabs } from 'app/typings';
import { OrgLdapDialogComponent } from 'app2/features/ldap/org-ldap-dialog.component';
import { ORG_CONSTANTS } from 'app2/features/rbac/rbac-constant';
import { DESCRIPTION_CN, DESCRIPTION_ZH_CN } from './help-text.constant';

@Component({
  selector: 'rc-app-org-ldap',
  templateUrl: './org-ldap.component.html',
  styleUrls: ['./org-ldap.component.scss'],
})
export class OrgLdapComponent implements OnInit {
  HELP_TEXTS: string;
  AzureCountrys: Array<{
    name: string;
    value: 'CN' | 'US';
  }>;
  submitting: boolean;
  ldapConfigLoading: boolean;
  orgLdapPageMap: any;
  ldapConfig: any;
  ldapMode: string;
  helpLink: string;

  @Input()
  orgName: string;
  @ViewChild('ldapConfigForm')
  ldapConfigForm: NgForm;

  constructor(
    private translate: TranslateService,
    private orgService: OrgService,
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
    @Inject(ACCOUNT) public account: Account,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {}

  async ngOnInit() {
    this.HELP_TEXTS =
      this.translate.currentLang === 'zh_cn'
        ? DESCRIPTION_CN
        : DESCRIPTION_ZH_CN;
    this.AzureCountrys = [
      {
        name: this.translate.get('org_ldap_account_country_cn'),
        value: 'CN',
      },
      {
        name: this.translate.get('org_ldap_account_country_us'),
        value: 'US',
      },
    ];
    this.submitting = false;
    this.ldapConfigLoading = true;
    this.helpLink = marked(this.getHelpText());
    this.orgLdapPageMap = ORG_CONSTANTS.ORG_LDAP_PAGE_MAP;
    try {
      this.ldapConfig = Object.assign(
        {
          test: {},
          schema: {},
          config: {},
        },
        await this.orgService.getOrgLdap(),
      );
    } catch (e) {
      this.ldapConfig = {};
    }
    this.ldapConfigLoading = false;
    this.ldapMode =
      this.ldapConfig && !this.ldapConfig.empty
        ? ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.DISPLAY
        : ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.CREATE;
    if (this.ldapMode === ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.CREATE) {
      this.selectLdapType('OpenLDAP');
    }
  }

  selectLdapType(type = 'OpenLDAP') {
    if (!this.ldapConfig || this.ldapConfig.type !== type) {
      this.ldapConfig = ORG_CONSTANTS.ORG_LDAP_TYPE_MAP[type];
    }
  }

  editLdapConfig() {
    this.ldapMode = ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.UPDATE;
  }

  getHelpText() {
    return this.HELP_TEXTS;
  }

  getTaskHelpTextPreview() {
    const helpText = this.getHelpText();
    return helpText.replace(/\s+/g, ' ');
  }

  helpLinkClicked() {
    this.dialogService.open(OrgLdapDialogComponent, {
      data: {
        helpLink: this.helpLink,
      },
    });
  }

  save(ldapConfigForm: any) {
    ldapConfigForm.onSubmit();
    if (ldapConfigForm.invalid) {
      return;
    }
    this.submitting = true;
    if (this.ldapMode === ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.CREATE) {
      this.orgService
        .createOrgLdap({ config: this.ldapConfig, orgName: this.orgName })
        .then(async () => {
          await this.ngOnInit();
        })
        .catch(e => {
          this.errorsToastService.error(e);
        })
        .then(() => {
          this.submitting = false;
        });
    } else if (this.ldapMode === ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.UPDATE) {
      this.orgService
        .updateOrgLdap({ config: this.ldapConfig, orgName: this.orgName })
        .then(async () => {
          await this.ngOnInit();
        })
        .catch(e => {
          this.errorsToastService.error(e);
        })
        .then(() => {
          this.submitting = false;
        });
    }
  }

  cancel() {
    this.ldapMode = ORG_CONSTANTS.ORG_LDAP_PAGE_MAP.DISPLAY;
  }
}
