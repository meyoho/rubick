import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { DashboardComponent } from 'app/features-shared/log/dashboard/dashboard.component';

const logRoutes: Routes = [
  {
    path: '',
    component: DashboardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(logRoutes)],
  exports: [RouterModule],
  declarations: [],
})
export class LogRoutingModule {}
