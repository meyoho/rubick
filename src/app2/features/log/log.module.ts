import { NgModule } from '@angular/core';
import { LogSharedModule } from 'app/features-shared/log/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { LogRoutingModule } from './log-routing.module';

@NgModule({
  imports: [SharedModule, LogRoutingModule, LogSharedModule],
})
export class LogModule {}
