import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnInit,
  ViewChild,
} from '@angular/core';
import { DomSanitizer, SafeResourceUrl } from '@angular/platform-browser';

import { get } from 'lodash-es';

import { IntegrationCenterService } from 'app/services/api/integration-center.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';

@Component({
  templateUrl: './grafana-dashboard.component.html',
  styleUrls: ['./grafana-dashboard.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class GrafanaDashboardComponent implements OnInit {
  grafanaUrl: SafeResourceUrl;
  @ViewChild('iframe')
  iframe: ElementRef;
  loading = true;
  regionBadgeOption = {
    onChange: (region: Cluster) => {
      const grafana_url = get(region, 'features.customized.metric.grafana_url');
      if (grafana_url) {
        this.setCustomizedGrafanaUrl(grafana_url);
      } else {
        const type = get(region, 'features.metric.type');
        if (!type) {
          this.grafanaUrl = '';
          this.cdr.markForCheck();
        } else {
          const integrationUuid = get(
            region,
            'features.metric.integration_uuid',
          );
          this.setGrafanaUrl(type, integrationUuid);
        }
      }
      return true;
    },
  };

  constructor(
    private sanitizer: DomSanitizer,
    private cdr: ChangeDetectorRef,
    private errorsToastService: ErrorsToastService,
    private integrationCenterService: IntegrationCenterService,
    private regionService: RegionService,
  ) {}

  ngOnInit() {
    this.regionService
      .getCurrentRegion()
      .then(res => {
        const grafana_url = get(res, 'features.customized.metric.grafana_url');
        if (grafana_url) {
          this.setCustomizedGrafanaUrl(grafana_url);
          return;
        }
        const type = get(res, 'features.metric.type');
        if (!type) {
          this.grafanaUrl = '';
          this.loading = false;
          this.cdr.markForCheck();
          return;
        }
        const integrationUuid = get(res, 'features.metric.integration_uuid');
        this.setGrafanaUrl(type, integrationUuid);
      })
      .catch(error => {
        this.errorsToastService.error(error);
        this.loading = false;
        this.cdr.markForCheck();
      });
  }

  private setGrafanaUrl(type: string, integrationUuid: string) {
    this.integrationCenterService
      .getIntegrationById(integrationUuid)
      .then(metric_setting => {
        const grafanaUrl = get(metric_setting, 'fields.grafana_url');
        if (type === 'prometheus' && grafanaUrl) {
          this.grafanaUrl = this.sanitizer.bypassSecurityTrustResourceUrl(
            grafanaUrl,
          );
        } else {
          this.grafanaUrl = null;
        }
      })
      .catch(error => {
        this.errorsToastService.error(error);
        this.grafanaUrl = null;
      })
      .finally(() => {
        this.loading = false;
        this.cdr.markForCheck();
      });
  }

  private setCustomizedGrafanaUrl(grafanaUrl: string) {
    this.grafanaUrl = this.sanitizer.bypassSecurityTrustResourceUrl(grafanaUrl);
    this.loading = false;
    this.cdr.markForCheck();
  }
}
