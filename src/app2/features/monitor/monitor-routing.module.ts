import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { GrafanaDashboardComponent } from './grafana-dashboard/grafana-dashboard.component';

const moitorRoutes: Routes = [
  {
    path: '',
    component: GrafanaDashboardComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(moitorRoutes)],
  exports: [RouterModule],
})
export class MonitorRoutingModule {}
