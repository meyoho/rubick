import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { MonitorRoutingModule } from 'app2/features/monitor/monitor-routing.module';

import { GrafanaDashboardComponent } from './grafana-dashboard/grafana-dashboard.component';

@NgModule({
  imports: [SharedModule, MonitorRoutingModule],
  declarations: [GrafanaDashboardComponent],
})
export class MonitorModule {}
