import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { isArray } from 'lodash-es';
import { Subject } from 'rxjs';

import { Project, ProjectService } from 'app/services/api/project.service';

@Component({
  templateUrl: 'bind-project.component.html',
  styleUrls: ['bind-project.component.scss'],
})
export class BindProjectComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  @Output()
  close = new EventEmitter();
  projectType = 'all_project';
  projects: string[];
  initialized: boolean;
  projectList: {
    uuid: string;
    name: string;
  }[] = [];
  @ViewChild('Form')
  form: NgForm;

  constructor(
    private projectService: ProjectService,
    @Inject(DIALOG_DATA)
    private modalData: {
      domain: string;
      projects: string[];
    },
  ) {}

  ngOnInit() {
    if (this.modalData && isArray(this.modalData.projects)) {
      switch (this.modalData.projects[0]) {
        case undefined:
          this.projectType = 'no_project';
          break;
        case '*':
          this.projectType = 'all_project';
          break;
        default:
          this.projectType = 'custom_project';
          this.projects = this.modalData.projects;
      }
    }
    this.projectService
      .getProjects({
        page_size: 1000,
        verbose: false,
      })
      .then(({ results }) => {
        this.projectList = results.map((item: Project) => {
          return {
            uuid: item.uuid,
            name: item.name,
          };
        });
        this.initialized = true;
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  cancel() {
    this.close.next(null);
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    switch (this.projectType) {
      case 'all_project':
        this.projects = ['*'];
        break;
      case 'no_project':
        this.projects = [];
        break;
    }
    this.close.next(this.projects);
  }
}
