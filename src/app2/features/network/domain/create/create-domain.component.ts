import {
  Component,
  EventEmitter,
  OnDestroy,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { Subject } from 'rxjs';

import { Project, ProjectService } from 'app/services/api/project.service';
import { DOMAIN_NAME } from 'app/utils/patterns';

@Component({
  templateUrl: 'create-domain.component.html',
  styleUrls: ['create-domain.component.scss'],
})
export class CreateDomainComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  @Output()
  close = new EventEmitter();
  type = 'extensive_domain';
  projectType = 'all_project';
  domain: string;
  initialized: boolean;
  projects: string[];
  domainRegex = DOMAIN_NAME;
  projectList: {
    uuid: string;
    name: string;
  }[] = [];
  @ViewChild('Form')
  form: NgForm;

  constructor(private projectService: ProjectService) {}

  ngOnInit() {
    this.projectService
      .getProjects({
        page_size: 1000,
        verbose: false,
      })
      .then(({ results }) => {
        this.projectList = results.map((item: Project) => {
          return {
            uuid: item.uuid,
            name: item.name,
          };
        });
        this.initialized = true;
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  cancel() {
    this.close.next(null);
  }

  confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    if (this.type === 'extensive_domain') {
      this.domain = `*.${this.domain}`;
    }
    switch (this.projectType) {
      case 'all_project':
        this.projects = ['*'];
        break;
      case 'no_project':
        this.projects = [];
        break;
    }
    this.close.next({
      domain: this.domain,
      projects: this.projects,
    });
  }
}
