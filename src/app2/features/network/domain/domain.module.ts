import { NgModule } from '@angular/core';

import { SharedModule } from 'app/shared/shared.module';
import { BindProjectComponent } from 'app2/features/network/domain/bind-project/bind-project.component';
import { CreateDomainComponent } from 'app2/features/network/domain/create/create-domain.component';
import { DomainRoutingModule } from 'app2/features/network/domain/domain-routing.module';
import { DomainListComponent } from 'app2/features/network/domain/list/domain-list.component';

@NgModule({
  imports: [SharedModule, DomainRoutingModule],
  declarations: [
    DomainListComponent,
    CreateDomainComponent,
    BindProjectComponent,
  ],
  entryComponents: [CreateDomainComponent, BindProjectComponent],
})
export class DomainModule {}
