import {
  DialogRef,
  DialogService,
  DialogSize,
  NotificationService,
} from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, Subject } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { Domain, DomainService } from 'app/services/api/domain.service';
import { Project, ProjectService } from 'app/services/api/project.service';
import { RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { BindProjectComponent } from 'app2/features/network/domain/bind-project/bind-project.component';
import { CreateDomainComponent } from 'app2/features/network/domain/create/create-domain.component';
import { PageParams, ResourceList } from 'app_user/core/types';

@Component({
  templateUrl: 'domain-list.component.html',
  styleUrls: ['domain-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DomainListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  private projectMap: {
    [key: string]: string;
  } = {};
  createEnabled: boolean;
  initialized: boolean;
  columns: string[];
  keyword: string;

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private errorsToastService: ErrorsToastService,
    private notificationService: NotificationService,
    private dialogService: DialogService,
    private domainService: DomainService,
    private translateService: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private projectService: ProjectService,
    private regionService: RegionService,
  ) {
    super(http, router, activedRoute, cdr);
  }

  ngOnInit() {
    super.ngOnInit();
    this.columns = ['domain', 'type', 'project', 'action'];
    this.projectService
      .getProjects({
        page_size: 1000,
        verbose: false,
      })
      .then(({ results }) => {
        results.map((item: Project) => {
          this.projectMap[item.uuid] = item.name;
        });
      });
    this.regionService.region$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(region => !!region),
      )
      .subscribe(region => {
        this.roleUtil
          .resourceTypeSupportPermissions(
            'domain',
            { cluster_name: region.name },
            'create',
          )
          .then(res => (this.createEnabled = res));
      });
    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.domainService.getDomains(true, {
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
        search: params.search,
      }),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: Domain) {
    return item.domain;
  }

  async createDomain() {
    let modalRef: DialogRef<CreateDomainComponent>;
    try {
      modalRef = this.dialogService.open(CreateDomainComponent, {
        size: DialogSize.Medium,
      });
    } catch (e) {
      return this.errorsToastService.error(e);
    }
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe(async (res: Domain) => {
        modalRef.close();
        if (!res) {
          return;
        }
        try {
          await this.domainService.createDomain(res);
        } catch (err) {
          return this.errorsToastService.error(err);
        }
        this.notificationService.success({
          title: this.translateService.get('success'),
          content: this.translateService.get('create_domain_success', {
            name: res.domain || '',
          }),
        });
        this.onUpdate(null);
      });
  }

  bindProject(domain: Domain) {
    let modalRef: DialogRef<BindProjectComponent>;
    try {
      modalRef = this.dialogService.open(BindProjectComponent, {
        size: DialogSize.Medium,
        data: {
          domain: domain.domain,
          projects: domain.projects,
        },
      });
    } catch (e) {
      return this.errorsToastService.error(e);
    }
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe(async (res: string[]) => {
        modalRef.close();
        if (!res) {
          return;
        }
        try {
          await this.domainService.updateDomainBindProject(
            domain.domain_id,
            res,
          );
        } catch (err) {
          return this.errorsToastService.error(err);
        }

        this.notificationService.success({
          title: this.translateService.get('success'),
          content: this.translateService.get('bind_project_success', {
            name: domain.domain || '',
          }),
        });
        this.onUpdate(null);
      });
  }

  pageChanged(pageEvent: PageParams) {
    this.onPageEvent({
      pageIndex: ++pageEvent.pageIndex,
      pageSize: pageEvent.pageSize,
    });
  }

  canShowUpdate(item: Domain) {
    return this.roleUtil.resourceHasPermission(item, 'domain', 'update');
  }

  canShowDelete(item: Domain) {
    return this.roleUtil.resourceHasPermission(item, 'domain', 'delete');
  }

  getDomainType(item: Domain) {
    return item.domain.startsWith('*.') ? 'extensive_domain' : 'domain';
  }

  getDomainProjectName(item: Domain) {
    switch (item.projects[0]) {
      case '*':
        return this.translateService.get('all_project');
        break;
      case undefined:
        return this.translateService.get('no_project');
        break;
      default:
        return this.projectMap[item.projects[0]] || item.projects[0];
    }
  }

  async delete(domain: Domain) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('delete_domain_confirm', {
          name: domain.domain,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      await this.domainService.deleteDomain(domain.domain_id);
    } catch (err) {
      return this.errorsToastService.error(err);
    }

    this.notificationService.success({
      title: this.translateService.get('success'),
      content: this.translateService.get('delete_domain_success', {
        name: domain.domain || '',
      }),
    });
    this.onUpdate(null);
  }
}
