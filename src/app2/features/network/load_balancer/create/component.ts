import { DIALOG_DATA, DialogService } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { NetworkService } from 'app/services/api/network.service';
import {
  Label,
  NodeLabels,
  RegionService,
} from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { LoadBalancerFormModel } from 'app/typings';
import {
  IP_ADDRESS_PATTERN,
  K8S_RESOURCE_NAME_BASE,
  POSITIVE_INT_PATTERN,
} from 'app/utils/patterns';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerCreateComponent implements OnInit {
  @Output()
  confirmed: EventEmitter<boolean> = new EventEmitter();

  cluster: string;
  labels: Label[] = [];
  nodeSelector: Label[] = [];
  matchNodes: string[] = [];
  nodeLabels: NodeLabels;
  confirming: boolean;
  loading: boolean;
  alb: LoadBalancerFormModel;
  type = 'standalone';
  minReplicas = 1;
  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;
  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;
  IP_ADDRESS_PATTERN = IP_ADDRESS_PATTERN;

  constructor(
    @Inject(DIALOG_DATA)
    private modalData: {
      cluster: string;
    },
    private regionService: RegionService,
    private networkService: NetworkService,
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
    private translateService: TranslateService,
  ) {
    this.cluster = this.modalData.cluster;
  }

  async ngOnInit() {
    this.loading = true;
    this.alb = {
      loadbalancerName: '',
      address: '',
      replicas: 1,
      nodeSelector: {},
    };
    let allLabels: Label[] = [];
    this.nodeLabels = await this.regionService.getRegionLabelsV2(this.cluster);
    Object.entries(this.nodeLabels).forEach(([, labels]: [string, Label[]]) => {
      allLabels = allLabels.concat(labels);
    });
    this.labels = allLabels.filter((label, index, self) => {
      return (
        index ===
        self.findIndex(t => t.key === label.key && t.value === label.value)
      );
    });
    this.loading = false;
  }

  typeChange(type: string) {
    if (type === 'standalone') {
      this.alb.replicas = 1;
      this.minReplicas = 1;
      return;
    }
    this.alb.replicas = 3;
    this.minReplicas = 2;
  }

  nodeSelectorChange(nodeSelector: Label[]) {
    this.matchNodes = this.regionService.checkNodeByLabels(
      this.nodeLabels,
      nodeSelector,
    );
  }

  disabledNodeLabel(label: Label) {
    return (
      this.nodeSelector.findIndex(
        t => t.key === label.key && t.value !== label.value,
      ) >= 0
    );
  }

  async submitForm(formRef: NgForm) {
    if (formRef.form.invalid) {
      return;
    }
    this.confirming = true;
    const selector = {};
    this.nodeSelector.map(item => {
      selector[item.key] = item.value;
    });
    this.alb.nodeSelector = selector;
    if (Number(this.matchNodes.length) !== Number(this.alb.replicas)) {
      // 点击取消返回上一层，不是直接关创建的弹窗
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('not_match_replicas_dialog_tip'),
          confirmText: this.translateService.get('continue_to_create'),
          cancelText: this.translateService.get('cancel'),
        });
      } catch (e) {
        this.confirming = false;
        return;
      }
    }
    try {
      await this.networkService.createLoadbalancer(
        this.cluster,
        'alauda-system',
        this.alb,
      );
      this.confirmed.next(true);
    } catch (err) {
      this.confirmed.next(false);
      this.errorsToastService.error(err);
    } finally {
      this.confirming = false;
    }
  }
}
