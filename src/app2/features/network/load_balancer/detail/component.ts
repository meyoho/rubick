import { DialogService, DialogSize, PageEvent } from '@alauda/ui';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { sortBy } from 'lodash-es';
import { combineLatest, Subject, Subscription } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import { PaginationDataWrapper } from 'app/abstract/pagination-data';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import {
  ALB_RESOURCE_TYPE,
  NetworkService,
} from 'app/services/api/network.service';
import { RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  Frontend,
  K8sResourceWithActions,
  LoadBalancer,
  Rule,
} from 'app/typings';
import { LoadBalancerUpdateDomainSuffixComponent } from 'app2/features/network/load_balancer/form/update-domain-suffix/component';
import { LoadBalancerPortUpdateServiceComponent } from 'app2/features/network/load_balancer/form/update-service/component';
import { ForceDeleteComponent } from 'app2/shared/components/force-delete/component';
import { ResourceList } from 'app_user/core/types';

interface FrontendAndRule {
  frontend: Frontend;
  rules: Rule[];
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerDetailComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  loadBalancerDetail: LoadBalancer;
  loadBalancer: K8sResourceWithActions<LoadBalancer>;
  paginationDataWrapper: PaginationDataWrapper<any>;
  paginationDataSubscription: Subscription;
  loadBalancerDataSubscription: Subscription;

  tableSelectIndex = 1;
  pageSize = 100;
  pageIndex = 0;
  count = 0;
  frontends: FrontendAndRule[] = [];
  showMore = false;
  basicLoading = false;

  namespace: string;
  cluster: string;
  albName: string;
  columns = ['port', 'protocol', 'rule', 'default_kube_service', 'action'];

  constructor(
    private networkService: NetworkService,
    private route: ActivatedRoute,
    private errorsToastService: ErrorsToastService,
    private router: Router,
    private regionService: RegionService,
    private translateService: TranslateService,
    private k8sResourceService: K8sResourceService,
    private dialogService: DialogService,
  ) {}

  ngOnInit() {
    const fetchFrontends = (pageNo: number, pageSize: number, params: any) => {
      return (this.refetchFrontends(pageNo, pageSize, params) as Promise<
        any
      >).then(res => {
        return {
          ...res,
          results: res.data,
        };
      });
    };

    this.paginationDataWrapper = new PaginationDataWrapper(
      fetchFrontends,
      this.pageSize,
    );

    combineLatest(this.route.paramMap, this.regionService.region$)
      .pipe(
        takeUntil(this.onDestroy$),
        filter(([params, region]) => !!(params && region)),
      )
      .subscribe(([params, region]) => {
        this.basicLoading = true;
        this.namespace = params.get('namespace');
        this.albName = params.get('name');
        this.cluster = region.name;
        this.refetchLB();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  searchChanged(queryString: string) {
    this.paginationDataWrapper.setParams({
      name: queryString,
    });
  }

  pageChanged(pageEvent: Partial<PageEvent>) {
    const { pageIndex, pageSize, length } = pageEvent;
    this.pageIndex = pageSize < length ? pageIndex : 0;
    this.pageSize = pageSize;
    this.paginationDataWrapper.refetch(this.pageIndex + 1, {}, pageSize);
  }

  async deleteFrontend(row: FrontendAndRule) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get(
          'load_balancer_delete_frontend_confirm',
          { port: row.frontend.metadata.name },
        ),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      await this.k8sResourceService.deleteK8sReource(
        this.cluster,
        ALB_RESOURCE_TYPE.FRONTEND,
        row.frontend,
      );
      this.refetchFrontends();
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  addPort() {
    this.router.navigate([
      '/console/admin/load_balancer/detail',
      this.loadBalancerDetail.metadata.namespace,
      this.loadBalancerDetail.metadata.name,
      'add-frontend',
    ]);
  }

  updateDefaultService(row: FrontendAndRule) {
    const modalRef = this.dialogService.open(
      LoadBalancerPortUpdateServiceComponent,
      {
        size: DialogSize.Big,
        data: {
          cluster: this.cluster,
          frontendDetail: row.frontend,
        },
      },
    );

    modalRef.componentInstance.confirmed.subscribe((service: boolean) => {
      if (service) {
        this.refetchFrontends();
      }
      modalRef.close();
    });
  }

  updateDomainSuffix() {
    const updateDomainSuffixModelRef = this.dialogService.open(
      LoadBalancerUpdateDomainSuffixComponent,
      {
        data: {
          cluster: this.cluster,
          loadbalancer: this.loadBalancerDetail,
        },
      },
    );
    updateDomainSuffixModelRef.componentInstance.finished
      .pipe(first())
      .subscribe(res => {
        updateDomainSuffixModelRef.close();
        if (res) {
          this.refetchLB();
        }
      });
  }

  async deleteLB() {
    try {
      const modelRef = await this.dialogService.open(ForceDeleteComponent, {
        data: {
          name: this.loadBalancerDetail.metadata.name,
          title: this.translateService.get('delete_load_balancer'),
          content: this.translateService.get('delete_load_balancer_content', {
            name: this.loadBalancerDetail.metadata.name,
          }),
        },
      });
      modelRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: Boolean) => {
          modelRef.close();
          if (res) {
            await this.networkService.deleteLoadbalancer(
              this.cluster,
              'alauda-system',
              this.loadBalancerDetail.metadata.name,
            );
            this.router.navigate(['/console/admin/load_balancer']);
          }
        });
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  buttonDisplay(action: string) {
    return this.networkService.canAction(
      this.loadBalancer,
      action,
      'k8s_others',
    );
  }

  get empty() {
    return !this.frontends.length;
  }

  get loadError() {
    return this.paginationDataWrapper.loadError;
  }

  get loading() {
    return this.paginationDataWrapper.loading;
  }

  get searching() {
    // TODO
    return false;
    // return this.paginationDataWrapper.searching;
  }

  refetchFrontends(pageNo?: number, pageSize?: number, params?: any): any {
    this.k8sResourceService
      .getK8sResourcesPaged(ALB_RESOURCE_TYPE.FRONTEND, {
        clusterName: this.cluster,
        namespace: this.namespace,
        page: pageNo || 1,
        page_size: pageSize || 100,
        name: params ? params.name : '',
        labelSelector: `alb2.alauda.io/name=${this.albName}`,
      })
      .then((res: ResourceList) => {
        const promiseList: any[] = [];
        this.count = res.count;
        res.results.map((item: K8sResourceWithActions<Frontend>) => {
          promiseList.push(this.getRules(item.kubernetes));
        });
        return Promise.all(promiseList);
      })
      .then((frontends: FrontendAndRule[]) => {
        //sorted by port
        this.frontends = sortBy(frontends, frontend => {
          //port set 0 when '',so always top
          return frontend.frontend.spec.port || 0;
        });
      })
      .catch(() => {});
    this.pageSize = pageSize || 100;
    this.pageIndex = this.pageIndex || 0;
  }

  getRules(frontend: Frontend) {
    return this.k8sResourceService
      .getK8sResources(ALB_RESOURCE_TYPE.RULE, {
        clusterName: this.cluster,
        namespace: this.namespace,
        labelSelector: `alb2.alauda.io/name=${
          this.albName
        },alb2.alauda.io/frontend=${frontend.metadata.name}`,
      })
      .then((ruleList: K8sResourceWithActions<Rule>[]) => {
        const rules = ruleList.map((item: K8sResourceWithActions<Rule>) => {
          return item.kubernetes;
        });
        return {
          frontend,
          rules,
        };
      })
      .catch(() => {
        return {
          frontend,
          rules: [],
        };
      });
  }

  refetchLB(): any {
    this.basicLoading = true;
    this.k8sResourceService
      .getK8sReourceDetail(ALB_RESOURCE_TYPE.ALB, {
        clusterName: this.cluster,
        name: this.albName,
        namespace: this.namespace,
      })
      .then((detail: K8sResourceWithActions<LoadBalancer>) => {
        this.loadBalancerDetail = detail.kubernetes;
        this.loadBalancer = detail;
      })
      .catch(() => {})
      .then(() => {
        this.basicLoading = false;
      });
    this.refetchFrontends();
  }

  toggleShowMore() {
    this.showMore = !this.showMore;
  }

  trackByFn(_index: number, item: FrontendAndRule) {
    return item.frontend.metadata.uid;
  }
}
