import { Component, EventEmitter, Input, Output } from '@angular/core';

import { LoadBalancer } from 'app/typings';

@Component({
  selector: 'rc-load-balancer-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerDetailInfoComponent {
  @Input()
  data: LoadBalancer;
  @Input()
  cluster: string;
  @Input()
  canUpdate: boolean;
  @Input()
  canDelete: boolean;
  @Output()
  onUpdateDomainSuffix = new EventEmitter<void>();
  @Output()
  onDelete = new EventEmitter<void>();
  constructor() {}
}
