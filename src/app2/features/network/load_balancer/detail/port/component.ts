import { DialogService, DialogSize, PageEvent } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import {
  ALB_RESOURCE_TYPE,
  NetworkService,
} from 'app/services/api/network.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  Frontend,
  K8sResourceWithActions,
  LoadBalancer,
  Rule,
} from 'app/typings';
import { LoadBalancerPortAddServiceComponent } from 'app2/features/network/load_balancer/form/add-service/component';
import { LoadBalancerUpdateDefaultCertificateComponent } from 'app2/features/network/load_balancer/form/update-default-certificate/component';
import { LoadBalancerPortUpdateServiceComponent } from 'app2/features/network/load_balancer/form/update-service/component';
import {
  getPartialIndicators,
  parseDSL,
} from 'app2/features/network/load_balancer/util';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerPortComponent implements OnInit {
  region: Cluster;
  loadBalancer: K8sResourceWithActions<
    LoadBalancer
  > = {} as K8sResourceWithActions<LoadBalancer>;
  frontendDetail: Frontend;

  loading = true;
  searching = true;
  search = '';
  count = 0;
  page = 0;
  pageSize = 20;
  rules: Rule[] = [];
  cluster: string;
  namespace: string;
  albName: string;
  frontendName: string;
  columns = ['description', 'rule', 'kube_service_group', 'action'];

  constructor(
    private networkService: NetworkService,
    private route: ActivatedRoute,
    private translate: TranslateService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
    private regionService: RegionService,
    private k8sResourceService: K8sResourceService,
    private dialogService: DialogService,
  ) {}

  async fetchFrontendDetail() {
    this.loading = true;

    try {
      const res = await this.k8sResourceService.getK8sReourceDetail(
        ALB_RESOURCE_TYPE.FRONTEND,
        {
          clusterName: this.cluster,
          name: this.frontendName,
          namespace: this.namespace,
        },
      );
      this.frontendDetail = res.kubernetes;
    } catch (e) {
      return this.router.navigate([
        '/console/admin/load_balancer/detail',
        this.namespace,
        this.albName,
      ]);
    } finally {
      this.loading = false;
    }
  }

  async getRules() {
    this.searching = true;
    try {
      const res = await this.k8sResourceService.getK8sResourcesPaged(
        ALB_RESOURCE_TYPE.RULE,
        {
          clusterName: this.cluster,
          namespace: this.namespace,
          page: this.page + 1,
          page_size: this.pageSize,
          name: this.search,
          labelSelector: `alb2.alauda.io/name=${
            this.albName
          },alb2.alauda.io/frontend=${this.frontendName}`,
        },
      );
      this.count = res.count;
      this.rules = res.results.map((item: K8sResourceWithActions<Rule>) => {
        return item.kubernetes;
      });
    } catch (e) {
      this.errorsToastService.error(e);
    } finally {
      this.searching = false;
    }
  }

  fetchData() {
    return Promise.all([this.fetchFrontendDetail(), this.getRules()]);
  }

  async ngOnInit() {
    this.route.paramMap.subscribe(async map => {
      this.albName = map.get('name');
      this.namespace = map.get('namespace');
      this.frontendName = map.get('port');

      const region = await this.regionService.getCurrentRegion();
      this.cluster = region.name;

      try {
        this.loadBalancer = (await this.k8sResourceService.getK8sReourceDetail(
          ALB_RESOURCE_TYPE.ALB,
          {
            clusterName: this.cluster,
            name: this.albName,
            namespace: this.namespace,
          },
        )) as K8sResourceWithActions<LoadBalancer>;
      } catch (e) {
        return this.router.navigate(['/console/admin/load_balancer']);
      }
      await this.fetchData();
    });
  }

  canUpdate() {
    return this.networkService.canAction(
      this.loadBalancer,
      'update',
      'k8s_others',
    );
  }

  addRule() {
    this.router.navigate([
      '/console/admin/load_balancer/detail',
      this.namespace,
      this.albName,
      'port',
      this.frontendName,
      'add-rule',
    ]);
  }

  parseDSL(rawDSL: string) {
    return parseDSL(rawDSL);
  }

  getPartiaIndicators(rawDSL: string) {
    return getPartialIndicators(rawDSL);
  }

  pageChanged(pageEvent: Partial<PageEvent>) {
    const { pageIndex, pageSize, length } = pageEvent;
    this.page = pageSize < length ? pageIndex : 0;
    this.pageSize = pageSize;
    this.getRules();
  }

  updateService(rule: Rule) {
    const modalRef = this.dialogService.open(
      LoadBalancerPortAddServiceComponent,
      {
        size: DialogSize.Big,
        data: {
          title: this.translate.get('add_k8s_service'),
          lbName: this.albName,
          protocol: this.frontendDetail.spec.protocol,
          port: this.frontendName,
          rule,
          cluster: this.cluster,
        },
      },
    );

    modalRef.componentInstance.confirmed.subscribe(
      async (isConfirm: boolean) => {
        modalRef.close(isConfirm);
        if (isConfirm) {
          await this.fetchData();
        }
      },
    );
  }

  updateRule(rule: Rule) {
    this.router.navigate([
      '/console/admin/load_balancer/detail',
      this.namespace,
      this.albName,
      'port',
      this.frontendName,
      'rule',
      rule.metadata.name,
      'update',
    ]);
  }

  updateDefaultService() {
    const modalRef = this.dialogService.open(
      LoadBalancerPortUpdateServiceComponent,
      {
        size: DialogSize.Big,
        data: {
          cluster: this.cluster,
          frontendDetail: this.frontendDetail,
        },
      },
    );

    modalRef.componentInstance.confirmed.subscribe((service: boolean) => {
      if (service) {
        this.fetchFrontendDetail();
      }
      modalRef.close();
    });
  }

  updateDefaultCertificate() {
    const modalRef = this.dialogService.open(
      LoadBalancerUpdateDefaultCertificateComponent,
      {
        data: {
          cluster: this.cluster,
          frontendDetail: this.frontendDetail,
        },
      },
    );

    modalRef.componentInstance.confirmed.subscribe((service: boolean) => {
      if (service) {
        this.fetchFrontendDetail();
      }
      modalRef.close();
    });
  }

  async deleteFrontendDetail() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_port'),
        content: this.translate.get('confirm_delete_port'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    try {
      await this.k8sResourceService.deleteK8sReource(
        this.cluster,
        ALB_RESOURCE_TYPE.FRONTEND,
        this.frontendDetail,
      );
      this.router.navigate([
        '/console/admin/load_balancer/detail',
        this.namespace,
        this.albName,
      ]);
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  async deleteRule(rule: Rule) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_rule'),
        content: this.translate.get('confirm_delete_rule'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    try {
      await this.k8sResourceService.deleteK8sReource(
        this.cluster,
        ALB_RESOURCE_TYPE.RULE,
        rule,
      );
      await this.fetchData();
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  trackByFn(_index: number) {
    return _index;
  }
}
