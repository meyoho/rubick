import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { cloneDeep } from 'lodash-es';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ALB_RESOURCE_TYPE } from 'app/services/api/network.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import {
  K8sResourceWithActions,
  Rule,
  RuleIndicator,
  RuleService,
  Service,
} from 'app/typings';
import {
  getPartialIndicators,
  parseDSL,
} from 'app2/features/network/load_balancer/util';

interface ModalData {
  title: string;
  lbName: string;
  protocol: string;
  port: number;
  rule: Rule;
  ruleList: Rule[];
  parsedRules: any[];
  type: string;
  cluster: string;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerPortAddServiceComponent implements OnInit {
  @Output()
  confirmed = new EventEmitter<boolean>();

  ruleServices: RuleService[];
  ruleIndicators: RuleIndicator[];
  type: string;
  selectedRuleIndex = -1;
  confirming: boolean;
  getPartiaIndicators = getPartialIndicators;

  cluster: string;
  namespaceList: string[] = [];
  serviceList: K8sResourceWithActions<Service>[] = [];
  rule: Rule;

  constructor(
    @Inject(DIALOG_DATA) public modalData: ModalData,
    private errorsToastService: ErrorsToastService,
    private k8sResourceService: K8sResourceService,
  ) {
    this.type = this.modalData.type || 'add';
    this.cluster = this.modalData.cluster;

    if (this.type === 'create') {
      return;
    }

    const { rule } = this.modalData;
    this.rule = rule;
    this.ruleServices = cloneDeep(rule.spec.serviceGroup.services);

    if (!this.ruleServices.length) {
      this.addService();
    }

    this.ruleIndicators = parseDSL(rule);
  }

  async ngOnInit() {
    const res_service = await this.k8sResourceService.getK8sResources(
      'services',
      {
        clusterName: this.cluster,
      },
    );
    res_service.map((item: K8sResourceWithActions<Service>) => {
      if (
        item.kubernetes.metadata.namespace &&
        !this.namespaceList.includes(item.kubernetes.metadata.namespace)
      ) {
        this.namespaceList.push(item.kubernetes.metadata.namespace);
      }
      return item.kubernetes.metadata.namespace;
    });
    this.serviceList = res_service;
  }

  parseDSL(rawDSL: string) {
    return parseDSL(rawDSL);
  }

  ruleSelectedChanged(va: boolean, index: number) {
    if (va) {
      this.selectedRuleIndex = index;
      this.ruleServices =
        cloneDeep(this.modalData.ruleList[index]).spec.serviceGroup.services ||
        [];
      this.rule = cloneDeep(this.modalData.ruleList[index]);
    } else if (this.selectedRuleIndex === index) {
      this.selectedRuleIndex = -1;
    }
  }

  showServiceGroup() {
    return !(this.type === 'create' && this.selectedRuleIndex < 0);
  }

  addService() {
    if (!this.showServiceGroup()) {
      return;
    }
    this.ruleServices.push({
      name: '',
      namespace: '',
      port: '',
      weight: 100,
    });
  }

  async submitForm(formRef: NgForm) {
    formRef.onSubmit(null);
    if (formRef.form.invalid || this.confirming) {
      return;
    }

    this.confirming = true;
    this.rule.spec.serviceGroup.services = this.ruleServices;
    delete this.rule.spec.source;
    try {
      await this.k8sResourceService.updateK8sReource(
        this.cluster,
        ALB_RESOURCE_TYPE.RULE,
        this.rule,
      );
      this.confirmed.next(true);
    } catch (e) {
      this.errorsToastService.error(e);
    } finally {
      this.confirming = false;
    }
  }
}
