import { Component, Injector, Input, OnInit } from '@angular/core';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { K8sResourceWithActions, Secret } from 'app/typings';

interface CertificateForm {
  namespace: string;
  certificate_name: string;
}
@Component({
  selector: 'rc-load-balancer-certificate-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
})
export class LoadBalancerCertificateFormComponent
  extends BaseResourceFormGroupComponent<String, CertificateForm>
  implements OnInit {
  @Input()
  secretList: K8sResourceWithActions<Secret>[];

  namespaceList: string[] = [];
  namespaceSecretMap: {
    [key: string]: string[];
  } = {};

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.secretList.map((item: K8sResourceWithActions<Secret>) => {
      const namespace = item.kubernetes.metadata.namespace;
      const name = item.kubernetes.metadata.name;
      if (this.namespaceSecretMap[namespace]) {
        this.namespaceSecretMap[namespace].push(name);
      } else {
        this.namespaceSecretMap[namespace] = [name];
      }
      if (!this.namespaceList.includes(namespace)) {
        this.namespaceList.push(namespace);
      }
    });
  }

  getDefaultFormModel() {
    return {
      namespace: '',
      certificate_name: '',
    };
  }

  createForm() {
    return this.fb.group({
      namespace: [''],
      certificate_name: [''],
    });
  }

  adaptResourceModel(resource: string): CertificateForm {
    if (resource) {
      const resSplit = resource.split('_');
      return {
        namespace: resSplit[0],
        certificate_name: resSplit[1],
      };
    }
  }

  adaptFormModel(formModel: CertificateForm): string {
    return formModel.namespace && formModel.certificate_name
      ? `${formModel.namespace}_${formModel.certificate_name}`
      : '';
  }
}
