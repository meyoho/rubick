import { Location } from '@angular/common';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { v4 } from 'uuid';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ALB_RESOURCE_TYPE, LB_TYPE } from 'app/services/api/network.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import {
  Frontend,
  K8sResourceWithActions,
  LoadBalancer,
  Rule,
  Service,
} from 'app/typings';

interface RuleModel {
  value: Rule;
}

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerEditRuleComponent implements OnInit {
  loadBalancer: LoadBalancer;
  cluster: string;
  albName: string;
  frontendName: string;
  ruleName: string;
  initialized: boolean;
  confirming: boolean;
  namespaceList: string[] = [];
  namespace: string;
  serviceList: K8sResourceWithActions<Service>[] = [];
  secretList: K8sResourceWithActions<Service>[] = [];
  ruleList: RuleModel[] = [];
  frontend: Frontend;
  @ViewChild('resourceForm')
  resourceForm: NgForm;

  constructor(
    private errorsToastService: ErrorsToastService,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private regionService: RegionService,
    private location: Location,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.paramMap;
    this.namespace = params.get('namespace');
    this.albName = params.get('name');
    this.frontendName = params.get('port');
    this.ruleName = params.get('rule');
    this.regionService
      .getCurrentRegion()
      .then((cluster: Cluster) => {
        this.cluster = cluster.name;
        const promiseList: Promise<any>[] = [
          this.k8sResourceService.getK8sResources('secrets', {
            clusterName: this.cluster,
          }),
          this.k8sResourceService.getK8sResources('services', {
            clusterName: this.cluster,
          }),
          this.k8sResourceService.getK8sReourceDetail(
            ALB_RESOURCE_TYPE.FRONTEND,
            {
              clusterName: this.cluster,
              name: this.frontendName,
              namespace: this.namespace,
            },
          ),
          this.k8sResourceService.getK8sReourceDetail(ALB_RESOURCE_TYPE.ALB, {
            clusterName: this.cluster,
            name: this.albName,
            namespace: this.namespace,
          }),
        ];
        if (this.ruleName) {
          promiseList.push(
            this.k8sResourceService.getK8sReourceDetail(
              ALB_RESOURCE_TYPE.RULE,
              {
                clusterName: this.cluster,
                name: this.ruleName,
                namespace: this.namespace,
              },
            ),
          );
        }
        return Promise.all(promiseList);
      })
      .then(([res_secret, res_service, frontend, alb, rule]) => {
        this.loadBalancer = alb.kubernetes;
        this.frontend = frontend.kubernetes;
        this.secretList = res_secret;
        if (this.ruleName) {
          this.addRule(rule.kubernetes);
        } else {
          this.addRule();
        }
        res_service.map((item: K8sResourceWithActions<Service>) => {
          if (
            item.kubernetes.metadata.namespace &&
            !this.namespaceList.includes(item.kubernetes.metadata.namespace)
          ) {
            this.namespaceList.push(item.kubernetes.metadata.namespace);
          }
          return item.kubernetes.metadata.namespace;
        });
        this.serviceList = res_service;
        this.initialized = true;
      })
      .catch(() => {
        this.router.navigate([
          '/console/admin/load_balancer/detail',
          this.namespace,
          this.albName,
          'port',
          this.frontendName,
        ]);
      });
    // this.regionService.region$.subscribe(cluster => {
    //   this.router.navigate(['/console/admin/load_balancer']);
    // });
  }

  canAddRule() {
    return (
      this.frontend.spec.protocol &&
      this.frontend.spec.protocol !== 'tcp' &&
      this.loadBalancer.spec.type !== LB_TYPE.ELB
    );
  }

  genDefaultRule(): Rule {
    return {
      apiVersion: 'crd.alauda.io/v1',
      kind: 'Rule',
      metadata: {
        name: v4(),
        namespace: this.loadBalancer.metadata.namespace,
        labels: {
          'alb2.alauda.io/name': this.loadBalancer.metadata.name,
          'alb2.alauda.io/frontend': '',
        },
      },
      spec: {
        priority: 10000,
        domain: '',
        url: '',
        dsl: '',
        description: '',
        serviceGroup: {
          session_affinity_policy: '',
          session_affinity_attribute: '',
          services: [
            {
              name: '',
              namespace: '',
              port: '',
              weight: 100,
            },
          ],
        },
      },
    };
  }

  addRule(rule?: Rule) {
    if (
      this.frontend.spec.protocol === 'tcp' ||
      this.loadBalancer.spec.type === LB_TYPE.ELB
    ) {
      return;
    }
    this.ruleList.push({ value: rule ? rule : this.genDefaultRule() });
    return false;
  }

  moveRule(from: number, to: number) {
    const { ruleList } = this;
    const temp = ruleList[to];
    ruleList[to] = ruleList[from];
    ruleList[from] = temp;
    return false;
  }

  async submitForm() {
    this.resourceForm.onSubmit(null);
    if (this.resourceForm.form.invalid || this.confirming) {
      return;
    }

    this.confirming = true;

    try {
      const { frontend } = this;
      await Promise.all(
        this.ruleList.map(_rule => {
          const rule = _rule.value;
          if (frontend && frontend.metadata.uid) {
            rule.metadata['ownerReferences'] = [
              {
                apiVersion: 'crd.alauda.io/v1',
                kind: ALB_RESOURCE_TYPE.FRONTEND,
                name: frontend.metadata.name,
                uid: frontend.metadata.uid,
              },
            ];
          }
          rule.spec.serviceGroup.services = rule.spec.serviceGroup.services.filter(
            service => {
              return service.namespace;
            },
          );
          return this.k8sResourceService[
            this.ruleName ? 'updateK8sReource' : 'createK8sReource'
          ](this.cluster, ALB_RESOURCE_TYPE.RULE, rule);
        }),
      );
      this.router.navigate([
        '/console/admin/load_balancer/detail',
        this.namespace,
        this.albName,
        'port',
        this.frontendName,
      ]);
    } catch (e) {
      return this.errorsToastService.handleGenericAjaxError({
        errors: e.errors,
        handleNonGenericCodes: true,
      });
    } finally {
      this.confirming = false;
    }
  }

  cancel() {
    this.location.back();
  }
}
