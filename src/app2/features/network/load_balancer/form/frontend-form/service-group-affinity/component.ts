import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { K8sResourceWithActions, Service, ServiceGroup } from 'app/typings';
@Component({
  selector: 'rc-load-balancer-service-group-with-affinity',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerServiceGroupWithAffinityComponent
  extends BaseResourceFormGroupComponent
  implements OnChanges {
  @Input()
  clusterName: string;
  @Input()
  namespaceList: string[];
  @Input()
  serviceList: K8sResourceWithActions<Service>[];
  @Input()
  protocol: string;

  sessionAffinityTypes = ['', 'sip-hash', 'cookie'];

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnChanges({ protocol }: SimpleChanges) {
    if (protocol && protocol.currentValue) {
      this.sessionAffinityTypes =
        protocol.currentValue.toLocaleUpperCase() === 'HTTP'
          ? ['', 'sip-hash', 'cookie']
          : ['', 'sip-hash'];
      if (this.form) {
        this.form.get('session_affinity_policy').setValue('');
        this.form.get('session_affinity_attribute').setValue('');
      }
    }
  }

  getDefaultFormModel(): ServiceGroup {
    return {
      session_affinity_policy: '',
      session_affinity_attribute: '',
      services: [],
    };
  }

  createForm() {
    return this.fb.group({
      session_affinity_attribute: [''],
      session_affinity_policy: [''],
      services: [''],
    });
  }
}
