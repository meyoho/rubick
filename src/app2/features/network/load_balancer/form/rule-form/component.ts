import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import {
  FormArray,
  FormControl,
  ValidatorFn,
  Validators,
} from '@angular/forms';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  LB_TYPE,
  MATCH_TYPE,
  RULE_TYPE,
} from 'app/services/api/network.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  K8sResourceWithActions,
  KubernetesResource,
  LoadBalancer,
  Rule,
  RuleIndicator,
  RuleSpec,
  Secret,
  Service,
} from 'app/typings';
import {
  parseDSL,
  stringifyDSL,
} from 'app2/features/network/load_balancer/util';

interface RuleSpecForm extends RuleSpec {
  ruleIndicators: RuleIndicator[];
}

interface RuleForm extends KubernetesResource {
  apiVersion?: string;
  kind?: string;
  spec: RuleSpecForm;
}

@Component({
  selector: 'rc-load-balancer-rule-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerRuleFormComponent extends BaseResourceFormGroupComponent {
  @Input()
  clusterName: string;
  @Input()
  namespaceList: string[];
  @Input()
  serviceList: K8sResourceWithActions<Service>[];
  @Input()
  secretList: K8sResourceWithActions<Secret>[];
  @Input()
  loadBalancer: LoadBalancer;
  @Input()
  frontendName: string;
  @Input()
  showCertificate = false;

  sessionAffinityTypes = ['', 'sip-hash', 'cookie'];
  urlRewrite = false;
  certificateEnabled = false;
  ruleTypes = [
    RULE_TYPE.HOST,
    RULE_TYPE.URL,
    RULE_TYPE.SRC_IP,
    RULE_TYPE.HEADER,
    RULE_TYPE.COOKIE,
    RULE_TYPE.PARAM,
  ];

  constructor(injector: Injector, private translateService: TranslateService) {
    super(injector);
  }

  getDefaultFormModel(): RuleForm {
    return {
      apiVersion: 'crd.alauda.io/v1',
      kind: 'Rule',
      metadata: {
        name: '',
        namespace: '',
        labels: {
          'alb2.alauda.io/name': '',
          'alb2.alauda.io/frontend': '',
        },
      },
      spec: {
        priority: 10000,
        domain: '',
        url: '',
        ruleIndicators: [{ values: [] } as RuleIndicator],
        dsl: '',
        description: '',
        certificate_name: '',
        serviceGroup: {
          session_affinity_policy: '',
          session_affinity_attribute: '',
          services: [
            {
              name: '',
              namespace: '',
              port: '',
              weight: 100,
            },
          ],
        },
      },
    };
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: [''],
      namespace: [''],
      labels: [''],
    });
    const specForm = this.fb.group({
      priority: [10000],
      domain: [''],
      url: [''],
      dsl: [''],
      ruleIndicators: this.fb.array([]),
      description: ['', Validators.required],
      certificate_name: [''],
      serviceGroup: this.fb.group({
        session_affinity_attribute: [''],
        session_affinity_policy: [''],
        services: [[]],
      }),
      rewrite_target: ['', Validators.required],
    });
    return this.fb.group({
      apiVersion: [''],
      kind: [''],
      metadata: metadataForm,
      spec: specForm,
    });
  }

  getRuleTypeDisplay(type: string) {
    return this.translateService.get('rule_' + type.toLocaleLowerCase());
  }

  get ruleIndicatorsForm(): FormArray {
    return this.form.get('spec.ruleIndicators') as FormArray;
  }

  removeRuleIndicator(index: number) {
    this.ruleIndicatorsForm.removeAt(index);
  }

  addRuleIndicator() {
    let addType = '';
    if (this.ruleIndicatorsForm.controls.length === this.ruleTypes.length) {
      return;
    }
    if (this.ruleIndicatorsForm.controls.length) {
      const backUpRules = this.ruleTypes.slice();
      this.ruleIndicatorsForm.controls.forEach(e => {
        const type = e.value.type;
        backUpRules.splice(backUpRules.indexOf(type), 1);
      });
      addType = backUpRules[0];
    } else {
      addType = this.ruleTypes[0];
    }
    const duplicateTypeValidator: ValidatorFn = (control: FormControl) => {
      const index = this.ruleIndicatorsForm.controls.indexOf(control);
      const previousTypes = this.ruleIndicatorsForm.controls
        .slice(0, index)
        .map(_control => _control.value.type)
        .filter(type => !!type);
      const { type } = control.value;
      if (previousTypes.includes(type)) {
        return {
          duplicateType: type,
        };
      } else {
        return null;
      }
    };
    this.ruleIndicatorsForm.push(
      this.fb.control(this.genDefaultFromType(addType), duplicateTypeValidator),
    );
  }

  genDefaultFromType(type: string): RuleIndicator {
    switch (type) {
      case RULE_TYPE.HOST: {
        return {
          type: RULE_TYPE.HOST,
          key: '',
          values: [[MATCH_TYPE.IN, '']],
        };
      }
      case RULE_TYPE.URL: {
        return {
          type: RULE_TYPE.URL,
          key: '',
          values: [[MATCH_TYPE.REGEX, '']],
        };
      }
      case RULE_TYPE.SRC_IP: {
        return {
          type: RULE_TYPE.SRC_IP,
          key: '',
          values: [[MATCH_TYPE.EQ, '']],
        };
      }
      case RULE_TYPE.HEADER: {
        return {
          type: RULE_TYPE.HEADER,
          key: '',
          values: [[MATCH_TYPE.EQ, '']],
        };
      }
      case RULE_TYPE.COOKIE: {
        return {
          type: RULE_TYPE.COOKIE,
          key: '',
          values: [[MATCH_TYPE.EQ, '']],
        };
      }
      case RULE_TYPE.PARAM: {
        return {
          type: RULE_TYPE.PARAM,
          key: '',
          values: [[MATCH_TYPE.EQ, '']],
        };
      }
      default:
        return null;
    }
  }

  urlRewriteChange(value: boolean) {
    if (value) {
      this.form.get('spec.rewrite_target').enable({
        emitEvent: false,
      });
    } else {
      this.form.get('spec.rewrite_target').disable({
        emitEvent: false,
      });
    }
  }

  certificateEnabledChange(value: boolean) {
    if (!value) {
      this.form.get('spec.certificate_name').setValue('');
    }
  }

  adaptResourceModel(resource: Rule): RuleForm {
    if (resource) {
      const spec: RuleSpecForm = {
        ...resource.spec,
        ruleIndicators: [],
      };
      this.urlRewrite = !!resource.spec.rewrite_target;
      this.certificateEnabled = !!resource.spec.certificate_name;
      this.urlRewriteChange(this.urlRewrite);
      spec.ruleIndicators = parseDSL(resource);
      return { ...resource, spec };
    }
  }

  adaptFormModel(formModel: RuleForm): Rule {
    formModel.metadata.labels['alb2.alauda.io/frontend'] = this.frontendName;
    const ruleSpec: RuleSpec = {
      priority: formModel.spec.priority,
      domain: '',
      url: '',
      dsl: '',
      description: formModel.spec.description,
      certificate_name: formModel.spec.certificate_name,
      serviceGroup: formModel.spec.serviceGroup,
      rewrite_target: formModel.spec.rewrite_target,
    };
    const ruleIndicators = formModel.spec.ruleIndicators;
    const spec = this.parseDSL(ruleIndicators, ruleSpec);
    return { ...formModel, spec };
  }

  private parseDSL(ruleIndicators: RuleIndicator[], spec: RuleSpec) {
    ruleIndicators.forEach(indicator => {
      const { type, values } = indicator;
      values.forEach(v => {
        const [matchType, value] = v;
        if (
          type === RULE_TYPE.URL &&
          matchType === MATCH_TYPE.STARTS_WITH &&
          !/^\//.test(value)
        ) {
          v[1] = '/' + value;
        }
      });
    });

    let domain = '',
      url = '';
    if (LB_TYPE[this.loadBalancer.spec.type]) {
      ruleIndicators.forEach(indicator => {
        const { type, values } = indicator;
        if (values[0]) {
          const [matchType, value] = values[0];
          switch (type) {
            case RULE_TYPE.HOST:
              domain = value;
              break;
            case RULE_TYPE.URL:
              let prefix = '';
              if (matchType === MATCH_TYPE.REGEX && !/^[.*]/.test(value)) {
                prefix = '^';
              }
              url = prefix + value;
              break;
          }
        }
      });

      if (this.loadBalancer.spec.type === 'clb') {
        if (!domain) {
          throw new Error(this.translateService.get('domain_required_in_clb'));
        } else {
          // url可以为空，以/开头的url地址，满足一定条件的正则
          const domainVaild =
            (/[a-zA-Z0-9_.-]/.test(domain) || /^\*\.|\.\*$/.test(domain)) &&
            !/["{};\` ']/.test(domain);
          const urlVaild =
            (url &&
              (/^\/[a-zA-Z0-9_.-]/.test(url) ||
                (!/["{};\` ']/.test(url) && /^\^/.test(url)))) ||
            !url;

          if (!domainVaild || !urlVaild) {
            throw new Error(
              this.translateService.get('domain_or_url_invaild_in_clb'),
            );
          }
        }
      }
    }
    spec.domain = domain;
    spec.url = url;
    spec.dsl = stringifyDSL(ruleIndicators);
    return spec;
  }
}
