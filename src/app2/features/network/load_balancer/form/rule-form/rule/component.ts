import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { AbstractControl, Validators } from '@angular/forms';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import {
  LB_TYPE,
  MATCH_TYPE,
  RULE_TYPE,
} from 'app/services/api/network.service';
import { TranslateService } from 'app/translate/translate.service';
import { RuleIndicator } from 'app/typings';

@Component({
  selector: 'rc-load-balancer-rule-part-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerRulePartFormComponent
  extends BaseResourceFormGroupComponent<RuleIndicator>
  implements OnInit, OnDestroy {
  @Input()
  lbType: string;
  onDestroy$ = new Subject<void>();
  @Input()
  chosenControls: AbstractControl[];
  matchTypesMap: Map<
    string,
    {
      key: boolean;
      matchTypes: MATCH_TYPE[];
    }
  > = new Map();

  ruleTypes: { display: string; value: string }[];

  constructor(injector: Injector, private translateService: TranslateService) {
    super(injector);
    this.matchTypesMap
      .set(RULE_TYPE.HOST, {
        key: false,
        matchTypes: [MATCH_TYPE.IN],
      })
      .set(RULE_TYPE.URL, {
        key: false,
        matchTypes:
          this.lbType === 'slb'
            ? [MATCH_TYPE.STARTS_WITH]
            : [MATCH_TYPE.REGEX, MATCH_TYPE.STARTS_WITH],
      })
      .set(RULE_TYPE.SRC_IP, {
        key: false,
        matchTypes: [MATCH_TYPE.EQ, MATCH_TYPE.RANGE],
      })
      .set(RULE_TYPE.HEADER, {
        key: true,
        matchTypes: [MATCH_TYPE.EQ, MATCH_TYPE.RANGE, MATCH_TYPE.REGEX],
      })
      .set(RULE_TYPE.COOKIE, {
        key: true,
        matchTypes: [MATCH_TYPE.EQ],
      })
      .set(RULE_TYPE.PARAM, {
        key: true,
        matchTypes: [MATCH_TYPE.EQ, MATCH_TYPE.RANGE],
      });

    const ruleTypes = [];
    switch (this.lbType) {
      case LB_TYPE.SLB:
      case LB_TYPE.CLB:
        ruleTypes.push(RULE_TYPE.HOST, RULE_TYPE.URL);
        break;
      default:
        ruleTypes.push(
          RULE_TYPE.HOST,
          RULE_TYPE.URL,
          RULE_TYPE.SRC_IP,
          RULE_TYPE.HEADER,
          RULE_TYPE.COOKIE,
          RULE_TYPE.PARAM,
        );
        break;
    }

    this.ruleTypes = ruleTypes.map(value => ({
      display: this.translateService.get('rule_' + value.toLocaleLowerCase()),
      value,
    }));
  }

  isOccurInGroup(value: string) {
    //can select current label
    if (
      !Array.isArray(this.chosenControls) ||
      !this.chosenControls.length ||
      value === this.form.get('type').value
    ) {
      return false;
    }
    return !!this.chosenControls.find(e => e.value.type === value);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form
      .get('type')
      .valueChanges.pipe(takeUntil(this.onDestroy$))
      .subscribe(() => {
        this.onIndicatorTypeChange();
      });
    const type = this.form.get('type').value;
    if (!this.matchTypesMap.get(type).key) {
      this.form.get('key').disable({
        emitEvent: false,
      });
    }
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.onDestroy$.next();
  }

  getDefaultFormModel(): RuleIndicator {
    return {
      type: '',
      values: [],
      key: '',
    };
  }

  createForm() {
    return this.fb.group({
      type: ['', Validators.required],
      values: [[]],
      key: ['', Validators.required],
    });
  }

  showIndicatorKey() {
    const typeValue = this.form.get('type').value;
    return typeValue && this.matchTypesMap.get(typeValue).key;
  }

  onIndicatorTypeChange() {
    this.form.get('values').setValue([]);
    if (!this.showIndicatorKey()) {
      this.form.get('key').setValue('');
      this.form.get('key').disable({
        emitEvent: false,
      });
    } else {
      this.form.get('key').enable({
        emitEvent: false,
      });
    }
  }
}
