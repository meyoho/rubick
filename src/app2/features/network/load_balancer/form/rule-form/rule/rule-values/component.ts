import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormControl, Validators } from '@angular/forms';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { MATCH_TYPE, RULE_TYPE } from 'app/services/api/network.service';

interface RuleIndicatorValue {
  type: string;
  value?: string;
  startValue?: string;
  endValue?: string;
}

@Component({
  selector: 'rc-load-balancer-rule-values-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerRuleValuesFormComponent
  extends BaseResourceFormArrayComponent<string[], RuleIndicatorValue>
  implements OnInit, OnDestroy {
  @Input()
  ruleIndicatorType: string;
  @Input()
  lbType: string;
  onDestroy$ = new Subject<void>();

  matchTypesMap: Map<
    string,
    {
      key: boolean;
      matchTypes: MATCH_TYPE[];
    }
  > = new Map();

  PATTERNS = {
    SRC_IP: /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/,
    NUMBER: /^\d+$/,
  };

  constructor(injector: Injector) {
    super(injector);
    this.matchTypesMap
      .set(RULE_TYPE.HOST, {
        key: false,
        matchTypes: [MATCH_TYPE.IN],
      })
      .set(RULE_TYPE.URL, {
        key: false,
        matchTypes:
          this.lbType === 'slb'
            ? [MATCH_TYPE.STARTS_WITH]
            : [MATCH_TYPE.REGEX, MATCH_TYPE.STARTS_WITH],
      })
      .set(RULE_TYPE.SRC_IP, {
        key: false,
        matchTypes: [MATCH_TYPE.EQ, MATCH_TYPE.RANGE],
      })
      .set(RULE_TYPE.HEADER, {
        key: true,
        matchTypes: [MATCH_TYPE.EQ, MATCH_TYPE.RANGE, MATCH_TYPE.REGEX],
      })
      .set(RULE_TYPE.COOKIE, {
        key: true,
        matchTypes: [MATCH_TYPE.EQ],
      })
      .set(RULE_TYPE.PARAM, {
        key: true,
        matchTypes: [MATCH_TYPE.EQ, MATCH_TYPE.RANGE],
      });
  }

  typeChange(type?: string, control?: FormControl) {
    switch (type) {
      case undefined:
        break;
      case 'Range':
        control.get('value').disable({
          emitEvent: false,
        });
        control.get('startValue').enable({
          emitEvent: false,
        });
        control.get('endValue').enable({
          emitEvent: false,
        });
        break;
      default:
        control.get('value').enable({
          emitEvent: false,
        });
        control.get('startValue').disable({
          emitEvent: false,
        });
        control.get('endValue').disable({
          emitEvent: false,
        });
    }
  }

  ngOnInit() {
    super.ngOnInit();
    this.form.valueChanges.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.form.controls.forEach(control => {
        const typeValue = control.get('type').value;
        this.typeChange(typeValue, control as FormControl);
      });
    });

    // init form (更新时存在 controls，根据每个control里type的值设置input是否disable)
    // todo: 没有真正解决子表单状态更新后父表单更新滞后的问题
    this.form.controls.forEach(control => {
      const typeValue = control.get('type').value;
      this.typeChange(typeValue, control as FormControl);
    });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.onDestroy$.next();
  }

  getDefaultFormModel(): RuleIndicatorValue[] {
    return [];
  }

  createForm() {
    return this.fb.array([]);
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    const control =
      this.ruleIndicatorType === 'HOST'
        ? [
            {
              value: '',
              disable: true,
            },
          ]
        : ['', Validators.required];
    return this.fb.group({
      type: control,
      value: ['', Validators.required],
      startValue: control,
      endValue: control,
    });
  }

  adaptResourceModel(resource: string[][]): RuleIndicatorValue[] {
    if (resource && resource.length) {
      return resource.map(item => {
        if (item[0] === 'Range') {
          return {
            type: item[0],
            startValue: item[1] || '',
            endValue: item[2] || '',
          };
        }
        return {
          type: item[0],
          value: item[1] || '',
        };
      });
    }
  }

  adaptFormModel(formModel: RuleIndicatorValue[]): string[][] {
    return formModel.map(item => {
      if (item.type === 'Range') {
        return [item.type, item.startValue, item.endValue];
      } else {
        return [item.type, item.value];
      }
    });
  }
}
