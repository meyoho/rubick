import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { RuleService } from 'app/typings';
import { K8sResourceWithActions, Service, ServicePort } from 'app/typings';

interface RuleServiceForm extends RuleService {
  nameList: string[];
  portList: number[];
}
@Component({
  selector: 'rc-load-balancer-service-group-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class LoadBalancerServiceGroupFormComponent
  extends BaseResourceFormArrayComponent<RuleService, RuleServiceForm>
  implements OnInit {
  @Input()
  clusterName: string;
  @Input()
  namespaceList: string[];
  @Input()
  serviceList: K8sResourceWithActions<Service>[];

  servicePortMap: {
    [key: string]: ServicePort[];
  } = {};

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.serviceList.map((item: K8sResourceWithActions<Service>) => {
      const namespace = item.kubernetes.metadata.namespace;
      const name = item.kubernetes.metadata.name;
      this.servicePortMap[`${namespace}_${name}`] = item.kubernetes.spec.ports;
    });
  }

  getDefaultFormModel(): RuleServiceForm[] {
    return [];
  }

  createForm() {
    return this.fb.array([]);
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      name: ['', Validators.required],
      namespace: ['', Validators.required],
      nameList: [],
      portList: [],
      port: ['', Validators.required],
      weight: [
        100,
        [Validators.required, Validators.max(100), Validators.min(0)],
      ],
    });
  }

  namespaceChange(namespace: string, control?: any) {
    let services: string[] = [];
    if (namespace) {
      const serviceList = this.serviceList.filter(
        (item: K8sResourceWithActions<Service>) => {
          return item.kubernetes.metadata.namespace === namespace;
        },
      );
      services = serviceList.map((item: K8sResourceWithActions<Service>) => {
        return item.kubernetes.metadata.name;
      });
    }
    return control ? control.get('nameList').setValue(services) : services;
  }

  serviceChange(service: string, control?: any, namespace?: string) {
    let list: number[] = [];
    if (service) {
      const namespaceValue = control
        ? control.get('namespace').value
        : namespace;
      const portList = this.servicePortMap[`${namespaceValue}_${service}`];
      list =
        portList &&
        (portList.map((item: ServicePort) => {
          return item.port;
        }) as number[]);
    }
    return control ? control.get('portList').setValue(list) : list;
  }

  getWeightsPercent(weight: number) {
    const total = this.form.controls.reduce(
      (total: number, control: any) =>
        total + (control.get('weight').value || 0),
      0,
    );

    return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
  }

  adaptResourceModel(resource: RuleService[]): RuleServiceForm[] {
    if (resource && resource.length) {
      return resource.map((item: RuleService) => {
        const nameList = this.namespaceChange(item.namespace);
        const portList = this.serviceChange(item.name, null, item.namespace);
        return {
          name: item.name,
          nameList,
          namespace: item.namespace,
          port: item.port,
          portList,
          weight: item.weight,
        };
      });
    }
  }

  adaptFormModel(formModel: RuleServiceForm[]): RuleService[] {
    return formModel.map((item: RuleServiceForm) => {
      return {
        name: item.name,
        namespace: item.namespace,
        port: item.port,
        weight: item.weight,
      };
    });
  }
}
