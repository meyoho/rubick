import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ALB_RESOURCE_TYPE } from 'app/services/api/network.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { Frontend, K8sResourceWithActions, Secret } from 'app/typings';
@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerUpdateDefaultCertificateComponent implements OnInit {
  @Output()
  confirmed: EventEmitter<boolean> = new EventEmitter();

  cluster: string;
  certificateName: string;
  secretList: K8sResourceWithActions<Secret>[] = [];
  confirming: boolean;

  constructor(
    private errorsToastService: ErrorsToastService,
    private k8sResourceService: K8sResourceService,
    @Inject(DIALOG_DATA)
    private modalData: {
      cluster: string;
      frontendDetail: Frontend;
    },
  ) {
    const { frontendDetail, cluster } = this.modalData;
    this.cluster = cluster;
    this.certificateName = frontendDetail.spec.certificate_name;
  }

  async ngOnInit() {
    const res_secret = await this.k8sResourceService.getK8sResources(
      'secrets',
      {
        clusterName: this.cluster,
      },
    );
    this.secretList = res_secret;
  }

  async submitForm(formRef: NgForm) {
    formRef.onSubmit(null);
    if (formRef.form.invalid || this.confirming) {
      return;
    }
    this.confirming = true;
    this.modalData.frontendDetail.spec.certificate_name = this.certificateName;
    try {
      await this.k8sResourceService.updateK8sReource(
        this.cluster,
        ALB_RESOURCE_TYPE.FRONTEND,
        this.modalData.frontendDetail,
      );
      this.confirmed.next(true);
    } catch (e) {
      this.errorsToastService.error(e);
    } finally {
      this.confirming = false;
    }
  }
}
