import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { cloneDeep } from 'lodash-es';

import { Domain, DomainService } from 'app/services/api/domain.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ALB_RESOURCE_TYPE } from 'app/services/api/network.service';
import { LoadBalancer } from 'app/typings';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class LoadBalancerUpdateDomainSuffixComponent implements OnInit {
  @Output()
  finished = new EventEmitter<any>();
  @ViewChild('domainSuffixForm')
  form: NgForm;

  initialized: boolean;
  loadbalancer: LoadBalancer;
  submitting = false;
  cluster: string;
  domainList: Domain[] = [];

  constructor(
    @Inject(DIALOG_DATA)
    modalData: {
      cluster: string;
      loadbalancer: LoadBalancer;
    },
    private domainService: DomainService,
    private k8sResourceService: K8sResourceService,
  ) {
    this.loadbalancer = cloneDeep(modalData.loadbalancer);
    this.cluster = modalData.cluster;
  }

  ngOnInit() {
    this.initialized = false;
    this.domainService
      .getDomains(false, {
        page: 1,
        page_size: 1000,
        search: '',
      })
      .then(({ results }) => {
        results.map((item: Domain) => {
          if (item.domain.startsWith('*.')) {
            this.domainList.push(item);
          }
        });
      })
      .finally(() => {
        this.initialized = true;
      });
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      const payload = {
        ...this.loadbalancer,
        metadata: {
          name: this.loadbalancer.metadata.name,
          namespace: this.loadbalancer.metadata.namespace,
        },
      };
      this.k8sResourceService
        .updateK8sReourceWithPatch(this.cluster, ALB_RESOURCE_TYPE.ALB, payload)
        .then(() => this.done(true))
        .catch(() => {
          this.submitting = false;
        });
    }
  }

  private done(res?: any) {
    this.finished.next(res);
    this.finished.complete();
  }

  cancel() {
    this.done();
  }
}
