import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { NgForm } from '@angular/forms';

import { merge } from 'lodash-es';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ALB_RESOURCE_TYPE } from 'app/services/api/network.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import {
  Frontend,
  K8sResourceWithActions,
  Service,
  ServiceGroup,
} from 'app/typings';
@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class LoadBalancerPortUpdateServiceComponent implements OnInit {
  @Output()
  confirmed: EventEmitter<boolean> = new EventEmitter();

  cluster: string;
  namespaceList: string[] = [];
  protocol: string;
  serviceList: K8sResourceWithActions<Service>[] = [];
  serviceGroup: ServiceGroup = {
    session_affinity_attribute: '',
    session_affinity_policy: '',
    services: [
      {
        name: '',
        namespace: '',
        port: '',
        weight: 100,
      },
    ],
  };

  confirming: boolean;

  constructor(
    private errorsToastService: ErrorsToastService,
    private k8sResourceService: K8sResourceService,
    @Inject(DIALOG_DATA)
    private modalData: {
      cluster: string;
      frontendDetail: Frontend;
    },
  ) {
    const { frontendDetail, cluster } = this.modalData;
    merge(this.serviceGroup, frontendDetail.spec.serviceGroup);
    this.cluster = cluster;
    this.protocol = frontendDetail.spec.protocol;
  }

  async ngOnInit() {
    const res_service = await this.k8sResourceService.getK8sResources(
      'services',
      {
        clusterName: this.cluster,
      },
    );
    res_service.map((item: K8sResourceWithActions<Service>) => {
      if (
        item.kubernetes.metadata.namespace &&
        !this.namespaceList.includes(item.kubernetes.metadata.namespace)
      ) {
        this.namespaceList.push(item.kubernetes.metadata.namespace);
      }
      return item.kubernetes.metadata.namespace;
    });
    this.serviceList = res_service;
  }

  async submitForm(formRef: NgForm) {
    formRef.onSubmit(null);
    if (formRef.form.invalid || this.confirming) {
      return;
    }
    this.confirming = true;
    this.modalData.frontendDetail.spec.serviceGroup = this.serviceGroup;
    try {
      await this.k8sResourceService.updateK8sReource(
        this.cluster,
        ALB_RESOURCE_TYPE.FRONTEND,
        this.modalData.frontendDetail,
      );
      this.confirmed.next(true);
    } catch (e) {
      this.errorsToastService.error(e);
    } finally {
      this.confirming = false;
    }
  }
}
