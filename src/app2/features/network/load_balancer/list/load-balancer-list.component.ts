import { DialogService } from '@alauda/ui';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { Subject } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import {
  ALB_RESOURCE_TYPE,
  NetworkService,
} from 'app/services/api/network.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, LoadBalancer } from 'app/typings';
import { LoadBalancerCreateComponent } from 'app2/features/network/load_balancer/create/component';
import { ForceDeleteComponent } from 'app2/shared/components/force-delete/component';

@Component({
  templateUrl: './load-balancer-list.component.html',
  styleUrls: ['./load-balancer-list.component.scss'],
})
export class LoadBalancerListComponent implements OnInit, OnDestroy {
  loadBalancerList: LoadBalancer[];
  onDestroy$ = new Subject<void>();
  loading: boolean;
  loadError: boolean;
  region: Cluster;
  filterKey = '';
  lbCreateEnabled = false;
  columns = ['name', 'type', 'address', 'action'];

  constructor(
    private regionService: RegionService,
    private dialogService: DialogService,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private translateService: TranslateService,
    private k8sResourceService: K8sResourceService,
    private networkService: NetworkService,
  ) {
    this.loading = this.loadError = false;
    this.loadBalancerList = [];
  }

  ngOnInit() {
    this.loading = true;
    this.regionService.region$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(region => !!region),
      )
      .subscribe(async region => {
        this.region = region;
        this.fetch(region.name);
        this.lbCreateEnabled = await this.roleUtil.resourceTypeSupportPermissions(
          'load_balancer',
          {
            cluster_name: region.name,
          },
        );
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.onDestroy$.complete();
  }

  async fetch(region_name: string) {
    try {
      const res = await this.k8sResourceService.getK8sResources(
        ALB_RESOURCE_TYPE.ALB,
        {
          clusterName: region_name,
        },
      );
      this.loadBalancerList = res.map(
        (item: K8sResourceWithActions<LoadBalancer>) => {
          return item.kubernetes;
        },
      );
    } catch (e) {
      this.loadError = true;
    } finally {
      this.loading = false;
    }
  }

  searchChanged(event: string) {
    this.filterKey = event;
  }

  create() {
    const modelRef = this.dialogService.open(LoadBalancerCreateComponent, {
      data: {
        cluster: this.region.name,
      },
    });
    modelRef.componentInstance.confirmed.pipe(first()).subscribe(res => {
      modelRef.close();
      if (res) {
        this.fetch(this.region.name);
      }
    });
  }

  getLoadBalancerList() {
    return this.loadBalancerList.filter(
      lb => lb.metadata.name.indexOf(this.filterKey.toLowerCase()) !== -1,
    );
  }

  viewLoadBalancer(lb: LoadBalancer) {
    this.router.navigate([
      '/console/admin/load_balancer/detail',
      lb.metadata.namespace,
      lb.metadata.name,
    ]);
  }

  async deleteLoadBalancer(lb: LoadBalancer) {
    try {
      const modelRef = await this.dialogService.open(ForceDeleteComponent, {
        data: {
          name: lb.metadata.name,
          title: this.translateService.get('delete_load_balancer'),
          content: this.translateService.get('delete_load_balancer_content', {
            name: lb.metadata.name,
          }),
        },
      });
      modelRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: Boolean) => {
          modelRef.close();
          if (res) {
            this.loading = true;
            await this.networkService.deleteLoadbalancer(
              this.region.name,
              'alauda-system',
              lb.metadata.name,
            );
            this.fetch(this.region.name);
          }
        });
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  trackByFn(_index: number, item: LoadBalancer) {
    return item.metadata.uid;
  }
}
