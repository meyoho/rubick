import {
  MATCH_TYPE,
  MATCH_TYPES,
  RULE_TYPE,
} from 'app/services/api/network.service.ts';
import { Rule, RuleIndicator } from 'app/typings';

const KEY_TYPES = [
  RULE_TYPE.COOKIE,
  RULE_TYPE.HEADER,
  RULE_TYPE.PARAM,
] as string[];

const RULE_INDICATORS_CACHE = {};
const PARTIAL_RULE_INDICATORS_CACHE = {};

function tokenizer(rawDsl: string): string[] {
  const tokens: string[] = [];
  let nextTokenBeg = 0;
  rawDsl.split('').forEach((char, i) => {
    if (char === '(') {
      tokens.push('(');
      nextTokenBeg = i + 1;
    } else if (char === ' ') {
      const token = rawDsl.substring(nextTokenBeg, i);
      if (token && token !== ' ' && token !== ')') {
        tokens.push(token);
      }
      nextTokenBeg = i + 1;
    } else if (char === ')') {
      const token = rawDsl.substring(nextTokenBeg, i);
      if (token && token !== ' ' && token !== ')') {
        tokens.push(token);
      }
      tokens.push(')');
      nextTokenBeg = i + 1;
    }
  });
  return tokens;
}

function parseTokens(tokens: string[]): string | any[] {
  if (!tokens.length) {
    return null;
  }

  const token = tokens.shift();

  if (token !== '(') {
    return token;
  }

  const exp = [];
  while (tokens[0] !== ')') {
    exp.push(parseTokens(tokens));
  }
  tokens.shift();
  return exp;
}

export function parseDSL(dslOrRule: string | Rule): RuleIndicator[] {
  let rawDSL;

  if (typeof dslOrRule === 'string') {
    rawDSL = dslOrRule;
  } else if (dslOrRule.spec.dsl) {
    rawDSL = dslOrRule.spec.dsl;
  } else {
    const indicators: RuleIndicator[] = [];

    if (dslOrRule.spec.domain) {
      indicators.push({
        type: RULE_TYPE.HOST,
        key: null,
        values: [[MATCH_TYPE.IN, dslOrRule.spec.domain]],
      });
    } else if (dslOrRule.spec.url) {
      indicators.push({
        type: RULE_TYPE.URL,
        key: null,
        values: [
          [
            dslOrRule.spec.url.startsWith('/')
              ? MATCH_TYPE.STARTS_WITH
              : MATCH_TYPE.REGEX,
            dslOrRule.spec.url,
          ],
        ],
      });
    } else {
      return [];
    }

    rawDSL = stringifyDSL(indicators);
  }

  if (RULE_INDICATORS_CACHE[rawDSL]) {
    return RULE_INDICATORS_CACHE[rawDSL];
  }

  const tokens = parseTokens(tokenizer(rawDSL)) as any[];

  if (!tokens) {
    return [];
  }

  const first = tokens[0];

  const indicators = first === MATCH_TYPE.AND ? tokens.slice(1) : tokens;

  const ruleIndicators: RuleIndicator[] = [];

  (Array.isArray(indicators[0]) ? indicators : [indicators]).forEach(
    indicator => {
      let matchType: MATCH_TYPE = indicator[0];
      const values: string[] | string[][] = indicator.slice(1);

      const ruleIndicator = { values: [] } as RuleIndicator;

      if (matchType === MATCH_TYPE.OR) {
        (values as string[][]).forEach(value => {
          matchType = value.shift() as MATCH_TYPE;
          const ruleType = value.shift();

          if (!ruleIndicator.type) {
            ruleIndicator.type = ruleType as RULE_TYPE;
          } else if (ruleIndicator.type !== ruleType) {
            throw new TypeError(
              'invalid dsl, rule type should be same in a single OR block',
            );
          }

          if (KEY_TYPES.includes(ruleIndicator.type)) {
            const key = value.shift() as string;

            if (!ruleIndicator.key) {
              ruleIndicator.key = key;
            } else if (ruleIndicator.key !== key) {
              throw new TypeError(
                'invalid dsl, rule key should be same in a single OR block',
              );
            }
          }

          ruleIndicator.values.push([MATCH_TYPE[matchType], ...value]);
        });
      } else {
        const ruleType = values.shift() as RULE_TYPE;

        ruleIndicator.type = ruleType;

        if (KEY_TYPES.includes(ruleIndicator.type)) {
          ruleIndicator.key = values.shift() as string;
        }

        if (ruleType === RULE_TYPE.HOST) {
          (values as string[]).forEach(value => {
            ruleIndicator.values.push([MATCH_TYPE[matchType], value]);
          });
        } else {
          ruleIndicator.values.push([MATCH_TYPE[matchType], ...values]);
        }
      }

      ruleIndicators.push(ruleIndicator);
    },
  );

  return (RULE_INDICATORS_CACHE[rawDSL] = ruleIndicators);
}

export function stringifyDSL(ruleIndicators: RuleIndicator[]): string {
  let result = '';

  ruleIndicators.forEach(({ key, type, values }) => {
    if (!values.length) {
      return;
    }

    if (type === RULE_TYPE.HOST) {
      result += ` (IN HOST ${values.map(valueArr => valueArr[1]).join(' ')})`;
      return;
    }

    const temp = values.reduce((temp, [matchType, ...args]) => {
      matchType = MATCH_TYPES[matchType];

      if (key) {
        args.unshift(key);
      }

      args.unshift(matchType, type);

      temp += ` (${args.join(' ')})`;
      return temp;
    }, '');

    result += values.length > 1 ? ` (OR${temp})` : temp;
  });

  return result && `(AND${result})`;
}

export function getPartialIndicators(
  rawDslOrIndicators: string | RuleIndicator[],
): { more: boolean; indicators: RuleIndicator[] } {
  if (typeof rawDslOrIndicators !== 'string') {
    rawDslOrIndicators = stringifyDSL(rawDslOrIndicators);
  }

  if (PARTIAL_RULE_INDICATORS_CACHE[rawDslOrIndicators]) {
    return PARTIAL_RULE_INDICATORS_CACHE[rawDslOrIndicators];
  }

  const ruleIndicators =
    typeof rawDslOrIndicators === 'string'
      ? parseDSL(rawDslOrIndicators)
      : rawDslOrIndicators;

  const indicators: RuleIndicator[] = [];

  let i = 0;
  let more: boolean;

  ruleIndicators.some(({ key, type, values }) => {
    const newValues: string[][] = [];

    values.some(value => {
      more = i++ > 2;

      if (!more) {
        newValues.push(value);
      }

      return more;
    });

    if (newValues.length) {
      indicators.push({ key, type, values: newValues });
    }

    return more;
  });

  return (PARTIAL_RULE_INDICATORS_CACHE[rawDslOrIndicators] = {
    more,
    indicators,
  });
}
