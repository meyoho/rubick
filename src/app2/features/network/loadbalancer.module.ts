import { NgModule } from '@angular/core';

import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { LoadBalancerCreateComponent } from 'app2/features/network/load_balancer/create/component';
import { LoadBalancerDetailComponent } from 'app2/features/network/load_balancer/detail/component';
import { LoadBalancerDetailInfoComponent } from 'app2/features/network/load_balancer/detail/info/component';
import { LoadBalancerPortInfoComponent } from 'app2/features/network/load_balancer/detail/port-info/component';
import { LoadBalancerPortComponent } from 'app2/features/network/load_balancer/detail/port/component';
import { LoadBalancerServiceWeightDisplayComponent } from 'app2/features/network/load_balancer/detail/service-weight-display/component';
import { LoadBalancerAddFrontendComponent } from 'app2/features/network/load_balancer/form/add-frontend/component';
import { LoadBalancerPortAddServiceComponent } from 'app2/features/network/load_balancer/form/add-service/component';
import { LoadBalancerCertificateFormComponent } from 'app2/features/network/load_balancer/form/certificate/component';
import { LoadBalancerEditRuleComponent } from 'app2/features/network/load_balancer/form/edit-rule/component';
import { LoadBalancerFrontEndFormComponent } from 'app2/features/network/load_balancer/form/frontend-form/component';
import { LoadBalancerServiceGroupWithAffinityComponent } from 'app2/features/network/load_balancer/form/frontend-form/service-group-affinity/component';
import { LoadBalancerRuleFormComponent } from 'app2/features/network/load_balancer/form/rule-form/component';
import { LoadBalancerRulePartFormComponent } from 'app2/features/network/load_balancer/form/rule-form/rule/component';
import { LoadBalancerRuleValuesFormComponent } from 'app2/features/network/load_balancer/form/rule-form/rule/rule-values/component';
import { LoadBalancerServiceGroupFormComponent } from 'app2/features/network/load_balancer/form/rule-form/service-group/component';
import { LoadBalancerUpdateDefaultCertificateComponent } from 'app2/features/network/load_balancer/form/update-default-certificate/component';
import { LoadBalancerUpdateDomainSuffixComponent } from 'app2/features/network/load_balancer/form/update-domain-suffix/component';
import { LoadBalancerPortUpdateServiceComponent } from 'app2/features/network/load_balancer/form/update-service/component';
import { LoadBalancerListComponent } from 'app2/features/network/load_balancer/list/load-balancer-list.component';
import { LoadbalancerRoutingModule } from 'app2/features/network/loadbalancer.routing.module';

@NgModule({
  imports: [SharedModule, LoadbalancerRoutingModule, FormTableModule],
  declarations: [
    LoadBalancerListComponent,
    LoadBalancerDetailComponent,
    LoadBalancerCreateComponent,
    LoadBalancerUpdateDomainSuffixComponent,
    LoadBalancerPortComponent,
    LoadBalancerServiceGroupFormComponent,
    LoadBalancerRuleFormComponent,
    LoadBalancerRulePartFormComponent,
    LoadBalancerRuleValuesFormComponent,
    LoadBalancerFrontEndFormComponent,
    LoadBalancerPortUpdateServiceComponent,
    LoadBalancerPortAddServiceComponent,
    LoadBalancerServiceGroupWithAffinityComponent,
    LoadBalancerDetailInfoComponent,
    LoadBalancerPortInfoComponent,
    LoadBalancerAddFrontendComponent,
    LoadBalancerEditRuleComponent,
    LoadBalancerCertificateFormComponent,
    LoadBalancerUpdateDefaultCertificateComponent,
    LoadBalancerServiceWeightDisplayComponent,
  ],
  entryComponents: [
    LoadBalancerCreateComponent,
    LoadBalancerUpdateDomainSuffixComponent,
    LoadBalancerPortUpdateServiceComponent,
    LoadBalancerPortAddServiceComponent,
    LoadBalancerUpdateDefaultCertificateComponent,
  ],
})
export class LoadbalancerModule {}
