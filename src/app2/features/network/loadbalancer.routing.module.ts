import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { LoadBalancerDetailComponent } from 'app2/features/network/load_balancer/detail/component';
import { LoadBalancerPortComponent } from 'app2/features/network/load_balancer/detail/port/component';
import { LoadBalancerAddFrontendComponent } from 'app2/features/network/load_balancer/form/add-frontend/component';
import { LoadBalancerEditRuleComponent } from 'app2/features/network/load_balancer/form/edit-rule/component';
import { LoadBalancerListComponent } from './load_balancer/list/load-balancer-list.component';

const routes: Routes = [
  {
    path: '',
    component: LoadBalancerListComponent,
    pathMatch: 'full',
  },
  {
    path: 'detail/:namespace/:name',
    component: LoadBalancerDetailComponent,
  },
  {
    path: 'detail/:namespace/:name/add-frontend',
    component: LoadBalancerAddFrontendComponent,
  },
  {
    path: 'detail/:namespace/:name/port/:port',
    component: LoadBalancerPortComponent,
  },
  {
    path: 'detail/:namespace/:name/port/:port/add-rule',
    component: LoadBalancerEditRuleComponent,
  },
  {
    path: 'detail/:namespace/:name/port/:port/rule/:rule/update',
    component: LoadBalancerEditRuleComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LoadbalancerRoutingModule {}
