import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SubnetDetailComponent } from 'app2/features/network/subnet/subnet-detail.component';
import { SubnetListComponent } from 'app2/features/network/subnet/subnet-list.component';

const routes: Routes = [
  {
    path: '',
    component: SubnetListComponent,
    pathMatch: 'full',
  },
  {
    path: 'detail/:name',
    component: SubnetDetailComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class SubnetRoutingModule {}
