import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { get } from 'lodash-es';

import {
  K8sSubnetPayload,
  NetworkService,
} from 'app/services/api/network.service';
import { Cluster } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { IPIP_MODE_TYPE } from 'app/typings';

@Component({
  templateUrl: './subnet-dailog.component.html',
})
export class SubnetDailogComponent implements OnInit {
  close: EventEmitter<Boolean> = new EventEmitter();
  model: K8sSubnetPayload;
  submitting = false;
  @ViewChild('createForm')
  form: NgForm;

  cniType = 'macvlan';

  constructor(
    private networkService: NetworkService,
    private errorsToastService: ErrorsToastService,
    @Inject(DIALOG_DATA) public modalData: { region: Cluster },
  ) {
    this.model = {
      name: '',
      cidr: '',
      gateway: '',
      block_size: 26,
      ipip_mode: IPIP_MODE_TYPE.CrossSubnet,
      nat_outgoing: false,
    };
  }

  async ngOnInit() {
    this.cniType = get(this.modalData.region, 'attr.kubernetes.cni.type');
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      try {
        await this.networkService.createK8sSubnet(
          this.modalData.region.id,
          this.model,
        );
        this.close.next(true);
      } catch (err) {
        this.close.next(false);
        this.errorsToastService.error(err);
      } finally {
        this.submitting = false;
      }
    }
  }

  cancel() {
    this.close.next(true);
  }
}
