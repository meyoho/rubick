import { DialogService } from '@alauda/ui';
import { ChangeDetectorRef } from '@angular/core';
import {
  ChangeDetectionStrategy,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';

import { get } from 'lodash-es';
import { from, merge, Subject } from 'rxjs';
import { map, switchMap, takeUntil } from 'rxjs/operators';

import { NetworkService } from 'app/services/api/network.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ErrorResponse } from 'app/services/http.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, SubnetIp } from 'app/typings';
import { SubnetImportIpComponent } from 'app2/features/network/subnet/subnet-import-ip.component';
import { SubnetUpdateComponent } from 'app2/features/network/subnet/subnet-update.component';

@Component({
  templateUrl: './subnet-detail.component.html',
  styleUrls: ['./subnet-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubnetDetailComponent implements OnInit, OnDestroy {
  detail: any;
  region: Cluster;
  update$: Subject<any> = new Subject();
  deleteIp$: Subject<any> = new Subject();
  import$: Subject<any> = new Subject();
  onDestroy$ = new Subject<void>();

  private _subnet_name: string;
  ips: K8sResourceWithActions<SubnetIp>[];
  ips_unused: K8sResourceWithActions<SubnetIp>[];
  columns = ['address', 'status', 'service', 'action'];
  importEnabled = false;
  cniType: string;

  constructor(
    private route$: ActivatedRoute,
    private networkService: NetworkService,
    private translate: TranslateService,
    private router: Router,
    private regionService: RegionService,
    private errorToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private cdr: ChangeDetectorRef,
    private dialogService: DialogService,
  ) {}

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.cniType = get(this.region, 'attr.kubernetes.cni.type');
    this.importEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'private_ip',
      { cluster_name: this.region.name },
      'create',
    );

    const updateObservable = this.update$.pipe(
      switchMap(() => {
        const ref = this.dialogService.open(SubnetUpdateComponent, {
          data: {
            subnet: this.detail,
            region: this.region,
          },
        });
        ref.componentInstance.close.subscribe((res: boolean) => {
          if (res) {
            ref.close();
          }
        });
        return ref.componentInstance.close;
      }),
    );

    const importObservable = this.import$.pipe(
      switchMap(() => {
        const modelRef = this.dialogService.open(SubnetImportIpComponent, {
          data: {
            subnet_name: this._subnet_name,
            region: this.region,
          },
        });
        modelRef.componentInstance.close.subscribe((res: boolean) => {
          if (res) {
            modelRef.close();
          }
        });
        return modelRef.componentInstance.close;
      }),
    );

    const deleteK8sIPObservable = this.deleteIp$.pipe(
      switchMap(async (ip_name: string) => {
        try {
          await this.dialogService.confirm({
            title: this.translate.get('delete'),
            content: this.translate.get('subnet_ip_delete', {
              address: ip_name,
            }),
            confirmText: this.translate.get('confirm'),
            cancelText: this.translate.get('cancel'),
          });
          await this.networkService.deleteK8sSubnetIp(
            this.region.id,
            this._subnet_name,
            ip_name,
          );
        } catch (err) {
          if (err instanceof ErrorResponse) {
            this.errorToastService.error(err);
          }
        }
      }),
    );

    const routeObservable = this.route$.paramMap.pipe(
      map((param: ParamMap) => {
        this._subnet_name = param.get('name');
        return this._subnet_name;
      }),
    );

    merge(
      routeObservable,
      updateObservable,
      importObservable,
      deleteK8sIPObservable,
    )
      .pipe(
        switchMap(() => {
          return from(
            this.networkService.getK8sSubnet(this.region.id, this._subnet_name),
          );
        }),
        takeUntil(this.onDestroy$),
      )
      .subscribe(
        detail => {
          this.detail = detail;
          this.networkService
            .getK8sSubnetIps(this.region.id, this._subnet_name)
            .then(ips => {
              this.ips = ips;
              this.ips_unused = ips.filter(
                ip =>
                  !(ip.kubernetes.spec.used || ip.kubernetes.spec.is_required),
              );
              this.cdr.markForCheck();
            });
          this.cdr.markForCheck();
        },
        _error => {},
      );
  }

  async deleteSubnet() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('subnet_delete', {
          name: this._subnet_name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      await this.networkService.deleteK8sSubnet(
        this.region.id,
        this._subnet_name,
      );
      this.router.navigateByUrl('/console/admin/subnet');
    } catch (err) {
      if (err instanceof ErrorResponse) {
        this.errorToastService.error(err);
      }
    }
  }

  buttonDisplay(data: K8sResourceWithActions<SubnetIp>, action: string) {
    return this.networkService.canAction(data, action || 'get', 'subnet');
  }

  trackByFn(_index: number, item: any) {
    return item.kubernetes.metadata.name;
  }

  visibleFn(item: K8sResourceWithActions<SubnetIp>) {
    return (
      this.cniType !== 'calico' &&
      this.buttonDisplay(this.detail, 'update') &&
      !(item.kubernetes.spec.used || item.kubernetes.spec.is_required) &&
      this.networkService.canAction(item, 'delete', 'private_ip')
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
