import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { NetworkService } from 'app/services/api/network.service';
import { Cluster } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './subnet-import-ip.component.html',
})
export class SubnetImportIpComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean>();
  @ViewChild('importForm')
  form: NgForm;

  importMethodOptions: any[];
  importMethod = 'range';
  range: object;
  ipList: string[];
  submitting = false;
  constructor(
    @Inject(DIALOG_DATA)
    public modelData: { subnet_name: string; region: Cluster },
    private errorsToastService: ErrorsToastService,
    private networkService: NetworkService,
    private translate: TranslateService,
  ) {
    this.importMethodOptions = [
      { name: this.translate.get('range'), value: 'range' },
      { name: this.translate.get('list'), value: 'list' },
    ];
    this.range = {
      address_start: '',
      address_end: '',
    };
  }

  async ngOnInit() {}

  cancel() {
    this.close.next(true);
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      const data = {};
      if (this.importMethod === 'range') {
        data['address_range'] = this.range;
      } else if (this.importMethod === 'list') {
        data['address_list'] = this.ipList;
      }
      try {
        await this.networkService.importK8sSubnetIp(
          this.modelData.region.id,
          this.modelData.subnet_name,
          data,
        );
        this.close.next(true);
      } catch (err) {
        this.close.next(false);
        this.errorsToastService.error(err);
      } finally {
        this.submitting = false;
      }
    }
  }
}
