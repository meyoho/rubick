import { DialogService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, Subject } from 'rxjs';
import { catchError, filter, takeUntil, tap } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { NetworkService } from 'app/services/api/network.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, Subnet, Weblabs } from 'app/typings';
import { SubnetDailogComponent } from 'app2/features/network/subnet/subnet-dailog.component';
import { SubnetUpdateComponent } from 'app2/features/network/subnet/subnet-update.component';
import { PageParams, ResourceList } from 'app_user/core/types';

@Component({
  templateUrl: './subnet-list.component.html',
  styleUrls: ['./subnet-list.component.scss'],
})
export class SubnetListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  private region: Cluster;
  subnetCanCreate = false;
  regionSubnetEnabled = false;
  subnets: K8sResourceWithActions<Subnet>[];

  initialized = false;
  columns = ['subnet_name', 'subnet_segment', 'gateway', 'action'];
  onDestroy$ = new Subject<void>();

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private regionService: RegionService,
    private networkService: NetworkService,
    private errorsToastService: ErrorsToastService,
    private translate: TranslateService,
    private role: RoleUtilitiesService,
    private dialogService: DialogService,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {
    super(http, router, activedRoute, cdr);
  }

  trackByFn(_index: number, item: any) {
    return item.key;
  }

  async ngOnInit() {
    this.regionService.region$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(region => !!region),
      )
      .subscribe(async region => {
        this.region = region;
        super.ngOnInit();
        this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
          this.initialized = true;
        });
        this.subnetCanCreate = await this.role.resourceTypeSupportPermissions(
          'subnet',
          { cluster_name: this.region.name },
        );
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    //disable page if network error
    return from(
      this.networkService.getK8sSubnets(this.region.id, {
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
        name: params.search,
      }),
    ).pipe(
      tap(() => {
        this.regionSubnetEnabled = true;
      }),
      catchError(() => {
        this.regionSubnetEnabled = false;
        return from([
          {
            results: [],
            count: 0,
          },
        ]);
      }),
    );
  }

  async deleteSubnet(sb: K8sResourceWithActions<Subnet>) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('subnet_delete', {
          name: sb.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      await this.networkService.deleteK8sSubnet(
        this.region.id,
        sb.kubernetes.metadata.name,
      );
      this.onUpdate(null);
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  createSubnet() {
    const ref = this.dialogService.open(SubnetDailogComponent, {
      data: {
        region: this.region,
      },
    });
    ref.componentInstance.close.subscribe((res: boolean) => {
      if (res) {
        ref.close();
        this.onUpdate(null);
      }
    });
  }

  updateSubnet(sb: K8sResourceWithActions<Subnet>) {
    const ref = this.dialogService.open(SubnetUpdateComponent, {
      data: {
        subnet: sb,
        region: this.region,
      },
    });
    ref.componentInstance.close.subscribe((res: boolean) => {
      if (res) {
        ref.close();
        this.onUpdate(null);
      }
    });
  }

  viewK8sSubnet(sb: K8sResourceWithActions<Subnet>) {
    this.router.navigateByUrl(
      `/console/admin/subnet/detail/${sb.kubernetes.metadata.name}`,
    );
  }

  buttonDisplay(data: K8sResourceWithActions<Subnet>, action: string) {
    return this.networkService.canAction(data, action || 'get', 'subnet');
  }

  pageChanged(pageEvent: PageParams) {
    this.onPageEvent({
      pageIndex: ++pageEvent.pageIndex,
      pageSize: pageEvent.pageSize,
    });
  }
}
