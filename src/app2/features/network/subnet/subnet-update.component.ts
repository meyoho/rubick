import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { get } from 'lodash-es';

import {
  NetworkService,
  UpdateCalicoSubnet,
  UpdateMacvlanSubnet,
} from 'app/services/api/network.service';
import { Cluster } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { IPIP_MODE_TYPE, K8sResourceWithActions, Subnet } from 'app/typings';

@Component({
  templateUrl: './subnet-update.component.html',
})
export class SubnetUpdateComponent implements OnInit {
  @Output()
  close = new EventEmitter<boolean>();
  @ViewChild('updateForm')
  form: NgForm;
  submitting = false;
  cniType = 'macvlan';
  model: UpdateCalicoSubnet | UpdateMacvlanSubnet;
  constructor(
    @Inject(DIALOG_DATA)
    public modelData: {
      subnet: K8sResourceWithActions<Subnet>;
      region: Cluster;
    },
    private networkService: NetworkService,
    private errorsToastService: ErrorsToastService,
  ) {}

  async ngOnInit() {
    this.cniType = get(this.modelData.region, 'attr.kubernetes.cni.type');
    switch (this.cniType) {
      case 'macvlan':
        this.model = {
          cidr: this.modelData.subnet.kubernetes.spec.cidr_block || '',
          gateway: this.modelData.subnet.kubernetes.spec.gateway || '',
        };
        break;
      case 'calico':
        this.model = {
          ipip_mode:
            this.modelData.subnet.kubernetes.spec.ipip_mode ||
            IPIP_MODE_TYPE.CrossSubnet,
          nat_outgoing:
            this.modelData.subnet.kubernetes.spec.nat_outgoing || false,
        };
        break;
    }
  }

  cancel() {
    this.close.next(true);
  }

  async submit() {
    if (this.form.valid && !this.submitting) {
      this.submitting = true;
      try {
        await this.networkService.updateK8sSubnet(
          this.modelData.region.id,
          this.modelData.subnet.kubernetes.metadata.name,
          this.model,
        );
        this.close.next(true);
      } catch (err) {
        this.close.next(false);
        this.errorsToastService.error(err);
      } finally {
        this.submitting = false;
      }
    }
  }
}
