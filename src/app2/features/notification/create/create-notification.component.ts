import {
  DIALOG_DATA,
  NotificationService as AuiNotificationService,
} from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import {
  FormArray,
  FormBuilder,
  FormControl,
  FormGroup,
  Validators,
} from '@angular/forms';

import { uniqBy } from 'lodash-es';

import {
  NotificationDetail,
  NotificationListItem,
  NotificationPayload,
  NotificationService,
  NotificationSubscription,
} from 'app/services/api/notification.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

interface DialogData {
  notification: NotificationDetail;
}

@Component({
  selector: 'rc-create-notification',
  templateUrl: './create-notification.component.html',
  styleUrls: ['./create-notification.component.scss'],
})
export class CreateNotificationComponent implements OnInit {
  @Output()
  finish = new EventEmitter<boolean>();
  form: FormGroup;
  initialized = false;
  submitting = false;
  updating = false;
  subMethods: any[];
  defaultSub: any = {
    method: '',
    recipient: '',
    remark: '',
  };
  initialSubs: FormGroup[];
  notification: NotificationListItem;
  errorMappers: any = {};
  patternMap = {
    name: { reg: /^[a-z][a-z0-9-_]*$/, hint: 'notification_name_hint' },
    email: { reg: /^.+@.+\..+$/, hint: 'email_format_limit' },
    sms: { reg: /^\+{0,1}[0-9]{7,15}$/, hint: 'phone_format_limit' },
    call: { reg: /^\+{0,1}[0-9]{7,15}$/, hint: 'phone_format_limit' },
    webhook: { reg: /^.*$/, hint: '' },
    dingtalk: { reg: /^[a-z0-9]{64}$/, hint: 'dingtalk_format_hint' },
  };
  constructor(
    private fb: FormBuilder,
    private auiNotificationService: AuiNotificationService,
    private translate: TranslateService,
    private notificationService: NotificationService,
    private errorToastService: ErrorsToastService,
    @Inject(DIALOG_DATA) public dialogData: DialogData,
  ) {
    this.subMethods = [
      {
        name: this.translate.get('email'),
        value: 'email',
      },
      {
        name: this.translate.get('sms'),
        value: 'sms',
      },
      {
        name: this.translate.get('notification_type_webhook'),
        value: 'webhook',
      },
      {
        name: this.translate.get('dingtalk'),
        value: 'dingtalk',
      },
    ];
    this.initForm();
  }

  async ngOnInit() {
    if (this.dialogData.notification) {
      this.initNotification(this.dialogData.notification);
    }
    this.initialized = true;
  }

  // Invoked when updating
  initNotification(notification: NotificationDetail) {
    this.notification = notification;
    this.initialSubs = notification.subscriptions.map(
      (sub: NotificationSubscription) => {
        return this.createSub(sub);
      },
    );
    this.form = this.fb.group({
      subs: this.fb.array(this.initialSubs),
    });
    this.updating = true;
  }

  initForm() {
    this.errorMappers = {
      name: { pattern: this.translate.get(this.patternMap.name.hint) },
      email: { pattern: this.translate.get(this.patternMap.email.hint) },
      sms: { pattern: this.translate.get(this.patternMap.sms.hint) },
      call: { pattern: this.translate.get(this.patternMap.sms.hint) },
      webhook: { pattern: '' },
      dingtalk: { pattern: this.translate.get(this.patternMap.dingtalk.hint) },
    };
    this.form = this.fb.group({
      name: '',
      space: '',
      subs: this.fb.array([this.createSub()]),
    });
  }

  createSub(sub = this.defaultSub): FormGroup {
    const subFormGroup: FormGroup = this.fb.group({
      method: sub.method,
      recipient: sub.recipient,
      // Validators.pattern(this.patternMap[sub.method].reg),
      // secret: sub.secret,
      remark: sub.remark,
    });
    if (sub.method.length) {
      subFormGroup
        .get('recipient')
        .setValidators([
          Validators.pattern(this.patternMap[sub.method].reg),
          Validators.required,
        ]);

      if (sub.method === 'webhook') {
        subFormGroup.addControl(
          'secret',
          new FormControl(sub.secret ? sub.secret : ''),
        );
      }
    }
    return subFormGroup;
  }

  onMethodChange(value: string, index: number) {
    const subFormGroup = this.subFormArray.at(index) as FormGroup;
    if (value === 'webhook') {
      subFormGroup.addControl('secret', new FormControl(''));
    }

    // Recipient pattern
    subFormGroup
      .get('recipient')
      .setValidators([
        Validators.pattern(this.patternMap[value].reg),
        Validators.required,
      ]);

    subFormGroup.get('recipient').updateValueAndValidity();
    subFormGroup.get('recipient').markAsTouched();
  }

  addSub() {
    this.subFormArray.push(this.createSub());
  }

  removeSub(index: number) {
    this.subFormArray.removeAt(index);
  }

  get subFormArray(): FormArray {
    return <FormArray>this.form.get('subs');
  }

  complete(result?: boolean) {
    this.finish.next(result);
    this.finish.complete();
  }

  cancel() {
    this.complete();
  }

  async submit() {
    if (this.form.invalid || this.submitting) {
      return;
    }
    const value = this.form.value;
    // Check subscription key
    const uniqed = uniqBy(value.subs, 'recipient');
    if (uniqed.length < value.subs.length) {
      this.auiNotificationService.error(
        this.translate.get('duplicate_subscription_key'),
      );
      return;
    }
    const filteredSubs = value.subs.filter(
      (sub: NotificationSubscription) => !!sub.method.length,
    );
    if (!filteredSubs.length) {
      this.auiNotificationService.error(
        this.translate.get('recipient_required'),
      );
      return;
    }
    this.submitting = true;
    const payload: NotificationPayload = {
      name: value.name,
      subscriptions: filteredSubs,
    };
    try {
      if (this.updating) {
        await this.notificationService.updateNotification({
          name: this.notification.name,
          uuid: this.notification.uuid,
          subscriptions: filteredSubs,
        });
      } else {
        await this.notificationService.createNotification(payload);
      }
      this.complete(true);
    } catch (error) {
      this.errorToastService.error(error);
    }
    this.submitting = false;
  }
}
