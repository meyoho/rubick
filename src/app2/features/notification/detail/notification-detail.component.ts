import {
  DialogService,
  DialogSize,
  NotificationService as AuiNotificationService,
} from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import {
  NotificationDetail,
  NotificationService,
} from 'app/services/api/notification.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { CreateNotificationComponent } from '../create/create-notification.component';

@Component({
  templateUrl: './notification-detail.component.html',
  styles: [
    `
      .rc-detail-container {
        min-height: auto;
      }
    `,
  ],
})
export class NotificationDetailComponent implements OnInit {
  loading = false;
  loadError = false;
  paramsSubscription: Subscription;
  name = '';
  notification: NotificationDetail;
  canDelete = false;
  canUpdate = false;
  isDeleting = false;
  columns = ['method', 'recipient', 'secret', 'remark'];
  constructor(
    private route: ActivatedRoute,
    private dialogService: DialogService,
    private errorsToastService: ErrorsToastService,
    private location: Location,
    private notificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private auiNotificationService: AuiNotificationService,
    private translate: TranslateService,
    private router: Router,
  ) {}

  trackByFn(i: number) {
    return i;
  }

  async ngOnInit() {
    this.paramsSubscription = this.route.params
      .pipe(first())
      .subscribe(async params => {
        this.name = params['uuid'];
        await this.getNotification();
      });
  }

  async update() {
    try {
      const modalRef = await this.dialogService.open(
        CreateNotificationComponent,
        {
          size: DialogSize.Big,
          data: {
            notification: this.notification,
          },
        },
      );
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            this.auiNotificationService.success(
              this.translate.get('notification_update_success'),
            );
            this.getNotification();
          }
        });
    } catch {}
  }

  async getNotification() {
    this.loading = true;
    try {
      const res = await this.notificationService.getNotificationDetail(
        this.name,
      );
      this.notification = res;
      this.canDelete = this.roleUtil.resourceHasPermission(
        this.notification,
        'notification',
        'delete',
      );
      this.canUpdate = this.roleUtil.resourceHasPermission(
        this.notification,
        'notification',
        'update',
      );
    } catch (errors) {
      this.loadError = true;
      this.errorsToastService.error(errors);
      this.location.back();
    }
    this.loading = false;
  }

  async delete() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_notification_title'),
        content: this.translate.get('delete_notification_content', {
          notification_name: this.notification.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch {
      return;
    }
    this.isDeleting = true;
    try {
      await this.notificationService.deleteNotification(this.notification.uuid);
      this.auiNotificationService.success(this.translate.get('delete_success'));
      this.router.navigate(['/console/admin/notification']);
    } catch ({ errors }) {}
  }
}
