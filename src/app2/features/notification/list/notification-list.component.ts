import {
  DialogService,
  DialogSize,
  NotificationService as AuiNotificationService,
} from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import {
  NotificationListItem,
  NotificationService,
} from 'app/services/api/notification.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';
import { cloneDeep, includes } from 'lodash-es';
import moment from 'moment';
import { first } from 'rxjs/operators';

import { CreateNotificationComponent } from '../create/create-notification.component';

@Component({
  templateUrl: './notification-list.component.html',
  styleUrls: ['./notification-list.component.scss'],
})
export class NotificationListComponent implements OnInit {
  loading = false;
  initialized = false;
  notifications: NotificationListItem[];
  notificationCache: NotificationListItem[];
  hasCreatePermission = false;
  keyword = '';
  columns = ['name', 'created_by', 'created_at', 'action'];
  constructor(
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private translate: TranslateService,
    private auiNotificationService: AuiNotificationService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.hasCreatePermission = await this.roleUtil.resourceTypeSupportPermissions(
      'notification',
    );
    await this.load();
    this.initialized = true;
  }

  trackByFn(index: number) {
    return index;
  }

  filter(keyword: string) {
    this.keyword = keyword;
    if (!this.keyword.length) {
      this.notifications = this.notificationCache;
      return;
    }
    this.notifications = this.notificationCache.filter(item => {
      return includes(
        item.name.toLocaleLowerCase(),
        this.keyword.toLocaleLowerCase(),
      );
    });
  }

  async load() {
    this.loading = true;
    try {
      const result = await this.notificationService.getNotifications();
      result.forEach(item => {
        item.isDeleting = false;
      });
      this.notifications = result.sort((prev, next) => {
        return moment(prev.created_at).isAfter(next.created_at) ? -1 : 1;
      });
      this.notificationCache = cloneDeep(this.notifications);
      this.filter(this.keyword);
    } catch ({ errors }) {}
    this.loading = false;
  }

  async create() {
    try {
      const modalRef = await this.dialogService.open(
        CreateNotificationComponent,
        {
          size: DialogSize.Big,
        },
      );
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            this.auiNotificationService.success(
              this.translate.get('notification_create_success'),
            );
            this.load();
          }
        });
    } catch {}
  }

  async update(notificationItem: NotificationListItem) {
    try {
      const notification = await this.notificationService.getNotificationDetail(
        notificationItem.name,
      );
      const modalRef = await this.dialogService.open(
        CreateNotificationComponent,
        {
          size: DialogSize.Big,
          data: {
            notification,
          },
        },
      );
      modalRef.componentInstance.finish
        .pipe(first())
        .subscribe((result: boolean) => {
          modalRef.close();
          if (result) {
            this.auiNotificationService.success(
              this.translate.get('notification_update_success'),
            );
            this.load();
          }
        });
    } catch {}
  }

  async delete(notification: NotificationListItem) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_notification_title'),
        content: this.translate.get('delete_notification_content', {
          notification_name: notification.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch {
      return;
    }
    notification.isDeleting = true;
    try {
      await this.notificationService.deleteNotification(notification.uuid);
      this.auiNotificationService.success(this.translate.get('delete_success'));
      this.load();
    } catch ({ errors }) {}
  }

  canUpdate(notification: NotificationListItem) {
    return this.roleUtil.resourceHasPermission(
      notification,
      'notification',
      'update',
    );
  }

  canDelete(notification: NotificationListItem) {
    return this.roleUtil.resourceHasPermission(
      notification,
      'notification',
      'delete',
    );
  }

  viewDetail(notification: NotificationListItem) {
    this.router.navigate([
      '/console/admin/notification/detail',
      notification.uuid,
    ]);
  }
}
