import { CodeEditorModule } from '@alauda/code-editor';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { PipelineModule } from 'app/features-shared/pipeline/pipeline-shared.module.ts';
import { SharedModule } from 'app/shared/shared.module';

import { PipelinesRoutingModule } from './pipelines.routing.module';
import { PipelineTemplateComponent } from './templates/pipeline-template.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    FormsModule,
    SharedModule,
    CodeEditorModule,
    PipelinesRoutingModule,
    PipelineModule,
  ],
  declarations: [PipelineTemplateComponent],
})
export class PipelinesModule {}
