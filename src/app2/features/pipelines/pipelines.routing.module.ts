import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PipelineTemplateComponent } from './templates/pipeline-template.component';

const routes: Routes = [
  {
    path: '',
    component: PipelineTemplateComponent,
  },
  {
    path: 'templates',
    component: PipelineTemplateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class PipelinesRoutingModule {}
