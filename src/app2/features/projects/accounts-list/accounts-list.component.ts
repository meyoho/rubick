import { DialogService } from '@alauda/ui';
import { NotificationService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { FormControl, FormGroup, NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { cloneDeep, flattenDeep, get, uniqBy, values } from 'lodash-es';

import { AccountService } from 'app/services/api/account.service';
import { AccountType, RcAccount } from 'app/services/api/account.service';
import { OrgService } from 'app/services/api/org.service';
import { RBACService } from 'app/services/api/rbac.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';
import { VendorCustomer } from 'app/typings/vendor-customer.types';
import { RBAC_USERNAME_PATTERN } from 'app/utils/patterns';

@Component({
  selector: 'rc-accounts-list',
  templateUrl: './accounts-list.component.html',
  styleUrls: ['./accounts-list.component.scss'],
})
export class ProjectAccountsListComponent implements OnInit {
  @Input()
  isCreate: boolean;

  @Input()
  projectName: string;

  @Input()
  namespaceName: string;

  @Input()
  namespaceUuid: string;

  @Input()
  clusterNames: string;

  @Output()
  onUserAdd = new EventEmitter<any>();
  @ViewChild('form')
  form: NgForm;

  isUserView: boolean;

  loading = true;
  loadingError = false;
  roleAdding: boolean;
  users: RcAccount[];
  currentUserPage = 1;
  userCount = 0;
  usernamePattern = RBAC_USERNAME_PATTERN;

  accountTypes: AccountType[] = [];

  templateMap = {};

  search = '';

  columnDefs: any[] = [
    {
      name: 'username',
      label: 'name',
    },
    {
      name: 'alias_name',
      label: 'alias_name',
    },
    {
      name: 'roles',
      label: 'roles',
    },
    {
      name: 'type',
      label: 'user_type',
    },
    {
      name: 'sub_type',
      label: 'user_source',
    },
    {
      name: 'is_valid',
      label: 'status',
    },
    {
      name: 'assigned_at',
      label: 'assigned_at',
    },
    {
      name: 'action',
      label: '',
    },
  ];
  columns = this.columnDefs.map(columnDef => columnDef.name);
  userFormGroup = new FormGroup({
    selectedUser: new FormControl(),
    selectedRole: new FormControl(),
  });

  rolesViewPermission: boolean;
  rolesAssignPermission: boolean;
  rolesRevokePermission: boolean;

  rolesCache: { [username: string]: AccountType }[] = [];

  constructor(
    private rbacService: RBACService,
    private orgService: OrgService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
    private errorToast: ErrorsToastService,
    private accountService: AccountService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  async ngOnInit() {
    this.isUserView = this.accountService.isUserView();
    [
      this.rolesViewPermission,
      this.rolesAssignPermission,
      this.rolesRevokePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'role',
      {
        project_name: this.projectName || '',
        namespace_name: this.namespaceName || '',
      },
      ['get', 'assign', 'revoke'],
    );
    await this.refetch();
    this.userFormGroup.get('selectedRole').setValue(this.accountTypes[0] || '');
    if (!this.accountTypes.length) {
      this.userFormGroup.get('selectedRole').disable();
    }
  }

  get canRoleView() {
    return this.rolesViewPermission && !this.isUserView;
  }

  checkCustomer(user: string) {
    return this.environments.vendor_customer === VendorCustomer[user];
  }

  async refetch() {
    try {
      const [, accounts] = await Promise.all([
        this.fetchRoleTypes(),
        this.fetchAccounts(),
      ]);

      await Promise.all(
        accounts.map(account => this.fetchAccountRoles(account.username)),
      );
    } catch (error) {
      this.errorToast.error(error);
    }
  }

  async fetchRoleTypes() {
    try {
      this.loading = true;
      this.loadingError = false;
      this.accountTypes = await this.rbacService
        .getValidRoleTypes({
          project_name: this.projectName || '',
          k8s_namespace_name: this.namespaceName || '',
          cluster_names: this.clusterNames || '',
          level: this.namespaceName ? 'namespace' : 'project',
        })
        .then(res =>
          res.map((role: AccountType) => ({
            role_uuid: role.role_uuid,
            role_name: role.role_name,
            template_display_name: role.template_display_name
              ? `${role.role_name}(${role.template_display_name})`
              : role.role_name,
            template_name: role.template_name,
            template_uuid: role.template_uuid,
          })),
        );

      if (this.isCreate) {
        this.accountTypes.forEach((role: AccountType) => {
          if (!this.templateMap[role.template_uuid]) {
            this.templateMap[role.template_uuid] = [{ name: role.role_name }];
          } else {
            this.templateMap[role.template_uuid].push({ name: role.role_name });
          }
        });
        this.accountTypes = uniqBy(this.accountTypes, 'template_uuid');
      }
    } catch (error) {
      this.loadingError = true;
      this.loading = false;
    }
    this.loading = false;
  }

  async fetchAccounts() {
    this.loading = true;
    this.loadingError = false;

    try {
      const res = await this.orgService.getAccounts({
        page: this.currentUserPage,
        page_size: 20,
        project_name: this.projectName || '',
        k8s_namespace_uuid: this.namespaceUuid || '',
        search: this.search,
        ignoreProject: true,
      });
      this.userCount = res.count;
      this.users = res.results;
      return res.results;
    } catch (err) {
      this.loadingError = true;
      this.errorToast.error(err);
    } finally {
      this.loading = false;
    }
  }

  onSearch($event: string) {
    this.search = $event;
    this.currentUserPage = 1;
    this.fetchAccounts();
  }

  pageChanged(page: number) {
    this.currentUserPage = page;
    this.fetchAccounts();
  }

  addAccount() {
    if (this.userFormGroup.invalid) {
      return;
    }
    const { selectedRole, selectedUser } = this.userFormGroup.value;
    this.roleAdding = true;
    let roles = [{ name: selectedRole.role_name }];
    if (this.isCreate) {
      roles = cloneDeep(this.templateMap[selectedRole.template_uuid]);
    }
    this.orgService
      .addAccountRoles(selectedUser, roles)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('user_add_success'),
        );
        this.fetchAccounts();
        this.fetchAccountRoles(selectedUser);
        this.onUserAdd.next(null);
        this.userFormGroup = new FormGroup({
          selectedUser: new FormControl(''),
          selectedRole: new FormControl(selectedRole),
        });
      })
      .catch(error => {
        if (get(error, 'errors[0].code') === 'user_have_role') {
          return this.auiNotificationService.error(
            this.translateService.get('user_have_role'),
          );
        } else if (get(error, 'errors[0].code') === 'role_assigned_overflow') {
          return this.auiNotificationService.error(
            this.translateService.get('only_one_administrator_allowed'),
          );
        }
        this.errorToast.error(error);
      })
      .then(() => {
        this.roleAdding = false;
      });
  }

  removeUser(username: string) {
    const roles = this.namespaceName
      ? flattenDeep(
          this.rolesCache[username].map((role: AccountType) => {
            return values(this.templateMap[role.template_uuid]).map(item => ({
              name: item.name,
            }));
          }),
        )
      : uniqBy(
          this.rolesCache[username].map((role: AccountType) => ({
            name: role.role_name,
          })),
          'name',
        );

    this.dialogService
      .confirm({
        title: this.translateService.get('delete_account_confirm', {
          name: username,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      })
      .then(() => {
        this.orgService
          .removeAccountRoles(username, roles)
          .then(() => {
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
            delete this.rolesCache[username];
            this.onUserAdd.next(null);
            this.pageChanged(1);
          })
          .catch(error => {
            this.errorToast.error(error);
          });
      })
      .catch(() => {});
  }

  removeRole(username: string, role_name: string) {
    this.rbacService
      .deleteUser(role_name, [{ user: username }])
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('delete_success'),
        );
        this.rolesCache[username] = this.rolesCache[username].filter(
          (role: AccountType) => role.role_name !== role_name,
        );
      })
      .catch(error => {
        this.errorToast.error(error);
      });
  }

  async fetchAccountRoles(username: string) {
    let params;
    if (this.namespaceUuid) {
      params = { k8s_namespace_uuid: this.namespaceUuid };
    } else {
      params = { project_name: this.projectName };
    }
    try {
      const data = await this.orgService.getAccountRoles(username, params);
      this.rolesCache[username] = data['result'] || [];
    } catch (err) {
      this.errorToast.error(err);
    }
  }

  getRoleDisplay(username: string) {
    if (!this.rolesCache[username] || !this.rolesCache[username][0]) {
      return '-';
    }
    const { role_name, template_display_name } = this.rolesCache[username][0];
    return template_display_name
      ? `${role_name}(${template_display_name})`
      : role_name;
  }

  showRoleDetail(username: string) {
    const { role_name } = this.rolesCache[username][0];
    if (role_name) {
      this.router.navigate(['/console/admin/rbac/roles/detail', role_name]);
    }
  }

  isInvalidAccount(account: RcAccount) {
    return account.type === 'organizations.LDAPAccount' && !account.is_valid;
  }

  get zeroState() {
    return this.users && !this.users.length && !this.loadingError;
  }

  getRoleDisplayName(item: RcAccount) {
    return item.roles[0].template_display_name
      ? item.roles[0].role_name +
          '(' +
          item.roles[0].template_display_name +
          ')'
      : item.roles[0].role_name;
  }

  accountTypeFilterFn(key: string, option: { label: string }) {
    return option.label.toLowerCase().includes(key.toLowerCase());
  }
}
