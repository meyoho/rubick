import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { cloneDeep, get, remove, union, uniqBy } from 'lodash-es';

import {
  ConfigurationApi,
  EnvironmentService,
} from 'app/services/api/environment.service';
import {
  ClusterInAvailabledResource,
  PreAddRegionData,
  PreSubmitData,
  ProjectService,
} from 'app/services/api/project.service';
import { MirrorCluster } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { Quota, QuotaUnit } from 'app/typings/quota.types';

interface ClusterWithLimit extends ClusterInAvailabledResource {
  mirror?: MirrorWithQuota;
  limitQuota?: Quota;
  uuid: string;
}

interface MirrorWithQuota {
  id: string;
  name: string;
  display_name: string;
  description?: string;
  flag: string;
  limitQuota?: Quota;
  quota?: Quota;
  regions: {
    name: string;
    display_name: string;
    id?: string;
    quota: any;
    limitQuota?: Quota;
  }[];
}

@Component({
  selector: 'rc-add-cluster',
  templateUrl: './add-cluster.component.html',
  styleUrls: ['./add-cluster.component.scss'],
})
export class AddClusterComponent implements OnInit {
  @ViewChild('quotaForm')
  quotaForm: NgForm;
  data: PreSubmitData = {
    name: '',
    service_type: 'kubernetes',
  };
  selectedRegions: ClusterInAvailabledResource[] = null;
  loading = false;
  project_name = '';
  clusters: ClusterWithLimit[] = [];
  normalRegions: PreAddRegionData[] = [];
  mirrorRegionsMap = {};
  setSameQuotaFlags = {};
  mirrorHiddenFlags = {};
  normalHiddenFlags: boolean[] = [];
  quotaPlaceholder = '';
  quotaKeys: string[] = [];
  defaultQuota: Quota;
  globalQuotaConfigs: ConfigurationApi[];

  constructor(
    private projectService: ProjectService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private environmentService: EnvironmentService,
    private errorToast: ErrorsToastService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      projectName: string;
      clusters: PreAddRegionData[];
    },
    private dialogRef: DialogRef<any>,
  ) {}

  async ngOnInit() {
    try {
      this.loading = true;
      const [globalQuotaConfigs, regionsAvaRes] = await Promise.all([
        this.environmentService.getConfigurations('quota'),
        this.projectService
          .getProjectAvailableResources()
          .then(({ clusters }) => clusters),
      ]);
      this.globalQuotaConfigs = globalQuotaConfigs;
      this.quotaKeys = globalQuotaConfigs.map(v => v.name);
      this.project_name = this.dialogData.projectName;
      const preClusters = this.dialogData.clusters;
      const excludeRegionsAvaRes = regionsAvaRes
        .filter(r => !preClusters.some(c => c.name === r.name))
        .map(r => {
          if (get(r, 'mirror.regions')) {
            r.mirror.regions = r.mirror.regions.filter(
              v => !preClusters.some(p => p.name === v.name),
            );
          }
          return r;
        });
      this.quotaPlaceholder = this.translateService.get('unlimited');
      const normalRegions = remove(
        excludeRegionsAvaRes,
        (region: ClusterInAvailabledResource) => !region.mirror.regions,
      );
      const basicQuota = {
        cpu: this.getBasicQuotaValue('cpu'),
        memory: this.getBasicQuotaValue('memory'),
        pvc_num: this.getBasicQuotaValue('pvc_num'),
        pods: this.getBasicQuotaValue('pods'),
        storage: this.getBasicQuotaValue('storage'),
      };
      this.defaultQuota =
        this.environmentService.getDefaultQuota('project') || basicQuota;
      const UniqRegions = union(
        uniqBy(
          excludeRegionsAvaRes.map((region: ClusterInAvailabledResource) => {
            region.display_name =
              region.mirror.display_name || region.mirror.name;
            region.name = region.mirror.name;
            region.mirror.regions = region.mirror.regions.map(
              (cluster: ClusterInAvailabledResource) => ({
                name: cluster.name,
                display_name: cluster.display_name,
                id: cluster.id,
                service_type: 'kubernetes',
              }),
            );
            return region;
          }),
          'mirror.name',
        ),
        normalRegions,
      );
      this.clusters = this.addLimitInfo(UniqRegions, regionsAvaRes);
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.loading = false;
    }
  }

  addLimitInfo(
    regions: ClusterInAvailabledResource[],
    originRegions: ClusterInAvailabledResource[],
  ) {
    const newRegions = cloneDeep(regions) as ClusterWithLimit[];
    newRegions.forEach((region: ClusterWithLimit) => {
      if (region.mirror.regions) {
        region.mirror.regions.forEach(region => {
          region.limitQuota = cloneDeep(
            originRegions.filter(r => (r.name = region.name))[0].quota,
          );
          region.quota = cloneDeep(this.defaultQuota);
        });
        region.mirror.limitQuota = this.getMirrorLimitQuota(
          region.mirror.regions,
        );
        region.mirror.quota = cloneDeep(this.defaultQuota);
      }
      region.limitQuota = cloneDeep(region.quota);
      region.quota = cloneDeep(this.defaultQuota);
    });
    return newRegions;
  }

  getBasicQuotaValue(quota_key: string): any {
    return this.quotaKeys.includes(quota_key) && null;
  }

  submit() {
    this.quotaForm.onSubmit(null);
    if (!this.quotaForm.valid) {
      return;
    }
    const data = cloneDeep(this.data);
    const normalRegions = cloneDeep(this.normalRegions);
    const mirrorRegionsMap = cloneDeep(this.mirrorRegionsMap);
    const mirrorRegions: PreAddRegionData[] = [];
    Object.keys(mirrorRegionsMap).forEach(id => {
      if (this.setSameQuotaFlags[id]) {
        mirrorRegionsMap[id].mirror.regions.forEach(
          (region: any) =>
            (region.quota = cloneDeep(mirrorRegionsMap[id].mirror.quota)),
        );
      }
      mirrorRegions.push(...mirrorRegionsMap[id].mirror.regions);
    });
    data.clusters = [...normalRegions, ...mirrorRegions].map(cluster => ({
      name: cluster.name,
      uuid: cluster.id,
      service_type: cluster.service_type,
      quota: cluster.quota,
    }));

    this.projectService
      .addProjectClusters(this.project_name, data)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('update_success'),
        );
        this.dialogRef.close(true);
      })
      .catch(e => {
        if (e.errors && e.errors[0].code === 'quota_is_insufficient') {
          return this.auiNotificationService.error(
            this.translateService.get(e.errors[0].code) +
              ': ' +
              Object.keys(e.errors[0].fields[0])
                .map(field => this.translateService.get(field))
                .join(', '),
          );
        }
        this.errorToast.error(e);
      });
  }

  keys(obj: Object) {
    return Object.keys(obj);
  }

  getQuotaUnit(type: string) {
    return QuotaUnit[type];
  }

  getQuotaLimitDesc(type: string, region: ClusterWithLimit) {
    const limit = get(region, `limitQuota[${type}]`);
    return limit === undefined || limit === -1
      ? this.translateService.get('unlimited')
      : `${this.translateService.get(
          'can_use',
        )} ${limit} ${this.translateService.get(QuotaUnit[type])}`;
  }

  getMirrorLimitQuota(regions: MirrorCluster[]) {
    return this.quotaKeys.reduce((t, c) => {
      t[c] = Math.min(
        ...regions.map(r => get(r, `limitQuota.${c}`), Number.MAX_SAFE_INTEGER),
      );
      return t;
    }, {});
  }

  selectedRegionChange(region: ClusterWithLimit) {
    this.mirrorRegionsMap = {};
    this.normalRegions = [];
    if (get(region, 'mirror.regions')) {
      const id = region.mirror.id;
      this.mirrorRegionsMap[id] = region;
      this.mirrorHiddenFlags[id] = [];
      this.setSameQuotaFlags[id] = true;
    } else {
      this.normalRegions.push(region);
    }
  }

  clusterQuotaDisplay(cluster: ClusterInAvailabledResource) {
    if (get(cluster, 'mirror.regions')) {
      return `${this.translateService.get(
        'containe',
      )}${cluster.mirror.regions.map(v => v.display_name).join('、')}`;
    } else if (cluster.quota) {
      return this.globalQuotaConfigs.reduce((total, curr, index) => {
        const limit = get(cluster, `limitQuota.${curr.name}`);
        return `${
          index === 0 ? `${total} ` : `${total}, `
        }${this.translateService.get(curr.name)} ${
          limit === undefined || limit === -1
            ? this.translateService.get('unlimited')
            : limit + this.translateService.get(QuotaUnit[curr.name])
        }`;
      }, this.translateService.get('can_assign'));
    }
    return '';
  }

  getMirrorCycleClusters(id: string) {
    return this.setSameQuotaFlags[id]
      ? [this.mirrorRegionsMap[id].mirror]
      : this.mirrorRegionsMap[id].mirror.regions;
  }
}
