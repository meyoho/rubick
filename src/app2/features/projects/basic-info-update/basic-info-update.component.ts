import { DIALOG_DATA, NotificationService } from '@alauda/ui';
import { Component, EventEmitter, Inject, OnInit, Output } from '@angular/core';
import { ProjectService } from 'app/services/api/project.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
@Component({
  selector: 'rc-project-basic-info-update',
  templateUrl: './basic-info-update.component.html',
  styleUrls: ['./basic-info-update.component.scss'],
})
export class ProjectBasicInfoUpdateComponent implements OnInit {
  @Output()
  finished = new EventEmitter<any>();
  data = {
    display_name: '',
    description: '',
  };
  name = '';
  uuid = '';

  constructor(
    private projectService: ProjectService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private errorsToastService: ErrorsToastService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      uuid: string;
      name: string;
      display_name: string;
      description: string;
    },
  ) {}

  ngOnInit(): void {
    this.name = this.dialogData.name;
    this.uuid = this.dialogData.uuid;
    this.data.display_name = this.dialogData.display_name;
    this.data.description = this.dialogData.description;
  }

  submit() {
    this.projectService
      .updateProjectBasicInfo(this.uuid, this.data)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('update_success'),
        );
        this.finished.next(this.data);
      })
      .catch(errors => {
        this.errorsToastService.error(errors);
      });
  }

  cancel() {
    this.finished.next(null);
  }
}
