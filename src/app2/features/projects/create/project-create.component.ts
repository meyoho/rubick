import { NotificationService } from '@alauda/ui';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { cloneDeep, findIndex, get, remove, union, uniqBy } from 'lodash-es';

import {
  ConfigurationApi,
  EnvironmentService,
} from 'app/services/api/environment.service';
import {
  ImageRegistry,
  RegistryPath,
} from 'app/services/api/image-registry.service';
import {
  ClusterInAvailabledResource,
  PreAddRegionData,
  PreSubmitData,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { MirrorCluster } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { AppState } from 'app/store';
import * as projectActions from 'app/store/actions/projects.actions';
import { TranslateService } from 'app/translate/translate.service';
import { Quota, QuotaUnit } from 'app/typings/quota.types';
import { PROJECT_PATTERN, REGISTRY_PATH } from 'app/utils/patterns';

enum CREATE_STEPS {
  BASIC = 1,
  QUOTA = 2,
  MEMBER = 3,
}

interface ClusterInQuotaUpdate extends PreAddRegionData {
  originQuota?: Quota;
}

interface MirrorWithQuota {
  id: string;
  name: string;
  display_name: string;
  description?: string;
  flag: string;
  limitQuota?: Quota;
  regions: {
    name: string;
    display_name: string;
    id?: string;
    quota: any;
    limitQuota?: Quota;
  }[];
}

interface ClusterWithQuotaLimitInfo extends ClusterInAvailabledResource {
  limitQuota?: Quota;
}

@Component({
  selector: 'rc-project-create',
  templateUrl: './project-create.component.html',
  styleUrls: ['./project-create.component.scss'],
})
export class ProjectCreateComponent implements OnInit {
  @ViewChild('basicForm')
  basicForm: NgForm;
  @ViewChild('quotaForm')
  quotaForm: NgForm;
  minQuota: number;
  submitting: boolean;
  data: PreSubmitData = {
    name: '',
    display_name: '',
    description: '',
    clusters: [],
    registerPath: '',
    env: '',
  };
  project_uuid: string;
  regions: ClusterWithQuotaLimitInfo[];
  originalRegions: ClusterWithQuotaLimitInfo[];
  selectedRegions: ClusterInAvailabledResource[] = [];
  normalRegions: PreAddRegionData[] = [];
  mirrorRegions: MirrorWithQuota[] = [];
  curStep = CREATE_STEPS['BASIC'];
  stepsEnum = CREATE_STEPS;
  steps: string[];
  kubernetes = true;
  registryUuid = '';

  sameQuotaMirrorFlags: boolean[] = [];
  normalHiddenFlags: boolean[] = [];
  mirrorHiddenFlagMap: { [id: string]: boolean } = {};

  registries: ImageRegistry[] = [];
  registryPaths: RegistryPath[] = [];
  selectRegistry: ImageRegistry;
  registryPath: string;
  loading = false;
  quotaPlaceholder = '';
  globalQuotaConfigs: ConfigurationApi[] = [];

  projectPatternObj = PROJECT_PATTERN;
  registryPatternObj = REGISTRY_PATH;
  basicQuota: Quota;

  constructor(
    private projectService: ProjectService,
    private auiNotificationService: NotificationService,
    private errorsToast: ErrorsToastService,
    private translateService: TranslateService,
    private router: Router,
    private environmentService: EnvironmentService,
    private store: Store<AppState>,
  ) {}

  get usedQuotaKeys() {
    return this.globalQuotaConfigs.map(v => v.name);
  }

  async ngOnInit() {
    this.loading = true;
    this.quotaPlaceholder = this.translateService.get('unlimited');
    this.steps = ['basic_info', 'set_region_quotas', 'add_member'];
    //register
    try {
      const [regions, globalQuotaConfigs] = await Promise.all([
        this.projectService
          .getProjectAvailableResources()
          .then(({ clusters }) => clusters),
        this.environmentService.getConfigurations('quota'),
      ]);
      this.globalQuotaConfigs = globalQuotaConfigs;
      this.originalRegions = cloneDeep(regions);
      this.loading = false;
      this.minQuota = 0;
      const defaultQuota = this.environmentService.getDefaultQuota('project');
      this.basicQuota = {
        cpu: this.getBasicQuotaValue('cpu'),
        memory: this.getBasicQuotaValue('memory'),
        pvc_num: this.getBasicQuotaValue('pvc_num'),
        pods: this.getBasicQuotaValue('pods'),
        storage: this.getBasicQuotaValue('storage'),
      };
      const normalRegions = remove(
        regions,
        (region: ClusterInAvailabledResource) => !region.mirror.regions,
      );
      const UniqRegions = union(
        uniqBy(
          regions.map((region: ClusterInAvailabledResource) => {
            region.display_name =
              region.mirror.display_name || region.mirror.name;
            region.name = region.mirror.name;
            region.mirror.regions = region.mirror.regions.map(
              (cluster: ClusterInAvailabledResource) => ({
                name: cluster.name,
                display_name: cluster.display_name,
                uuid: cluster.id,
                service_type: 'kubernetes',
                quota: defaultQuota || cloneDeep(this.basicQuota),
              }),
            );
            return region;
          }),
          'mirror.name',
        ),
        normalRegions,
      );
      this.regions = cloneDeep(UniqRegions);
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  getBasicQuotaValue(quota_key: string): any {
    return this.usedQuotaKeys.includes(quota_key) && null;
  }

  stepQuota() {
    this.basicForm.onSubmit(null);
    if (this.basicForm.valid) {
      this.curStep = this.stepsEnum['QUOTA'];
    }
    this.dealWithRegions();
  }

  setHiddenFlag(mirrorName: string, regionName?: string) {
    const id = mirrorName + regionName;
    this.mirrorHiddenFlagMap[id] = !this.mirrorHiddenFlagMap[id];
  }

  getHiddenFlag(mirrorName: string, regionName?: string) {
    return !!this.mirrorHiddenFlagMap[mirrorName + regionName];
  }

  create() {
    if (this.basicForm) {
      this.basicForm.onSubmit(null);
      if (!this.basicForm.valid) {
        return;
      }
    }
    if (this.quotaForm) {
      this.quotaForm.onSubmit(null);
      if (!this.quotaForm.valid) {
        return;
      }
    }
    this.submitting = true;
    const data: PreSubmitData = cloneDeep(this.data);
    let preAddMirrorRegions: PreAddRegionData[] = [];
    this.mirrorRegions.forEach((region: MirrorWithQuota, i) => {
      if (this.sameQuotaMirrorFlags[i]) {
        region.regions.forEach(
          (item: PreAddRegionData) =>
            (item.quota = cloneDeep(region.regions[0].quota)),
        );
      }
      const regions: PreAddRegionData[] = get(region, 'regions');
      preAddMirrorRegions = [...preAddMirrorRegions, ...regions];
    });
    const preAddregions = union(this.normalRegions, preAddMirrorRegions);
    data.clusters = preAddregions;

    this.projectService
      .createProject(data)
      .then((res: Project) => {
        this.store.dispatch(new projectActions.CreateProjectSuccess(res));
        this.auiNotificationService.success(
          this.translateService.get('create_success'),
        );
        this.project_uuid = res.uuid;
        this.curStep = this.stepsEnum['MEMBER'];
      })
      .catch(error => this.errorsToast.error(error))
      .then(() => (this.submitting = false));
  }

  dealWithRegions() {
    const defaultQuota = this.environmentService.getDefaultQuota('project');
    this.normalRegions = [];
    this.mirrorRegions = [];
    this.normalHiddenFlags = [];
    this.mirrorHiddenFlagMap = {};
    this.selectedRegions.forEach((region: ClusterInAvailabledResource) => {
      if (region.mirror.regions) {
        (region.mirror as MirrorWithQuota).regions.forEach(region => {
          region.limitQuota = this.originalRegions.filter(
            r => r.name === region.name,
          )[0].quota;
        });
        (region.mirror as MirrorWithQuota).limitQuota = this.getMirrorLimitQuota(
          region.mirror.regions,
        );
        this.mirrorRegions.push(region.mirror as MirrorWithQuota);
        this.sameQuotaMirrorFlags[
          findIndex(this.mirrorRegions, ['id', region.mirror.id])
        ] = true;
      } else {
        this.normalRegions.push({
          name: region.name,
          display_name: region.display_name,
          uuid: region.id,
          service_type: 'kubernetes',
          quota: defaultQuota || cloneDeep(this.basicQuota),
          limitQuota: region.quota,
        });
      }
    });
  }

  goToDetail() {
    this.router.navigateByUrl(
      `/console/admin/projects/detail/${this.data.name};uuid=${
        this.project_uuid
      }`,
    );
  }

  cancel() {
    this.router.navigateByUrl('/console/admin/projects');
  }

  isNaN(number: number) {
    return isNaN(number);
  }

  clusterQuotaDisplay(cluster: ClusterInAvailabledResource) {
    if (get(cluster, 'mirror.regions')) {
      return `${this.translateService.get(
        'containe',
      )}${cluster.mirror.regions.map(v => v.display_name).join('、')}`;
    } else if (cluster.quota) {
      return this.globalQuotaConfigs.reduce((total, curr, index) => {
        //:yintemp
        return `${
          index === 0 ? `${total} ` : `${total}, `
        }${this.translateService.get(curr.name)} ${
          cluster.quota[curr.name] === undefined ||
          cluster.quota[curr.name] === -1
            ? this.translateService.get('unlimited')
            : cluster.quota[curr.name] +
              this.translateService.get(QuotaUnit[curr.name])
        }`;
      }, this.translateService.get('can_assign'));
    }
    return '';
  }

  getQuotaLimitDesc(type: string, region: ClusterWithQuotaLimitInfo) {
    const limit = get(region, `limitQuota[${type}]`);
    return limit === undefined || limit === -1
      ? this.translateService.get('unlimited')
      : `${this.translateService.get(
          'can_use',
        )} ${limit} ${this.translateService.get(QuotaUnit[type])}`;
  }

  includesQuotaType(type: string) {
    return this.usedQuotaKeys.includes(type);
  }

  getMirrorLimitQuota(regions: MirrorCluster[]) {
    return this.usedQuotaKeys.reduce((t, c) => {
      t[c] = Math.min(
        ...regions.map(r => get(r, `limitQuota.${c}`), Number.MAX_SAFE_INTEGER),
      );
      return t;
    }, {});
  }

  getMirrorQuotaLimitDesc(type: string, regions: ClusterInQuotaUpdate[]) {
    let minNum: number;
    regions.forEach((r, i) => {
      const limitQuota = get(r, `limitQuota.${type}`);
      if (i === 0 || minNum === -1 || limitQuota < minNum) {
        minNum = limitQuota;
      }
    });
    if (minNum === -1) {
      return this.translateService.get('unlimited');
    }
    return `${this.translateService.get(
      'can_use',
    )}${minNum}${this.translateService.get(QuotaUnit[type])}`;
  }

  getQuotaUnit(type: string) {
    return QuotaUnit[type];
  }
}
