import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './delete-project.component.html',
  styleUrls: ['./delete-project.component.scss'],
})
export class DeleteProjectComponent {
  deleteLoading: boolean;
  projectName: string;
  displayName: string;
  inputName = '';
  afterClose: () => Promise<void>;
  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      projectName: string;
      displayName: string;
      afterClose: () => Promise<void>;
    },
    private dialogRef: DialogRef<any>,
    private errorToast: ErrorsToastService,
    private notification: NotificationService,
    private translate: TranslateService,
  ) {
    this.projectName = this.dialogData.projectName;
    this.displayName = this.dialogData.displayName;
    this.afterClose = this.dialogData.afterClose;
  }

  get inputCorrect() {
    return this.inputName && this.inputName === this.projectName;
  }

  async delete() {
    this.deleteLoading = true;
    try {
      await this.afterClose();
      this.dialogRef.close();
    } catch (e) {
      if (e.errors && e.errors[0].code === 'role_is_parent') {
        this.notification.error(
          this.translate.get('role_is_parent', {
            roles: e.errors[0].message.split(':')[1],
          }),
        );
      } else {
        this.errorToast.error(e);
      }
    } finally {
      this.deleteLoading = false;
    }
  }

  cancel() {
    this.dialogRef.close();
  }
}
