import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { get, trim } from 'lodash-es';
import { Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

import { AccountService } from 'app/services/api/account.service';
import { EnvironmentService } from 'app/services/api/environment.service';
import {
  Admin,
  ClusterInProject,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS, WEBLABS } from 'app/shared/tokens';
import { AppState } from 'app/store';
import * as projectActions from 'app/store/actions/projects.actions';
import { TranslateService } from 'app/translate/translate.service';
import { Environments, Weblabs } from 'app/typings';
import { SerieChartData } from 'app/typings/chart.types';
import { VendorCustomer } from 'app/typings/vendor-customer.types';
import { ProjectBasicInfoUpdateComponent } from 'app2/features/projects/basic-info-update/basic-info-update.component';
import { DeleteProjectComponent } from 'app2/features/projects/delete/delete-project.component';
import { QuotaUpdateComponent } from 'app2/features/projects/quota-update/quota-update.component';
import { AddClusterComponent } from '../add-cluster/add-cluster.component';
import { RemoveClusterComponent } from '../remove-cluster/remove-cluster.component';

interface QuotaItem {
  cpu?: {
    max: number;
    used: number;
  };
  memory?: {
    max: number;
    used: number;
  };
  storage?: {
    max: number;
    used: number;
  };
  pvc_num?: {
    max: number;
    used: number;
  };
  pods?: {
    max: number;
    used: number;
  };
}

interface PieData {
  cpu: SerieChartData[];
  memory: SerieChartData[];
  storage: SerieChartData[];
  pvc_num: SerieChartData[];
  pods: SerieChartData[];
}

interface PreAddRegionData extends ClusterInProject {
  service_type?: string;
}

interface PreSubmitData {
  uuid: string;
  name: string;
  display_name: string;
  description: string;
  project_uuid?: string;
  clusters?: PreAddRegionData[];
  updated_at?: string;
  admin_list?: Admin[];
  is_system?: boolean;
  created_by?: string;
  created_at?: string;
  service_type?: string[];
}

@Component({
  selector: 'rc-project-detail',
  templateUrl: './project-detail.component.html',
  styleUrls: ['./project-detail.component.scss'],
})
export class ProjectDetailComponent implements OnInit {
  @Input()
  project: Project;
  isUserView = true;
  updatePermission: boolean;
  deletePermission: boolean;
  namespaceCreatePermission: boolean;
  spaceCreatePermission: boolean;
  canListAudit: boolean;
  canViewToolChain: boolean;
  canViewCredential: boolean;
  emptyArr = new Array(10).fill('');
  data: PreSubmitData = {
    name: '',
    uuid: '',
    description: '',
    display_name: '',
    clusters: [],
    admin_list: [],
  };
  seletedTabIndex = 0;
  unitMap = {
    cpu: this.translateService.get('unit_core'),
    memory: this.translateService.get('unit_gb'),
    pvc_num: this.translateService.get('ge'),
    pods: this.translateService.get('ge'),
    storage: this.translateService.get('unit_gb'),
  };
  clustersMap: any = {
    kubernetes: [],
    PCF: [],
  };
  shouldHiddenQuotas = false;
  curQuota: QuotaItem = {
    cpu: {
      max: 0,
      used: 0,
    },
    memory: {
      max: 0,
      used: 0,
    },
    storage: {
      max: 0,
      used: 0,
    },
    pvc_num: {
      max: 0,
      used: 0,
    },
    pods: {
      max: 0,
      used: 0,
    },
  };
  curPieDataMap: {
    [key: string]: PieData;
  } = {};
  quotaSortQuery = ['cpu', 'memory', 'pods', 'pvc_num', 'storage'];
  pieDataMap: any = {};
  curCluster = { uuid: '', name: '' };
  serviceType: 'kubernetes' | 'PCF' = 'kubernetes';
  paramsSubscription: Subscription;
  initialized: boolean;
  spaceEnabled: boolean;

  constructor(
    private projectService: ProjectService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private errorToast: ErrorsToastService,
    private accountService: AccountService,
    private store: Store<AppState>,
    private dialogService: DialogService,
    private environmentService: EnvironmentService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  initStateParams() {
    this.seletedTabIndex =
      this.activatedRoute.snapshot.queryParams['tabIndex'] || 0;
  }

  onTabIndexChange(index: number) {
    return this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        ...this.activatedRoute.snapshot.queryParams,
        tabIndex: index,
      },
    });
  }

  async ngOnInit() {
    try {
      this.spaceEnabled = this.weblabs.GROUP_ENABLED;
      this.isUserView = this.accountService.isUserView();
      const [canListAudit, globalQuotaConfigs] = await Promise.all([
        this.roleUtil.resourceTypeSupportPermissions('audit', {}, 'list'),
        this.environmentService.getConfigurations('quota'),
      ]);
      this.canListAudit = canListAudit;
      this.canViewToolChain = this.weblabs.DEVOPS_INTEGRATION;
      this.canViewCredential = this.weblabs.DEVOPS_INTEGRATION;
      this.quotaSortQuery = globalQuotaConfigs.map(v => v.name);
      [
        this.updatePermission,
        this.deletePermission,
      ] = await this.roleUtil.resourceTypeSupportPermissions('project', {}, [
        'update',
        'delete',
      ]);
      if (this.isUserView) {
        [
          this.namespaceCreatePermission,
          this.spaceCreatePermission,
        ] = await Promise.all([
          this.roleUtil.resourceTypeSupportPermissions(
            'namespace',
            { project_name: this.project.name },
            'create',
          ),
          this.roleUtil.resourceTypeSupportPermissions(
            'space',
            { project_name: this.project.name },
            'create',
          ),
        ]);
      }
      this.activatedRoute.params.subscribe(async params => {
        this.data.uuid = params['uuid'];
        this.data.name = params['name'];
      });

      await this.refetch();
      this.initStateParams();
      this.initialized = true;
    } catch (e) {
      this.errorToast.error(e);
    }
  }

  async refetch(force: boolean = false) {
    try {
      if (this.project && !force) {
        this.data = this.project;
      } else {
        this.data = await this.projectService.getProject(
          this.data.uuid || this.data.name,
        );
      }

      this.data.clusters = this.data.clusters
        ? await this.projectService
            .getProjectQuota(this.data.name)
            .then(({ clusters }) => clusters)
        : [];

      this.clustersMap = {
        kubernetes: [],
        PCF: [],
      };

      if (!this.data.clusters.length) {
        return;
      }

      this.data.clusters.forEach((region: PreAddRegionData) => {
        switch (region.service_type) {
          case 'kubernetes':
            this.clustersMap.kubernetes.push(region);
            break;
          case 'PCF':
            this.clustersMap.PCF.push(region);
            break;

          default:
            // 更新如果不传service_type该字段会被置空, 后端暂时不好解决
            this.clustersMap.kubernetes.push(region);
            break;
        }

        const regionPie: any = {};
        Object.keys(this.curQuota).forEach(key => {
          regionPie[key] = this.getPieData(
            region.quota[key].used,
            region.quota[key].max,
          );
        });
        this.pieDataMap[region.name] = regionPie;
      });
      this.curCluster = this.clustersMap.kubernetes[0];
      this.curQuota = this.clustersMap.kubernetes[0].quota;
      this.curPieDataMap = this.pieDataMap;
    } catch (error) {
      this.errorToast.error(error);
      if (!this.isUserView) {
        this.router.navigateByUrl('/console/admin/projects');
      }
    }
  }

  regionChange($event: any) {
    this.curQuota = $event.quota;
  }

  keys(obj: object) {
    return Object.keys(obj);
  }

  getPieData(used: number, max: number): SerieChartData[] {
    return [
      {
        name: this.translateService.get('chart_status_USED'),
        value: used,
      },
      {
        name: this.translateService.get('chart_status_UNUSED'),
        value: max - used,
      },
    ];
  }

  showUpdateQuotaDialog(): void {
    const dialogRef = this.dialogService.open(QuotaUpdateComponent, {
      data: {
        project_uuid: this.data.uuid,
        project_name: this.data.name,
        curCluster: this.curCluster.uuid,
        clusters: this.data.clusters,
      },
    });

    dialogRef.afterClosed().subscribe((res: boolean) => {
      if (res) {
        this.refetch();
      }
    });
  }

  showAddClusterDialog() {
    const modelRef = this.dialogService.open(AddClusterComponent, {
      size: DialogSize.Medium,
      data: {
        projectName: this.data.name,
        clusters: this.data.clusters,
      },
    });
    modelRef.afterClosed().subscribe(v => {
      if (v) {
        this.refetch();
      }
    });
  }

  showDeleteDialog() {
    this.dialogService.open(DeleteProjectComponent, {
      data: {
        projectName: this.data.name,
        displayName: this.data.display_name,
        afterClose: () => {
          return this.projectService.deleteProject(this.data.uuid).then(() => {
            this.store.dispatch(
              new projectActions.DeleteProjectSuccess(this.data.name),
            );
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
            this.router.navigateByUrl('/console/admin/projects');
          });
        },
      },
    });
  }

  showRemoveClusterDialog() {
    const dialogRef = this.dialogService.open(RemoveClusterComponent, {
      data: {
        projectName: this.data.name,
        displayName: this.data.display_name,
        clusters: this.data.clusters,
      },
    });

    dialogRef.afterClosed().subscribe(v => {
      if (v) {
        this.refetch();
      }
    });
  }

  showUpdateBasicInfoDialog(): void {
    const dialogRef = this.dialogService.open(ProjectBasicInfoUpdateComponent, {
      data: {
        uuid: this.data.uuid,
        name: this.data.name,
        display_name: this.data.display_name,
        description: this.data.description,
      },
    });

    dialogRef.componentInstance.finished
      .pipe(first())
      .subscribe((res: { display_name: string; description: string }) => {
        dialogRef.close();
        if (res) {
          this.projectService
            .getProject(this.data.uuid)
            .then((res: Project) => {
              this.data.display_name = res.display_name;
              this.data.description = res.description;
              this.data.updated_at = res.updated_at;
            });
        }
      });
  }

  checkCustomer(user: string) {
    return this.environments.vendor_customer === VendorCustomer[user];
  }

  getAdminRolesDisplay(
    roles: { name: string; realname: string }[],
    default_display = '',
  ): string {
    return get(roles, length)
      ? roles
          .map(
            role =>
              `${role.name}${
                role.realname ? '(' + trim(role.realname) + ')' : ''
              }`,
          )
          .join(', ')
      : default_display;
  }
}
