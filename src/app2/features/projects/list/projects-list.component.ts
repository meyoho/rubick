import { Component, Input, OnInit } from '@angular/core';

import { get } from 'lodash-es';

import {
  ConfigurationApi,
  EnvironmentService,
} from 'app/services/api/environment.service';
import {
  Admin,
  ClusterInProject,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { QuotaUnit } from 'app/typings/quota.types';

@Component({
  selector: 'rc-projects-list',
  templateUrl: './projects-list.component.html',
  styleUrls: ['./projects-list.component.scss'],
})
export class ProjectsListComponent implements OnInit {
  loading = false;
  createPermission: boolean;
  listPermission: boolean;

  searching: boolean;
  list: Project[];
  projects: Project[];
  filterString = '';
  pagination = {
    count: 0,
    current: 1,
    size: 20,
    hiddenSize: true,
  };
  searchKey = '';
  globalQuotaConfig: ConfigurationApi[] = [];

  keyword: string;
  //support in cluster detail
  @Input()
  readOnly: boolean;
  @Input()
  clusterUuid = '';

  constructor(
    private projectService: ProjectService,
    private roleUtil: RoleUtilitiesService,
    private errorToast: ErrorsToastService,
    private environmentService: EnvironmentService,
    private translateService: TranslateService,
  ) {}

  get columns() {
    const secCloumn = 'clusters';
    return ['name', secCloumn, 'admin_list', 'created_at'];
  }

  async ngOnInit() {
    try {
      this.loading = true;
      this.globalQuotaConfig = await this.environmentService.getConfigurations(
        'quota',
      );
      [, [this.createPermission, this.listPermission]] = await Promise.all([
        this.fetchProjectList(),
        this.roleUtil.resourceTypeSupportPermissions('project', {}, [
          'create',
          'list',
        ]),
      ]);
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.loading = false;
    }
  }

  searchByKeyword(keyword: string) {
    this.keyword = keyword;
    this.fetchProjectList();
  }

  pageChanged(page: number) {
    this.pagination.current = page;
    this.fetchProjectList();
  }

  sizeChanged(size: number) {
    this.pagination.size = size;
    this.pagination.current = 1;
    this.fetchProjectList();
  }

  async fetchProjectList() {
    try {
      this.searching = true;
      const { results, count } = await this.projectService.getProjects({
        verbose: !!this.clusterUuid,
        search: this.keyword,
        page: this.pagination.current,
        page_size: this.pagination.size,
        cluster_uuids: this.clusterUuid,
      });
      this.getProjectExtraInfoAsync(results.map(r => r.name).join(','));
      this.searching = false;
      this.list = results;
      this.pagination.count = count || results.length;
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.searching = false;
    }
  }

  getProjectExtraInfoAsync(projectNames: string) {
    this.projectService.getProjectsExtra(projectNames).then(extraList => {
      this.list.forEach(project => {
        const matchExtraProject = extraList.filter(
          e => e.name === project.name,
        )[0];
        if (matchExtraProject) {
          project.clusters = matchExtraProject.clusters;
          project.admin_list = matchExtraProject.admin_list;
        }
      });
    });
  }

  showQuotaSetting(type: string) {
    return this.globalQuotaConfig.map(v => v.name).includes(type);
  }

  clusterInfoDisplay(cluster: ClusterInProject) {
    return (
      '<aui-icon icon="basic:plus"></aui-icon>' +
      this.globalQuotaConfig
        .map(quota => {
          return cluster.quota[quota.name];
        })
        .join(', ') +
      `<aui-icon icon="basic:plus"></aui-icon>`
    );
  }

  getClusters(row: { clusters: { display_name: string; name: string }[] }) {
    const clusters = row.clusters || [];
    return clusters
      .map((item: { display_name: string; name: string }) => {
        return item.display_name || item.name;
      })
      .filter(name => !!name)
      .join(', ');
  }

  getServiceType(row: { service_type: string[] }) {
    return row.service_type && row.service_type.length
      ? row.service_type.join(', ')
      : '-';
  }

  getAdminList(row: { admin_list: Admin[] }) {
    if (Array.isArray(row.admin_list) && row.admin_list.length) {
      return row.admin_list
        .map(
          admin =>
            `${admin.name}${admin.realname && '(' + admin.realname + ')'}`,
        )
        .join(', ');
    } else {
      return '-';
    }
  }

  getQuotaLimitDesc(type: string, region: ClusterInProject) {
    const currQuota = get(region, `quota.${type}`);
    return currQuota === 0 || currQuota > 0
      ? currQuota + this.translateService.get(QuotaUnit[type])
      : this.translateService.get('unlimited');
  }

  getQuotaDisplayValues(region: ClusterInProject) {
    const listKeys = ['cpu', 'memory', 'storage'];
    return listKeys.reduce((t, c) => {
      if (this.showQuotaSetting(c)) {
        t[c] = this.getQuotaLimitDesc(c, region);
      }
      return t;
    }, {});
  }

  filterClusters(clusters: ClusterInProject[]) {
    return this.clusterUuid
      ? clusters.filter(c => c.uuid === this.clusterUuid)
      : clusters;
  }
}
