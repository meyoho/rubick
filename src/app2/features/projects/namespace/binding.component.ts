import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { Space } from 'app/services/api/space.service';

@Component({
  templateUrl: './binding.component.html',
})
export class BindingSpacesDialogComponent {
  spaces: Space[];
  value: Space[];
  namepsaceName: string;

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      namepsaceName: string;
      spaces: Space[];
    },
    private dialogRef: DialogRef<BindingSpacesDialogComponent>,
  ) {
    this.namepsaceName = this.dialogData.namepsaceName;
    this.spaces = this.dialogData.spaces;
  }

  binding() {
    this.dialogRef.close(this.value);
  }

  cancel() {
    this.dialogRef.close();
  }
}
