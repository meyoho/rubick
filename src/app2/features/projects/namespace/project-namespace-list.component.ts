import { DialogService, DialogSize } from '@alauda/ui';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { get } from 'lodash-es';
import { isArray } from 'util';

import { AccountService } from 'app/services/api/account.service';
import {
  ConfigurationApi,
  EnvironmentService,
} from 'app/services/api/environment.service';
import { NamespaceService } from 'app/services/api/namespace.service';
import {
  NamespaceWithQuotaInfo,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { Space, SpaceService } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { Account } from 'app/typings';
import { DeleteNamespaceComponent } from 'app_user/features/namespace/delete/component';
import { UpdateQuotaComponent } from 'app_user/features/namespace/update-quota/component';
import { BindingSpacesDialogComponent } from './binding.component';
import { UnbindingSpacesDialogComponent } from './unbinding.component';

interface NamespaceWithQuotaSpacesDetail extends NamespaceWithQuotaInfo {
  name: string;
  uuid: string;
  cluster_name: string;
  cluster_display_name: string;
}
@Component({
  selector: 'rc-project-namespace-list',
  templateUrl: './project-namespace-list.component.html',
  styleUrls: ['./project-namespace-list.component.scss'],
})
export class ProjectNamespacesListComponent implements OnInit {
  columns = ['name', 'quota', 'spaces', 'created_at', 'action'];
  namespaces: NamespaceWithQuotaSpacesDetail[];
  pagination = {
    count: 0,
    current: 1,
    size: 10,
  };
  searchKey: string;
  globalQuotaConfig: ConfigurationApi[];
  createPermission: boolean;
  isUserView: boolean;

  @Input()
  project: Project;

  constructor(
    private errorsToast: ErrorsToastService,
    private dialogService: DialogService,
    private spaceService: SpaceService,
    private environmentService: EnvironmentService,
    private projectService: ProjectService,
    private namespaceService: NamespaceService,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
    private accountService: AccountService,
    private regionService: RegionService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  async ngOnInit() {
    try {
      this.isUserView = this.accountService.isUserView();
      [this.createPermission, this.globalQuotaConfig] = await Promise.all([
        this.roleUtil.resourceTypeSupportPermissions(
          'namespace',
          { project_name: this.project.name },
          'create',
        ),
        this.environmentService.getConfigurations('quota'),
      ]);

      this.fetchNamespaces();
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  async fetchNamespaces() {
    try {
      const params = {
        namespace_name: this.searchKey,
        page: this.pagination.current,
        page_size: this.pagination.size,
      };
      const [
        { results: namespacesQuotaSapces, count },
        userNamespaces,
      ] = await Promise.all([
        this.projectService.getNamespacesWithSpaceInfo(params),
        this.namespaceService.getBatchNamespaceOptions(
          this.project.clusters || [],
          this.project.name,
        ),
      ]);
      this.namespaces = namespacesQuotaSapces.map(ns => {
        const currUserNamespace =
          userNamespaces.filter(uns => uns.uuid === ns.namespace_uuid)[0] || {};
        return Object.assign(
          {},
          currUserNamespace,
          ns,
        ) as NamespaceWithQuotaSpacesDetail;
      });
      this.pagination.count = count;
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  async bindingSpaceDialog(item: NamespaceWithQuotaSpacesDetail) {
    try {
      const { results: spaces } = await this.spaceService.getSpaces();
      const excludesSpaces = spaces.reduce((total, curr) => {
        if (!item.spaces.some(v => v.name === curr.name)) {
          total.push(curr);
        }
        return total;
      }, []);
      const dialogRef = this.dialogService.open(BindingSpacesDialogComponent, {
        data: {
          namepsaceName: item.name,
          spaces: excludesSpaces,
        },
      });
      dialogRef.afterClosed().subscribe(async v => {
        if (v && isArray(v)) {
          await this.projectService.bindSpaceWithNamespace(
            v.map((space: Space) => ({
              namespace_name: item.namespace_uuid,
              namespace_uuid: item.namespace_uuid,
              space_name: space.name,
              space_uuid: space.uuid,
            })),
          );
          await this.fetchNamespaces();
        }
      });
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  async unbindingSpaceDialog(item: NamespaceWithQuotaInfo) {
    try {
      const dialogRef = this.dialogService.open(
        UnbindingSpacesDialogComponent,
        {
          data: {
            namepsaceName: item.namespace_uuid,
            spaces: item.spaces,
          },
        },
      );
      dialogRef.afterClosed().subscribe(async v => {
        if (v && isArray(v)) {
          await this.projectService.unbindSpaceWithNamespace(
            v.map((space: Space) => ({
              namespace_name: item.namespace_uuid,
              namespace_uuid: item.namespace_uuid,
              space_name: space.name,
              space_uuid: space.uuid,
            })),
          );
          await this.fetchNamespaces();
        }
      });
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  onSearch(key: string) {
    this.searchKey = key;
    this.fetchNamespaces();
  }

  pageChanged(num: number) {
    this.pagination.current = num;
    this.fetchNamespaces();
  }

  showQuotaSetting(type: string) {
    return this.globalQuotaConfig.map(v => v.name).includes(type);
  }

  namespaceDetail(namespace: NamespaceWithQuotaSpacesDetail, event: Event) {
    event.stopPropagation();
    this.router.navigate([
      '/console/user/workspace',
      {
        project: this.project.name,
        cluster: namespace.cluster_name,
        namespace: namespace.name,
      },
      'namespace',
    ]);
  }

  async updateNsQuota(item: NamespaceWithQuotaSpacesDetail) {
    try {
      const currCluserInfo = await this.regionService.getCluster(
        item.cluster_name,
      );
      const mirrorClusters =
        currCluserInfo && currCluserInfo.mirror
          ? currCluserInfo.mirror.regions || []
          : [];
      const isMirror = !!mirrorClusters.length;
      let k8sLimitRange = null;
      const promiseArr: Promise<any>[] = !isMirror
        ? [
            this.namespaceService.getNamespaceQuota(
              currCluserInfo.name,
              item.name,
            ),
          ]
        : mirrorClusters.map(c =>
            this.namespaceService.getNamespaceQuota(c.name, item.name),
          );
      const [{ clusters }, ...quotaResources] = await Promise.all([
        this.projectService.getProjectClustersQuota(
          this.project.name,
          !isMirror
            ? [currCluserInfo.id]
            : currCluserInfo.mirror.regions.map(region => region.id),
        ),
        ...promiseArr,
      ]);
      const quotaSettingDate = clusters;

      const limitRangeResource = await this.namespaceService.getNamespacePodQuota(
        item.cluster_name,
        item.name,
      );
      k8sLimitRange = get(limitRangeResource, 'kubernetes.spec.limits[0]');

      this.dialogService.open(UpdateQuotaComponent, {
        size: DialogSize.Large,
        data: {
          projectName: this.project.name,
          clustersQuota: quotaSettingDate,
          namespaceName: item.name,
          clusterName: item.cluster_name,
          mirror: currCluserInfo ? currCluserInfo.mirror : null,
          selfQuotaConfigs: quotaResources.map(qr =>
            get(qr, 'kubernetes.spec.hard'),
          ),
          selfUsedConfigs: quotaResources.map(qr =>
            get(qr, 'kubernetes.status.used'),
          ),
          k8sLimitRange,
          clusterQuotaKeys: this.globalQuotaConfig.map(v => v.name),
          successFun: () => this.fetchNamespaces(),
        },
      });
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  async openDeleteDialog(item: NamespaceWithQuotaSpacesDetail) {
    const [currCluserInfo, nameDetail] = await Promise.all([
      this.regionService.getCluster(item.cluster_name),
      this.namespaceService.getNamespace(item.cluster_name, item.name),
    ]);
    this.dialogService.open(DeleteNamespaceComponent, {
      data: {
        projectName: this.project.name,
        namespace: {
          cluster: currCluserInfo,
          kubernetes: nameDetail.kubernetes,
        },
        successFun: () => this.fetchNamespaces(),
      },
    });
  }
}
