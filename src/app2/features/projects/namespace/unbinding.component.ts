import { ConfirmType, DIALOG_DATA, DialogRef, DialogService } from '@alauda/ui';
import { Component, Inject } from '@angular/core';

import { Space } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './unbinding.component.html',
  styleUrls: ['./unbinding.component.scss'],
})
export class UnbindingSpacesDialogComponent {
  spaces: Space[];
  namepsaceName: string;
  spaceSelected: Space[] = [];

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      namepsaceName: '';
      spaces: [];
    },
    private dialogRef: DialogRef<any>,
    private dialogService: DialogService,
    private translate: TranslateService,
    private errorToast: ErrorsToastService,
  ) {
    this.namepsaceName = this.dialogData.namepsaceName;
    this.spaces = this.dialogData.spaces;
  }

  unbinding() {
    this.dialogService
      .confirm({
        title: this.translate.get('unbinding_spaces_confirm', {
          namespaces: this.spaceSelected.map(v => v.name).join(', '),
        }),
        confirmType: ConfirmType.Primary,
      })
      .then(() => {
        this.dialogRef.close(this.spaceSelected);
      })
      .catch(err => {
        this.errorToast.error(err);
      });
  }

  cancel() {
    this.dialogRef.close();
  }
}
