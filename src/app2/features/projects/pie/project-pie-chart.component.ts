import { Component, Input, OnChanges } from '@angular/core';

import * as Highcharts from 'highcharts';

import { getGaugeChartsOptions } from 'app/features-shared/app/utils/monitor';
import { SerieChartData } from 'app/typings/chart.types';
require('highcharts/highcharts-more')(Highcharts);
require('highcharts/modules/solid-gauge')(Highcharts);

@Component({
  selector: 'rc-project-pie-chart',
  templateUrl: './project-pie-chart.component.html',
  styleUrls: ['./project-pie-chart.component.scss'],
})
export class ProjectPieChartComponent implements OnChanges {
  _data: SerieChartData[] = [];
  pieChartData: SerieChartData[] = [];
  @Input()
  get data() {
    return this._data;
  }
  set data(v) {
    this._data = v;
    this.pieChartData = this.isNegative(v[1].value)
      ? [
          {
            name: v[0].name,
            value: 0,
          },
          {
            name: v[1].name,
            value: 1,
          },
        ]
      : v;
  }
  @Input()
  unit: string;
  @Input()
  title: string;
  Highcharts = Highcharts;
  chartOptions: Highcharts.Options = getGaugeChartsOptions();

  constructor() {}

  ngOnChanges() {
    if (this.data) {
      const newOption: Highcharts.Options = getGaugeChartsOptions();
      newOption.series = [
        {
          name: 'Percentage',
          data: [this.getPercent()],
        },
      ];
      this.chartOptions = newOption;
    }
  }

  getPercent() {
    const data: number = Math.round(
      (this.data[0].value / (this.data[0].value + this.data[1].value)) * 100,
    );
    return this.isNegative(this.data[1].value) ? 0 : data;
  }

  isNegative(num: number) {
    return num < 0;
  }
}
