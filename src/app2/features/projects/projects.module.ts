import { NgModule } from '@angular/core';
import { NamespaceSharedModule } from 'app/features-shared/namespace/shared.module';
import { ProjectSharedModule } from 'app/features-shared/project/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { ProjectBasicInfoUpdateComponent } from 'app2/features/projects/basic-info-update/basic-info-update.component';
import { DeleteProjectComponent } from 'app2/features/projects/delete/delete-project.component';

import { ProjectCreateComponent } from './create/project-create.component';
import { ProjectsRoutingModule } from './projects.routing.module';
import { RemoveClusterComponent } from './remove-cluster/remove-cluster.component';

@NgModule({
  imports: [
    SharedModule,
    ProjectSharedModule,
    ProjectsRoutingModule,
    NamespaceSharedModule,
  ],
  declarations: [
    ProjectCreateComponent,
    ProjectBasicInfoUpdateComponent,
    DeleteProjectComponent,
    RemoveClusterComponent,
  ],
  entryComponents: [
    ProjectBasicInfoUpdateComponent,
    DeleteProjectComponent,
    RemoveClusterComponent,
  ],
})
export class ProjectsModule {}
