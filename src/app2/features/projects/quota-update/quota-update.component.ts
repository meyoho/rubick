import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { cloneDeep, get } from 'lodash-es';

import { EnvironmentService } from 'app/services/api/environment.service';
import {
  PreAddRegionData,
  PreSubmitData,
  ProjectService,
} from 'app/services/api/project.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { Quota, QuotaUnit } from 'app/typings/quota.types';
import { QuotaRange } from 'app_user/features/project/types';

interface ClusterInQuotaUpdate extends PreAddRegionData {
  originQuota?: Quota;
}

interface ClustersDisplayField {
  uuid: string;
  display_name: string;
  service_type: string;
}

@Component({
  selector: 'rc-quota-update',
  templateUrl: './quota-update.component.html',
  styleUrls: ['./quota-update.component.scss'],
})
export class QuotaUpdateComponent implements OnInit {
  @ViewChild('quotaForm')
  quotaForm: NgForm;
  data: PreSubmitData = {
    project_uuid: '',
    name: '',
    service_type: 'kubernetes',
    clusters: [],
  };
  loading = false;
  project_name = '';
  curCluster = '';
  clusters: any[] = [];
  normalRegions: any[] = [];
  mirrorRegionsMap = {};
  setSameQuotaFlags = {};
  mirrorHiddenFlags = {};
  normalHiddenFlags: any[] = [];
  quotaPlaceholder = '';
  quotaKeys: string[] = [];

  clustersDisplayField: ClustersDisplayField[] = [];

  constructor(
    private projectService: ProjectService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private environmentService: EnvironmentService,
    private errorToast: ErrorsToastService,
    private dialogRef: DialogRef,
    @Inject(DIALOG_DATA)
    public dialogData: {
      project_uuid: string;
      project_name: string;
      curCluster: string;
      clusters: any[];
    },
  ) {
    this.data.project_uuid = this.dialogData.project_uuid;
    this.project_name = this.dialogData.project_name;
    this.curCluster = this.dialogData.curCluster;
    this.clusters = this.dialogData.clusters;
  }

  async ngOnInit() {
    try {
      this.loading = true;
      const [quotaKeys, regionsAvaRes] = await Promise.all([
        this.environmentService
          .getConfigurations('quota')
          .then(v => v.map(c => c.name)),
        this.projectService
          .getProjectAvailableResources()
          .then(({ clusters }) => clusters),
      ]);
      this.quotaKeys = quotaKeys;
      this.quotaPlaceholder = this.translateService.get('unlimited');
      this.clustersDisplayField = this.clusters.map(cluster => ({
        display_name: `${cluster.service_type} ${cluster.display_name} (${
          cluster.name
        })`,
        uuid: cluster.uuid,
        service_type: cluster.service_type,
      }));
      this.clusters = this.clusters.map(cluster => ({
        uuid: cluster.uuid,
        name: cluster.name,
        display_name: cluster.display_name,
        service_type: cluster.service_type,
        mirror: get(cluster, 'mirror.name')
          ? {
              id: cluster.mirror.id,
              name: cluster.mirror.name,
              display_name: cluster.mirror.display_name,
            }
          : null,
        quota: this.parpareQuota(cluster.quota),
        originQuota: this.parpareQuota(cluster.quota),
        limitQuota: get(
          regionsAvaRes.filter(r => r.id === cluster.uuid)[0],
          'quota',
        ),
      }));
      this.clusters.forEach(cluster => {
        if (get(cluster, 'mirror')) {
          const id = cluster.mirror.id;
          this.mirrorRegionsMap[id]
            ? this.mirrorRegionsMap[id].push(cluster)
            : (this.mirrorRegionsMap[id] = [cluster]);

          this.mirrorHiddenFlags[id] = [];
          this.setSameQuotaFlags[id] = true;
        } else {
          this.normalRegions.push(cluster);
        }
      });
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.loading = false;
    }
  }

  parpareQuota(origin: QuotaRange) {
    const quota = {};
    this.quotaKeys.forEach(k => {
      const originMax = get(origin, `${k}.max`, 0);
      quota[k] = originMax === -1 ? null : originMax;
    });
    return quota;
  }

  submit() {
    this.quotaForm.onSubmit(null);
    if (!this.quotaForm.valid) {
      return;
    }

    const data = cloneDeep(this.data);
    const normalRegions = cloneDeep(this.normalRegions);
    const mirrorRegionsMap = cloneDeep(this.mirrorRegionsMap);
    const mirrorRegions: any[] = [];
    Object.keys(mirrorRegionsMap).forEach(id => {
      if (this.setSameQuotaFlags[id]) {
        mirrorRegionsMap[id].forEach(
          (region: any) =>
            (region.quota = cloneDeep(mirrorRegionsMap[id][0].quota)),
        );
      }
      mirrorRegions.push(...mirrorRegionsMap[id]);
    });
    data.clusters = [...normalRegions, ...mirrorRegions].map(cluster => ({
      name: cluster.name,
      uuid: cluster.uuid,
      service_type: cluster.service_type,
      quota: cluster.quota,
    }));

    this.projectService
      .updateProjectQuota(this.project_name, data)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('update_success'),
        );
        this.dialogRef.close(true);
      })
      .catch(e => {
        if (e.errors && e.errors[0].code === 'quota_is_insufficient') {
          return this.auiNotificationService.error(
            this.translateService.get(e.errors[0].code) +
              ': ' +
              Object.keys(e.errors[0].fields[0])
                .map(field => this.translateService.get(field))
                .join(', '),
          );
        }
        this.errorToast.error(e);
      });
  }

  cancel() {
    this.dialogRef.close();
  }

  keys(obj: Object) {
    return Object.keys(obj);
  }

  getQuotaUnit(type: string) {
    return QuotaUnit[type];
  }

  getQuotaLimitDesc(type: string, region: ClusterInQuotaUpdate) {
    const originQuota = get(region, `originQuota.${type}`);
    const limitQuota = get(region, `limitQuota.${type}`);
    if (limitQuota === -1) {
      return this.translateService.get('unlimited');
    }
    return `${this.translateService.get('can_use')}${limitQuota +
      originQuota}${this.translateService.get(QuotaUnit[type])}`;
  }

  getMirrorQuotaLimitDesc(type: string, regions: ClusterInQuotaUpdate[]) {
    let minNum: number;
    regions.forEach((r, i) => {
      const originQuota = get(r, `originQuota.${type}`);
      const limitQuota = get(r, `limitQuota.${type}`);
      if (
        i === 0 ||
        minNum === -1 ||
        (typeof originQuota === 'number' && originQuota + limitQuota < minNum)
      ) {
        minNum = limitQuota === -1 ? -1 : originQuota + limitQuota;
      }
    });
    if (minNum === -1) {
      return this.translateService.get('unlimited');
    }
    return `${this.translateService.get(
      'can_use',
    )}${minNum}${this.translateService.get(QuotaUnit[type])}`;
  }
}
