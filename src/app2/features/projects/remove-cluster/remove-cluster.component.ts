import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { Component, Inject, TemplateRef, ViewChild } from '@angular/core';

import clipboard from 'clipboard-polyfill';

import {
  NamespaceOption,
  NamespaceService,
} from 'app/services/api/namespace.service';
import {
  ClusterInProject,
  ProjectService,
} from 'app/services/api/project.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

const KUBECTL_DELETE_NS_PREFIX = 'kubectl delete ns ';

@Component({
  templateUrl: './remove-cluster.component.html',
  styleUrls: ['./remove-cluster.component.scss'],
})
export class RemoveClusterComponent {
  removeLoading = false;
  inputName: string;
  projectName: string;
  displayName: string;
  clustersDisplay: string;
  KubectlDeleteScriptPrefix = KUBECTL_DELETE_NS_PREFIX;
  removeConfirmDialogRef: DialogRef;
  namespaceOpts: NamespaceOption[];
  @ViewChild('removeConfirmDialog')
  removeConfirmDialog: TemplateRef<any>;

  constructor(
    private dialogRef: DialogRef,
    private errorToast: ErrorsToastService,
    private dialogService: DialogService,
    private messageService: MessageService,
    private translateService: TranslateService,
    private namespaceService: NamespaceService,
    private projectService: ProjectService,
    private notification: NotificationService,
    @Inject(DIALOG_DATA)
    public dialogData: {
      projectName: string;
      displayName: string;
      clusters: ClusterInProject[];
    },
  ) {
    this.projectName = this.dialogData.projectName;
    this.displayName = this.dialogData.displayName;
    this.clustersDisplay = this.dialogData.clusters.map(c => c.name).join('、');
  }

  get canRemove() {
    return (
      this.dialogData.clusters.map(c => c.name).indexOf(this.inputName) !== -1
    );
  }

  async confirm() {
    if (!this.inputName) {
      return;
    }
    if (this.dialogData.clusters.length === 1) {
      return this.notification.warning(
        this.translateService.get('project_at_least_one_cluster'),
      );
    }
    try {
      this.removeLoading = true;
      this.namespaceOpts = await this.namespaceService.getNamespaceOptions(
        this.inputName,
        { project_name: this.projectName },
      );
      await this.projectService.removeProjectClusters(this.projectName, [
        this.inputName,
      ]);
      this.dialogRef.close(true);

      if (this.namespaceOpts.length > 0) {
        this.removeConfirmDialogRef = this.dialogService.open(
          this.removeConfirmDialog,
          {
            size: DialogSize.Big,
          },
        );
      }
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.removeLoading = false;
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  copyScript() {
    clipboard
      .writeText(
        this.getKubectlDeleteScript(
          this.namespaceOpts.map(ns => ns.name).join(' '),
        ),
      )
      .then(() => {
        this.messageService.success(
          this.translateService.get('copy_clipboard_success'),
        );
        this.removeConfirmDialogRef.close();
      })
      .catch(() => {
        this.messageService.error(
          this.translateService.get('copy_clipboard_fail'),
        );
      });
  }

  getKubectlDeleteScript(ns: string) {
    return `kubectl delete ns ${ns}`;
  }

  cancelScript() {
    this.removeConfirmDialogRef.close();
  }
}
