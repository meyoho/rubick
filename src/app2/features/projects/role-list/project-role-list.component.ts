import { DialogService } from '@alauda/ui';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { forEach } from 'lodash-es';

import { AccountService } from 'app/services/api/account.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { RoleService } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';

interface Role {
  parents: any[];
  name: string;
  permissions: any[];
}

interface ResourceType {
  privilege?: string;
  resource_actions?: string[];
}

@Component({
  selector: 'rc-project-role-list',
  templateUrl: './project-role-list.component.html',
  styleUrls: ['./project-role-list.component.scss'],
})
export class ProjectRoleListComponent implements OnInit {
  @Input()
  projectName: string;
  isUserView = true;
  initialized: boolean;
  roles: Array<any>;
  pagination: {
    page: number;
    count: number;
    page_size: number;
  };
  popoverConfig: any;
  rolesLoading: boolean;
  search: string;
  searching: boolean;
  roleUsersCache: any = {};

  constructor(
    private router: Router,
    private dialogService: DialogService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private roleService: RoleService,
    private accountService: AccountService,
    private errorHandle: ErrorsToastService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  searched() {}

  async ngOnInit() {
    this.isUserView = this.accountService.isUserView();
    this.initialized = false;
    this.roles = [];
    this.pagination = {
      page: 1,
      count: 0,
      page_size: 20,
    };
    this.popoverConfig = {
      list: [],
      loading: false,
      role_name: '',
      role: null, // used for resource_actions check
      noDataText: this.translate.get('role_has_no_user'),
    };

    this.fetchRoleList().then(() => {
      // rbSafeApply();
      this.initialized = true;
    });
  }

  roleTemplateList() {
    this.router.navigate(['/console/admin/role-template']);
  }

  fetchRoleList() {
    this.rolesLoading = true;
    return this.roleService
      .getRoleList({
        search: this.search,
        page: this.pagination.page,
        page_size: this.pagination.page_size,
        project_name: this.projectName || '',
        ignoreProject: this.projectName ? true : false,
      })
      .then((data: any) => {
        this.roles = data.results;
        this.pagination.count = data.count;
      })
      .catch(e => this.errorHandle.error(e))
      .then(() => {
        this.rolesLoading = false;
      });
  }

  onSearchChanged(value: string) {
    this.search = value;
    this.pagination.page = 1;
    this.searching = true;
    this.fetchRoleList().then(() => {
      this.searching = false;
    });
  }

  pageNoChange(page: number) {
    this.pagination.page = page;
    this.fetchRoleList();
  }

  showRoleDetail(role: Role) {
    this.router.navigate(['/console/admin/rbac/roles/detail', role.name]);
  }

  async deleteRole(role: Role) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_role'),
        content: this.translate.get('delete_role_confirm', {
          name: role.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      this._deleteRole(role);
    } catch (err) {}
  }

  _deleteRole(role: Role) {
    this.roleService
      .deleteRole(role.name)
      .then(() => {
        if (this.roles.length === 1) {
          this.pagination.page = Math.max(this.pagination.page - 1, 1);
        }
        this.fetchRoleList();
      })
      .catch(e => this.errorHandle.error(e));
  }

  fetchRoleUsers(role: Role) {
    const role_name = role.name;
    this.popoverConfig.role = role;
    this.popoverConfig.role_name = role_name;
    this.popoverConfig.list = [];
    this.popoverConfig.loading = true;
    // rbDelay(200);
    this.roleService
      .listRoleUsers({
        page_size: 50,
        role_name,
      })
      .then((data: any) => {
        this.roleUsersCache[role_name] = data.results || [];
        this.popoverConfig.list = this.roleUsersCache[role_name];
      })
      .catch(e => this.errorHandle.error(e))
      .then(() => {
        this.popoverConfig.loading = false;
      });
  }

  deleteUserOfRole(username: string) {
    const users = [
      {
        user: username,
      },
    ];
    const list = this.roleUsersCache[this.popoverConfig.role_name];
    this.popoverConfig.loading = true;
    this.roleService
      .deleteRoleUsers(this.popoverConfig.role_name, users)
      .then(() => {
        forEach(list, (item, index) => {
          if (item.user === username) {
            list.splice(index, 1);
            return false;
          }
        });
      })
      .catch(e => this.errorHandle.error(e))
      .then(() => {
        this.popoverConfig.loading = false;
      });
  }

  buttonDisplayExpr(item: ResourceType, action: string) {
    if (action !== 'get' && this.isUserView) {
      return false;
    }
    return this.roleUtil.resourceHasPermission(item, 'role', action);
  }
}
