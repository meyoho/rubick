import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { Namespace } from 'app/services/api/space.service';

@Component({
  templateUrl: './binding.component.html',
})
export class BindingSpaceDialogComponent {
  namespaces: Namespace[];
  value: Namespace[];
  spaceName = '';

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      spaceName: '';
      namespaces: [];
    },
    private dialogRef: DialogRef<any>,
  ) {
    this.spaceName = this.dialogData.spaceName;
    this.namespaces = this.dialogData.namespaces;
  }

  binding() {
    this.dialogRef.close(this.value);
  }

  cancel() {
    this.dialogRef.close();
  }
}
