import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  NotificationService,
} from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { SpaceService } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

import { NoticeDialogComponent } from './notice.component';

@Component({
  templateUrl: './delete.component.html',
  styleUrls: ['./delete.component.scss'],
})
export class DeleteSpaceDialogComponent {
  @Output()
  close = new EventEmitter<any>();

  spaceName = '';
  envs = '';
  inputValue = '';

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      spaceName: '';
      envs: '';
    },
    private dialogRef: DialogRef<any>,
    private notification: NotificationService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private errorToast: ErrorsToastService,
    private spaceService: SpaceService,
  ) {
    this.spaceName = this.dialogData.spaceName;
    this.envs = this.dialogData.envs;
  }

  get sentences() {
    return {
      space_delete_confirm: this.translate.get('space_delete_confirm', {
        spaceName: this.spaceName,
      }),
      input_space: this.translate.get('input_space', {
        spaceName: this.spaceName,
      }),
    };
  }

  async delete() {
    try {
      await this.spaceService.deleteSpace(this.spaceName);
      this.dialogRef.close(true);
    } catch (e) {
      // TODO:需要接口返回已绑定环境信息
      if (e.errors && e.errors[0].code === 'child_resources_exist') {
        this.dialogService.open(NoticeDialogComponent, {
          data: {
            msg: this.translate.get('delete_space_failed', {
              spaceName: this.spaceName,
              envs: this.envs,
            }),
          },
        });
      } else if (e.errors && e.errors[0].code === 'role_is_parent') {
        this.notification.error(
          this.translate.get('role_is_parent', {
            roles: e.errors[0].message.split(':')[1],
          }),
        );
      } else {
        this.errorToast.error(e);
      }
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  get disabled() {
    return this.inputValue !== this.spaceName;
  }
}
