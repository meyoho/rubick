import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';

@Component({
  templateUrl: './notice.component.html',
  styleUrls: ['./notice.component.scss'],
})
export class NoticeDialogComponent {
  @Output()
  close = new EventEmitter<any>();
  spaceName = '';
  displayName = '';
  msg = '';

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      msg: '';
    },
    private dialogRef: DialogRef,
  ) {
    this.msg = this.dialogData.msg;
  }

  async confirm() {
    this.dialogRef.close();
    this.close.emit();
  }
}
