import { DialogService, DialogSize, MessageService } from '@alauda/ui';
import {
  Component,
  Inject,
  Input,
  OnChanges,
  SimpleChange,
} from '@angular/core';
import { Router } from '@angular/router';

import { get, isArray } from 'lodash-es';
import { BehaviorSubject, Observable, of } from 'rxjs';
import { switchMap } from 'rxjs/operators';

import {
  NamespaceService,
  UserNamespace,
} from 'app/services/api/namespace.service';
import { Project, ProjectService } from 'app/services/api/project.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { Namespace, Space, SpaceService } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { BindingSpaceDialogComponent } from './binding.component';
import { DeleteSpaceDialogComponent } from './delete.component';
import { UnbindingSpaceDialogComponent } from './unbinding.component';
import { UpdateSpaceDialogComponent } from './update.component';

@Component({
  selector: 'rc-project-space-list',
  templateUrl: './project-space-list.component.html',
  styleUrls: ['./project-space-list.component.scss'],
})
export class ProjectSpaceListComponent implements OnChanges {
  columns = ['name', 'namespaces', 'created_at', 'action'];

  spaces$: Observable<any>;
  spacesWrap$: BehaviorSubject<any>;
  searchKey: string;
  searching = false;
  pagination = {
    count: 0,
    current: 1,
    size: 10,
  };
  createPermission: boolean;
  updatePermission: boolean;
  deletePermission: boolean;
  bindNamespacePermission: boolean;
  unbindNamespacePermission: boolean;
  @Input()
  project: Project;

  constructor(
    private errorsToast: ErrorsToastService,
    private dialogService: DialogService,
    private spaceService: SpaceService,
    private projectService: ProjectService,
    private namespaceService: NamespaceService,
    private messageService: MessageService,
    private translate: TranslateService,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
    @Inject(ACCOUNT) public account: Account,
  ) {
    this.spacesWrap$ = new BehaviorSubject([]);
    this.spaces$ = this.spacesWrap$.pipe(switchMap(spaces => spaces));
  }

  onSearch(key: string) {
    this.pagination.current = 1;
    this.searchKey = key;
    this.fetchSpaces();
  }

  async ngOnChanges({ project }: { project: SimpleChange }) {
    if (project.currentValue) {
      this.fetchSpaces();

      [
        this.createPermission,
        this.updatePermission,
        this.deletePermission,
        this.bindNamespacePermission,
        this.unbindNamespacePermission,
      ] = await this.roleUtil.resourceTypeSupportPermissions(
        'space',
        { project_name: this.project.name },
        ['create', 'update', 'delete', 'bind_namespace', 'unbind_namespace'],
      );
    }
  }

  async fetchSpaces() {
    try {
      this.searching = true;
      const params = {
        search: this.searchKey,
        page: this.pagination.current,
        page_size: this.pagination.size,
      };
      const { count, results } = await this.spaceService.getSpaces(params);
      this.spacesWrap$.next(of(results));
      this.pagination.count = count;
    } catch (err) {
      this.errorsToast.error(err);
    } finally {
      this.searching = false;
    }
  }

  async openBindingSpaceDialog(space: Space) {
    try {
      const namespaces: UserNamespace[] = await this.namespaceService.getBatchNamespaceOptions(
        this.project.clusters || [],
        this.project.name,
      );

      const filterExcludedNamespacs = namespaces.filter(ns => {
        return !space.namespaces.some(sns => sns.uuid === ns.uuid);
      });

      const dialogRef = this.dialogService.open(BindingSpaceDialogComponent, {
        size: DialogSize.Medium,
        data: {
          spaceName: space.name,
          namespaces: filterExcludedNamespacs,
        },
      });
      dialogRef.afterClosed().subscribe(async v => {
        if (v && isArray(v)) {
          await this.projectService.bindSpaceWithNamespace(
            v.map((namespace: UserNamespace) => ({
              namespace_name: namespace.name,
              namespace_uuid: namespace.uuid,
              space_name: space.name,
              space_uuid: space.uuid,
            })),
          );
          this.messageService.success(this.translate.get('bind_success'));
          await this.fetchSpaces();
        }
      });
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  openUnbindingSpaceDialog(space: Space) {
    try {
      const dialogRef = this.dialogService.open(UnbindingSpaceDialogComponent, {
        size: DialogSize.Medium,
        data: {
          spaceName: space.name,
          namespaces: space.namespaces,
        },
      });
      dialogRef.afterClosed().subscribe(async v => {
        if (v && isArray(v)) {
          await this.projectService.unbindSpaceWithNamespace(
            v.map((namespace: UserNamespace) => ({
              namespace_name: namespace.name,
              namespace_uuid: namespace.uuid,
              space_name: space.name,
              space_uuid: space.uuid,
            })),
          );
          this.messageService.success(this.translate.get('unbind_success'));
          await this.fetchSpaces();
        }
      });
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  openUpdateSpaceDialog(space: Space) {
    const dialogRef = this.dialogService.open(UpdateSpaceDialogComponent, {
      size: DialogSize.Medium,
      data: {
        spaceName: space.name,
        displayName: space.display_name,
      },
    });

    dialogRef.afterClosed().subscribe(() => {
      this.fetchSpaces();
    });
  }

  openDeleteSpaceDialog(space: Space) {
    const dialogRef = this.dialogService.open(DeleteSpaceDialogComponent, {
      size: DialogSize.Medium,
      data: {
        spaceName: space.name,
        envs: space.namespaces.map(namespace => namespace.name).join(', '),
      },
    });

    dialogRef.afterClosed().subscribe(v => {
      if (v) {
        this.fetchSpaces();
      }
    });
  }

  pageChanged(num: number) {
    this.pagination.current = num;
    this.fetchSpaces();
  }

  namespaceDetail(namespace: Namespace, event: Event) {
    event.stopPropagation();
    const cluster_name = get(
      this.project.clusters.filter(c => {
        return c.uuid === namespace.cluster_uuid;
      })[0],
      'name',
      '',
    );
    this.router.navigate([
      '/console/user/workspace',
      {
        project: this.project.name,
        cluster: cluster_name,
        namespace: namespace.name,
      },
    ]);
  }
}
