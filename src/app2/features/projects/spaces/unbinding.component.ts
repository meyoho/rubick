import { ConfirmType, DIALOG_DATA, DialogRef, DialogService } from '@alauda/ui';
import { Component, Inject } from '@angular/core';

import { Namespace } from 'app/services/api/space.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './unbinding.component.html',
  styleUrls: ['./unbinding.component.scss'],
})
export class UnbindingSpaceDialogComponent {
  spaceName = '';
  namespaces: Namespace[] = [];
  namespaceSelected: Namespace[] = [];

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      spaceName: '';
      namespaces: [];
    },
    private dialogRef: DialogRef<any>,
    private dialogService: DialogService,
    private translate: TranslateService,
  ) {
    this.spaceName = this.dialogData.spaceName;
    this.namespaces = this.dialogData.namespaces;
  }

  unbinding() {
    this.dialogService
      .confirm({
        title: this.translate.get('unbinding_namespaces_confirm', {
          namespaces: this.namespaceSelected
            .map(namespace => namespace.name)
            .join(', '),
        }),
        confirmType: ConfirmType.Primary,
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        this.dialogRef.close(this.namespaceSelected);
      });
  }

  cancel() {
    this.dialogRef.close();
  }
}
