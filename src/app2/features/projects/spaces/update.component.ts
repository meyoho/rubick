import { DIALOG_DATA, DialogRef, MessageService } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';

import { SpaceService } from 'app/services/api/space.service';

import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './update.component.html',
})
export class UpdateSpaceDialogComponent {
  @Output()
  close = new EventEmitter<any>();
  spaceName = '';
  displayName = '';
  loading = false;

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      spaceName: '';
      displayName: '';
    },
    private spaceService: SpaceService,
    private dialogRef: DialogRef,
    private errorsToast: ErrorsToastService,
    private translate: TranslateService,
    private messageService: MessageService,
  ) {
    this.spaceName = this.dialogData.spaceName;
    this.displayName = this.dialogData.displayName;
  }

  async update() {
    try {
      this.loading = true;
      await this.spaceService.updateSpace(this.spaceName, this.displayName);
      this.messageService.success(this.translate.get('update_success'));
      this.dialogRef.close(this.spaceName);
    } catch (err) {
      this.errorsToast.error(err);
    } finally {
      this.loading = false;
    }
  }

  cancel() {
    this.dialogRef.close();
  }
}
