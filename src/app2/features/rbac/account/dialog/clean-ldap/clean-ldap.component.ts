import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  NotificationService,
} from '@alauda/ui';
import {
  Component,
  Inject,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';

import { debounce } from 'lodash-es';

import { RcAccount } from 'app/services/api/account.service';
import { OrgService } from 'app/services/api/org.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';

@Component({
  selector: 'rc-app-clean-ldap',
  templateUrl: './clean-ldap.component.html',
  styleUrls: ['./clean-ldap.component.scss'],
})
export class CleanLdapComponent implements OnInit {
  searchAccounts: Function;
  invalidAccounts: number;
  accounts: Array<RcAccount>;
  count = 0;
  accountsLoading: boolean;
  username: string;
  confirmDialogTitle: string;
  confirmDialogRef: DialogRef;

  @ViewChild('ConfirmTemplate')
  confirmTemplate: TemplateRef<any>;

  constructor(
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private orgService: OrgService,
    private dialogRef: DialogRef,
    private dialogService: DialogService,
    private errorsToastService: ErrorsToastService,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(DIALOG_DATA) public dialogData: any,
  ) {}

  ngOnInit() {
    this.searchAccounts = debounce(this.fetchAccountList, 300);
    this.invalidAccounts = this.dialogData.invalidAccounts || 0;
    this.fetchAccountList();
  }

  async fetchAccountList(search?: string) {
    this.accountsLoading = true;
    try {
      const ret: any = await this.orgService.listOrgFilteredAccounts({
        search: search,
        page_size: 0,
        invalid_ldap: 'true',
      });
      this.accounts = ret.results;
      this.count = ret.count;
    } catch (e) {
      this.accounts = [];
    } finally {
      this.accountsLoading = false;
    }
  }

  async confirm() {
    try {
      const username = this.username;

      await this.orgService.deleteLdapAccounts({
        delete_all: !username,
        usernames: username,
      });

      if (username) {
        this.accounts = this.accounts.filter(
          account => account.username !== username,
        );
        this.invalidAccounts--;
      } else {
        this.accounts = [];
        this.dialogRef.close();
      }

      this.auiNotificationService.success({
        content: this.translate.get(
          username ? 'delete_success' : 'delete_invalid_accounts_success',
          {
            invalid_accounts: this.invalidAccounts,
          },
        ),
      });
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.confirmDialogRef.close();
    }
  }

  async openConfirmDialog(username?: string) {
    this.username = username;
    this.confirmDialogTitle = this.translate.get(
      username
        ? 'confrim_delete_invalid_user_tip'
        : 'confrim_delete_all_invalid_users_tip',
      {
        username,
        invalid_accounts: this.invalidAccounts,
      },
    );
    this.confirmDialogRef = this.dialogService.open(this.confirmTemplate);
  }

  confirmDialogCancel() {
    this.confirmDialogRef.close();
  }

  cancel() {
    this.dialogRef.close();
  }
}
