import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { Component, Inject, Input, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { forEach } from 'lodash-es';

import { RolesAddComponent } from 'app/features-shared/account/roles-add/account-roles-add-dialog.component';
import { AccountUpdatePwdComponent } from 'app/features-shared/account/update-password/component';
import { RcAccount, RcProfile } from 'app/services/api/account.service';
import { AccountService } from 'app/services/api/account.service';
import { OrgService } from 'app/services/api/org.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { RoleService, RoleUser } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT, ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, Environments } from 'app/typings';
import { CleanLdapComponent } from 'app2/features/rbac/account/dialog/clean-ldap/clean-ldap.component';

@Component({
  selector: 'rc-app-account-list',
  templateUrl: './account-list.component.html',
  styleUrls: ['./account-list.component.scss'],
})
export class AccountListComponent implements OnInit {
  @Input()
  project: string;
  isSubAccount: boolean;
  initialized: boolean;
  accounts: Array<RcAccount>;
  ldapConfigTrue: boolean;
  pagination: {
    page: number;
    count: number;
    page_size: number;
  } = {
    page: 1,
    count: 0,
    page_size: 20,
  };
  popoverConfig: any;
  user: any;
  accountsLoading: boolean;
  createAccountEnabled: boolean;
  roleAssignEnabled: boolean;
  ldapSyncEnabled: boolean;
  addUserEnabled: boolean;
  search: string;
  searching: boolean;
  invalid_accounts: number;
  userRolesCache: { [username: string]: RoleUser[] } = {};
  submitting: boolean;
  triggerLdapSyncIng: boolean;
  profile: RcProfile;
  isAdmin: boolean;

  columns = [
    'name',
    'roles',
    'user_type',
    'user_source',
    'status',
    'created_at',
    'action',
  ];

  constructor(
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private router: Router,
    private roleService: RoleService,
    private orgService: OrgService,
    private accountService: AccountService,
    private errorHandle: ErrorsToastService,
    private notificationService: NotificationService,
    private dialogService: DialogService,
    @Inject(ENVIRONMENTS) public environments: Environments,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  async ngOnInit() {
    this.isSubAccount = this.accountService.isSubAccount();
    this.initialized = false;
    this.accounts = [];
    let ldapConfig: any;
    try {
      if (!this.isSubAccount) {
        ldapConfig = await this.orgService.getOrgLdap();
      } else {
        ldapConfig = await this.orgService.getOrgLdapSync();
      }
    } catch (e) {
      ldapConfig = null;
    }
    this.ldapConfigTrue = ldapConfig && !ldapConfig.empty;
    this.popoverConfig = {
      list: [],
      loading: false,
      username: '',
      noDataText: this.translate.get('account_has_no_role'),
    };
    this.user = await this.orgService.getOrgAccount({
      username: this.account.username,
    });

    await this.fetchBasicInfo();
    this.profile = await this.accountService.getAuthProfile();
    this.isAdmin = this.profile.is_admin;
    this.initialized = true;
    //TPAccount ，Third party account type
    this.createAccountEnabled =
      (await this.roleUtil.resourceTypeSupportPermissions('subaccount')) &&
      this.user.type !== 'organizations.TPAccount';
    this.roleAssignEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'role',
      {},
      'assign',
    );
    this.ldapSyncEnabled = await this.roleUtil.resourceTypeSupportPermissions(
      'organization',
      {},
      'sync_users',
    );
    // rbSafeApply();
  }

  async fetchAccountList() {
    this.accountsLoading = true;
    try {
      const { results, count } = (await this.orgService.listOrgFilteredAccounts(
        {
          search: this.search,
          page: this.pagination.page,
          page_size: this.pagination.page_size,
          ignoreProject: true,
        },
      )) as { results?: Array<RcAccount>; count?: number };

      this.accounts = results;

      this.accounts.forEach(account => {
        this.fetchAccountRoles(account.username);
      });

      this.pagination.count = count;
    } catch (e) {
      this.accounts = [];
    } finally {
      this.accountsLoading = false;
    }
  }

  async getOrgLdapInfo() {
    const data = (await this.orgService.getOrgLdapInfo()) as {
      result?: { invalid_accounts: number };
    };
    this.invalid_accounts = data.result.invalid_accounts;
  }

  fetchBasicInfo() {
    let promises: Promise<any> | Array<Promise<any>> = this.fetchAccountList();
    if (!this.isSubAccount) {
      promises = Promise.all([promises, this.getOrgLdapInfo()]);
    }
    return promises;
  }

  onSearchChanged(value: string) {
    this.search = value;
    this.pagination.page = 1;
    this.searching = true;
    this.fetchAccountList().then(() => {
      this.searching = false;
    });
  }

  async deleteInvalidAccounts() {
    const dialogRef = this.dialogService.open(CleanLdapComponent, {
      size: DialogSize.Large,
      data: {
        invalidAccounts: this.invalid_accounts,
      },
    });

    dialogRef.afterClosed().subscribe(() => {
      this.fetchBasicInfo();
    });
  }

  pageNoChange(page: number) {
    this.pagination.page = page;
    this.fetchAccountList();
  }

  sizeChanged(size: number) {
    this.pagination.page_size = size;
    this.pagination.page = 1;
    this.fetchAccountList();
  }

  async updatePassword(item: RcAccount) {
    this.dialogService.open(AccountUpdatePwdComponent, {
      data: {
        username: item.username,
      },
    });
  }

  addRole(account: RcAccount) {
    const dialogRef = this.dialogService.open(RolesAddComponent, {
      fitViewport: true,
      data: {
        username: account.username,
      },
    });

    dialogRef.afterClosed().subscribe((added: boolean) => {
      if (added) {
        this.fetchAccountRoles(account.username);
      }
    });
  }

  async deleteAccount(item: RcAccount) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('confirm_remove_account_from_org', {
          username: item.username,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      this._deleteAccount(item);
    } catch (e) {
      this.errorHandle.error(e);
    }
  }

  async _deleteAccount(item: RcAccount) {
    this.submitting = true;
    try {
      await (this.isInvalidAccount(item)
        ? this.orgService.deleteLdapAccounts({
            usernames: item.username,
          })
        : this.orgService.removeOrgAccount({
            username: item.username,
          }));

      await this.fetchBasicInfo();
    } finally {
      this.submitting = false;
    }
  }

  fetchAccountRoles(username: string) {
    this.orgService
      .getAccountRoles(username)
      .then((data: { result?: any }) => {
        this.userRolesCache[username] = data.result;
      })
      .catch(e => this.errorHandle.error(e));
  }

  showPopover(username: string) {
    this.popoverConfig.username = username;
    this.popoverConfig.list = this.userRolesCache[username];
  }

  async revokeRole(role_name: string) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('rbac_delete_user_role_confirm', {
          role_name: this.popoverConfig.username,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      const users = [
        {
          user: this.popoverConfig.username,
        },
      ];
      const list = this.userRolesCache[this.popoverConfig.username];
      this.popoverConfig.loading = true;
      await this.roleService.deleteRoleUsers(role_name, users);
      forEach(list, (item, index) => {
        if (item.role_name === role_name) {
          list.splice(index, 1);
          return false;
        }
      });
    } catch (e) {
      this.errorHandle.error(e);
    } finally {
      this.popoverConfig.loading = false;
    }
  }

  async createAccounts() {
    this.router.navigate(['/console/admin/rbac/users/create']);
  }

  buttonDisplayExpr(item: any, action: string) {
    const accountTypeTest =
      item.type !== 'organizations.LDAPAccount' ||
      (item.is_valid
        ? !['delete', 'update_password'].includes(action)
        : action === 'delete' && !this.isSubAccount);

    return (
      accountTypeTest &&
      this.roleUtil.resourceHasPermission(item, 'subaccount', action)
    );
  }

  isInvalidAccount(account: any) {
    return account.type === 'organizations.LDAPAccount' && !account.is_valid;
  }

  checkRoleResourceAction(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(item, 'role', action);
  }

  async triggerLdapSync() {
    this.triggerLdapSyncIng = true;
    try {
      await this.orgService.triggerOrgLdapSync({
        orgName: this.account.namespace,
      });
      await this.fetchBasicInfo();
    } catch (e) {
      this.errorHandle.error(e);
    } finally {
      this.triggerLdapSyncIng = false;
    }
  }

  getAccountRoleNameDisplay(item: any) {
    return item.project_name
      ? `${item.project_name}/${item.role_name}`
      : item.role_name;
  }

  viewAccountRoleDetail(item: any, $event: Event) {
    $event.preventDefault();
    this.router.navigate(['/console/admin/rbac/roles/detail', item.role_name]);
  }

  shouldShowRoleLink(item: any) {
    const currentProjectName = window.sessionStorage.getItem('project');
    return (
      !item.project_name ||
      (currentProjectName && currentProjectName === item.project_name)
    );
  }

  shouldShowPwdUpBtn(account: RcAccount) {
    return (
      this.buttonDisplayExpr(account, 'update_password') &&
      account.type !== 'organizations.TPAccount'
    );
  }

  shouldShowDelBtn(account: RcAccount) {
    return (
      this.account.username !== account.username &&
      this.buttonDisplayExpr(account, 'delete') &&
      account.type !== 'organizations.TPAccount'
    );
  }

  shouldDisableMenu(account: RcAccount) {
    return !(
      (this.roleAssignEnabled &&
        (account.type !== 'organizations.LDAPAccount' || account.is_valid)) ||
      this.shouldShowPwdUpBtn(account) ||
      this.shouldShowDelBtn(account)
    );
  }

  async ResetPassword(item: RcAccount) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('reset_password'),
        content: this.translate.get('reset_password_desc', {
          username: item.username,
        }),
        confirmText: this.translate.get('reset'),
        cancelText: this.translate.get('cancel'),
      });
      await this.accountService.resetAccountPassword(item);
      this.notificationService.success(
        this.translate.get('reset_password_success'),
      );
    } catch (e) {
      this.errorHandle.error(e);
    }
  }
}
