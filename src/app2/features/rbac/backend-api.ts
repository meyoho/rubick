import { StringMap } from 'app/typings';

// Ref: http://confluence.alaudatech.com/pages/viewpage.action?pageId=31920356#

export type ResourceClass = 'platform' | 'kubernetes' | 'openshift';
export const PermissionActions = [
  'get',
  'list',
  'proxy',
  'watch',
  'create',
  'update',
  'patch',
  'delete',
  'use',
  'bind',
  'impersonate',
  'deletecollection',
];
export interface PermissionsMap {
  [identifier: string]: RolePermissionItem[];
}
export interface PermissionMap {
  [identifier: string]: RolePermissionItem;
}

export const IDENTIFIER_SEPARATOR = '$';
export const DEFAULT_API_GROUP = 'core';

export interface RoleResource {
  resource_type?: string;
  api_group?: string;
  api_group_source?: string;
  resource_class?: ResourceClass;
  resource_source?: string;
  source?: string;
}

export interface K8sTranslation {
  [api_group: string]: { cn: string; en: string };
}

export function isPlatformResource(resource: RoleResource) {
  return !resource.resource_class || resource.resource_class === 'platform';
}

// identifier: resource_type$class$group
export function toIdentifier(resource: RoleResource): string {
  return isPlatformResource(resource)
    ? resource.resource_type
    : [
        resource.resource_type,
        resource.resource_class,
        getRoleResourceApiGroup(resource),
      ].join(IDENTIFIER_SEPARATOR);
}

export function toPermissionsMap(
  permissions: RolePermissionItem[],
): PermissionsMap {
  return permissions.reduce(
    (accum, permission) => {
      const identifier = toIdentifier(permission);
      if (!accum[identifier]) {
        accum[identifier] = [];
      }
      delete permission.uuid;
      delete permission.template_uuid;
      accum[identifier].push(permission);
      return accum;
    },
    {} as PermissionsMap,
  );
}

export function toPermissionMap(
  permissions: RolePermissionItem[],
): PermissionMap {
  return permissions.reduce(
    (accum, permission) => {
      const identifier = toIdentifier(permission);
      delete permission.uuid;
      delete permission.template_uuid;
      accum[identifier] = permission;
      return accum;
    },
    {} as PermissionMap,
  );
}

export function getRoleResourceApiGroup(resource: RoleResource) {
  if (!isPlatformResource(resource)) {
    return resource.api_group || DEFAULT_API_GROUP;
  }
}

export function toRoleResource(identifier: string): RoleResource {
  const [resource_type, resource_class, api_group] = identifier.split(
    IDENTIFIER_SEPARATOR,
  );
  return {
    resource_type,
    resource_class: resource_class as ResourceClass,
    api_group:
      api_group ||
      (resource_class && resource_class !== 'platform' && DEFAULT_API_GROUP),
  };
}

export interface RolePermissionItemSpec {
  group?: string; // Platform resource only. It's a feature category key.
  resource?: string[];
  actions?: string[];
  constraints?: StringMap[];
  uuid?: string;
  template_uuid?: string;
}

export type RolePermissionItem = RoleResource & RolePermissionItemSpec;

export interface RoleParentRef {
  uuid?: string;
  name?: string;
  assigned_at?: string;
}

export interface Role {
  id?: number;
  created_at?: string;
  created_by?: string;
  name?: string;
  namespace?: string;
  namespace_uuid?: string;
  project_name?: string;
  project_uuid?: string;
  region_id?: string | null;
  resource_actions: string[];
  space_name?: '';
  space_uuid?: '';
  type?: string;
  uuid?: string;
  _state?: string;
}

export interface RoleDetail {
  name?: string;
  uuid?: string;
  namespace?: string;
  created_by?: string;
  created_at?: string;
  updated_at?: string;
  template_uuid?: string;
  resource_actions?: string[];
  admin_role?: boolean;
  parents?: RoleParentRef[];
  permissions?: RolePermissionItem[];
  role_type?: string;
  project_name?: string;
  k8s_namespace_uuid?: string;
  space_uuid?: string;
}

export interface RoleTemplate {
  name?: string;
  uuid?: string;
  created_by?: string;
  created_at?: string;
  updated_at?: string;
  level?: string;
  official?: boolean;
  display_name?: string;
  display_name_en?: string;
  description?: string;
  resource_actions?: string[];
  role_type?: string;
}

export interface ScopeItem {
  actions: string[];
  resource_type: string;
}

export interface RoleTemplateDetail extends RoleTemplate {
  scope?: {
    [key: string]: boolean;
  };
  permissions?: RolePermissionItem[];
}

export interface RoleSchemaSpec {
  actions?: string[];
  constraints?: string[];
  general?: boolean;
}

export type RoleSchema = RoleSchemaSpec & RoleResource;

export interface LinkageMap {
  action: {
    [action: string]: string[];
  };
  resource: {
    [resource: string]: RoleResource[];
  };
}

export interface LegacyPlatformRoleSchema {
  actions?: string[];
  constraints?: StringMap;
  general?: boolean;
  resource?: string;
}

export function adaptLegacyPlatformRoleSchema(
  schema: LegacyPlatformRoleSchema,
): RoleSchema {
  return {
    actions: schema.actions,
    constraints: Object.keys(schema.constraints || {}),
    general: schema.general,

    resource_class: 'platform',
    resource_type: schema.resource,
  };
}

export interface RolePermissionMap {
  resource: {
    [platformResourceType: string]: RoleResource[];
  };
  action: {
    [platformActionName: string]: string[];
  };
}
export interface ResourceTypeFormModel {
  api_group: string;
  resource_type: string;
  actions: string[];
}

export interface RoleApiGroup {
  source: string;
  name: string;
  resource_class: string;
}
