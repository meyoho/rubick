import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { RBACService } from 'app/services/api/rbac.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  selector: 'rc-parent-view',
  templateUrl: './parent-view.component.html',
  styleUrls: ['./parent-view.component.scss'],
})
export class ParentViewComponent implements OnInit {
  role: any = {
    permissions: [],
  };
  initialized = false;

  constructor(
    private translateService: TranslateService,
    private rbacService: RBACService,
    private dialogRef: DialogRef,
    @Inject(DIALOG_DATA) public dialogData: { role_name: string },
  ) {}

  ngOnInit() {
    this.rbacService
      .getRole(this.dialogData.role_name)
      .then(res => {
        this.role = res;
        this.initialized = true;
      })
      .catch(() => {});
  }

  cancel() {
    this.dialogRef.close();
  }

  keys(obj: object) {
    return Object.keys(obj);
  }

  showAllconstraints(constraint: Object) {
    return Object.entries(constraint).map(([key, value]) => {
      return key.split(':')[1] + ': ' + value;
    });
  }

  getActions(actions: string[]) {
    if (actions && actions.length) {
      return actions.map(action => {
        const temp = action.split(':');
        return (
          this.translateService.get(temp[0]) +
          ':' +
          this.translateService.get(temp[1])
        );
      });
    }
    return '-';
  }
}
