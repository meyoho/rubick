// platform groups
export const PLATFORM_GROUPS: string[] = [
  'container',
  'image',
  'cluster',
  'k8s_others',
  'storage',
  'network',
  'app_platform',
  'jenkins',
  'ops_center',
  'platform_management',
  'devops',
];

// 二级，三级导航
export const PLATFORM_GROUPS_MAP = {
  container: {
    application: ['application'],
    configmap: ['configmap'],
  },
  image: {
    repository: ['repository', 'registry', 'registry_project'],
  },
  cluster: {
    cluster: ['cluster', 'cluster_node', 'namespace', 'k8s_resourcequotas'],
  },
  k8s_others: {
    k8s_others: ['k8s_others'],
  },
  storage: {
    persistentvolumeclaim: ['persistentvolumeclaim'],
    persistentvolume: ['persistentvolume'],
    k8s_storageclasses: ['k8s_storageclasses'],
  },
  network: {
    domain: ['domain'],
    load_balancer: ['load_balancer'],
    subnet: ['subnet', 'private_ip'],
    certificate: ['certificate'],
    k8s_networkpolicies: ['k8s_networkpolicies'],
    k8s_service: ['service'],
    ingresses: ['ingress'],
  },
  app_platform: {
    middleware_service: ['public_helm_template_repo', 'public_helm_template'],
    appcatalog: ['helm_template_repo', 'helm_template'],
    integration_center: ['integration'],
  },
  jenkins: {
    jenkins: [
      'jenkins_credential',
      'jenkins_pipeline',
      'jenkins_pipeline_history',
      'jenkins_pipeline_template',
      'jenkins_pipeline_template_repository',
    ],
  },
  ops_center: {
    event_audit: ['event', 'audit'],
    notification_alarm: ['notification', 'alarm', 'alert_template'],
  },
  platform_management: {
    project: ['project'],
    space: ['space'],
    permission_manage: ['role', 'role_template', 'subaccount', 'organization'],
    rbac_view: ['operation_view'],
  },
  devops: {
    devops_pipelineconfig: [
      'devops_pipelineconfig',
      'devops_pipeline_template',
    ],
    devops_secret: ['devops_secret'],
    devops_toolchain: [
      'devops_tool',
      'devops_toolbindingreplica',
      'devops_tool_projects',
    ],
  },
};

export const TRANSLATE_MAP = {
  '*': 'rbac_all',
  update: 'update',
  create: 'create',
  delete: 'delete',
  view: 'view',
};

export const contentTip = {
  zh_cn: `
  资源类型：平台上的各种功能；如：应用、集群、权限管理等<br/>
  操作：可对资源类型的操作；如：创建、更新、触发等<br/>
  约束条件：对资源类型所关联资源的名称进行约束，可使用通配符“*”<br/>
  资源名称：根据资源类型所创建的实例名称，可使用通配符“*”<br/><br/>

  例如：若一个角色拥有如下权限：<br/>
  资源类型：应用<br/>
  操作：查看、更新<br/>
  约束条件：[集群：devcluster] <br/>
  资源名称：dev<br/>
  则该角色拥有如下权限：在名为devcluster的集群上，可查看和更新名为dev的应用`,
  en: `
  Resource type: E.g. application, cluster, permission management, etc.<br/>
  Action: Actions enabled of resource type; e.g. create, update, trigger, etc.<br/>
  Constraints: Constraints of resource type, limited by name, '*' is enabled<br/>
  Resource name: Instance name according to resource type, '*' is enabled<br/><br/>

  E.g. One role like this: <br/>
  Resource type: Application<br/>
  Action: view, update<br/>
  Constraints: [cluster: devcluster] <br/>
  Resource name: dev<br/><br/>
  then this role has permissions: cluster named devcluster，view and update is enabled to application dev`,
};

// 页面状态：已配置，创建，更新
const ORG_LDAP_PAGE_MAP = {
  DISPLAY: 'initial',
  CREATE: 'create',
  UPDATE: 'update',
};

const ORG_LDAP_TYPE_MAP = {
  OpenLDAP: {
    type: 'OpenLDAP',
    config: {
      host: '',
      port: '',
      tls: false,
      service_account: '',
      service_password: '',
      search_base: 'dc=example,dc=org',
      server_cert: '',
      client_key: '',
      domain: '',
    },
    schema: {
      users: {
        class_type: 'inetOrgPerson',
        login_field: 'uid',
        search: '',
      },
    },
    test: {
      username: '',
      password: '',
    },
  },
  ActiveDirectory: {
    type: 'ActiveDirectory',
    config: {
      host: '',
      port: '',
      tls: false,
      service_account: '',
      service_password: '',
      search_base: 'dc=example,dc=org',
      domain: '',
    },
    schema: {
      users: {
        class_type: 'person',
        login_field: 'sAMAccountName',
        search: '',
      },
    },
    test: {
      username: '',
      password: '',
    },
  },
  AzureAD: {
    type: 'AzureAD',
    config: {
      service_account: '',
      service_password: '',
      tenant_id: '',
      country: '',
      domain: '',
    },
    schema: {},
    test: {
      username: '',
      password: '',
    },
  },
};

const ORG_LDAP_STATUS_MAP = {
  bare: 'bare',
  synced: 'synced',
  syncing: 'syncing',
  failure: 'failure',
};

export const ORG_CONSTANTS = {
  ORG_LDAP_PAGE_MAP,
  ORG_LDAP_TYPE_MAP,
  ORG_LDAP_STATUS_MAP,
};

export enum RoleTypes {
  Platform = 'platform',
  Project = 'project',
  Namespace = 'namespace',
  Space = 'space',
}

// 对应翻译文件中的key
export const getRoleTypeLevel = function getRoleType(
  roleType: RoleTypes | string,
  isTemplate: boolean = true,
) {
  if (isTemplate) {
    // 角色模板
    return `rbac_${roleType}_level`;
  } else {
    // 角色
    return `rbac_${roleType}_level_role`;
  }
};

export interface MultipleOption {
  name: string;
  value: string;
}

export const PARENT_PLACHOLDER = {
  [RoleTypes.Platform]: {
    zh_cn: '',
    en: '',
  },
  [RoleTypes.Project]: {
    zh_cn: '请先确定“项目”， 才可以选择父角色',
    en: 'Please determine the "project" before you can select the parent role',
  },
  [RoleTypes.Namespace]: {
    zh_cn: '请先确定“命名空间”， 才可以选择父角色',
    en:
      'Please determine the "namespace" before you can select the parent role',
  },
};

export const getParentPlacholder = function getParentPlacholder(
  roleType: RoleTypes,
  lang: string,
) {
  return PARENT_PLACHOLDER[roleType][lang];
};
