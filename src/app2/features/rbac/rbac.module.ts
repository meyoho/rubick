import { NgModule } from '@angular/core';
import { AccountSharedModule } from 'app/features-shared/account/module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { RbacSharedModule } from 'app/features-shared/rbac/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { CleanLdapComponent } from 'app2/features/rbac/account/dialog/clean-ldap//clean-ldap.component';
import { AccountListComponent } from 'app2/features/rbac/account/list/account-list.component';
import { ParentViewComponent } from 'app2/features/rbac/parent-view/parent-view.component';
import { RoleCreateComponent } from 'app2/features/rbac/roles/create/role-create.component';
import { RoleImportComponent } from 'app2/features/rbac/roles/create/role-template-import.component';
import { AddParentComponent } from 'app2/features/rbac/roles/detail/add-parent/add-parent.component';
import { AddUserComponent } from 'app2/features/rbac/roles/detail/add-user/add-user.component';
import { RoleDetailPermissionComponent } from 'app2/features/rbac/roles/detail/permission/component';
import { RoleDetailComponent } from 'app2/features/rbac/roles/detail/role-detail.component';
import { RoleListComponent } from 'app2/features/rbac/roles/list/role-list.component';
import { RoleUpdateComponent } from 'app2/features/rbac/roles/update/role-update.component';

import { RBACRoutingModule } from 'app2/features/rbac/rbac.routing.module';
import { RbacRolesComponent } from 'app2/features/rbac/roles/roles.component';
import { UserCreatingSuccessComponent } from 'app2/features/rbac/users/creating-success.component';
import { UserCreatingComponent } from 'app2/features/rbac/users/user-create.component';
import { RbacUsersComponent } from 'app2/features/rbac/users/users.component';

@NgModule({
  imports: [
    SharedModule,
    EventSharedModule,
    AccountSharedModule,
    RBACRoutingModule,
    RbacSharedModule,
  ],
  declarations: [
    RbacUsersComponent,
    UserCreatingComponent,
    UserCreatingSuccessComponent,
    RbacRolesComponent,
    RoleCreateComponent,
    RoleDetailComponent,
    RoleDetailPermissionComponent,
    RoleImportComponent,
    ParentViewComponent,
    AccountListComponent,
    CleanLdapComponent,
    AddUserComponent,
    AddParentComponent,
    RoleListComponent,
    RoleUpdateComponent,
  ],
  entryComponents: [
    RoleImportComponent,
    ParentViewComponent,
    AccountListComponent,
    CleanLdapComponent,
    AddUserComponent,
    AddParentComponent,
    UserCreatingSuccessComponent,
  ],
})
export class RBACModule {}
