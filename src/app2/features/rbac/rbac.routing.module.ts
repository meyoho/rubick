import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleCreateComponent } from 'app2/features/rbac/roles/create/role-create.component';
import { RoleDetailComponent } from 'app2/features/rbac/roles/detail/role-detail.component';
import { RbacRolesComponent } from 'app2/features/rbac/roles/roles.component';
import { RoleUpdateComponent } from 'app2/features/rbac/roles/update/role-update.component';
import { UserCreatingComponent } from 'app2/features/rbac/users/user-create.component';
import { RbacUsersComponent } from 'app2/features/rbac/users/users.component';

const routes: Routes = [
  {
    path: 'users',
    children: [
      {
        path: '',
        component: RbacUsersComponent,
      },
      {
        path: 'create',
        component: UserCreatingComponent,
      },
    ],
  },
  {
    path: 'roles',
    children: [
      {
        path: '',
        component: RbacRolesComponent,
      },
      {
        path: 'create',
        component: RoleCreateComponent,
      },
      {
        path: 'detail/:name',
        component: RoleDetailComponent,
      },
      {
        path: 'update/:name',
        component: RoleUpdateComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class RBACRoutingModule {}
