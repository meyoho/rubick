import { NgModule } from '@angular/core';
import { RbacSharedModule } from 'app/features-shared/rbac/shared.module';
import { RoleTemplateRoutingModule } from 'app2/features/rbac/role-template.routing.module';
import { RoleTemplateCreateComponent } from 'app2/features/rbac/role-template/create/role-template-create.component';
import { RoleTemplateDetailComponent } from 'app2/features/rbac/role-template/detail/role-template-detail.component';
import { RoleTemplateListComponent } from 'app2/features/rbac/role-template/list/role-template-list.component';
import { RoleTemplateUpdateComponent } from 'app2/features/rbac/role-template/update/role-template-update.component';

import { SharedModule } from 'app/shared/shared.module';
@NgModule({
  imports: [SharedModule, RbacSharedModule, RoleTemplateRoutingModule],
  declarations: [
    RoleTemplateListComponent,
    RoleTemplateDetailComponent,
    RoleTemplateCreateComponent,
    RoleTemplateUpdateComponent,
  ],
})
export class RoleTemplateModule {}
