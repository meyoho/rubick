import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { RoleTemplateCreateComponent } from 'app2/features/rbac/role-template/create/role-template-create.component';
import { RoleTemplateDetailComponent } from 'app2/features/rbac/role-template/detail/role-template-detail.component';
import { RoleTemplateUpdateComponent } from 'app2/features/rbac/role-template/update/role-template-update.component';

import { RoleTemplateListComponent } from './role-template/list/role-template-list.component';

const routes: Routes = [
  {
    path: '',
    component: RoleTemplateListComponent,
  },
  {
    path: 'detail',
    component: RoleTemplateDetailComponent,
  },
  {
    path: 'create',
    component: RoleTemplateCreateComponent,
  },
  {
    path: 'update/:uuid',
    component: RoleTemplateUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class RoleTemplateRoutingModule {}
