import { DialogService, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { cloneDeep } from 'lodash-es';
import { from } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';

import { TableType } from 'app/features-shared/rbac/permissions-table/constants';
import { RBACService } from 'app/services/api/rbac.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';
import { Account } from 'app/typings';
import {
  RolePermissionItem,
  RoleSchema,
  RoleTemplateDetail,
  ScopeItem,
  toIdentifier,
} from 'app2/features/rbac/backend-api';
import {
  contentTip,
  getRoleTypeLevel,
  RoleTypes,
} from 'app2/features/rbac/rbac-constant';
import { formatPermissions } from 'app2/features/rbac/util';
@Component({
  selector: 'rc-role-template-create',
  templateUrl: './role-template-create.component.html',
  styleUrls: ['./role-template-create.component.scss'],
})
export class RoleTemplateCreateComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;

  getRoleTypeLevel = getRoleTypeLevel;
  RoleTypes = RoleTypes;
  contentTip = contentTip;
  scopes: string[] = [];
  TableType = TableType;

  data: RoleTemplateDetail = {
    name: '',
    display_name: '',
    description: '',
    scope: {},
    permissions: [],
    created_by: '',
    level: '',
  };

  selectedScopes: string[] = [];
  constraintsMap: { [identifier: string]: string[] };
  spaceEnabled: boolean;

  constructor(
    private rbacService: RBACService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private router: Router,
    private route: ActivatedRoute,
    private dialogService: DialogService,
    @Inject(ACCOUNT) public account: Account,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {
    this.data.level = RoleTypes.Platform;
  }

  get templateTooltip() {
    return (
      this.translateService.get('role_template_tooltip_basic') +
      this.translateService.get(
        this.spaceEnabled
          ? 'role_template_tooltip_space_enabled'
          : 'role_template_tooltip_namespace_enabled',
      )
    );
  }

  async ngOnInit() {
    this.spaceEnabled = this.weblabs.GROUP_ENABLED;
    const schemas = await this.rbacService.getRoleSchema();
    this.constraintsMap = schemas.reduce(
      (acc: { [identifer: string]: string[] }, curr: RoleSchema) => {
        acc[toIdentifier(curr)] = curr.constraints;
        return acc;
      },
      {},
    );
    const data = await this.rbacService.getTemplateScopes();
    this.scopes = data.map(({ resource_type }: ScopeItem) => resource_type);

    this.route.queryParams
      .pipe(
        filter(({ uuid }) => !!uuid),
        switchMap(({ uuid }) => from(this.rbacService.getTemplateDetail(uuid))),
      )
      .subscribe((res: RoleTemplateDetail) => {
        res['created_by'] = '';
        delete res.uuid;
        if (res.scope) {
          this.selectedScopes = Object.keys(res.scope);
        } else {
          this.selectedScopes = [];
        }
        res.permissions = formatPermissions(
          res.permissions,
          this.constraintsMap,
          this.selectedScopes,
        );
        this.data = res;
      });
  }

  updatePermissions(permissions: RolePermissionItem[], scopes: string[]) {
    this.data.permissions = formatPermissions(
      permissions,
      this.constraintsMap,
      scopes,
    );
  }

  submit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    const data = cloneDeep(this.data);

    if (!data.permissions.length) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_template_permission_required'),
      );
      return;
    }

    // 提交的时候将constraints置为[]
    data.permissions.forEach(item => {
      item.constraints = [];
    });

    data.created_by = this.account.username;

    data.scope = this.selectedScopes.reduce((scopeMap, curr) => {
      scopeMap[curr] = true;
      return scopeMap;
    }, {});

    delete this.data.created_at;
    this.rbacService
      .createTemplate(data)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('create_success'),
        );
        this.router.navigateByUrl('/console/admin/role-template');
      })
      .catch(errors => {
        this.errorsToastService.error(errors);
      });
  }

  async cancel() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('cancel'),
        content: this.translateService.get('rbac_give_up'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      this.router.navigateByUrl('/console/admin/role-template');
    } catch (rejection) {}
  }

  get lang() {
    return this.translateService.currentLang;
  }
}
