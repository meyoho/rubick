import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TableType } from 'app/features-shared/rbac/permissions-table/constants';
import { RBACService } from 'app/services/api/rbac.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  RolePermissionItem,
  RoleSchema,
  RoleTemplateDetail,
  toIdentifier,
} from 'app2/features/rbac/backend-api';
import {
  TRANSLATE_MAP,
  contentTip,
  getRoleTypeLevel,
} from 'app2/features/rbac/rbac-constant';
import { formatPermissions } from 'app2/features/rbac/util';
import { from } from 'rxjs';
import { filter, switchMap } from 'rxjs/operators';

@Component({
  selector: 'rc-role-template-detail',
  templateUrl: './role-template-detail.component.html',
  styleUrls: ['./role-template-detail.component.scss'],
})
export class RoleTemplateDetailComponent implements OnInit {
  createEnabled = false;
  createRoleEnabled = false;
  updateEnabled = false;
  deleteEnabled = false;
  loading = true;
  expanded: boolean;
  translateMap = TRANSLATE_MAP;
  contentTip = contentTip;
  data: RoleTemplateDetail;
  hasKubernetesPermissions = false;
  permissions: RolePermissionItem[] = [];
  uuid: string;
  TableType = TableType;
  getRoleTypeLevel = getRoleTypeLevel;
  constraintsMap: { [identifier: string]: string[] };

  constraints: string[] = [];

  constructor(
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private rbacService: RBACService,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
  ) {}

  async ngOnInit() {
    const schemas = await this.rbacService.getRoleSchema();
    this.constraintsMap = schemas.reduce(
      (acc: { [identifer: string]: string[] }, curr: RoleSchema) => {
        acc[toIdentifier(curr)] = curr.constraints;
        return acc;
      },
      {},
    );

    this.route.queryParams
      .pipe(
        filter(({ uuid }) => !!uuid),
        switchMap(({ uuid }) => {
          this.uuid = uuid;
          return from(this.rbacService.getTemplateDetail(uuid));
        }),
      )
      .subscribe((res: RoleTemplateDetail) => {
        delete res.uuid;
        this.constraints = Object.keys(res.scope || {}).map(key =>
          this.translateService.get(key),
        );

        // 官方模板的constraints字段自带handlebars语法，无需处理
        if (!res.official) {
          res.permissions = formatPermissions(
            res.permissions,
            this.constraintsMap,
            res.scope ? Object.keys(res.scope) : [],
          );
        }

        this.data = res;
        this.roleUtil
          .resourceTypeSupportPermissions('role_template', {}, [
            'create',
            'update',
            'delete',
          ])
          .then(permissions => {
            this.createEnabled = permissions[0];
            this.updateEnabled = permissions[1] && !this.data.official;
            this.deleteEnabled = permissions[2] && !this.data.official;
          });

        this.roleUtil
          .resourceTypeSupportPermissions('role')
          .then(res => (this.createRoleEnabled = res));
      });
  }

  get lang() {
    return this.translateService.currentLang;
  }

  delete(template_name: string) {
    this.dialogService
      .confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('rbac_delete_template_confirm', {
          template_name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      })
      .then(() => {
        this.rbacService
          .deleteTemplate(this.uuid)
          .then(() => {
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
          })
          .catch(() => {
            this.auiNotificationService.error(
              this.translateService.get('delete_failed'),
            );
          })
          .then(() => {
            this.router.navigateByUrl('/console/admin/role-template');
          });
      })
      .catch(() => {});
  }

  update() {
    this.router.navigate(['/console/admin/role-template/update', this.uuid]);
  }

  duplicate() {
    this.router.navigateByUrl(
      `/console/admin/role-template/create?uuid=${this.uuid}`,
    );
  }

  createRole() {
    this.router.navigateByUrl(
      `/console/admin/rbac/roles/create?uuid=${this.uuid}`,
    );
  }
}
