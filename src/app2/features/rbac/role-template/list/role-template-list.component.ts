import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { RBACService } from 'app/services/api/rbac.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';
import { RoleTemplate } from 'app2/features/rbac/backend-api';

@Component({
  selector: 'rc-role-template-list',
  templateUrl: './role-template-list.component.html',
  styleUrls: ['./role-template-list.component.scss'],
})
export class RoleTemplateListComponent implements OnInit {
  loading = true;
  loadError = false;
  createEnabled = false;
  createRoleEnabled = false;
  updateEnabled = false;
  deleteEnabled = false;
  list: RoleTemplate[];
  constructor(
    private translateService: TranslateService,
    private router: Router,
    private rbacService: RBACService,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private roleUtil: RoleUtilitiesService,
  ) {}

  ngOnInit() {
    this.refetch();
    // get permissions
    this.roleUtil
      .resourceTypeSupportPermissions('role_template', {}, [
        'create',
        'updata',
        'delete',
      ])
      .then(permissions => {
        this.createEnabled = permissions[0];
        this.updateEnabled = permissions[1];
        this.deleteEnabled = permissions[2];
      });
    this.roleUtil
      .resourceTypeSupportPermissions('role')
      .then(res => (this.createRoleEnabled = res));
  }

  refetch() {
    this.rbacService
      .getTemplateList()
      .then(res => {
        this.list = res;
      })
      .catch(() => {
        this.loadError = true;
      })
      .then(() => {
        this.loading = false;
      });
  }

  showDetail(uuid: string) {
    this.router.navigateByUrl(
      `/console/admin/role-template/detail?uuid=${uuid}`,
    );
  }

  create() {
    this.router.navigateByUrl('/console/admin/role-template/create');
  }

  update(uuid: string) {
    this.router.navigate(['/console/admin/role-template/update', uuid]);
  }

  delete(uuid: string, template_name: string) {
    this.dialogService
      .confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('rbac_delete_template_confirm', {
          template_name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      })
      .then(() => {
        this.rbacService
          .deleteTemplate(uuid)
          .then(() => {
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
          })
          .catch(() => {
            this.auiNotificationService.error(
              this.translateService.get('delete_failed'),
            );
          })
          .then(() => {
            this.refetch();
          });
      })
      .catch(() => {});
  }

  duplicate(uuid: string) {
    this.router.navigateByUrl(
      `/console/admin/role-template/create?uuid=${uuid}`,
    );
  }

  createRole(uuid: string) {
    this.router.navigateByUrl(`/console/admin/rbac/roles/create?uuid=${uuid}`);
  }

  get lang() {
    return this.translateService.currentLang;
  }
}
