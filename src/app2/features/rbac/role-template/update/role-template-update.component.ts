import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { cloneDeep } from 'lodash-es';
import { from, of } from 'rxjs';
import { catchError, filter, switchMap } from 'rxjs/operators';

import { TableType } from 'app/features-shared/rbac/permissions-table/constants';
import { RBACService } from 'app/services/api/rbac.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  RoleSchema,
  RoleTemplateDetail,
  ScopeItem,
  toIdentifier,
} from 'app2/features/rbac/backend-api';
import { contentTip, getRoleTypeLevel } from 'app2/features/rbac/rbac-constant';
import { formatPermissions } from '../../util';
@Component({
  selector: 'rc-role-template-update',
  templateUrl: './role-template-update.component.html',
  styleUrls: ['./role-template-update.component.scss'],
})
export class RoleTemplateUpdateComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;

  getRoleTypeLevel = getRoleTypeLevel;
  TableType = TableType;
  contentTip = contentTip;

  data: RoleTemplateDetail = {
    name: '',
    display_name: '',
    description: '',
    scope: {},
    permissions: [],
    uuid: '',
  };
  scopes: string[] = [];
  selectedScopes: string[] = [];
  constraintsMap: { [identifier: string]: string[] };
  uuid: string;

  constructor(
    private errorsToast: ErrorsToastService,
    private rbacService: RBACService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: DialogService,
  ) {}

  async ngOnInit() {
    try {
      const schemas = await this.rbacService.getRoleSchema();
      this.constraintsMap = schemas.reduce(
        (acc: { [identifer: string]: string[] }, curr: RoleSchema) => {
          acc[toIdentifier(curr)] = curr.constraints;
          return acc;
        },
        {},
      );
      const data = await this.rbacService.getTemplateScopes();
      this.scopes = data.map(({ resource_type }: ScopeItem) => resource_type);
    } catch (err) {}

    this.route.params
      .pipe(
        filter(({ uuid }) => !!uuid),
        switchMap(({ uuid }) => {
          this.uuid = uuid;
          return from(this.rbacService.getTemplateDetail(uuid)).pipe(
            catchError(() => of(null)),
          );
        }),
      )
      .subscribe((res: RoleTemplateDetail) => {
        if (res) {
          if (!res.scope) {
            res.scope = {};
          }
          this.selectedScopes = Object.keys(res.scope);

          res.permissions = formatPermissions(
            res.permissions,
            this.constraintsMap,
            this.selectedScopes,
          );

          this.data = res;
        } else {
          this.auiNotificationService.error(
            this.translateService.get('loading_error'),
          );
          this.router.navigateByUrl('/console/admin/role-template');
        }
      });
  }

  updatePermissions() {
    this.data.permissions = formatPermissions(
      this.data.permissions,
      this.constraintsMap,
      this.selectedScopes,
    );
  }

  onScopesChange($event: string[]) {
    const scope = {};
    $event.forEach((key: string) => {
      scope[key] = true;
    });
    this.data.scope = scope;
    this.updatePermissions();
  }

  submit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    const data = cloneDeep(this.data);

    if (!data.permissions.length) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_template_permission_required'),
      );
      return;
    }

    data.permissions.forEach(item => {
      item.constraints = [];
    });

    this.rbacService
      .updateTemplate(data)
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('update_success'),
        );
        this.router.navigateByUrl(
          `/console/admin/role-template/detail?uuid=${this.uuid}`,
        );
      })
      .catch(errors => {
        this.errorsToast.error(errors);
      });
  }

  async cancel() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('cancel'),
        content: this.translateService.get('rbac_give_up'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      this.router.navigateByUrl('/console/admin/role-template');
    } catch (rejection) {}
  }

  get lang() {
    return this.translateService.currentLang;
  }
}
