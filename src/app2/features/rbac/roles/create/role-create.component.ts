import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { cloneDeep } from 'lodash-es';
import { first } from 'rxjs/operators';

import { TableType } from 'app/features-shared/rbac/permissions-table/constants';
import {
  NamespaceService,
  UserNamespace,
} from 'app/services/api/namespace.service';
import {
  NamespaceQuotaWithSpace,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { RBACService } from 'app/services/api/rbac.service';
import { Space, SpaceService } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';
import { RoleDetail, RolePermissionItem } from 'app2/features/rbac/backend-api';
import { ParentViewComponent } from 'app2/features/rbac/parent-view/parent-view.component';
import {
  contentTip,
  getRoleTypeLevel,
  RoleTypes,
} from 'app2/features/rbac/rbac-constant';
import { RoleImportComponent } from './role-template-import.component';
@Component({
  selector: 'rc-role-create',
  templateUrl: './role-create.component.html',
  styleUrls: ['./role-create.component.scss'],
})
export class RoleCreateComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;

  expanded: boolean;
  tableType = TableType.Create;
  contentTip = contentTip;
  RoleTypes = RoleTypes;
  getRoleTypeLevel = getRoleTypeLevel;
  initialized = false;

  data: RoleDetail = {
    name: '',
    parents: [],
    permissions: [],
    role_type: RoleTypes.Platform,
    project_name: '',
    k8s_namespace_uuid: '',
    space_uuid: '',
  };

  projects: Project[];
  projectsLoading: boolean;
  namespaces: UserNamespace[];
  namespacesLoading: boolean;

  currentNamespace: UserNamespace;

  spaceEnabled: boolean;
  spaces: Space[] = [];
  spacesLoading = false;
  k8s_auth_enabled: boolean;

  constructor(
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
    private rbacService: RBACService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private spaceService: SpaceService,
    private projectService: ProjectService,
    private namespaceService: NamespaceService,
    private route: ActivatedRoute,
    @Inject(WEBLABS) private weblabs: Weblabs,
  ) {}

  get templateTooltip() {
    return (
      this.translateService.get('role_template_tooltip_basic') +
      this.translateService.get(
        this.spaceEnabled
          ? 'role_template_tooltip_space_enabled'
          : 'role_template_tooltip_namespace_enabled',
      )
    );
  }

  ngOnInit() {
    this.spaceEnabled = this.weblabs.GROUP_ENABLED;
    this.k8s_auth_enabled = this.weblabs.K8S_AUTH_ENABLED;

    this.projectService
      .getProjects({
        verbose: true,
        page_size: 1000, // get all projects
      })
      .then(({ results }) => {
        this.projectsLoading = false;
        this.projects = results;
      });

    this.route.queryParams.pipe(first()).subscribe(({ uuid }) => {
      if (uuid) {
        this.expanded = true;
        this.import(uuid);
      } else {
        this.expanded = false;
      }
    });
  }

  get parentRolesSelectDisabled() {
    let disabled;
    switch (this.data.role_type) {
      case RoleTypes.Project:
        disabled = !this.data.project_name;
        break;
      case RoleTypes.Namespace:
        disabled = !this.data.k8s_namespace_uuid;
        break;
      default:
        disabled = false;
    }
    return disabled;
  }

  async onProjectChange(project_name: string) {
    const currentProject = this.projects.find(
      project => project.name === project_name,
    );
    if (this.data.role_type === RoleTypes.Namespace) {
      this.currentNamespace = null;
      this.data.k8s_namespace_uuid = '';
      this.namespaces = [];
      if (currentProject) {
        this.fetchNameSpaces(currentProject);
      }
    } else if (this.data.role_type === RoleTypes.Space) {
      this.data.space_uuid = '';
      this.spaces = [];
      this.spacesLoading = true;
      try {
        this.spaces = (await this.spaceService.getSpaces({
          project_name,
        })).results;
      } catch (err) {
        this.errorsToastService.error(err);
      } finally {
        this.spacesLoading = false;
      }
    }
  }

  async fetchNameSpaces(project: Project) {
    try {
      this.namespacesLoading = true;
      const [namespaces, quotas] = await Promise.all([
        this.namespaceService.getBatchNamespaceOptions(
          project.clusters || [],
          project.name,
        ),
        this.projectService.getProjectNamespaceQuotas(project.name),
      ]);

      this.namespaces = namespaces.map((namespace: UserNamespace) => {
        const quota: NamespaceQuotaWithSpace = quotas.find(
          (q: NamespaceQuotaWithSpace) => {
            return q.namespace_uuid === namespace.uuid;
          },
        );
        return quota
          ? Object.assign(namespace, {
              quota: quota.quota,
            })
          : namespace;
      });
    } catch (err) {
    } finally {
      this.namespacesLoading = false;
    }
  }

  onRoleTypeChange() {
    this.data.project_name = '';
    this.data.k8s_namespace_uuid = '';
  }

  resourceNameCell(permission: RolePermissionItem) {
    if (
      permission &&
      permission.resource &&
      !permission.resource.includes('*')
    ) {
      return permission.resource.join(', ');
    } else {
      return this.translateService.get('rbac_any');
    }
  }

  import(uuid: string) {
    const dialogRef = this.dialogService.open(RoleImportComponent, {
      size: DialogSize.Fullscreen,
      // fitViewport: true,
      data: { uuid },
    });
    dialogRef.afterClosed().subscribe(res => {
      if (res) {
        // TODO: 移除template_uuid
        // 现在本来取消使用这个字段的，但是后端没有template_uuid这个字段会报错
        // 暂时赋空字符串
        this.data.template_uuid = '';
        this.data.permissions = res.permissions;
        this.data.role_type = res.level;
        this.auiNotificationService.success(
          this.translateService.get('rbac_import_success'),
        );
        this.expanded = true;
      }
    });
  }

  namespaceChange() {
    this.data.k8s_namespace_uuid = `${this.currentNamespace['cluster_uuid']}:${
      this.currentNamespace.name
    }`;
  }

  submit() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const data = cloneDeep(this.data);

    if (!data.permissions.length) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_at_least_a_permission'),
      );
      return;
    }

    if (data.role_type !== RoleTypes.Namespace) {
      delete data.k8s_namespace_uuid;
    }

    const { project_name, name } = this.data;
    this.rbacService
      .createRole(
        data,
        data.role_type !== RoleTypes.Platform ? { project_name } : undefined,
      )
      .then(() => {
        this.auiNotificationService.success(
          this.translateService.get('create_success'),
        );
        this.router.navigate(['/console/admin/rbac/roles/detail', name]);
      })
      .catch(errors => {
        this.errorsToastService.error(errors);
      });
  }

  showParent(role_name: string) {
    this.dialogService.open(ParentViewComponent, {
      data: {
        role_name,
      },
    });
  }

  async cancel() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('cancel'),
        content: this.translateService.get('rbac_give_up'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      this.router.navigateByUrl('/console/admin/rbac');
    } catch (rejection) {}
  }

  get lang() {
    return this.translateService.currentLang;
  }

  selectFilterFn(key: string, option: { label: string }) {
    return option.label.toLowerCase().includes(key.toLowerCase());
  }
}
