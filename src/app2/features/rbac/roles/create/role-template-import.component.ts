import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { TableType } from 'app/features-shared/rbac/permissions-table/constants';
import { RBACService } from 'app/services/api/rbac.service';
import { TranslateService } from 'app/translate/translate.service';
import { RBAC_CONSTRAINT_PATTERN } from 'app/utils/patterns';
import {
  RolePermissionItem,
  RoleResource,
  RoleSchema,
  RoleTemplate,
  toIdentifier,
} from 'app2/features/rbac/backend-api';
import { formatPermissions } from 'app2/features/rbac/util';
import { cloneDeep, orderBy } from 'lodash-es';
import moment from 'moment';
@Component({
  templateUrl: './role-template-import.component.html',
  styleUrls: ['./role-template-import.component.scss'],
})
export class RoleImportComponent implements OnInit {
  @ViewChild('form')
  form: any;

  @Output()
  finished = new EventEmitter<{
    template_uuid: string;
    permissions: RolePermissionItem[];
    level: string;
  }>();

  TableType = TableType;
  RBAC_CONSTRAINT_PATTERN = RBAC_CONSTRAINT_PATTERN;
  loading = true;
  title = '';
  description = '';
  scope: { key: string; val: string }[];

  tabIndex: number;
  tabList: RoleTemplate[];
  curUuid = '';

  level = '';

  hasKubernetesPermissions = false;
  permissions: RolePermissionItem[] = [];

  constraintsMap: { [identifier: string]: string[] };

  constructor(
    private rbacService: RBACService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private cdr: ChangeDetectorRef,
    private dialogRef: DialogRef,
    private dialogService: DialogService,
    @Inject(DIALOG_DATA) public dialogData: { uuid: string },
  ) {}

  async ngOnInit() {
    const schemas = await this.rbacService.getRoleSchema();
    this.constraintsMap = schemas.reduce(
      (acc: { [identifer: string]: string[] }, curr: RoleSchema) => {
        acc[toIdentifier(curr)] = curr.constraints;
        return acc;
      },
      {},
    );

    try {
      const res = await this.rbacService.getTemplateList();
      this.tabList = orderBy(
        res,
        [item => moment.utc(item.updated_at).valueOf()],
        'desc',
      );
    } catch (err) {
      console.error('Failed to load ajax data for role-template-import');
    }

    this.loading = false;

    if (this.dialogData.uuid) {
      const index = this.tabList.findIndex(tab => {
        return tab.uuid === this.dialogData.uuid;
      });

      this.tabChanged(index);
    }
  }

  async tabChanged(index: number) {
    if (index === undefined || this.tabIndex === index) {
      return;
    }

    const dirty = Object.keys(this.form.value).some(
      key => !!this.form.value[key],
    );
    if (dirty) {
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('rbac_switch'),
          content: this.translateService.get('rbac_switch_confirm'),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });
      } catch {
        return;
      }
    }
    this.loading = true;
    this.tabIndex = index;
    if (this.tabList[index].uuid) {
      this.curUuid = this.tabList[index].uuid;
      // TODO:更新permission
      try {
        const res = await this.rbacService.getTemplateDetail(
          this.tabList[index].uuid,
        );

        // set title & description
        this.title = this.lang.includes('en')
          ? res.display_name_en
          : res.display_name;

        this.title = this.title || res.name;
        this.description = res.description;
        // set scope
        if (res.scope && Object.keys(res.scope).length) {
          this.scope = Object.keys(res.scope).map(scope => ({
            key: scope,
            val: '',
          }));
        } else {
          this.scope = [];
        }
        this.level = res.level;

        if (!res.official) {
          this.permissions = formatPermissions(
            res.permissions,
            this.constraintsMap,
            res.scope ? Object.keys(res.scope) : [],
          );
        } else {
          this.permissions = res.permissions;
        }
        // Hack: Force Angular to re-draw rc-table.
        this.cdr.detectChanges();
      } catch (err) {
        console.error('Failed to load template for tab', err.message);
      }
    }
  }

  get lang() {
    return this.translateService.currentLang;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid && this.scope.length > 0) {
      return;
    }
    const scopeMap = {};
    this.scope.forEach(item => {
      scopeMap[item.key + '_name'] = item.val;
    });
    const reg = /([\{]{2})([^\{\}]*)([\}]{2})/g;
    const permissions: RolePermissionItem[] = cloneDeep(this.permissions);
    try {
      permissions.forEach(permission => {
        if (permission.constraints) {
          permission.constraints = permission.constraints.map(constraint => {
            const str = JSON.stringify(constraint);
            return JSON.parse(
              str.replace(reg, (_match, _$1, $2) => scopeMap[$2]),
            );
          });
        }
        permission.resource = permission.resource.map(resource => {
          return resource.replace(reg, (_match, _$1, $2) => scopeMap[$2]);
        });
      });
    } catch (err) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_parse_failed'),
      );
      return;
    }

    // dry-run
    const error = await this.rbacService
      .templateGenerate(this.tabList[this.tabIndex].uuid)
      .then(() => null)
      .catch(({ errors }) => errors[0]);
    if (error && error.code === 'permissions_partially_accepted') {
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('rbac_confirm_continue_import'),
          content: this.translateService.get(
            'rbac_permission_partially_denied',
          ),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });
        error.permissions.forEach((permission: RoleResource) => {
          if (permission !== null) {
            for (let i = 0; i < permissions.length; i++) {
              if (toIdentifier(permissions[i]) === toIdentifier(permission)) {
                permissions.splice(i, 1);
                break;
              }
            }
          }
        });
      } catch (rejection) {
        return;
      }
    } else if (error) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_import_failed'),
      );
      return;
    }
    this.dialogRef.close({
      template_uuid: this.curUuid,
      permissions: permissions,
      level: this.level,
    });
  }

  async cancel() {
    if (this.tabIndex !== undefined || this.form.dirty) {
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('cancel'),
          content: this.translateService.get('rbac_give_up'),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });
        this.dialogRef.close();
      } catch (rejection) {}
    } else {
      this.dialogRef.close();
    }
  }
}
