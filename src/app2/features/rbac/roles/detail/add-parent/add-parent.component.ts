import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { RBACService } from 'app/services/api/rbac.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { Role } from 'app2/features/rbac/backend-api';
import { RoleParentRef } from 'app2/features/rbac/backend-api';
@Component({
  selector: 'rc-add-parent',
  templateUrl: './add-parent.component.html',
  styleUrls: ['./add-parent.component.scss'],
})
export class AddParentComponent implements OnInit {
  roles: Role[] = [];
  loading = true;
  checkMap = {};
  lastParent: Role;
  size = 10;
  page = 1;
  total = 0;

  constructor(
    private rbacService: RBACService,
    private translateService: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToast: ErrorsToastService,
    private dialogRef: DialogRef,
    @Inject(DIALOG_DATA)
    public dialogData: { role_name: string; parents: RoleParentRef[] },
  ) {}

  ngOnInit() {
    this.fetchRoles();
  }

  pageChange(page: { pageIndex: number }) {
    this.page = page.pageIndex + 1;
    this.fetchRoles();
  }

  searched(value: string) {
    this.fetchRoles(value);
  }

  async fetchRoles(value?: string) {
    this.loading = true;
    try {
      const { results, count } = (await this.rbacService.getRoles(
        this.size,
        this.page,
        value,
      )) as { results: Role[]; count: number };
      this.roles = results.map(role => {
        if (this.dialogData.parents.some(parent => parent.uuid === role.uuid)) {
          role['disabled'] = true;
        } else {
          role['disabled'] = false;
        }
        return role;
      });

      this.total = count;
      this.lastParent = null;
    } catch (err) {
      this.errorsToast.error(err);
    }
    this.loading = false;
  }

  onParentChecked(checked: any, parent: Role) {
    if (checked) {
      if (this.lastParent) {
        this.checkMap[this.lastParent.uuid] = false;
      }
      this.lastParent = parent;
    } else {
      this.lastParent = null;
    }
  }

  confirm() {
    if (!this.lastParent.uuid) {
      this.auiNotificationService.error(
        this.translateService.get('rbac_parent_required'),
      );
      this.loading = false;
      return;
    }
    this.loading = true;
    this.rbacService
      .addParent(this.dialogData.role_name, {
        name: this.lastParent.name,
        uuid: this.lastParent.uuid,
      })
      .then(() => {
        this.dialogRef.close(true);
      })
      .catch(err => {
        this.errorsToast.error(err);
      })
      .then(() => {
        this.loading = false;
      });
  }

  cancel() {
    this.dialogRef.close();
  }
}
