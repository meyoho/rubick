import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { AccountType } from 'app/services/api/account.service';
import { OrgService } from 'app/services/api/org.service';
import { RBACService } from 'app/services/api/rbac.service';
import { RoleType } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
interface PostDataItem {
  user: string;
  disabled: boolean;
}

interface UserType {
  checked: boolean;
  disabled: boolean;
  username: string;
  roles: RoleType[];
}

@Component({
  selector: 'rc-add-user',
  templateUrl: './add-user.component.html',
  styleUrls: ['./add-user.component.scss'],
})
export class AddUserComponent implements OnInit {
  users: UserType[] = [];
  initialized = false;
  pagination = {
    count: 0,
    current: 1,
    size: 10,
    hiddenSize: true,
  };
  keyword = '';
  checkedUserList: PostDataItem[] = [];
  rolesCache: { [username: string]: AccountType }[] = [];

  get allChecked(): boolean {
    return this.users.every(user => {
      return this.checkedUserList.some(currentUser => {
        return currentUser.user === user.username;
      });
    });
  }

  get confirmDisabled(): boolean {
    return this.checkedUserList.filter(user => !user.disabled).length === 0;
  }

  constructor(
    private orgService: OrgService,
    private rbacService: RBACService,
    private dialogRef: DialogRef,
    private errorsToast: ErrorsToastService,
    @Inject(DIALOG_DATA) public dialogData: { role_name: string },
  ) {}

  ngOnInit() {
    this.fetchUserlist();
  }

  // We can't invoked this function directly in pageChanged function
  // which will cause the search button to flicker
  searched(value: string) {
    this.pagination.current = 1;
    this.keyword = value;
    this.fetchUserlist(value);
  }

  isInvalidAccount(account: any) {
    return account.type === 'organizations.LDAPAccount' && !account.is_valid;
  }

  pageChanged(num: number) {
    this.pagination.current = num;
    this.fetchUserlist(this.keyword);
  }

  onCheckAll($event: boolean) {
    this.checkedUserList = [];
    this.users.forEach(user => {
      if (!user.disabled) {
        this.onCheck($event, user);
      }
    });
  }

  onCheck($event: boolean, user: UserType) {
    user.checked = $event;
    if ($event) {
      this.checkedUserList.push({ user: user.username, disabled: false });
    } else {
      this.checkedUserList = this.checkedUserList.filter(anotherUser => {
        return anotherUser.user !== user.username;
      });
    }
  }

  async confirm() {
    try {
      await this.rbacService.assignUser(
        this.dialogData.role_name,
        this.checkedUserList
          .filter(currentUser => {
            return currentUser.disabled === false;
          })
          .map(currenUser => {
            return { user: currenUser.user };
          }),
      );
      this.dialogRef.close(true);
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  cancel() {
    this.dialogRef.close();
  }

  async fetchUserlist(value: string = '') {
    const params = {
      search: value,
      page: this.pagination.current,
      page_size: this.pagination.size,
    };

    try {
      const response = (await this.orgService.listOrgAccounts(params)) as {
        results?: UserType[];
        count?: number;
      };
      this.pagination.count = response.count;
      this.users = response.results;

      // cache roles
      const usernames: string[] = [];
      const promises = this.users.reduce((accu, curr) => {
        const { username } = curr;
        if (!this.rolesCache[username]) {
          usernames.push(username);
          accu.push(this.orgService.getAccountRoles(username));
        }
        return accu;
      }, []);
      (await Promise.all(promises)).forEach(({ result }, index) => {
        this.rolesCache[usernames[index]] = result || [];
      });

      this.users.forEach(user => {
        // 与当前角色具有相同角色的用户需要disabled掉
        const belongToCurrentRole = this.rolesCache[user.username].some(
          (role: AccountType) => {
            return role.role_name === this.dialogData.role_name;
          },
        );
        const checked = this.checkedUserList.some(currentUser => {
          return currentUser.user === user.username;
        });
        if (belongToCurrentRole) {
          user.checked = user.disabled = true;
          if (!checked) {
            this.checkedUserList.push({ user: user.username, disabled: true });
          }
        } else {
          user.disabled = false;
          user.checked = checked;
        }
      });

      this.initialized = true;
    } catch (err) {
      this.errorsToast.error(err);
    }
  }
}
