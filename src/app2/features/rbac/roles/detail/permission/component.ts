import { NotificationService } from '@alauda/ui';
import { Component, Input, OnInit } from '@angular/core';
import { TableType } from 'app/features-shared/rbac/permissions-table/constants';
import { RBACService } from 'app/services/api/rbac.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { RolePermissionItem } from 'app2/features/rbac/backend-api';

import { RoleDetailComponent } from '../role-detail.component';
@Component({
  selector: 'rc-role-detail-permission',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class RoleDetailPermissionComponent implements OnInit {
  @Input()
  permissions: RolePermissionItem[];

  type = TableType.Edit;

  constructor(
    private errorToast: ErrorsToastService,
    public parent: RoleDetailComponent,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private rbacService: RBACService,
  ) {}

  async ngOnInit() {}

  get roleDetail() {
    return this.parent.roleDetail;
  }

  permissionsChange() {
    this.submit();
  }

  async submit() {
    this.roleDetail.permissions = this.permissions;
    try {
      await this.rbacService.updateRole(this.roleDetail, this.roleDetail.name);
      this.auiNotificationService.success(
        this.translateService.get('update_success'),
      );
    } catch (err) {
      this.errorToast.error(err);
    }
  }
}
