import { DialogService, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';

import { RBACService } from 'app/services/api/rbac.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import {
  RoleService,
  RoleSyncEvent,
  RoleUser,
} from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';
import { RoleDetail, RoleParentRef } from 'app2/features/rbac/backend-api';
import { RolePermissionItem } from 'app2/features/rbac/backend-api';
import { ParentViewComponent } from 'app2/features/rbac/parent-view/parent-view.component';
import { getRoleTypeLevel, RoleTypes } from 'app2/features/rbac/rbac-constant';
import { AddParentComponent } from 'app2/features/rbac/roles/detail/add-parent/add-parent.component';
import { AddUserComponent } from 'app2/features/rbac/roles/detail/add-user/add-user.component';
@Component({
  selector: 'rc-role-detail',
  templateUrl: './role-detail.component.html',
  styleUrls: ['./role-detail.component.scss'],
})
export class RoleDetailComponent implements OnInit {
  roleUpdatePermission: boolean;
  roleAssignPermission: boolean;
  roleRevokePermission: boolean;

  getRoleTypeLevel = getRoleTypeLevel;
  RoleTypes = RoleTypes;

  translateTabs = {
    permission: this.translateService.get('permission'),
    parent: this.translateService.get('parent'),
  };

  initialized = false;
  loading = true;
  currentUserPage = 1;
  userCount = 0;
  users: RoleUser[] = [];

  roleDetail: RoleDetail = {};
  parents: RoleParentRef[];
  permissions: RolePermissionItem[] = [];
  roleSyncStatus: string;
  k8s_auth_enabled: boolean;

  constructor(
    private errorsToastService: ErrorsToastService,
    private translateService: TranslateService,
    private rbacService: RBACService,
    private route: ActivatedRoute,
    private dialogService: DialogService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
    private roleService: RoleService,
    @Inject(WEBLABS) private weblabs: Weblabs,
  ) {}

  async ngOnInit() {
    this.k8s_auth_enabled = this.weblabs.K8S_AUTH_ENABLED;
    [
      this.roleUpdatePermission,
      this.roleAssignPermission,
      this.roleRevokePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions('role', {}, [
      'update',
      'assign',
      'revoke',
    ]);
    this.route.params.subscribe(async ({ name }) => {
      this.roleDetail.name = name;
      await this.getRole();
      if (this.k8s_auth_enabled) {
        this.roleService
          .getRoleSyncEvents([this.roleDetail.uuid])
          .then((res: RoleSyncEvent[]) => {
            this.roleSyncStatus =
              res[0] && res[0].events.length
                ? res[0].events.findIndex(_event => {
                    return _event.status === 'failed';
                  }) > -1
                  ? 'failed'
                  : 'pending'
                : undefined;
          });
      }
      await this.pageChanged(1);
      this.initialized = true;
    });
  }

  async getRole() {
    try {
      const res: RoleDetail = await this.rbacService.getRole(
        this.roleDetail.name,
      );

      this.roleDetail = res;

      if (!this.k8s_auth_enabled) {
        this.parents = res.parents;
      }

      this.permissions = res.permissions;
    } catch (errors) {
      this.errorsToastService.error(errors);
      this.router.navigateByUrl('/console/admin/rbac');
    }
  }

  keys(object: object) {
    return Object.keys(object || {});
  }

  showParent(role_name: string) {
    this.dialogService.open(ParentViewComponent, {
      data: {
        role_name,
      },
    });
  }

  async removeParent(role_name: string, uuid: string) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('confirm'),
        content: this.translateService.get('rbac_delete_parent_confirm', {
          role_name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      await this.rbacService.deleteParent(this.roleDetail.name, uuid);
      this.parents = this.parents.filter(parent => parent.name !== role_name);
      this.auiNotificationService.success(
        this.translateService.get('delete_success'),
      );
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  addParent() {
    const dialogRef = this.dialogService.open(AddParentComponent, {
      data: {
        role_name: this.roleDetail.name,
        parents: this.parents || [],
      },
    });

    dialogRef
      .afterClosed()
      .pipe(first())
      .subscribe((res: boolean) => {
        if (res) {
          this.getRole();
        }
      });
  }

  async republish() {
    await this.roleService.roleSyncRetry([this.roleDetail.uuid]);
    this.getRole();
  }

  update() {
    this.router.navigate([
      '/console/admin/rbac/roles/update',
      this.roleDetail.name,
    ]);
  }

  pageChanged($event: any, search: string = '') {
    this.loading = true;
    this.currentUserPage = $event;
    this.rbacService
      .getUsers(this.roleDetail.name, search, $event)
      .then((res: any) => {
        this.userCount = res.count;
        this.users = res.results || [];
      })
      .catch(errors => {
        this.errorsToastService.error(errors);
      })
      .then(() => {
        this.loading = false;
      });
  }

  removeUser(user: string) {
    this.dialogService
      .confirm({
        title: this.translateService.get('remove'),
        content: this.translateService.get('delete_user_role_confirm', {
          name: user,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      })
      .then(() => {
        this.rbacService
          .deleteUser(this.roleDetail.name, [{ user }])
          .then(() => {
            this.auiNotificationService.success(
              this.translateService.get('delete_success'),
            );
            this.pageChanged(1);
          })
          .catch(errors => {
            this.errorsToastService.error(errors);
          });
      })
      .catch(() => {});
  }

  async addUsers() {
    const dialogRef = this.dialogService.open(AddUserComponent, {
      data: {
        role_name: this.roleDetail.name,
      },
    });
    dialogRef.afterClosed().subscribe((res: boolean) => {
      if (res) {
        this.pageChanged(1);
      }
    });
  }

  search($event: any) {
    this.pageChanged(1, $event);
  }

  async showDetail(role_name: string) {
    this.router.navigate(['/console/admin/rbac/roles/detail', role_name]);
  }
}
