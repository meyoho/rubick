import { DialogService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { RBACService } from 'app/services/api/rbac.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import {
  Role,
  RoleResourceList,
  RoleService,
  RoleSyncEvent,
  RoleSyncStatics,
  RoleUser,
} from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';
import { RoleDetail } from 'app2/features/rbac/backend-api';
import { ResourceList } from 'app_user/core/types';

interface PopoverConfig {
  list: RoleUser[];
  role_name: string;
  role: Role;
  noDataText: string;
}
@Component({
  selector: 'rc-role-list',
  templateUrl: './role-list.component.html',
  styleUrls: ['./role-list.component.scss'],
})
export class RoleListComponent extends BaseResourceListComponent<Role>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  initialized: boolean;
  popoverConfig: PopoverConfig;
  createRoleEnabled: boolean;
  hasTemplatePermission: boolean;
  roleUsersCache: {
    [key: string]: RoleUser[];
  } = {};
  totalAbnormal: RoleSyncStatics[] = [];
  keyword: string;

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private roleService: RoleService,
    private errorHandle: ErrorsToastService,
    private notification: NotificationService,
    private dialogService: DialogService,
    private rbacService: RBACService,
    @Inject(WEBLABS) private weblabs: Weblabs,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    this.initialized = false;
    this.popoverConfig = {
      list: [],
      role_name: '',
      role: null, // used for resource_actions check
      noDataText: this.translate.get('role_has_no_user'),
    };
    this.roleUtil
      .resourceTypeSupportPermissions('role')
      .then(res => (this.createRoleEnabled = res));
    this.roleUtil
      .resourceTypeSupportPermissions('role_template', {}, 'list')
      .then(res => (this.hasTemplatePermission = res));
    //
    super.ngOnInit();
    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      this.initialized = !!list.length;
    });
  }

  getPollInterval() {
    return 30000;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  fetchResources(params: FetchParams): Observable<RoleResourceList> {
    return from(
      this.weblabs.K8S_AUTH_ENABLED
        ? this.getResources(params)
        : this.roleService
            .getRoleList({
              page: params.pageParams.pageIndex,
              page_size: params.pageParams.pageSize,
              search: params.search,
            })
            .then((res: RoleResourceList) => {
              // 获取角色下的用户
              res.results.forEach(role => {
                this.fetchRoleUsers(role.name);
              });
              return res;
            }),
    );
  }

  async getResources(params: FetchParams): Promise<RoleResourceList> {
    // 异步，不阻塞获取Role列表
    this.roleService.getRoleSyncStatics().then((res: RoleSyncStatics[]) => {
      this.totalAbnormal = res;
    });
    const roleRes: RoleResourceList = await this.roleService.getRoleList({
      page: params.pageParams.pageIndex,
      page_size: params.pageParams.pageSize,
      search: params.search,
    });
    // 取单前页所有role ids
    const roleIds = roleRes.results.map((role: Role) => {
      return role.uuid;
    });

    // 获取角色下的用户
    roleRes.results.forEach(role => {
      this.fetchRoleUsers(role.name);
    });

    try {
      // 获取单前页所有role 的同步状态
      const syncEvent: RoleSyncEvent[] = await this.roleService.getRoleSyncEvents(
        roleIds,
      );

      if (syncEvent.length) {
        roleRes.results.forEach((role: Role) => {
          const event = syncEvent.find(_event => {
            return _event.role_uuid === role.uuid;
          });
          if (event) {
            const failedIndex = event.events.findIndex(_event => {
              return _event.status === 'failed';
            });
            role.sync_status = failedIndex > -1 ? 'failed' : 'pending';
          }
        });
      }
    } finally {
      // 不管获取同步状态成功与否，都能返回role list
      return roleRes;
    }
  }

  pageChanged(pageIndex: number) {
    this.onPageEvent({
      pageIndex: pageIndex,
    });
  }

  pageSizeChanged(size: number) {
    this.onPageEvent({
      pageIndex: 1,
      pageSize: size,
    });
  }

  roleTemplateList() {
    this.router.navigate(['/console/admin/role-template']);
  }

  createRole() {
    this.router.navigateByUrl('/console/admin/rbac/roles/create');
  }

  showRoleDetail(role: Role) {
    this.router.navigate(['/console/admin/rbac/roles/detail', role.name]);
  }

  async deleteRole(role: Role) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_role'),
        content: this.translate.get('delete_role_confirm', {
          name: role.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      await this.roleService.deleteRole(role.name);
      this.notification.success(
        this.translate.get('delete_role_success', {
          roleName: role.name,
        }),
      );
      this.onUpdate(null);
    } catch (e) {
      const error = e.errors[0];
      if (error && error.code === 'role_is_parent') {
        this.notification.error(
          this.translate.get('role_is_parent', {
            roles: error.message.split(':')[1],
          }),
        );
      } else {
        this.errorHandle.error(e);
      }
    }
  }

  showUsersPopover(role: Role) {
    this.popoverConfig.role = role;
    this.popoverConfig.role_name = role.name;
    this.popoverConfig.list = this.roleUsersCache[role.name];
  }

  async fetchRoleUsers(role_name: string) {
    try {
      const data: ResourceList<RoleUser> = await this.roleService.listRoleUsers(
        {
          page_size: 50,
          role_name,
        },
      );
      this.roleUsersCache[role_name] = data.results || [];
    } catch (e) {
      this.errorHandle.error(e);
    }
  }

  async deleteUserOfRole(username: string) {
    const users = [
      {
        user: username,
      },
    ];
    const list = this.roleUsersCache[this.popoverConfig.role_name];

    try {
      await this.roleService.deleteRoleUsers(
        this.popoverConfig.role_name,
        users,
      );
      list.forEach((item: RoleUser, index: number) => {
        if (item.user === username) {
          list.splice(index, 1);
          return false;
        }
      });
    } catch (e) {
      this.errorHandle.error(e);
    }
  }

  async checkDeletePermission(role: { name: string; canDelete: boolean }) {
    const roleDetail: RoleDetail = await this.rbacService.getRole(role.name);
    if (this.roleUtil.resourceHasPermission(roleDetail, 'role', 'delete')) {
      role.canDelete = true;
    }
  }
}
