import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { TableType } from 'app/features-shared/rbac/permissions-table/constants';
import { RBACService } from 'app/services/api/rbac.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { RoleDetail } from 'app2/features/rbac/backend-api';
import { contentTip } from 'app2/features/rbac/rbac-constant';
import { RoleTypes, getRoleTypeLevel } from 'app2/features/rbac/rbac-constant';
@Component({
  selector: 'rc-role-update',
  templateUrl: './role-update.component.html',
  styleUrls: ['./role-update.component.scss'],
})
export class RoleUpdateComponent implements OnInit {
  contentTip = contentTip;
  TableType = TableType;
  initialized: boolean;
  roleDetail: RoleDetail = {};
  roleName: string;

  getRoleTypeLevel = getRoleTypeLevel;
  RoleTypes = RoleTypes;

  constructor(
    private dialogService: DialogService,
    private errorsToast: ErrorsToastService,
    private translateService: TranslateService,
    private rbacService: RBACService,
    private route: ActivatedRoute,
    private auiNotificationService: NotificationService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.route.params.subscribe(async ({ name }) => {
      this.initialized = false;
      this.roleName = name;
      try {
        this.roleDetail = await this.rbacService.getRole(name);
      } catch (err) {
        this.errorsToast.error(err);
        this.router.navigateByUrl('/console/admin/rbac');
      }
      this.initialized = true;
    });
  }

  async update() {
    try {
      await this.rbacService.updateRole(this.roleDetail, this.roleName);
      this.auiNotificationService.success(
        this.translateService.get('update_success'),
      );
      this.router.navigate(['/console/admin/rbac/roles/detail', this.roleName]);
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  async cancel() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('cancel'),
        content: this.translateService.get('rbac_give_up'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
      this.router.navigate(['/console/admin/rbac/roles/detail', this.roleName]);
    } catch (rejection) {}
  }

  get lang() {
    return this.translateService.currentLang;
  }
}
