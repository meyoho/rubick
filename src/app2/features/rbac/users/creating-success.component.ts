import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { RcRole } from 'app/services/api/role.service';

@Component({
  templateUrl: './creating-success.component.html',
})
export class UserCreatingSuccessComponent {
  users: Array<RcRole> = [];

  constructor(
    private dialogRef: DialogRef,
    private router: Router,
    @Inject(DIALOG_DATA)
    private data: {
      users: RcRole[];
    },
  ) {
    this.users = this.data.users || [];
  }

  ok() {
    this.dialogRef.close();
    this.router.navigate(['/console/admin/rbac/users']);
  }
}
