import { DialogService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { pick } from 'lodash-es';

import { RcProfile } from 'app/services/api/account.service';
import { AccountService } from 'app/services/api/account.service';
import { OrgService } from 'app/services/api/org.service';
import { RcRole, RoleService } from 'app/services/api/role.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT, ENVIRONMENTS, WEBLABS } from 'app/shared/tokens';
import { Account, Environments, Weblabs } from 'app/typings';
import {
  ACCOUNT_EMAIL_PATTERN,
  ACCOUNT_MOBILE_PATTERN,
  RBAC_PASSWORD_PATTERN,
  RBAC_USERNAME_PATTERN,
} from 'app/utils/patterns';
import { UserCreatingSuccessComponent } from 'app2/features/rbac/users/creating-success.component';

enum PasswordMode {
  Custom,
  Random,
}

@Component({
  templateUrl: './user-create.component.html',
  styleUrls: ['./user-create.component.scss'],
})
export class UserCreatingComponent implements OnInit {
  selectedRoles: Array<RcRole> = [];
  privateDeploy: boolean;
  profile: RcProfile;
  username = '';
  changePasswordErrorMapper: any;
  password = '';
  randomPassword = false;
  showMoreInfo = false;
  passwordView = false;
  initialized: boolean;
  submitting: boolean;
  office_address: string;
  position: string;
  department: string;
  mobile: string;
  email: string;
  landline_phone: string;
  realname: string;
  usernamePattern = RBAC_USERNAME_PATTERN;
  passwordPattern = RBAC_PASSWORD_PATTERN;
  emailPattern = ACCOUNT_EMAIL_PATTERN;
  mobilePattern = ACCOUNT_MOBILE_PATTERN;

  users: Array<RcRole> = [];
  allRoles: Array<RcRole> = [];
  @ViewChild('accountCreateForm')
  form: NgForm;
  PasswordMode = PasswordMode;
  password_mode: PasswordMode = PasswordMode.Custom;

  constructor(
    private orgService: OrgService,
    private roleService: RoleService,
    private accountService: AccountService,
    private errorHandle: ErrorsToastService,
    private dialogService: DialogService,
    private router: Router,
    @Inject(ACCOUNT) public account: Account,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {}

  async ngOnInit() {
    try {
      this.privateDeploy = this.environments.is_private_deploy_enabled;
      this.profile = await this.accountService.getAuthProfile();
      this.allRoles = await this.roleService
        .getRoleList({ page_size: 0, ignoreProject: true })
        .then(({ results }) => results);
    } catch (err) {
      this.errorHandle.error(err);
    } finally {
      this.initialized = true;
    }
  }

  passwordModeChange(target: PasswordMode) {
    this.password = '';
    this.passwordView = target === this.PasswordMode.Random;
  }

  responseItemFilter(option: RcRole) {
    return !this.selectedRoles.find(
      (item: RcRole) => item.uuid === option.uuid,
    );
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    try {
      const data: any = await this.orgService.createRoleBasedAccounts({
        username: this.username,
        ignoreProject: true,
        roles: this.selectedRoles.map((item: RcRole) => {
          return {
            name: item.name,
            uuid: item.uuid,
          };
        }),
        password:
          this.password_mode === PasswordMode.Random ? '' : this.password,
        ...pick(
          this,
          'department',
          'position',
          'mobile',
          'email',
          'landline_phone',
          'office_address',
          'realname',
        ),
      });

      if (this.passwordView) {
        this.users = data.result;
        this.dialogService.open(UserCreatingSuccessComponent, {
          data: {
            users: this.users,
          },
        });
      } else {
        this.router.navigate(['/console/admin/rbac/users']);
      }
    } catch (e) {
      this.errorHandle.error(e);
    } finally {
      this.submitting = false;
    }
  }

  cancel() {
    this.router.navigate(['/console/admin/rbac/users']);
  }

  selectFilterFn(key: string, option: { label: string }) {
    return option.label.toLowerCase().includes(key.toLowerCase());
  }
}
