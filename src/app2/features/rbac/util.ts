import {
  RolePermissionItem,
  toIdentifier,
} from 'app2/features/rbac/backend-api';

// 将constraint处理成handlebars形式
export const formatConstraints = function formatPermissions(
  constraints: string[],
  scopes: string[],
) {
  const temp = {};
  let empty = true;

  scopes.forEach(scope => {
    const key = `res:${scope}`;

    if (constraints.includes(key) && !temp[key]) {
      if (empty) {
        empty = false;
      }
      temp[key] = `{{${scope}_name}}`;
    }
  });

  return empty ? [] : [temp];
};

export const formatPermissions = function formatPermissions(
  permissions: RolePermissionItem[],
  constraintsMap: { [identifier: string]: string[] },
  scopes: string[],
) {
  return permissions.map(permission => {
    const constraints = constraintsMap[toIdentifier(permission)];
    permission.constraints = formatConstraints(constraints, scopes);
    return permission;
  });
};
