import { Component } from '@angular/core';

import { ActivatedRoute } from '@angular/router';

@Component({
  templateUrl: './create-page.component.html',
})
export class ResourceCreatePageComponent {
  constructor(private activatedRoute: ActivatedRoute) {}

  get isUpdate() {
    return this.activatedRoute.snapshot.routeConfig.path === 'update';
  }
}
