import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  templateUrl: './list-page.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ResourceListPageComponent {}
