import { NgModule } from '@angular/core';
import { ResourceManageSharedModule } from 'app/features-shared/resource-manage/module';
import { SharedModule } from 'app/shared/shared.module';
import { ResourceManageRoutingModule } from 'app2/features/resource-manage/routing.module';

import { ResourceCreatePageComponent } from './create-page.component';
import { ResourceListPageComponent } from './list-page.component';

@NgModule({
  declarations: [ResourceListPageComponent, ResourceCreatePageComponent],
  imports: [
    SharedModule,
    ResourceManageSharedModule,
    ResourceManageRoutingModule,
  ],
})
export class ResourceManageModule {}
