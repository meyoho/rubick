import { DIALOG_DATA, NotificationService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { RBACService } from 'app/services/api/rbac.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';

@Component({
  templateUrl: 'template.html',
})
export class ApiGroupFormComponent {
  @Output()
  close = new EventEmitter();
  name: string;
  @ViewChild('Form')
  form: NgForm;
  oldName: string;
  confirming: boolean;

  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;

  constructor(
    @Inject(DIALOG_DATA)
    private modalData: {
      name: string;
    },
    private rbacService: RBACService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private errorsToastService: ErrorsToastService,
  ) {
    this.oldName = this.name = this.modalData.name || '';
  }

  cancel() {
    this.close.next(null);
  }

  async confirm() {
    this.confirming = true;
    try {
      if (this.oldName) {
        const payload = { name: this.name };
        await this.rbacService.updateApiGroup(this.oldName, payload);
        this.notificationService.success({
          content: this.translateService.get('update_api_group_success', {
            name: this.oldName,
          }),
        });
      } else {
        const payload = [{ name: this.name }];
        await this.rbacService.createApiGroup(payload);
        this.notificationService.success({
          content: this.translateService.get('add_api_group_success', {
            name: this.name,
          }),
        });
      }
      this.close.next(true);
    } catch (err) {
      this.errorsToastService.error(err);
      this.close.next(false);
    } finally {
      this.confirming = false;
    }
  }
}
