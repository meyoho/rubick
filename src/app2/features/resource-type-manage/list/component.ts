import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnDestroy, OnInit } from '@angular/core';

import {
  getAction,
  K8sPermissionsMap,
  parseSchema,
} from 'app/features-shared/rbac/permissions-table/constants';
import { RBACService } from 'app/services/api/rbac.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  RoleApiGroup,
  RolePermissionItem,
  RoleSchema,
} from 'app2/features/rbac/backend-api';
import { ApiGroupFormComponent } from 'app2/features/resource-type-manage/api-group-form/component';
import { ResourceOperationRejectedComponent } from 'app2/features/resource-type-manage/resource-operation-rejected/component';
import { ResourceTypeFormComponent } from 'app2/features/resource-type-manage/resource-type-form/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class K8sResourceTypeManegeListComponent implements OnInit, OnDestroy {
  columns = ['resource_type', 'permission_action', 'source', 'action'];
  k8sPermissionsMap: K8sPermissionsMap = {};
  initialized: boolean;
  apiGroupList: RoleApiGroup[] = [];
  expandFlag: boolean[] = [];

  constructor(
    private rbacService: RBACService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private notificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnInit() {
    this.fetchResource();
  }

  async fetchResource() {
    this.initialized = false;
    this.expandFlag = [];
    this.apiGroupList = await this.rbacService.getApiGroupList();
    const schemas: RoleSchema[] = await this.rbacService.getRoleSchema();
    const { k8sPermissionsMap } = parseSchema(schemas, [], true);
    this.k8sPermissionsMap = k8sPermissionsMap;
    this.initialized = true;
  }

  ngOnDestroy() {}

  trackByFn(_index: number, item: RolePermissionItem) {
    return item.resource_type;
  }

  getActionsTranslation(actions: string[]) {
    return actions
      .map(action => {
        return this.translateService.get(getAction(action));
      })
      .join(', ');
  }

  async operateApiGroup(name: string) {
    if (this.k8sPermissionsMap[name] && this.k8sPermissionsMap[name].length) {
      await this.dialogService.confirm({
        title: this.translateService.get('can_not_update_api_group_content', {
          name,
        }),
        confirmText: this.translateService.get('got_it'),
        cancelButton: false,
      });
      return;
    }
    try {
      const modalRef = this.dialogService.open(ApiGroupFormComponent, {
        data: {
          name,
        },
      });
      modalRef.componentInstance.close.subscribe((res: boolean) => {
        modalRef.close();
        if (!res) {
          return;
        }
        this.fetchResource();
      });
    } catch (e) {}
  }

  async deleteApiGroup(name: string) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete_api_group_confirm', {
          name,
        }),
        confirmText: this.translateService.get('delete'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    if (this.k8sPermissionsMap[name] && this.k8sPermissionsMap[name].length) {
      await this.dialogService.confirm({
        title: this.translateService.get('can_not_detele_api_group_content', {
          name,
        }),
        confirmText: this.translateService.get('got_it'),
        cancelButton: false,
      });
      return;
    }
    try {
      await this.rbacService.deleteApiGroup(name);
      this.notificationService.success({
        content: this.translateService.get('delete_api_group_success', {
          name,
        }),
      });
      this.fetchResource();
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  async operateResourceType(resourceType?: RolePermissionItem) {
    try {
      const modalRef = this.dialogService.open(ResourceTypeFormComponent, {
        data: {
          apiGroups: this.apiGroupList,
          resourceType: resourceType,
        },
      });
      modalRef.componentInstance.close.subscribe((res: boolean) => {
        modalRef.close();
        if (!res) {
          return;
        }
        this.fetchResource();
      });
    } catch (e) {}
  }

  async deleteResourceType(resourceType: RolePermissionItem) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('resource_type_delete_confirm', {
          name: resourceType.resource_type,
          api_group: resourceType.api_group,
        }),
        confirmText: this.translateService.get('delete'),
        cancelText: this.translateService.get('cancel'),
      });
      await this.rbacService.deleteResourceType(
        resourceType.api_group,
        resourceType.resource_type,
      );
      this.notificationService.success({
        content: this.translateService.get('delete_resource_type_success', {
          name: resourceType.resource_type,
          api_group: resourceType.api_group,
        }),
      });
      this.fetchResource();
    } catch (err) {
      if (
        err.errors &&
        err.errors[0] &&
        err.errors[0].code === 'schema_resource_used'
      ) {
        this.resourceOperationRejected(
          'delete',
          resourceType.resource_type,
          err.errors[0].fields[0]['role.name'],
        );
      } else {
        this.errorsToastService.error(err);
      }
    }
  }

  resourceOperationRejected(action: string, name: string, list: string[]) {
    const modalRef = this.dialogService.open(
      ResourceOperationRejectedComponent,
      {
        data: {
          title: this.translateService.get(`resource_type_can_not_${action}`),
          content: this.translateService.get('resource_in_use_tip', { name }),
          list,
        },
      },
    );
    modalRef.componentInstance.close.subscribe(() => {
      modalRef.close();
    });
  }

  expandedChange(target: boolean, index: number) {
    this.expandFlag[index] = target;
  }
}
