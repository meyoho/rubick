import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { ApiGroupFormComponent } from 'app2/features/resource-type-manage/api-group-form/component';
import { K8sResourceTypeManegeListComponent } from 'app2/features/resource-type-manage/list/component';
import { ResourceOperationRejectedComponent } from 'app2/features/resource-type-manage/resource-operation-rejected/component';
import { ResourceTypeFormComponent } from 'app2/features/resource-type-manage/resource-type-form/component';
import { ResourceTypeManageRoutingModule } from 'app2/features/resource-type-manage/routing.module';

@NgModule({
  imports: [SharedModule, ResourceTypeManageRoutingModule],
  declarations: [
    K8sResourceTypeManegeListComponent,
    ApiGroupFormComponent,
    ResourceTypeFormComponent,
    ResourceOperationRejectedComponent,
  ],
  entryComponents: [
    ApiGroupFormComponent,
    ResourceTypeFormComponent,
    ResourceOperationRejectedComponent,
  ],
})
export class ReourceTypeManageModule {}
