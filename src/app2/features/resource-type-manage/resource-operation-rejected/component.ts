import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  Output,
} from '@angular/core';
import { Subject } from 'rxjs';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ResourceOperationRejectedComponent implements OnDestroy {
  private onDestroy$ = new Subject<void>();
  @Output()
  close = new EventEmitter();

  constructor(
    @Inject(DIALOG_DATA)
    public modalData: {
      title: string;
      content: string;
      list: string[];
    },
  ) {}

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  cancel() {
    this.close.next();
  }
}
