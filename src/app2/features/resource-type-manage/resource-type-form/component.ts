import { DIALOG_DATA, DialogService, NotificationService } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { RBACService } from 'app/services/api/rbac.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';
import {
  PermissionActions,
  ResourceTypeFormModel,
  RoleApiGroup,
  RolePermissionItem,
} from 'app2/features/rbac/backend-api';
import { ResourceOperationRejectedComponent } from 'app2/features/resource-type-manage/resource-operation-rejected/component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class ResourceTypeFormComponent {
  @Output()
  close = new EventEmitter();
  formModel: ResourceTypeFormModel;
  @ViewChild('Form')
  form: NgForm;
  isUpdate: boolean;
  actions = PermissionActions;
  confirming: boolean;

  K8S_RESOURCE_NAME_BASE = K8S_RESOURCE_NAME_BASE;
  apiGroups: RoleApiGroup[];

  constructor(
    @Inject(DIALOG_DATA)
    private modalData: {
      resourceType: RolePermissionItem;
      apiGroups: RoleApiGroup[];
    },
    private rbacService: RBACService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
  ) {
    this.formModel = this.getDefaultFormModel();
    this.isUpdate = this.modalData.resourceType ? true : false;
    if (this.isUpdate) {
      Object.assign(this.formModel, this.modalData.resourceType);
    }
    this.apiGroups = this.modalData.apiGroups || [];
  }

  getDefaultFormModel(): ResourceTypeFormModel {
    return {
      api_group: '',
      resource_type: '',
      actions: [],
    } as ResourceTypeFormModel;
  }

  cancel() {
    this.close.next(null);
  }

  async confirm() {
    this.confirming = true;
    try {
      if (this.isUpdate) {
        await this.rbacService.updateResourceType(this.formModel);
        this.notificationService.success({
          content: this.translateService.get('update_resource_type_success', {
            name: this.formModel.resource_type,
            api_group: this.formModel.api_group,
          }),
        });
      } else {
        await this.rbacService.createResourceType(this.formModel);
        this.notificationService.success({
          content: this.translateService.get('add_resource_type_success', {
            name: this.formModel.resource_type,
            api_group: this.formModel.api_group,
          }),
        });
      }
      this.close.next(true);
    } catch (err) {
      this.close.next(false);
      if (
        err.errors &&
        err.errors[0] &&
        err.errors[0].code === 'schema_resource_used'
      ) {
        this.resourceOperationRejected(
          'update',
          this.formModel.resource_type,
          err.errors[0].fields[0]['role.name'],
        );
      } else {
        this.errorsToastService.error(err);
      }
    } finally {
      this.confirming = false;
    }
  }

  resourceOperationRejected(action: string, name: string, list: string[]) {
    const modalRef = this.dialogService.open(
      ResourceOperationRejectedComponent,
      {
        data: {
          title: this.translateService.get(`resource_type_can_not_${action}`),
          content: this.translateService.get('resource_in_use_tip', { name }),
          list,
        },
      },
    );
    modalRef.componentInstance.close.subscribe(() => {
      modalRef.close();
    });
  }
}
