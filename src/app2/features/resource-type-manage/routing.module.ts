import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { K8sResourceTypeManegeListComponent } from 'app2/features/resource-type-manage/list/component';

const eventRoutes: Routes = [
  {
    path: '',
    component: K8sResourceTypeManegeListComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(eventRoutes)],
  exports: [RouterModule],
})
export class ResourceTypeManageRoutingModule {}
