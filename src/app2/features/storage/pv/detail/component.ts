import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { Pv } from 'app/services/api/storage.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { PersistentVolume } from 'app/typings';
import * as codeEditorUtil from 'app/utils/code-editor-config';
import * as jsyaml from 'js-yaml';
import { cloneDeep } from 'lodash-es';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class PvDetailComponent implements OnInit {
  region: Cluster;
  namespaceName: string;
  name: string;
  pvYaml: string;
  private RESOURCE_TYPE = 'persistentvolumes';
  codeEditorOptions = codeEditorUtil.yamlOptions;
  actionsConfigView = codeEditorUtil.viewActions;
  initialized = false;
  loading = false;
  pv: Pv;

  constructor(
    private regionService: RegionService,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {}

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.activatedRoute.paramMap.subscribe(async params => {
      this.name = params.get('name');
      await this.getPvDetail();
      this.initialized = true;
    });
  }

  canShowUpdate(item: Pv) {
    return this.roleUtil.resourceHasPermission(
      item,
      'persistentvolume',
      'update',
    );
  }

  canShowDelete(item: Pv) {
    return (
      this.roleUtil.resourceHasPermission(item, 'persistentvolume', 'delete') &&
      item.kubernetes.status.phase !== 'Bound'
    );
  }

  getPvYaml(data: PersistentVolume) {
    this.pvYaml = this.formToYaml(data);
  }

  formToYaml(formModel: PersistentVolume) {
    try {
      const json = cloneDeep(formModel);
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  async getPvDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail<
        PersistentVolume
      >(this.RESOURCE_TYPE, {
        clusterName: this.region.name,
        name: this.name,
      });
      this.pv = result;
      this.getPvYaml(result.kubernetes);
    } catch (err) {
      this.errorsToastService.error(err);
      return this.jumpToListPage();
    }
  }

  jumpToListPage() {
    this.router.navigateByUrl('/console/admin/storage/pv');
  }

  update() {
    this.router.navigateByUrl(`/console/admin/storage/pv/update/${this.name}`);
  }

  async delete() {
    const result = this.pv;
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('delete_pv_content', {
          name: result.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.loading = true;
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.region.name,
        this.RESOURCE_TYPE,
        result.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translate.get('success'),
        content: this.translate.get('pv_delete_success'),
      });
      this.jumpToListPage();
      this.loading = false;
    } catch (e) {
      this.loading = false;
      this.errorsToastService.error(e);
    }
  }
}
