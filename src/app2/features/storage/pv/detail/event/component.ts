import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Pv } from 'app/services/api/storage.service';

interface Filters {
  or: {
    and: {
      term?: {
        kind?: string;
        name?: string;
      };
      regexp?: {
        name: string;
      };
    }[];
  }[];
}
@Component({
  selector: 'rc-pv-detail-event',
  templateUrl: 'template.html',
})
export class PvDetailEventComponent implements OnInit, OnChanges {
  @Input()
  appData: Pv;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  initialized = false;
  empty = false;
  filters: Filters = {
    or: [],
  };
  filterString: string;
  name: string;
  pv: Pv;
  constructor() {}

  ngOnInit() {}
  ngOnChanges({ appData }: SimpleChanges) {
    if (appData && appData.currentValue) {
      this.extractAllResources();
      this.empty = !this.appData;
      this.initialized = true;
    }
  }

  extractAllResources() {
    this.name = this.appData.kubernetes.metadata.name;
    this.produceFilter(this.appData);
    this.filterString = JSON.stringify(this.filters);
  }

  produceFilter(item: Pv) {
    this.filters.or = [
      {
        and: [
          {
            term: {
              kind: item.kubernetes.kind,
            },
          },
          {
            term: {
              name: item.kubernetes.metadata.name,
            },
          },
        ],
      },
    ];
  }
}
