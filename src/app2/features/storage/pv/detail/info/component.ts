import { Component, Input, OnInit } from '@angular/core';
import { Pv } from 'app/services/api/storage.service';
import {
  ACCESS_MODES,
  PVC_STATUS_MAP,
  PV_RECYCLE_STRATEGIES,
} from 'app2/features/storage/storage.constant';

@Component({
  selector: 'rc-pv-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class PvDetailInfoComponent implements OnInit {
  @Input()
  data: Pv['kubernetes'];
  statusMap = PVC_STATUS_MAP;
  accessModes = ACCESS_MODES;
  recycleModesMap = PV_RECYCLE_STRATEGIES;
  constructor() {}
  ngOnInit() {}
}
