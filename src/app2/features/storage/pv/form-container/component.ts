import { NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { Pv } from 'app/services/api/storage.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { PersistentVolume } from 'app/typings/raw-k8s';
import { safeDump, safeLoad } from 'js-yaml';

@Component({
  selector: 'rc-pv-form-container',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvFormContainerComponent implements OnInit {
  @Input()
  data: Pv;
  @ViewChild(NgForm)
  form: NgForm;
  region: Cluster;
  private RESOURCE_TYPE = 'persistentvolumes';
  pvYaml: string;
  originalYaml: string;
  initialized = false;
  submitting = false;
  formModel = 'UI';
  pvModel: PersistentVolume;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };
  resourceCreateTip = 'pv_create_success';
  resourceUpdateTip = 'pv_update_success';

  constructor(
    private location: Location,
    private auiNotificationService: NotificationService,
    private k8sResourceService: K8sResourceService,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private translateService: TranslateService,
    private regionService: RegionService,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.updateEntrance();
    this.initialized = true;
    this.cdr.markForCheck();
  }

  updateEntrance() {
    if (this.data) {
      this.pvModel = this.data.kubernetes;
    } else {
      this.pvModel = this.genDefaultPv();
    }
  }

  jumpToListPage() {
    this.router.navigateByUrl('/console/admin/storage/pv');
  }

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        this.originalYaml = this.pvYaml = this.formToYaml(this.pvModel);
        break;
      case 'UI':
        const k8sPv = this.yamlToForm(this.pvYaml);
        this.pvModel = k8sPv;
        break;
    }
  }

  formChange(form: PersistentVolume) {
    this.pvModel = form;
  }

  formToYaml(formModel: any) {
    try {
      const yaml = safeDump(formModel);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  yamlToForm(yaml: string) {
    let formModel: any;
    try {
      formModel = safeLoad(yaml);
    } catch (e) {
      this.auiNotificationService.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    this.pvModel = formModel;
    return this.pvModel;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    try {
      const payload = this.genPayload();
      if (!payload) {
        this.submitting = false;
        return;
      }
      await this.confirmNotMirror(payload);
      this.jumpToListPage();
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.submitting = false;
      this.cdr.markForCheck();
    }
  }

  private async confirmNotMirror(payload: PersistentVolume) {
    let notifyContent;
    if (this.data) {
      // update service
      notifyContent = this.resourceUpdateTip;
      delete payload.metadata.resourceVersion;
      await this.k8sResourceService.updateK8sReource(
        this.region.name,
        this.RESOURCE_TYPE,
        payload,
      );
    } else {
      // create service
      notifyContent = this.resourceCreateTip;
      await this.k8sResourceService.createK8sReource(
        this.region.name,
        this.RESOURCE_TYPE,
        payload,
      );
    }
    this.auiNotificationService.success({
      title: this.translateService.get('success'),
      content: this.translateService.get(notifyContent, {
        name: payload.metadata.name || '',
      }),
    });
  }

  cancel() {
    this.location.back();
  }

  genPayload() {
    if (this.formModel === 'YAML') {
      return this.genK8sPvFromYaml();
    }
    return this.pvModel;
  }

  genK8sPvFromYaml(): PersistentVolume {
    let sampleYaml: PersistentVolume;
    try {
      sampleYaml = safeLoad(this.pvYaml) || {};
    } catch (err) {
      this.auiNotificationService.error(
        this.translateService.get('sample_error'),
      );
      return;
    }
    return sampleYaml;
  }

  genDefaultPv(): PersistentVolume {
    return {
      apiVersion: 'v1',
      kind: 'PersistentVolume',
      metadata: {
        name: '',
        annotations: {},
        labels: {},
      },
      spec: {
        persistentVolumeReclaimPolicy: 'Retain',
        accessModes: ['ReadWriteOnce'],
        capacity: {
          storage: '',
        },
        hostPath: {
          path: '',
        },
      },
    };
  }
}
