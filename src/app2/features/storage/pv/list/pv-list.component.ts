import { DialogService, NotificationService, PageEvent } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, Subject, Subscription } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { Pv } from 'app/services/api/storage.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';
import { PVC_STATUS_MAP } from 'app2/features/storage/storage.constant';
import { ResourceList } from 'app_user/core/types';

@Component({
  templateUrl: './pv-list.component.html',
  styleUrls: ['./pv-list.component.scss'],
})
export class PvListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  statusMap = PVC_STATUS_MAP;
  hasCreatePvPermission = false;
  initialized: boolean;
  notificationVisible = true;
  RESOURCE_TYPE = 'persistentvolumes';

  get nameKey() {
    return `pv.${this.env.label_base_domain}/volume_name`;
  }

  get uuidKey() {
    return `pv.${this.env.label_base_domain}/volume_uuid`;
  }

  columns = [
    'name',
    'status',
    'referenced_pvc',
    'size',
    'created_date',
    'action',
  ];
  private onDestroy$ = new Subject<void>();
  regionSubscription: Subscription;
  private region: Cluster;

  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = [];
  clusters: string[];
  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private regionService: RegionService,
    private dialogService: DialogService,
    private k8sResourceService: K8sResourceService,
    @Inject(ENVIRONMENTS) public env: Environments,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.regionSubscription = this.regionService.region$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(region => !!region),
      )
      .subscribe((region: Cluster) => {
        this.region = region;
        this.onUpdate(null);
      });
    this.hasCreatePvPermission = await this.roleUtil.resourceTypeSupportPermissions(
      'persistentvolume',
      {
        cluster_name: this.region.name,
      },
    );
    super.ngOnInit();
    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.k8sResourceService.getK8sResourcesPaged(this.RESOURCE_TYPE, {
        clusterName: this.region.name,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
      }),
    );
  }

  trackByFn(_index: number, item: Pv) {
    return item.kubernetes.metadata.uid;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  pageChanged(pageEvent: Partial<PageEvent>) {
    const { pageIndex, pageSize, length } = pageEvent;
    const _pageIndex = pageSize < length ? pageIndex : 0;
    this.onPageEvent({
      pageIndex: _pageIndex + 1,
      pageSize,
    });
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'persistentvolume',
      action,
    );
  }

  async deletePv(pv: Pv) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_pv_title'),
        content: this.translate.get('delete_pv_content', {
          name: pv.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return false;
    }

    try {
      await this.k8sResourceService.deleteK8sReource(
        this.region.name,
        this.RESOURCE_TYPE,
        pv.kubernetes,
      );
      this.auiNotificationService.success(
        this.translate.get('pv_delete_success'),
      );
      this.onUpdate(null);
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }
}
