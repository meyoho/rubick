import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { K8sResourceWithActions, PersistentVolume } from 'app/typings';

@Component({
  templateUrl: 'template.html',
})
export class PvUpdateComponent implements OnInit {
  private RESOURCE_TYPE = 'persistentvolumes';
  region: Cluster;
  data: K8sResourceWithActions<PersistentVolume>;
  name: string;
  constructor(
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private regionService: RegionService,
  ) {}

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.activatedRoute.paramMap.subscribe(params => {
      this.name = params.get('name');
      this.getPvDetail();
    });
  }

  async getPvDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail<
        PersistentVolume
      >(this.RESOURCE_TYPE, {
        clusterName: this.region.name,
        name: this.name,
      });
      this.data = result;
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigateByUrl('/console/admin/storage/pv');
    }
  }
}
