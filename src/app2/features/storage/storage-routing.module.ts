import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PvCreateComponent } from './pv/create/component';
import { PvDetailComponent } from './pv/detail/component';
import { PvListComponent } from './pv/list/pv-list.component';
import { PvUpdateComponent } from './pv/update/component';
import { CreateStorageClassComponent } from './storageclass/create/create-storageclass.component';
import { StorageClassDetailComponent } from './storageclass/detail/storageclass-detail.component';
import { StorageClassListComponent } from './storageclass/list/storageclass-list.component';

const storageRoutes: Routes = [
  {
    path: '',
    redirectTo: 'pv',
    pathMatch: 'full',
  },
  {
    path: 'pv',
    component: PvListComponent,
  },
  {
    path: 'pv/detail/:name',
    component: PvDetailComponent,
  },
  {
    path: 'pv/create',
    component: PvCreateComponent,
  },
  {
    path: 'pv/update/:name',
    component: PvUpdateComponent,
  },
  {
    path: 'storageclass',
    component: StorageClassListComponent,
  },
  {
    path: 'storageclass/create',
    component: CreateStorageClassComponent,
  },
  {
    path: 'storageclass/detail/:name',
    component: StorageClassDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(storageRoutes)],
  exports: [RouterModule],
})
export class StorageRoutingModule {}
