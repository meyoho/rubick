export const DRIVERS = {
  ebs: ['size', 'volume_type'],
  glusterfs: ['size'],
  local: ['bricks'],
  loongstore: ['size'],
  ceph_rbd: ['size'],
};

// Translate volume states to Rubick status
export const VOLUME_STATES = {
  available: 'free',
  'in-use': 'used',
  error: 'error',
  pending: 'pending',
};

// Translate snapshot states to Rubick status
export const SNAPSHOT_STATES = {
  completed: 'completed',
  error: 'error',
  pending: 'pending',
};

export const SNAPSHOT_SUPPORTED_DRIVERS = ['ebs', 'glusterfs'];

export const VOLUME_TYPES = ['io1', 'gp2'];

export enum PV_STATUS_MAP {
  Available = 'free',
  Created = 'free',
  Failed = 'recycling_failed',
  Bound = 'bound',
  Released = 'waiting_recycle',
}

export enum PV_RECYCLE_STRATEGIES {
  Retain = 'pv_strategy_retain',
  Delete = 'pv_strategy_delete',
  Recycle = 'pv_strategy_recycle',
}

export const PV_VOLUME_TYPES = ['hostPath', 'nfs'];

export const ACCESS_MODE_LIMIT = {
  ebs: ['ReadOnlyMany', 'ReadWriteMany'],
};

export const RECYCLE_STRATEGY_LIMIT = {
  ebs: ['Recycle'],
};

export enum PVC_STATUS_MAP {
  Available = 'free',
  Lost = 'matching_failed',
  Pending = 'matching',
  Bound = 'bound',
  Released = 'released',
  Failed = 'failed',
}

export enum ACCESS_MODES {
  ReadWriteOnce = 'read_write_once',
  ReadOnlyMany = 'read_only_many',
  ReadWriteMany = 'read_write_many',
}
