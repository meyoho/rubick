import { NgModule } from '@angular/core';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { StorageRoutingModule } from 'app2/features/storage/storage-routing.module';

import { PvCreateComponent } from './pv/create/component';
import { PvDetailComponent } from './pv/detail/component';
import { PvDetailEventComponent } from './pv/detail/event/component';
import { PvDetailInfoComponent } from './pv/detail/info/component';
import { PvFormContainerComponent } from './pv/form-container/component';
import { PvFormComponent } from './pv/form/component';
import { PvListComponent } from './pv/list/pv-list.component';
import { PvUpdateComponent } from './pv/update/component';
import { CreateStorageClassComponent } from './storageclass/create/create-storageclass.component';
import { StorageClassDetailComponent } from './storageclass/detail/storageclass-detail.component';
import { StorageClassListComponent } from './storageclass/list/storageclass-list.component';

@NgModule({
  imports: [
    SharedModule,
    FormTableModule,
    EventSharedModule,
    StorageRoutingModule,
  ],
  declarations: [
    PvListComponent,
    PvDetailComponent,
    PvDetailEventComponent,
    PvDetailInfoComponent,
    StorageClassListComponent,
    CreateStorageClassComponent,
    StorageClassDetailComponent,
    PvCreateComponent,
    PvFormComponent,
    PvFormContainerComponent,
    PvUpdateComponent,
  ],
})
export class StorageModule {}
