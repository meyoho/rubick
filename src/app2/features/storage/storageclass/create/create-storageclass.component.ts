import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import * as jsyaml from 'js-yaml';
import { Subscription } from 'rxjs';

import { DialogService, NotificationService } from '@alauda/ui';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { StorageService } from 'app/services/api/storage.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: './create-storageclass.component.html',
})
export class CreateStorageClassComponent implements OnInit, OnDestroy {
  loading = false;
  initialized = false;
  updating = false;
  submitting = false;
  region: Cluster;
  regionSubscription$: Subscription;
  // errorsMapper: any;
  yamlContent = '';
  payload: any;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };

  @ViewChild('form')
  form: NgForm;
  constructor(
    private regionService: RegionService,
    private translate: TranslateService,
    private router: Router,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private storageService: StorageService,
    private errorService: ErrorsToastService,
  ) {}

  async ngOnInit() {
    this.region = await this.regionService.getCurrentRegion();
    this.regionSubscription$ = this.regionService.region$.subscribe(
      (region: Cluster) => (this.region = region),
    );
    this.initialized = true;
  }

  async cancel() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('cancel'),
        content: this.translate.get('cancel_create_storageclass_content'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (error) {
      return false;
    }
    this.router.navigate(['/console/admin/storage/storageclass']);
  }

  async submit() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    try {
      this.payload = jsyaml.safeLoadAll(this.yamlContent);
    } catch (error) {
      this.auiNotificationService.error(this.translate.get('yaml_format_hint'));
      return;
    }
    const payload = this.payload[0];
    // Reserved name or not
    const name = payload.metadata && payload.metadata.name;
    if (['alauda-system-gfs', 'alauda-system-ebs'].includes(name)) {
      this.auiNotificationService.error(
        this.translate.get('storageclass_reserved_name_hint', {
          name,
        }),
      );
      return;
    }
    try {
      await this.dialogService.confirm({
        title: this.translate.get('alarm'),
        content: this.translate.get('storageclass_create_confirm'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (error) {
      return;
    }
    this.submitting = true;
    try {
      await this.storageService.createStorageClass(this.region.id, payload);
      this.auiNotificationService.success(
        this.translate.get('storageclass_create_success'),
      );
      this.router.navigate([
        '/console/admin/storage/storageclass/detail',
        payload.metadata.name,
      ]);
    } catch (error) {
      this.errorService.error(error);
    }
    this.submitting = false;
  }

  ngOnDestroy() {}
}
