import { DialogService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { get } from 'lodash-es';
import { from, Observable, Subject } from 'rxjs';
import { filter, map, takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { StorageClass, StorageService } from 'app/services/api/storage.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { PageParams, ResourceList } from 'app_user/core/types';

interface StorageClassItem extends StorageClass {
  isDefault: boolean;
  isSystem: boolean;
  canUpdate: boolean;
  canDelete: boolean;
  inAction: boolean;
}

@Component({
  templateUrl: './storageclass-list.component.html',
  styleUrls: [
    '../../storage.common.scss',
    './storageclass-list.component.scss',
  ],
})
export class StorageClassListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  initialized = false;
  hasCreatePermission = false;
  loadError = false;
  inAction = false;
  lang: string;
  notificationVisible = true;
  columns = ['name', 'storageclass_type', 'created_at', 'action'];
  clusterName: string;
  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private storageService: StorageService,
    private roleUtil: RoleUtilitiesService,
    private errorService: ErrorsToastService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private regionService: RegionService,
  ) {
    super(http, router, activedRoute, cdr);
    this.lang = this.translate.currentLang;
  }

  async ngOnInit() {
    super.ngOnInit();
    this.regionService.region$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(region => !!region),
      )
      .subscribe((region: Cluster) => {
        this.clusterName = region.name;
        this.onUpdate(null);
      });
    this.hasCreatePermission = await this.roleUtil.resourceTypeSupportPermissions(
      'k8s_storageclasses',
      {
        cluster_name: this.clusterName,
      },
    );

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
        map(list => {
          list.forEach((item: StorageClassItem) => {
            const anno = get(item, 'kubernetes.metadata.annotations');
            item.isDefault =
              anno &&
              anno['storageclass.kubernetes.io/is-default-class'] === 'true';
            item.isSystem = ['alauda-system-gfs', 'alauda-system-ebs'].includes(
              item.kubernetes.metadata.name,
            );
            item.canUpdate = this.hasPermission(item, 'update');
            item.canDelete = this.hasPermission(item, 'delete');
          });
          return list;
        }),
      )
      .subscribe(list => {
        if (list.length) {
          this.initialized = true;
        }
      });
    this.initialized = true;
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.storageService.getStorageClasses({
        cluster_id: this.clusterName,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
        name: params.search,
      }),
    );
  }

  pageChanged(pageEvent: PageParams) {
    this.onPageEvent({
      pageIndex: ++pageEvent.pageIndex,
      pageSize: pageEvent.pageSize,
    });
  }

  async setDefault(sc: StorageClassItem) {
    const payload = sc.kubernetes;
    payload.metadata['annotations'] = payload.metadata.annotations || {};
    payload.metadata.annotations[
      'storageclass.kubernetes.io/is-default-class'
    ] = sc.isDefault ? 'false' : 'true';
    sc.inAction = true;
    try {
      await this.storageService.updateStorageClass(
        this.clusterName,
        sc.kubernetes.metadata.name,
        payload,
      );
      this.onUpdate(null);
    } catch (error) {
      this.errorService.error(error);
    }
    sc.inAction = false;
  }

  async delete(sc: StorageClassItem) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_storageclass_title'),
        content: this.translate.get('delete_storageclass_content', {
          name: sc.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (error) {
      this.errorService.error(error);
      return;
    }
    sc.inAction = true;
    try {
      await this.storageService.deleteStorageClass(
        this.clusterName,
        sc.kubernetes.metadata.name,
      );
      this.auiNotificationService.success(
        this.translate.get('storageclass_delete_success'),
      );
      this.onUpdate(null);
    } catch (error) {
      this.errorService.error(error);
    }
    sc.inAction = false;
  }

  viewDetail(storageClass: StorageClassItem) {
    this.router.navigate([
      '/console/admin/storage/storageclass/detail',
      storageClass.kubernetes.metadata.name,
    ]);
  }

  hasPermission(item: StorageClassItem, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'k8s_storageclasses',
      action,
    );
  }

  createStorageClass() {
    this.router.navigate(['/console/admin/storage/storageclass/create']);
  }

  extractStorageClassName(storageClass: StorageClassItem) {
    return storageClass.kubernetes.provisioner.split('/')[1];
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: StorageClassItem) {
    return item.kubernetes.metadata.name;
  }
}
