import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { from, Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { CredentialCreateDialogComponent } from 'app/features-shared/credential';
import {
  Credential,
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  SupportedSecretTypes,
  ToolChainSecret,
  ToolService,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  mapCredentialToToolChainSecret,
  mapResourceToRegistryService,
} from 'app/services/api/tool-chain/utils';
import { TranslateService } from 'app/translate/translate.service';
import { KubernetesResource } from 'app/typings';

@Component({
  selector: 'rc-add-credential',
  templateUrl: 'add-credential-dialog.component.html',
  styleUrls: ['add-credential-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddCredentialDialogComponent {
  @ViewChild('form')
  formRef: NgForm;

  loading = false;
  secretTypes = CredentialType;

  formData = {
    name: this.tool.name,
    visitHost: this.tool.visitHost,
    apiHost: this.tool.host,
    allowSubscribe: this.tool.subscription,
    useSecret: true,
    secretType: this.tool.auth_type || CredentialType.BasicAuth,
    projectName: this.tool.projectName,
    secret: this.tool.secret,
  };

  private secretsUpdated$$ = new Subject<CredentialType>();

  secrets$ = this.secretsUpdated$$.pipe(startWith(null)).pipe(
    switchMap(() => this.secretApi.find({})),
    map(res =>
      res.items.filter(item => item.type === this.formData.secretType),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    @Inject(DIALOG_DATA) private tool: ToolService,
    private secretApi: CredentialApiService,
    private dialog: DialogService,
    private cdr: ChangeDetectorRef,
    private message: MessageService,
    private notification: NotificationService,
    private translate: TranslateService,
    private toolChainApi: ToolChainApiService,
    private dialogRef: DialogRef<
      AddCredentialDialogComponent,
      {
        name: string;
        visitHost: string;
        apiHost?: string;
        allowSubscribe?: boolean;
        useSecret?: boolean;
        secretType?: string;
        secret?: ToolChainSecret;
      }
    >,
  ) {}

  secretToValue(credential: Credential) {
    this.formData = {
      ...this.formData,
      secret: mapCredentialToToolChainSecret(credential),
    };
  }

  resetSecret(credential?: Credential) {
    if (credential) {
      this.formData.secretType = credential.type;
      this.formData.secret = mapCredentialToToolChainSecret(credential);
    } else {
      this.formData.secret = {
        name: '',
        namespace: '',
      };
    }
    this.secretsUpdated$$.next();
  }

  addSecret() {
    const dialogRef = this.dialog.open(CredentialCreateDialogComponent, {
      data: {
        types: [this.formData.secretType],
        showHint: false,
      },
    });

    dialogRef.afterClosed().subscribe((credential?: Credential) => {
      if (credential) {
        this.resetSecret(credential);
      }
    });
  }

  supportedSecretType(type: string) {
    return (this.tool.supportedSecretTypes || [])
      .map((item: SupportedSecretTypes) => item.type)
      .includes(type);
  }

  addCredential() {
    this.formRef.onSubmit(null);
    if (this.formRef.invalid) {
      return;
    }
    this.loading = true;
    const currentData = this.formData;
    from(
      this.toolChainApi.updateTool(this.tool.kind, {
        ...this.tool,
        ...currentData,
      }),
    ).subscribe(
      (res: KubernetesResource) => {
        this.loading = false;
        this.cdr.markForCheck();
        this.message.success(
          this.translate.get('tool_chain_update_successful'),
        );
        this.dialogRef.close(mapResourceToRegistryService(this.tool.kind, res));
      },
      (error: any) => {
        this.loading = false;
        this.cdr.markForCheck();
        this.notification.error({
          title: this.translate.get('update_failed'),
          content: error.errors[0].message,
        });
      },
    );
  }
}
