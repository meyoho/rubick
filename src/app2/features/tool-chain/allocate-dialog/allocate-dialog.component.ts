import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { from, of, Subject } from 'rxjs';
import {
  catchError,
  first,
  map,
  pluck,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { ToolChainCreateResourceDialogComponent } from 'app/features-shared/tool-chain';
import { ProjectService } from 'app/services/api/project.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  AllocateFormModel,
  ToolResource,
  ToolService,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  canCreateResource,
  getToolResourceDisplayName,
  mapFormModelToToolBindingReplica,
  resourceSwitcher,
} from 'app/services/api/tool-chain/utils';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';
import { iconsType } from 'app2/features/tool-chain/allocate-dialog/allocate-dialog.types';

@Component({
  templateUrl: 'allocate-dialog.component.html',
  styleUrls: ['allocate-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AllocateDialogComponent implements OnInit {
  resourceCreatePermission: boolean;
  spaceEnabled: boolean;
  resourceSwitcher: boolean;
  canCreateResource: boolean;
  formData: AllocateFormModel = {
    tenant: {
      name: '',
    },
    resource: [],
    registry: '',
    name: '',
    description: '',
    selector: {
      name: this.tool.name,
      kind: this.tool.kind as string,
    },
    integrateBy: this.tool.integrateBy || '',
    __original: this.tool.__original,
  };
  loading = false;
  loadError = false;
  resourceLoading = false;
  resourcePlaceholder = '';
  resourceNamePattern = K8S_RESOURCE_NAME_BASE;

  get iconType(): string {
    return iconsType[this.tool.type.toLowerCase()];
  }
  updated$ = new Subject<{ [key: string]: string }>();
  updateResource$ = new Subject<{ [key: string]: string }>();

  tenant$ = this.updated$.pipe(startWith({})).pipe(
    switchMap(() =>
      from(this.projectService.getProjects({ page: 1, page_size: 1000 })).pipe(
        tap(() => (this.formData.tenant = { name: '' })),
        pluck('results'),
        publishReplay(1),
        refCount(),
      ),
    ),
  );

  resources$ = this.updateResource$.pipe(startWith({})).pipe(
    tap(() => {
      this.formData.resource = [];
      this.resourceLoading = true;
    }),
    switchMap(() =>
      from(
        this.toolChainApi.getResources({
          name: this.tool.name,
          kind: this.tool.kind,
          secret: this.tool.secret,
        }),
      ).pipe(
        map((res: any) => (res.items || []).map((r: any) => r.metadata)),
        catchError(error => {
          this.loadError = true;
          this.errorService.error(error);
          return of(null);
        }),
        tap((res: any) => {
          this.resourceLoading = false;
          let placeholder = '';
          if (this.loadError) {
            placeholder = 'tool_chain_get_resource_error';
          } else {
            if (res && res.length) {
              placeholder = 'tool_chain_please_select_resource';
            } else {
              placeholder = 'tool_chain_currently_no_resource';
            }
          }
          this.resourcePlaceholder = this.translate.get(placeholder, {
            resource: this.getToolResourceDisplayName(this.tool.type),
          });
        }),
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  @ViewChild('form')
  formRef: NgForm;

  constructor(
    @Inject(DIALOG_DATA) public tool: ToolService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private dialogRef: DialogRef<AllocateDialogComponent>,
    private projectService: ProjectService,
    private roleUtil: RoleUtilitiesService,
    private toolChainApi: ToolChainApiService,
    private message: MessageService,
    private notification: NotificationService,
    private errorService: ErrorsToastService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private dialog: DialogService,
  ) {
    this.resourceSwitcher = resourceSwitcher(this.tool.type);
    this.canCreateResource = canCreateResource(this.tool.type);
    this.spaceEnabled = this.weblabs.GROUP_ENABLED;
  }

  async ngOnInit() {
    [
      this.resourceCreatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_tool_projects',
      {},
      ['create'],
    );
  }

  close(created = '') {
    this.dialogRef.close(created);
  }

  secretToValue(tenant: string) {
    this.formData = {
      ...this.formData,
      tenant: {
        name: tenant,
      },
    };
  }

  allocate() {
    this.formRef.onSubmit(null);
    if (this.formRef.invalid) {
      return;
    }
    this.loading = true;
    if (this.tool.kind === 'ArtifactRegistry') {
      this.formData.resource = [{ name: this.tool.name }];
    }
    const payload = mapFormModelToToolBindingReplica(
      this.formData,
      this.spaceEnabled,
    );
    from(
      this.toolChainApi.allocate(payload, this.formData.tenant.name),
    ).subscribe(
      res => {
        this.loading = false;
        this.cdr.markForCheck();
        this.message.success(
          `${this.formData.name} ${this.translate.get(
            'tool_chain_allocate_successful',
          )}`,
        );
        this.dialogRef.close(res);
      },
      (error: any) => {
        this.loading = false;
        this.cdr.markForCheck();
        this.notification.error({
          title: this.translate.get('tool_chain_allocate_failed'),
          content: error.errors[0].message,
        });
      },
    );
  }

  createResource() {
    this.resources$.pipe(first()).subscribe((resource: ToolResource[]) => {
      const data = {
        tenantName: this.formData.tenant.name,
        toolName: this.tool.name,
        type: this.tool.type,
        kind: this.tool.kind,
        resourceNameList: resource,
        secretname: this.tool.secret.name,
        namespace: this.tool.secret.namespace,
        options: {
          mode: 'input',
          hideOptions: true,
        },
      };
      const dialogRef = this.dialog.open(
        ToolChainCreateResourceDialogComponent,
        {
          data,
          size: DialogSize.Medium,
        },
      );
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.updateResource$.next();
        }
      });
    });
  }

  getToolResourceDisplayName(tool: string) {
    return this.translate.get(getToolResourceDisplayName(tool));
  }
}
