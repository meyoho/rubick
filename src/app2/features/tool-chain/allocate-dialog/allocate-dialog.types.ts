export enum ToolChainType {
  jira = 'jira',
  taiga = 'taiga',
  gitlab = 'gitlab',
  gitee = 'gitee',
  bitbucket = 'bitbucket',
  github = 'github',
  harbor = 'harbor',
  confluence = 'confluence',
  maven2 = 'maven2',
}

export const iconsType = {
  [ToolChainType.jira]: 'allocate-resource-icon-jira',
  [ToolChainType.taiga]: 'allocate-resource-icon-taiga',
  [ToolChainType.gitlab]: 'allocate-resource-icon-gitlab',
  [ToolChainType.gitee]: 'allocate-resource-icon-gitee',
  [ToolChainType.bitbucket]: 'allocate-resource-icon-bitbucket',
  [ToolChainType.github]: 'allocate-resource-icon-github',
  [ToolChainType.harbor]: 'allocate-resource-icon-harbor',
  [ToolChainType.confluence]: 'allocate-resource-icon-confluence',
  [ToolChainType.maven2]: 'allocate-resource-icon-maven2',
};
