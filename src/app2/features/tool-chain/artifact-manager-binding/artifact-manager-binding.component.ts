import { Component, EventEmitter, Input, Output } from '@angular/core';

import { snakeCase } from 'lodash-es';

import { ArtiFactDetail } from 'app/services/api/tool-chain/tool-chain-api.types';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  selector: 'rc-artifact-manager-binding',
  templateUrl: './artifact-manager-binding.component.html',
  styleUrls: ['./artifact-manager-binding.component.scss'],
})
export class ArtifactManagerBindingComponent {
  @Input()
  artifactList: ArtiFactDetail[];

  @Output()
  cardClick = new EventEmitter<ArtiFactDetail>();

  snakeCase = snakeCase;

  constructor(private translate: TranslateService) {}

  cardClickHandler(service: ArtiFactDetail) {
    this.cardClick.emit(service);
  }

  getIntegrateBy(name: string) {
    return this.translate.get('tool_chain_integrate_by', { integrateBy: name });
  }
}
