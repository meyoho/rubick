import {
  ConfirmType,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { flatten, get, snakeCase } from 'lodash-es';
import { combineLatest, EMPTY, from, Subject } from 'rxjs';
import {
  catchError,
  first,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { CredentialType } from 'app/services/api/credential.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  ArtiFactDetail,
  AuthorizeInfo,
  SupportedSecretTypes,
  ToolKind,
  ToolService,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import { TranslateService } from 'app/translate/translate.service';
import { AddCredentialDialogComponent } from 'app2/features/tool-chain/add-credential-dialog/add-credential-dialog.component';
import { CreateRepositoryDialogComponent } from 'app2/features/tool-chain/create-repository-dialog/create-repository-dialog.component';
import { SecretValidatingDialogComponent } from 'app2/features/tool-chain/secret-validating-dialog/secret-validating-dialog.component';
import { UpdateToolComponent } from 'app2/features/tool-chain/update-tool/update-tool.component';
import { ForceDeleteComponent } from 'app2/shared/components/force-delete/component';

@Component({
  selector: 'rc-artifact-manager-detail',
  templateUrl: './artifact-manager-detail.component.html',
  styleUrls: ['./artifact-manager-detail.component.scss'],
})
export class ArtifactManagerDetailComponent implements OnInit {
  toolPermissions = {
    canUpdate: false,
    canDelete: false,
  };
  needAuthorization = false;
  secretCreatePermission: boolean;
  roleSyncEnabled = false;
  supportedSecretTypes: SupportedSecretTypes[] = [];
  private paramKind = 'ArtifactRegistryManager';
  private updated$ = new Subject<{ [key: string]: string }>();
  updateArtifactList$ = new Subject<void>();

  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `/static/images/icon/enterprise_${lang}.svg`),
  );
  routeParams$ = this.activatedRoute.paramMap.pipe(
    map(param => {
      return {
        kind: this.paramKind as ToolKind,
        name: param.get('name'),
      };
    }),
  );

  toolDetail$ = this.routeParams$.pipe(
    switchMap(params => this.toolChainApi.getService(params.kind, params.name)),
    catchError(error => {
      this.notification.error({
        title: this.translate.get('tool_chain_not_exist', {
          name: this.activatedRoute.snapshot.paramMap.get('name'),
        }),
        content: error.errors[0].message,
      });
      this.router.navigateByUrl('/console/admin/tool_chain');
      return EMPTY;
    }),
    publishReplay(1),
    refCount(),
  );

  toolDetailUpdated$ = combineLatest(
    this.toolDetail$,
    this.updated$.pipe(startWith({})),
  ).pipe(
    tap(([details]) => {
      const message = get(details, ['status', 'message'], '');
      if (message.includes('needs human intervation')) {
        this.needAuthorization = true;
      }
      this.getToolSetting(details);
    }),
    map(([detail, update]) => ({ ...detail, ...update })),
    publishReplay(1),
    refCount(),
  );

  artifactList$ = combineLatest(
    this.routeParams$,
    this.updateArtifactList$.pipe(startWith([])),
  ).pipe(
    switchMap(([params]) =>
      from(this.toolChainApi.getArtifactList(params.name)),
    ),
    publishReplay(1),
    refCount(),
  );

  private validateStatus$ = combineLatest(
    this.routeParams$,
    this.toolDetailUpdated$,
  ).pipe(
    switchMap(([params, data]) => {
      const info = {
        kind: params.kind,
        name: params.name,
        secretName: data.secret.name,
        namespace: data.secret.namespace,
      };
      return this.toolChainApi.getValidateStatus(info);
    }),
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private toolChainApi: ToolChainApiService,
    private notification: NotificationService,
    private translate: TranslateService,
    private router: Router,
    private dialog: DialogService,
    private cdr: ChangeDetectorRef,
    private roleUtil: RoleUtilitiesService,
    private message: MessageService,
  ) {}

  async ngOnInit() {
    [
      this.toolPermissions.canUpdate,
      this.toolPermissions.canDelete,
    ] = await this.roleUtil.resourceTypeSupportPermissions('devops_tool', {}, [
      'update',
      'delete',
    ]);
    [
      this.secretCreatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_secret',
      {},
      ['create'],
    );
  }

  getToolSetting(tool: ToolService) {
    this.toolChainApi.getToolChains().then(res => {
      const settings = res.map(item => item.items || []);
      const setting = flatten(settings).find(
        item =>
          item.type.toLowerCase() === tool.type.toLowerCase() &&
          item.enterprise === tool.enterprise,
      );
      this.roleSyncEnabled = (setting && setting.roleSyncEnabled) || false;
      this.supportedSecretTypes = setting && setting.supportedSecretTypes;
      this.cdr.markForCheck();
    });
  }

  update() {
    this.toolDetailUpdated$.pipe(first()).subscribe(data => {
      const dialogRef = this.dialog.open(UpdateToolComponent, {
        data: {
          ...data,
          setting: {
            roleSyncEnabled: this.roleSyncEnabled,
            supportedSecretTypes: this.supportedSecretTypes,
          },
        },
        size: DialogSize.Large,
      });
      dialogRef.afterClosed().subscribe(update => {
        if (update) {
          this.updated$.next(update);
        }
      });
    });
  }

  delete() {
    combineLatest(this.toolDetailUpdated$, this.artifactList$)
      .pipe(first())
      .subscribe(([info, artifactList]) => {
        if (artifactList.length) {
          const dialogRef = this.dialog.open(ForceDeleteComponent, {
            data: {
              name: info.name,
              title: this.translate.get('tool_chain_delete_tool'),
              content: this.translate.get('tool_chain_force_delete_tool_hint', {
                name: info.name,
              }),
              confirmText: this.translate.get('tool_chain_delete_tool'),
            },
          });
          dialogRef.componentInstance.close
            .pipe(first())
            .subscribe(async (res: Boolean) => {
              dialogRef.close();
              if (res) {
                try {
                  await this.toolChainApi.deleteTool(info.kind, info.name);
                  this.message.success(
                    this.translate.get('tool_chain_delete_successful'),
                  );
                  this.router.navigateByUrl('/console/admin/tool_chain');
                } catch (err) {
                  this.notification.error({
                    title: this.translate.get('delete_failed'),
                    content: err.errors || err.message,
                  });
                }
              }
            });
        } else {
          this.dialog
            .confirm({
              title: this.translate.get('tool_chain_delete_confirm', {
                name: info.name,
              }),
              confirmText: this.translate.get('delete'),
              confirmType: ConfirmType.Danger,
              cancelText: this.translate.get('cancel'),
              beforeConfirm: (resolve, reject) => {
                from(
                  this.toolChainApi.deleteTool(info.kind, info.name),
                ).subscribe(resolve, (error: any) => {
                  this.notification.error({
                    title: this.translate.get('delete_failed'),
                    content: error.error || error.message,
                  });
                  reject();
                });
              },
            })
            .then(() => {
              this.message.success(
                this.translate.get('tool_chain_delete_successful'),
              );
              this.router.navigateByUrl('/console/admin/tool_chain');
            })
            .catch(() => {});
        }
      });
  }

  addSecret() {
    this.toolDetailUpdated$.pipe(first()).subscribe(data => {
      const dialogRef = this.dialog.open(AddCredentialDialogComponent, {
        data: {
          ...data,
          supportedSecretTypes: this.supportedSecretTypes,
        },
        size: DialogSize.Large,
      });
      dialogRef.afterClosed().subscribe(update => {
        if (update) {
          this.updated$.next(update);
        }
      });
    });
  }

  validateStatus() {
    combineLatest(this.validateStatus$, this.toolDetailUpdated$)
      .pipe(first())
      .subscribe(([data, info]: [AuthorizeInfo, any]) => {
        const dialogRef = this.dialog.open(SecretValidatingDialogComponent, {
          size: DialogSize.Small,
          data: {
            url: data.authorizeUrl,
            secret: info.secret,
            name: info.name,
            kind: info.kind,
          },
        });
        return dialogRef.afterClosed().subscribe(res => {
          if (res) {
            this.needAuthorization = false;
            this.updated$.next(res);
            this.cdr.markForCheck();
          }
        });
      });
  }

  snakeCaseKind(kind: ToolKind) {
    return snakeCase(kind);
  }

  getAuthType(type: CredentialType) {
    switch (type) {
      case CredentialType.BasicAuth:
        return this.translate.get('secret_username_password');
      case CredentialType.OAuth2:
        return this.translate.get('tool_chain_oauth2');
      default:
        return '-';
    }
  }

  secretRoute(value: { [key: string]: string }) {
    return ['/console/admin/credential/detail', value.name];
  }

  secretName(value: { [key: string]: string }) {
    return value.name;
  }

  addRepo() {
    this.toolDetailUpdated$.pipe(first()).subscribe((data: ToolService) => {
      const dialogRef = this.dialog.open(CreateRepositoryDialogComponent, {
        size: DialogSize.Large,
        data: {
          ...data,
          secretCreatePermission: this.secretCreatePermission,
        },
      });
      dialogRef.afterClosed().subscribe(res => {
        if (res) {
          this.updateArtifactList$.next();
          this.cdr.markForCheck();
        }
      });
    });
  }

  navigateToDetail(ins: ArtiFactDetail) {
    const managerName = this.activatedRoute.snapshot.paramMap.get('name');
    this.router.navigate([
      '/console/admin/tool_chain/',
      ins.kind,
      managerName,
      ins.artifactName,
    ]);
  }
}
