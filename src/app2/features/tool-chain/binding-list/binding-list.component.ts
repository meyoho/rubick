import {
  ConfirmType,
  DialogService,
  DialogSize,
  MessageService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { from } from 'rxjs';

import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { AllocateInfo } from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  getToolResourceDisplayName,
  resourceSwitcher,
} from 'app/services/api/tool-chain/utils';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { iconsType } from 'app2/features/tool-chain/allocate-dialog/allocate-dialog.types';
import { UpdateAllocateDialogComponent } from 'app2/features/tool-chain/update-allocate-dialog/update-allocate-dialog.component';

@Component({
  selector: 'rc-binding-list',
  templateUrl: 'binding-list.component.html',
  styleUrls: ['binding-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class BindingListComponent {
  private _columns = [
    'tenant_name',
    'resource',
    'allocate',
    'description',
    'actions',
  ];

  @Input()
  bindings: AllocateInfo[];
  @Input()
  toolType: string;
  @Input()
  updateDisabled: boolean;
  @Input()
  permissions: {
    canUpdate: boolean;
    canDelete: boolean;
  };
  @Output()
  updateTable = new EventEmitter();

  get iconType(): string {
    return iconsType[this.toolType.toLowerCase()];
  }

  constructor(
    private toolChainApi: ToolChainApiService,
    private dialog: DialogService,
    private translate: TranslateService,
    private message: MessageService,
    private errorsToastService: ErrorsToastService,
  ) {}

  judgeType(type: string) {
    if (!resourceSwitcher(type)) {
      return ['tenant_name', 'allocate', 'description', 'actions'];
    } else {
      return this._columns;
    }
  }

  bindingName(_: number, binding: AllocateInfo) {
    return binding.name;
  }

  delete(item: AllocateInfo) {
    this.dialog
      .confirm({
        title: this.translate.get('tool_chain_delete_confirm', {
          name: item.name,
        }),
        content:
          item.toolItemType === 'Jenkins'
            ? this.translate.get('confirm_delete_jenkins_toolbindingreplica')
            : '',
        confirmText: this.translate.get('delete'),
        confirmType: ConfirmType.Danger,
        cancelText: this.translate.get('cancel'),
        beforeConfirm: (resolve, reject) => {
          from(this.toolChainApi.deleteAllocate(item)).subscribe(resolve, e => {
            this.errorsToastService.error(e);
            reject();
          });
        },
      })
      .then(() => {
        this.message.success(
          this.translate.get('tool_chain_delete_successful'),
        );
        this.updateTable.emit();
      })
      .catch(() => {});
  }

  update(item: AllocateInfo) {
    const dialogRef = this.dialog.open(UpdateAllocateDialogComponent, {
      data: item,
      size: DialogSize.Large,
    });
    dialogRef.afterClosed().subscribe(update => {
      if (update) {
        this.updateTable.emit();
      }
    });
  }

  getResourceTips(createdMode: string) {
    if (createdMode === 'subscription') {
      return this.translate.get('tool_chain_subscribe_at');
    }
    return this.translate.get('tool_chain_allocate_at');
  }

  getToolResourceDisplayName(tool: string) {
    return this.translate.get(getToolResourceDisplayName(tool));
  }
}
