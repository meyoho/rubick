import { DIALOG_DATA, DialogRef, DialogService } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { from, Observable, of, Subject } from 'rxjs';
import {
  catchError,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import { CredentialCreateDialogComponent } from 'app/features-shared/credential';
import {
  Credential,
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { ToolService } from 'app/services/api/tool-chain/tool-chain-api.types';
import { mapCredentialToToolChainSecret } from 'app/services/api/tool-chain/utils';
import { ErrorsToastService } from 'app/services/errors-toast.service';

export interface CreateArtifactRegistryForm {
  addWay: string;
  allowSubscribe: boolean;
  authType: string;
  fileStorageLocation: string;
  repoName: string;
  repoNameSelect: string;
  repoType: string;
  secret: { name: string; namespace: string };
  useSecret: boolean;
  versionPolicy: string;
}

@Component({
  selector: 'rc-create-repository-dialog',
  templateUrl: './create-repository-dialog.component.html',
  styleUrls: ['./create-repository-dialog.component.scss'],
})
export class CreateRepositoryDialogComponent implements OnInit, OnDestroy {
  loadError = false;
  show_advanced = false;
  createForm: FormGroup;
  secretTypes = CredentialType;

  blobStore$: Observable<any>;
  repository$: Observable<string[]>;

  private secretsUpdated$$ = new Subject<CredentialType>();
  private unsubscribe$ = new Subject<void>();

  secrets$ = this.secretsUpdated$$.pipe(startWith(null)).pipe(
    switchMap(() => this.secretApi.find({})),
    map(res => res.items.filter(item => item.type === this.data.auth_type)),
    publishReplay(1),
    refCount(),
  );

  constructor(
    @Inject(DIALOG_DATA)
    private data: ToolService,
    private dialogRef: DialogRef<CreateRepositoryDialogComponent>,
    private fb: FormBuilder,
    private toolChainApi: ToolChainApiService,
    private secretApi: CredentialApiService,
    private dialog: DialogService,
    private errorService: ErrorsToastService,
    private cdr: ChangeDetectorRef,
    private errorToastService: ErrorsToastService,
  ) {}

  ngOnInit() {
    this.createForm = this.buildForm();
    this.blobStore$ = this.getBlobStore();
    this.adjustValidators();
  }

  ngOnDestroy() {
    this.unsubscribe$.next();
    this.unsubscribe$.complete();
  }

  adjustValidators() {
    this.createForm
      .get('addWay')
      .valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((mode: string) => {
        if (mode === 'selectMode') {
          this.repository$ = this.getRepository();
          this.createForm.controls['repoNameSelect'].setValidators([
            Validators.required,
          ]);
          this.createForm.controls['fileStorageLocation'].clearValidators();
          this.createForm.controls['fileStorageLocation'].reset('');
          this.createForm.controls['repoName'].clearValidators();
          this.createForm.controls['repoName'].reset('');
          this.createForm.controls['versionPolicy'].reset('');
          this.createForm.controls['versionPolicy'].clearValidators();
        } else {
          this.loadError = false;
          this.createForm.controls['versionPolicy'].setValue('Release');
          this.createForm.controls['repoNameSelect'].clearValidators();
          this.createForm.controls['repoNameSelect'].reset('');
          this.createForm.controls['repoName'].setValidators([
            Validators.required,
          ]);
          this.createForm.controls['fileStorageLocation'].setValue('default');
          this.createForm.controls['fileStorageLocation'].setValidators([
            Validators.required,
          ]);
        }
        this.createForm.get('repoName').updateValueAndValidity();
        this.createForm.get('repoNameSelect').updateValueAndValidity();
        this.createForm.get('fileStorageLocation').updateValueAndValidity();
        this.createForm.get('versionPolicy').updateValueAndValidity();
      });

    this.createForm
      .get('useSecret')
      .valueChanges.pipe(takeUntil(this.unsubscribe$))
      .subscribe((mode: boolean) => {
        if (mode && this.show_advanced) {
          this.createForm.controls['secret'].setValidators([
            Validators.required,
          ]);
        } else {
          this.createForm.controls['secret'].clearValidators();
        }
        this.createForm.get('secret').updateValueAndValidity();
      });
  }

  buildForm(): FormGroup {
    return this.fb.group({
      addWay: this.fb.control('createMode'),
      repoType: this.fb.control('Maven'),
      repoName: this.fb.control('', [
        Validators.required,
        Validators.maxLength(20),
      ]),
      repoNameSelect: this.fb.control(''),
      versionPolicy: this.fb.control('Release'),
      fileStorageLocation: this.fb.control('default', [Validators.required]),
      allowSubscribe: this.fb.control(true),
      useSecret: this.fb.control(true),
      authType: this.fb.control(this.data.auth_type),
      secret: this.fb.control({
        name: this.data.secret.name,
        namespace: 'global-credentials',
      }),
    });
  }

  resetSecret(credential?: Credential) {
    if (credential) {
      this.createForm.controls['authType'].setValue(credential.type as string);
      this.createForm.controls['secret'].setValue(
        mapCredentialToToolChainSecret(credential),
      );
    } else {
      this.createForm.controls['secret'].setValue({
        name: '',
        namespace: '',
      });
    }
    this.secretsUpdated$$.next();
  }

  secretToValue(credential: Credential) {
    this.createForm.controls['secret'].setValue(
      mapCredentialToToolChainSecret(credential),
    );
  }

  getBlobStore() {
    return from(this.toolChainApi.getArtifactBlobStore(this.data.name)).pipe(
      catchError(() => {
        return of(['default']);
      }),
      publishReplay(1),
      refCount(),
    );
  }

  getRepository() {
    const repoType = this.createForm.value['repoType'] || 'Maven';
    return from(
      this.toolChainApi.getRepository({
        managerName: this.data.name,
        type: repoType,
      }),
    ).pipe(
      catchError(error => {
        this.loadError = true;
        this.errorService.error(error);
        return of(null);
      }),
      publishReplay(1),
      refCount(),
    );
  }

  supportedSecretType(type: string) {
    return this.data.auth_type === type;
  }

  trackSecretFn(val: { name: string; namespace: string }) {
    return val && val.name ? val.name : '';
  }

  addRepo() {
    const formData: CreateArtifactRegistryForm = this.createForm.value;
    from(
      this.toolChainApi.createArtifactRegistry<CreateArtifactRegistryForm>({
        managerName: this.data.name,
        formData,
      }),
    ).subscribe(
      res => {
        this.cdr.markForCheck();
        this.dialogRef.close({ ...res });
      },
      err => {
        this.cdr.markForCheck();
        this.errorToastService.error(err);
      },
    );
  }

  addSecret() {
    const dialogRef = this.dialog.open(CredentialCreateDialogComponent, {
      data: {
        types: [this.createForm.value['authType']],
        showHint: false,
      },
    });
    dialogRef.afterClosed().subscribe((credential?: Credential) => {
      if (credential) {
        this.resetSecret(credential);
      }
    });
  }

  handleShowAdvanced() {
    this.show_advanced = !this.show_advanced;
    if (this.show_advanced) {
      this.createForm.controls['secret'].setValidators([Validators.required]);
    } else {
      this.createForm.controls['secret'].clearValidators();
    }
    this.createForm.get('secret').updateValueAndValidity();
  }
}
