import {
  ConfirmType,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { flatten, get, snakeCase } from 'lodash-es';
import { BehaviorSubject, combineLatest, EMPTY, from, Subject } from 'rxjs';
import {
  catchError,
  first,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { CredentialType } from 'app/services/api/credential.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  AuthorizeInfo,
  ToolKind,
  ToolService,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import { TranslateService } from 'app/translate/translate.service';
import { AddCredentialDialogComponent } from 'app2/features/tool-chain/add-credential-dialog/add-credential-dialog.component';
import { AllocateDialogComponent } from 'app2/features/tool-chain/allocate-dialog/allocate-dialog.component';
import { SecretValidatingDialogComponent } from 'app2/features/tool-chain/secret-validating-dialog/secret-validating-dialog.component';
import { UpdateToolComponent } from 'app2/features/tool-chain/update-tool/update-tool.component';
import { ForceDeleteComponent } from 'app2/shared/components/force-delete/component';

@Component({
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DetailPageComponent implements OnInit {
  toolPermissions = {
    canUpdate: false,
    canDelete: false,
  };
  allocatePermissions = {
    canCreate: false,
    canUpdate: false,
    canDelete: false,
  };
  roleSyncEnabled = false;
  supportedSecretTypes: {
    type: string;
    description: { en: string; zh: string };
  }[] = [];
  loadingBindings = false;
  showZeroState = false;
  needAuthorization = false;
  pagination = {
    count: 0,
    current: 1,
    size: 10,
    hiddenSize: true,
    keyword: '',
  };
  private updated$ = new Subject<{ [key: string]: string }>();
  harborProjectsUpdated$ = new BehaviorSubject(null);

  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `/static/images/icon/enterprise_${lang}.svg`),
  );

  routeBindingtoolDetailUpdated$ = this.activatedRoute.paramMap.pipe(
    map(param => {
      return {
        kind: param.get('kind'),
        name: param.get('name'),
        manager: param.get('manager'),
      };
    }),
  );

  toolDetail$ = this.activatedRoute.paramMap.pipe(
    switchMap(params =>
      this.toolChainApi.getService(
        params.get('kind') as ToolKind,
        params.get('name'),
      ),
    ),
    catchError(error => {
      this.notification.error({
        title: this.translate.get('tool_chain_not_exist', {
          name: this.activatedRoute.snapshot.paramMap.get('name'),
        }),
        content: error.errors[0].message,
      });
      this.router.navigateByUrl('/console/admin/tool_chain');
      return EMPTY;
    }),
    publishReplay(1),
    refCount(),
  );

  bindings$ = combineLatest(
    this.routeBindingtoolDetailUpdated$,
    this.updated$.pipe(startWith({})),
  ).pipe(
    tap(() => {
      this.loadingBindings = true;
    }),
    switchMap(([params]) =>
      this.toolChainApi.getAllocateByToolName(
        {
          page: this.pagination.current,
          page_size: this.pagination.size,
          keyword: this.pagination.keyword,
          order_by: '-metadata.creationTimestamp',
        },
        params.name,
      ),
    ),
    tap(result => {
      this.pagination.count = result.length;
      this.toolDetail$.subscribe(res => {
        this.loadingBindings = false;
        this.showZeroState = result.length === 0;
        result['type'] = res.type;
      });
    }),
    publishReplay(1),
    refCount(),
  );

  toolDetailUpdated$ = combineLatest(
    this.toolDetail$,
    this.updated$.pipe(startWith({})),
  ).pipe(
    tap(([details]) => {
      if (details.status.conditions) {
        details.status.conditions.forEach((item: any) => {
          if (item.type === 'UserCount' && item.status !== 'Error') {
            details['userCount'] = item.reason;
          }
        });
      }
      const message = get(details, ['status', 'message'], '');
      if (message.includes('needs human intervation')) {
        this.needAuthorization = true;
      }
      this.getToolSetting(details);
    }),
    map(([detail, update]) => ({ ...detail, ...update })),
    publishReplay(1),
    refCount(),
  );

  validateStatus$ = combineLatest(
    this.routeBindingtoolDetailUpdated$,
    this.toolDetailUpdated$,
  ).pipe(
    switchMap(([params, data]) => {
      const info = {
        kind: params.kind,
        name: params.name,
        secretName: data.secret.name,
        namespace: data.secret.namespace,
      };
      return this.toolChainApi.getValidateStatus(info);
    }),
  );

  constructor(
    private activatedRoute: ActivatedRoute,
    private toolChainApi: ToolChainApiService,
    private roleUtil: RoleUtilitiesService,
    private dialog: DialogService,
    private translate: TranslateService,
    private router: Router,
    private message: MessageService,
    private notification: NotificationService,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    [
      this.toolPermissions.canUpdate,
      this.toolPermissions.canDelete,
    ] = await this.roleUtil.resourceTypeSupportPermissions('devops_tool', {}, [
      'update',
      'delete',
    ]);
    [
      this.allocatePermissions.canCreate,
      this.allocatePermissions.canUpdate,
      this.allocatePermissions.canDelete,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_toolbindingreplica',
      {},
      ['create', 'update', 'delete'],
    );
    this.cdr.markForCheck();
  }

  getToolSetting(tool: ToolService) {
    this.toolChainApi.getToolChains().then(res => {
      const settings = res.map(item => item.items || []);
      const setting = flatten(settings).find(
        item =>
          item.type.toLowerCase() === tool.type.toLowerCase() &&
          item.enterprise === tool.enterprise,
      );
      this.roleSyncEnabled = (setting && setting.roleSyncEnabled) || false;
      this.supportedSecretTypes = setting && setting.supportedSecretTypes;
      this.cdr.markForCheck();
    });
  }

  update() {
    this.toolDetailUpdated$.pipe(first()).subscribe(data => {
      const dialogRef = this.dialog.open(UpdateToolComponent, {
        data: {
          ...data,
          setting: {
            roleSyncEnabled: this.roleSyncEnabled,
            supportedSecretTypes: this.supportedSecretTypes,
          },
        },
        size: DialogSize.Large,
      });
      dialogRef.afterClosed().subscribe(update => {
        if (update) {
          this.updated$.next(update);
        }
      });
    });
  }

  delete() {
    combineLatest(this.toolDetailUpdated$, this.bindings$)
      .pipe(first())
      .subscribe(([info, bindings]) => {
        if (bindings.length) {
          const dialogRef = this.dialog.open(ForceDeleteComponent, {
            data: {
              name: info.name,
              title: this.translate.get('tool_chain_delete_tool'),
              content: this.translate.get('tool_chain_force_delete_tool_hint', {
                name: info.name,
              }),
              confirmText: this.translate.get('tool_chain_delete_tool'),
            },
          });
          dialogRef.componentInstance.close
            .pipe(first())
            .subscribe(async (res: Boolean) => {
              dialogRef.close();
              if (res) {
                try {
                  await this.toolChainApi.deleteTool(info.kind, info.name);
                  this.message.success(
                    this.translate.get('tool_chain_delete_successful'),
                  );
                  this.navigateByUrl();
                } catch (err) {
                  this.notification.error({
                    title: this.translate.get('delete_failed'),
                    content: err.errors || err.message,
                  });
                }
              }
            });
        } else {
          this.dialog
            .confirm({
              title: this.translate.get('tool_chain_delete_confirm', {
                name: info.name,
              }),
              confirmText: this.translate.get('delete'),
              confirmType: ConfirmType.Danger,
              cancelText: this.translate.get('cancel'),
              beforeConfirm: (resolve, reject) => {
                from(
                  this.toolChainApi.deleteTool(info.kind, info.name),
                ).subscribe(resolve, (error: any) => {
                  this.notification.error({
                    title: this.translate.get('delete_failed'),
                    content: error.error || error.message,
                  });
                  reject();
                });
              },
            })
            .then(() => {
              this.message.success(
                this.translate.get('tool_chain_delete_successful'),
              );
              this.navigateByUrl();
            })
            .catch(() => {});
        }
      });
  }

  navigateByUrl() {
    const manager = this.activatedRoute.snapshot.paramMap.get('manager');
    if (!!manager) {
      this.router.navigateByUrl(
        `/console/admin/tool_chain/ArtifactRegistryManager/${manager}`,
      );
    } else {
      this.router.navigateByUrl('/console/admin/tool_chain');
    }
  }

  allocate() {
    this.toolDetailUpdated$.pipe(first()).subscribe((data: ToolService) => {
      if (!!data.secret.name) {
        const dialogRef = this.dialog.open(AllocateDialogComponent, {
          data,
          size: DialogSize.Large,
        });
        dialogRef.afterClosed().subscribe(result => {
          if (result) {
            this.updated$.next();
          }
        });
      }
    });
  }

  onSelectedIndexChange(event: number) {
    if (event === 1) {
      this.harborProjectsUpdated$.next(null);
    }
  }

  snakeCaseKind(kind: ToolKind) {
    return snakeCase(kind);
  }

  getAuthType(type: CredentialType) {
    switch (type) {
      case CredentialType.BasicAuth:
        return this.translate.get('secret_username_password');
      case CredentialType.OAuth2:
        return this.translate.get('tool_chain_oauth2');
      default:
        return '-';
    }
  }

  secretRoute(value: { [key: string]: string }) {
    return ['/console/admin/credential/detail', value.name];
  }

  getIntegrateBy(name: string) {
    return this.translate.get('tool_chain_integrate_by', { integrateBy: name });
  }

  secretName(value: { [key: string]: string }) {
    return value.name;
  }

  addSecret() {
    this.toolDetailUpdated$.pipe(first()).subscribe(data => {
      const dialogRef = this.dialog.open(AddCredentialDialogComponent, {
        data,
        size: DialogSize.Large,
      });
      dialogRef.afterClosed().subscribe(update => {
        if (update) {
          this.updated$.next(update);
        }
      });
    });
  }

  validateStatus() {
    combineLatest(this.validateStatus$, this.toolDetailUpdated$)
      .pipe(first())
      .subscribe(([data, info]: [AuthorizeInfo, any]) => {
        const dialogRef = this.dialog.open(SecretValidatingDialogComponent, {
          size: DialogSize.Small,
          data: {
            url: data.authorizeUrl,
            secret: info.secret,
            name: info.name,
            kind: info.kind,
          },
        });
        return dialogRef.afterClosed().subscribe(res => {
          if (res) {
            this.needAuthorization = false;
            this.updated$.next(res);
            this.cdr.markForCheck();
          }
        });
      });
  }

  pageChanged(num: number) {
    this.pagination.current = num;
    this.updated$.next();
  }

  updateTable() {
    this.pagination.current = 1;
    this.updated$.next();
  }

  onSearch(name: string) {
    this.pagination.keyword = name;
    this.pagination.current = 1;
    this.updated$.next();
  }

  pageSizeChange(pageSize: number) {
    this.pagination.size = pageSize;
    this.pagination.current = 1;
    this.updated$.next();
  }
}
