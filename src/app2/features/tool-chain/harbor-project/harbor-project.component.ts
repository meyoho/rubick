import { Component, Input, OnInit } from '@angular/core';

import { BehaviorSubject, from, Observable } from 'rxjs';
import { map, publishReplay, refCount, switchMap } from 'rxjs/operators';

import { CredentialIdentity } from 'app/services/api/credential.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { getToolResourceDisplayName } from 'app/services/api/tool-chain/utils';
import { TranslateService } from 'app/translate/translate.service';

interface HarborResourceData {
  Name: string;
  RepoCount: string;
  CreationTime: string;
  Metadata: string;
  [key: string]: string;
}

interface HarborResourceItem {
  data: HarborResourceData;
  metadata: object;
}

interface HarborResource {
  items: HarborResourceItem[];
  kind: string;
  apiVersion: string;
  metadata: object;
}

@Component({
  selector: 'rc-harbor-project',
  templateUrl: 'harbor-project.component.html',
  styleUrls: ['harbor-project.component.scss'],
})
export class HarborProjectComponent implements OnInit {
  resources$: Observable<HarborResourceData[]>;

  @Input()
  update$ = new BehaviorSubject(null);
  @Input()
  global = true;
  @Input()
  projectName: string;
  @Input()
  bindingName: string;
  @Input()
  name: string;
  @Input()
  kind: string;
  @Input()
  credential: CredentialIdentity;

  constructor(
    private toolChainApi: ToolChainApiService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    if (this.global) {
      this.initResources(this.name, this.kind, this.credential);
    } else {
      this.initPrivateResources();
    }
  }

  initResources(name: string, kind: string, credential: CredentialIdentity) {
    this.resources$ = this.update$.pipe(
      switchMap(() =>
        from(
          this.toolChainApi.getResources({
            name: name,
            kind: kind,
            secret: credential,
          }),
        ).pipe(
          map((res: HarborResource) =>
            (res.items || []).map((r: HarborResourceItem) => r.data),
          ),
        ),
      ),
      publishReplay(1),
      refCount(),
    );
  }

  initPrivateResources() {
    this.resources$ = this.update$.pipe(
      switchMap(() =>
        from(
          this.toolChainApi.getPrivateResources(
            this.bindingName,
            this.projectName,
          ),
        ).pipe(
          map((res: HarborResource) =>
            (res.items || []).map((r: HarborResourceItem) => r.data),
          ),
        ),
      ),
      publishReplay(1),
      refCount(),
    );
  }

  parseAccessLevel(metadata: string) {
    const access = JSON.parse(metadata).Public || '';
    if (access === 'true') {
      return this.translate.get('image_select_public');
    } else if (access === 'false') {
      return this.translate.get('image_select_private');
    }
  }

  getToolResourceDisplayName(tool: string) {
    return getToolResourceDisplayName(tool);
  }
}
