import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { from, Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { CredentialCreateDialogComponent } from 'app/features-shared/credential';
import {
  Credential,
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { API_GROUP, API_VERSION } from 'app/services/api/tool-chain/constants';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  SupportedSecretTypes,
  Tool,
  ToolBindingReplica,
  ToolType,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  getMatchExpressions,
  mapCredentialToToolChainSecret,
} from 'app/services/api/tool-chain/utils';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { KubernetesResource } from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';

@Component({
  selector: 'rc-integrate-tool',
  templateUrl: 'integrate-tool.component.html',
  styleUrls: ['integrate-tool.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IntegrateToolComponent implements OnInit {
  secretCreatePermission: boolean;
  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  selectedType = 'all';
  selectedTool: Tool;
  secretTypes = CredentialType;
  formData: {
    name: string;
    apiHost: string;
    visitHost: string;
    allowSubscribe: boolean;
    useSecret: boolean;
    secretType: string;
    secret: {
      namespace: string;
      name: string;
    };
    projectName?: string;
    roleSyncEnabled: boolean;
  } = {
    name: '',
    apiHost: '',
    visitHost: '',
    allowSubscribe: !this.toolTypes['subscribeMode'],
    useSecret: true,
    secretType: this.secretTypes.BasicAuth,
    secret: {
      namespace: '',
      name: '',
    },
    projectName: this.toolTypes['projectName'] || '',
    roleSyncEnabled: false,
  };
  loading = false;
  allTools: Tool[] = [];

  @ViewChild('form')
  formRef: NgForm;

  private secretsUpdated$$ = new Subject<CredentialType>();

  secrets$ = this.secretsUpdated$$.pipe(startWith(null)).pipe(
    switchMap(() => this.secretApi.find({})),
    map(res =>
      res.items.filter(item => item.type === this.formData.secretType),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    @Inject(DIALOG_DATA) public toolTypes: ToolType[],
    private dialogRef: DialogRef<IntegrateToolComponent, KubernetesResource>,
    private toolChainApi: ToolChainApiService,
    private roleUtil: RoleUtilitiesService,
    private message: MessageService,
    private notification: NotificationService,
    private errorToastService: ErrorsToastService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private secretApi: CredentialApiService,
    private dialog: DialogService,
  ) {
    this.toolTypes.forEach(type => {
      this.allTools.push(...type.items);
    });
  }

  async ngOnInit() {
    [
      this.secretCreatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_secret',
      {},
      ['create'],
    );
  }
  getCurrentTools() {
    if (this.selectedType === 'all') {
      return this.allTools;
    } else {
      return this.toolTypes.find(type => type.name === this.selectedType).items;
    }
  }

  handleSelectedToolChange(tool: Tool) {
    this.selectedTool = tool;
    if (tool) {
      this.formData = {
        ...this.formData,
        name: tool.name.toLowerCase(),
        apiHost: tool.host,
        visitHost: tool.html,
        roleSyncEnabled: false,
      };
    } else {
      this.formData = {
        ...this.formData,
        secret: {
          namespace: '',
          name: '',
        },
        name: '',
        apiHost: '',
        visitHost: '',
        roleSyncEnabled: false,
      };
    }
  }

  resetSecret(credential?: Credential) {
    if (credential) {
      this.formData.secretType = credential.type as string;
      this.formData.secret = mapCredentialToToolChainSecret(credential);
    } else {
      this.formData.secret = {
        name: '',
        namespace: '',
      };
    }
    this.secretsUpdated$$.next();
  }

  integrate() {
    this.formRef.onSubmit(null);
    if (this.formRef.invalid) {
      return;
    }
    this.loading = true;
    const currentTool = this.selectedTool;
    const currentData = this.formData;
    if (this.toolTypes['subscribeMode']) {
      currentData['useSecret'] = false;
    }
    from(
      this.toolChainApi.integrateTool(currentTool.kind, {
        ...currentData,
        type: currentTool.type,
        public: currentTool.public,
      }),
    ).subscribe(
      (res: KubernetesResource) => {
        this.loading = false;
        this.cdr.markForCheck();
        this.message.success(
          this.translate.get('tool_chain_integrate_successful'),
        );
        if (this.toolTypes['subscribeMode']) {
          const projectName = window.sessionStorage.getItem('project');
          const payload: ToolBindingReplica = {
            apiVersion: `${API_GROUP}/${API_VERSION}`,
            kind: 'ToolBindingReplica',
            metadata: {
              name: res.metadata.name,
              namespace: '',
              annotations: {
                'alauda.io/description': '',
                'alauda.io/createdMode': 'subscription',
                'alauda.io/toolItemProject': projectName,
              },
            },
            spec: {
              selector: {
                name: res.metadata.name,
                kind: res.kind,
              },
              secret: this.formData.secret,
            },
          };
          this.allocateParamsHandler(payload, projectName);
          from(this.toolChainApi.subscribeTool(payload)).subscribe(
            res => {
              if (res) {
                this.cdr.markForCheck();
              }
            },
            (e: any) => {
              this.cdr.markForCheck();
              this.errorToastService.error(e);
            },
          );
        }
        this.dialogRef.close({ ...res });
      },
      (error: any) => {
        this.loading = false;
        this.cdr.markForCheck();
        this.notification.error({
          title: this.translate.get('tool_chain_integrate_failed'),
          content: error.errors[0].message,
        });
      },
    );
  }

  secretToValue(credential: Credential) {
    this.formData = {
      ...this.formData,
      secret: mapCredentialToToolChainSecret(credential),
    };
  }

  supportedSecretType(type: string) {
    return (this.selectedTool.supportedSecretTypes || [])
      .map((item: SupportedSecretTypes) => item.type)
      .includes(type);
  }

  addSecret() {
    const dialogRef = this.dialog.open(CredentialCreateDialogComponent, {
      data: {
        types: [this.formData.secretType],
        showHint: false,
      },
    });

    dialogRef.afterClosed().subscribe((credential?: Credential) => {
      if (credential) {
        this.resetSecret(credential);
      }
    });
  }

  allocateParamsHandler(payload: any, projectName: string) {
    switch (payload.spec.selector.kind) {
      case 'Jenkins': {
        return Object.assign(payload, {
          spec: {
            continuousIntegration: {
              template: {
                bindings: [
                  {
                    selector: {
                      matchExpressions: getMatchExpressions(projectName),
                    },
                    spec: {
                      jenkins: {
                        name: '',
                      },
                    },
                  },
                ],
              },
            },
            selector: payload.spec.selector,
            secret: this.formData.secret,
          },
        });
      }
      case 'CodeQualityTool': {
        return Object.assign(payload, {
          spec: {
            codeQualityTool: {
              template: {
                bindings: [
                  {
                    selector: {
                      matchExpressions: getMatchExpressions(projectName),
                    },
                    spec: {
                      codeQualityTool: {
                        name: '',
                      },
                    },
                  },
                ],
              },
            },
            selector: payload.spec.selector,
            secret: this.formData.secret,
          },
        });
      }
    }
  }
}
