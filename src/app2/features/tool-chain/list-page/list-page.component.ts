import { DialogService, DialogSize } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { BehaviorSubject, combineLatest, from, merge, Subject } from 'rxjs';
import {
  debounceTime,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  take,
} from 'rxjs/operators';

import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { ToolService } from 'app/services/api/tool-chain/tool-chain-api.types';
import { IntegrateToolComponent } from 'app2/features/tool-chain/integrate-tool/integrate-tool.component';

@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent implements OnInit {
  createPermission: boolean;
  selectedType = 'all';
  loading = true;
  filter$ = new BehaviorSubject<string>('');

  selectedType$$ = new BehaviorSubject<string>(this.selectedType);

  init$ = new Subject<void>();
  updated$ = new Subject<void>();

  toolTypes$ = from(this.toolChainApi.getToolChains()).pipe(
    map(types => types.filter(type => type.enabled)),
    map(types => {
      types.forEach(type => {
        if (type.name === 'artifactRepository') {
          type.items = type.items.filter(item => item.name !== 'Maven2');
        }
      });
      return types;
    }),
    publishReplay(1),
    refCount(),
  );

  items$ = merge(this.init$, this.updated$)
    .pipe(startWith([]))
    .pipe(
      switchMap(() => {
        this.loading = false;
        return from(this.toolChainApi.findToolServices());
      }),
      map(data => data.items),
      publishReplay(1),
      refCount(),
    );

  allServices$ = combineLatest(
    this.items$,
    this.filter$.pipe(debounceTime(300)),
  ).pipe(
    map(([items, keyword]) =>
      (items || []).filter(item => item.name.includes(keyword)),
    ),
    publishReplay(1),
    refCount(),
  );

  filteredServices$ = combineLatest(
    this.allServices$,
    this.selectedType$$,
  ).pipe(
    map(([tools, selectedType]) =>
      tools.filter(
        tool => selectedType === 'all' || tool.toolType === selectedType,
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  hasServices$ = this.allServices$.pipe(
    map(ins => ins && ins.length),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private toolChainApi: ToolChainApiService,
    private roleUtil: RoleUtilitiesService,
    private dialog: DialogService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.createPermission = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_tool',
      {},
      'create',
    );
  }

  integrateTool() {
    combineLatest(
      this.toolTypes$,
      this.allServices$.pipe(
        map(items => items.filter(item => item.public).map(item => item.type)),
      ),
    )
      .pipe(
        take(1),
        map(([allTypes, excludeTypes]) =>
          allTypes.map(toolType => ({
            ...toolType,
            items: toolType.items.filter(
              tool => !tool.public || !excludeTypes.includes(tool.type),
            ),
          })),
        ),
      )
      .subscribe(toolTypes => {
        const dialogRef = this.dialog.open(IntegrateToolComponent, {
          data: toolTypes,
          size: DialogSize.Large,
        });
        dialogRef.afterClosed().subscribe(res => {
          if (res) {
            this.updated$.next();
          }
        });
      });
  }

  navigateToDetail(ins: ToolService) {
    this.router.navigate(['/console/admin/tool_chain', ins.kind, ins.name]);
  }
}
