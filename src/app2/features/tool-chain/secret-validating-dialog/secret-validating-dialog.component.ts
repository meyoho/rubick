import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { ChangeDetectionStrategy, Component, Inject } from '@angular/core';
import { OauthValidatorService } from 'app/services/api/tool-chain/oauth-validator.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { ToolKind } from 'app/services/api/tool-chain/tool-chain-api.types';
import { TranslateService } from 'app/translate/translate.service';
import { Subject, from, of } from 'rxjs';
import {
  catchError,
  concatMap,
  delay,
  skipWhile,
  startWith,
  switchMap,
  take,
  tap,
  timeout,
} from 'rxjs/operators';

const OAUTH_INTERACTIVE_TIMEOUT = 60 * 1000;
const WATCH_SECRET_DELAY = 2 * 1000;

@Component({
  templateUrl: 'secret-validating-dialog.component.html',
  styleUrls: ['secret-validating-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SecretValidatingDialogComponent {
  validating = false;

  constructor(
    private dialogRef: DialogRef<SecretValidatingDialogComponent>,
    private oauthValidatorService: OauthValidatorService,
    private toolChainApiService: ToolChainApiService,
    private notification: NotificationService,
    private translate: TranslateService,
    @Inject(DIALOG_DATA)
    private data: {
      url: string;
      secret: { name: string; namespace: string; [key: string]: string };
      name: string;
      kind: string;
    },
  ) {}

  private watchSecret() {
    const { kind, name } = this.data;
    const fetchSecret$ = new Subject<void>();

    return fetchSecret$.pipe(
      startWith(null),
      switchMap(() =>
        from(this.toolChainApiService.getService(kind as ToolKind, name)).pipe(
          catchError(() => of(null)),
        ),
      ),
      delay(WATCH_SECRET_DELAY),
      tap(res => {
        if (
          !res ||
          !res.status.conditions ||
          this.needsAuthorization(res.status.conditions)
        ) {
          fetchSecret$.next();
        }
      }),
      skipWhile(res => this.needsAuthorization(res.status.conditions)),
      take(1),
    );
  }

  private needsAuthorization(c: any) {
    return (c || []).some((i: any) => i.status === 'NeedsAuthorization');
  }

  validate() {
    this.validating = true;
    const { url, secret, name, kind } = this.data;
    const info = {
      name,
      kind,
      secretName: secret.name,
      namespace: secret.namespace,
    };
    const sub = this.oauthValidatorService
      .validate(url)
      .pipe(
        concatMap((code: string) => {
          return this.oauthValidatorService.callback(info, code);
        }),
        concatMap(() => this.watchSecret()),
        take(1),
        timeout(OAUTH_INTERACTIVE_TIMEOUT),
      )
      .subscribe(
        res => {
          this.dialogRef.close(res);
          sub.unsubscribe();
        },
        (error: any) => {
          if (error.name === 'TimeoutError') {
            this.notification.error({
              title: this.translate.get(
                'tool_chain_oauth_authorization_timeout',
              ),
            });
          } else {
            this.notification.error({
              title: this.translate.get('tool_chain_oauth_validate_error'),
              content: error.error || error.message,
            });
          }
          this.dialogRef.close();
          sub.unsubscribe();
        },
      );
  }
}
