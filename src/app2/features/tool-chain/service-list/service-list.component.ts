import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { snakeCase } from 'lodash-es';
import { map } from 'rxjs/operators';

import {
  AllocateInfo,
  ToolService,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import { allocatedDialogMode } from 'app/services/api/tool-chain/utils';
import { TranslateService } from 'app/translate/translate.service';
import { ToolChainAllocateResourceDialogComponent } from 'app_user/features/project/tool-chain/allocate-resource/allocate-resource-dialog/allocate-resource-dialog.component';

@Component({
  selector: 'rc-service-list',
  templateUrl: 'service-list.component.html',
  styleUrls: ['service-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceListComponent {
  @Input()
  services: ToolService[];
  @Input()
  showTag = true;

  @Input()
  subscribeMode = false;

  @Input()
  canUpdateSubscription = false;

  @Input()
  selectMode = false;

  @Input()
  initTool = false;

  @Output()
  cardClick = new EventEmitter<ToolService>();

  @Output()
  afterAllocated = new EventEmitter<AllocateInfo>();

  snakeCase = snakeCase;

  selectedTool: ToolService;

  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `/static/images/icon/enterprise_${lang}.svg`),
  );

  constructor(
    private translate: TranslateService,
    private dialog: DialogService,
  ) {}

  trackByName(_: number, item: ToolService) {
    return item.name;
  }

  getIntegrateBy(name: string) {
    return this.translate.get('tool_chain_integrate_by', { integrateBy: name });
  }

  cardClickHandler(service: ToolService) {
    this.selectedTool = service;
    this.cardClick.emit(service);
  }

  openAllocateDialog(service: ToolService) {
    const dialogRef = this.dialog.open(
      ToolChainAllocateResourceDialogComponent,
      {
        data: {
          data: service,
          mode: allocatedDialogMode(service),
        },
        size: DialogSize.Large,
      },
    );
    dialogRef.afterClosed().subscribe((res?: AllocateInfo) => {
      this.afterAllocated.emit(res);
    });
  }
}
