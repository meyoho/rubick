import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ToolChainSharedModule } from 'app/features-shared/tool-chain/tool-chain-shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { AddCredentialDialogComponent } from 'app2/features/tool-chain/add-credential-dialog/add-credential-dialog.component';
import { AllocateDialogComponent } from 'app2/features/tool-chain/allocate-dialog/allocate-dialog.component';
import { ArtifactManagerBindingComponent } from 'app2/features/tool-chain/artifact-manager-binding/artifact-manager-binding.component';
import { ArtifactManagerDetailComponent } from 'app2/features/tool-chain/artifact-manager-detail/artifact-manager-detail.component';
import { BindingListComponent } from 'app2/features/tool-chain/binding-list/binding-list.component';
import { CreateRepositoryDialogComponent } from 'app2/features/tool-chain/create-repository-dialog/create-repository-dialog.component';
import { DetailPageComponent } from 'app2/features/tool-chain/detail/detail.component';
import { ListPageComponent } from 'app2/features/tool-chain/list-page/list-page.component';
import { ToolChainRoutingModule } from 'app2/features/tool-chain/tool-chain.routing.module';
import { UpdateAllocateDialogComponent } from 'app2/features/tool-chain/update-allocate-dialog/update-allocate-dialog.component';
import { UpdateToolComponent } from 'app2/features/tool-chain/update-tool/update-tool.component';

@NgModule({
  imports: [
    CommonModule,
    SharedModule,
    ToolChainRoutingModule,
    ToolChainSharedModule,
  ],
  declarations: [
    ListPageComponent,
    DetailPageComponent,
    ArtifactManagerDetailComponent,
    ArtifactManagerBindingComponent,
    UpdateToolComponent,
    AllocateDialogComponent,
    AddCredentialDialogComponent,
    BindingListComponent,
    UpdateAllocateDialogComponent,
    CreateRepositoryDialogComponent,
  ],
  entryComponents: [
    DetailPageComponent,
    ArtifactManagerDetailComponent,
    ArtifactManagerBindingComponent,
    UpdateToolComponent,
    AllocateDialogComponent,
    AddCredentialDialogComponent,
    BindingListComponent,
    UpdateAllocateDialogComponent,
    CreateRepositoryDialogComponent,
  ],
})
export class ToolChainModule {}
