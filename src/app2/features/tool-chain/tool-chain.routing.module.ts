import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ArtifactManagerDetailComponent } from 'app2/features/tool-chain/artifact-manager-detail/artifact-manager-detail.component';
import { DetailPageComponent } from 'app2/features/tool-chain/detail/detail.component';
import { ListPageComponent } from 'app2/features/tool-chain/list-page/list-page.component';

const routes: Routes = [
  { path: '', component: ListPageComponent },
  {
    path: 'ArtifactRegistryManager/:name',
    component: ArtifactManagerDetailComponent,
  },
  {
    path: ':kind/:manager/:name',
    component: DetailPageComponent,
  },
  { path: ':kind/:name', component: DetailPageComponent },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ToolChainRoutingModule {}
