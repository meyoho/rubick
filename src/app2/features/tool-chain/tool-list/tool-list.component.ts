import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { snakeCase } from 'lodash-es';
import { map } from 'rxjs/operators';

import { ToolService } from 'app/services/api/tool-chain/tool-chain-api.types';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  selector: 'rc-tool-list',
  templateUrl: 'tool-list.component.html',
  styleUrls: ['tool-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolListComponent {
  @Input()
  tools: ToolService[];
  @Input()
  showType: boolean;
  @Input()
  tool: ToolService;
  @Input()
  toolType: string;
  @Input()
  isSelected: boolean;

  @Output()
  selectedChange = new EventEmitter<ToolService>();

  selectedTool = '';
  snakeCase = snakeCase;

  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `/static/images/icon/enterprise_${lang}.svg`),
  );

  constructor(private translate: TranslateService) {}

  clearSelection() {
    this.selectedTool = '';
    this.selectedChange.emit(null);
  }

  handleCardClicked(tool: ToolService) {
    this.selectedTool = tool.name;
    this.selectedChange.emit(tool);
  }

  trackByName(_: number, item: ToolService) {
    return item.name;
  }
}
