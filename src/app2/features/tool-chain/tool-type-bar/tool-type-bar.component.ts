import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

import { snakeCase } from 'lodash-es';

import {
  ToolBinding,
  ToolService,
  ToolType,
} from 'app/services/api/tool-chain/tool-chain-api.types';

@Component({
  selector: 'rc-tool-type-bar',
  templateUrl: 'tool-type-bar.component.html',
  styleUrls: ['tool-type-bar.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolTypeBarComponent {
  @Input()
  selectedType = 'all';
  @Input()
  types: ToolType[] = [];
  @Input()
  resources: Array<ToolService | ToolBinding>;
  @Output()
  selectedTypeChange = new EventEmitter<string>();

  snakeCase = snakeCase;

  handleLabelClicked(type: string) {
    if (type !== this.selectedType) {
      this.selectedTypeChange.emit(type);
    }
  }

  getResourcesCount(type: ToolType) {
    return this.resources.filter(item => {
      const toolType =
        (item as ToolService).toolType || (item as ToolBinding).tool.toolType;
      return toolType === type.name;
    }).length;
  }
}
