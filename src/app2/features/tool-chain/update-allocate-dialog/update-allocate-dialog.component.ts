import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { from, merge, Observable, of, Subject } from 'rxjs';
import {
  catchError,
  first,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { ToolChainCreateResourceDialogComponent } from 'app/features-shared/tool-chain';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  AllocateFormModel,
  AllocateInfo,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  canCreateResource,
  getToolResourceDisplayName,
  mapFormModelToToolBindingReplica,
  resourceSwitcher,
} from 'app/services/api/tool-chain/utils';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { KubernetesResource } from 'app/typings';
import { Weblabs } from 'app/typings';
import { iconsType } from 'app2/features/tool-chain/allocate-dialog/allocate-dialog.types';

@Component({
  templateUrl: 'update-allocate-dialog.component.html',
  styleUrls: ['update-allocate-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateAllocateDialogComponent implements OnInit {
  @ViewChild('form')
  formRef: NgForm;

  get iconType(): string {
    return iconsType[this.allocate.toolItemType.toLowerCase()];
  }

  spaceEnabled: boolean;
  resourceSwitcher: boolean;
  canCreateResource: boolean;
  loadError = false;
  loading = false;
  resourceLoading = false;
  resourcePlaceholder = '';
  formData: AllocateFormModel;
  init$ = new Subject<void>();
  updated$ = new Subject<void>();

  resources$: Observable<[]>;

  constructor(
    @Inject(DIALOG_DATA) public allocate: AllocateInfo,
    @Inject(WEBLABS) public weblabs: Weblabs,
    private toolChainApi: ToolChainApiService,
    private dialogRef: DialogRef<UpdateAllocateDialogComponent>,
    private cdr: ChangeDetectorRef,
    private notification: NotificationService,
    private errorService: ErrorsToastService,
    private message: MessageService,
    private translate: TranslateService,
    private dialog: DialogService,
  ) {
    this.resourceSwitcher = resourceSwitcher(this.allocate.toolItemType);
    this.canCreateResource = canCreateResource(this.allocate.toolItemType);
    this.formData = {
      tenant: {
        name: this.allocate.project,
      },
      resource: this.allocate.resources,
      name: this.allocate.name,
      description: this.allocate.description,
      selector: this.allocate.selector,
      integrateBy: this.allocate.integrateBy,
      __original: this.allocate.__original,
    };
    this.spaceEnabled = this.weblabs.GROUP_ENABLED;
  }

  ngOnInit(): void {
    this.resources$ = merge(this.init$, this.updated$)
      .pipe(startWith([]))
      .pipe(
        switchMap(() =>
          from(
            this.toolChainApi.getResources({
              name: this.allocate.selector.name,
              kind: this.allocate.selector.kind,
              secret: this.allocate.secret,
            }),
          ).pipe(
            map((res: any) => res.items.map((r: any) => r.metadata)),
            catchError(error => {
              this.loadError = true;
              this.errorService.error(error);
              return of(null);
            }),
            tap((res: any) => {
              this.resourceLoading = false;
              let placeholder = '';
              if (this.loadError) {
                placeholder = 'tool_chain_get_resource_error';
              } else {
                if (res && res.length) {
                  placeholder = 'tool_chain_please_select_resource';
                } else {
                  placeholder = 'tool_chain_currently_no_resource';
                }
              }
              this.resourcePlaceholder = this.translate.get(placeholder, {
                resource: this.getToolResourceDisplayName(
                  this.allocate.toolItemType,
                ),
              });
            }),
          ),
        ),
        publishReplay(1),
        refCount(),
      );
  }

  update() {
    this.formRef.onSubmit(null);
    if (this.formRef.invalid) {
      return;
    }
    this.loading = true;
    const payload = mapFormModelToToolBindingReplica(
      this.formData,
      this.spaceEnabled,
    );
    from(
      this.toolChainApi.updateAllocate(payload, this.formData.tenant.name),
    ).subscribe(
      (res: KubernetesResource) => {
        this.loading = false;
        this.cdr.markForCheck();
        this.message.success(
          this.translate.get('tool_chain_update_successful'),
        );
        this.dialogRef.close(res);
      },
      (error: any) => {
        this.loading = false;
        this.cdr.markForCheck();
        this.notification.error({
          title: this.translate.get('update_failed'),
          content: error.errors[0].message,
        });
      },
    );
  }

  createResource() {
    this.resources$.pipe(first()).subscribe((resource: []) => {
      const data = {
        tenantName: this.formData.tenant.name,
        toolName: this.allocate.selector.name,
        type: this.allocate.toolItemType,
        kind: this.allocate.selector.kind,
        resourceNameList: resource,
        secretname: this.allocate.secret.name,
        namespace: this.allocate.secret.namespace,
        options: {
          mode: 'input',
          hideOptions: true,
        },
      };
      const dialogRef = this.dialog.open(
        ToolChainCreateResourceDialogComponent,
        {
          data,
          size: DialogSize.Large,
        },
      );
      dialogRef.afterClosed().subscribe(result => {
        if (result) {
          this.updated$.next();
        }
      });
    });
  }

  trackFn = (val: any = {}) => {
    return val.name;
  };

  getToolResourceDisplayName(tool: string) {
    return this.translate.get(getToolResourceDisplayName(tool));
  }
}
