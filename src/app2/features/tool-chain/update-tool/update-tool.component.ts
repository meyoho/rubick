import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { get } from 'lodash-es';
import { from, Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { CredentialCreateDialogComponent } from 'app/features-shared/credential';
import {
  Credential,
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  SupportedSecretTypes,
  ToolChainSecret,
  ToolService,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  mapCredentialToToolChainSecret,
  mapResourceToRegistryService,
} from 'app/services/api/tool-chain/utils';
import { TranslateService } from 'app/translate/translate.service';
import { KubernetesResource } from 'app/typings';

@Component({
  selector: 'rc-update-tool',
  templateUrl: 'update-tool.component.html',
  styleUrls: ['update-tool.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UpdateToolComponent implements OnInit {
  @ViewChild('form')
  formRef: NgForm;

  secretCreatePermission: boolean;
  secretTypes = CredentialType;
  loading = false;

  formData = {
    name: this.tool.name,
    visitHost: this.tool.visitHost,
    apiHost: this.tool.host,
    allowSubscribe: this.tool.subscription,
    useSecret: this.tool.secret.name,
    secretType: this.tool.auth_type || CredentialType.BasicAuth,
    projectName: this.tool.projectName,
    secret: this.tool.secret,
    roleSyncEnabled: this.tool.roleSyncEnabled,
  };

  private secretsUpdated$$ = new Subject<CredentialType>();

  secrets$ = this.secretsUpdated$$.pipe(startWith(null)).pipe(
    switchMap(() => this.secretApi.find({})),
    map(res =>
      res.items.filter(item => item.type === this.formData.secretType),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    @Inject(DIALOG_DATA) public tool: ToolService,
    private toolChainApi: ToolChainApiService,
    private dialogRef: DialogRef<
      UpdateToolComponent,
      {
        name: string;
        visitHost: string;
        apiHost?: string;
        allowSubscribe?: boolean;
        useSecret?: boolean;
        secretType?: string;
        secret?: ToolChainSecret;
      }
    >,
    private message: MessageService,
    private notification: NotificationService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private secretApi: CredentialApiService,
    private roleUtil: RoleUtilitiesService,
    private dialog: DialogService,
  ) {}

  async ngOnInit() {
    [
      this.secretCreatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_secret',
      {},
      ['create'],
    );
  }

  supportedSecretType(type: string) {
    return (get(this.tool, 'setting.supportedSecretTypes') || [])
      .map((item: SupportedSecretTypes) => item.type)
      .includes(type);
  }

  update() {
    this.formRef.onSubmit(null);
    if (this.formRef.invalid) {
      return;
    }
    this.loading = true;
    const currentData = this.formData;
    from(
      this.toolChainApi.updateTool(this.tool.kind, {
        ...this.tool,
        ...currentData,
      }),
    ).subscribe(
      (res: KubernetesResource) => {
        this.loading = false;
        this.cdr.markForCheck();
        this.message.success(
          this.translate.get('tool_chain_update_successful'),
        );
        this.dialogRef.close(mapResourceToRegistryService(this.tool.kind, res));
      },
      (error: any) => {
        this.loading = false;
        this.cdr.markForCheck();
        this.notification.error({
          title: this.translate.get('update_failed'),
          content: error.errors[0].message,
        });
      },
    );
  }

  resetSecret(credential?: Credential) {
    if (credential) {
      this.formData.secretType = credential.type;
      this.formData.secret = mapCredentialToToolChainSecret(credential);
    } else {
      this.formData.secret = {
        name: '',
        namespace: '',
      };
    }
    this.secretsUpdated$$.next();
  }

  addSecret() {
    const dialogRef = this.dialog.open(CredentialCreateDialogComponent, {
      data: {
        types: [this.formData.secretType],
        showHint: false,
      },
    });

    dialogRef.afterClosed().subscribe((credential?: Credential) => {
      if (credential) {
        this.resetSecret(credential);
      }
    });
  }

  secretToValue(credential: Credential) {
    this.formData = {
      ...this.formData,
      secret: mapCredentialToToolChainSecret(credential),
    };
  }
}
