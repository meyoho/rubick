import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
@Component({
  template: 'Redirecting ...',
})
export class HomeComponent implements OnInit {
  constructor(private router: Router) {}

  ngOnInit(): void {
    // TODO: 写在导航配置里
    this.router.navigateByUrl('console/admin/cluster', { replaceUrl: true });
  }
}
