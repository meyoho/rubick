import { NavItemConfig } from '@alauda/ui';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  ChangeDetectorRef,
  Component,
  Inject,
  Injector,
  OnDestroy,
  OnInit,
} from '@angular/core';

import { debounce } from 'lodash-es';
import { Observable } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { AccountService } from 'app/services/api/account.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RouterUtilService } from 'app/services/router-util.service';
import { BaseLayoutComponent } from 'app/shared/layout/base-layout-component';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';

@Component({
  selector: 'rc-layout',
  templateUrl: 'layout.component.html',
  styleUrls: ['../../app/shared/layout/layout.common.scss'],
  animations: [
    trigger('scaleInOut', [
      state(
        '0',
        style({ transform: 'scale(0.8)', opacity: '0', display: 'none' }),
      ),
      state('1', style({ transform: 'scale(1)', opacity: '1' })),
      transition('0 => 1', [
        animate('.1s', style({ transform: 'scale(1.1)', opacity: '1' })),
        animate('.1s', style({ transform: 'scale(1)' })),
      ]),
      transition('1 => 0', animate('.1s')),
    ]),
  ],
})
export class LayoutComponent extends BaseLayoutComponent
  implements OnInit, OnDestroy {
  routerStateName: Observable<string>;
  titleText = '';
  mouseMoveHandler: any;
  timer: number;
  destroyed: boolean;
  shouldShowRegionBadge: boolean;
  clusterName: string;

  constructor(
    injector: Injector,
    private accountService: AccountService,
    private cdr: ChangeDetectorRef,
    @Inject(ENVIRONMENTS) private env: Environments,
    public routerUtilService: RouterUtilService,
    private regionService: RegionService,
  ) {
    super(injector);
    this.initNavConfig('admin', segment => `/console/admin/${segment}`);
    this.initClusterNameObserver();
  }

  ngOnInit() {
    super.ngOnInit();
    this.initMouseMoveHandler();
    document.addEventListener('mousemove', this.mouseMoveHandler);
  }

  ngOnDestroy() {
    super.ngOnDestroy();
    this.destroyed = true;
    clearTimeout(this.timer);
    document.removeEventListener('mousemove', this.mouseMoveHandler);
  }

  handleActivatedItemChange(config: NavItemConfig) {
    this.router.navigateByUrl('/console/admin/' + config.routerLink);
  }

  getClusterName() {
    return this.activatedRoute.snapshot.params.cluster;
  }

  private _mouseMoveHandler() {
    if (this.destroyed) {
      return;
    }
    this.resetTimer();
  }

  private resetTimer() {
    clearTimeout(this.timer);
    const latencyMinutes = this.env.auto_logout_latency;
    this.timer = window.setTimeout(
      () => this.accountService.logout(),
      latencyMinutes * 60 * 1000,
    );
  }

  private initMouseMoveHandler() {
    this.mouseMoveHandler = debounce(() => {
      this._mouseMoveHandler();
    }, 200);
    this.resetTimer();
  }

  private initClusterNameObserver() {
    this.regionService.region$
      .pipe(takeUntil(this.destroy$))
      .subscribe((cluster: Cluster) => {
        const clusterNameInUrl = this.activatedRoute.snapshot.params.cluster;
        if (clusterNameInUrl && cluster.name !== clusterNameInUrl) {
          // redirect
          this.redirectWithClusterName(cluster.name);
        }
      });
    this.activatedRoute.params.subscribe(params => {
      this.shouldShowRegionBadge = !!params.cluster;
      this.cdr.markForCheck();
    });
  }
  private redirectWithClusterName(clusterName: string) {
    const tree = this.routerUtilService.createUrlTreeWithClusterParam(
      clusterName,
      this.router.url,
    );
    this.router.navigateByUrl(tree);
  }
}
