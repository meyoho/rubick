import { LayoutModule as AuiLayoutModule, PlatformNavModule } from '@alauda/ui';
import { ScrollDispatchModule } from '@angular/cdk/scrolling';
import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedLayoutModule } from 'app/shared/layout/module';
import { SharedModule } from 'app/shared/shared.module';
import { RegionBadgeComponent } from 'app2/layout/region-badge/region-badge.component';
import { RegionBadgeOptionService } from 'app2/shared/directives/region-badge-option/region-badge-option.service';

import { LayoutComponent } from './layout.component';

/**
 * For fundamental page Layout.
 */
@NgModule({
  providers: [RegionBadgeOptionService],
  imports: [
    RouterModule,
    ScrollDispatchModule,
    SharedModule,
    AuiLayoutModule,
    SharedLayoutModule,
    PlatformNavModule,
  ],
  exports: [LayoutComponent],
  declarations: [LayoutComponent, RegionBadgeComponent],
  schemas: [CUSTOM_ELEMENTS_SCHEMA],
})
export class LayoutModule {}
