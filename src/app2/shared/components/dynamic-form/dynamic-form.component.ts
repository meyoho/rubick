import {
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
  FormGroupDirective,
} from '@angular/forms';

import { Subscription } from 'rxjs';
import { startWith } from 'rxjs/operators';

import { DynamicFormFieldDefinition } from 'app2/shared/form-field-control';

@Component({
  selector: 'rc-dynamic-form',
  templateUrl: './dynamic-form.component.html',
  providers: [DynamicFormComponent],
})
export class DynamicFormComponent implements OnChanges, OnDestroy {
  @Input()
  fieldModels: DynamicFormFieldDefinition[] = [];
  /**
   * Embedded NgForm. Will only available after view init.
   */
  @ViewChild('ngForm')
  ngForm: FormGroupDirective;

  get form() {
    return this._form;
  }

  get valid() {
    return this.ngForm && this.ngForm.valid;
  }

  get submitted() {
    return this.ngForm && this.ngForm.submitted;
  }

  /** Should the user be able to submit the form? */
  get errorState() {
    return !this.valid && (this.submitted || this.ngForm.dirty);
  }

  get value() {
    return this.ngForm && this.ngForm.value;
  }

  /** The form model will be emitted when submitted */
  @Output()
  submit = new EventEmitter(true);

  /** Emit events when status changed. */
  @Output()
  statusChanges = new EventEmitter(true);

  /** Emit events when value changed. */
  @Output()
  valueChanges = new EventEmitter(true);

  private _form: FormGroup;
  private subscriptions: Subscription[] = [];

  onSubmit($event: Event): boolean {
    return !!this.ngForm && this.ngForm.onSubmit($event);
  }

  onReset() {
    if (this.ngForm) {
      this.ngForm.onReset();
    }
  }

  setValue(value: any) {
    if (this.ngForm && this.ngForm.control) {
      this.ngForm.control.patchValue(value);
    }
  }

  ngOnChanges({ fieldModels }: SimpleChanges) {
    if (fieldModels) {
      this.onFormFieldChanges();
    }
  }

  ngOnDestroy(): void {
    this.subscriptions.forEach(sub => sub.unsubscribe());
  }

  private registerFormObservables() {
    this.subscriptions.forEach(sub => sub.unsubscribe());

    const ngSubmitSub: Subscription = this.ngForm.ngSubmit.subscribe(() => {
      this.submit.emit(this.ngForm.value);
      this.statusChanges.emit();
    });

    const statusChangesSub = this.ngForm.statusChanges.subscribe(() => {
      this.statusChanges.emit();
    });

    const valueChangesSub = this.ngForm.valueChanges
      .pipe(startWith(this.value))
      .subscribe((value: any) => {
        this.valueChanges.emit(value);
      });

    this.subscriptions = [ngSubmitSub, statusChangesSub, valueChangesSub];
  }

  fieldTrackBy(_: number, { name }: DynamicFormFieldDefinition) {
    return name;
  }

  constructor(
    private formBuilder: FormBuilder,
    private cdr: ChangeDetectorRef,
  ) {}

  private onFormFieldChanges() {
    this._form = this.toFormGroup(this.fieldModels);
    this.cdr.detectChanges();

    this.valueChanges.emit(this.value);
    this.statusChanges.emit();

    this.registerFormObservables();
  }

  /**
   * Convert model array to (controlName => FormControl) mapping.
   * Note: the existing form control with the same name will be used to preserve
   * states.
   */
  private toFormGroup(fieldModels: DynamicFormFieldDefinition[]) {
    const controlConfigs = fieldModels.reduce((accum, field) => {
      let control: AbstractControl = this.form && this.form.get(field.name);
      control = control || new FormControl(field.value, field.validators);
      return { ...accum, [field.name]: control };
    }, {});

    return this.formBuilder.group(controlConfigs);
  }
}
