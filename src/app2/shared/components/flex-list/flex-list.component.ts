import {
  Component,
  HostBinding,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { MessagerService } from 'app2/shared/components/flex-list/messager.service';

@Component({
  // tslint:disable-next-line
  selector: '[rcFlexList]',
  template:
    '<ng-content></ng-content><div *ngFor="let i of arr(rcFlexHolderCount)" rcFlexCell [rcFlexCellHolder]="true"></div>',
  styleUrls: ['flex-list.component.scss'],
  providers: [MessagerService],
})
export class FlexListComponent implements OnChanges {
  @Input()
  rcFlexCellMargin: [number, number] = [20, 20];
  @Input()
  rcFlexCellWidth = '25%';
  @Input()
  rcFlexCellHeight = 'auto';
  @Input()
  rcFlexHolderCount = 10;

  @HostBinding('class.rc-flex-list')
  readonly bindClass = true;
  @HostBinding('style.margin-left')
  @HostBinding('style.margin-right')
  margin = -this.rcFlexCellMargin[0] / 2 + 'px';

  arr = Array;

  constructor(private messagerService: MessagerService) {
    this.messagerService.updateMargin(this.rcFlexCellMargin);
    this.messagerService.updateWidth(this.rcFlexCellWidth);
    this.messagerService.updateHeight(this.rcFlexCellHeight);
  }

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.rcFlexCellMargin) {
      this.margin = -changes.rcFlexCellMargin.currentValue[0] / 2 + 'px';
      this.messagerService.updateMargin(changes.rcFlexCellMargin.currentValue);
    }
    if (changes.rcFlexCellWidth) {
      this.messagerService.updateWidth(changes.rcFlexCellWidth.currentValue);
    }
    if (changes.rcFlexCellHeight) {
      this.messagerService.updateHeight(changes.rcFlexCellHeight.currentValue);
    }
  }
}
