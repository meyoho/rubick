import { MenuComponent } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';

@Component({
  selector: 'rc-menu-trigger',
  templateUrl: './menu-trigger.component.html',
  styleUrls: ['./menu-trigger.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class MenuTriggerComponent {
  @Input()
  size: string;
  @Input()
  menu: MenuComponent;
  @Input()
  context: { [key: string]: any };
  @Output()
  menuTriggerShow = new EventEmitter<void>();
}
