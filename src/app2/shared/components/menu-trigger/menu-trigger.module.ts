import { ButtonModule, DropdownModule, IconModule } from '@alauda/ui';
import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { MenuTriggerComponent } from './menu-trigger.component';

@NgModule({
  imports: [CommonModule, ButtonModule, DropdownModule, IconModule],
  declarations: [MenuTriggerComponent],
  exports: [MenuTriggerComponent],
})
export class MenuTriggerModule {}
