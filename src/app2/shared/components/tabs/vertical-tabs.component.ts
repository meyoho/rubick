import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import {
  AfterContentInit,
  Component,
  ContentChild,
  ContentChildren,
  Directive,
  EventEmitter,
  Input,
  Output,
  QueryList,
  TemplateRef,
} from '@angular/core';

const _Tab_content_animation = trigger('activeState', [
  state(
    'active',
    style({
      transform: 'translate3d(0, 0, 0)',
    }),
  ),
  state(
    'inactive',
    style({
      transform: 'translate3d(100%, 0, 0)',
    }),
  ),
  transition('active<=>inactive', animate('0.5s ease-in-out')),
]);

@Directive({
  selector: '[rcTabNav]',
})
export class TabNavDirective {}

@Component({
  selector: 'rc-vertical-tab',
  styleUrls: ['./vertical-tabs.component.scss'],
  templateUrl: './vertical-tab.component.html',
  animations: [_Tab_content_animation],
})
export class VerticalTabComponent {
  _state = 'inactive';
  @ContentChild(TabNavDirective, { read: TemplateRef })
  tabNavRef: TemplateRef<any>;
}

@Component({
  selector: 'rc-vertical-tabs',
  templateUrl: './vertical-tabs.component.html',
  styleUrls: ['./vertical-tabs.component.scss'],
})
export class VerticalTabsComponent implements AfterContentInit {
  /*
   * child tab list
   */
  @ContentChildren(VerticalTabComponent)
  tabs: QueryList<VerticalTabComponent>;

  /*
   * defaut select tab index
   */
  @Input()
  defaultTabIndex: number;

  /*
   * tab changed event
   */
  @Output()
  tabChanged: EventEmitter<number> = new EventEmitter();

  ngAfterContentInit() {
    setTimeout(() => {
      if (this.defaultTabIndex && this.defaultTabIndex) {
        this.selectTab(this.tabs.toArray()[this.defaultTabIndex]);
        this.tabChanged.next(this.defaultTabIndex);
      }
    }, 0);
  }

  selectTab(tab: VerticalTabComponent, index?: number) {
    this.tabs.toArray().forEach(tab => (tab._state = 'inactive'));
    tab._state = 'active';
    this.tabChanged.next(index);
  }
}
