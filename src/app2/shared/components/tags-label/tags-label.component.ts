import {
  ChangeDetectionStrategy,
  Component,
  DoCheck,
  ElementRef,
  HostListener,
  Input,
  OnChanges,
  Renderer2,
  SimpleChanges,
} from '@angular/core';

/**
 * User could provide two types of tags to use this component.
 * Tags requires a map of tags, while tagList requires a list of key/value pairs.
 */
@Component({
  selector: 'rc-tags-label',
  templateUrl: './tags-label.component.html',
  styleUrls: ['./tags-label.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagsLabelComponent implements OnChanges, DoCheck {
  @Input()
  tags: { [key: string]: string };
  @Input()
  tagList: [string, string][] | string[];

  showToggleLabel: boolean;

  ngOnChanges({ tags }: SimpleChanges) {
    if (tags) {
      this.tagList = this.tags ? Object.entries(this.tags) : [];
    }
  }

  renderTag(tag: string | string[]) {
    return Array.isArray(tag) ? tag.filter(t => !!t).join(': ') : tag;
  }

  ngDoCheck(): void {
    const hostWidth = this.el.nativeElement.offsetWidth;
    const parentContentWidth = this.renderer.parentNode(this.el.nativeElement)
      .clientWidth;
    this.showToggleLabel = hostWidth >= parentContentWidth;
  }

  @HostListener('window:resize', ['$event'])
  onResize() {}

  constructor(private renderer: Renderer2, private el: ElementRef) {}
}
