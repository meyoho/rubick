import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';

import flatpickr from 'flatpickr';

import { TranslateService } from 'app/translate/translate.service';

const locales = require('flatpickr/dist/l10n/zh');

type Instance = flatpickr.Instance;
type CustomLocale = flatpickr.CustomLocale;
type Options = flatpickr.Options.Options;

export interface CalendarEvent {
  dates: Date[];
  dateString: string;
  instance: Instance;
  data?: any;
}

const INPUT = 'INPUT';

const HOOKS = ['onClose', 'onOpen'];

/**
 * 使用 rcCalendar 属性传入 flatpickr 配置参数，onClose/onOpen 事件由 angular 事件系统向上传递 CalendarEvent 实例
 *
 * 1. value 属性为双向绑定属性，输出选中的日期格式化字符串
 * 2. 可使用指令对外暴露的 calendar 获取到指令上下文，使用 calendarRef.calendar 获取 flatpickr 实例
 *
 * flatpickr 官方文档: {@link https://chmln.github.io/flatpickr/}
 * DEMO: {@link http://rubick-components-demo.e2equota.haproxy-40-125-201-3-alaudaorg.myalauda.cn/#/calendar}
 */
@Directive({
  selector: '[rcCalendar]',
  exportAs: 'calendar',
})
export class CalendarDirective implements AfterViewInit, OnDestroy {
  // tslint:disable-next-line:no-input-rename
  @Input('rcCalendar')
  config: Options = {};

  @Output()
  onChange: EventEmitter<CalendarEvent> = new EventEmitter();
  @Output()
  onClose: EventEmitter<CalendarEvent> = new EventEmitter();
  @Output()
  onOpen: EventEmitter<CalendarEvent> = new EventEmitter();

  @Input()
  value: string;
  @Output()
  valueChange: EventEmitter<string> = new EventEmitter();

  calendar: Instance;

  constructor(private elRef: ElementRef, private translate: TranslateService) {}

  private makeCalendarEvent(...args: any[]): CalendarEvent {
    const [dates, dateString, instance, data] = args;
    return {
      dates,
      dateString,
      instance,
      data,
    };
  }

  ngAfterViewInit() {
    const el = this.elRef.nativeElement;

    // 当不允许输入内容且当前生效的元素不是 input 时自动将元素内部的 input 设置为 readOnly 避免用户在内部 input 进行手动输入
    if (!this.config.allowInput && el.tagName !== INPUT) {
      el.querySelectorAll(INPUT).forEach(
        (el: HTMLInputElement) => (el.readOnly = true),
      );
    }
    this.calendar = flatpickr(el, {
      ...this.config,
      defaultDate: this.value || this.config.defaultDate || '',
      locale: {
        ...locales.default[this.translate.currentLang === 'en' ? 'en' : 'zh'],
        ...(this.config.locale as CustomLocale),
      },
      onChange: (...args: any[]) => {
        this.valueChange.emit(args[1]);
        this.onChange.emit(this.makeCalendarEvent(...args));
      },
      ...HOOKS.reduce((hooks, event) => {
        hooks[event] = (...args: any[]) =>
          this[event].emit(this.makeCalendarEvent(...args));
        return hooks;
      }, {}),
    }) as Instance;
  }

  ngOnDestroy() {
    this.calendar.destroy();
  }
}
