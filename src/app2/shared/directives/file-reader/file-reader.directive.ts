import {
  AfterViewInit,
  Directive,
  ElementRef,
  EventEmitter,
  Input,
  OnDestroy,
  Output,
} from '@angular/core';

@Directive({ selector: 'input[type="file"][rcFileReader]' })
export class FileReaderDirective implements AfterViewInit, OnDestroy {
  @Input()
  rcResultType: 'text' | 'binary' | 'dataurl' | 'buffer';
  @Output()
  onComplete = new EventEmitter();
  @Output()
  onError = new EventEmitter();
  @Output()
  onAbort = new EventEmitter();
  @Output()
  onSelect = new EventEmitter();
  @Output()
  onProgress = new EventEmitter();
  reader: FileReader;
  eventHandler: any;
  constructor(private el: ElementRef) {}

  ngAfterViewInit() {
    this.eventHandler = this.eventBind();
  }

  private eventBind() {
    const handler = (event: any) => {
      const file = event.target.files[0];
      if (!this.checkFileType(file.name)) {
        this.onError.emit(new Error('Invalid file type.'));
        return;
      }
      this.onSelect.emit(file);
      this.reader = new FileReader();
      this.reader.onload = () => {
        this.onComplete.emit(this.reader.result);
      };
      this.reader.onerror = () => {
        this.onError.emit(event);
      };
      this.reader.onabort = () => {
        this.onAbort.emit();
      };
      this.reader.onprogress = () => {
        this.onProgress.emit(event);
      };
      this.read(file);
    };

    this.el.nativeElement.addEventListener('change', handler, false);
    return {
      remove: () => {
        this.el.nativeElement.removeEventListener('change', handler, false);
      },
    };
  }

  private checkFileType(filename: string) {
    return /^.*\.yaml$/.test(filename);
  }

  private read(file: File) {
    switch (this.rcResultType) {
      case 'binary':
        return this.reader.readAsBinaryString(file);
        break;
      case 'dataurl':
        return this.reader.readAsDataURL(file);
        break;
      case 'buffer':
        return this.reader.readAsArrayBuffer(file);
      default:
        return this.reader.readAsText(file);
        break;
    }
  }

  ngOnDestroy() {
    this.eventHandler.remove();
  }
}
