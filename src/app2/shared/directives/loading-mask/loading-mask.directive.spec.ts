import { CommonModule } from '@angular/common';
import { Component, Input, NgModule } from '@angular/core';
import { ComponentFixture, TestBed } from '@angular/core/testing';
import { By } from '@angular/platform-browser';
import { LoadingMaskComponent } from 'app2/shared/components/loading-mask/loading-mask.component';
import { registerCleanup } from 'app2/testing/helpers.spec';

import { LoadingMaskDirective } from './loading-mask.directive';

@Component({
  template: `
    <div class="host" [rcLoadingMask]="loading">CONTENT</div>
  `,
  styles: [
    `
      div {
        width: 500px;
        height: 600px;
        position: relative;
      }
    `,
  ],
})
class TestComponent {
  @Input()
  loading: boolean;
}

@NgModule({
  imports: [CommonModule],
  declarations: [TestComponent, LoadingMaskDirective, LoadingMaskComponent],
  entryComponents: [TestComponent, LoadingMaskComponent],
  providers: [],
})
class TestModule {}

describe('LoadingMaskDirective', () => {
  let comp: TestComponent;
  let fixture: ComponentFixture<TestComponent>;
  const queryLoadingMask = () =>
    fixture.debugElement.query(By.directive(LoadingMaskComponent));

  beforeEach(() => {
    TestBed.configureTestingModule({
      declarations: [],
      imports: [TestModule],
      providers: [],
    });

    fixture = TestBed.createComponent(TestComponent);

    comp = fixture.componentInstance;
  });

  it('should show loading mask when true and vice verse', () => {
    comp.loading = true;
    fixture.detectChanges();

    const loadingMaskEl = queryLoadingMask().nativeElement;

    expect(loadingMaskEl.clientWidth).toBe(500);
    expect(loadingMaskEl.clientHeight).toBe(600);

    comp.loading = false;
    fixture.detectChanges();

    expect(loadingMaskEl.clientWidth).toBe(0);
    expect(loadingMaskEl.clientHeight).toBe(0);
  });

  it('should not render overlay when initial value is false', () => {
    comp.loading = false;
    fixture.detectChanges();

    expect(queryLoadingMask()).toBeNull();
  });

  it('will apply `loading-mask-overlay` when loading is true', () => {
    const hostElement: JQuery = $(fixture.nativeElement).find('.host');
    comp.loading = true;
    fixture.detectChanges();
    expect(hostElement.hasClass('loading-mask-overlay')).toBeTruthy();
  });

  registerCleanup();
});
