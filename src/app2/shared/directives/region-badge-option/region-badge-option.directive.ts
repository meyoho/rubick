import { Directive, Input, OnDestroy, OnInit } from '@angular/core';

import {
  RegionBadgeDirectiveOption,
  RegionBadgeOption,
  RegionBadgeOptionService,
} from 'app2/shared/directives/region-badge-option/region-badge-option.service';

/**
 * 在任意业务页面，插入以下指令，配置集群切换按钮行为。
 *
 * Usage:
 *
 * <rc-region-badge-option
 *   [onChangeConfirm]="true"
 *   filterMode="ALL">
 * </rc-region-badge-option>
 */
@Directive({
  /* tslint:disable:directive-selector */
  selector: 'rc-region-badge-option',
})
export class RegionBadgeOptionDirective
  implements OnInit, OnDestroy, RegionBadgeDirectiveOption {
  /**
   * 切换集群时是否需要用户二次确认。
   */
  @Input()
  onChangeConfirm: boolean;

  /**
   * 过滤模式。
   * 默认情况下只包含健康集群。
   */
  @Input()
  filterMode: 'ALL' | 'DEFAULT';

  /**
   * 用户自定义的完整option，一般来说用不着，除非你想更细致的调整集群切换按钮的行为。
   */
  @Input()
  option: RegionBadgeOption;

  private id: number;

  constructor(private regionBadge: RegionBadgeOptionService) {}

  ngOnInit() {
    this.regionBadge.setRegionBadgeOptionByDirective(this);
    this.id = this.regionBadge.optionCounter;
  }

  ngOnDestroy() {
    if (this.id === this.regionBadge.optionCounter) {
      this.regionBadge.setRegionBadgeOption(
        this.regionBadge.getDefaultOption(),
      );
    }
  }
}
