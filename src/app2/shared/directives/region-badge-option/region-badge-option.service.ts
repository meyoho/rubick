import { DialogService } from '@alauda/ui';
import { Injectable } from '@angular/core';

import { BehaviorSubject } from 'rxjs';

import { Cluster } from 'app/services/api/region.service';
import { TranslateService } from 'app/translate/translate.service';

/**
 * Display option for global region badge.
 */
export interface RegionBadgeOption {
  /**
   * Filter function for region list. By default, non-healthy regions will be excluded.
   */
  filterFunction?: (region: Cluster) => boolean;

  /**
   * Callback to be called when a region is about to change. If the result is false, then the
   * displayed region will stay as it is.
   */
  onChange?: (region: Cluster) => boolean | Promise<boolean>;
}

/**
 * RegionBadgeDirective指令的选项
 */
export interface RegionBadgeDirectiveOption {
  /**
   * 切换集群时是否需要用户二次确认。
   */
  onChangeConfirm: boolean;

  /**
   * 过滤模式。
   * 默认情况下只包含健康集群。
   */
  filterMode: 'ALL' | 'DEFAULT';

  /**
   * 用户自定义的完整option，一般来说用不着，除非你想更细致的调整集群切换按钮的行为。
   */
  option: RegionBadgeOption;
}

/**
 * By default, region list will be filtered by healthy state.
 */
export const DEFAULT_REGION_LIST_FILTER = (region: Cluster) =>
  region.container_manager !== 'NONE' && region.state === 'RUNNING';

@Injectable()
export class RegionBadgeOptionService {
  private regionBadgeOptionSubject = new BehaviorSubject<RegionBadgeOption>(
    this.getDefaultOption(),
  );

  /**
   * BehaviorSubject of RegionBadgeOption changes.
   */
  regionBadgeOption$ = this.regionBadgeOptionSubject.asObservable();

  /**
   * Indicator to identify the option sequence.
   */
  optionCounter = 0;

  constructor(
    private translate: TranslateService,
    private dialogService: DialogService,
  ) {}

  getDefaultOption(): RegionBadgeOption {
    return {
      filterFunction: DEFAULT_REGION_LIST_FILTER,
    };
  }

  setRegionBadgeOptionByDirective(optionDirective: RegionBadgeDirectiveOption) {
    this.setRegionBadgeOption(this.getOptionByDirective(optionDirective));
  }

  setRegionBadgeOption(option: RegionBadgeOption = this.getDefaultOption()) {
    this.optionCounter++;
    this.regionBadgeOptionSubject.next(option);
  }

  private getOptionByDirective(
    directive: RegionBadgeDirectiveOption,
  ): RegionBadgeOption {
    const option = this.getDefaultOption();

    if (directive.onChangeConfirm) {
      option.onChange = async (region: Cluster) => {
        if (directive.onChangeConfirm) {
          try {
            await this.dialogService.confirm({
              title: this.translate.get(
                'region_badge_change_region_confirm_title',
              ),
              content: this.translate.get(
                'region_badge_change_region_confirm_content',
                { name: region.display_name },
              ),
              confirmText: this.translate.get('confirm'),
              cancelText: this.translate.get('cancel'),
            });
          } catch (e) {
            // 意味着不切换集群
            return false;
          }
        }
        return true;
      };
    }
    return {
      ...option,
      filterFunction:
        directive.filterMode === 'ALL'
          ? () => true
          : DEFAULT_REGION_LIST_FILTER,
      // 以上默认值将会被用户提供的option对象覆盖
      ...directive.option,
    };
  }
}
