import {
  ComponentPortal,
  DomPortalHost,
  PortalHost,
} from '@angular/cdk/portal';
import {
  ApplicationRef,
  ComponentFactoryResolver,
  Injector,
  ViewContainerRef,
} from '@angular/core';

import {
  TooltipConfigs,
  TooltipContentComponent,
} from './tooltip-content/tooltip-content.component';

export class BaseTooltip {
  static readonly DEFAULT_HOST = document.querySelector('body');

  protected portalHost: PortalHost;
  protected portal: ComponentPortal<TooltipContentComponent>;
  protected attachedInstance: TooltipContentComponent;

  constructor(
    private injector: Injector,
    private appRef: ApplicationRef,
    private componentFactoryResolver: ComponentFactoryResolver,
    private viewContainerRef: ViewContainerRef,
  ) {}

  private createTooltip(hostElm: HTMLElement) {
    if (this.portal && this.portalHost) {
      return;
    }
    this.portalHost = new DomPortalHost(
      hostElm,
      this.componentFactoryResolver,
      this.appRef,
      this.injector,
    );
    this.portal = new ComponentPortal(
      TooltipContentComponent,
      this.viewContainerRef,
    );
  }

  protected attachTooltip(hostElm: HTMLElement = BaseTooltip.DEFAULT_HOST) {
    this.createTooltip(hostElm);
    if (!this.portalHost.hasAttached()) {
      this.attachedInstance = this.portalHost.attach(this.portal).instance;
    }
  }

  protected detachTooltip() {
    if (this.portalHost) {
      this.portalHost.detach();
    }
    this.attachedInstance = null;
  }

  protected setupConfigs(configs: TooltipConfigs) {
    if (!this.attachedInstance) {
      return;
    }
    Object.entries(configs).forEach(([key, value]) => {
      this.attachedInstance[key] = value;
    });
  }

  public updatePosition() {
    if (this.attachedInstance) {
      this.attachedInstance.updatePosition();
    }
  }
}
