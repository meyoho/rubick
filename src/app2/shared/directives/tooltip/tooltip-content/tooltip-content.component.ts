import {
  AfterViewInit,
  Component,
  ElementRef,
  OnDestroy,
  TemplateRef,
  ViewChild,
} from '@angular/core';

import Popper from 'popper.js';

export enum TooltipMode {
  Default = 'default',
  Success = 'success',
  Warning = 'warning',
  Error = 'error',
  Info = 'info',
  Empty = 'empty',
}

export interface TooltipConfigs {
  mode?: TooltipMode;
  noArrow?: boolean;
  empty?: boolean;
  reference?: HTMLElement;
  placement?: string;
  offset?: number | string;
  modifiers?: Popper.Modifiers;
}

// TODO: fade in out animation
@Component({
  selector: 'rc-tooltip-content',
  templateUrl: 'tooltip-content.component.html',
  styleUrls: ['tooltip-content.component.scss'],
})
export class TooltipContentComponent
  implements AfterViewInit, OnDestroy, TooltipConfigs {
  @ViewChild('tooltip')
  tooltip: ElementRef;
  @ViewChild('arrow')
  arrow: ElementRef;

  title: number | string;
  template: TemplateRef<any>;

  mode: TooltipMode = TooltipMode.Default;
  noArrow = false;
  empty = false;
  reference: HTMLElement;
  placement: Popper.Placement = 'right';
  offset: number | string = '0,8';
  context: any;
  modifiers: Popper.Modifiers;

  popper: Popper;

  static convertPlacementString(position: string): Popper.Placement {
    return <Popper.Placement>position
      .split(' ')
      .map((item, index) => {
        if (index === 0) {
          return item;
        } else {
          switch (item) {
            case 'left':
            case 'top':
              return 'start';
            case 'right':
            case 'bottom':
              return 'end';
            default:
              return 'center';
          }
        }
      })
      .join('-');
  }

  constructor() {}

  ngAfterViewInit() {
    this.popper = new Popper(
      this.reference,
      this.tooltip.nativeElement.parentElement,
      {
        placement: TooltipContentComponent.convertPlacementString(
          this.placement,
        ),
        modifiers: {
          arrow: {
            element: this.arrow.nativeElement,
          },
          offset: {
            offset: this.offset,
          },
          ...this.modifiers,
        },
      },
    );
  }

  ngOnDestroy() {
    this.popper.destroy();
  }

  updatePosition() {
    if (this.popper) {
      setTimeout(() => {
        this.popper.update();
      });
    }
  }
}
