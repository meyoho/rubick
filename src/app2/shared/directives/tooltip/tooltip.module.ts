import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';

import { ErrorsTooltipDirective } from './errors-tooltip.directive';
import { TooltipContentComponent } from './tooltip-content/tooltip-content.component';

@NgModule({
  imports: [CommonModule],
  declarations: [TooltipContentComponent, ErrorsTooltipDirective],
  entryComponents: [TooltipContentComponent],
  exports: [TooltipContentComponent, ErrorsTooltipDirective],
})
export class TooltipModule {}
