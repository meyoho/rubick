/* tslint:disable:directive-selector */

import { ChangeDetectorRef, Directive, OnDestroy } from '@angular/core';

import { Subscription } from 'rxjs';

import { TranslateService } from 'app/translate/translate.service';

@Directive({
  selector: 'aui-code-editor',
})
export class AuiCodeEditorHelperDirective implements OnDestroy {
  private translateSubscription: Subscription;
  constructor(
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
  ) {
    this.translateSubscription = this.translate.currentLang$.subscribe(() => {
      this.cdr.markForCheck();
    });
  }
  ngOnDestroy() {
    this.translateSubscription.unsubscribe();
  }
}
