import { Directive, Input } from '@angular/core';
import { AbstractControl, NG_VALIDATORS, Validator } from '@angular/forms';

import * as parser from 'cron-parser';

const CRON_FORMAT_ERROR = 'cronFormatError';
const CRON_MIN_INTERVAL_ERROR = 'cronMinIntervalError';

@Directive({
  selector: '[rcCron][ngModel]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: CronValidatorDirective,
      multi: true,
    },
  ],
})
export class CronValidatorDirective implements Validator {
  @Input()
  rcCron: {
    minInterval: number;
  };

  isFiledCountCorrect(exp: string) {
    const spaceCount = exp.split('').filter(val => val === ' ').length;
    const filedCount = exp.split(' ').filter(val => val).length;
    return spaceCount === 4 && filedCount === 5;
  }

  validate(ctrl: AbstractControl): { [key: string]: any } {
    const { value } = ctrl;

    if (!value) {
      return null;
    }

    if (!this.isFiledCountCorrect(value)) {
      return {
        [CRON_FORMAT_ERROR]: true,
      };
    }

    try {
      const iterator = parser.parseExpression(value);

      let minIntervalValidity = true;

      if (value && this.rcCron && this.rcCron.minInterval) {
        const next1 = iterator.next() as any;
        const next2 = iterator.next() as any;

        // minute
        if ((next2._date - next1._date) / 1000 / 60 < this.rcCron.minInterval) {
          minIntervalValidity = false;
        }
      }

      return minIntervalValidity
        ? null
        : {
            [CRON_MIN_INTERVAL_ERROR]: true,
          };
    } catch (e) {
      return {
        [CRON_FORMAT_ERROR]: true,
      };
    }
  }
}
