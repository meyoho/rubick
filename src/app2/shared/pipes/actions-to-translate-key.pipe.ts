import { Pipe, PipeTransform } from '@angular/core';

import { uniq } from 'lodash-es';

import { TranslateService } from 'app/translate/translate.service';

/**
 * Actions to translated text
 */
@Pipe({ name: 'rcActionsToTranslateKeys' })
export class ActionsToTranslateKeysPipe implements PipeTransform {
  constructor(private translateService: TranslateService) {}
  transform(actions: string[], allActions: string[]): string {
    let res: string[] = [];

    if (actions.length > 0) {
      res = uniq(
        actions.map(action => {
          return action.split(':')[1];
        }),
      );
      if (allActions && res.length === allActions.length) {
        res = ['rbac_all'];
      }
      res = res.map(act => this.translateService.get(act));
    }

    return res.join(', ');
  }
}
