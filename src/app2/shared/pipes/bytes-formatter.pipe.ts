import { Pipe, PipeTransform } from '@angular/core';

// Filter a size number to pretty format
// Eg, returns '5.54 K' for 5678
// @params: vague need exact divisible
//      eg: vague === true , 1024M return 1g,1025M return 1g
//      eg: vague === false , 1024M return 1g,1025M return 1025M
@Pipe({ name: 'rcBytesFormatter' })
export class BytesFormatterPipe implements PipeTransform {
  transform(bytes: any, precision: number, base = 1, vague = true): any {
    const units = ['', 'K', 'M', 'G', 'T', 'P'];
    if (bytes === 0) {
      return bytes;
    }
    if (bytes < 1) {
      return `${bytes}`;
    }
    bytes = parseFloat(bytes);
    if (isNaN(bytes) || !isFinite(bytes)) {
      return '?';
    }

    let unit = Math.floor(Math.log(bytes * base) / Math.log(1024));
    const minUnit = Math.floor(Math.log(base) / Math.log(1024));

    if (!vague && (bytes * base) % Math.pow(1024, unit) !== 0) {
      unit = Math.max(0, unit - 1, minUnit);
    }
    unit = Math.min(unit, units.length - 1);

    return (
      ((bytes * base) / Math.pow(1024, unit)).toFixed(+precision) +
      ' ' +
      units[unit]
    );
  }
}
