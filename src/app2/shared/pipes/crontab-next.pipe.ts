import { Pipe, PipeTransform } from '@angular/core';

import * as parser from 'cron-parser';
import * as moment from 'moment';

@Pipe({
  name: 'rcCrontabNext',
})
export class CrontabNextPipe implements PipeTransform {
  isFiledCountCorrect(exp: string) {
    const spaceCount = exp.split('').filter(val => val === ' ').length;
    const filedCount = exp.split(' ').filter(val => val).length;
    return spaceCount === 4 && filedCount === 5;
  }

  transform(
    rule: string,
    step = 1,
    options = {
      utc: false,
      format: 'YYYY-MM-DD HH:mm:ss',
      tz: 'Asia/Shanghai',
    },
  ) {
    const INPUT_FORMAT = 'ddd MMM DD YYYY HH:mm:ss';

    let dateString = 'N/A';
    if (!rule || !this.isFiledCountCorrect(rule)) {
      return dateString;
    }
    try {
      const interval = parser.parseExpression(rule, options);
      for (let i = 0; i < step; i++) {
        dateString = interval.next().toString();
      }
      const momentInstance = moment(dateString, INPUT_FORMAT, 'en');
      if (options.utc) {
        dateString = momentInstance.utcOffset(0).format(options.format);
      } else if (options.tz === 'Asia/Shanghai') {
        dateString = momentInstance.utcOffset(480).format(options.format);
      } else {
        dateString = momentInstance.format(options.format);
      }
    } catch (err) {
      dateString = 'N/A';
    }
    return dateString;
  }
}
