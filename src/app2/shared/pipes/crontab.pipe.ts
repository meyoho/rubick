import { Pipe, PipeTransform } from '@angular/core';

import parser from 'cron-parser';
import moment from 'moment';

@Pipe({
  name: 'rcCrontabParse',
})
export class CrontabParsePipe implements PipeTransform {
  format = 'ddd MMM DD YYYY HH:mm:ss GMTZ';
  transform(
    rule: string,
    step = 1,
    options = {
      utc: false,
      format: 'YYYY-MM-DD HH:mm:ss',
      tz: 'Asia/Shanghai',
    },
  ) {
    let ret = '-';
    if (!rule || !this.isFiledCountCorrect(rule)) {
      return ret;
    }
    try {
      const iterator = parser.parseExpression(rule, options);
      for (let i = 0; i < step; i++) {
        ret = iterator.next().toString();
      }
      const momentInstance = moment(ret, this.format, 'en');
      if (options.utc) {
        ret = momentInstance.utcOffset(0).format(options.format) + ' UTC';
      } else if (options.tz === 'Asia/Shanghai') {
        ret = momentInstance.utcOffset(480).format(options.format) + ' GMT+8';
      } else {
        ret = momentInstance.format(options.format);
      }
    } catch (error) {}
    return ret;
  }

  private isFiledCountCorrect(exp: string) {
    const spaceCount = exp.split('').filter(val => val === ' ').length;
    const filedCount = exp.split(' ').filter(val => val).length;
    return spaceCount === 4 && filedCount === 5;
  }
}
