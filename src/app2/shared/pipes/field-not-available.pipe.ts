import { Pipe, PipeTransform } from '@angular/core';

/**
 * Pipe a title sentence to correct cases.
 */
@Pipe({ name: 'rcFieldNotAvailable' })
export class FieldNotAvailablePipe implements PipeTransform {
  constructor() {}

  transform(field: string | number): string | number {
    if (field === null || field === undefined || `${field}`.trim() === '') {
      return '-';
    }
    return field;
  }
}
