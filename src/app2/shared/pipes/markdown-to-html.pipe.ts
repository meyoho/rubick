import { Pipe, PipeTransform } from '@angular/core';

import marked from 'marked';

/**
 * Format a UTC string.
 */
@Pipe({ name: 'rcMarkdownToHtml' })
export class MarkdownToHtmlPipe implements PipeTransform {
  transform(str: string): string {
    return marked(str);
  }
}
