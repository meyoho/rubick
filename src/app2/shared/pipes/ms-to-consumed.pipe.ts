import { Pipe, PipeTransform } from '@angular/core';

import moment from 'moment';

/**
 * UTC string to consumed time.
 */
@Pipe({ name: 'rcMsToConsumed' })
export class MsToConsumedPipe implements PipeTransform {
  transform(ms: string): string {
    if (!ms) {
      return '-';
    }
    return ms ? moment(ms).fromNow() : '';
  }
}
