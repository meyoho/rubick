import { Pipe, PipeTransform } from '@angular/core';

/**
 * Transform an Object to Map.
 * Useful for iterating an Object with keys using ngFor
 */
@Pipe({ name: 'rcObjectToMap' })
export class ObjectToMapPipe implements PipeTransform {
  transform(object: { [index: string]: any }): Map<string, any> {
    return new Map(Object.entries(object));
  }
}
