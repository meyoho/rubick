import { Pipe, PipeTransform } from '@angular/core';

import { TimeService } from 'app/services/time.service';

@Pipe({ name: 'rcRelativeTime', pure: false })
export class RelativeTimePipe implements PipeTransform {
  constructor(private time: TimeService) {}

  transform(value: string): any {
    if (!value) {
      return '-';
    }
    return this.time.transformRelative(value, { addSuffix: true });
  }
}
