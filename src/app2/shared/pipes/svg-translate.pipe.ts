import { Pipe, PipeTransform } from '@angular/core';

@Pipe({ name: 'rcSvgTranslate' })
export class SvgTranslatePipe implements PipeTransform {
  transform(matrix: [number, number]): string {
    const [x, y] = matrix;
    return `translate(${x}, ${y})`;
  }
}
