import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

/**
 * Pipe a style string to safe state.
 *
 * Angular has some native security concerns on how to interpolate html content,
 * we shall bypass it with some magic.
 *
 * Credit:
 * - http://stackoverflow.com/questions/39628007/angular2-innerhtml-binding-remove-style-attribute
 */
@Pipe({ name: 'rcTrustStyle' })
export class TrustStylePipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(html: string): SafeHtml {
    return this.sanitizer.bypassSecurityTrustStyle(html);
  }
}
