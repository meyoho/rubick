import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeHtml } from '@angular/platform-browser';

/**
 * Pipe a URL string to safe state.
 *
 * Angular has some native security concerns on how to interpolate html content,
 * we shall bypass it with some magic.
 *
 * Credit:
 * - http://stackoverflow.com/questions/39628007/angular2-innerhtml-binding-remove-style-attribute
 */
@Pipe({ name: 'rcTrustUrl' })
export class TrustUrlPipe implements PipeTransform {
  constructor(private sanitizer: DomSanitizer) {}

  transform(url: string): SafeHtml {
    return url ? this.sanitizer.bypassSecurityTrustUrl(url) : undefined;
  }
}
