import { Pipe, PipeTransform } from '@angular/core';
import { DomSanitizer, SafeUrl } from '@angular/platform-browser';

import { Observable, of } from 'rxjs';
import { map, switchMap } from 'rxjs/operators';

import { HttpService } from 'app/services/http.service';

const default_account_icon = require('../../../static/images/user/default-account-logo.svg');

/**
 * Proxy image url inside docker container.
 */
@Pipe({ name: 'rcUrlProxy' })
export class UrlProxy implements PipeTransform {
  constructor(private http: HttpService, private domSanitizer: DomSanitizer) {}

  transform(path: string | Observable<string>): Observable<SafeUrl> {
    return (path instanceof Observable ? path : of(path)).pipe(
      switchMap((url: string) => {
        if (!url) {
          return of(default_account_icon);
        }
        if (!url.startsWith('http')) {
          return of(url);
        }
        return this.http
          .request('/ajax/image-proxy', {
            params: {
              url,
            },
            method: 'GET',
            cache: true,
          })
          .then(({ body }) => body)
          .catch(() => default_account_icon);
      }),
      map(url => this.domSanitizer.bypassSecurityTrustUrl(url)),
    );
  }
}
