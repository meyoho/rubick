import { Pipe, PipeTransform } from '@angular/core';

import moment from 'moment';
import 'moment/locale/zh-cn';

/**
 * UTC string to consumed time.
 */
@Pipe({ name: 'rcUtcToConsumed' })
export class UtcToConsumedPipe implements PipeTransform {
  transform(utcStr: string, lang: string = 'en'): string {
    if (!utcStr) {
      return '-';
    }
    return utcStr
      ? moment
          .utc(utcStr)
          .locale(lang)
          .fromNow()
      : '';
  }
}
