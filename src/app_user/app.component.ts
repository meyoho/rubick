import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  ViewEncapsulation,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { AccountService } from 'app/services/api/account.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import * as fromStore from 'app/store';
import { Environments } from 'app/typings';
import { debounce } from 'lodash-es';
@Component({
  selector: 'rc-app-root',
  template: '<router-outlet></router-outlet>',
  encapsulation: ViewEncapsulation.None,
  styleUrls: ['./app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  constructor(
    private store: Store<fromStore.AppState>,
    private accountService: AccountService,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}
  timer: number;
  mouseMoveHandler: any;
  destroyed: boolean;

  ngOnInit() {
    this.store.dispatch(new fromStore.LoadProjects());
    this.mouseMoveHandler = debounce(() => {
      this._mouseMoveHandler();
    }, 200);
    document.addEventListener('mousemove', this.mouseMoveHandler);
    this.resetTimer();
  }

  ngOnDestroy() {
    this.destroyed = true;
    document.removeEventListener('mousemove', this.mouseMoveHandler);
    clearTimeout(this.timer);
  }

  private _mouseMoveHandler() {
    if (this.destroyed) {
      return;
    }
    this.resetTimer();
  }

  private resetTimer() {
    clearTimeout(this.timer);
    const latencyMinutes = this.env.auto_logout_latency;
    this.timer = window.setTimeout(
      () => this.accountService.logout(),
      latencyMinutes * 60 * 1000,
    );
  }
}
