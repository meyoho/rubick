import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';
import { AppComponent } from 'app_user/app.component';
import { AppRoutingModule } from 'app_user/app.routing.module';

@NgModule({
  imports: [SharedModule, AppRoutingModule],
  declarations: [AppComponent],
})
export class AppModule {}
