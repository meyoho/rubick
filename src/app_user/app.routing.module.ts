import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AppComponent } from 'app_user/app.component';

import { ProjectGuard } from './guards';

export const routes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: 'project',
        canActivate: [ProjectGuard],
        loadChildren: './features/project/project.module#ProjectModule',
      },
      {
        path: 'workspace',
        canActivate: [ProjectGuard],
        loadChildren: './features/workspace/workspace.module#WorkspaceModule',
      },
      {
        path: '',
        redirectTo: 'project',
        pathMatch: 'full',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
