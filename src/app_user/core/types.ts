import { HttpErrorResponse } from '@angular/common/http';

/**
 * @deprecated
 */
export interface Resource {
  type?: string;
  kind?: string;
  // TODO: 参考 http://confluence.alaudatech.com/display/DEV/All+Resources+YAML 目前application和其他k8s资源返回结构尚未统一，待完善
}

/**
 * TODO: to be moved to app/typings
 */
export interface ResourceList<T = any> {
  results: T[];
  count: number;
  num_pages?: number;
  page_size?: number;
}

export interface ListMeta {
  count: number;
  num_pages: number;
}

export interface PageParams {
  pageIndex: number; // 1 based
  pageSize?: number;
}

export function isHttpErrorResponse(res: any): res is HttpErrorResponse {
  return res instanceof HttpErrorResponse;
}

export function isNotHttpErrorResponse<T>(
  res: T | HttpErrorResponse,
): res is T {
  return !isHttpErrorResponse(res);
}

export interface BatchReponse {
  responses: {
    [key: string]: {
      body: string;
      code: number;
    };
  };
}

/***** to be moved to app/typings ******/

/**
 * @deprecated
 */
export interface BatchApi {
  responses: {
    [s: string]: {
      body: string;
      code: number;
      headers: any;
    };
  };
}
