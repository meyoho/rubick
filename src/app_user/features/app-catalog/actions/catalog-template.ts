import { Action } from '@ngrx/store';

import {
  AppCatalogTemplate,
  AppCatalogTemplateRepository,
} from 'app/services/api/app-catalog.service';

export enum ListPageViewMode {
  // Show import progress
  Import = 'IMPORT',

  // Show templates
  Templates = 'TEMPLATES',
}

export interface AppListParams {
  templateId: string;
  search: string;
}

export enum ListPageActionType {
  ModeChange = '@catalog.template/list-page/mode-change',
  TemplateIdChange = '@catalog.template/list-page/selected-template-id-change',
  AppSearchChange = '@catalog.template/list-page/app-search-change',
}

export enum RepoActionType {
  Load = '@catalog.template/repo/load',
  LoadComplete = '@catalog.template/repo/load-complete',
  LoadError = '@catalog.template/repo/load-error',
  Update = '@catalog.template/repo-update',
  UpdateComplete = '@catalog.template/repo/update-complete',
  UpdateError = '@catalog.template/repo/update-error',
}

export enum TemplateActionType {
  Load = '@catalog.template/template/load',
  LoadError = '@catalog.template/template/load-error',
  LoadComplete = '@catalog.template/template/load-complete',
}

export class ListPageModeChangeAction implements Action {
  readonly type = ListPageActionType.ModeChange;
  constructor(public payload: ListPageViewMode) {}
}

export class ListPageTemplateIdChangeAction implements Action {
  readonly type = ListPageActionType.TemplateIdChange;
  constructor(public payload: string) {}
}

export class ListPageAppSearchChangeAction implements Action {
  readonly type = ListPageActionType.AppSearchChange;
  constructor(public payload: string) {}
}

export class RepoLoadAction implements Action {
  readonly type = RepoActionType.Load;
  constructor(public payload: string) {}
}

export class RepoLoadCompleteAction implements Action {
  readonly type = RepoActionType.LoadComplete;

  constructor(public payload: AppCatalogTemplateRepository) {}
}

export class RepoLoadErrorAction implements Action {
  readonly type = RepoActionType.LoadError;

  constructor(public payload: any) {}
}

export class TemplateLoadAction implements Action {
  readonly type = TemplateActionType.Load;

  constructor(
    public payload: string, // UUID
  ) {}
}

export class TemplateLoadCompleteAction implements Action {
  readonly type = TemplateActionType.LoadComplete;

  constructor(public payload: AppCatalogTemplate) {}
}

export class TemplateLoadErrorAction implements Action {
  readonly type = TemplateActionType.LoadError;

  constructor(public payload: { error: Error; uuid: string }) {}
}

export type Actions =
  | ListPageModeChangeAction
  | ListPageTemplateIdChangeAction
  | ListPageAppSearchChangeAction
  | RepoLoadAction
  | RepoLoadCompleteAction
  | RepoLoadErrorAction
  | TemplateLoadAction
  | TemplateLoadCompleteAction
  | TemplateLoadErrorAction;
