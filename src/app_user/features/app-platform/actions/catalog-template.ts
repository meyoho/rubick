import { Action } from '@ngrx/store';

import {
  AppCatalogTemplate,
  AppCatalogTemplateRepository,
} from 'app/services/api/app-catalog.service';

export enum ListPageViewMode {
  // Show import progress
  Import = 'IMPORT',

  // Show empty templates view
  Empty = 'EMPTY',

  // Show templates
  Templates = 'TEMPLATES',
}

export interface AppListParams {
  templateId: string;
  search: string;
}

export enum ListPageActionType {
  ModeChange = '@catalog.public.template/list-page/mode-change',
  TemplateIdChange = '@catalog.public.template/list-page/selected-template-id-change',
  AppSearchChange = '@catalog.public.template/list-page/app-search-change',
}

export enum RepoActionType {
  Load = '@catalog.public.template/repo/load',
  LoadComplete = '@catalog.public.template/repo/load-complete',
  LoadError = '@catalog.public.template/repo/load-error',
  Update = '@catalog.public.template/repo-update',
  UpdateComplete = '@catalog.public.template/repo/update-complete',
  UpdateError = '@catalog.public.template/repo/update-error',
  Import = '@catalog.public.template/repo/import',
  ImportStarted = '@catalog.public.template/repo/import-started',
  ImportError = '@catalog.public.template/repo-import-error',
  Refresh = '@catalog.public.template/repo/refresh',
  RefreshComplete = '@catalog.public.template/repo/refresh-complete',
  RefreshError = '@catalog.public.template/repo/refresh-error',
}

export enum TemplateActionType {
  Load = '@catalog.public.template/template/load',
  LoadError = '@catalog.pubic.template/template/load-error',
  LoadComplete = '@catalog.public.template/template/load-complete',
}

export class ListPageModeChangeAction implements Action {
  readonly type = ListPageActionType.ModeChange;
  constructor(public payload: ListPageViewMode) {}
}

export class ListPageTemplateIdChangeAction implements Action {
  readonly type = ListPageActionType.TemplateIdChange;
  constructor(public payload: string) {}
}

export class ListPageAppSearchChangeAction implements Action {
  readonly type = ListPageActionType.AppSearchChange;
  constructor(public payload: string) {}
}

export class RepoLoadAction implements Action {
  readonly type = RepoActionType.Load;
  constructor(public payload: string) {}
}

export class RepoLoadCompleteAction implements Action {
  readonly type = RepoActionType.LoadComplete;

  constructor(public payload: AppCatalogTemplateRepository) {}
}

export class RepoLoadErrorAction implements Action {
  readonly type = RepoActionType.LoadError;

  constructor(public payload: any) {}
}

export class TemplateLoadAction implements Action {
  readonly type = TemplateActionType.Load;

  constructor(
    public payload: string, // UUID
  ) {}
}

export class TemplateLoadCompleteAction implements Action {
  readonly type = TemplateActionType.LoadComplete;

  constructor(public payload: AppCatalogTemplate) {}
}

export class TemplateLoadErrorAction implements Action {
  readonly type = TemplateActionType.LoadError;

  constructor(public payload: { error: Error; uuid: string }) {}
}

export type Actions =
  | ListPageModeChangeAction
  | ListPageTemplateIdChangeAction
  | ListPageAppSearchChangeAction
  | RepoLoadAction
  | RepoLoadCompleteAction
  | RepoLoadErrorAction
  | TemplateLoadAction
  | TemplateLoadCompleteAction
  | TemplateLoadErrorAction;
