import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';

import { AppPlatformTemplateEmptyViewComponent } from './components/empty-view/app-platform-template-empty-view.component';
import { AppPlatformAppCreatePageComponent } from './components/pages/app-platform-create-app-page.component';
import { AppPlatformTemplateListPageComponent } from './components/pages/app-platform-template-list-page.component';
import { AppPlatformTemplateCardComponent } from './components/template-card/app-platform-template-card.component';

const components = [
  AppPlatformTemplateListPageComponent,
  AppPlatformAppCreatePageComponent,
  AppPlatformTemplateEmptyViewComponent,
  AppPlatformTemplateCardComponent,
];

@NgModule({
  imports: [SharedModule],
  declarations: components,
  exports: components,
})
export class AppPlatformSharedModule {}
