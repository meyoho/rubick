import { DialogService, DialogSize } from '@alauda/ui';
import { Component, OnInit, TemplateRef, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs';
import { filter, map, pluck, switchMap } from 'rxjs/operators';

import { AppCatalogService } from 'app/services/api/app-catalog.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { YamlCommentFormComponent } from 'app2/shared/components/yaml-comment-form/yaml-comment-form.component';
import { PlatFormUtilService } from 'app_user/features/app-platform/platform-util.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { TemplateLoadAction } from '../../actions/catalog-template';
import * as fromAppCatalog from '../../reducers';
import * as catalogTemplate from '../../reducers/catalog-template';

@Component({
  templateUrl: 'app-platform-create-app-page.component.html',
  styleUrls: ['app-platform-create-app-page.component.scss'],
})
export class AppPlatformAppCreatePageComponent implements OnInit {
  templateId$ = this.activatedRoute.params.pipe(
    pluck<Params, string>('template_id'),
  );
  templateState$: Observable<
    catalogTemplate.TemplateState
  > = this.templateId$.pipe(
    switchMap(uuid =>
      this.store
        .select(fromAppCatalog.getTemplateStateById)
        .pipe(map(storeFn => storeFn(uuid))),
    ),
  );
  yaml$: Observable<string> = this.templateState$.pipe(
    filter(
      state =>
        state.template &&
        state.template.versions &&
        state.template.versions.length > 0,
    ),
    map(state => {
      this.catalogPayload.template.version.uuid =
        state.template.versions[0].uuid;
      this.catalogPayload.template.name = state.template.name;

      Object.assign(this.basic_info, {
        display_name: state.template.display_name,
        description: state.template.description,
        name: state.template.name,
        version: state.template.versions[0].version,
        icon: state.template.icon,
      });
      return state.template.versions[0].values_yaml_content;
    }),
  );
  showLoadingMask$: Observable<boolean> = this.templateState$.pipe(
    map(state => state.loading && !state.template),
  );
  showComment = false;
  submitting = false;
  clusterName: string;
  namespaceName: string;
  basic_info = {
    display_name: '',
    name: '',
    version: '',
    description: '',
    icon: '',
  };
  catalogPayload = {
    name: '',
    display_name: '',
    values_yaml_content: '',
    template: {
      uuid: '',
      name: '',
      version: {
        uuid: '',
      },
    },
    namespace: {
      name: '',
    },
    cluster: {
      name: '',
    },
  };
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: true,
  };

  @ViewChild('appCatalogForm')
  form: NgForm;
  @ViewChild('yamlModalTemplate', { read: TemplateRef })
  yamlModalTemplateRef: TemplateRef<any>;
  @ViewChild(YamlCommentFormComponent)
  yamlCommentForm: YamlCommentFormComponent;

  constructor(
    private translate: TranslateService,
    private activatedRoute: ActivatedRoute,
    private store: Store<fromAppCatalog.State>,
    private router: Router,
    private appCatalogService: AppCatalogService,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    private dialogService: DialogService,
    public platformService: PlatFormUtilService,
  ) {}

  ngOnInit(): void {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = baseParams['cluster'];
    this.namespaceName = baseParams['namespace'];
    this.templateId$.subscribe(uuid => {
      this.catalogPayload.template.uuid = uuid;
      this.store.dispatch(new TemplateLoadAction(uuid));
    });
  }

  get publicRepoType() {
    return this.activatedRoute.snapshot.parent.routeConfig.path;
  }

  get publicRepoTypeTranslate() {
    return this.translate.get(`nav_${this.publicRepoType.split('-')[1]}`);
  }

  viewYamlConfigFile() {
    this.dialogService.open(this.yamlModalTemplateRef, {
      size: DialogSize.Large,
    });
  }

  async onYamlFormSubmit(newYaml: string) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('create'),
        content: this.translate.get('app_catalog_create_app_confirm', {
          app_name: this.catalogPayload.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return false;
    }
    this.catalogPayload.values_yaml_content = newYaml;
    this.catalogPayload.cluster.name = this.clusterName;
    this.catalogPayload.namespace.name = this.namespaceName;

    this.submitting = true;

    try {
      await this.appCatalogService.createAppWithTemplate(this.catalogPayload);
      this.router.navigate(['app', 'detail', this.catalogPayload.name], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } catch (err) {
      this.errorsToastService.error(err);
    }
    this.submitting = false;
  }

  cancelCreateAppCatalog() {
    this.router.navigate([this.publicRepoType, 'list'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
