import { NotificationService } from '@alauda/ui';
import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { AppCatalogService } from 'app/services/api/app-catalog.service';
import { TranslateService } from 'app/translate/translate.service';
import { defer, of } from 'rxjs';
import { catchError, map, mergeMap } from 'rxjs/operators';

import * as catalogTemplate from '../actions/catalog-template';

@Injectable()
export class AppCatalogCatalogTemplateEffects {
  @Effect()
  repoLoad$ = this.actions$
    .pipe(ofType(catalogTemplate.RepoActionType.Load))
    .pipe(
      mergeMap((action: catalogTemplate.RepoLoadAction) =>
        defer(() =>
          this.appCatalog.getTemplateRepositories(action.payload),
        ).pipe(
          map(repos => new catalogTemplate.RepoLoadCompleteAction(repos[0])),
          catchError((error: Error) =>
            of(new catalogTemplate.RepoLoadErrorAction(error)),
          ),
        ),
      ),
    );

  @Effect()
  templateLoad$ = this.actions$
    .pipe(ofType(catalogTemplate.TemplateActionType.Load))
    .pipe(
      mergeMap((action: catalogTemplate.TemplateLoadAction) =>
        defer(() => this.appCatalog.getTemplateById(action.payload)).pipe(
          map(
            template =>
              new catalogTemplate.TemplateLoadCompleteAction(template),
          ),
          catchError((error: Error) => {
            const message = this.translate.get(
              'app_catalog_failed_get_template',
            );
            this.auiNotificationService.error(message);
            return of(
              new catalogTemplate.TemplateLoadErrorAction({
                uuid: action.payload,
                error,
              }),
            );
          }),
        ),
      ),
    );

  constructor(
    private actions$: Actions,
    private appCatalog: AppCatalogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {}
}
