import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { AppCatalogService } from 'app/services/api/app-catalog.service';
import { YamlCommentParserService } from 'app/services/yaml-comment-parser.service';
import { SharedModule } from 'app/shared/shared.module';

import { AppPlatformSharedModule } from './app-platform-shared.module';
import { AppCatalogCatalogTemplateEffects } from './effects/catalog-template';
import { AppPlatformBigDataRoutingModule } from './platform-big_data-routing.module';
import { FEATURE_NAME, reducers } from './reducers';

@NgModule({
  imports: [
    SharedModule,
    AppPlatformSharedModule,
    AppPlatformBigDataRoutingModule,
    StoreModule.forFeature(FEATURE_NAME, reducers),
    EffectsModule.forFeature([AppCatalogCatalogTemplateEffects]),
  ],
  declarations: [],
  exports: [],
  providers: [AppCatalogService, YamlCommentParserService],
})
export class AppPlatformBigDataModule {}
