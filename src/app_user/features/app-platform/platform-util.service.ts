import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root',
})
export class PlatFormUtilService {
  getLocalPicture(template: { name: string }) {
    if (!template || !template.name) {
      return '';
    }
    return `/static/images/app-catalog/${template.name.toLowerCase()}.png`;
  }
}
