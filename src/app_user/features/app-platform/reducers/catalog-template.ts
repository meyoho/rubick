import {
  AppCatalogTemplate,
  AppCatalogTemplateRepository,
} from 'app/services/api/app-catalog.service';

import * as catalogTemplate from '../actions/catalog-template';

export interface AppListState {
  params?: catalogTemplate.AppListParams;
  applications?: any[]; // PH
  loading?: boolean;
  error?: any; // PH
}

export interface TemplateListState {
  mode?: catalogTemplate.ListPageViewMode;

  // Contains the current template repository data
  repository?: AppCatalogTemplateRepository;

  // Current selected template ID
  selectedTemplateId?: string;

  // Current search query for apps
  searchQuery?: string;

  // Repo loading
  loading: boolean;

  // Template UUID -> template. Only viable when fetched by UUID.
  templateMap?: { [uuid: string]: TemplateState };

  // Repo load error
  error?: Error; // PH
}

export interface TemplateState {
  template?: AppCatalogTemplate;

  // Template is loading
  loading: boolean;

  // Load error
  error?: Error;
}

const initialState: TemplateListState = {
  loading: false,
  templateMap: {},
};

export function reducer(
  state = initialState,
  action: catalogTemplate.Actions,
): TemplateListState {
  switch (action.type) {
    case catalogTemplate.ListPageActionType.ModeChange: {
      return {
        ...state,
        mode: action.payload,
      };
    }
    case catalogTemplate.ListPageActionType.TemplateIdChange: {
      return {
        ...state,
        selectedTemplateId: action.payload,
      };
    }
    case catalogTemplate.ListPageActionType.AppSearchChange: {
      return {
        ...state,
        searchQuery: action.payload,
      };
    }
    case catalogTemplate.RepoActionType.Load: {
      return {
        ...state,
        loading: true,
        error: undefined,
      };
    }
    case catalogTemplate.TemplateActionType.Load: {
      const templateMap = state.templateMap;
      return {
        ...state,
        templateMap: {
          ...templateMap,
          [action.payload]: {
            loading: true,
            error: undefined,
            template:
              templateMap[action.payload] &&
              templateMap[action.payload].template,
          },
        },
      };
    }
    case catalogTemplate.TemplateActionType.LoadComplete: {
      const templateMap = state.templateMap;
      return {
        ...state,
        templateMap: {
          ...templateMap,
          [action.payload.uuid]: {
            loading: false,
            error: undefined,
            template: action.payload,
          },
        },
      };
    }
    case catalogTemplate.TemplateActionType.LoadError: {
      const templateMap = state.templateMap;
      const uuid = action.payload.uuid;
      return {
        ...state,
        templateMap: {
          ...templateMap,
          [uuid]: {
            loading: false,
            error: action.payload.error,
            template: templateMap[uuid] && templateMap[uuid].template,
          },
        },
      };
    }
    case catalogTemplate.RepoActionType.LoadComplete: {
      return {
        ...state,
        repository: action.payload,
        loading: false,
        error: undefined,
      };
    }
    case catalogTemplate.RepoActionType.LoadError: {
      return {
        ...state,
        error: action.payload,
        loading: false,
      };
    }
    default:
      return state;
  }
}

export const getRepository = (state: TemplateListState) => state.repository;
export const getLoading = (state: TemplateListState) => state.loading;
export const getMode = (state: TemplateListState) => state.mode;
export const getTemplates = (state: TemplateListState) =>
  state.repository && state.repository.templates;
export const getTemplateById = (state: TemplateListState, id: string) =>
  state.templateMap[id];
