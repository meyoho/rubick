import { createFeatureSelector, createSelector } from '@ngrx/store';
import * as fromRoot from 'app/store';

import * as catalogTemplate from './catalog-template';

export interface State extends fromRoot.AppState {
  templateList: catalogTemplate.TemplateListState;

  // TODO: add create state
}

export const reducers = {
  templateList: catalogTemplate.reducer,
};

export const FEATURE_NAME = 'app-platform';
export const selectFeature = createFeatureSelector<State>(FEATURE_NAME);

export const getTemplateListPageState = createSelector(
  selectFeature,
  (state: State) => {
    return state.templateList;
  },
);

export const getRepositoryState = createSelector(
  getTemplateListPageState,
  catalogTemplate.getRepository,
);

export const getTemplatesState = createSelector(
  getTemplateListPageState,
  catalogTemplate.getTemplates,
);

export const getTemplateStateById = createSelector(
  getTemplateListPageState,
  state => (uuid: string) => catalogTemplate.getTemplateById(state, uuid),
);

export const getTemplateListLoadingState = createSelector(
  getTemplateListPageState,
  catalogTemplate.getLoading,
);

export const getTemplateListModeState = createSelector(
  getTemplateListPageState,
  catalogTemplate.getMode,
);

export const getRefreshLoadingState = createSelector(
  getTemplateListPageState,
  catalogTemplate.getLoading,
);
