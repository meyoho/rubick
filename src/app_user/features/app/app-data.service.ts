import { Inject, Injectable } from '@angular/core';

import { get } from 'lodash-es';
import { all } from 'ramda';
import { BehaviorSubject, Observable, ReplaySubject } from 'rxjs';
import { filter, publishReplay, refCount } from 'rxjs/operators';

import { AppService } from 'app/services/api/app.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';
import {
  K8sApplicationPayload,
  K8sResourceWithActions,
} from 'app/typings/backend-api';
import {
  ApplicationFormModel,
  ApplicationMetaDataFormModel,
  ComponentFormModel,
} from 'app/typings/k8s-form-model';
import { KubernetesResource, Service } from 'app/typings/raw-k8s';
import { AppUtilService } from './app-util.service';
import { generateApplicationModel } from './util';

@Injectable()
export class AppDataService {
  applicationModelSubject$: BehaviorSubject<
    ApplicationFormModel
  > = new BehaviorSubject<ApplicationFormModel>(null);
  applicationModel$: Observable<ApplicationFormModel>;
  resourceNotFound$ = new ReplaySubject<boolean>();
  updateAppName: string; // name parameter in url when updating app
  private labelBaseDomain: string;
  constructor(
    private appService: AppService,
    private appUtil: AppUtilService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {
    this.applicationModel$ = this.applicationModelSubject$.asObservable().pipe(
      filter(r => !!r),
      publishReplay(1),
      refCount(),
    );
    this.labelBaseDomain = this.environments.label_base_domain || 'alauda.io';
  }

  async initFetchRequest(cluster: string, namespace: string, name: string) {
    this.updateAppName = name;
    const mirrorClusters = await this.appUtil.getCurrentMirrorClusters();
    this.appService
      .getAppDetailCRD({
        cluster_name: cluster,
        namespace,
        app: name,
      })
      .then(result => {
        const formModel = generateApplicationModel(
          result.map((item: K8sResourceWithActions) => {
            return item.kubernetes;
          }),
          this.labelBaseDomain,
        );
        if (mirrorClusters.length) {
          formModel.clusters = mirrorClusters.map(i => i.name);
        }
        this.applicationModelSubject$.next(formModel);
      })
      .catch(() => {
        this.resourceNotFound$.next(true);
      });
  }

  get applicationModel() {
    return this.applicationModelSubject$.value;
  }

  // ***** ApplicationFormModel ***** //

  setApplicationFormModel(app: ApplicationFormModel) {
    this.applicationModelSubject$.next(app);
  }

  updateMetadata(metadata: ApplicationMetaDataFormModel) {
    const applicationModel = { ...this.applicationModel };
    applicationModel.metadata = { ...metadata };
    this.applicationModelSubject$.next(applicationModel);
  }

  addComponent(component: ComponentFormModel) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    applicationModel.components.push(component);
    this.applicationModelSubject$.next(applicationModel);
  }

  // 在”添加计算组件“页，也会允许同时自动创建一个Service
  addService(service: Service | Service[]) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    if (Array.isArray(service)) {
      if (!applicationModel.services) {
        applicationModel.services = [...service];
      } else {
        applicationModel.services.push(...service);
      }
    } else {
      if (!applicationModel.services) {
        applicationModel.services = [service];
      } else {
        applicationModel.services.push(service);
      }
    }
    this.applicationModelSubject$.next(applicationModel);
  }

  checkComponentName(
    component: ComponentFormModel,
    original?: {
      name: string;
      kind: string;
    },
  ) {
    const distinct = (item: ComponentFormModel) => {
      return (
        item.controller.metadata.name !== component.controller.metadata.name ||
        item.controller.kind.toLowerCase() !==
          component.controller.kind.toLowerCase()
      );
    };
    if (original) {
      return (
        (component.controller.kind.toLowerCase() ===
          original.kind.toLowerCase() &&
          component.controller.metadata.name === original.name) ||
        all(distinct)(this.applicationModel.components)
      );
    } else {
      return all(distinct)(this.applicationModel.components);
    }
  }

  updateComponent(
    component: ComponentFormModel,
    original: {
      name: string;
      kind: string;
    },
  ) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    const originalComponent = applicationModel.components.find(
      (item: ComponentFormModel) => {
        return (
          item.controller.metadata.name === original.name &&
          item.controller.kind.toLowerCase() === original.kind
        );
      },
    );
    Object.assign(originalComponent, component);
    this.applicationModelSubject$.next(applicationModel);
  }

  removeComponent(index: number) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    applicationModel.components.splice(index, 1);
    this.applicationModelSubject$.next(applicationModel);
  }

  updateService(index: number, service: Service) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    applicationModel.services[index] = service;
    this.applicationModelSubject$.next(applicationModel);
  }

  removeService(index: number) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    applicationModel.services.splice(index, 1);
    this.applicationModelSubject$.next(applicationModel);
  }

  updateResource(
    resource: KubernetesResource,
    original: {
      name: string;
      kind: string;
    },
  ) {
    const applicationModel = { ...this.applicationModel };
    const originalResource = applicationModel.resources.find(
      (item: KubernetesResource) => {
        return (
          item.metadata.name === original.name && item.kind === original.kind
        );
      },
    );
    Object.assign(originalResource, resource);
    this.applicationModelSubject$.next(applicationModel);
  }

  addResource(resource: KubernetesResource) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    applicationModel.resources.push(resource);
    this.applicationModelSubject$.next(applicationModel);
  }

  removeResource(index: number) {
    if (!this.applicationModel) {
      return;
    }
    const applicationModel = { ...this.applicationModel };
    applicationModel.resources.splice(index, 1);
    this.applicationModelSubject$.next(applicationModel);
  }

  checkResourceName(
    resource: KubernetesResource,
    original?: {
      name: string;
      kind: string;
    },
  ) {
    const distinct = (item: KubernetesResource) => {
      return (
        item.metadata.name !== resource.metadata.name ||
        item.kind !== resource.kind
      );
    };
    const resources = [
      ...this.applicationModel.services,
      ...this.applicationModel.resources,
    ];
    if (original) {
      return (
        (resource.kind === original.kind &&
          resource.metadata.name === original.name) ||
        all(distinct)(resources)
      );
    } else {
      return all(distinct)(resources);
    }
  }

  // ***** Utility function ***** //
  generateApplicationPayload(
    formModel: ApplicationFormModel,
  ): K8sApplicationPayload {
    const kubernetes: KubernetesResource[] = [];
    formModel.components.forEach((component: ComponentFormModel) => {
      kubernetes.push(component.controller);
      if (component.autoscaler) {
        kubernetes.push(component.autoscaler);
      }
    });
    if (formModel.crd) {
      const crd = formModel.crd;
      // update displayName
      const annotations = get(crd, 'metadata.annotations', {});
      annotations[`app.${this.labelBaseDomain}/display-name`] =
        formModel.metadata.displayName;
      crd.metadata.annotations = annotations;
      kubernetes.push({
        ...crd,
      } as any);
    }
    if (formModel.services) {
      const selectorKey = `service.${this.labelBaseDomain}/name`;
      const appSelectorKey = `app.${this.labelBaseDomain}/name`;
      const services = formModel.services.map((service: Service) => {
        const selectorValue = get(service, `spec.selector`, {})[
          `${selectorKey}`
        ];
        if (selectorValue) {
          service.spec.selector[appSelectorKey] = `${formModel.metadata.name}.${
            formModel.metadata.namespace
          }`;
        }
        return {
          ...service,
        };
      });
      kubernetes.push(...services);
    }
    if (formModel.resources) {
      kubernetes.push(...formModel.resources);
    }
    return {
      resource: {
        name: formModel.metadata.name,
        namespace: formModel.metadata.namespace,
        displayName: formModel.metadata.displayName || '',
      },
      kubernetes,
    };
  }

  getCurrentApplicationPayload() {
    return this.generateApplicationPayload(this.applicationModel);
  }
}
