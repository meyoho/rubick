import { TagType } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import {
  AppService,
  ApplicationStatus,
  ApplicationStatusMessage,
} from 'app/services/api/app.service';
import { TranslateService } from 'app/translate/translate.service';
import { Application, ApplicationStatusEnum } from 'app/typings';
import { get } from 'lodash-es';

@Component({
  selector: 'rc-app-status',
  templateUrl: 'template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppStatusComponent implements OnChanges {
  @Input()
  application: Application;
  @Input()
  appName: string;
  @Input()
  baseParams: {
    cluster: string;
    namespace: string;
  };
  @Input()
  appStatus: ApplicationStatus;

  tagType: string;

  rawStatus: string;
  statusText: string;
  runningWorkloads: number;
  totalWorkloads: number;
  messages: ApplicationStatusMessage[];
  initialized: boolean;

  private statusTypeMap = {
    [ApplicationStatusEnum.Running]: TagType.Success,
    [ApplicationStatusEnum.PartialRunning]: TagType.Warning,
    [ApplicationStatusEnum.Stopped]: TagType.Info,
    [ApplicationStatusEnum.Pending]: TagType.Primary,
    [ApplicationStatusEnum.Failed]: TagType.Error,
    [ApplicationStatusEnum.Empty]: TagType.Info,
  };

  ngOnChanges({ application, appName, baseParams }: SimpleChanges) {
    if (
      (application && application.currentValue) ||
      (appName && appName.currentValue)
    ) {
      this.onInputChange();
    }

    if (baseParams && baseParams.currentValue && !baseParams.firstChange) {
      this.onInputChange();
    }
  }

  private async onInputChange() {
    if (this.application) {
      this.rawStatus = this.application.spec.assemblyPhase || 'Unknown';
    } else {
      const res = this.appStatus || (await this.getApplicationStatus());
      if (!res) {
        this.useUnknownStatus();
        return;
      }
      this.rawStatus = res.app_status || 'Unknown';
      this.totalWorkloads = Object.keys(res.workloads || {}).length;
      this.runningWorkloads = Object.keys(res.workloads || {}).filter(
        key => get(res, `workloads.${key}.status`, '') === 'Running',
      ).length;
      this.messages = res.messages || [];
    }
    this.statusText = this.translateService.get(
      `application_status_${this.rawStatus.toLowerCase()}`,
    );
    this.tagType = this.statusTypeMap[this.rawStatus] || 'info';
    this.initialized = true;
    this.cdr.markForCheck();
  }

  private getApplicationStatus() {
    return this.appService.getApplicationStatus({
      cluster: this.baseParams.cluster,
      namespace: this.baseParams.namespace,
      app: this.application ? this.application.metadata.name : this.appName,
    });
  }

  private useUnknownStatus() {
    this.tagType = 'info';
    this.messages = [];
    this.statusText = this.translateService.get('application_status_unknown');
    this.initialized = true;
    this.cdr.markForCheck();
  }

  constructor(
    private appService: AppService,
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}
}
