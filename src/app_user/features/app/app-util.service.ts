import {
  ConfirmType,
  DialogService,
  DialogSize,
  NotificationService as AuiNotificationService,
} from '@alauda/ui';
import { Injectable } from '@angular/core';
import { Store } from '@ngrx/store';

import { filter, first, tap } from 'rxjs/operators';

import { UpdateImageTagDialogComponent } from 'app/features-shared/app/dialog/update-image-tag/component';
import { UpdateResourceSizeDialogComponent } from 'app/features-shared/app/dialog/update-resource-size/component';
import { AlarmService } from 'app/services/api/alarm.service';
import {
  ApplicationAddress,
  AppService,
  WorkloadAddress,
} from 'app/services/api/app.service';
import { Cluster } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import * as fromStore from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { WorkspaceBaseParams } from 'app/typings';
import { K8sApplicationPayload, PodController } from 'app/typings/backend-api';
import { Service } from 'app/typings/raw-k8s';
import { BatchReponse } from 'app_user/core/types';
import { getDefaultServiceFromImagePorts } from 'app_user/features/app/util';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';

interface MirrorCluster {
  name: string;
  display_name: string;
}
@Injectable()
export class AppUtilService {
  constructor(
    private dialogService: DialogService,
    private alarmService: AlarmService,
    private appService: AppService,
    private translateService: TranslateService,
    private errorToastService: ErrorsToastService,
    private auiNotificationService: AuiNotificationService,
    private store: Store<fromStore.AppState>,
  ) {}

  updateImageTag(params: {
    podController: PodController;
    containerName: string;
    baseParams: WorkspaceBaseParams;
  }) {
    const dialogRef = this.dialogService.open(UpdateImageTagDialogComponent, {
      size: DialogSize.Big,
      data: params,
    });
    return dialogRef.componentInstance.close.pipe(
      first(),
      tap(() => {
        dialogRef.close();
      }),
    );
  }

  updateResourceSize(params: {
    podController: PodController;
    containerName: string;
    baseParams: WorkspaceBaseParams;
  }) {
    const dialogRef = this.dialogService.open(
      UpdateResourceSizeDialogComponent,
      {
        size: DialogSize.Big,
        data: params,
      },
    );
    return dialogRef.componentInstance.close.pipe(
      first(),
      tap(() => {
        dialogRef.close();
      }),
    );
  }

  rollback(params: {
    cluster: string;
    namespace: string;
    revision: number;
    app: string;
  }) {
    return this.dialogService.confirm({
      title: this.translateService.get('application_rollback_confirm_title', {
        revision: params.revision,
      }),
      confirmType: ConfirmType.Primary,
      confirmText: this.translateService.get('rollback'),
      cancelText: this.translateService.get('cancel'),
      beforeConfirm: (res, rej) => {
        this.appService
          .rollback({
            cluster: params.cluster,
            namespace: params.namespace,
            revision: params.revision,
            app: params.app,
          })
          .then(() => res())
          .catch(e => {
            this.errorToastService.error(e);
            return rej();
          });
      },
    });
  }

  async removeComponent(params: {
    payload: any;
    app: string;
    component: string;
    cluster: string;
    namespace: string;
    mirrorClusters?: MirrorCluster[];
    project?: string;
  }) {
    if (params.mirrorClusters && params.mirrorClusters.length > 1) {
      const requireSelect = params.mirrorClusters.filter(
        c => c.name === params.cluster,
      );
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      const mirrorClusterMeta = {
        displayName: 'display_name',
        mirrorResource: params.mirrorClusters,
        confirmTitle: this.translateService.get('delete_app'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'app_delete_selected_cluster_component_confirm',
          {
            app_name: params.app,
            component_name: params.component,
          },
        ),
      };
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorCluster[]) => {
          modalRef.close();
          if (res) {
            this.appService
              .batchRemoveComponentCRD({
                payload: params.payload,
                app: params.app,
                clusters: res.map(c => c.name),
                namespace: params.namespace,
                project: params.project,
              })
              .then((res: BatchReponse) => {
                let success = false;
                Object.entries(res.responses).forEach(([clusterName, data]) => {
                  const clusterDisplayName = params.mirrorClusters.find(
                    c => c.name === clusterName,
                  ).display_name;
                  if (data.code >= 300) {
                    const body = JSON.parse(data.body);
                    const error = this.errorToastService.parseErrors({
                      errors: body.errors,
                    });
                    const title = this.translateService.get(
                      'cluster_app_component_delete_error',
                      {
                        appName: params.app,
                        componentName: params.component,
                        clusterName: clusterDisplayName,
                      },
                    );
                    this.auiNotificationService.error({
                      content: `${title}: ${error.content}`,
                    });
                  } else {
                    this.auiNotificationService.success({
                      content: this.translateService.get(
                        'cluster_app_component_delete_success',
                        {
                          appName: params.app,
                          componentName: params.component,
                          clusterName: clusterDisplayName,
                        },
                      ),
                    });
                    if (params.cluster === clusterName) {
                      success = true;
                    }
                  }
                });
                return success;
              })
              .catch(e => {
                this.errorToastService.error(e);
                throw e;
              });
          }
        });
    } else {
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('delete'),
          content: this.translateService.get(
            'app_service_delete_component_confirm',
            {
              app_name: params.app,
              component_name: params.component,
            },
          ),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });
      } catch (e) {
        throw e;
        return;
      }
      return this.appService
        .removeComponentCRD({
          cluster: params.cluster,
          namespace: params.namespace,
          payload: params.payload,
          app: params.app,
        })
        .then(res => {
          this.auiNotificationService.success({
            content: this.translateService.get('delete_success'),
          });
          return res;
        })
        .catch(e => {
          this.errorToastService.error(e);
          throw e;
        });
    }
  }

  async createApplication(params: {
    payload: K8sApplicationPayload;
    cluster: string; // current cluster name
    mirrorClusters?: MirrorCluster[]; // selected clusters
    project?: string;
  }) {
    // TODO: 暂时移除二次确认
    // await this.dialogService.confirm({
    //   title: this.translateService.get('create'),
    //   content: this.translateService.get('app_service_create_app_confirm', {
    //     app_name: params.payload.resource.name,
    //   }),
    //   confirmText: this.translateService.get('confirm'),
    //   cancelText: this.translateService.get('cancel'),
    // });
    if (params.mirrorClusters && params.mirrorClusters.length > 1) {
      return this.appService
        .batchCreateAppCRD({
          payload: params.payload,
          clusters: params.mirrorClusters.map(c => c.name),
          project: params.project,
        })
        .then((res: BatchReponse) => {
          let success = false;
          Object.entries(res.responses).forEach(([clusterName, data]) => {
            const clusterDisplayName = params.mirrorClusters.find(
              c => c.name === clusterName,
            ).display_name;
            if (data.code >= 300) {
              const body = JSON.parse(data.body);
              const error = this.errorToastService.parseErrors({
                errors: body.errors,
              });
              const title = this.translateService.get(
                'cluster_app_create_error',
                {
                  appName: params.payload.resource.name,
                  clusterName: clusterDisplayName,
                },
              );
              this.auiNotificationService.error({
                content: `${title}: ${error.content}`,
              });
            } else {
              this.auiNotificationService.success({
                content: this.translateService.get(
                  'cluster_app_create_success',
                  {
                    appName: params.payload.resource.name,
                    clusterName: clusterDisplayName,
                  },
                ),
              });
              if (params.cluster === clusterName) {
                success = true;
              }
            }
          });
          return success;
        })
        .catch(e => {
          this.errorToastService.error(e);
          throw e;
        });
    } else {
      return this.appService
        .createAppCRD({
          payload: params.payload,
          cluster: params.cluster,
        })
        .then(res => {
          this.auiNotificationService.success({
            content: this.translateService.get('app_create_success', {
              appName: params.payload.resource.name,
            }),
          });
          return res;
        })
        .catch(e => {
          this.errorToastService.error(e);
          throw e;
        });
    }
  }

  async updateApplication(params: {
    payload: K8sApplicationPayload;
    cluster: string; // current cluster name
    mirrorClusters?: MirrorCluster[]; // selected clusters
    project: string;
  }) {
    // TODO: 暂时移除二次确认
    // await this.dialogService.confirm({
    //   title: this.translateService.get('update'),
    //   content: this.translateService.get('app_service_update_app_confirm', {
    //     app_name: params.payload.resource.name,
    //   }),
    //   confirmText: this.translateService.get('confirm'),
    //   cancelText: this.translateService.get('cancel'),
    // });

    if (params.mirrorClusters && params.mirrorClusters.length > 1) {
      return this.appService
        .batchUpdateAppCRD({
          cluster: params.cluster,
          payload: params.payload,
          clusters: params.mirrorClusters.map(c => c.name),
          project: params.project,
        })
        .then((res: BatchReponse) => {
          let success = false;
          Object.entries(res.responses).forEach(([clusterName, data]) => {
            const clusterDisplayName = params.mirrorClusters.find(
              c => c.name === clusterName,
            ).display_name;
            if (data.code >= 300) {
              const body = JSON.parse(data.body);
              const error = this.errorToastService.parseErrors({
                errors: body.errors,
              });
              const title = this.translateService.get(
                'cluster_app_update_error',
                {
                  appName: params.payload.resource.name,
                  clusterName: clusterDisplayName,
                },
              );
              this.auiNotificationService.error({
                content: `${title}: ${error.content}`,
              });
            } else {
              this.auiNotificationService.success({
                content: this.translateService.get(
                  'cluster_app_update_success',
                  {
                    appName: params.payload.resource.name,
                    clusterName: clusterDisplayName,
                  },
                ),
              });
              if (params.cluster === clusterName) {
                success = true;
              }
            }
          });
          return success;
        })
        .catch(e => {
          this.errorToastService.error(e);
          throw e;
        });
    } else {
      return this.appService
        .updateAppCRD({
          payload: params.payload,
          cluster: params.cluster,
        })
        .then(res => {
          this.auiNotificationService.success({
            content: this.translateService.get('app_update_success', {
              appName: params.payload.resource.name,
            }),
          });
          return res;
        })
        .catch(e => {
          this.errorToastService.error(e);
          throw e;
        });
    }
  }

  async deleteApplication(params: {
    cluster: string;
    namespace: string;
    app: string;
    mirrorClusters?: MirrorCluster[];
    project?: string;
  }) {
    if (params.mirrorClusters && params.mirrorClusters.length > 1) {
      return new Promise((resolve, reject) => {
        this.batchDeleteApplication({
          ...params,
          resolveFn: resolve,
          rejectFn: reject,
        });
      });
    } else {
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('delete'),
          content: this.translateService.get('app_service_delete_app_confirm', {
            app_name: params.app,
          }),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });
      } catch (e) {
        throw e;
        return;
      }
      return Promise.all([
        this.appService
          .deleteAppCRD({
            cluster: params.cluster,
            namespace: params.namespace,
            app: params.app,
          })
          .then(res => {
            this.auiNotificationService.success({
              content: this.translateService.get('app_delete_success', {
                appName: params.app,
              }),
            });
            return res;
          })
          .catch(e => {
            this.errorToastService.error(e);
            throw e;
          }),
        this.alarmService
          .deleteAlertsByLabelSelector(
            params.cluster,
            params.namespace,
            'application',
            params.app,
          )
          .catch(() => {}),
      ]);
    }
  }

  private batchDeleteApplication(params: {
    cluster: string;
    namespace: string;
    app: string;
    mirrorClusters?: MirrorCluster[];
    project?: string;
    resolveFn?: Function;
    rejectFn?: Function;
  }) {
    const requireSelect = params.mirrorClusters.filter(
      c => c.name === params.cluster,
    );
    const modalRef = this.dialogService.open(
      MirrorResourceDeleteConfirmComponent,
    );
    const mirrorClusterMeta = {
      displayName: 'display_name',
      mirrorResource: params.mirrorClusters,
      confirmTitle: this.translateService.get('delete_app'),
      confirmLabel: this.translateService.get('cluster'),
      confirmContent: this.translateService.get(
        'app_service_delete_selected_cluster_app_confirm',
        {
          app_name: params.app,
        },
      ),
    };
    modalRef.componentInstance.setMirrorResourceMeta(
      mirrorClusterMeta,
      requireSelect,
    );
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe(async (res: MirrorCluster[]) => {
        modalRef.close();
        if (res) {
          this.appService
            .batchDeleteAppCRD({
              clusters: res.map(c => c.name),
              namespace: params.namespace,
              app: params.app,
              project: params.project,
            })
            .then((res: BatchReponse) => {
              let success = false;
              Object.entries(res.responses).forEach(([clusterName, data]) => {
                const clusterDisplayName = params.mirrorClusters.find(
                  c => c.name === clusterName,
                ).display_name;
                if (data.code >= 300) {
                  const body = JSON.parse(data.body);
                  const error = this.errorToastService.parseErrors({
                    errors: body.errors,
                  });
                  const title = this.translateService.get(
                    'cluster_app_delete_error',
                    {
                      appName: params.app,
                      clusterName: clusterDisplayName,
                    },
                  );
                  this.auiNotificationService.error({
                    content: `${title}: ${error.content}`,
                  });
                } else {
                  this.auiNotificationService.success({
                    content: this.translateService.get(
                      'cluster_app_delete_success',
                      {
                        appName: params.app,
                        clusterName: clusterDisplayName,
                      },
                    ),
                  });
                  if (params.cluster === clusterName) {
                    success = true;
                  }
                }
              });
              if (params.resolveFn) {
                params.resolveFn(success);
              }
              return success;
            })
            .catch(e => {
              this.errorToastService.error(e);
              if (params.rejectFn) {
                params.rejectFn(e);
              }
              throw e;
            });
        }
      });
  }

  async deleteComponent(params: {
    cluster: string;
    resource_type: string;
    namespace: string;
    app: string;
    component: string;
    mirrorClusters?: MirrorCluster[];
    project?: string;
  }) {
    if (params.mirrorClusters && params.mirrorClusters.length > 1) {
      return new Promise((resolve, reject) => {
        this.batchDeleteComponent({
          ...params,
          resolveFn: resolve,
          rejectFn: reject,
        });
      });
    } else {
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('delete'),
          content: this.translateService.get(
            'app_service_delete_component_confirm',
            {
              app_name: params.app,
              component_name: params.component,
            },
          ),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });
      } catch (e) {
        throw e;
        return;
      }
      return this.appService
        .deleteResource({
          cluster: params.cluster,
          resource_type: params.resource_type,
          namespace: params.namespace,
          name: params.component,
        })
        .catch(e => {
          this.errorToastService.error(e);
          throw e;
        });
    }
  }

  batchDeleteComponent(params: {
    cluster: string;
    resource_type: string;
    namespace: string;
    app: string;
    component: string;
    mirrorClusters?: MirrorCluster[];
    project?: string;
    resolveFn?: Function;
    rejectFn?: Function;
  }) {
    const requireSelect = params.mirrorClusters.filter(
      c => c.name === params.cluster,
    );
    const modalRef = this.dialogService.open(
      MirrorResourceDeleteConfirmComponent,
    );
    const mirrorClusterMeta = {
      displayName: 'display_name',
      mirrorResource: params.mirrorClusters,
      confirmTitle: this.translateService.get('delete_app'),
      confirmLabel: this.translateService.get('cluster'),
      confirmContent: this.translateService.get(
        'app_delete_selected_cluster_component_confirm',
        {
          app_name: params.app,
          component_name: params.component,
        },
      ),
    };
    modalRef.componentInstance.setMirrorResourceMeta(
      mirrorClusterMeta,
      requireSelect,
    );
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe(async (res: MirrorCluster[]) => {
        modalRef.close();
        if (res) {
          this.appService
            .batchDeleteResource({
              clusters: res.map(c => c.name),
              resource_type: params.resource_type,
              namespace: params.namespace,
              name: params.component,
              project: params.project,
            })
            .then((res: BatchReponse) => {
              let success = false;
              Object.entries(res.responses).forEach(([clusterName, data]) => {
                const clusterDisplayName = params.mirrorClusters.find(
                  c => c.name === clusterName,
                ).display_name;
                if (data.code >= 300) {
                  const body = JSON.parse(data.body);
                  const error = this.errorToastService.parseErrors({
                    errors: body.errors,
                  });
                  const title = this.translateService.get(
                    'cluster_app_component_delete_error',
                    {
                      appName: params.app,
                      componentName: params.component,
                      clusterName: clusterDisplayName,
                    },
                  );
                  this.auiNotificationService.error({
                    content: `${title}: ${error.content}`,
                  });
                } else {
                  this.auiNotificationService.success({
                    content: this.translateService.get(
                      'cluster_app_component_delete_success',
                      {
                        appName: params.app,
                        componentName: params.component,
                        clusterName: clusterDisplayName,
                      },
                    ),
                  });
                  if (params.cluster === clusterName) {
                    success = true;
                  }
                }
              });
              if (params.resolveFn) {
                params.resolveFn(success);
              }
              return success;
            })
            .catch(e => {
              this.errorToastService.error(e);
              if (params.rejectFn) {
                params.rejectFn(e);
              }
              throw e;
            });
        }
      });
  }

  public getDefaultServiceFromImagePorts(params: {
    name: string;
    ports: number[];
    cluster: string;
    namespace: string;
  }): Service {
    if (!params.ports || !params.ports.length) {
      return null;
    }

    return getDefaultServiceFromImagePorts(params);
  }

  public async getApplicationAddress(params: {
    cluster: string;
    namespace: string;
    app: string;
  }) {
    const res: ApplicationAddress = await this.appService
      .getApplicationAddress({
        cluster: params.cluster,
        namespace: params.namespace,
        app: params.app,
      })
      .catch(_e => ({}));
    return Object.entries(res.workloads || {}).reduce(
      (obj, [resourceName, serviceMap]) => {
        obj[resourceName] = Object.keys(serviceMap).reduce((arr, key) => {
          return arr.concat(
            serviceMap[key]
              .filter((addrObj: WorkloadAddress) => !!addrObj.host)
              .map((addrObj: WorkloadAddress) => {
                return `${addrObj.protocol}://${addrObj.host}:${
                  addrObj.frontend
                }${addrObj.url || ''}`;
              }),
          );
        }, []);
        return obj;
      },
      {},
    );
  }

  public getCurrentMirrorClusters() {
    return this.store
      .select(fromStore.getCurrentCluster)
      .pipe(
        filter(cluster => !!cluster),
        first(),
      )
      .toPromise()
      .then((cluster: Cluster) => {
        if (cluster.mirror && cluster.mirror.regions) {
          return cluster.mirror.regions.map(({ name, display_name }) => {
            return {
              name,
              display_name,
            };
          });
        } else {
          return [];
        }
      });
  }
}
