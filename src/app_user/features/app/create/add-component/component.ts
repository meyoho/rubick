import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { AppShardService } from 'app/features-shared/app/app-shard.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments, Service, WorkspaceBaseParams } from 'app/typings';
import {
  ComponentFormModel,
  RcImageSelection,
} from 'app/typings/k8s-form-model';
import { AppDataService } from 'app_user/features/app/app-data.service';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import {
  checkImageSelectionParam,
  getBindingServicesByPodController,
  getDefaultPodController,
} from '../../util';
@Component({
  templateUrl: './template.html',
  styleUrls: ['../styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationCreateAddComponentPageComponent implements OnInit {
  @ViewChild(NgForm)
  form: NgForm;
  appName: string;
  isUpdatingApp: boolean; // indicates whether user is in a process of updating an app or not

  formModel: ComponentFormModel;
  services: Service[] = [];
  baseParams: WorkspaceBaseParams;

  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private router: Router,
    private translateService: TranslateService,
    private messageService: MessageService,
    private workspaceComponent: WorkspaceComponent,
    private activatedRoute: ActivatedRoute,
    private appDataService: AppDataService,
    private appUtilService: AppUtilService,
    private appShardService: AppShardService,
  ) {}

  ngOnInit() {
    if (!this.appDataService.applicationModel) {
      return this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
    this.isUpdatingApp = !!this.appDataService.updateAppName;
    this.appName = this.appDataService.applicationModel.metadata.name;

    const queryParams = this.activatedRoute.snapshot.queryParams;

    if (!checkImageSelectionParam(queryParams as RcImageSelection)) {
      return this.back();
    }

    this.baseParams = this.workspaceComponent.baseParamsSnapshot;

    this.formModel = {
      controller: getDefaultPodController(
        queryParams as RcImageSelection,
        this.baseParams.namespace,
      ),
    };
    // 获取镜像的端口列表，如果存在，则自动生成一个 Service 对象
    this.appShardService.getImagePorts(queryParams).then((ports: number[]) => {
      this.addDefaultService({
        name: queryParams.repository_name,
        ports,
      });
    });
  }

  save() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }

    if (!this.appDataService.checkComponentName(this.formModel)) {
      return this.messageService.error(
        this.translateService.get('pod_controller_name_existed'),
      );
    }
    this.appDataService.addComponent(this.formModel);
    this.addServices();
    this.back();
  }

  back() {
    this.router.navigate(['preview'], {
      relativeTo: this.activatedRoute.parent,
    });
  }

  private async addDefaultService(queryParams: {
    name: string;
    ports: number[];
  }) {
    const service = this.appUtilService.getDefaultServiceFromImagePorts({
      ...queryParams,
      cluster: this.baseParams.cluster,
      namespace: this.baseParams.namespace,
    });
    this.services = service ? [service] : [];
  }

  private addServices() {
    if (this.services.length) {
      const services = getBindingServicesByPodController({
        services: this.services,
        podController: this.formModel.controller,
        labelBaseDomain: this.environments.label_base_domain,
      });
      this.appDataService.addService(services);
    }
  }
}
