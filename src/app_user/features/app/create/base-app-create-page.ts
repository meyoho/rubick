import {
  ChangeDetectorRef,
  Injector,
  OnDestroy,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Subject } from 'rxjs';

import { AppShardService } from 'app/features-shared/app/app-shard.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { ApplicationFormModel, WorkspaceBaseParams } from 'app/typings';
import { Environments } from 'app/typings';
import { AppDataService } from 'app_user/features/app/app-data.service';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

export abstract class BaseAppCreatePage implements OnDestroy {
  onDestroy$: Subject<void> = new Subject<void>();
  @ViewChild(NgForm)
  form: NgForm;
  protected router: Router;
  protected activatedRoute: ActivatedRoute;
  protected environments: Environments;
  protected workspaceComponent: WorkspaceComponent;
  protected appDataService: AppDataService;
  protected appUtilService: AppUtilService;
  protected appShardService: AppShardService;
  protected cdr: ChangeDetectorRef;
  protected errorService: ErrorsToastService;

  baseParams: WorkspaceBaseParams;
  submitting: boolean;
  // to be initialized by subclass
  application: ApplicationFormModel;
  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = [];

  constructor(public injector: Injector) {
    this.cdr = this.injector.get(ChangeDetectorRef);
    this.router = this.injector.get(Router);
    this.activatedRoute = this.injector.get(ActivatedRoute);
    this.environments = this.injector.get(ENVIRONMENTS);
    this.errorService = this.injector.get(ErrorsToastService);
    this.workspaceComponent = this.injector.get(WorkspaceComponent);
    this.appDataService = this.injector.get(AppDataService);
    this.appUtilService = this.injector.get(AppUtilService);
    this.appShardService = this.injector.get(AppShardService);

    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  cancel(isUpdate = false) {
    if (isUpdate) {
      this.router.navigate(['app', 'detail', this.application.metadata.name], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } else {
      this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }

  protected checkFormValid() {
    this.form.onSubmit(null);
    return this.form.valid;
  }

  protected async submit(isUpdate = false) {
    let payload;
    if (this.appDataService.applicationModel) {
      payload = this.appDataService.getCurrentApplicationPayload();
    } else {
      payload = this.appDataService.generateApplicationPayload(
        this.application,
      );
    }
    // console.log('formModel: ', this.application);
    // console.log('create app: ', JSON.stringify(payload, null, 2));
    // return;
    this.submitting = true;
    if (!isUpdate) {
      try {
        const res = await this.appUtilService.createApplication({
          cluster: this.baseParams.cluster,
          payload,
          mirrorClusters: this.mirrorClusters.filter(
            c => this.application.clusters.indexOf(c.name) >= 0,
          ),
          project: this.baseParams.project,
        });
        this.redirectAfterSubmit(res);
      } catch (err) {
        this.errorService.error(err);
      }
    } else {
      try {
        const res = await this.appUtilService.updateApplication({
          cluster: this.baseParams.cluster,
          payload,
          mirrorClusters: this.mirrorClusters.filter(
            c => this.application.clusters.indexOf(c.name) >= 0,
          ),
          project: this.baseParams.project,
        });
        this.redirectAfterSubmit(res);
      } catch (err) {
        this.errorService.error(err);
      }
    }

    this.submitting = false;
  }

  private redirectAfterSubmit(res: any) {
    if (!!res) {
      this.router.navigate(['app', 'detail', this.application.metadata.name], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
