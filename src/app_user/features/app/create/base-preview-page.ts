import { Injector } from '@angular/core';

import { RcImageSelection } from 'app/typings/k8s-form-model';
import { BaseAppCreatePage } from './base-app-create-page';
export class BasePreviewPage extends BaseAppCreatePage {
  isUpdate = false;
  constructor(injector: Injector) {
    super(injector);
  }

  onAddComponent(params: RcImageSelection) {
    this.router.navigate(['add-component'], {
      relativeTo: this.activatedRoute.parent,
      queryParams: params,
    });
  }

  onUpdateComponent(params: { name: string; kind: string }) {
    this.router.navigate(['update-component', params.kind, params.name], {
      relativeTo: this.activatedRoute.parent,
    });
  }

  onCreate() {
    this.application = this.appDataService.applicationModel;
    this.submit(this.isUpdate);
  }

  onCancel() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
