import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnDestroy,
  OnInit,
} from '@angular/core';

// import { lensPath, set, view } from 'ramda';

import { RcImageSelection } from 'app/typings/k8s-form-model';
import {
  getBindingServicesByPodController,
  getDefaultPodController,
} from '../../util';
import { checkImageSelectionParam } from '../../util';
import { BaseAppCreatePage } from '../base-app-create-page';

@Component({
  templateUrl: './template.html',
  styleUrls: ['../styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationCreatePrimaryPageComponent extends BaseAppCreatePage
  implements OnInit, OnDestroy {
  constructor(public injector: Injector) {
    super(injector);
  }

  async ngOnInit() {
    const queryParams = this.activatedRoute.snapshot.queryParams;
    if (!checkImageSelectionParam(queryParams as RcImageSelection)) {
      return this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }

    this.application = {
      metadata: {
        name: '',
        namespace: this.baseParams.namespace,
      },
      components: [
        {
          controller: getDefaultPodController(
            queryParams,
            this.baseParams.namespace,
          ),
        },
      ],
      clusters: [],
      services: [],
      resources: [],
    };
    // 获取镜像的端口列表，如果存在，则自动生成一个 Service 对象
    this.appShardService.getImagePorts(queryParams).then((ports: number[]) => {
      this.setDefaultService({
        name: queryParams.repository_name,
        ports,
      });
    });
    this.setDefaultMirrorClusters();
  }

  confirm() {
    if (!this.checkFormValid()) {
      return;
    }
    this.updateApplicationModel();
    this.submit();
  }

  preview() {
    if (!this.checkFormValid()) {
      return;
    }
    this.updateApplicationModel();
    this.router.navigate(['preview'], {
      relativeTo: this.activatedRoute.parent,
    });
  }

  getAppName() {
    return this.application ? this.application.metadata.name : '';
  }

  async addComponent() {
    if (!this.checkFormValid()) {
      return;
    }
    this.updateApplicationModel();
    this.appShardService
      .selectImage({
        projectName: this.baseParams.project,
      })
      .subscribe((params: RcImageSelection) => {
        if (params) {
          this.router.navigate(['add-component'], {
            relativeTo: this.activatedRoute.parent,
            queryParams: params,
          });
        }
      });
  }

  private updateServicesSelector() {
    if (this.application.services.length) {
      this.application.services = getBindingServicesByPodController({
        services: this.application.services,
        podController: this.application.components[0].controller,
        labelBaseDomain: this.environments.label_base_domain,
      });
    }
  }

  private updateApplicationModel() {
    this.updateServicesSelector();
    // const nameLens = lensPath([
    //   'components',
    //   0,
    //   'controller',
    //   'metadata',
    //   'name',
    // ]);
    // const formModel = set(
    //   nameLens,
    //   `${this.getAppName()}-${view(nameLens, this.application)}`,
    //   this.application,
    // );
    // console.log('update: formModel: ', formModel);
    // this.appDataService.setApplicationFormModel(formModel);
    this.appDataService.setApplicationFormModel(this.application);
  }

  private async setDefaultMirrorClusters() {
    this.mirrorClusters = await this.appUtilService.getCurrentMirrorClusters();
    if (this.mirrorClusters && this.mirrorClusters.length) {
      this.application.clusters = this.mirrorClusters.map(
        cluster => cluster.name,
      );
    }
  }

  private async setDefaultService(params: { name: string; ports: number[] }) {
    const service = this.appUtilService.getDefaultServiceFromImagePorts({
      ...params,
      cluster: this.baseParams.cluster,
      namespace: this.baseParams.namespace,
    });
    this.application.services = service ? [service] : [];
  }
}
