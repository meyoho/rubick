import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ChangeDetectorRef } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { cloneDeep } from 'lodash-es';
import { take } from 'rxjs/operators';

import { TranslateService } from 'app/translate/translate.service';
import { WorkspaceBaseParams } from 'app/typings';
import { ComponentFormModel } from 'app/typings/k8s-form-model';
import { AppDataService } from 'app_user/features/app/app-data.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: './template.html',
  styleUrls: ['../styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationCreateUpdateComponentPageComponent implements OnInit {
  @ViewChild(NgForm)
  form: NgForm;
  isUpdatingApp: boolean;
  appName: string;
  formModel: ComponentFormModel;
  baseParams: WorkspaceBaseParams;
  private original: {
    name: string;
    kind: string;
  };

  constructor(
    private router: Router,
    private messageService: MessageService,
    private workspaceComponent: WorkspaceComponent,
    private activatedRoute: ActivatedRoute,
    private appDataService: AppDataService,
    private translateService: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.original = {
      name: params.name,
      kind: params.kind,
    };
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.isUpdatingApp = !!this.appDataService.updateAppName;

    if (!this.isUpdatingApp && !this.appDataService.applicationModel) {
      return this.router.navigate(['app'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }

    this.appDataService.applicationModel$
      .pipe(take(1))
      .subscribe(application => {
        const formModel = application.components.find(
          (item: ComponentFormModel) => {
            return (
              item.controller.kind.toLowerCase() === this.original.kind &&
              item.controller.metadata.name === this.original.name
            );
          },
        );
        this.appName = application.metadata.name;
        this.formModel = cloneDeep(formModel);
        this.cdr.markForCheck();
      });
  }

  save() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    if (
      !this.appDataService.checkComponentName(this.formModel, this.original)
    ) {
      return this.messageService.error(
        this.translateService.get('pod_controller_name_existed'),
      );
    }
    this.appDataService.updateComponent(this.formModel, this.original);
    this.back();
  }

  back() {
    this.router.navigate(['preview'], {
      relativeTo: this.activatedRoute.parent,
    });
  }
}
