import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
  TemplateRef,
} from '@angular/core';

import { safeDump, safeLoadAll } from 'js-yaml';

import { TranslateService } from 'app/translate/translate.service';
import { KubernetesResource } from 'app/typings/raw-k8s';
import { createActions } from 'app/utils/code-editor-config';
import { generateApplicationModel, getResourcesFromYaml } from '../../util';
import { BaseAppCreatePage } from '../base-app-create-page';
import { demo } from './demo';
@Component({
  templateUrl: 'template.html',
  styleUrls: ['../styles.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AppYamlPageComponent extends BaseAppCreatePage implements OnInit {
  yaml = '';
  yamlDemo: string;
  codeEditorOptions = {
    language: 'yaml',
  };

  actionsConfig = createActions;

  constructor(
    injector: Injector,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
  ) {
    super(injector);
    this.application = {
      metadata: {
        namespace: this.baseParams.namespace,
      },
      clusters: [],
      components: [],
    };
    this.initYamlDemo();
  }

  async ngOnInit() {
    this.mirrorClusters = await this.appUtilService.getCurrentMirrorClusters();
    if (this.mirrorClusters && this.mirrorClusters.length) {
      this.application.clusters = this.mirrorClusters.map(
        cluster => cluster.name,
      );
    }
  }

  initYamlDemo() {
    const resources = safeLoadAll(demo).filter(r => !!r);
    resources.forEach((r: KubernetesResource) => {
      r.metadata.namespace = this.baseParams.namespace;
    });
    this.yamlDemo = resources
      .map(r => safeDump(r, { sortKeys: true }))
      .join('---\r\n');
  }

  async confirm() {
    if (!this.checkFormValid()) {
      return;
    }
    if (this.yamlToFormModel()) {
      this.submit();
    }
  }

  cancel() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  useDemoTemplate() {
    this.yaml = this.yamlDemo;
  }

  showDemoTemplate(template: TemplateRef<any>) {
    this.dialogService.open(template, {
      size: DialogSize.Big,
    });
  }

  private yamlToFormModel() {
    try {
      const resources = getResourcesFromYaml(
        this.yaml,
        this.baseParams.namespace,
      );
      const formModel = generateApplicationModel(
        resources,
        this.environments.label_base_domain,
      );
      this.application = {
        ...this.application,
        ...formModel,
      };
    } catch (err) {
      this.auiNotificationService.error({
        title: this.translate.get('yaml_format_error_message'),
        content: err.message,
      });
      return false;
    }
    return true;
  }
}
