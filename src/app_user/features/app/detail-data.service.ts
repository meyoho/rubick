import { Injectable } from '@angular/core';

import { BehaviorSubject, merge, Observable, Subject, timer } from 'rxjs';
import { switchMap, takeUntil, takeWhile } from 'rxjs/operators';

import { KubernetesResource } from 'app/typings';

@Injectable()
export class DetailDataService {
  applicationData$ = new BehaviorSubject<KubernetesResource[]>(null);

  defaultPollingInterval = 15000;
  polling$: Observable<number>;

  private stopPolling$ = new Subject<void>();
  private refreshSub$ = new BehaviorSubject<null>(null);

  private suspended: boolean;

  startPolling(pollingInterval?: number) {
    const interval = pollingInterval || this.defaultPollingInterval;
    const timer$ = this.refreshSub$.pipe(
      switchMap(_ =>
        timer(interval, interval).pipe(takeWhile(_ => !this.suspended)),
      ),
      takeUntil(this.stopPolling$),
    );

    this.polling$ = merge(this.refreshSub$, timer$).pipe(
      takeUntil(this.stopPolling$),
    );
    this.refreshSub$.next(null);
  }

  stopPolling() {
    this.stopPolling$.next();
  }

  refreshPolling() {
    this.refreshSub$.next(null);
  }

  suspendPolling() {
    this.suspended = true;
  }

  resumePolling() {
    this.suspended = false;
    this.refreshPolling();
  }

  setApplicationData(data: KubernetesResource[]) {
    this.applicationData$.next(data);
  }
}
