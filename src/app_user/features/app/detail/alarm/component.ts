import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { find, get } from 'lodash-es';

import { AlarmUpdateParam } from 'app/services/api/alarm.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environment } from 'app/typings';
import { K8sResourceWithActions } from 'app/typings/backend-api';

@Component({
  selector: 'rc-app-core-alarm',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailAlarmComponent implements OnInit, OnChanges {
  @Input()
  appData: K8sResourceWithActions[];
  @Input()
  clusterName: string;
  @Input()
  namespaceName: string;
  @Input()
  projectName: string;

  initialized = false;
  status = 'list';
  alarmUpdateParam: AlarmUpdateParam;
  app: string;
  listParam: {
    kind: string;
    name: string;
  };

  ngOnChanges({ appData }: SimpleChanges) {
    if (appData && appData.currentValue) {
      const application = find(this.appData, [
        'kubernetes.kind',
        'Application',
      ]);
      this.app = get(application, `kubernetes.metadata.name`, '');
      this.listParam = {
        kind: 'application',
        name: this.app,
      };

      this.initialized = true;
    }
  }

  onAlarmRoute(alarmUpdateParam: AlarmUpdateParam) {
    this.alarmUpdateParam = alarmUpdateParam;
    this.status = alarmUpdateParam.path;
  }

  ngOnInit(): void {}

  constructor(@Inject(ENVIRONMENTS) public env: Environment) {}
}
