import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { find, get } from 'lodash-es';

import { K8sResourceWithActions } from 'app/typings/backend-api';

interface Filters {
  or: {
    and: {
      term?: {
        kind?: string;
        name?: string;
      };
      regexp?: {
        name: string;
      };
    }[];
  }[];
}

@Component({
  selector: 'rc-application-event',
  templateUrl: './template.html',
  styleUrls: ['../style.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailEventComponent implements OnInit, OnChanges {
  @Input()
  appData: K8sResourceWithActions[];
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  initialized = false;
  empty = false;
  filters: Filters = {
    or: [],
  };
  filterString: string;
  names: string;

  application: K8sResourceWithActions;
  resourceItems: K8sResourceWithActions[];

  ngOnChanges({ appData }: SimpleChanges) {
    if (appData && appData.currentValue) {
      this.extractAllResources();
      this.empty = !this.appData.length;
      this.initialized = true;
    }
  }

  ngOnInit(): void {}

  extractAllResources() {
    const names = this.appData.map(el => {
      return get(el, 'kubernetes.metadata.name');
    });
    this.names = [...new Set(names)].join(',');
    this.application = find(this.appData, ['kubernetes.kind', 'Application']);
    this.resourceItems = this.appData.filter(el => {
      return ['Deployment', 'DaemonSet', 'StatefulSet'].includes(
        el.kubernetes.kind,
      );
    });
    this.filters.or = [];
    this.produceFilter([this.application], true);
    this.produceFilter(this.resourceItems);
    this.filterString = JSON.stringify(this.filters);
  }

  produceFilter(
    resourceItems: K8sResourceWithActions[],
    isApplication = false,
  ) {
    resourceItems.forEach(el => {
      if (!isApplication) {
        this.filters.or.push({
          and: [
            {
              term: {
                kind: 'Pod',
              },
            },
            {
              regexp: {
                name: el.kubernetes.metadata.name + '-.{1,}',
              },
            },
          ],
        });
      }
      this.filters.or.push({
        and: [
          {
            term: {
              kind: el.kubernetes.kind,
            },
          },
          {
            term: {
              name: el.kubernetes.metadata.name,
            },
          },
        ],
      });
    });
  }

  constructor() {}
}
