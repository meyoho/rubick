import { DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { includes } from 'lodash-es';
import { filter, first } from 'rxjs/operators';

import {
  PodControllerKindEnum,
  PodControllerKinds,
} from 'app/features-shared/app/utils/pod-controller';
import { AlarmService } from 'app/services/api/alarm.service';
import { AppService } from 'app/services/api/app.service';
import * as fromStore from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { Deployment, StatefulSet } from 'app/typings';
import { K8sResourceWithActions } from 'app/typings/backend-api';
import { delay } from 'app2/utils';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  selector: 'rc-app-detail-header',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailHeaderComponent implements OnChanges {
  @Input()
  app: string;
  @Input()
  actions: string[];
  @Input()
  cluster: string;
  @Input()
  appData: K8sResourceWithActions[];

  mirrorClusters: Array<{
    name: string;
    display_name: string;
  }> = []; // avaliable mirror clusters

  project: string;
  namespace: string;

  hasUpdatePermission: boolean;
  hasDeletePermission: boolean;
  hasStartPermission: boolean;
  hasStopPermission: boolean;
  showStart: boolean;
  showStop: boolean;
  canStart: boolean;
  canStop: boolean;

  async ngOnChanges() {
    if (this.actions && this.appData) {
      await delay(100);
      this.onInputChange();
    }
  }

  private onInputChange() {
    this.hasUpdatePermission = includes(this.actions, 'application:update');
    this.hasDeletePermission = includes(this.actions, 'application:delete');
    this.hasStartPermission = includes(this.actions, 'application:start');
    this.hasStopPermission = includes(this.actions, 'application:stop');
    this.showStart =
      this.hasStartPermission && !this.isDaemonSetApplication(this.appData);
    this.showStop =
      this.hasStopPermission && !this.isDaemonSetApplication(this.appData);

    const workloads = this.appData.filter(i =>
      [
        PodControllerKindEnum.Deployment,
        PodControllerKindEnum.StatefulSet,
      ].includes(i.kubernetes.kind as PodControllerKindEnum),
    );
    this.canStart = workloads.some(
      (i: { kubernetes: Deployment | StatefulSet }) => {
        return i.kubernetes.spec.replicas === 0;
      },
    );
    this.canStop = workloads.some(
      (i: { kubernetes: Deployment | StatefulSet }) => {
        return i.kubernetes.spec.replicas !== 0;
      },
    );
    this.cdr.markForCheck();
  }

  private isDaemonSetApplication(appData: K8sResourceWithActions[]) {
    return appData
      .filter(i => PodControllerKinds.includes(i.kubernetes.kind))
      .every(i => i.kubernetes.kind === PodControllerKindEnum.DaemonSet);
  }

  update() {
    this.router.navigate(['app', 'update', this.app], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async delete() {
    this.mirrorClusters = [];
    await this.getMirrorClusters();
    try {
      await this.appUtilService.deleteApplication({
        cluster: this.cluster,
        namespace: this.namespace,
        app: this.app,
        mirrorClusters: this.mirrorClusters,
        project: this.project,
      });
      this.alarmService
        .deleteAlertsByLabelSelector(
          this.cluster,
          this.namespace,
          'application',
          this.app,
        )
        .catch(() => {});
      this.jumpToListPage();
    } catch (_err) {}
  }

  private async getMirrorClusters() {
    const cluster = await this.store
      .select(fromStore.getCurrentCluster)
      .pipe(
        filter(cluster => !!cluster),
        first(),
      )
      .toPromise();
    if (cluster.mirror && cluster.mirror.regions) {
      this.mirrorClusters = cluster.mirror.regions.map(
        ({ name, display_name }) => ({ name, display_name }),
      );
    }
    if (this.mirrorClusters.length > 1) {
      const clusterAppMap = await this.appService.batchGetAppDetailCRD({
        project: this.project,
        clusters: this.mirrorClusters.map(c => c.name),
        namespace: this.namespace,
        name: this.app,
      });
      const appClusters = Object.keys(clusterAppMap);
      this.mirrorClusters = this.mirrorClusters.filter(
        c => appClusters.indexOf(c.name) >= 0,
      );
    }
  }

  async start() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('start'),
        content: this.translateService.get('app_service_start_app_confirm', {
          app_name: this.app,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.disableActions();
    this.cdr.markForCheck();
    this.appService
      .startApplication({
        cluster: this.cluster,
        namespace: this.namespace,
        app: this.app,
      })
      .then(() => {
        this.detailDataService.refreshPolling();
      })
      .catch(_e => {});
  }

  async stop() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('stop'),
        content: this.translateService.get('app_service_stop_app_confirm', {
          app_name: this.app,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.disableActions();
    this.cdr.markForCheck();
    this.appService
      .stopApplication({
        cluster: this.cluster,
        namespace: this.namespace,
        app: this.app,
      })
      .then(() => {
        this.detailDataService.refreshPolling();
      })
      .catch(_e => {});
  }

  private disableActions() {
    this.canStart = false;
    this.canStop = false;
  }

  jumpToListPage() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  constructor(
    private appService: AppService,
    private router: Router,
    private cdr: ChangeDetectorRef,
    private workspaceComponent: WorkspaceComponent,
    private appUtilService: AppUtilService,
    private store: Store<fromStore.AppState>,
    private detailDataService: DetailDataService,
    private alarmService: AlarmService,
    private dialogService: DialogService,
    private translateService: TranslateService,
  ) {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    [this.project, this.namespace] = [baseParams.project, baseParams.namespace];
  }
}
