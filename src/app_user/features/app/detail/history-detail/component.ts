import { CodeEditorIntl } from '@alauda/code-editor';
import { NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import {
  ApplicationHistory,
  ApplicationHistoryDiff,
  AppService,
} from 'app/services/api/app.service';
import { TranslateService } from 'app/translate/translate.service';
import { updateActions } from 'app/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { AppUtilService } from '../../app-util.service';
import { HistoryDetailCodeEditorIntlService } from './code-editor-intl.service';

@Component({
  selector: 'rc-application-history-detail',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: CodeEditorIntl,
      useClass: HistoryDetailCodeEditorIntlService,
    },
  ],
})
export class ApplicationHistoryDetailComponent implements OnInit {
  appName: string;
  namespace: string;
  cluster: string;
  versionName: string;
  revision: number;

  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: true,
  };
  codeEditorUpdateActions = updateActions;

  history: ApplicationHistory;
  canUpdate: boolean;
  diffsWithLatest: string[];
  latestYaml = '';
  latestRevision: number;
  yaml = '';

  isCurrentVersion: boolean;
  initialized: boolean;

  constructor(
    private router: Router,
    private cdr: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private appService: AppService,
    private appUtilService: AppUtilService,
    private workspaceComponent: WorkspaceComponent,
    private translateService: TranslateService,
    private notificationService: NotificationService,
  ) {}

  ngOnInit(): void {
    this.appName = this.activatedRoute.snapshot.params.app;
    this.versionName = this.activatedRoute.snapshot.params.name;
    this.cluster = this.workspaceComponent.baseParamsSnapshot.cluster;
    this.namespace = this.workspaceComponent.baseParamsSnapshot.namespace;

    this.fetchDetail();
  }

  private fetchDetail() {
    this.appService
      .getApplicationHistoryDetail({
        cluster: this.cluster,
        namespace: this.namespace,
        name: this.versionName,
      })
      .then(res => {
        this.history = res.kubernetes;
        this.canUpdate = res.resource_actions.includes('k8s_others:update');
        this.revision = this.history.spec.revision;
        this.latestRevision = this.history.spec.latestRevision;
        this.isCurrentVersion = this.latestRevision === this.revision;
        if (!this.isCurrentVersion) {
          this.fetchLatestYaml();
        }
        this.codeEditorUpdateActions = Object.assign(
          {},
          this.codeEditorUpdateActions,
          {
            diffMode: !this.isCurrentVersion,
          },
        );
        this.yaml = this.history.spec.yaml;
        const diffData: ApplicationHistoryDiff = this.history.spec
          .resourceDiffsWithLatest;
        this.diffsWithLatest = Object.keys(diffData || {}).reduce(
          (acc, cur) => {
            return acc.concat(
              diffData[cur].map((i: { kind: string; name: string }) => {
                return this.translateService.get('application_diff_tip', {
                  action: this.translateService.get(cur),
                  kind: i.kind,
                  name: i.name,
                });
              }),
            );
          },
          [],
        );
      })
      .catch(_ => null)
      .finally(() => {
        this.initialized = true;
        this.cdr.markForCheck();
      });
  }

  fetchLatestYaml() {
    this.appService
      .getApplicationHistoryDetail({
        cluster: this.cluster,
        namespace: this.namespace,
        name: this.appName + '-' + this.latestRevision,
      })
      .then(res => {
        this.latestYaml = res.kubernetes.spec.yaml;
      })
      .catch(_ => null);
  }

  rollback() {
    this.appUtilService
      .rollback({
        cluster: this.cluster,
        namespace: this.namespace,
        revision: this.revision,
        app: this.appName,
      })
      .then(() => {
        this.notificationService.success(
          this.translateService.get('application_rollback_successfully'),
        );
        this.jumpToDetailPage();
      })
      .catch(_ => null);
  }

  jumpToListPage() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  jumpToDetailPage() {
    this.router.navigate(['app', 'detail', this.appName], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  backToHistoryListPage() {
    this.router.navigate(['app', 'detail', this.appName], {
      queryParams: {
        tabIndex: 4,
      },
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
