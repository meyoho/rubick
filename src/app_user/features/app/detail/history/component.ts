import { NotificationService } from '@alauda/ui';
import { state, style, trigger } from '@angular/animations';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { Router } from '@angular/router';

import {
  ApplicationHistory,
  ApplicationHistoryDiff,
} from 'app/services/api/app.service';
import { EnvironmentService } from 'app/services/api/environment.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions } from 'app/typings';
import { delay } from 'app2/utils';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { AppUtilService } from '../../app-util.service';
import { DetailDataService } from '../../detail-data.service';

enum DiffType {
  ROLLBACKFROM = 'rollbackFrom',
  HASDIFF = 'hasDiff',
  NODIFF = 'noDiff',
}

@Component({
  selector: 'rc-application-history',
  templateUrl: './template.html',
  styleUrls: ['./style.scss', '../style.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('display', [
      state('show', style({ display: 'block' })),
      state('hide', style({ display: 'none' })),
    ]),
  ],
})
export class ApplicationHistoryComponent implements OnChanges {
  @Input()
  appHistory: K8sResourceWithActions<ApplicationHistory>[];
  @Input()
  cluster: string;
  @Input()
  namespace: string;
  @Input()
  project: string;
  @Input()
  appName: string;
  @Input()
  appActions: string[];

  initialized: boolean;

  historyItemsReversed: K8sResourceWithActions<ApplicationHistory>[];

  rowExpanded = {};

  DiffType = DiffType;

  constructor(
    private router: Router,
    private cdr: ChangeDetectorRef,
    private workspaceComponent: WorkspaceComponent,
    private translateService: TranslateService,
    private appUtilService: AppUtilService,
    private notificationService: NotificationService,
    private detailDataService: DetailDataService,
    private envService: EnvironmentService,
  ) {}

  async ngOnChanges({ appHistory }: SimpleChanges) {
    if (appHistory && appHistory.currentValue) {
      await delay(100);
      this.historyItemsReversed = this.appHistory.length
        ? this.appHistory.sort(
            (a, b) => b.kubernetes.spec.revision - a.kubernetes.spec.revision,
          )
        : [];
      this.initialized = true;
      this.cdr.markForCheck();
    }
  }

  view(item: K8sResourceWithActions<ApplicationHistory>) {
    this.router.navigate(
      ['app', 'version', this.appName, item.kubernetes.metadata.name],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      },
    );
  }

  getRollbackFrom(item: K8sResourceWithActions<ApplicationHistory>): string {
    return item.kubernetes.metadata.annotations
      ? item.kubernetes.metadata.annotations[
          `app.${this.envService.environments.label_base_domain}/rollback-from`
        ]
      : null;
  }

  getDiffType(item: K8sResourceWithActions<ApplicationHistory>): DiffType {
    return this.getRollbackFrom(item)
      ? DiffType.ROLLBACKFROM
      : item.kubernetes.spec.resourceDiffs
      ? DiffType.HASDIFF
      : DiffType.NODIFF;
  }

  getDiffs(diffData: ApplicationHistoryDiff) {
    return Object.keys(diffData).reduce((acc, cur) => {
      return acc.concat(
        diffData[cur].map((i: { kind: string; name: string }) => {
          return this.translateService.get('application_diff_tip', {
            action: this.translateService.get(cur),
            kind: i.kind,
            name: i.name,
          });
        }),
      );
    }, []);
  }

  rollback(revision: number) {
    this.appUtilService
      .rollback({
        cluster: this.cluster,
        namespace: this.namespace,
        revision: revision,
        app: this.appName,
      })
      .then(() => {
        this.notificationService.success(
          this.translateService.get('application_rollback_successfully'),
        );
        this.detailDataService.refreshPolling();
      })
      .catch(_ => null);
  }

  toggleRow(index: number) {
    this.rowExpanded = {
      ...this.rowExpanded,
      [index]: !this.rowExpanded[index],
    };
  }

  canUpdate(item: K8sResourceWithActions<ApplicationHistory>): boolean {
    return (
      item.resource_actions.includes('k8s_others:update') && this.appActions
    ).includes('application:update');
  }
}
