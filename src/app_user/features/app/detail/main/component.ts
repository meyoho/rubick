import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { filter, find, get, includes, sortBy } from 'lodash-es';
import { Subject } from 'rxjs';

import {
  PodControllerKindEnum,
  PodControllerKinds,
} from 'app/features-shared/app/utils/pod-controller';
import { ApplicationStatus } from 'app/services/api/app.service';
import { DaemonSet, Deployment, StatefulSet } from 'app/typings';
import { K8sResourceWithActions, Service } from 'app/typings';
import { AppMeta } from 'app_user/features/app/detail/meta/component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { AppUtilService } from '../../app-util.service';

export enum ResourceKind {
  Application = 'Application',
  Deployment = 'Deployment',
  Daemonset = 'Daemonset',
  Statefulset = 'Statefulset',
  Other = 'Other',
}

@Component({
  selector: 'rc-app-detail-main',
  templateUrl: './template.html',
  styleUrls: ['../style.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailComponent implements OnChanges {
  @Input()
  appData: K8sResourceWithActions[];
  @Input()
  cluster: string;
  @Input()
  appStatus: ApplicationStatus;
  @Input()
  actions: string[];

  empty = false;
  appMeta: AppMeta;
  project: string;
  namespace: string;
  initialized = false;
  addresses: string[] = []; // 访问地址
  onChange$ = new Subject<null>();
  k8sServices: K8sResourceWithActions<Service>[];
  allResources: Array<{ kind: ResourceKind; data: K8sResourceWithActions[] }>;

  // all resources kind
  application: K8sResourceWithActions;
  deployments: K8sResourceWithActions<Deployment>[];
  statefulsets: K8sResourceWithActions<StatefulSet>[];
  daemonsets: K8sResourceWithActions<DaemonSet>[];
  others: K8sResourceWithActions[];

  constructor(
    private cdr: ChangeDetectorRef,
    private appUtilService: AppUtilService,
    private workspaceComponent: WorkspaceComponent,
  ) {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    [this.namespace, this.project] = [baseParams.namespace, baseParams.project];
  }

  ngOnChanges({ appData }: SimpleChanges) {
    if (appData && appData.currentValue) {
      this.initialized = true;
      this.onChange();
    }
  }

  private onChange() {
    if (!this.appData.length && !this.application) {
      this.empty = true;
      return;
    }
    this.empty = false;
    this.extractAllResources();
    this.getAppAddress();
    this.updateMeta();
  }

  private extractAllResources() {
    this.application = find(this.appData, ['kubernetes.kind', 'Application']);
    this.deployments = filter(this.appData, [
      'kubernetes.kind',
      PodControllerKindEnum.Deployment,
    ]);
    this.daemonsets = filter(this.appData, [
      'kubernetes.kind',
      PodControllerKindEnum.DaemonSet,
    ]);
    this.statefulsets = filter(this.appData, [
      'kubernetes.kind',
      PodControllerKindEnum.StatefulSet,
    ]);
    this.others = filter(
      this.appData,
      i => !includes(['Application', ...PodControllerKinds], i.kubernetes.kind),
    );
    this.allResources = [
      { kind: ResourceKind.Deployment, data: this.deployments },
      { kind: ResourceKind.Daemonset, data: this.daemonsets },
      { kind: ResourceKind.Statefulset, data: this.statefulsets },
      {
        kind: ResourceKind.Other,
        data: sortBy(this.others, 'kubernetes.kind'),
      },
    ];
  }

  private updateMeta() {
    const _app = this.application;
    this.appMeta = _app
      ? {
          name: get(_app, 'kubernetes.metadata.name', ''),
          namespace: get(_app, 'kubernetes.metadata.namespace', ''),
          status: get(_app, 'kubernetes.spec.assemblyPhase', ''),
          annotations: get(_app, 'kubernetes.metadata.annotations', {}),
          creationTimestamp: get(
            _app,
            'kubernetes.metadata.creationTimestamp',
            '',
          ),
          addresses: this.addresses.sort(),
        }
      : null;
  }

  private async getAppAddress() {
    const res = await this.appUtilService.getApplicationAddress({
      cluster: this.cluster,
      namespace: this.namespace,
      app: this.application.kubernetes.metadata.name,
    });
    this.addresses = Object.keys(res).reduce(
      (arr, key) => arr.concat(res[key]),
      [],
    );
    this.updateMeta();
    this.cdr.markForCheck();
  }

  trackByFn(_index: number, resource: { kind: ResourceKind; data: any }) {
    return resource.kind || _index;
  }
}
