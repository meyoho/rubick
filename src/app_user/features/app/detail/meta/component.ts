import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { filter, first } from 'rxjs/operators';

import { ApplicationStatus, AppService } from 'app/services/api/app.service';
import { Cluster } from 'app/services/api/region.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import * as fromStore from 'app/store';
import { Environments } from 'app/typings';
import { ApplicationStatusEnum } from 'app/typings';
import { K8sResourceWithActions } from 'app/typings/backend-api';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

export interface AppMeta {
  name: string;
  namespace: string;
  creationTimestamp: string;
  status: ApplicationStatusEnum;
  annotations: {
    [key: string]: any;
  };
  addresses: string[];
}

@Component({
  selector: 'rc-app-detail-meta',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailMetaComponent implements OnInit, OnChanges {
  @Input()
  meta: AppMeta;
  @Input()
  cluster: string;
  @Input()
  appStatus: ApplicationStatus;
  @Input()
  actions: string[];
  @Input()
  appData: K8sResourceWithActions[];

  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = []; // avaliable mirror clusters
  mirrorAppClusters: {
    name: string;
    display_name: string;
    baseParams: {
      cluster: string;
      namespace: string;
    };
  }[] = [];

  project: string;
  clusterDetail: Cluster;
  namespace: string;
  clusterDisplayName: string;

  displayName: string;

  baseParams: {
    cluster: string;
    namespace: string;
  };

  get labelBaseDomain(): string {
    return this.env.label_base_domain;
  }

  ngOnChanges({ meta }: SimpleChanges): void {
    if (meta && meta.currentValue) {
      this.onInputChange();
    }
  }

  ngOnInit(): void {}

  private async onInputChange() {
    this.baseParams = {
      cluster: this.cluster,
      namespace: this.namespace,
    };
    this.getDisplayName();
    await this.getClusterDetail();
    await this.getClusterDisplayName();
    await this.getMirrorClustesrViewModel();
    this.cdr.markForCheck();
  }

  private async getClusterDetail() {
    this.clusterDetail = await this.store
      .select(fromStore.getCurrentCluster)
      .pipe(
        filter(cluster => !!cluster),
        first(),
      )
      .toPromise();
  }

  private getDisplayName() {
    this.displayName = this.meta.annotations[
      `app.${this.labelBaseDomain}/display-name`
    ];
  }

  private async getClusterDisplayName() {
    this.clusterDisplayName = this.clusterDetail.display_name;
  }

  private async getMirrorClustesrViewModel() {
    this.getMirrorClusters();
    if (this.mirrorClusters.length) {
      const clusterAppMap = await this.appService.batchGetAppDetailCRD({
        project: this.project,
        clusters: this.mirrorClusters.map(c => c.name),
        namespace: this.namespace,
        name: this.meta.name,
      });
      const appClusters = Object.keys(clusterAppMap);
      this.mirrorAppClusters = this.mirrorClusters
        .filter(c => appClusters.indexOf(c.name) >= 0)
        .map(
          ({ name, display_name }: { name: string; display_name: string }) => ({
            name,
            display_name,
            baseParams: {
              cluster: name,
              namespace: this.namespace,
            },
          }),
        );
    }
  }

  getMirrorClusters() {
    const cluster = this.clusterDetail;
    if (cluster.mirror && cluster.mirror.regions) {
      this.mirrorClusters = cluster.mirror.regions
        .filter(c => c.name !== cluster.name)
        .map(({ name, display_name }) => ({ name, display_name }));
    }
  }

  redirectToMirrorApp(cluster: { name: string; display_name: string }) {
    this.router.navigate([
      '/console/user/workspace',
      {
        project: this.project,
        cluster: cluster.name,
        namespace: this.namespace,
      },
      'app',
      'detail',
      this.meta.name,
    ]);
  }

  trackByFn(_index: number, cluster: { name: string; display_name: string }) {
    return cluster.name;
  }

  constructor(
    private appService: AppService,
    private router: Router,
    @Inject(ENVIRONMENTS) public env: Environments,
    private cdr: ChangeDetectorRef,
    private store: Store<fromStore.AppState>,
    private workspaceComponent: WorkspaceComponent,
  ) {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    [this.project, this.namespace] = [baseParams.project, baseParams.namespace];
  }
}
