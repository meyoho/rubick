import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';

import * as Highcharts from 'highcharts';
import { cloneDeep, get } from 'lodash-es';

import { BaseTimeSelectComponent } from 'app/features-shared/alarm/base-time-select.component';
import {
  AGGREGATORS,
  MetricNumericOptions,
  MetricPercentOptions,
  QueryMetrics,
  TIME_STAMP_OPTIONS,
} from 'app/features-shared/app/utils/monitor';
import { AppService } from 'app/services/api/app.service';
import { LogService } from 'app/services/api/log.service';
import {
  Metric,
  MetricQueries,
  MetricQuery,
  MetricService,
} from 'app/services/api/metric.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { KubernetesResource } from 'app/typings/raw-k8s';
require('highcharts/modules/no-data-to-display')(Highcharts);
const uuidv4 = require('uuid/v4');

const QUERY_TIME_STAMP_OPTIONS = TIME_STAMP_OPTIONS.concat([
  {
    type: 'custom_time_range',
    offset: 0,
  },
]);

@Component({
  selector: 'rc-application-monitor-chart',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationMonitorChartComponent extends BaseTimeSelectComponent
  implements OnInit, OnDestroy {
  @Input()
  appName: string;
  @Input()
  resources?: Array<{ kubernetes: KubernetesResource }>;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  queryElements: Array<any>;
  clusterName: string;
  namespaceName: string;
  private intervalTimer: number;

  aggregators = AGGREGATORS;
  timeStampOptions = QUERY_TIME_STAMP_OPTIONS.map(option => {
    return {
      ...option,
      name: option.type,
    };
  });
  selectedAggregator = this.aggregators[0].key;
  timeRangeOutOfDate = false;
  Highcharts = Highcharts;
  cpuChartOptions: Highcharts.Options = cloneDeep(MetricPercentOptions);
  memChartOptions: Highcharts.Options = cloneDeep(MetricPercentOptions);
  sentBytesChartOptions: Highcharts.Options = cloneDeep(MetricNumericOptions);
  receivedBytesChartOptions: Highcharts.Options = cloneDeep(
    MetricNumericOptions,
  );
  queryMetrics = QueryMetrics;
  queryIdMap = new Map();

  constructor(
    private cdr: ChangeDetectorRef,
    logService: LogService,
    auiMessageService: MessageService,
    translateService: TranslateService,
    private appService: AppService,
    private metricService: MetricService,
    @Inject(ACCOUNT) public account: Account,
  ) {
    super(logService, auiMessageService, translateService);
  }

  async ngOnInit() {
    this.initTimer();
    Highcharts.setOptions({
      lang: {
        noData: this.translateService.get('no_data'),
      },
    });
    this.sentBytesChartOptions.tooltip.valueSuffix = this.translateService.get(
      'byte/second',
    );
    this.receivedBytesChartOptions.tooltip.valueSuffix = this.translateService.get(
      'byte/second',
    );
    this.clusterName = this.cluster;
    this.namespaceName = this.namespace;
    const pods = await Promise.all(
      this.resources.map(async el => await this.getPods(el)),
    );
    this.queryElements = this.resources.map((el, i) => {
      return {
        componentName: get(el, 'kubernetes.metadata.name'),
        componentType: get(el, 'kubernetes.kind'),
        pods: pods[i],
      };
    });
    this.resetTimeRange();
    this.loadCharts();
    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    window.clearInterval(this.intervalTimer);
  }

  initTimer() {
    this.intervalTimer = window.setInterval(() => {
      if (!this.chartLoading) {
        if (this.selectedTimeStampOption !== 'custom_time_range') {
          this.resetTimeRange();
        }
        if (this.checkTimeRange()) {
          this.chartLoading = true;
          this.cdr.markForCheck();
          this.loadCharts();
        }
      }
    }, 60000);
  }

  async getPods(resource: { kubernetes: KubernetesResource }) {
    const matchLabels = get(
      resource,
      'kubernetes.spec.selector.matchLabels',
      {},
    );
    const labelSelector = Object.keys(matchLabels || {})
      .map(key => `${key}=${matchLabels[key]}`)
      .join(',');
    const response = await this.appService
      .getResourcesInNamespace({
        cluster: this.cluster,
        namespace: this.namespace,
        labelSelector,
        resource_type: 'pods',
      })
      .then((res: any) => res.results || [])
      .catch(_err => []);
    return response.map((pod: any) => {
      return get(pod, 'metadata.name');
    });
  }

  loadCharts() {
    const args = {
      aggregator: this.selectedAggregator,
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    };
    // http://jira.alaudatech.com/browse/DEV-12815
    let start = 0;
    if (args.end - args.start < 1800) {
      this.step = 60;
      start = args.start;
    } else {
      this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
      start = args.start + this.step;
    }
    this.end_time = args.end;
    const params: MetricQuery = {
      start,
      end: args.end,
      step: this.step,
    };
    const queries = this.queryElements.map(el => {
      return this.queryMetrics.map(query => {
        let pod_name = '';
        if (el.pods.length) {
          pod_name = el.pods.join('|');
        }
        const uuid = uuidv4();
        this.queryIdMap.set(uuid, {
          ...query,
          componentName: el.componentName,
        });
        const metricQuery: MetricQueries = {
          aggregator: args.aggregator,
          id: uuid,
          labels: [
            {
              type: 'EQUAL',
              name: '__name__',
              value: `workload.${query.metric}`,
            },
            {
              type: 'EQUAL',
              name: 'namespace',
              value: this.namespaceName,
            },
            {
              type: 'EQUAL',
              name: 'kind',
              value: el.componentType,
            },
            {
              type: 'EQUAL',
              name: 'name',
              value: el.componentName,
            },
            {
              type: 'IN',
              name: 'pod_name',
              value: pod_name,
            },
          ],
        };
        return metricQuery;
      });
    });
    params.queries = [].concat(...queries);
    this.metricService
      .queryMetrics(this.clusterName, params)
      .then(results => {
        const chartData = new Map<string, Metric[]>([
          ['cpu.utilization', []],
          ['memory.utilization', []],
          ['network.transmit_bytes', []],
          ['network.receive_bytes', []],
        ]);
        results.forEach(result => {
          const { metric } = this.queryIdMap.get(result.metric.__query_id__);
          chartData.set(metric, [result, ...chartData.get(metric)]);
        });
        chartData.forEach(values => {
          if (values.length) {
            const { series, unit } = this.queryIdMap.get(
              values[0]['metric'].__query_id__,
            );
            if (unit) {
              this[series]['series'] = this.parseMetricsResponse(values, 100);
            } else {
              this[series]['series'] = this.parseMetricsResponse(values);
            }
          }
        });
      })
      .catch(error => {
        this.cpuChartOptions['series'] = [];
        this.memChartOptions['series'] = [];
        this.sentBytesChartOptions['series'] = [];
        this.receivedBytesChartOptions['series'] = [];
        throw error;
      })
      .finally(() => {
        this.chartLoading = false;
        this.cdr.markForCheck();
      });
  }

  private parseMetricsResponse(metric: Metric[], multiplier?: number) {
    metric.sort((a, b) => {
      const m_a = this.queryIdMap.get(a.metric.__query_id__);
      const m_b = this.queryIdMap.get(b.metric.__query_id__);
      return m_a.componentName.localeCompare(m_b.componentName);
    });
    return metric.map(el => {
      if (this.step >= 60 && el.values.length < 30) {
        el.values = this.metricService.fillUpResult(
          el.values,
          this.end_time,
          this.step,
        );
      }
      const { componentName } = this.queryIdMap.get(el.metric.__query_id__);
      return {
        name: componentName,
        data: el.values.map((value: Array<number | string>) => {
          let y = null;
          if (value[1] !== '+Inf' && value[1] !== '') {
            y = Number(value[1]);
            if (multiplier) {
              y = y * 100;
            }
          }
          return {
            x: Number(value[0]) * 1000,
            y,
          };
        }),
      };
    });
  }
}
