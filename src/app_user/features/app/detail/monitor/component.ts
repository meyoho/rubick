import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { K8sResourceWithActions } from 'app/typings/backend-api';

@Component({
  selector: 'rc-application-monitor',
  templateUrl: './template.html',
  styleUrls: ['../style.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationMonitorComponent implements OnInit, OnChanges {
  @Input()
  appData: K8sResourceWithActions[];
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  initialized = false;
  empty = false;

  applications: K8sResourceWithActions[];

  ngOnChanges({ appData }: SimpleChanges) {
    if (appData && appData.currentValue) {
      this.applications = this.appData.filter(el => {
        return ['Deployment', 'DaemonSet', 'StatefulSet'].includes(
          el.kubernetes.kind,
        );
      });
      this.empty = !this.appData.length;
      this.initialized = true;
    }
  }

  ngOnInit(): void {}

  constructor() {}
}
