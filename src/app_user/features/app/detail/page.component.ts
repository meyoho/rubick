import { NotificationService } from '@alauda/ui';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { safeDump } from 'js-yaml';
import { find, get, map as _map, sortBy } from 'lodash-es';
import { combineLatest, from, Observable, of, Subject } from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import {
  ApplicationHistory,
  ApplicationStatus,
  AppService,
} from 'app/services/api/app.service';
import { RegionService } from 'app/services/api/region.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments, WorkspaceBaseParams } from 'app/typings';
import { K8sResourceWithActions } from 'app/typings';
import { Pod } from 'app/typings/raw-k8s';
import * as codeEditorUtil from 'app/utils/code-editor-config';
import { delay } from 'app2/utils';
import {
  isHttpErrorResponse,
  isNotHttpErrorResponse,
} from 'app_user/core/types';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

interface FetchParams {
  cluster: string;
  namespace: string;
  app: string; // app name
}

@Component({
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  providers: [DetailDataService],
})
export class ApplicationDetailPageComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  private fetchParams$: Observable<FetchParams>;
  private urlQueryParams$ = this.activatedRoute.queryParams;

  appData$: Observable<any[]>;
  pods$: Observable<Pod[]>;
  cluster$: Observable<string>;
  project$: Observable<string>;
  namespace$: Observable<string>;
  baseParams$: Observable<WorkspaceBaseParams>;
  namespace: string;
  app: string;
  app$: Observable<string>;
  appStatus$: Observable<ApplicationStatus>;
  actions$: Observable<string[]>;
  codeEditorOptions = codeEditorUtil.yamlOptions;
  actionsConfigView = codeEditorUtil.viewActions;
  yaml$: Observable<string>;
  seletedTabIndex$: Observable<number>;
  logInitialState$: Observable<{ pod: string; container: string }>;
  showMetricTab$: Observable<string | {}>;
  appHistory$: Observable<K8sResourceWithActions<ApplicationHistory>[]>;

  get labelBaseDomain(): string {
    return this.env.label_base_domain || 'alauda.io';
  }

  constructor(
    private router: Router,
    private appService: AppService,
    private regionService: RegionService,
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private detailDataService: DetailDataService,
    @Inject(ENVIRONMENTS) public env: Environments,
    private workspaceComponent: WorkspaceComponent,
    private notificationService: NotificationService,
  ) {
    this.detailDataService.startPolling();
    this.onDestroy$.subscribe(_ => this.detailDataService.stopPolling());
  }

  async ngOnInit() {
    this.initStateParams$();
    this.initFetchParams$();
    this.initAppData$();
    this.initActions$();
    this.initYaml$();
  }

  initStateParams$() {
    this.baseParams$ = this.workspaceComponent.baseParams.pipe(
      map(params => ({
        project: params.project,
        cluster: params.cluster,
        namespace: params.namespace,
      })),
    );
    this.cluster$ = this.workspaceComponent.baseParams.pipe(
      map(params => params.cluster),
    );
    this.project$ = this.workspaceComponent.baseParams.pipe(
      map(params => params.project),
    );
    this.namespace = this.workspaceComponent.baseParamsSnapshot.namespace;
    this.app = this.activatedRoute.snapshot.params.app;
    this.showMetricTab$ = combineLatest(
      this.cluster$,
      from(this.regionService.getRegions()),
    ).pipe(
      map(([regionName, regions]) => {
        return regions.find(region => region.name === regionName);
      }),
      map(region => {
        if (
          get(region, 'features.customized.metric') ||
          get(region, 'features.metric.type')
        ) {
          return true;
        } else {
          return false;
        }
      }),
      catchError(() => ''),
    );
    this.seletedTabIndex$ = combineLatest(
      this.showMetricTab$,
      this.urlQueryParams$,
    ).pipe(map(([_showMetricTab, queryParams]) => queryParams.tabIndex || 0));
    this.logInitialState$ = this.urlQueryParams$.pipe(
      map(queryParams => ({
        pod: queryParams.pod,
        container: queryParams.container,
      })),
    );
  }

  initFetchParams$() {
    this.fetchParams$ = combineLatest(
      this.cluster$,
      this.detailDataService.polling$,
    ).pipe(
      map(([cluster]) => ({
        cluster,
        namespace: this.namespace,
        app: this.app,
      })),
    );
  }

  initAppData$() {
    this.appData$ = this.fetchParams$.pipe(
      takeUntil(this.onDestroy$),
      debounceTime(50),
      switchMap(params =>
        this.fetchData(params).pipe(
          catchError(err => of(err)),
          tap(res => {
            if (isHttpErrorResponse(res) && res.status === 404) {
              this.notificationService.warning(
                this.translateService.get('application_not_exist'),
              );
              this.jumpToListPage();
            } else if (isNotHttpErrorResponse(res)) {
              this.detailDataService.setApplicationData(
                _map(res, i => i.kubernetes),
              );
            }
          }),
          map(res => (isNotHttpErrorResponse(res) ? res : [])),
        ),
      ),
      publishReplay(1),
      refCount(),
    );

    this.pods$ = this.fetchParams$.pipe(
      takeUntil(this.onDestroy$),
      debounceTime(50),
      switchMap(params =>
        this.fetchPods(params).pipe(
          catchError(err => of(err)),
          map(res =>
            isNotHttpErrorResponse(res)
              ? _map(res.result, i => i.kubernetes)
              : [],
          ),
        ),
      ),
      publishReplay(1),
      refCount(),
    );

    this.appStatus$ = this.fetchParams$.pipe(
      takeUntil(this.onDestroy$),
      debounceTime(50),
      switchMap(params => this.fetchStatus(params)),
      publishReplay(1),
      refCount(),
    );

    this.appHistory$ = this.fetchParams$.pipe(
      takeUntil(this.onDestroy$),
      switchMap(params =>
        this.fetchHistory(params).pipe(
          catchError(err => of(err)),
          map(res => (isNotHttpErrorResponse(res) ? res : [])),
        ),
      ),
      publishReplay(1),
      refCount(),
    );
  }

  initActions$() {
    this.actions$ = this.appData$.pipe(
      filter(app => !!app),
      map(
        app =>
          (find(app, ['kubernetes.kind', 'Application']) || {})
            .resource_actions || [],
      ),
    );
  }

  initYaml$() {
    this.yaml$ = this.appData$.pipe(
      filter(app => !!app),
      map(app =>
        _map(sortBy(app, 'kubernetes.kind'), i =>
          safeDump(get(i, 'kubernetes', {}), {
            sortKeys: true,
          }),
        ).join('---\r\n'),
      ),
    );
  }

  fetchData(params: FetchParams): Observable<K8sResourceWithActions[]> {
    return from(
      this.appService.getAppDetailCRD({
        cluster_name: params.cluster,
        namespace: params.namespace,
        app: params.app,
      }),
    );
  }

  fetchPods(params: {
    cluster: string;
    namespace: string;
    app: string;
  }): Observable<K8sResourceWithActions<Pod>> {
    return from(
      this.appService.getApplicationPods({
        cluster: params.cluster,
        namespace: params.namespace,
        app: params.app,
      }),
    );
  }

  fetchStatus(params: {
    cluster: string;
    namespace: string;
    app: string;
  }): Observable<ApplicationStatus> {
    return from(
      this.appService.getApplicationStatus({
        cluster: params.cluster,
        namespace: params.namespace,
        app: params.app,
      }),
    );
  }

  fetchHistory(params: {
    cluster: string;
    namespace: string;
    app: string;
  }): Observable<K8sResourceWithActions<ApplicationHistory>[]> {
    return from(
      this.appService.getApplicationHistoryList({
        cluster: params.cluster,
        namespace: params.namespace,
        app: params.app,
      }),
    );
  }

  async onTabIndexChange(index: number) {
    const currentQueryParams = this.activatedRoute.snapshot.queryParams;
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        ...currentQueryParams,
        tabIndex: index,
      },
    });

    await delay(100);
    if (index !== 0) {
      this.detailDataService.suspendPolling();
    } else {
      this.detailDataService.resumePolling();
    }
  }

  jumpToListPage() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
