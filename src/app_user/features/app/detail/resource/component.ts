import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { safeDump } from 'js-yaml';
import { find, get, includes } from 'lodash-es';
import { from, of, Subject } from 'rxjs';
import { catchError, delay, first, switchMap, takeUntil } from 'rxjs/operators';

import { AppShardService } from 'app/features-shared/app/app-shard.service';
import { PodControllerKindToResourceTypeEnum } from 'app/features-shared/app/utils/pod-controller';
import { ExecCommandDialogComponent } from 'app/features-shared/exec/exec-command/component';
import { AccountService } from 'app/services/api/account.service';
import { ApplicationStatus, AppService } from 'app/services/api/app.service';
import { TerminalService } from 'app/services/api/terminal.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, PodController } from 'app/typings';
import { Account } from 'app/typings';
import { Container, Pod } from 'app/typings/raw-k8s';
import { viewActions } from 'app/utils/code-editor-config';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { ResourceKind } from 'app_user/features/app/detail/main/component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { removeDirtyDataBeforeUpdate } from '../../util';

@Component({
  selector: 'rc-app-detail-resource',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationDetailResourceComponent
  implements OnInit, OnChanges, OnDestroy {
  @Input()
  kind: ResourceKind;
  @Input()
  resources: K8sResourceWithActions[];
  @Input()
  cluster: string;
  @Input()
  namespace: string;
  @Input()
  project: string;
  @Input()
  appStatus: ApplicationStatus;
  userToken: string;
  yamlInputValue: string;
  edtiorActions = viewActions;
  editorOptions = {
    language: 'yaml',
    readOnly: true,
    renderLineHighlight: 'none',
  };
  app: string;

  filterName: string;

  containers = {};
  podStatus = {};

  pods: K8sResourceWithActions<Pod>[] = [];
  podsLoading: boolean;
  podsLoaded: boolean;

  private updateReplicas$ = new Subject<{
    replicas: number;
    resource: K8sResourceWithActions;
  }>();
  private onDestroy$ = new Subject<void>();

  get resourceKind(): string {
    return this.kind === ResourceKind.Other
      ? this.translate.get('k8s_others')
      : this.kind;
  }

  get isUserView(): boolean {
    return this.accountService.isUserView();
  }

  get filteredResources() {
    return this.filterName
      ? this.resources.filter(r =>
          r.kubernetes.metadata.name
            .toLowerCase()
            .includes(this.filterName.toLowerCase()),
        )
      : this.resources;
  }

  constructor(
    @Inject(ACCOUNT) public account: Account,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private workspaceComponent: WorkspaceComponent,
    private activatedRoute: ActivatedRoute,
    private accountService: AccountService,
    private router: Router,
    private appUtilService: AppUtilService,
    private terminalService: TerminalService,
    private appService: AppService,
    private detailDataService: DetailDataService,
    private errorToastService: ErrorsToastService,
    private notificationService: NotificationService,
    private appShardService: AppShardService,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.app = params.app;

    this.updateReplicas$
      .pipe(
        switchMap(({ replicas, resource }) => {
          return of({ replicas, resource }).pipe(delay(1000));
        }),
        switchMap(({ replicas, resource }) => {
          const applicationData = this.detailDataService.applicationData$.value;
          const payload = find(
            applicationData,
            i =>
              i.kind === resource.kubernetes.kind &&
              i.metadata.name === resource.kubernetes.metadata.name,
          );
          payload.spec.replicas = replicas;
          removeDirtyDataBeforeUpdate(payload);
          return from(
            this.appService.updateResource({
              cluster: this.cluster,
              namespace: this.namespace,
              resource_type:
                PodControllerKindToResourceTypeEnum[resource.kubernetes.kind],
              name: resource.kubernetes.metadata.name,
              payload,
            }),
          ).pipe(
            catchError(err => {
              this.errorToastService.error(err);
              return of(err);
            }),
          );
        }),
        takeUntil(this.onDestroy$),
      )
      .subscribe(() => {
        this.detailDataService.refreshPolling();
      });
  }

  ngOnChanges({ resources, cluster }: SimpleChanges): void {
    if (resources && resources.currentValue) {
      if (this.kind !== 'Other') {
        this.onInputChange();
      }
    }
    if (cluster && cluster.currentValue && !cluster.firstChange) {
      this.onInputChange();
    }
  }

  private onInputChange() {
    this.getAllContainers();
    this.getAllPodStatus();
  }

  private getAllContainers() {
    (this.resources || []).forEach(r => {
      this.containers[get(r, 'kubernetes.metadata.name')] = this.getContainers(
        r,
      );
    });
  }

  getContainers(resource: any) {
    return get(resource, 'kubernetes.spec.template.spec.containers', []).map(
      (c: Container) => ({
        name: c.name || '-',
        image: c.image || '-',
        cpu: this.appShardService.formatContainerCPU(
          get(c, 'resources.limits.cpu'),
        ),
        memory: get(c, 'resources.limits.memory', '-'),
      }),
    );
  }

  async getPods(resource: K8sResourceWithActions<PodController>) {
    const matchLabels = get(
      resource,
      'kubernetes.spec.selector.matchLabels',
      {},
    );
    const labelSelector = Object.keys(matchLabels || {})
      .map(key => `${key}=${matchLabels[key]}`)
      .join(',');

    return this.appService
      .getResourcesInNamespace({
        cluster: this.cluster,
        namespace: this.namespace,
        labelSelector,
        resource_type: 'pods',
      })
      .then((res: any) => res.results || [])
      .catch(_err => []);
  }

  async onMenuShow(resource: K8sResourceWithActions<PodController>) {
    this.podsLoading = true;

    this.pods = await this.getPods(resource);

    this.podsLoaded = true;
    this.podsLoading = false;
    this.cdr.markForCheck();
  }

  onMenuHide() {
    this.podsLoaded = false;
  }

  private getAllPodStatus() {
    (this.resources || []).forEach(r => {
      const workloads = get(
        this.appStatus,
        `workloads.${r.kubernetes.kind.toLowerCase() +
          '-' +
          r.kubernetes.metadata.name}`,
        {},
      );
      this.podStatus[get(r, 'kubernetes.metadata.name')] = {
        desired: workloads.desired || 0,
        current: workloads.current || 0,
        status: workloads.status || 'Unknown',
      };
    });
    this.cdr.markForCheck();
  }

  canEXEC(pod: K8sResourceWithActions<Pod>, container: Container): boolean {
    const podStatus = get(pod, 'kubernetes.status.containerStatuses', null);
    if (!podStatus) {
      return false;
    }
    const containerStatus = find(podStatus, co => co.name === container.name);

    return !!get(containerStatus, 'state.running', null);
  }

  canNotEXECAll(
    pods: K8sResourceWithActions<Pod>[],
    container: Container,
  ): boolean {
    return pods.every(pod => {
      return !this.canEXEC(pod, container);
    });
  }

  openTerminal(
    pods: K8sResourceWithActions<Pod>[],
    container: Container,
    resource: K8sResourceWithActions<PodController>,
    selected?: K8sResourceWithActions<Pod>,
  ) {
    const dialogRef = this.dialogService.open(ExecCommandDialogComponent);
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (!!data) {
        const podNames = pods
          .map(pod => get(pod, 'kubernetes.metadata.name', ''))
          .join(';');
        const selectedName = get(selected, 'kubernetes.metadata.name', '');
        const containerName = container.name;
        this.terminalService.openTerminal({
          app: this.app,
          ctl: resource.kubernetes.metadata.name,
          pods: podNames,
          selectedPod: selectedName || '',
          container: containerName,
          namespace: this.namespace,
          cluster: this.cluster,
          user: data.user,
          command: data.command,
        });
      }
    });
  }

  view(data: any, templateRef: TemplateRef<any>) {
    this.yamlInputValue = safeDump(data, { lineWidth: 9999, sortKeys: true });
    this.dialogService.open(templateRef, {
      size: DialogSize.Large,
      data: {
        name: data.metadata.name,
      },
    });
  }

  viewLog(pod: any, container: any) {
    const podName = get(pod, 'kubernetes.metadata.name', '');
    const containerName = container.name;

    return this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 5,
        pod: podName,
        container: containerName,
      },
    });
  }

  canUpdate(resource: {
    resource_actions: string[];
    kubernetes: PodController;
  }) {
    return includes(resource.resource_actions, 'k8s_others:update');
  }

  isDaemonSet(resource: {
    resource_actions: string[];
    kubernetes: PodController;
  }) {
    return resource.kubernetes.kind === 'DaemonSet';
  }

  navigateToComponent(component: K8sResourceWithActions) {
    this.router.navigate(
      [
        'app',
        'detail',
        this.app,
        component.kubernetes.kind.toLowerCase(),
        component.kubernetes.metadata.name,
      ],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      },
    );
  }

  onSearch(queryString: string) {
    this.filterName = queryString.trim();
    setTimeout(() => this.cdr.markForCheck(), 100);
  }

  onSearchKeyChange(queryString: string) {
    this.onSearch(queryString);
  }

  updateReplicas(replicas: number, resource: K8sResourceWithActions) {
    this.updateReplicas$.next({
      replicas,
      resource,
    });
  }

  trackByFn(_index: number, resource: any) {
    return resource.kubernetes.metadata.name || _index;
  }

  getPodShortName(podFullName: string, resourceName: string) {
    return podFullName.substring(resourceName.length + 1);
  }

  updateImageTag(containerName: string, resource: K8sResourceWithActions) {
    this.appUtilService
      .updateImageTag({
        podController: resource.kubernetes,
        containerName,
        baseParams: {
          cluster: this.cluster,
          namespace: this.namespace,
          project: this.project,
        },
      })
      .subscribe(res => {
        if (res) {
          this.notificationService.success(
            this.translate.get('update_success'),
          );
          this.detailDataService.refreshPolling();
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next(null);
  }
}
