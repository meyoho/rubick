import { DIALOG_DATA, MessageService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
} from '@angular/core';

import { safeDump, safeLoad } from 'js-yaml';

import { PodControllerKinds } from 'app/features-shared/app/utils/pod-controller';
import { TranslateService } from 'app/translate/translate.service';
import { KubernetesResource } from 'app/typings/raw-k8s';
import { updateActions } from 'app/utils/code-editor-config';
import { AppDataService } from '../../app-data.service';

@Component({
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AddResourceComponent implements OnInit {
  @Output()
  close = new EventEmitter<KubernetesResource>();
  yaml: string;
  isUpdate: boolean;
  original: string;
  editorActions = updateActions;
  editorOptions = {
    language: 'yaml',
  };

  constructor(
    @Inject(DIALOG_DATA)
    private data: {
      appDataService: AppDataService;
      resource?: KubernetesResource;
    },
    private notificationService: NotificationService,
    private messageService: MessageService,
    private translate: TranslateService,
  ) {}

  ngOnInit() {
    if (this.data && this.data.resource) {
      this.isUpdate = true;
      this.yaml = safeDump(this.data.resource);
      this.original = this.yaml;
    }
  }

  cancel() {
    this.close.next();
  }

  confirm() {
    let resource;
    try {
      resource = safeLoad(this.yaml);
    } catch (err) {
      return this.notificationService.error({
        title: this.translate.get('yaml_format_error_message'),
        content: err.message,
      });
    }
    if (!resource.kind || !resource.metadata || !resource.metadata.name) {
      return this.messageService.error(
        this.translate.get('yaml_format_error_message'),
      );
    }
    if (!this.checkResourceName(resource)) {
      return this.messageService.error(
        this.translate.get('resource_name_conflict'),
      );
    }
    this.close.next(resource);
  }

  private checkResourceName(resource: KubernetesResource) {
    let original;
    if (this.data.resource) {
      original = {
        name: this.data.resource.metadata.name,
        kind: this.data.resource.kind,
      };
    }
    if (PodControllerKinds.includes(resource.kind)) {
      return this.data.appDataService.checkComponentName(
        {
          controller: resource,
        },
        original,
      );
    } else {
      return this.data.appDataService.checkResourceName(resource, original);
    }
  }
}
