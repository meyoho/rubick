import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { get, set } from 'lodash-es';

import { PodControllerKindToResourceTypeEnum } from 'app/features-shared/app/utils/pod-controller';
import { AppService } from 'app/services/api/app.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { StringMap } from 'app/typings';
import { PodController } from 'app/typings/backend-api';
import { removeDirtyDataBeforeUpdate } from '../../util';

/**
 * 更新容器组标签：{Deployment}.spec.template.metadata.labels
 */
@Component({
  templateUrl: './template.html',
})
export class UpdateLabelDialogComponent implements OnInit {
  constructor(
    private appService: AppService,
    private errorToastService: ErrorsToastService,
    private k8sResourceService: K8sResourceService,
    @Inject(DIALOG_DATA)
    private data: {
      podController: PodController;
      baseParams: {
        cluster: string;
        namespace: string;
      };
    },
  ) {}

  @Output()
  close = new EventEmitter<boolean>();
  @ViewChild(NgForm)
  form: NgForm;
  submitting = false;
  labels: StringMap;
  readonlyLabelKeys: string[] = [];

  ngOnInit(): void {
    const matchLabels = get(
      this.data,
      'podController.spec.selector.matchLabels',
      {},
    );
    this.labels = get(
      this.data,
      'podController.spec.template.metadata.labels',
      {},
    );
    const filteredLabels = this.k8sResourceService.getUserAndBuiltInLabelsOrAnnotations(
      this.labels,
    );
    this.readonlyLabelKeys = Object.keys(
      Object.assign(filteredLabels.builtIn, matchLabels),
    );
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    set(this.data, 'podController.spec.template.metadata.labels', this.labels);
    this.appService
      .updateResource({
        cluster: this.data.baseParams.cluster,
        namespace: this.data.baseParams.namespace,
        resource_type:
          PodControllerKindToResourceTypeEnum[this.data.podController.kind],
        name: this.data.podController.metadata.name,
        payload: removeDirtyDataBeforeUpdate(this.data.podController),
      })
      .then(() => {
        this.submitting = false;
        this.close.next(true);
      })
      .catch(err => {
        this.submitting = false;
        this.errorToastService.error(err);
      });
  }

  cancel() {
    this.close.next(null);
  }
}
