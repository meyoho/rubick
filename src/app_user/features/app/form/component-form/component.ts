import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { OnDestroy } from '@angular/core';

import { get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';
import { filter, takeUntil } from 'rxjs/operators';

import { PodControllerKindEnum } from 'app/features-shared/app/utils/pod-controller';
import { PodController } from 'app/typings/backend-api';
import { ComponentFormModel } from 'app/typings/k8s-form-model';

@Component({
  selector: 'rc-component-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentFormComponent
  extends BaseResourceFormGroupComponent<ComponentFormModel>
  implements OnInit, OnDestroy {
  @Input()
  project: string;
  @Input()
  cluster: string;
  @Input()
  namespace: string;
  @Input() appName: string;
  @Input() hasPrefix = false;
  @Input()
  isUpdate: boolean;
  onDestroy$ = new Subject<void>();
  shouldHideHPA: boolean;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    this.form
      .get('controller')
      .valueChanges.pipe(
        takeUntil(this.onDestroy$),
        filter(value => !!value),
      )
      .subscribe((value: PodController) => {
        const kind = value.kind;
        if (!kind || kind === PodControllerKindEnum.DaemonSet) {
          this.shouldHideHPA = true;
        } else {
          this.shouldHideHPA = false;
        }
        this.cdr.detectChanges();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createForm() {
    return this.fb.group({
      controller: this.fb.control({}),
      autoscaler: this.fb.control(''),
      services: this.fb.array([]),
    });
  }

  getDefaultFormModel(): ComponentFormModel {
    return {
      controller: {},
    };
  }

  adaptFormModel(formModel: ComponentFormModel) {
    const { autoscaler, controller } = formModel;
    if (autoscaler) {
      autoscaler.spec.scaleTargetRef.name = get(
        controller,
        'metadata.name',
        '',
      );
      autoscaler.spec.scaleTargetRef.kind = get(controller, 'kind', '');
      if (!autoscaler.metadata) {
        autoscaler.metadata = {};
      }
      autoscaler.metadata.name = get(controller, 'metadata.name');
    }

    return controller.kind === PodControllerKindEnum.DaemonSet
      ? {
          controller,
        }
      : {
          controller,
          autoscaler,
        };
  }
}
