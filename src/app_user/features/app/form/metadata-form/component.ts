import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';
import { ApplicationMetaDataFormModel } from 'app/typings/k8s-form-model';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';

@Component({
  selector: 'rc-application-metadata-form',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationMetadataFormComponent
  extends BaseResourceFormGroupComponent<ApplicationMetaDataFormModel>
  implements OnInit {
  @Input()
  isUpdate: boolean;
  @Input()
  namespace: string;
  environments: Environments;
  resourceNamePattern = K8S_RESOURCE_NAME_BASE;

  constructor(injector: Injector) {
    super(injector);
    this.environments = this.injector.get(ENVIRONMENTS);
  }

  getResourceMergeStrategy() {
    return true;
  }

  getDefaultFormModel() {
    return {};
  }

  adaptFormModel(formModel: ApplicationMetaDataFormModel) {
    return { namespace: this.namespace, createMethod: 'UI', ...formModel };
  }

  adaptResourceModel(resource: ApplicationMetaDataFormModel) {
    return resource;
  }

  createForm() {
    return this.fb.group({
      name: [
        '',
        [
          Validators.required,
          Validators.maxLength(32),
          Validators.pattern(K8S_RESOURCE_NAME_BASE.pattern),
        ],
      ],
      displayName: [
        {
          value: '',
          disabled: false,
        },
      ],
    });
  }
}
