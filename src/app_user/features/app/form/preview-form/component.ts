import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { ChangeDetectorRef, OnDestroy } from '@angular/core';
import { NgForm } from '@angular/forms';

import { safeDump } from 'js-yaml';
import { flatten, get } from 'lodash-es';
import { Subject } from 'rxjs';
import { filter, first, take, takeUntil } from 'rxjs/operators';

import { AppShardService } from 'app/features-shared/app/app-shard.service';
import { PodControllerKindEnum } from 'app/features-shared/app/utils/pod-controller';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import {
  Environments,
  KubernetesResource,
  WorkspaceBaseParams,
} from 'app/typings';
import { RcImageSelection } from 'app/typings/k8s-form-model';
import {
  ApplicationFormModel,
  ComponentFormModel,
} from 'app/typings/k8s-form-model';
import { Container, Service, ServiceTypeMeta } from 'app/typings/raw-k8s';
import { updateActions } from 'app/utils/code-editor-config';
import { ServiceFormDialogComponent } from 'app_user/features/app/form/services-form/dialog';
import { AppDataService } from '../../app-data.service';
import { AddResourceComponent } from '../../dialog/add-resource/component';
import { generateApplicationModel, getResourcesFromYaml } from '../../util';

@Component({
  selector: 'rc-app-create-preview-form',
  templateUrl: './template.html',
  styleUrls: ['../../create/styles.common.scss', './styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationCreatePreviewFormComponent
  implements OnInit, OnDestroy {
  @ViewChild(NgForm)
  form: NgForm;
  @Input() isUpdate: boolean;
  @Input() baseParams: WorkspaceBaseParams;
  @Input() mirrorClusters: {
    name: string;
    display_name: string;
  }[];
  @Output() onAddComponent = new EventEmitter<RcImageSelection>();
  @Output() onUpdateComponent = new EventEmitter<{
    name: string;
    kind: string;
  }>();
  @Output() onSubmit = new EventEmitter<void>();
  @Output() onCancel = new EventEmitter<void>();

  onDestroy$ = new Subject<void>();

  initialized: boolean;
  application: ApplicationFormModel;
  yaml = '';
  latestYaml = '';
  mode = 'list';
  columns = ['name', 'type', 'image', 'action'];
  editorActions = updateActions;
  editorOptions = {
    language: 'yaml',
  };
  constructor(
    @Inject(ENVIRONMENTS) private environments: Environments,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private messageService: MessageService,
    private notificationService: NotificationService,
    private dialogService: DialogService,
    private appDataService: AppDataService,
    private appShardService: AppShardService,
  ) {}

  get resources(): KubernetesResource[] {
    const podContorollers = this.application.components.map(c => c.controller);
    return flatten([
      podContorollers,
      this.application.services,
      this.application.resources,
    ]).filter(r => !!r);
  }

  ngOnInit() {
    this.appDataService.resourceNotFound$.pipe(take(1)).subscribe(res => {
      if (res) {
        this.messageService.error(this.translate.get('resource_not_exist'));
        this.onCancel.emit();
      }
      this.cdr.markForCheck();
    });

    this.appDataService.applicationModel$
      .pipe(
        filter(item => !!item),
        takeUntil(this.onDestroy$),
      )
      .subscribe((formModel: ApplicationFormModel) => {
        if (!this.application) {
          this.application = formModel;
          this.initialized = true;
        } else {
          this.application = this.getMergedApplicationModel(formModel);
        }
        this.cdr.markForCheck();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  submit() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      if (
        this.mode === 'list' ||
        (this.mode === 'yaml' && this.yamlToFormModel())
      ) {
        return this.onSubmit.emit(null);
      }
    }
  }

  cancel() {
    this.onCancel.emit(null);
  }

  getContainerSize(container: Container) {
    return {
      cpu: get(container, 'resources.limits.cpu', '-'),
      memory: get(container, 'resources.limits.memory', '-'),
    };
  }

  getResourceKindGroup(kind: string) {
    switch (kind) {
      case PodControllerKindEnum.Deployment:
      case PodControllerKindEnum.DaemonSet:
      case PodControllerKindEnum.StatefulSet:
        return 'PodController';
      case 'Service':
        return 'Service';
      default:
        return 'Resource';
    }
  }

  modeChange(mode: 'list' | 'yaml') {
    if (this.mode === mode) {
      return;
    }
    if (mode === 'yaml') {
      this.yaml = this.formModelToYaml();
      this.latestYaml = this.yaml;
    } else if (mode === 'list') {
      this.yamlToFormModel();
    }
  }

  trackByFn(_index: number) {
    return _index;
  }

  async addComponent() {
    this.appShardService
      .selectImage({
        projectName: this.baseParams.project,
      })
      .subscribe((params: RcImageSelection) => {
        if (params) {
          this.onAddComponent.emit(params);
        }
      });
  }

  addService() {
    const dialogRef = this.dialogService.open(ServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        podControllers: this.getPodControllerOptions(),
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: Service) => {
        dialogRef.close();
        if (!!data) {
          this.appDataService.addService({ ...data });
        }
      });
  }

  addResourceFromYaml() {
    const dialogRef = this.dialogService.open(AddResourceComponent, {
      size: DialogSize.Large,
      data: {
        appDataService: this.appDataService,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((resource: KubernetesResource) => {
        dialogRef.close();
        if (!!resource) {
          switch (resource.kind) {
            case PodControllerKindEnum.Deployment:
            case PodControllerKindEnum.DaemonSet:
            case PodControllerKindEnum.StatefulSet:
              return this.appDataService.addComponent({
                controller: resource,
              });
            case ServiceTypeMeta.kind:
              return this.appDataService.addService(resource);
            default:
              return this.appDataService.addResource(resource);
          }
        }
      });
  }

  // TODO: any should be removed after HPA is deprecated
  updateResource(i: number, resource: any) {
    const resources = this.resources;
    const res = resources[i];
    switch (res.kind) {
      case PodControllerKindEnum.Deployment:
      case PodControllerKindEnum.DaemonSet:
      case PodControllerKindEnum.StatefulSet:
        this.updateComponent({
          name: res.metadata.name,
          kind: res.kind.toLowerCase(),
        });
        break;
      case ServiceTypeMeta.kind: {
        const index = i - this.application.components.length;
        this.updateService(index);
        break;
      }
      default: {
        this._updateResource(resource);
        break;
      }
    }
  }

  removeResource(i: number) {
    const resources = this.resources;
    const res = resources[i];
    switch (res.kind) {
      case PodControllerKindEnum.Deployment:
      case PodControllerKindEnum.DaemonSet:
      case PodControllerKindEnum.StatefulSet:
        this.removeComponent(i);
        break;
      case 'Service': {
        const index = i - this.application.components.length;
        this.removeService(index);
        break;
      }
      default: {
        const index =
          i -
          this.application.components.length -
          this.application.services.length;
        this._removeResource(index);
        break;
      }
    }
  }

  private updateComponent(params: { name: string; kind: string }) {
    this.onUpdateComponent.emit(params);
  }

  private removeComponent(index: number) {
    this.appDataService.removeComponent(index);
  }

  private updateService(index: number) {
    const service = this.application.services[index];
    const dialogRef = this.dialogService.open(ServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        data: { ...service },
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        podControllers: this.getPodControllerOptions(),
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: Service) => {
        dialogRef.close();
        if (!!data) {
          this.appDataService.updateService(index, { ...data });
        }
      });
  }

  private removeService(i: number) {
    this.appDataService.removeService(i);
  }

  private _updateResource(resource: KubernetesResource) {
    const original = {
      name: resource.metadata.name,
      kind: resource.kind,
    };
    const dialogRef = this.dialogService.open(AddResourceComponent, {
      size: DialogSize.Fullscreen,
      data: {
        appDataService: this.appDataService,
        resource,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((resource: KubernetesResource) => {
        dialogRef.close();
        if (!!resource) {
          return this.appDataService.updateResource(resource, original);
        }
      });
  }

  private _removeResource(index: number) {
    this.appDataService.removeResource(index);
  }

  private getPodControllerOptions(): { name: string; kind: string }[] {
    return this.application.components.map((c: ComponentFormModel) => {
      return {
        name: c.controller.metadata.name,
        kind: c.controller.kind,
      };
    });
  }

  private yamlToFormModel() {
    try {
      const resources = getResourcesFromYaml(
        this.yaml,
        this.baseParams.namespace,
      );
      const formModel = generateApplicationModel(
        resources,
        this.environments.label_base_domain,
      );
      this.appDataService.setApplicationFormModel(
        this.getMergedApplicationModel({
          ...formModel,
          clusters: this.application.clusters,
        }),
      );
    } catch (err) {
      this.notificationService.error({
        title: this.translate.get('yaml_format_error_message'),
        content: err.message,
      });
      return false;
    }
    return true;
  }

  private formModelToYaml() {
    let yaml = '';
    const payload = this.appDataService.getCurrentApplicationPayload();
    try {
      const jsonArray = JSON.parse(JSON.stringify(payload.kubernetes));
      yaml = jsonArray
        .sort((a: KubernetesResource, b: KubernetesResource) =>
          a.kind <= b.kind ? -1 : 1,
        )
        .map((res: KubernetesResource) =>
          safeDump(res, {
            sortKeys: true,
          }),
        )
        .join('---\r\n');
    } catch (err) {
      console.log(err);
    }
    return yaml;
  }

  // TODO: refactor
  private getMergedApplicationModel(formModel: ApplicationFormModel) {
    return {
      ...this.application,
      components: formModel.components,
      services: formModel.services,
      resources: formModel.resources,
      clusters: formModel.clusters,
    };
  }

  // @Deprecated
  // private async getMirrorApps() {
  //   await this.getMirrorClusters();
  //   if (this.mirrorClusters.length > 1) {
  //     let clusters;
  //     try {
  //       const clusterAppMap = await this.appService.batchGetAppDetailCRD({
  //         project: this.project,
  //         clusters: this.mirrorClusters.map(c => c.name),
  //         namespace: this.namespace,
  //         name: this.application.metadata.name,
  //       });
  //       clusters = Object.keys(clusterAppMap);
  //     } catch (_e) {
  //       clusters = [this.cluster];
  //     }

  //     if (!this.application.clusters || !this.application.clusters.length) {
  //       this.application.clusters = clusters;
  //     }
  //     this.mirrorClusters = this.mirrorClusters.filter(
  //       c => this.application.clusters.indexOf(c.name) >= 0,
  //     );
  //   }
  // }
}
