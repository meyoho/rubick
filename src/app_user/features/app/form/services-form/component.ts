import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectorRef,
  Component,
  forwardRef,
  Input,
  OnInit,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { first } from 'rxjs/operators';

import { Service } from 'app/typings';
import { ServiceFormDialogComponent } from 'app_user/features/app/form/services-form/dialog';

@Component({
  selector: 'rc-services-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ServicesFormComponent),
      multi: true,
    },
  ],
})
export class ServicesFormComponent implements OnInit, ControlValueAccessor {
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  services: Service[] = [];

  constructor(
    private cdr: ChangeDetectorRef,
    private dialogService: DialogService,
  ) {}

  ngOnInit() {}

  onChange() {
    this.onCvaChange(this.services);
  }

  onCvaChange = (_: Service[]) => {};
  onCvaTouched = () => {};
  onValidatorChange = () => {};

  registerOnChange(fn: (value: Service[]) => void): void {
    this.onCvaChange = fn;
  }

  registerOnTouched(fn: () => void): void {
    this.onCvaTouched = fn;
  }

  writeValue(_resource: Service[]) {
    this.services = _resource || [];
    this.cdr.markForCheck();
  }

  add() {
    const dialogRef = this.dialogService.open(ServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        cluster: this.cluster,
        namespace: this.namespace,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: Service) => {
        dialogRef.close();
        if (!!data) {
          this.services.push(data);
        }
        this.onChange();
      });
  }

  remove(index: number) {
    // this.volumes = this.volumes.splice(index, 1);
    this.services = this.services.filter((_v, i: number) => {
      return index !== i;
    });
    this.onChange();
  }

  // removePort(index: number) {}

  update(index: number) {
    const service = this.services[index];
    const dialogRef = this.dialogService.open(ServiceFormDialogComponent, {
      size: DialogSize.Large,
      data: {
        data: { ...service },
        cluster: this.cluster,
        namespace: this.namespace,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((data: Service) => {
        dialogRef.close();
        if (!!data) {
          this.services[index] = { ...data };
        }
        this.onChange();
      });
  }

  getType(service: Service) {
    // TODO
    if (service.spec.type === 'ClusterIP') {
      return 'HTTP (ClusterIP)';
    } else {
      return 'TCP (NodePort)';
    }
  }
}
