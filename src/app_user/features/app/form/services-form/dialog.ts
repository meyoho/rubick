import { DIALOG_DATA } from '@alauda/ui';
import { Component, EventEmitter, Inject, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { getDefaultService } from 'app/features-shared/app/utils/service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { TranslateService } from 'app/translate/translate.service';
import { LoadBalancer, Service } from 'app/typings';

@Component({
  templateUrl: 'dialog.html',
})
export class ServiceFormDialogComponent {
  @ViewChild(NgForm)
  form: NgForm;
  service: Service;
  relatedAlbs: LoadBalancer[] = [];
  close = new EventEmitter<Service>();
  podControllers: { name: string; kind: string }[] = [];
  titlePrefix: string;

  constructor(
    @Inject(DIALOG_DATA)
    dialogData: {
      cluster: string;
      namespace: string;
      data?: Service;
      podControllers?: { name: string; kind: string }[];
    },
    private k8sResourceService: K8sResourceService,
    private translateService: TranslateService,
  ) {
    if (dialogData) {
      if (dialogData.data) {
        this.service = dialogData.data;
        this.titlePrefix = this.translateService.get('update');
      } else {
        this.service = getDefaultService(dialogData.namespace);
        this.titlePrefix = this.translateService.get('create');
      }
      if (dialogData.podControllers && dialogData.podControllers.length) {
        this.podControllers = dialogData.podControllers;
      }
    }

    this.getAlbNameOptions(dialogData.cluster);
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.close.next(this.service);
  }

  cancel() {
    this.close.next();
  }

  private getAlbNameOptions(cluster: string) {
    this.k8sResourceService
      .getK8sResources<LoadBalancer>('alaudaloadbalancer2', {
        clusterName: cluster,
      })
      .then(res => {
        this.relatedAlbs = res.map(item => item.kubernetes);
      });
  }
}
