import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { from, Observable, of, Subject } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { AppShardService } from 'app/features-shared/app/app-shard.service';
import { PodControllerKinds } from 'app/features-shared/app/utils/pod-controller';
import { AlarmService } from 'app/services/api/alarm.service';
import { AppService } from 'app/services/api/app.service';
import { AppUtilitiesService } from 'app/services/api/app.utilities.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import * as fromStore from 'app/store';
import { Environments, WorkspaceBaseParams } from 'app/typings';
import { K8sResourceWithActions } from 'app/typings/backend-api';
import { RcImageSelection } from 'app/typings/k8s-form-model';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { AppUtilService } from '../app-util.service';

@Component({
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ResourceListPermissionsService],
})
export class ApplicationListPageComponent
  extends BaseResourceListComponent<K8sResourceWithActions[]>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  project: string;
  cluster: string;
  namespace: string;
  createEnabled: boolean;
  initialized: boolean;
  baseParams: WorkspaceBaseParams;
  columns = ['name', 'status', 'component', 'action'];
  mirrorClusters: {
    name: string;
    display_name: string;
  }[]; // avaliable mirror clusters

  constructor(
    private appUtilService: AppUtilService,
    private workspaceComponent: WorkspaceComponent,
    private roleUtil: RoleUtilitiesService,
    private appService: AppService,
    private alarmService: AlarmService,
    private appUtilities: AppUtilitiesService,
    private appShardService: AppShardService,
    private store: Store<fromStore.AppState>,
    http: HttpClient,
    public router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    @Inject(ENVIRONMENTS) public env: Environments,
    public resourcePermissionService: ResourceListPermissionsService,
  ) {
    super(http, router, activedRoute, cdr);
  }

  ngOnInit() {
    super.ngOnInit();
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.resourcePermissionService.initContext({
      resourceNameParamKey: 'application_name',
      context: {
        project_name: this.baseParams.project,
        cluster_name: this.baseParams.cluster,
        namespace_name: this.baseParams.namespace,
      },
    });

    this.project = this.baseParams.project;
    this.cluster = this.baseParams.cluster;
    this.namespace = this.baseParams.namespace;
    this.roleUtil
      .resourceTypeSupportPermissions(
        'application',
        {
          project_name: this.project,
          namespace_name: this.namespace,
          cluster_name: this.cluster,
        },
        'create',
      )
      .then((res: boolean) => {
        this.createEnabled = res;
      });

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe(() => {
        this.initialized = true;
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    if (!this.namespace) {
      return of({
        count: 0,
        results: [],
      });
    }
    return from(
      this.appService.getAppListCRD({
        cluster_name: this.cluster,
        namespace: this.namespace,
        params: {
          name: params.search,
          page: params.pageParams.pageIndex,
          page_size: params.pageParams.pageSize,
          search: params.search,
        },
      }),
    );
  }

  getName(resources: K8sResourceWithActions[]) {
    return this.getApplication(resources).kubernetes.metadata.name;
  }

  get labelBaseDomain(): string {
    return this.env.label_base_domain;
  }

  getDisplayName(resources: K8sResourceWithActions[]) {
    return this.getApplication(resources).kubernetes.metadata.annotations[
      `app.${this.labelBaseDomain}/display-name`
    ];
  }

  getStatus(resources: K8sResourceWithActions[]) {
    return this.getApplication(resources).kubernetes.spec.assemblyPhase;
  }

  getComponentsCount(resources: K8sResourceWithActions[]) {
    return resources.filter(item =>
      PodControllerKinds.includes(item.kubernetes.kind),
    ).length;
  }

  showUpdate(resources: K8sResourceWithActions[]) {
    return this.appUtilities.showUpdate(this.getApplication(resources));
  }

  showDelete(resources: K8sResourceWithActions[]) {
    return this.appUtilities.showDelete(this.getApplication(resources));
  }

  create() {
    this.appShardService
      .selectImage({ projectName: this.project })
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.router.navigate(['app', 'create'], {
            relativeTo: this.workspaceComponent.baseActivatedRoute,
            queryParams: res,
          });
        }
      });
  }

  createFromYaml() {
    this.router.navigate(['app', 'yaml'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  createFromTemplate() {
    this.router.navigate(['app_catalog'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  view(resources: K8sResourceWithActions[]) {
    const name = this.getApplication(resources).kubernetes.metadata.name;
    this.router.navigate(['app', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  update(resources: K8sResourceWithActions[]) {
    const name = this.getApplication(resources).kubernetes.metadata.name;
    this.router.navigate(['app', 'update', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async deleteApp(resources: K8sResourceWithActions[]) {
    this.mirrorClusters = [];
    const name = this.getApplication(resources).kubernetes.metadata.name;
    await this.getMirrorClusters(name);
    try {
      await this.appUtilService.deleteApplication({
        cluster: this.cluster,
        namespace: this.namespace,
        app: name,
        mirrorClusters: this.mirrorClusters,
        project: this.project,
      });
      this.alarmService
        .deleteAlertsByLabelSelector(
          this.cluster,
          this.namespace,
          'application',
          name,
        )
        .catch(() => {});
      this.onUpdate(null);
    } catch (_err) {}
  }

  private async getMirrorClusters(name: string) {
    const cluster = await this.store
      .select(fromStore.getCurrentCluster)
      .pipe(
        filter(cluster => !!cluster),
        first(),
      )
      .toPromise();
    if (cluster.mirror && cluster.mirror.regions) {
      this.mirrorClusters = cluster.mirror.regions.map(
        ({ name, display_name }) => ({ name, display_name }),
      );
    } else {
      this.mirrorClusters = [];
    }
    if (this.mirrorClusters.length > 1) {
      const clusterAppMap = await this.appService.batchGetAppDetailCRD({
        project: this.project,
        clusters: this.mirrorClusters.map(c => c.name),
        namespace: this.namespace,
        name: name,
      });
      const appClusters = Object.keys(clusterAppMap);
      this.mirrorClusters = this.mirrorClusters.filter(
        c => appClusters.indexOf(c.name) >= 0,
      );
    }
    this.cdr.markForCheck();
  }

  trackByFn(_index: number, resources: K8sResourceWithActions[]) {
    const resource = resources.find(
      item => item.kubernetes.kind === 'Application',
    );
    return resource ? resource.kubernetes.metadata.name : _index;
  }

  getApplication(resources: K8sResourceWithActions[]) {
    return resources.find(
      item => item.kubernetes.kind === 'Application',
    ) as K8sResourceWithActions;
  }

  getApplicationKubernetes(resources: K8sResourceWithActions[]) {
    return this.getApplication(resources).kubernetes;
  }

  pageChanged(param: string, value: number) {
    this.onPageEvent({ [param]: value });
  }
}
