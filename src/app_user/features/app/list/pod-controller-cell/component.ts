import { Component, Input, OnInit } from '@angular/core';

import { get } from 'lodash-es';

import { PodControllerKinds } from 'app/features-shared/app/utils/pod-controller';
import { KubernetesResource, WorkspaceBaseParams } from 'app/typings';

interface ResourceItem {
  kubernetes: KubernetesResource;
  resource_actions: string[];
}

@Component({
  selector: 'rc-pod-controller-cell',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class PodControllerCellComponent implements OnInit {
  @Input()
  resources: ResourceItem[];
  @Input()
  baseParams: WorkspaceBaseParams;
  constructor() {}

  ngOnInit() {}

  get resourcesItems() {
    const res =
      this.resources && this.resources.length
        ? this.resources
            .map((item: ResourceItem) => item.kubernetes)
            .filter((item: KubernetesResource) => {
              return PodControllerKinds.includes(item.kind);
            })
        : [];
    return res;
  }

  get appName(): string {
    const app =
      this.resources && this.resources.length
        ? this.resources
            .map((item: ResourceItem) => item.kubernetes)
            .find((item: KubernetesResource) => {
              return item.kind === 'Application';
            })
        : null;
    return get(app, 'metadata.name');
  }

  getResourceName(item: KubernetesResource) {
    return get(item, 'metadata.name', '-');
  }

  getResourceKind(item: KubernetesResource) {
    return item.kind.toLowerCase();
  }

  trackByFn(_index: number, resource: KubernetesResource) {
    return resource.metadata.name;
  }
}
