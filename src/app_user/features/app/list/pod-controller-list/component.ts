import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';

import { get } from 'lodash-es';
import { Subject, timer } from 'rxjs';
import { startWith, takeUntil } from 'rxjs/operators';

import { PodControllerKinds } from 'app/features-shared/app/utils/pod-controller';
import {
  ApplicationStatus,
  AppService,
  WorkloadStatus,
} from 'app/services/api/app.service';
import { KubernetesResource } from 'app/typings';
import { K8sResourceWithActions } from 'app/typings/backend-api';

@Component({
  selector: 'rc-pod-controller-list',
  templateUrl: 'template.html',
  styleUrls: ['../pod-controller-cell/styles.scss', 'styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerListComponent implements OnInit, OnDestroy {
  private onDestory$: Subject<void> = new Subject<void>();
  private status: ApplicationStatus;
  initialized: boolean;
  @Input()
  resources: K8sResourceWithActions[];
  @Input()
  baseParams: {
    project: string;
    cluster: string;
    namespace: string;
  };
  constructor(private appService: AppService, private cdr: ChangeDetectorRef) {}

  ngOnInit() {
    timer(5000)
      .pipe(
        startWith(0),
        takeUntil(this.onDestory$),
      )
      .subscribe(() => {
        this.fetchApplicationStatus();
      });
  }

  ngOnDestroy() {
    this.onDestory$.next();
  }

  private fetchApplicationStatus() {
    if (!this.resources) {
      return;
    }
    const name = this.appName;
    if (!name) {
      return;
    }
    this.appService
      .getApplicationStatus({
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        app: name,
      })
      .then((res: ApplicationStatus) => {
        if (res) {
          this.status = res;
        }
        this.initialized = true;
        this.cdr.markForCheck();
      });
  }

  get resourcesItems() {
    const res =
      this.resources && this.resources.length
        ? this.resources
            .map((item: K8sResourceWithActions) => item.kubernetes)
            .filter((item: KubernetesResource) => {
              return PodControllerKinds.includes(item.kind);
            })
        : [];
    return res;
  }

  get appName(): string {
    const app =
      this.resources && this.resources.length
        ? this.resources
            .map((item: K8sResourceWithActions) => item.kubernetes)
            .find((item: KubernetesResource) => {
              return item.kind === 'Application';
            })
        : null;
    return get(app, 'metadata.name');
  }

  getComponentStatus(item: KubernetesResource) {
    if (!this.status) {
      return 'Unknown';
    }

    const workload: WorkloadStatus = this.getStatusWorkload(item);
    return workload.status || 'Unknown';
  }

  getComponentPods(item: KubernetesResource, type: 'desired' | 'current') {
    if (!this.status) {
      return 0;
    }
    const workload: WorkloadStatus = this.getStatusWorkload(item);
    return workload[type] || 0;
  }

  getComponentName(item: KubernetesResource) {
    return get(item, 'metadata.name', '-');
  }

  getComponentKind(item: KubernetesResource) {
    return item.kind.toLowerCase();
  }

  trackByFn(_index: number, resource: KubernetesResource) {
    return resource.metadata.name;
  }

  private getStatusWorkload(item: KubernetesResource): WorkloadStatus {
    if (!this.status) {
      return {};
    }
    return get(
      this.status,
      `workloads.${item.kind.toLowerCase()}-${item.metadata.name}`,
    ) as WorkloadStatus;
  }
}
