import { NgModule } from '@angular/core';

import { AlarmSharedModule } from 'app/features-shared/alarm/shared.module';
import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { ExecSharedModule } from 'app/features-shared/exec/shared.module';
import { ImageSharedModule } from 'app/features-shared/image/shared.module';
import { LogSharedModule } from 'app/features-shared/log/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { AppAddressComponent } from './app-address/component';
import { AppDataService } from './app-data.service';
import { AppStatusComponent } from './app-status/component';
import { AppUtilService } from './app-util.service';
import { ApplicationCreateAddComponentPageComponent } from './create/add-component/component';
import { ApplicationCreateComponent } from './create/component';
import { ApplicationCreatePreviewPageComponent } from './create/preview/component';
import { ApplicationCreatePrimaryPageComponent } from './create/primary/component';
import { ApplicationCreateUpdateComponentPageComponent } from './create/update-component/component';
import { AppYamlPageComponent } from './create/yaml-page/component';
import { ApplicationDetailAlarmComponent } from './detail/alarm/component';
import { ApplicationDetailEventComponent } from './detail/event/component';
import { ApplicationDetailHeaderComponent } from './detail/header/component';
import { ApplicationHistoryDetailComponent } from './detail/history-detail/component';
import { ApplicationHistoryComponent } from './detail/history/component';
import { ApplicationDetailComponent } from './detail/main/component';
import { ApplicationDetailMetaComponent } from './detail/meta/component';
import { ApplicationMonitorChartComponent } from './detail/monitor/chart/component';
import { ApplicationMonitorComponent } from './detail/monitor/component';
import { ApplicationDetailPageComponent } from './detail/page.component';
import { ApplicationDetailResourceComponent } from './detail/resource/component';
import { AddResourceComponent } from './dialog/add-resource/component';
import { UpdateLabelDialogComponent } from './dialog/update-label/component';
import { ComponentFormComponent } from './form/component-form/component';
import { ApplicationMetadataFormComponent } from './form/metadata-form/component';
import { ApplicationCreatePreviewFormComponent } from './form/preview-form/component';
import { ServicesFormComponent } from './form/services-form/component';
import { ServiceFormDialogComponent } from './form/services-form/dialog';
import { ApplicationListPageComponent } from './list/component';
import { PodControllerCellComponent } from './list/pod-controller-cell/component';
import { PodControllerListComponent } from './list/pod-controller-list/component';
import { PodControllerStatusComponent } from './pod-controller-status/component';
import { TemplateApplyComponent } from './pod-controller/alarm/apply/template-apply.component';
import { PodControllerAlarmComponent } from './pod-controller/alarm/component';
import { AlarmDetailComponent } from './pod-controller/alarm/detail/component';
import { AlarmFormComponent } from './pod-controller/alarm/form/component';
import { PodControllerDetailHeaderComponent } from './pod-controller/header/component';
import { PodControllerDetailMainComponent } from './pod-controller/main/component';
import { PodControllerDetailMetaComponent } from './pod-controller/meta/component';
import { PodControllerMonitorChartComponent } from './pod-controller/monitor/chart/component';
import { PodControllerMonitorComponent } from './pod-controller/monitor/component';
import { PodControllerDetailPageComponent } from './pod-controller/page.component';
import { ApplicationPodGroupComponent } from './pod-group/component';
import { PodStatusComponent } from './pod-status/component';
import { K8sResourceIconComponent } from './resource-icon/component';
import { ApplicationRoutingModule } from './routing.module';
import { ServiceDisplayComponent } from './service-display/component';
import { AppTopologyComponent } from './topology/component';
import { D3Service } from './topology/d3.service';
import { ArcLinkVisualComponent } from './topology/visuals/arc-link/component';
import { DraggableDirective } from './topology/visuals/draggable.directive';
import { GraphComponent } from './topology/visuals/graph/component';
import { GraphLegendComponent } from './topology/visuals/legend/component';
import { NodeVisualComponent } from './topology/visuals/node/component';
import { WorkloadStatusComponent } from './topology/visuals/workload-status/component';
import { ZoomableDirective } from './topology/visuals/zoomable.directive';
import { ApplicationUpdateComponent } from './update/component';
import { ApplicationUpdatePreviewPageComponent } from './update/preview/component';

const TOPOLOGY_COMPONENTS = [
  NodeVisualComponent,
  ArcLinkVisualComponent,
  GraphComponent,
  DraggableDirective,
  WorkloadStatusComponent,
  GraphLegendComponent,
  ZoomableDirective,
  AppTopologyComponent,
];

const FORM_COMPONENTS = [
  ApplicationCreatePreviewFormComponent,
  ComponentFormComponent,
  ApplicationMetadataFormComponent,
  ServicesFormComponent,
];

const DETAIL_COMPONENTS = [
  ApplicationDetailComponent,
  ApplicationDetailAlarmComponent,
  PodControllerAlarmComponent,
  PodControllerDetailMainComponent,
  PodControllerDetailHeaderComponent,
  PodControllerDetailMetaComponent,
  PodControllerMonitorComponent,
  PodControllerDetailPageComponent,
  ApplicationDetailEventComponent,
  ApplicationDetailHeaderComponent,
  ApplicationDetailMetaComponent,
  ApplicationMonitorComponent,
  ApplicationDetailPageComponent,
  ApplicationDetailResourceComponent,
  ApplicationMonitorChartComponent,
  ApplicationPodGroupComponent,
  ServiceDisplayComponent,
  PodControllerMonitorChartComponent,
  K8sResourceIconComponent,
  PodStatusComponent,
  UpdateLabelDialogComponent,
  AlarmFormComponent,
  AlarmDetailComponent,
  ApplicationHistoryComponent,
  ApplicationHistoryDetailComponent,
];

@NgModule({
  imports: [
    SharedModule,
    AlarmSharedModule,
    EventSharedModule,
    LogSharedModule,
    AppSharedModule,
    FormTableModule,
    ApplicationRoutingModule,
    ImageSharedModule,
    ExecSharedModule,
  ],
  declarations: [
    ApplicationListPageComponent,
    ApplicationCreateComponent,
    ApplicationUpdateComponent,
    ApplicationUpdatePreviewPageComponent,
    ApplicationCreatePrimaryPageComponent,
    ApplicationCreateAddComponentPageComponent,
    ApplicationCreateUpdateComponentPageComponent,
    ApplicationCreatePreviewPageComponent,
    AddResourceComponent,
    AppYamlPageComponent,
    AppStatusComponent,
    PodControllerCellComponent,
    PodControllerListComponent,
    ServiceFormDialogComponent,
    AppAddressComponent,
    PodControllerStatusComponent,
    TemplateApplyComponent,
    ...FORM_COMPONENTS,
    ...DETAIL_COMPONENTS,
    ...TOPOLOGY_COMPONENTS,
  ],
  providers: [D3Service, AppDataService, AppUtilService],
  entryComponents: [
    ServiceFormDialogComponent,
    TemplateApplyComponent,
    UpdateLabelDialogComponent,
    AddResourceComponent,
  ],
})
export class ApplicationModule {}
