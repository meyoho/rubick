import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
} from '@angular/core';
import { SimpleChanges } from '@angular/core';

import { TranslateService } from 'app/translate/translate.service';
import { getUnifiedStatus } from 'app2/shared/components/status-badge/status-mapper';

@Component({
  selector: 'rc-pod-controller-status',
  templateUrl: './template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerStatusComponent implements OnChanges {
  @Input()
  currentInstances = 0;

  @Input()
  desiredInstances = 0;

  @Input()
  status = 'Unknown'; // Running/Warning/Error/Stopped/Deploying/Starting/Updating/Stopping/Deleting

  statusText: string;
  statusAttr: string;
  instanceStatusText: string;
  tagType: string;

  private statusTypeMap = {
    Running: 'success',
    Warning: 'warning',
    Error: 'error',
    Stopped: 'info',
    Deploying: 'primary',
    Starting: 'primary',
    Updating: 'primary',
    Stopping: 'warning',
    Deleting: 'warning',
    Unknown: 'info',
    Pending: 'primary',
  };

  constructor(private translateService: TranslateService) {}

  ngOnChanges({ status }: SimpleChanges) {
    if (status && status.currentValue) {
      this.refresh();
    }
  }

  private refresh() {
    const status = this.status;
    const current = this.currentInstances || 0;
    const desired = this.desiredInstances || 0;
    this.statusText = this.translateService.get(
      `status_${status.toLowerCase()}`,
    );
    this.statusAttr = getUnifiedStatus(this.status);
    this.tagType = this.statusTypeMap[status] || 'info';
    this.instanceStatusText = `${current}/${desired}`;
  }
}
