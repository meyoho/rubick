import { DIALOG_DATA, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { AlarmService, AlarmTemplate } from 'app/services/api/alarm.service';
import { IndicatorType } from 'app/services/api/metric.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import {
  getRule,
  getRulesAction,
  parseAlertList,
} from 'app2/features/alarm/alarm.util';

@Component({
  selector: 'rc-template-apply',
  templateUrl: 'template-apply.component.html',
  styleUrls: ['template-apply.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TemplateApplyComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;
  @Output()
  close: EventEmitter<boolean> = new EventEmitter();

  loading = true;
  initialized = false;
  submitting = false;
  workload = 'workload';

  templates: AlarmTemplate[];
  template: AlarmTemplate;
  parseAlertList = parseAlertList;

  getRule = getRule;
  getRulesAction = getRulesAction;

  constructor(
    @Inject(DIALOG_DATA)
    private modalData: {
      metric_type: IndicatorType[];
      componentName: string;
      clusterName: string;
      namespaceName: string;
      appName: string;
      kind: string;
    },
    private cdr: ChangeDetectorRef,
    private alarmService: AlarmService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    @Inject(ACCOUNT) public account: Account,
  ) {}

  async ngOnInit() {
    try {
      const alarmTemplates = await this.alarmService.getAlertTemplates({
        page: 1,
        page_size: 999,
      });
      this.templates = alarmTemplates.results.filter(el => {
        return el.kind === 'workload';
      });
    } finally {
      this.cdr.markForCheck();
      this.loading = false;
      this.initialized = true;
    }
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    try {
      this.submitting = true;
      await this.alarmService.applyAlertTemplate(this.template.name, {
        cluster: {
          name: this.modalData.clusterName,
        },
        metric: {
          queries: [
            {
              labels: [
                {
                  name: 'name',
                  value: this.modalData.componentName,
                },
                {
                  name: 'kind',
                  value: this.modalData.kind,
                },
                {
                  name: 'namespace',
                  value: this.modalData.namespaceName,
                },
                {
                  name: 'application',
                  value: this.modalData.appName,
                },
              ],
            },
          ],
        },
      });
      this.close.next(true);
      this.auiNotificationService.success(
        this.translate.get('alarm_template_apply_success'),
      );
    } catch (rejection) {
      this.submitting = false;
      this.errorsToastService.error(rejection);
      this.cdr.markForCheck();
    }
  }

  cancel() {
    this.close.next(false);
  }
}
