import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { get } from 'lodash-es';

import { AlarmUpdateParam } from 'app/services/api/alarm.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environment, WorkspaceBaseParams } from 'app/typings';
import { PodController } from 'app/typings/backend-api';

@Component({
  selector: 'rc-pod-controller-alarm',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerAlarmComponent implements OnChanges {
  @Input()
  podController: PodController;
  @Input()
  appName: string;
  @Input()
  baseParams: WorkspaceBaseParams;

  initialized = false;
  empty = false;
  status = 'list';
  alarmUpdateParam: AlarmUpdateParam;

  listParam: {
    kind: string;
    name: string;
  };

  get labelBaseDomain(): string {
    return this.env['label_base_domain'];
  }

  ngOnChanges({ podController }: SimpleChanges) {
    if (podController && podController.currentValue) {
      this.onInputChange();
      this.initialized = true;
    }
  }

  private onInputChange() {
    this.listParam = {
      kind: get(this.podController, 'kind').toLowerCase(),
      name: get(this.podController, 'metadata.name'),
    };
  }

  onAlarmRoute(alarmUpdateParam: AlarmUpdateParam) {
    this.alarmUpdateParam = alarmUpdateParam;
    this.status = alarmUpdateParam.path;
  }

  constructor(@Inject(ENVIRONMENTS) public env: Environment) {}
}
