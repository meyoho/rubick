import { NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';
import { FormBuilder } from '@angular/forms';

import * as Highcharts from 'highcharts';
import { get, keyBy, mapValues } from 'lodash-es';
import { debounceTime, filter, takeUntil } from 'rxjs/operators';

import { BaseAlarmFormComponent } from 'app/features-shared/alarm/base-alarm-form.component';
import {
  AlarmMetric,
  AlarmService,
  AlarmUpdateParam,
} from 'app/services/api/alarm.service';
import { MetricService } from 'app/services/api/metric.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { PodController } from 'app/typings/backend-api';
import { parseAlertList } from 'app2/features/alarm/alarm.util';
import { BatchReponse } from 'app_user/core/types';
require('highcharts/modules/no-data-to-display')(Highcharts);

@Component({
  selector: 'rc-alarm-form',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class AlarmFormComponent extends BaseAlarmFormComponent
  implements OnInit, OnDestroy {
  @Input()
  podController?: PodController;
  @Input()
  projectName: string;
  @Input()
  alarmUpdateParam: AlarmUpdateParam;
  @Input()
  appName: string;
  @Output()
  alarmRoute = new EventEmitter<AlarmUpdateParam>();

  metricName: string;
  componentName: string;
  componentType: string;

  constructor(
    @Inject(ACCOUNT) public account: Account,
    cdr: ChangeDetectorRef,
    fb: FormBuilder,
    metricService: MetricService,
    translate: TranslateService,
    auiNotificationService: NotificationService,
    alarmService: AlarmService,
    private errorsToastService: ErrorsToastService,
    private httpService: HttpService,
  ) {
    super(
      cdr,
      fb,
      metricService,
      translate,
      auiNotificationService,
      alarmService,
    );
  }

  async ngOnInit() {
    super.ngOnInit();
    this.form
      .get('alarm_metric')
      .valueChanges.pipe(
        takeUntil(this.onDestroy$),
        debounceTime(500),
        filter(value => !!value),
      )
      .subscribe((alarm_metric: AlarmMetric) => {
        if (alarm_metric.unit) {
          this.unit_name = this.translate.get(alarm_metric.unit);
          this.chartNumericOptions.tooltip.valueSuffix = this.unit_name;
        } else {
          const unit = this.getUnitType(alarm_metric.metric_name);
          if (!unit) {
            this.unit_name = '';
            this.chartNumericOptions.tooltip.valueDecimals = 2;
            this.chartNumericOptions.tooltip.valueSuffix = '';
          } else {
            this.unit_name = this.translate.get(unit);
            this.chartNumericOptions.tooltip.valueDecimals = 4;
            this.chartNumericOptions.tooltip.valueSuffix = this.unit_name;
          }
        }
        this.metricName = alarm_metric.metric_name;
        if (this.checkAlarmMetric(alarm_metric)) {
          this.cdr.markForCheck();
          return;
        }
        this.payloadMetricLabels = [
          {
            type: 'EQUAL',
            name: 'namespace',
            value: this.alarmUpdateParam.namespaceName,
          },
          {
            type: 'EQUAL',
            name: 'kind',
            value: this.componentType,
          },
          {
            type: 'EQUAL',
            name: 'name',
            value: this.componentName,
          },
          {
            type: 'EQUAL',
            name: 'application',
            value: this.appName,
          },
        ];
        if (alarm_metric.metric.queries[0].labels.length < 5) {
          alarm_metric.metric.queries[0].labels = alarm_metric.metric.queries[0].labels.concat(
            this.payloadMetricLabels,
          );
        }
        super.loadChart(
          this.componentName,
          this.alarmUpdateParam.clusterName,
          alarm_metric,
        );
      });
    this.cluster_name = this.alarmUpdateParam.clusterName;
    this.name = this.alarmUpdateParam.name;
    if (this.alarmUpdateParam.resource_name) {
      this.resource_name = this.alarmUpdateParam.resource_name;
      this.componentType = this.resource_name.split('--')[1];
      this.componentName = this.resource_name.split('--')[2];
    }
    if (!this.name && this.podController) {
      this.componentName = get(this.podController, 'metadata.name');
      this.componentType = get(this.podController, 'kind').toLowerCase();
      this.resource_name = `${this.alarmUpdateParam.namespaceName}--${this.componentType}--${this.componentName}`;
    }
    this.metric_type = await this.metricService.getIndicators(
      this.cluster_name,
    );
    // workload下创建和更新告警时，workload的name和kind通过Input podController拿到
    if (this.podController) {
      this.componentName = get(this.podController, 'metadata.name');
      this.componentType = get(this.podController, 'kind').toLowerCase();
    }
    if (this.name) {
      this.formSubmitText = 'update';
      const alarm = await this.alarmService.getAlert(
        this.cluster_name,
        this.alarmUpdateParam.namespaceName,
        this.resource_name,
        null,
        this.name,
      );
      this.data = parseAlertList(
        [alarm],
        '',
        this.metric_type,
        this.cluster_name,
      )[0];
      // 应用下没有创建告警功能。应用下更新告警时，workload的name和kind通过metric.queries[0]解析出来
      this.componentName = this.data.display_name;
      this.componentType = this.data.kind;
    }
    super.initViewModel();
    this.initialized = true;
    this.cdr.markForCheck();
  }

  async onSubmit() {
    this.alarmForm.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    const body = super.generatePayload();
    body['project'] = this.projectName;
    if (
      this.actions.filter(el => {
        return el.action_type === 'scale_up';
      }).length > 0
    ) {
      body['scale_up'] = [
        {
          name: this.componentName,
          namespace: this.alarmUpdateParam.namespaceName,
        },
      ];
    }
    if (
      this.actions.filter(el => {
        return el.action_type === 'scale_down';
      }).length > 0
    ) {
      body['scale_down'] = [
        {
          name: this.componentName,
          namespace: this.alarmUpdateParam.namespaceName,
        },
      ];
    }
    if (body.metric_name === 'workload.log.keyword.count') {
      delete body.unit;
    }
    if (this.data) {
      delete body.name;
      try {
        this.submitting = true;
        if (
          this.alarmUpdateParam.clusters &&
          this.alarmUpdateParam.clusters.length > 1
        ) {
          const res: BatchReponse = await this.operateK8sAppAlarmBatch(
            body,
            this.alarmUpdateParam.clusters,
            false,
          );
          this.submitting = false;
          Object.entries(res.responses).forEach(([key, data]) => {
            if (data.code >= 300) {
              const body = JSON.parse(data.body);
              const error = this.errorsToastService.parseErrors({
                errors: body.errors,
              });
              const title = this.translate.get('cluster_alarm_update_error', {
                alarmName: this.data.name,
                clusterName: key,
              });
              this.auiNotificationService.error({
                content: `${title}:<br>${error.content}`,
              });
            } else {
              this.cancel();
              this.auiNotificationService.success({
                content: this.translate.get('cluster_alarm_update_success', {
                  alarmName: this.data.name,
                  clusterName: key,
                }),
              });
            }
          });
        } else {
          await this.alarmService.updateAlert(
            this.cluster_name,
            this.alarmUpdateParam.namespaceName,
            this.data.resource_name,
            this.data.group_name,
            this.data.name,
            body,
          );
          this.cancel();
          this.auiNotificationService.success(
            this.translate.get('alarm_update_success'),
          );
        }
      } catch (rejection) {
        this.submitting = false;
        this.errorsToastService.error(rejection);
        this.cdr.markForCheck();
      }
    } else {
      try {
        this.submitting = true;
        if (
          this.alarmUpdateParam.clusters &&
          this.alarmUpdateParam.clusters.length > 1
        ) {
          const res: BatchReponse = await this.operateK8sAppAlarmBatch(
            body,
            this.alarmUpdateParam.clusters,
            true,
          );
          this.submitting = true;
          Object.entries(res.responses).forEach(([key, data]) => {
            if (data.code >= 300) {
              const body = JSON.parse(data.body);
              const error = this.errorsToastService.parseErrors({
                errors: body.errors,
              });
              const title = this.translate.get('cluster_alarm_create_error', {
                alarmName: this.form.get('name').value,
                clusterName: key,
              });
              this.auiNotificationService.error({
                content: `${title}:<br>${error.content}`,
              });
            } else {
              this.cancel();
              this.auiNotificationService.success({
                content: this.translate.get('cluster_alarm_create_success', {
                  alarmName: this.form.get('name').value,
                  clusterName: key,
                }),
              });
            }
          });
        } else {
          const res = await this.alarmService.createAlert(
            this.cluster_name,
            body,
          );
          this.alarmRoute.emit({
            ...this.alarmUpdateParam,
            name: res['name'],
            path: 'detail',
            path_from: 'form',
            resource_name: res['resource_name'],
          });
          this.auiNotificationService.success(
            this.translate.get('alarm_create_success'),
          );
        }
      } catch (rejection) {
        this.submitting = false;
        this.errorsToastService.error(rejection);
        this.cdr.markForCheck();
      }
    }
  }

  cancel() {
    this.alarmRoute.emit({
      ...this.alarmUpdateParam,
      path: this.alarmUpdateParam.path_from,
      path_from: 'form',
    });
  }

  operateK8sAppAlarmBatch(
    body: any,
    clusters: string[],
    isCreate: boolean,
  ): Promise<BatchReponse> {
    const requests = clusters.map((cluster: string) => {
      let url = '';
      if (isCreate) {
        url = `/v1/alerts/${cluster}?project_name=${this.projectName}`;
      } else {
        url = `/v1/alerts/${cluster}/${this.alarmUpdateParam.namespaceName}/${this.data.resource_name}/${this.data.group_name}/${this.data.name}`;
      }
      return {
        cluster,
        url,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: isCreate ? 'POST' : 'PUT',
          header: {
            'Content-Type': 'application/json',
          },
          body: JSON.stringify(body),
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }
}
