import {
  DialogService,
  DialogSize,
  NotificationService,
  PageEvent,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';

import { debounce, keyBy, mapValues } from 'lodash-es';
import {
  BehaviorSubject,
  combineLatest,
  from,
  Observable,
  of,
  Subject,
} from 'rxjs';
import {
  catchError,
  debounceTime,
  filter,
  map,
  publishReplay,
  refCount,
  scan,
  startWith,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import {
  Alarm,
  AlarmService,
  AlarmUpdateParam,
} from 'app/services/api/alarm.service';
import { IndicatorType, MetricService } from 'app/services/api/metric.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { HttpService } from 'app/services/http.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { getRule, parseAlertList } from 'app2/features/alarm/alarm.util';
import { delay } from 'app2/utils';
import { BatchReponse } from 'app_user/core/types';
import { ResourceList } from 'app_user/core/types';
import { TemplateApplyComponent } from '../apply/template-apply.component';

const ORDERED_METRICS = [
  'cpu_usage',
  'memory_usage',
  'sent_bytes',
  'received_bytes',
];

@Component({
  selector: 'rc-component-alarm',
  styleUrls: ['styles.scss'],
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ComponentAlarmComponent implements OnInit, OnDestroy {
  @Input()
  clusterName: string;
  @Input()
  namespaceName: string;
  @Input()
  appName: string;
  @Input()
  listParam = {
    kind: '',
    name: '',
  };
  @Output()
  alarmRoute = new EventEmitter<AlarmUpdateParam>();

  canCreateAlarm: boolean;
  canUpdateAlarm: boolean;
  canDeleteAlarm: boolean;
  uuid: string;
  clusters: string[];
  mirrorClusters: Array<{
    name: string;
    display_name: string;
  }> = [];
  metric_type: IndicatorType[];

  private onDestroy$ = new Subject<void>();
  search$ = new BehaviorSubject<string>(null);
  paginationEvent$ = new BehaviorSubject<Partial<PageEvent>>({});
  pagination$: Observable<PageEvent>;
  updated$ = new BehaviorSubject(null);
  list$: Observable<ResourceList>;
  filteredList$: Observable<Alarm[]>;
  columns = [
    'name',
    'workload',
    'alarm_type',
    'threshold',
    'level',
    'status',
    'action',
  ];

  initialized: boolean;
  fetching = true;
  searching = false;
  showZeroState = true;

  protected poll$ = new BehaviorSubject(null);
  pollInterval = 60000;
  protected debouncedUpdate: () => void;

  getRule = getRule;

  constructor(
    @Inject(ACCOUNT) public account: Account,
    public cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private translate: TranslateService,
    private httpService: HttpService,
    private roleUtil: RoleUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private alarmService: AlarmService,
    private metricService: MetricService,
    private regionService: RegionService,
  ) {
    this.debouncedUpdate = debounce(() => {
      this.poll$.next(null);
    }, this.pollInterval);
  }

  onUpdate() {
    this.updated$.next(null);
  }

  async ngOnInit() {
    [
      this.canCreateAlarm,
      this.canUpdateAlarm,
      this.canDeleteAlarm,
    ] = await this.roleUtil.resourceTypeSupportPermissions('alarm', {}, [
      'create',
      'update',
      'delete',
    ]);
    this.getMirrorClusters();
    this.metric_type = await this.metricService.getIndicators(this.clusterName);
    this.cdr.markForCheck();

    this.pagination$ = this.paginationEvent$.pipe(
      scan((prev, changes) => ({ ...prev, ...changes }), {
        pageSize: 10,
        pageIndex: 0,
        previousPageIndex: 0,
        length: 0,
      }),
      publishReplay(1),
      refCount(),
    );
    this.list$ = combineLatest(
      this.pagination$,
      this.search$.pipe(startWith('')),
      this.poll$,
      this.updated$,
    ).pipe(
      debounceTime(100),
      tap(() => {
        this.fetching = true;
        this.cdr.markForCheck();
      }),
      switchMap(([pagination, search]) =>
        this.fetchResources(pagination, search).pipe(
          catchError(error => of(error)),
        ),
      ),
      tap(result => {
        this.fetching = false;
        this.searching = false;
        if (!result.results) {
          this.showZeroState = true;
        } else {
          this.showZeroState = result.results.length === 0;
          if (result.results.length === 0) {
            if (result.num_pages) {
              this.pageChanged({ pageIndex: result.num_pages - 1 });
            }
          }
        }
        this.cdr.markForCheck();
        this.debouncedUpdate();
      }),
      publishReplay(1),
      refCount(),
    );

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe(list => {
        if (list.results.length) {
          this.initialized = true;
        }
      });
    this.filteredList$ = this.list$
      .pipe(
        map(list => {
          return parseAlertList(list.results, null, this.metric_type).sort(
            (a, b) =>
              ORDERED_METRICS.indexOf(a.metric_name) -
              ORDERED_METRICS.indexOf(b.metric_name),
          );
        }),
      )
      .pipe(catchError(() => []));
    this.poll$.next(null);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  onSearchChanged(search: string) {
    this.pageChanged({ pageIndex: 0 });
    this.search$.next(search);
  }

  pageChanged(pageEvent: Partial<PageEvent>) {
    this.paginationEvent$.next(pageEvent);
  }

  fetchResources(pagination: PageEvent, name: string): Observable<Alarm> {
    const { pageIndex, pageSize } = pagination;
    return from(
      this.alarmService.getAlertsByLabelSelector(
        this.clusterName,
        this.listParam.kind,
        this.listParam.name,
        this.namespaceName,
        {
          page: pageIndex + 1,
          page_size: pageSize,
          name,
        },
      ),
    );
  }

  trackByFn(_index: number, item: Alarm) {
    return item.name;
  }

  getRouteParams() {
    return {
      path_from: 'list',
      clusters: this.clusters,
      clusterName: this.clusterName,
      accountNamespace: this.account.namespace,
      namespaceName: this.namespaceName,
    };
  }

  alarmDetail(alarm: Alarm) {
    this.alarmRoute.emit({
      path: 'detail',
      name: alarm.name,
      resource_name: alarm.resource_name,
      ...this.getRouteParams(),
    });
  }

  createAlarmTemplate() {
    const modelRef = this.dialogService.open(TemplateApplyComponent, {
      data: {
        metric_type: this.metric_type,
        componentName: this.listParam.name,
        clusterName: this.clusterName,
        namespaceName: this.namespaceName,
        appName: this.appName,
        kind: this.listParam.kind,
      },
      size: DialogSize.Large,
    });
    modelRef.componentInstance.close.subscribe(async () => {
      modelRef.close();
      await delay(500);
      this.onUpdate();
    });
  }

  createAlarm() {
    this.alarmRoute.emit({
      path: 'form',
      ...this.getRouteParams(),
    });
  }

  updateAlarm(alarm: Alarm) {
    this.alarmRoute.emit({
      path: 'form',
      name: alarm.name,
      resource_name: alarm.resource_name,
      ...this.getRouteParams(),
    });
  }

  async deleteAlarm(alarm: Alarm) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_alarm_title'),
        content: this.translate.get('delete_alarm_content', {
          alarm_name: alarm.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      if (this.clusters && this.clusters.length > 1) {
        const res: BatchReponse = await this.deleteK8sAppAlarmBatch(
          this.clusters,
          alarm,
        );
        Object.entries(res.responses).forEach(([key, data]) => {
          if (data.code >= 300) {
            const body = JSON.parse(data.body);
            const error = this.errorsToastService.parseErrors({
              errors: body.errors,
            });
            const title = this.translate.get('cluster_alarm_delete_error', {
              alarmName: alarm.name,
              clusterName: key,
            });
            this.auiNotificationService.error({
              content: `${title}:<br>${error.content}`,
            });
          } else {
            this.auiNotificationService.success({
              content: this.translate.get('cluster_alarm_delete_success', {
                alarmName: alarm.name,
                clusterName: key,
              }),
            });
          }
        });
      } else {
        await this.alarmService.deleteAlert(
          this.clusterName,
          this.namespaceName,
          alarm.resource_name,
          alarm.group_name,
          alarm.name,
        );
        this.auiNotificationService.success(
          this.translate.get('alarm_delete_success'),
        );
      }
      await delay(500);
      this.onUpdate();
    } catch (rejection) {
      this.errorsToastService.error(rejection);
    }
  }

  deleteK8sAppAlarmBatch(
    clusters: string[],
    alarm: Alarm,
  ): Promise<BatchReponse> {
    const requests = clusters.map((cluster: string) => {
      const url = `/v1/alerts/${cluster}/${this.namespaceName}/${alarm.resource_name}/${alarm.group_name}/${alarm.name}`;
      return {
        cluster,
        url,
      };
    });
    return this.httpService.request({
      method: 'POST',
      url: '/ajax/v2/batch/',
      body: {
        common: {
          method: 'DELETE',
          header: {
            'Content-Type': 'application/json',
          },
        },
        requests: mapValues(
          keyBy(requests, (item: any) => {
            return item.cluster;
          }),
          (item: any) => {
            return {
              url: item.url,
            };
          },
        ),
      },
    });
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror) {
      this.mirrorClusters = cluster.mirror.regions || [];
      this.clusters = this.mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
    } else {
      this.clusters = [this.clusterName];
    }
  }
}
