import { DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { get } from 'lodash-es';
import { filter, first } from 'rxjs/operators';

import { PodControllerKindToResourceTypeEnum } from 'app/features-shared/app/utils/pod-controller';
import { AlarmService } from 'app/services/api/alarm.service';
import { AppService } from 'app/services/api/app.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import * as fromStore from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { Deployment, PodController, StatefulSet } from 'app/typings';
import { delay } from 'app2/utils';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { DetailDataService } from '../../detail-data.service';
import { PodStatus } from '../../pod-status/component';

@Component({
  selector: 'rc-pod-controller-detail-header',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerDetailHeaderComponent implements OnInit, OnChanges {
  @Input()
  appName: string;
  @Input()
  component: string;
  @Input()
  data: PodController;
  @Input()
  resourceActions: string[];
  @Input()
  appResourceActions: string[];
  @Input()
  podStatus: PodStatus;

  showStart: boolean;
  showStop: boolean;
  canStart: boolean;
  canStop: boolean;

  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = []; // avaliable mirror clusters

  namespace: string;
  project: string;
  cluster: string;

  canDelete: boolean;
  canUpdate: boolean;

  async ngOnChanges() {
    if (this.data && this.resourceActions) {
      await delay(100);
      this.onInputChange();
    }
  }

  // ===== actions =====
  update() {
    this.router.navigate(
      [
        'app',
        'update',
        this.appName,
        'update-component',
        this.data.kind.toLowerCase(),
        this.data.metadata.name,
      ],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      },
    );
  }

  async delete() {
    this.mirrorClusters = [];
    await this.getMirrorClusters();
    try {
      await this.appUtilService.deleteComponent({
        cluster: this.cluster,
        resource_type: PodControllerKindToResourceTypeEnum[this.data.kind],
        namespace: this.namespace,
        app: this.appName,
        component: this.component,
        mirrorClusters: this.mirrorClusters,
        project: this.project,
      });
      this.alarmService
        .deleteAlertsByLabelSelector(
          this.cluster,
          this.namespace,
          'workload',
          this.component,
        )
        .catch(() => {});
      this.router.navigate(['app', 'detail', this.appName], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } catch (_err) {}
  }

  private async onInputChange() {
    this.canUpdate = this.resourceActions.includes('k8s_others:update');
    this.canDelete = this.resourceActions.includes('k8s_others:delete');
    if (this.data.kind === 'Deployment' || this.data.kind === 'StatefulSet') {
      this.showStart = this.appResourceActions.includes('application:start');
      this.showStop = this.appResourceActions.includes('application:stop');
      this.canStart =
        (this.data as Deployment | StatefulSet).spec.replicas === 0 &&
        get(this.podStatus, 'status') !== 'Pending';
      this.canStop =
        (this.data as Deployment | StatefulSet).spec.replicas !== 0 &&
        get(this.podStatus, 'status') !== 'Pending';
    }
    this.cdr.markForCheck();
  }

  private async getMirrorClusters() {
    const cluster = await this.store
      .select(fromStore.getCurrentCluster)
      .pipe(
        filter(cluster => !!cluster),
        first(),
      )
      .toPromise();
    if (cluster.mirror && cluster.mirror.regions) {
      this.mirrorClusters = cluster.mirror.regions.map(
        ({ name, display_name }) => ({ name, display_name }),
      );
    }
    if (this.mirrorClusters.length > 1) {
      const clusterAppMap = await this.appService.batchGetAppDetailCRD({
        project: this.project,
        clusters: this.mirrorClusters.map(c => c.name),
        namespace: this.namespace,
        name: this.appName,
      });
      const appClusters = Object.keys(clusterAppMap);
      this.mirrorClusters = this.mirrorClusters.filter(
        c => appClusters.indexOf(c.name) >= 0,
      );
    }
  }

  async start() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('start'),
        content: this.translateService.get(
          'app_service_start_component_confirm',
          {
            name: this.data.metadata.name,
          },
        ),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.disableActions();
    this.cdr.markForCheck();
    this.k8sResourceService
      .startWorkload(this.data.kind, {
        namespace: this.namespace,
        name: this.data.metadata.name,
        clusterName: this.cluster,
      })
      .then(() => {
        this.detailDataService.refreshPolling();
      })
      .catch(_e => {});
  }

  async stop() {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('stop'),
        content: this.translateService.get(
          'app_service_stop_component_confirm',
          {
            name: this.data.metadata.name,
          },
        ),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.disableActions();
    this.cdr.markForCheck();
    this.k8sResourceService
      .stopWorkload(this.data.kind, {
        namespace: this.namespace,
        name: this.data.metadata.name,
        clusterName: this.cluster,
      })
      .then(() => {
        this.detailDataService.refreshPolling();
      })
      .catch(_e => {});
  }

  private disableActions() {
    this.canStart = false;
    this.canStop = false;
  }

  ngOnInit(): void {}
  constructor(
    private router: Router,
    private cdr: ChangeDetectorRef,
    private workspaceComponent: WorkspaceComponent,
    private appUtilService: AppUtilService,
    private store: Store<fromStore.AppState>,
    private alarmService: AlarmService,
    private appService: AppService,
    private k8sResourceService: K8sResourceService,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private detailDataService: DetailDataService,
  ) {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    [this.cluster, this.namespace, this.project] = [
      baseParams.cluster,
      baseParams.namespace,
      baseParams.project,
    ];
  }
}
