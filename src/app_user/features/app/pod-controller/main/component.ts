import { DialogService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { get } from 'lodash-es';
import { first } from 'rxjs/operators';

import { ExecCommandDialogComponent } from 'app/features-shared/exec/exec-command/component';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { TerminalService } from 'app/services/api/terminal.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  Container,
  HorizontalPodAutoscaler,
  K8sResourceWithActions,
  PodController,
  Service,
  TopologyResponse,
} from 'app/typings';
import { StringMap, WorkspaceBaseParams } from 'app/typings';
import { AppUtilService } from '../../app-util.service';
import { DetailDataService } from '../../detail-data.service';
import { EMPTY_POD_CONTROLLER } from '../page.component';

@Component({
  selector: 'rc-pod-controller-detail-main',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerDetailMainComponent implements OnChanges {
  @Input()
  podController: PodController | Symbol;
  @Input()
  resourceActions: string[];
  @Input()
  appResourceActions: string[];
  @Input()
  autoScaler: HorizontalPodAutoscaler;
  @Input()
  baseParams: WorkspaceBaseParams;
  @Input()
  appName: string;
  services: K8sResourceWithActions<Service>[];

  podControllerData: PodController;
  canUpdate: boolean;
  containers: Container[];
  matchLabels: StringMap;
  initialized: boolean;

  constructor(
    private cdr: ChangeDetectorRef,
    private activatedRoute: ActivatedRoute,
    private appUtilService: AppUtilService,
    private k8sResourceService: K8sResourceService,
    private translateService: TranslateService,
    private detailDataService: DetailDataService,
    private notificationService: NotificationService,
    private terminalService: TerminalService,
    private dialogService: DialogService,
    private router: Router,
  ) {}

  ngOnChanges({ podController }: SimpleChanges) {
    if (podController) {
      if (
        podController.currentValue &&
        podController.currentValue !== EMPTY_POD_CONTROLLER
      ) {
        this.onInputChange();
      }
      if (!podController.isFirstChange()) {
        this.initialized = true;
      }
    }
  }

  get isEmpty() {
    return this.initialized && !this.podControllerData;
  }

  updateImageTag(containerName: string) {
    this.appUtilService
      .updateImageTag({
        podController: this.podControllerData,
        containerName: containerName,
        baseParams: this.baseParams,
      })
      .subscribe(res => {
        if (res) {
          this.onUpdateSuccess();
        }
      });
  }

  updateResourceSize(containerName: string) {
    this.appUtilService
      .updateResourceSize({
        podController: this.podControllerData,
        containerName: containerName,
        baseParams: this.baseParams,
      })
      .subscribe(res => {
        if (res) {
          this.onUpdateSuccess();
        }
      });
  }

  openTerminal(params: {
    pods: string; // pod names separated by ;
    selectedPod: string;
    container: string;
  }) {
    const dialogRef = this.dialogService.open(ExecCommandDialogComponent);
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (!!data) {
        this.terminalService.openTerminal({
          app: this.appName,
          ctl: this.podControllerData.metadata.name,
          pods: params.pods,
          selectedPod: params.selectedPod,
          container: params.container,
          namespace: this.baseParams.namespace,
          cluster: this.baseParams.cluster,
          user: data.user,
          command: data.command,
        });
      }
    });
  }

  openLog(params: { pod: string; container: string }) {
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        tabIndex: 4,
        pod: params.pod,
        container: params.container,
      },
    });
  }

  private async onInputChange() {
    this.podControllerData = this.podController as PodController;
    this.canUpdate = this.resourceActions.includes('k8s_others:update');
    this.getServices();
    this.cdr.markForCheck();
  }

  private async getServices() {
    const clusterName = this.baseParams.cluster;
    const namespace = this.baseParams.namespace;
    const [services, topology]: [
      K8sResourceWithActions<Service>[],
      TopologyResponse
    ] = await Promise.all([
      this.k8sResourceService
        .getK8sResources('services', {
          clusterName,
          namespace,
        })
        .catch(_e => [] as K8sResourceWithActions<Service>[]),
      this.k8sResourceService
        .getK8sTopologyByResource({
          cluster: clusterName,
          namespace: namespace,
          kind: get(this.podControllerData, 'kind').toLowerCase(),
          name: get(this.podControllerData, 'metadata.name'),
        })
        .catch(_e => ({} as TopologyResponse)),
    ]);
    const names: string[] = Object.values(topology.nodes || {})
      .filter(item => item.kind === 'Service')
      .map(item => item.metadata.name);

    this.services = services.filter((sv: K8sResourceWithActions<Service>) =>
      names.includes(sv.kubernetes.metadata.name),
    );
    this.cdr.markForCheck();
  }

  private onUpdateSuccess() {
    this.notificationService.success(
      this.translateService.get('update_success'),
    );
    this.detailDataService.refreshPolling();
  }
}
