import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ChangeDetectorRef, OnChanges, SimpleChanges } from '@angular/core';

import { cloneDeep, get } from 'lodash-es';
import { from, of, Subject } from 'rxjs';
import {
  catchError,
  delay,
  first,
  switchMap,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { getLabels } from 'app/features-shared/app/utils/affinity';
import { PodControllerKindToResourceTypeEnum } from 'app/features-shared/app/utils/pod-controller';
import { AppService } from 'app/services/api/app.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  AffinityTerm,
  PodAffinityType,
  WorkspaceBaseParams,
} from 'app/typings';
import { PodController } from 'app/typings/backend-api';
import {
  Affinity,
  Deployment,
  HorizontalPodAutoscaler,
  PodAffinityTerm,
  PodSpecAffinity,
  StatefulSet,
  StringMap,
  WeightedPodAffinityTerm,
} from 'app/typings/raw-k8s';
import { AppUtilService } from 'app_user/features/app/app-util.service';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { PodStatus } from 'app_user/features/app/pod-status/component';
import { UpdateLabelDialogComponent } from '../../dialog/update-label/component';
import {
  getPodControllerUpdateStrategy,
  removeDirtyDataBeforeUpdate,
} from '../../util';

interface PodControllerMeta {
  name: string;
  namespace: string;
  labels: StringMap;
  kind: string;
  creationTimestamp: string;
  nodeSelector: StringMap;
  strategy: {
    updateStrategy: string;
    rollingUpdateStrategy: string;
  };
}

interface AffinityItem {
  isAnti: boolean;
  val: AffinityTerm;
}

@Component({
  selector: 'rc-pod-controller-detail-meta',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerDetailMetaComponent
  implements OnInit, OnDestroy, OnChanges {
  @Input()
  podController: PodController;
  @Input()
  autoScaler: HorizontalPodAutoscaler;
  @Input()
  resourceActions: string[];
  @Input()
  appResourceActions: string[];
  @Input()
  baseParams: WorkspaceBaseParams;

  @Input() appName: string;

  addresses: string[] = [];
  podStatus: PodStatus;
  meta: PodControllerMeta;
  scalingConfig: {
    min: number;
    max: number;
  };
  canUpdate: boolean;
  shouldShowScalingConfig: boolean;
  private updateReplicas$ = new Subject<number>();
  private onDestroy$ = new Subject<void>();

  private podAffinity: PodSpecAffinity;
  isAffinityActive: boolean;
  podAffinities: AffinityItem[] = [];

  get showUpdateStrategy() {
    return this.meta.strategy.updateStrategy;
  }

  get showRollingUpdateStrategy() {
    return this.meta.strategy.rollingUpdateStrategy;
  }

  constructor(
    private appService: AppService,
    private detailDataService: DetailDataService,
    private errorToastService: ErrorsToastService,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private appUtilService: AppUtilService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnChanges({ podController, autoScaler, resourceActions }: SimpleChanges) {
    if (podController && podController.currentValue) {
      this.getPodControllerMeta();
      this.getAddresses();
      this.getPodStatus();
    }
    if (autoScaler && autoScaler.currentValue) {
      this.getScalingConfig();
    }
    if (resourceActions && resourceActions.currentValue) {
      this.canUpdate = this.resourceActions.includes('k8s_others:update');
    }
  }

  ngOnInit() {
    this.updateReplicas$
      .pipe(
        switchMap(replicas => {
          return of(replicas).pipe(delay(1000));
        }),
        switchMap(replicas => {
          const payload: Deployment | StatefulSet = cloneDeep(
            this.podController,
          );
          payload.spec.replicas = replicas;
          return from(
            this.appService.updateResource({
              cluster: this.baseParams.cluster,
              namespace: this.baseParams.namespace,
              resource_type:
                PodControllerKindToResourceTypeEnum[this.meta.kind],
              name: this.meta.name,
              payload: removeDirtyDataBeforeUpdate(payload),
            }),
          ).pipe(
            catchError(err => {
              this.errorToastService.error(err);
              return of(err);
            }),
          );
        }),
        takeUntil(this.onDestroy$),
      )
      .subscribe(() => {
        this.detailDataService.refreshPolling();
      });
    this.podAffinity = get(this.podController, 'spec.template.spec.affinity');
    this.podAffinities = get(
      this.podAffinity,
      `podAffinity.${PodAffinityType.required}`,
      [],
    )
      .map((val: Affinity) => ({
        val,
        isAnti: false,
      }))
      .concat(
        get(
          this.podAffinity,
          `podAffinity.${PodAffinityType.preferred}`,
          [],
        ).map((val: Affinity) => ({
          val,
          isAnti: false,
        })),
      )
      .concat(
        get(
          this.podAffinity,
          `podAntiAffinity.${PodAffinityType.required}`,
          [],
        ).map((val: Affinity) => ({
          val,
          isAnti: true,
        })),
      )
      .concat(
        get(
          this.podAffinity,
          `podAntiAffinity.${PodAffinityType.preferred}`,
          [],
        ).map((val: Affinity) => ({
          val,
          isAnti: true,
        })),
      );
  }

  getLabels(rowData: AffinityItem) {
    return getLabels(rowData.val);
  }

  isMatchExpressions(rowData: AffinityItem) {
    return get(rowData.val as WeightedPodAffinityTerm, 'weight')
      ? !!(rowData.val as WeightedPodAffinityTerm).podAffinityTerm.labelSelector
          .matchExpressions
      : !!(rowData.val as PodAffinityTerm).labelSelector.matchExpressions;
  }

  updateLabels() {
    const dialogRef = this.dialogService.open(UpdateLabelDialogComponent, {
      size: DialogSize.Big,
      data: {
        podController: this.podController,
        baseParams: {
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
        },
      },
    });
    return dialogRef.componentInstance.close
      .pipe(
        first(),
        tap(() => {
          dialogRef.close();
        }),
      )
      .subscribe(res => {
        if (res) {
          this.notificationService.success(
            this.translateService.get('update_success'),
          );
          this.detailDataService.refreshPolling();
        }
      });
  }

  updateReplicas(replicas: number) {
    this.updateReplicas$.next(replicas);
  }

  ngOnDestroy() {
    this.onDestroy$.next(null);
  }

  private getPodControllerMeta() {
    const _pd = this.podController;
    this.meta = {
      name: get(_pd, 'metadata.name', ''),
      namespace: get(_pd, 'metadata.namespace', ''),
      labels: get(_pd, 'spec.template.metadata.labels', {}),
      kind: get(_pd, 'kind', ''),
      creationTimestamp: get(_pd, 'metadata.creationTimestamp', ''),
      nodeSelector: get(_pd, 'spec.template.spec.nodeSelector', {}),
      strategy: getPodControllerUpdateStrategy(get(_pd, 'kubernetes', null)),
    };
  }

  private getScalingConfig() {
    this.shouldShowScalingConfig = true;
    this.scalingConfig = {
      min: get(this.autoScaler, 'spec.minReplicas', 0),
      max: get(this.autoScaler, 'spec.maxReplicas', 0),
    };
  }

  private async getAddresses() {
    try {
      const res = await this.appUtilService.getApplicationAddress({
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        app: this.appName,
      });
      this.addresses = get(
        res,
        `${this.podController.kind + '-' + this.podController.metadata.name}`,
        [],
      );
      this.cdr.markForCheck();
    } catch (_) {}
  }

  private async getPodStatus() {
    try {
      const res = await this.appService
        .getApplicationStatus({
          cluster: this.baseParams.cluster,
          namespace: this.baseParams.namespace,
          app: this.appName,
        })
        .catch(_err => null);
      const workloads = get(
        res,
        `workloads.${this.podController.kind.toLowerCase() +
          '-' +
          this.podController.metadata.name}`,
        {},
      );
      this.podStatus = {
        desired: workloads.desired || 0,
        current: workloads.current || 0,
        status: workloads.status || 'Unknown',
      };
      this.cdr.markForCheck();
    } catch (_) {}
  }
}
