import { MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
} from '@angular/core';

import * as Highcharts from 'highcharts';
import { cloneDeep, get } from 'lodash-es';

import { BaseTimeSelectComponent } from 'app/features-shared/alarm/base-time-select.component';
import {
  AGGREGATORS,
  MetricNumericOptions,
  MetricPercentOptions,
  MODES,
  QueryMetrics,
} from 'app/features-shared/app/utils/monitor';
import { AppService } from 'app/services/api/app.service';
import { LogService } from 'app/services/api/log.service';
import {
  Metric,
  MetricQueries,
  MetricQuery,
  MetricService,
} from 'app/services/api/metric.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, WorkspaceBaseParams } from 'app/typings';
import { PodController } from 'app/typings/backend-api';
require('highcharts/modules/no-data-to-display')(Highcharts);
const uuidv4 = require('uuid/v4');

@Component({
  selector: 'rc-pod-controller-monitor-chart',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerMonitorChartComponent extends BaseTimeSelectComponent
  implements OnInit, OnDestroy {
  @Input()
  podController: PodController;
  @Input()
  baseParams: WorkspaceBaseParams;

  private intervalTimer: number;
  private pods: string[];

  componentName: string;
  componentType: string;
  modes = MODES;
  aggregators = AGGREGATORS;
  selectedGroupMode = this.modes[0].key;
  selectedAggregator = this.aggregators[0].key;
  Highcharts = Highcharts;
  cpuChartOptions: Highcharts.Options = cloneDeep(MetricPercentOptions);
  memChartOptions: Highcharts.Options = cloneDeep(MetricPercentOptions);
  sentBytesChartOptions: Highcharts.Options = cloneDeep(MetricNumericOptions);
  receivedBytesChartOptions: Highcharts.Options = cloneDeep(
    MetricNumericOptions,
  );
  queryMetrics = QueryMetrics;
  queryIdMap = new Map();

  constructor(
    private cdr: ChangeDetectorRef,
    logService: LogService,
    auiMessageService: MessageService,
    translateService: TranslateService,
    private appService: AppService,
    private metricService: MetricService,
    @Inject(ACCOUNT) public account: Account,
  ) {
    super(logService, auiMessageService, translateService);
  }

  async ngOnInit() {
    this.initTimer();
    Highcharts.setOptions({
      lang: {
        noData: this.translateService.get('no_data'),
      },
    });
    this.sentBytesChartOptions.tooltip.valueSuffix = this.translateService.get(
      'byte/second',
    );
    this.receivedBytesChartOptions.tooltip.valueSuffix = this.translateService.get(
      'byte/second',
    );
    this.componentName = get(this.podController, 'metadata.name');
    this.componentType = get(this.podController, 'kind');
    const matchLabels = get(
      this.podController,
      'spec.selector.matchLabels',
      {},
    );
    const labelSelector = Object.keys(matchLabels || {})
      .map(key => `${key}=${matchLabels[key]}`)
      .join(',');
    const response = await this.appService
      .getResourcesInNamespace({
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        labelSelector,
        resource_type: 'pods',
      })
      .then((res: any) => res.results || [])
      .catch(_err => []);
    this.pods = response.map((pod: any) => {
      return get(pod, 'metadata.name');
    });
    this.resetTimeRange();
    this.loadCharts();
    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    window.clearInterval(this.intervalTimer);
  }

  initTimer() {
    this.intervalTimer = window.setInterval(() => {
      if (!this.chartLoading) {
        if (this.selectedTimeStampOption !== 'custom_time_range') {
          this.resetTimeRange();
        }
        if (this.checkTimeRange()) {
          this.chartLoading = true;
          this.cdr.markForCheck();
          this.loadCharts();
        }
      }
    }, 60000);
  }

  loadCharts() {
    const args = {
      mode: this.selectedGroupMode,
      aggregator: this.selectedAggregator,
      start: parseInt((this.queryDates.start_time / 1000).toFixed(0), 10),
      end: parseInt((this.queryDates.end_time / 1000).toFixed(0), 10),
    };
    // http://jira.alaudatech.com/browse/DEV-12815
    let start = 0;
    if (args.end - args.start < 1800) {
      this.step = 60;
      start = args.start;
    } else {
      this.step = parseInt(((args.end - args.start) / 30).toFixed(0), 10);
      start = args.start + this.step;
    }
    this.end_time = args.end;
    const params: MetricQuery = {
      start,
      end: args.end,
      step: this.step,
    };
    let pod_name = '';
    if (this.pods.length) {
      pod_name = this.pods.join('|');
    }
    const queries = this.queryMetrics.map(query => {
      const uuid = uuidv4();
      this.queryIdMap.set(uuid, query);
      const metricQuery: MetricQueries = {
        aggregator: args.aggregator,
        id: uuid,
        labels: [
          {
            type: 'EQUAL',
            name: '__name__',
            value: `${args.mode}.${query.metric}`,
          },
          {
            type: 'EQUAL',
            name: 'namespace',
            value: this.baseParams.namespace,
          },
          {
            type: 'EQUAL',
            name: 'kind',
            value: this.componentType,
          },
          {
            type: 'EQUAL',
            name: 'name',
            value: this.componentName,
          },
          {
            type: 'IN',
            name: 'pod_name',
            value: pod_name,
          },
        ],
      };
      switch (args.mode) {
        case 'pod':
          metricQuery.group_by = 'pod_name';
          break;
        case 'container':
          metricQuery.group_by = 'container_name';
      }
      return metricQuery;
    });
    params.queries = queries;
    this.metricService
      .queryMetrics(this.baseParams.cluster, params)
      .then(results => {
        const chartData = new Map<string, Metric[]>([
          ['cpu.utilization', []],
          ['memory.utilization', []],
          ['network.transmit_bytes', []],
          ['network.receive_bytes', []],
        ]);
        results.forEach(result => {
          const { metric } = this.queryIdMap.get(result.metric.__query_id__);
          chartData.set(metric, [result, ...chartData.get(metric)]);
        });
        chartData.forEach(values => {
          const { series, unit } = this.queryIdMap.get(
            values[0]['metric'].__query_id__,
          );
          if (unit) {
            this[series]['series'] = this.parseMetricsResponse(values, 100);
          } else {
            this[series]['series'] = this.parseMetricsResponse(values);
          }
        });
      })
      .catch(error => {
        this.cpuChartOptions['series'] = [];
        this.memChartOptions['series'] = [];
        this.sentBytesChartOptions['series'] = [];
        this.receivedBytesChartOptions['series'] = [];
        throw error;
      })
      .finally(() => {
        this.chartLoading = false;
        this.cdr.markForCheck();
      });
  }

  private parseMetricsResponse(metric: Metric[], multiplier?: number) {
    if (this.selectedGroupMode === 'container') {
      metric = metric.filter(el => el.metric.container_name);
    }
    return metric.map(el => {
      if (this.step >= 60 && el.values.length < 30) {
        el.values = this.metricService.fillUpResult(
          el.values,
          this.end_time,
          this.step,
        );
      }
      let name = this.componentName;
      if (this.selectedGroupMode === 'pod') {
        name = el.metric.pod_name;
      } else if (this.selectedGroupMode === 'container') {
        name = `${el.metric.pod_name} ${el.metric.container_name}`;
      }
      return {
        name: name,
        data: el.values.map((value: Array<number | string>) => {
          let y = null;
          if (value[1] !== '+Inf' && value[1] !== '') {
            y = Number(value[1]);
            if (multiplier) {
              y = y * 100;
            }
          }
          return {
            x: Number(value[0]) * 1000,
            y,
          };
        }),
      };
    });
  }
}
