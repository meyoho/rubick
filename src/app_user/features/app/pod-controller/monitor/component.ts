import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

import { WorkspaceBaseParams } from 'app/typings';
import { PodController } from 'app/typings/backend-api';

@Component({
  selector: 'rc-pod-controller-monitor',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodControllerMonitorComponent implements OnChanges {
  @Input()
  podController: PodController;
  @Input()
  baseParams: WorkspaceBaseParams;

  initialized = false;

  ngOnChanges({ podController }: SimpleChanges) {
    if (podController && podController.currentValue) {
      this.initialized = true;
    }
  }

  constructor() {}
}
