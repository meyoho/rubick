import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { safeDump } from 'js-yaml';
import { find, get } from 'lodash-es';
import { from, Observable, Subject } from 'rxjs';
import {
  debounceTime,
  filter,
  first,
  map,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
} from 'rxjs/operators';

import { UpdateEnvDialogComponent } from 'app/features-shared/app/dialog/update-env/component';
import { PodControllerKindEnum } from 'app/features-shared/app/utils/pod-controller';
import { AppService } from 'app/services/api/app.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import {
  HorizontalPodAutoscaler,
  HorizontalPodAutoscalerMeta,
  PodController,
  WorkspaceBaseParams,
} from 'app/typings';
import { Environment } from 'app/typings';
import { K8sResourceWithActions } from 'app/typings/backend-api';
import * as codeEditorUtil from 'app/utils/code-editor-config';
import { delay } from 'app2/utils';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

export const EMPTY_POD_CONTROLLER = Symbol('EMPTY_POD_CONTROLLER');

@Component({
  templateUrl: './page.component.html',
  styleUrls: ['./page.component.scss'],
  providers: [DetailDataService],
})
export class PodControllerDetailPageComponent implements OnInit, OnDestroy {
  private appData$: Observable<K8sResourceWithActions[]>;
  private onDestroy$ = new Subject<void>();
  private podController: PodController;
  private kind: PodControllerKindEnum;

  podController$: Observable<PodController | Symbol>;
  autoScaler$: Observable<HorizontalPodAutoscaler>;
  resourceActions$: Observable<string[]>;
  appResourceActions$: Observable<string[]>;
  appName: string;
  podControllerName: string;

  seletedTabIndex$: Observable<number>;
  yaml$: Observable<string>;
  logInitialState$: Observable<{ pod: string; container: string }>;

  codeEditorOptions = codeEditorUtil.yamlOptions;
  actionsConfigView = codeEditorUtil.viewActions;
  showMetricTab: boolean;
  canUpdate: boolean;
  baseParams: WorkspaceBaseParams;

  constructor(
    @Inject(ENVIRONMENTS) public env: Environment,
    private appService: AppService,
    private activatedRoute: ActivatedRoute,
    private notificationService: NotificationService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private workspaceComponent: WorkspaceComponent,
    private regionService: RegionService,
    private router: Router,
    private detailDataService: DetailDataService,
  ) {
    this.detailDataService.startPolling();
    this.onDestroy$
      .pipe(first())
      .subscribe(_ => this.detailDataService.stopPolling());
  }

  ngOnInit() {
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.initStateParams$();
    this.initAppData$();

    this.resourceActions$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe((resourceActions: string[]) => {
        this.canUpdate = resourceActions.includes('k8s_others:update');
      });
  }

  initStateParams$() {
    this.kind = this.activatedRoute.snapshot.params.kind;
    this.appName = this.activatedRoute.snapshot.params.app;
    this.podControllerName = this.activatedRoute.snapshot.params.name;

    this.regionService
      .getCluster(this.baseParams.cluster)
      .then((cluster: Cluster) => {
        this.showMetricTab =
          get(cluster, 'features.customized.metric') ||
          get(cluster, 'features.metric.type');
      });

    this.seletedTabIndex$ = this.activatedRoute.queryParams.pipe(
      map(queryParams => queryParams.tabIndex || 0),
    );
    this.logInitialState$ = this.activatedRoute.queryParams.pipe(
      map(queryParams => ({
        pod: queryParams.pod,
        container: queryParams.container,
      })),
    );
  }

  initAppData$() {
    this.appData$ = this.detailDataService.polling$.pipe(
      takeUntil(this.onDestroy$),
      debounceTime(50),
      switchMap(() =>
        from(
          this.appService
            .getAppDetailCRD({
              cluster_name: this.baseParams.cluster,
              namespace: this.baseParams.namespace,
              app: this.appName,
            })
            .catch(() => []),
        ),
      ),
      publishReplay(1),
      refCount(),
    );

    const podControllerResource$ = this.appData$.pipe(
      map(resources =>
        find(
          resources,
          c =>
            c.kubernetes.kind.toLowerCase() === this.kind.toLowerCase() &&
            c.kubernetes.metadata.name === this.podControllerName,
        ),
      ),
    );
    this.podController$ = podControllerResource$.pipe(
      map(r => (r ? r.kubernetes : EMPTY_POD_CONTROLLER)),
      publishReplay(1),
      refCount(),
    );
    this.resourceActions$ = podControllerResource$.pipe(
      map(r => (r ? r.resource_actions : [])),
    );
    this.appResourceActions$ = this.appData$.pipe(
      map(r => (r ? r[0].resource_actions : [])),
    );
    this.yaml$ = this.podController$.pipe(
      filter(pc => !!pc),
      map(pc =>
        safeDump(pc, {
          sortKeys: true,
        }),
      ),
      publishReplay(1),
      refCount(),
    );
    this.autoScaler$ = this.appData$.pipe(
      map(resources => resources.map(r => r.kubernetes)),
      map(
        resources =>
          find(resources, r => {
            return (
              r.kind.toLowerCase() ===
                HorizontalPodAutoscalerMeta.kind.toLowerCase() &&
              get(r, 'spec.scaleTargetRef.kind', '').toLowerCase() ===
                this.kind.toLowerCase() &&
              get(r, 'spec.scaleTargetRef.name', '') === this.podControllerName
            );
          }) as HorizontalPodAutoscaler,
      ),
    );
    this.podController$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(p => p !== EMPTY_POD_CONTROLLER),
      )
      .subscribe(p => (this.podController = p as PodController));
  }

  async onTabIndexChange(index: number) {
    const currentQueryParams = this.activatedRoute.snapshot.queryParams;
    this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        ...currentQueryParams,
        tabIndex: index,
      },
    });

    await delay(100);
    if (index !== 0) {
      this.detailDataService.suspendPolling();
    } else {
      this.detailDataService.resumePolling();
    }
  }

  jumpToListPage() {
    this.router.navigate(['app'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  viewAppDetail() {
    this.router.navigate(['app', 'detail', this.appName], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  updateEnv(params: { containerName: string; type: 'env' | 'envFrom' }) {
    const dialogRef = this.dialogService.open(UpdateEnvDialogComponent, {
      size: params.type === 'env' ? DialogSize.Big : DialogSize.Medium,
      fitViewport: true,
      data: {
        podController: this.podController,
        containerName: params.containerName,
        baseParams: this.baseParams,
        type: params.type,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((res: boolean) => {
        if (res) {
          this.notificationService.success(
            this.translateService.get('update_success'),
          );
          this.detailDataService.refreshPolling();
        }
        dialogRef.close();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
