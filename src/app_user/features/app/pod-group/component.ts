import { DialogService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { find, get } from 'lodash-es';
import { first } from 'rxjs/operators';

import { ExecCommandDialogComponent } from 'app/features-shared/exec/exec-command/component';
import { AppService } from 'app/services/api/app.service';
import { PodService } from 'app/services/api/pod.service';
import { TerminalService } from 'app/services/api/terminal.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { WorkspaceBaseParams } from 'app/typings';
import { K8sResourceWithActions, PodController } from 'app/typings/backend-api';
import { Container, Pod } from 'app/typings/raw-k8s';
import { GenericStatus } from 'app/typings/status';
import { DetailDataService } from 'app_user/features/app/detail-data.service';
import { EMPTY_POD_CONTROLLER } from '../pod-controller/page.component';

interface PodGroupItem {
  name: string;
  status: string;
  host_ip: string; // 所在主机ip
  pod_ip: string; // 容器ip
  creationTimestamp: string;
  containers: Container[];
}

enum Status {
  Failed = 'Failed',
  Succeeded = 'Succeeded',
  Running = 'Running',
  Pending = 'Pending',
  Unknown = 'Unknown',
}

@Component({
  selector: 'rc-app-detail-pod-group',
  templateUrl: './template.html',
  styleUrls: ['./style.scss', '../detail/style.common.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ApplicationPodGroupComponent implements OnInit, OnChanges {
  @Input()
  podController: PodController;
  @Input()
  resourceActions: string[] = [];
  @Input()
  baseParams: WorkspaceBaseParams;
  @Input()
  appName: string;
  @Input()
  pods: Pod[];

  viewModel: PodGroupItem[] = [];
  canUpdate: boolean;
  loading: boolean;

  columnDef = [
    'name',
    'status',
    'host_ip',
    'pod_ip',
    'creationTimestamp',
    'action',
  ];

  async ngOnChanges({ podController, pods, resourceActions }: SimpleChanges) {
    if (podController && podController.currentValue) {
      try {
        const pods =
          this.podController === EMPTY_POD_CONTROLLER
            ? []
            : await this.getPods();
        this.viewModel = this.adaptPodsViewModel(pods);
      } catch (_e) {
        this.viewModel = [];
      }
      this.cdr.markForCheck();
    }

    if (pods && pods.currentValue) {
      this.viewModel = this.adaptPodsViewModel(this.pods);
    }

    if (resourceActions && resourceActions.currentValue) {
      this.canUpdate =
        this.resourceActions.includes('k8s_others:update') ||
        this.resourceActions.includes('application:update');
    }
  }

  getPodStatus(status: Status) {
    switch (status) {
      case Status.Succeeded:
        return GenericStatus.Success;
      case Status.Pending:
        return GenericStatus.Pending;
      case Status.Failed:
        return GenericStatus.Error;
      case Status.Running:
        return GenericStatus.Running;
      default:
        return GenericStatus.Unknown;
    }
  }

  /**
   * matchLabels => labelSelector => Pod group: Pod[]
   */
  private async getPods() {
    this.loading = true;
    const matchLabels = get(
      this.podController,
      'spec.selector.matchLabels',
      {},
    );
    const labelSelector = Object.keys(matchLabels || {})
      .map(key => `${key}=${matchLabels[key]}`)
      .join(',');
    const result = await this.appService
      .getResourcesInNamespace({
        cluster: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        labelSelector,
        resource_type: 'pods',
      })
      .then((res: any) => res.results || [])
      .catch(_err => []);
    this.loading = false;
    return result
      ? result.map((p: K8sResourceWithActions<Pod>) => p.kubernetes)
      : [];
  }

  // param: Pod[]
  private adaptPodsViewModel(pods: Pod[]): PodGroupItem[] {
    return pods.map(r => ({
      name: get(r, 'metadata.name', ''),
      status: get(r, 'status.phase', ''),
      host_ip: get(r, 'status.hostIP', ''),
      pod_ip: get(r, 'status.podIP', ''),
      creationTimestamp: get(r, 'metadata.creationTimestamp', ''),
      containers: get(r, 'spec.containers', []),
      containerStatuses: get(r, 'status.containerStatuses', []),
    }));
  }

  // actions
  async deleteInstance(pod: PodGroupItem) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get(
          'service_instance_restart_confirm_title',
        ),
        content: this.translateService.get('service_instance_restart_confirm'),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.podService
      .deletePod({
        cluster_id: this.baseParams.cluster,
        namespace: this.baseParams.namespace,
        pod_name: pod.name,
      })
      .then(() => {
        this.auiNotificationService.success({
          content: this.translateService.get(
            'service_instance_restart_success',
          ),
        });
        this.detailDataService.refreshPolling();
      })
      .catch(e => this.errorsToastService.error(e));
  }

  onContainerSelect(pod: PodGroupItem, container: Container) {
    const dialogRef = this.dialogService.open(ExecCommandDialogComponent);
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (!!data) {
        const podName = get(pod, 'name', '');
        const containerName = get(container, 'name', '');
        this.terminalService.openTerminal({
          app: this.appName,
          ctl: this.podController ? this.podController.metadata.name : '',
          pods: podName,
          selectedPod: podName,
          container: containerName,
          namespace: this.baseParams.namespace,
          cluster: this.baseParams.cluster,
          user: data.user,
          command: data.command,
        });
      }
    });
  }

  canEXEC(pod: PodGroupItem, container: Container) {
    const allContainerStatus = get(pod, 'containerStatuses', null);
    const selectedContainerStatus = find(
      allContainerStatus,
      co => co.name === get(container, 'name', ''),
    );
    return !!get(selectedContainerStatus, 'state.running', null);
  }

  ngOnInit(): void {}
  constructor(
    private appService: AppService,
    private dialogService: DialogService,
    private translateService: TranslateService,
    private podService: PodService,
    private errorsToastService: ErrorsToastService,
    private terminalService: TerminalService,
    private cdr: ChangeDetectorRef,
    private detailDataService: DetailDataService,
    private auiNotificationService: NotificationService,
  ) {}
}
