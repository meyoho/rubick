import { Status, StatusType } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
  SimpleChanges,
} from '@angular/core';

import { TranslateService } from 'app/translate/translate.service';

export interface PodStatus {
  desired: number;
  current: number;
  status: string;
}

enum status {
  Stopped = StatusType.Info,
  Failed = StatusType.Error,
  Pending = StatusType.Primary,
  Running = StatusType.Success,
  Unknown = StatusType.Info,
}

@Component({
  selector: 'rc-pod-status',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PodStatusComponent implements OnChanges {
  @Input()
  podStatus: PodStatus;
  @Input()
  canUpdate: boolean;

  @Output()
  change = new EventEmitter<number>();

  status: Status[];
  tooltip: string;
  desired: number;

  constructor(private translate: TranslateService) {}

  ngOnChanges({ podStatus }: SimpleChanges) {
    if (podStatus && podStatus.currentValue) {
      this.onStatusChange();
    }
  }

  private onStatusChange() {
    this.desired = this.podStatus.desired;
    this.tooltip =
      this.translate.get('status_running') + ': ' + this.podStatus.current;
    if (!this.podStatus.desired && !this.podStatus.current) {
      this.status = [
        {
          scale: 1,
          type: StatusType.Info,
        },
      ];
      return;
    }
    if (this.podStatus.desired > this.podStatus.current) {
      this.status = [
        {
          scale: Number.parseFloat(
            (this.podStatus.current / this.podStatus.desired).toFixed(1),
          ),
          type: StatusType.Success,
        },
        {
          scale: Number.parseFloat(
            (
              (this.podStatus.desired - this.podStatus.current) /
              this.podStatus.desired
            ).toFixed(1),
          ),
          type: status[this.podStatus.status] || StatusType.Info,
        },
      ];
    } else {
      this.status = [
        {
          scale: 1,
          type: StatusType.Success,
        },
      ];
    }
  }

  decrease() {
    if (this.desired > 0) {
      --this.desired;
      this.change.emit(this.desired);
    }
  }

  increase() {
    ++this.desired;
    this.change.emit(this.desired);
  }
}
