import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  SimpleChanges,
} from '@angular/core';

@Component({
  selector: 'rc-k8s-resource-icon',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class K8sResourceIconComponent implements OnChanges {
  @Input()
  kind: string;

  ngOnChanges({ kind }: SimpleChanges) {
    if (kind && kind.currentValue) {
      this.kind = this.getResoureKind(this.kind.toLocaleLowerCase());
    }
  }

  private getResoureKind(resourceKind: string) {
    switch (resourceKind) {
      case 'deployment':
        return 'D';
      case 'deployments':
        return 'D';
      case 'statefulset':
        return 'SS';
      case 'statefulsets':
        return 'SS';
      case 'daemonset':
        return 'DS';
      case 'daemonsets':
        return 'DS';
      case 'container':
        return 'C';
      default:
        return '-';
    }
  }
}
