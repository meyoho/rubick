import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';

import { ApplicationCreateAddComponentPageComponent } from './create/add-component/component';
import { ApplicationCreateComponent } from './create/component';
import { ApplicationCreatePreviewPageComponent } from './create/preview/component';
import { ApplicationCreatePrimaryPageComponent } from './create/primary/component';
import { ApplicationCreateUpdateComponentPageComponent } from './create/update-component/component';
import { AppYamlPageComponent } from './create/yaml-page/component';
import { ApplicationHistoryDetailComponent } from './detail/history-detail/component';
import { ApplicationDetailPageComponent } from './detail/page.component';
import { ApplicationListPageComponent } from './list/component';
import { PodControllerDetailPageComponent } from './pod-controller/page.component';
import { ApplicationUpdateComponent } from './update/component';
import { ApplicationUpdatePreviewPageComponent } from './update/preview/component';

@NgModule({
  imports: [
    RouterModule.forChild([
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list',
      },
      {
        path: 'list',
        component: ApplicationListPageComponent,
      },
      {
        path: 'create',
        component: ApplicationCreateComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            component: ApplicationCreatePrimaryPageComponent,
          },
          {
            path: 'add-component',
            component: ApplicationCreateAddComponentPageComponent,
          },
          {
            path: 'update-component/:kind/:name',
            component: ApplicationCreateUpdateComponentPageComponent,
          },
          {
            path: 'preview',
            component: ApplicationCreatePreviewPageComponent,
          },
        ],
      },
      {
        path: 'update/:name',
        component: ApplicationUpdateComponent,
        children: [
          {
            path: '',
            pathMatch: 'full',
            redirectTo: 'preview',
          },
          {
            path: 'preview',
            component: ApplicationUpdatePreviewPageComponent,
          },
          {
            path: 'add-component',
            component: ApplicationCreateAddComponentPageComponent,
          },
          {
            path: 'update-component/:kind/:name',
            component: ApplicationCreateUpdateComponentPageComponent,
          },
        ],
      },
      {
        path: 'yaml',
        component: AppYamlPageComponent,
      },
      {
        path: 'detail/:app',
        component: ApplicationDetailPageComponent,
      },
      {
        path: 'version/:app/:name',
        component: ApplicationHistoryDetailComponent,
      },
      {
        path: 'detail/:app/:kind/:name',
        component: PodControllerDetailPageComponent,
      },
    ]),
  ],
  exports: [RouterModule],
})
export class ApplicationRoutingModule {}
