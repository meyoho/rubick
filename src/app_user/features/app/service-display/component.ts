import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';

import { get } from 'lodash-es';

import { adaptServiceResourceToFormModel } from 'app/features-shared/app/utils/service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import {
  ServiceFormModel,
  ServicePortFormModel,
  ServiceTypeDisplay,
} from 'app/typings';
import { Environments } from 'app/typings';
import { Service } from 'app/typings/raw-k8s';

@Component({
  selector: 'rc-service-display',
  templateUrl: './template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceDisplayComponent implements OnInit, OnChanges {
  @Input()
  service: Service;

  viewModel: ServiceFormModel;
  containerPortsOptions: any[] = [];

  ngOnChanges({ service }: SimpleChanges) {
    if (service && service.currentValue) {
      this.viewModel = this.adaptResource(this.service);
    }
  }

  private adaptResource(resource: Service) {
    if (!resource) {
      return {};
    }
    return adaptServiceResourceToFormModel({
      resource: resource,
      labelBaseDomain: this.env.label_base_domain,
    });
  }

  getName() {
    return get(this.viewModel, 'metadata.name', '');
  }

  getType() {
    return ServiceTypeDisplay[this.viewModel.type];
  }

  getAlbPort(port: ServicePortFormModel) {
    return port.albPort ? `${port.albPort}(${port.albProtocol})` : '-';
  }

  ngOnInit(): void {}
  constructor(@Inject(ENVIRONMENTS) private env: Environments) {}
}
