import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';

import { get, remove } from 'lodash-es';

import { PodControllerKinds } from 'app/features-shared/app/utils/pod-controller';
import {
  AppService,
  AppTopology,
  AppTopologyEdge,
} from 'app/services/api/app.service';
import { ApplicationStatus } from 'app/services/api/app.service';
import { KubernetesResource } from 'app/typings';
import { K8sResourceWithActions } from 'app/typings/backend-api';
import { Container } from 'app/typings/raw-k8s';
import { D3Service } from './d3.service';
import { Link, Node } from './models';
import { GraphComponent } from './visuals/graph/component';

@Component({
  selector: 'rc-app-topology',
  templateUrl: 'template.html',
  styleUrls: ['./style.scss'],
})
export class AppTopologyComponent implements OnInit, OnChanges {
  @ViewChild('topology')
  graphComopnent: GraphComponent;
  @Input()
  cluster: string;
  @Input()
  namespace: string;
  @Input()
  name: string;
  @Input()
  app: K8sResourceWithActions[];
  @Input()
  appStatus: ApplicationStatus;
  nodes: Node[] = [];
  links: Link[] = [];
  loadError: boolean;
  loading: boolean;
  initialized: boolean;

  constructor(public d3Service: D3Service, public appService: AppService) {}

  async ngOnInit() {
    // this.refresh();
  }

  ngOnChanges({ appStatus, cluster }: SimpleChanges) {
    if (appStatus && appStatus.currentValue && this.nodes) {
      this.updateNodesStatus();
    }
    if (cluster && cluster.currentValue && this.app) {
      this.refresh();
    }
    if (this.cluster && this.app && !this.initialized && !this.loading) {
      this.refresh();
    }
  }

  async refresh() {
    if (this.loading) {
      return;
    }
    this.loading = true;
    try {
      const topology = await this.getAppTopology();
      this.initNodesAndLinks(this.app, topology);
      if (this.appStatus) {
        this.updateNodesStatus();
      }
      this.initialized = true;
    } catch (_e) {
      console.log(_e);
      this.loadError = true;
    }
    this.loading = false;
  }

  initNodesAndLinks(app: K8sResourceWithActions[], topology: AppTopology) {
    this.nodes = this.getNodesFromResourcesAndTopology(app, topology.nodes);
    this.links = this.getLinksFromEdges(topology.edges, topology.nodes);
  }

  // 暂时没有用到，实时更新Nodes和Links
  updateNodesAndLinks(app: K8sResourceWithActions[], topology: AppTopology) {
    const nodes = this.getNodesFromResourcesAndTopology(app, topology.nodes);
    remove(this.nodes, (node: Node) => {
      return !Object.keys(topology.nodes).find(uuid => uuid === node.uuid);
    });
    nodes.forEach((_node: Node) => {
      if (!this.nodes.find((node: Node) => node.uuid === _node.uuid)) {
        this.nodes.push(_node);
      }
    });
    const links = this.getLinksFromEdges(topology.edges, topology.nodes);
    remove(this.links, (link: Link) => {
      return links.find((newLink: Link) => {
        return newLink.source !== link.source || newLink.target !== link.target;
      });
    });
    links.forEach((newLink: Link) => {
      if (
        !this.links.find((link: Link) => {
          return (
            newLink.source === link.source && newLink.target === link.target
          );
        })
      ) {
        this.links.push(newLink);
      }
    });
    this.nodes = [...this.nodes];
    this.links = [...this.links];
  }

  updateNodesStatus() {
    Object.keys(this.appStatus.workloads).forEach(key => {
      const node = this.nodes.find((node: Node) => {
        return `${node.kind}-${node.name}` === key.toLowerCase();
      });
      if (node) {
        node.updateStatus(this.appStatus.workloads[key]);
      }
    });
  }

  private getAppTopology() {
    return this.appService.getApplicationTopology({
      cluster: this.cluster,
      namespace: this.namespace,
      name: this.name,
    });
  }

  private getNodesFromResourcesAndTopology(
    app: K8sResourceWithActions[],
    nodes: {
      [key: string]: any;
    },
  ) {
    const resources = app.map((k: any) => {
      const data = k.kubernetes;
      return {
        name: data.metadata.name,
        kind: data.kind.toLowerCase(),
        kubernetes: data,
      };
    });
    return Object.values(nodes).map((resource: any) => {
      const uuid = resource.metadata.uid;
      const name = resource.metadata.name;
      const kind = resource.kind.toLowerCase();
      let isRef: boolean;
      const _resource = resources.find(
        (res: { name: string; kind: string }) => {
          return res.name === name && res.kind === kind;
        },
      );
      if (kind !== 'application') {
        isRef = !_resource;
      }
      const node = new Node({
        uuid,
        name,
        kind,
        isRef,
      });
      if (PodControllerKinds.includes(resource.kind)) {
        node.containers = get(
          _resource,
          'kubernetes.spec.template.spec.containers',
          [],
        ).map((container: Container) => {
          const image = container.image;
          const [, ...rest] = image.split('/');
          const cpu = get(container, 'resources.limits.cpu', '');
          const memory = get(container, 'resources.limits.memory', '');
          return {
            image: rest.join('/'),
            cpu,
            memory,
          };
        });
      }
      return node;
    });
  }

  private getLinksFromEdges(
    edges: AppTopologyEdge[],
    nodes: {
      [key: string]: KubernetesResource;
    },
  ): Link[] {
    return edges
      .filter((edge: AppTopologyEdge) => edge.from !== edge.to)
      .map((edge: AppTopologyEdge) => {
        const link = new Link(
          edge.from,
          edge.to,
          edge.type.toLowerCase() === 'reference',
        );
        const source = nodes[edge.from];
        const target = nodes[edge.to];
        if (
          source.kind.toLowerCase() === 'application' &&
          !PodControllerKinds.includes(target.kind)
        ) {
          // 1. 先找到 source是 application， target不是 PodController 的 edge
          // 2. 若还存在 (source是 PodController 而 target相同 的 edge) 或
          //  （target是 PodController 而 source相同 的 edge），则过滤掉(1)中这条 edge
          const existingEdge = edges.find((e: AppTopologyEdge) => {
            const s = nodes[e.from];
            const t = nodes[e.to];
            if (
              (PodControllerKinds.includes(s.kind) && e.to === edge.to) ||
              (PodControllerKinds.includes(t.kind) && e.from === edge.to)
            ) {
              return true;
            }
            return false;
          });
          if (existingEdge) {
            link.invisible = true;
          }
        }
        return link;
      });
  }
}
