import { EventEmitter } from '@angular/core';
import * as d3 from 'd3';

import { Link } from './link';
import { Node } from './node';

const FORCES = {
  LINKS: 0,
  REFERENCE_LINKS: 0,
  COLLISION: 1,
};

/**
 * d3-force + angular
 * 实现思路参考：https://medium.com/netscape/visualizing-data-with-angular-and-d3-209dde784aeb
 */

export class Graph {
  public ticker: EventEmitter<d3.Simulation<Node, Link>> = new EventEmitter();
  public simulation: d3.Simulation<any, any>;
  public nodes: Node[] = [];
  public links: Link[] = [];

  constructor(
    options: { width: number; height: number },
    nodes?: Node[],
    links?: Link[],
  ) {
    this.initSimulation(options);
    if (nodes && links) {
      this.nodes = nodes;
      this.links = links;
      this.initNodes();
      this.initLinks();
    }
  }

  updateNodesAndLinks(nodes: Node[], links: Link[]) {
    this.simulation.stop();
    this.nodes = nodes;
    this.links = links;
    this.initNodes();
    this.initLinks();
    this.simulation
      .alphaDecay(0.01)
      .alpha(1)
      .restart();
  }

  initSimulation(options: { width: number; height: number }) {
    if (!options || !options.width || !options.height) {
      throw new Error('missing options when initializing simulation');
    }

    /** Creating the simulation */
    if (!this.simulation) {
      const ticker = this.ticker;

      this.simulation = d3
        .forceSimulation()
        .force(
          'charge',
          d3.forceManyBody().strength((node: Node) => {
            if (node.kind === 'application') {
              return -200;
            } else {
              return -50;
            }
          }),
        )
        .force(
          'collide',
          d3
            .forceCollide()
            .strength(FORCES.COLLISION)
            .radius(32)
            .iterations(1),
        );
      this.simulation.on('tick', function() {
        ticker.emit(this);
      });
    }

    this.simulation.force(
      'centers',
      d3.forceCenter(options.width / 2, options.height / 2),
    );
    // this.simulation.restart();
  }

  private initNodes() {
    if (!this.simulation) {
      throw new Error('simulation was not initialized yet');
    }

    this.simulation.nodes(this.nodes);
  }

  private initLinks() {
    if (!this.simulation) {
      throw new Error('simulation was not initialized yet');
    }
    this.simulation.force(
      'links',
      d3
        .forceLink(this.links)
        .id((node: Node) => {
          return node.uuid;
        })
        .strength(1 / 100),
      // .distance(200),
    );
  }
}
