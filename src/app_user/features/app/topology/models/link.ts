import { Node } from './node';

type nodeRefType = Node | string | number;

export class Link implements d3.SimulationLinkDatum<Node> {
  // optional - defining optional implementation properties - required for relevant typing assistance
  index?: number;

  // must - defining enforced implementation properties
  source: nodeRefType;
  target: nodeRefType;
  isRef?: boolean;
  focused?: boolean;
  invisible?: boolean;

  constructor(source: nodeRefType, target: nodeRefType, isRef?: boolean) {
    this.source = source;
    this.target = target;
    this.isRef = isRef;
  }
}
