import { Directive, ElementRef, Input, OnInit } from '@angular/core';

import { D3Service } from '../d3.service';
import { Graph, Node } from '../models';

@Directive({
  selector: '[rcDraggableNode]',
})
export class DraggableDirective implements OnInit {
  @Input('rcDraggableNode')
  draggableNode: Node;
  //tslint:disable-next-line:no-input-rename
  @Input('rcDraggableInGraph')
  draggableInGraph: Graph;

  constructor(
    private topologyService: D3Service,
    private _element: ElementRef,
  ) {}

  ngOnInit() {
    this.topologyService.applyDraggableBehaviour(
      this._element.nativeElement,
      this.draggableNode,
      this.draggableInGraph,
    );
  }
}
