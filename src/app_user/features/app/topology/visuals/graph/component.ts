import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  HostListener,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { ElementRef, OnDestroy } from '@angular/core';

import { D3Service } from '../../d3.service';
import { Graph, Link, Node } from '../../models';

@Component({
  selector: 'rc-topology-graph',
  changeDetection: ChangeDetectionStrategy.OnPush,
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class GraphComponent
  implements OnInit, AfterViewInit, OnChanges, OnDestroy {
  @Input()
  nodes: Node[] = [];
  @Input()
  links: Link[] = [];
  graph: Graph;

  options: { width: number; height: number } = {
    width: 400,
    height: 300,
  };

  @HostListener('window:resize', ['$event'])
  onResize(_event: any) {
    this.graph.initSimulation(this.options);
  }

  constructor(
    private d3Service: D3Service,
    private cdr: ChangeDetectorRef,
    private element: ElementRef,
  ) {}

  ngOnInit() {
    this.graph = this.d3Service.getGraph(this.options);
    this.graph.ticker.subscribe(() => {
      this.cdr.markForCheck();
    });
    if (this.nodes && this.links) {
      this.graph.updateNodesAndLinks(this.nodes, this.links);
    }
  }

  ngAfterViewInit() {
    setTimeout(() => {
      const ele = this.element.nativeElement;
      this.options.width = parseInt(ele.offsetWidth, 10);
      this.options.height = parseInt(ele.offsetHeight, 10);
      this.graph.initSimulation(this.options);
      // console.log(ele, ele.offsetWidth, ele.offsetHeight);
    }, 50);
  }

  ngOnChanges({ nodes, links }: SimpleChanges) {
    if ((nodes || links) && this.graph && this.nodes && this.links) {
      this.graph.updateNodesAndLinks(this.nodes, this.links);
    }
  }

  ngOnDestroy() {
    this.graph.simulation.stop();
  }

  nodeFocused(node: Node) {
    this.graph.links.forEach((link: Link) => {
      if (link.source === node || link.target === node) {
        link.focused = true;
      }
    });
  }

  nodeBlured(node: Node) {
    this.graph.links.forEach((link: Link) => {
      if (link.source === node || link.target === node) {
        link.focused = false;
      }
    });
  }
}
