import { Component, Input, OnInit } from '@angular/core';

import { Node } from '../../models';

@Component({
  selector: 'rc-workload-status',
  templateUrl: 'template.html',
  styleUrls: ['./style.scss'],
})
export class WorkloadStatusComponent implements OnInit {
  @Input()
  node: Node;
  constructor() {}

  ngOnInit() {}
}
