import { Directive, ElementRef, Input, OnInit } from '@angular/core';

import { D3Service } from '../d3.service';

@Directive({
  selector: '[rcZoomableOf]',
})
export class ZoomableDirective implements OnInit {
  //tslint:disable-next-line:no-input-rename
  @Input('rcZoomableOf')
  zoomableOf: ElementRef;

  constructor(
    private topolopyService: D3Service,
    private _element: ElementRef,
  ) {}

  ngOnInit() {
    this.topolopyService.applyZoomableBehaviour(
      this.zoomableOf,
      this._element.nativeElement,
    );
  }
}
