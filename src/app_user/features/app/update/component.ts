import { MessageService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { take } from 'rxjs/operators';

import { TranslateService } from 'app/translate/translate.service';
import { WorkspaceComponent } from '../../workspace/workspace.component';
import { AppDataService } from '../app-data.service';

@Component({
  template: '<router-outlet></router-outlet>',
  providers: [AppDataService],
})
export class ApplicationUpdateComponent implements OnInit {
  constructor(
    private appDataService: AppDataService,
    private activatedRoute: ActivatedRoute,
    private workspaceComponent: WorkspaceComponent,
    private translate: TranslateService,
    private router: Router,
    private messageService: MessageService,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.appDataService.initFetchRequest(
      baseParams.cluster,
      baseParams.namespace,
      params.name,
    );
    this.appDataService.resourceNotFound$.pipe(take(1)).subscribe(res => {
      if (res) {
        this.messageService.error(this.translate.get('resource_not_exist'));
        return this.router.navigate(['app'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      }
    });
  }
}
