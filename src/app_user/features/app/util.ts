import { safeLoadAll } from 'js-yaml';
import { cloneDeep, get, uniq, unset } from 'lodash-es';

import {
  getDefaultContainerConfig,
  PodControllerKindEnum,
  PodControllerRollingUpdateType,
  PodControllerUpdateStrategyType,
} from 'app/features-shared/app/utils/pod-controller';
import { PodControllerKinds } from 'app/features-shared/app/utils/pod-controller';
import { getDefaultService } from 'app/features-shared/app/utils/service';
import {
  Application,
  ApplicationFormModel,
  KubernetesResource,
  PodController,
  Service,
} from 'app/typings';
import { ApplicationMetaDataFormModel } from 'app/typings/k8s-form-model';
import { RcImageSelection } from 'app/typings/k8s-form-model';
import { HorizontalPodAutoscaler, List } from 'app/typings/raw-k8s';

// 规则: 非[a-z0-9-]的替换为_，多个_合并为1个
// 防止xx?_?xx的形式，分为2次替换
// 1. 非a-z/0-9/-替换为- 2. 多个连续的-合并为- 3.去除开头和结尾的-
export function adaptRepoNameToContainerName(repoName: string) {
  return repoName
    .replace(/[^a-z0-9-]/g, '-') // 非法输入转为-
    .replace(/-+/g, '-') // 合并多个-
    .replace(/^-|-$/g, ''); // 去除前后缀 -
}

export function getDefaultPodController(
  imageSelectionParams: RcImageSelection,
  namespace: string,
) {
  const repositoryName = imageSelectionParams.repository_name;
  const fullImageName = imageSelectionParams.full_image_name;
  const imageTag = imageSelectionParams.tag;
  const name = adaptRepoNameToContainerName(repositoryName || '');
  const container = getDefaultContainerConfig({
    name,
    image: `${fullImageName}:${imageTag}`,
  });
  const podController: PodController = {
    apiVersion: 'extensions/v1beta1',
    kind: 'Deployment',
    metadata: {
      name,
      namespace,
    },
    spec: {
      replicas: 1,
      template: {
        spec: {
          containers: [container],
        },
      },
    },
  };
  return podController;
}

export function getBindingServicesByPodController(params: {
  services: Service[];
  podController: PodController;
  labelBaseDomain?: string;
}) {
  const labelBaseDomain = params.labelBaseDomain || 'alauda.io';
  const name = params.podController.metadata.name;
  const kind = params.podController.kind;
  return params.services.map((item: Service) => {
    const service = { ...item };
    if (!service.spec.selector) {
      service.spec.selector = {};
    }
    service.spec.selector[
      `service.${labelBaseDomain}/name`
    ] = `${kind.toLowerCase()}-${name}`;
    return service;
  });
}

export function getDefaultServiceFromImagePorts(
  params: {
    name: string;
    ports?: number[];
    namespace: string;
  } = {
    name: '',
    ports: [],
    namespace: '',
  },
): Service {
  if (params.ports && params.ports.length) {
    const service = getDefaultService();
    service.metadata.name = params.name;
    service.metadata.namespace = params.namespace;
    service.spec.ports = uniq(params.ports).map((port: number) =>
      getServicePort(port),
    );
    return service;
  } else {
    return null;
  }
}

function getServicePort(port: number) {
  return {
    name: `${port}-${port}`,
    port,
    protocol: 'TCP',
    targetPort: port,
  };
}

export function checkImageSelectionParam(params: RcImageSelection) {
  return (
    params.full_image_name && params.repository_name && params.registry_endpoint
  );
}

export function getPodControllerUpdateStrategy(
  rawK8s: PodController,
): {
  updateStrategy: PodControllerUpdateStrategyType;
  rollingUpdateStrategy: string;
} {
  if (!rawK8s) {
    return {
      updateStrategy: null,
      rollingUpdateStrategy: null,
    };
  }
  return {
    updateStrategy:
      rawK8s.kind === PodControllerKindEnum.Deployment
        ? get(rawK8s, 'spec.strategy.type')
        : get(rawK8s, 'spec.updateStrategy.type'),
    rollingUpdateStrategy: getRollingUpdateStrategy(
      rawK8s.kind === PodControllerKindEnum.Deployment
        ? get(rawK8s, 'spec.strategy.rollingUpdate', null)
        : get(rawK8s, 'spec.updateStrategy.rollingUpdate', null),
    ),
  };
}

function getRollingUpdateStrategy(
  rollingUpdate: PodControllerRollingUpdateType,
): string {
  if (!rollingUpdate) {
    return null;
  }
  return Object.keys(rollingUpdate).reduce((accu: string, key: string) => {
    return accu.concat(`${key}: ${rollingUpdate[key]} `);
  }, '');
}

export function removeDirtyDataBeforeUpdate(resource: KubernetesResource) {
  const newResource = cloneDeep(resource);
  unset(newResource, 'status');
  unset(newResource, 'metadata.resourceVersion');
  return newResource;
}

export function getResourcesFromYaml(yaml: string, namespace: string) {
  let kubernetes = safeLoadAll(yaml)
    .map(item => (item === 'undefined' ? undefined : item))
    .filter(item => !!item);
  if (!kubernetes || typeof kubernetes === 'string') {
    kubernetes = [];
  }
  kubernetes.forEach((resource: KubernetesResource | List) => {
    if (resource.kind === 'List') {
      (resource as List).items.forEach((_resource: KubernetesResource) => {
        updateResourceNamespace(_resource, namespace);
      });
    } else {
      updateResourceNamespace(resource, namespace);
    }
  });
  return kubernetes;
}

function updateResourceNamespace(
  resource: KubernetesResource,
  namespace: string,
) {
  if (!resource.metadata) {
    resource.metadata = {};
  }
  resource.metadata.namespace = namespace;
}

export function generateApplicationModel(
  kubernetes: KubernetesResource[],
  labelBaseDomain: string,
): ApplicationFormModel {
  const componentEntities: any = {};
  const resources = kubernetes.filter((item: KubernetesResource) => {
    const kinds = [
      'Application',
      'Service',
      'HorizontalPodAutoscaler',
      ...PodControllerKinds,
    ];
    return !kinds.includes(item.kind);
  });
  const services = kubernetes.filter((item: KubernetesResource) => {
    return item.kind === 'Service';
  }) as Service[];
  kubernetes
    .filter((resource: KubernetesResource) => {
      return PodControllerKinds.includes(resource.kind);
    })
    .forEach((resource: KubernetesResource) => {
      const key = `${resource.kind}-${resource.metadata.name}`;
      componentEntities[key] = {
        controller: {
          ...resource,
        },
      };
    });
  kubernetes
    .filter((item: KubernetesResource) => {
      return item.kind === 'HorizontalPodAutoscaler';
    })
    .forEach((resource: KubernetesResource) => {
      const targetName = (resource as HorizontalPodAutoscaler).spec
        .scaleTargetRef.name;
      const targetKind = (resource as HorizontalPodAutoscaler).spec
        .scaleTargetRef.kind;
      const key = `${targetKind}-${targetName}`;
      if (componentEntities[key]) {
        componentEntities[key]['autoscaler'] = resource;
      }
    });
  const formModel = {
    components: Object.values(componentEntities),
    services,
    resources,
  };
  const crd = kubernetes.find((item: KubernetesResource) => {
    return item.kind === 'Application';
  }) as Application;
  if (crd) {
    const metadata: ApplicationMetaDataFormModel = {
      name: '',
      namespace: '',
      displayName: '',
    };
    const annotations = get(crd, 'metadata.annotations', {});
    metadata.name = crd.metadata.name;
    metadata.namespace = crd.metadata.namespace;
    metadata.displayName =
      annotations[`app.${labelBaseDomain}/display-name`] || '';
    formModel['crd'] = crd;
    formModel['metadata'] = metadata;
  }

  return formModel;
}
