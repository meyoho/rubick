import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ArtifactRepositoriesRoutingModule } from 'app_user/features/artifact-repositories/artifact-repositories-routing.module';
import { ImageTagListComponent } from 'app_user/features/artifact-repositories/image-tag-list/image-tag-list.component';
import { ListPageComponent } from 'app_user/features/artifact-repositories/list-page/list-page.component';
import { RepositoryListComponent } from 'app_user/features/artifact-repositories/repository-list/repository-list.component';
import { ScanStatusComponent } from 'app_user/features/artifact-repositories/scan-status/scan-status.component';
import { TagIconComponent } from 'app_user/features/artifact-repositories/tag-icon/tag-icon.component';
import { TagListPageComponent } from 'app_user/features/artifact-repositories/tag-list-page/tag-list-page.component';
import { TagListSectionComponent } from 'app_user/features/artifact-repositories/tag-list-section/tag-list-section.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule,
    ArtifactRepositoriesRoutingModule,
  ],
  declarations: [
    ListPageComponent,
    TagListPageComponent,
    RepositoryListComponent,
    TagIconComponent,
    TagListSectionComponent,
    ImageTagListComponent,
    ScanStatusComponent,
  ],
})
export class ArtifactRepositoriesModule {}
