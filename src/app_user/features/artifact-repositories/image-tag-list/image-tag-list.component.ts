import { MessageService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  Output,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { ImageTag } from 'app/services/api/pipeline';
import { RegistryApiService } from 'app/services/api/registry/registry-api.service';
import { AppState, getCurrentSpace } from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { combineLatest } from 'rxjs';
import { filter, first, map, publishReplay, refCount } from 'rxjs/operators';

@Component({
  selector: 'rc-iamge-tag-list',
  templateUrl: 'image-tag-list.component.html',
  styleUrls: ['image-tag-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ImageTagListComponent {
  @Input()
  tags: ImageTag[] = [];
  @Input()
  type: string;
  @Input()
  repository: string;
  @Input()
  imagePath: string;
  @Output()
  scanTrigger = new EventEmitter<ImageTag>();

  project$ = this.getProject();
  space$ = this.store.select(getCurrentSpace).pipe(filter(s => !!s));

  constructor(
    private registryApi: RegistryApiService,
    private message: MessageService,
    private notifaction: NotificationService,
    private translate: TranslateService,
    private workspaceComponent: WorkspaceComponent,
    private store: Store<AppState>,
  ) {}

  private getProject() {
    return this.workspaceComponent.baseParams.pipe(
      map(params => params.project),
      publishReplay(1),
      refCount(),
    );
  }

  trackByName(_: number, item: ImageTag) {
    return item.name;
  }

  getRowDef() {
    return this.type === 'Harbor'
      ? ['name', 'vulnerability', 'size', 'digest', 'action']
      : ['name', 'digest'];
  }

  scan(tag: ImageTag) {
    combineLatest(this.project$, this.space$)
      .pipe(first())
      .subscribe(([project, space]) => {
        this.registryApi
          .triggerSecurityScan(
            project,
            space.name || '',
            this.repository,
            tag.name,
          )
          .subscribe(
            () => {
              this.message.success(this.translate.get('devops_scan_started'));
              this.scanTrigger.emit(tag);
            },
            (error: any) => {
              this.notifaction.error({
                title: this.translate.get('devops_scan_start_failed'),
                content: error.message || error.error,
              });
            },
          );
      });
  }

  getFullTagPath(item: ImageTag) {
    return `${this.imagePath}:${item.name}`;
  }
}
