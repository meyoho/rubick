import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { Store } from '@ngrx/store';
import { RegistryApiService } from 'app/services/api/registry/registry-api.service';
import { AppState, getCurrentSpace } from 'app/store';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { BehaviorSubject, NEVER, combineLatest } from 'rxjs';
import {
  catchError,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent {
  loading = false;
  showZeroState = false;
  pagination = {
    count: 0,
    current: 1,
    size: 20,
  };
  project$ = this.getProject();
  space$ = this.store.select(getCurrentSpace).pipe(filter(s => !!s));

  repositoriesFilter$$ = new BehaviorSubject<string>('');
  refetch$$ = new BehaviorSubject(null);

  repositories$ = combineLatest(
    this.project$,
    this.space$,
    this.refetch$$,
  ).pipe(
    tap(() => {
      this.loading = true;
    }),
    switchMap(([project, space]) =>
      this.registryApi
        .findRepositoriesByProjectAndSpace(project, space.name || '', {
          page: this.pagination.current,
          page_size: this.pagination.size,
        })
        .pipe(
          catchError(() => {
            this.loading = false;
            this.cdr.markForCheck();
            return NEVER;
          }),
        ),
    ),
    tap(data => {
      this.pagination.count = data.total;
      this.showZeroState = data.total === 0;
      this.loading = false;
    }),
    map(list => list.items),
    publishReplay(1),
    refCount(),
  );

  filteredRepositories$ = combineLatest(
    this.repositories$,
    this.repositoriesFilter$$,
  ).pipe(
    map(([items, keyword]) =>
      items.filter(item => item.image.includes(keyword)),
    ),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private registryApi: RegistryApiService,
    private cdr: ChangeDetectorRef,
    private workspaceComponent: WorkspaceComponent,
    private store: Store<AppState>,
  ) {}

  private getProject() {
    return this.workspaceComponent.baseParams.pipe(
      map(params => params.project),
      publishReplay(1),
      refCount(),
    );
  }

  pageSizeChange(pageSize: number) {
    this.pagination.size = pageSize;
    this.pagination.current = 1;
    this.refetch$$.next(null);
  }

  pageChanged(num: number) {
    this.pagination.current = num;
    this.refetch$$.next(null);
  }
}
