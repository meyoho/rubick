import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

import { ImageRepository } from 'app/services/api/registry/registry-api.types';

@Component({
  selector: 'rc-repository-list',
  templateUrl: 'repository-list.component.html',
  styleUrls: ['repository-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RepositoryListComponent {
  private _repositories: ImageRepository[] = [];
  @Input()
  get repositories() {
    return this._repositories;
  }
  set repositories(val) {
    if (!val) {
      return;
    }
    this._repositories = this.sortData(val);
  }

  sortData(data: ImageRepository[]) {
    return [...data].sort((a, b) => {
      const stringA = a.creationTimestamp;
      const stringB = b.creationTimestamp;
      return stringB.localeCompare(stringA);
    });
  }

  trackByPath(_: number, item: ImageRepository) {
    return `${item.endpoint}/${item.name}`;
  }
}
