import { ChangeDetectionStrategy, Component, Input } from '@angular/core';

@Component({
  selector: 'rc-tag-icon',
  templateUrl: 'tag-icon.component.html',
  styleUrls: ['tag-icon.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagIconComponent {
  @Input()
  type: string;
  @Input()
  size: 'small' | 'large' = 'small';

  getClass() {
    return `rc-tag-icon rc-tag-icon--${
      this.size
    } rc-tag-icon--${this.type.toLowerCase()}`;
  }
}
