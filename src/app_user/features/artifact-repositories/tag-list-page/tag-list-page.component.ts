import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Store } from '@ngrx/store';

import {
  BehaviorSubject,
  combineLatest,
  interval,
  merge,
  NEVER,
  of,
} from 'rxjs';
import {
  catchError,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  switchMapTo,
  tap,
} from 'rxjs/operators';

import { RegistryApiService } from 'app/services/api/registry/registry-api.service';
import { AppState, getCurrentSpace } from 'app/store';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: 'tag-list-page.component.html',
  styleUrls: ['tag-list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TagListPageComponent {
  loading = false;
  error: any;
  project$ = this.getProject();
  space$ = this.store.select(getCurrentSpace).pipe(filter(s => !!s));

  repoName$ = this.activatedRoute.paramMap.pipe(
    map(param => param.get('name')),
  );

  keyword$$ = new BehaviorSubject<string>('');
  updated$$ = new BehaviorSubject<void>(null);

  repo$ = combineLatest(this.project$, this.space$, this.repoName$).pipe(
    tap(() => {
      this.loading = true;
      this.error = null;
    }),
    switchMap(([project, space, name]) =>
      this.registryApi.getRepository(project, space.name || '', name).pipe(
        catchError(err => {
          this.loading = false;
          this.error = err;
          this.cdr.markForCheck();
          return NEVER;
        }),
      ),
    ),
    tap(() => {
      this.loading = false;
    }),
    publishReplay(1),
    refCount(),
  );

  tags$ = combineLatest(
    this.project$,
    this.space$,
    this.repoName$,
    this.keyword$$,
    this.updated$$.pipe(switchMapTo(merge(of(null), interval(10000)))),
  ).pipe(
    map(([project, space, name, keyword]) => {
      return this.registryApi
        .getTags(project, space.name || '', name)
        .pipe(map(tags => tags.filter(tag => tag.name.includes(keyword))));
    }),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private registryApi: RegistryApiService,
    private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private workspaceComponent: WorkspaceComponent,
    private store: Store<AppState>,
  ) {}

  private getProject() {
    return this.workspaceComponent.baseParams.pipe(
      map(params => params.project),
      publishReplay(1),
      refCount(),
    );
  }
}
