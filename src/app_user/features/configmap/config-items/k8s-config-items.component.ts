import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfigmapService } from 'app/services/api/configmap.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ConfigMap, K8sResourceWithActions } from 'app/typings';
import { cloneDeep } from 'lodash-es';

import { ConfigmapUtilService } from '../util.service';

@Component({
  selector: 'rc-k8s-config-items',
  templateUrl: './k8s-config-items.component.html',
  styleUrls: ['./k8s-config-items.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class K8sConfigItemsComponent
  implements OnInit, OnDestroy, AfterViewInit {
  @Input()
  set _configmapData(_configmapData: K8sResourceWithActions<ConfigMap>) {
    this.configmapData = _configmapData;
    this.handleData(_configmapData);
  }
  @Input()
  params: {
    clusterId: string;
    namespace: string;
    name: string;
  };
  @Input()
  clusterId: string;
  @Output()
  actionComplete = new EventEmitter<void>();

  @ViewChild(NgForm)
  form: NgForm;
  dialogRef: DialogRef<TemplateRef<any>>;

  configmapData: K8sResourceWithActions<ConfigMap>;
  loading: boolean;
  deleteKey = '';
  referenced_list: any[] = [];
  keyValueList: any[] = [];
  keyValueListOrigin: any[] = [];
  columns = ['key', 'value', 'action'];

  submitting = false;

  get canUpdate(): boolean {
    return this.configmapUtilities.showUpdate(this.configmapData);
  }

  constructor(
    private configmapUtilities: ConfigmapUtilService,
    private dialogService: DialogService,
    private errorsToastService: ErrorsToastService,
    private configmapService: ConfigmapService,
    private errorToastService: ErrorsToastService,
  ) {}

  ngOnInit() {
    if (this.params) {
      this.clusterId = this.clusterId || this.params.clusterId;
      this.refetch();
    }
  }

  ngAfterViewInit() {}

  ngOnDestroy() {}

  trackByFn(_index: number, item: any) {
    return item.key;
  }

  async refetch() {
    this.loading = true;
    try {
      this.configmapData = await this.configmapService.getK8sConfigmap(
        this.params,
      );
      this.handleData(this.configmapData);
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
    this.loading = false;
  }

  handleData(rowData: K8sResourceWithActions<ConfigMap>) {
    this.keyValueList = [];
    let data;
    if (rowData) {
      if (!rowData.kubernetes.data) {
        rowData.kubernetes.data = {};
      }
      data = Object.entries(rowData.kubernetes.data);
      for (const [key, value] of data) {
        this.keyValueList.push({
          key,
          value,
        });
      }
      this.keyValueListOrigin = cloneDeep(this.keyValueList);
    }
  }

  updateConfig(
    data: { key: string; value: string },
    templateRef: TemplateRef<any>,
  ) {
    this.dialogRef = this.dialogService.open(templateRef, {
      size: DialogSize.Large,
      data,
    });
  }

  showReferencedList(list: any) {
    this.referenced_list = list || [];
  }

  deleteConfigmapKey(rowData: any) {
    this.deleteKey = rowData.key;
    this.deleteConfigmapKeyAction();
  }

  async deleteConfigmapKeyAction() {
    const configmapDataCopy = cloneDeep(this.configmapData);
    delete configmapDataCopy.kubernetes.data[this.deleteKey];
    try {
      await this.configmapUtilities.deleteConfigmapKey(
        this.clusterId || this.params.clusterId,
        configmapDataCopy,
        this.deleteKey,
      );
      if (this.params) {
        this.refetch();
      } else {
        this.actionComplete.next();
      }
    } catch (errors) {
      this.errorsToastService.error(errors);
    }
  }

  onKeywordChange(queryString: string) {
    this.keyValueList = this.keyValueListOrigin.filter(item => {
      return new RegExp(queryString, 'i').test(item.key);
    });
  }

  confirm(key: string, newVal: string) {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    const payload = cloneDeep(this.configmapData);
    payload.kubernetes.data[key] = newVal;
    this.configmapService
      .updateConfigmap(this.clusterId || this.params.clusterId, payload)
      .then(() => {
        this.submitting = false;
        this.dialogRef.close();
        if (this.params) {
          this.refetch();
        } else {
          this.actionComplete.next();
        }
      })
      .catch(err => {
        this.errorToastService.error(err);
        this.submitting = false;
      });
  }

  cancel() {
    this.dialogRef.close();
  }
}
