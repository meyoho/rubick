import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import jsyaml from 'js-yaml';
import { get } from 'lodash-es';
import { Subject } from 'rxjs';
import { first, map, takeUntil } from 'rxjs/operators';

import {
  ConfigmapService,
  MirrorConfigmap,
} from 'app/services/api/configmap.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { ConfigMap, Environments, K8sResourceWithActions } from 'app/typings';
import { viewActions } from 'app/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import { AddConfigDialogComponent } from '../dialog/add-config-dialog.component';
import { ConfigmapUtilService } from '../util.service';

@Component({
  templateUrl: './configmap-detail.component.html',
  styleUrls: ['./configmap-detail.component.scss'],
})
export class ConfigmapDetailComponent implements OnInit {
  private _loading: boolean;
  private _loadError: any;
  private projectName: string;
  private namespaceName: string;
  private name: string;
  private onDestroy$ = new Subject<void>();
  configmapData: K8sResourceWithActions<ConfigMap>;
  clusterName: string;
  configmapYAML: string;
  mirrorConfigmaps: MirrorConfigmap[];
  originMirrorConfigmaps: MirrorConfigmap[];
  clusterDisplayName: string;
  edtiorActions = viewActions;
  editorOptions = {
    language: 'yaml',
    readOnly: true,
    renderLineHighlight: 'none',
  };
  initialized = false;

  constructor(
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private configmapService: ConfigmapService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    public configmapUtilities: ConfigmapUtilService,
    private router: Router,
    private dialogService: DialogService,
    private workspaceComponent: WorkspaceComponent,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}

  async ngOnInit() {
    this.workspaceComponent.baseParams
      .pipe(
        takeUntil(this.onDestroy$),
        map((params: Params) => {
          return [params['project'], params['cluster'], params['namespace']];
        }),
      )
      .subscribe(async ([projectName, clusterName, namespaceName]) => {
        this.projectName = projectName;
        this.clusterName = clusterName;
        this.namespaceName = namespaceName;
        const params = this.route.snapshot.params;
        this.name = params['name'];
        await this.refetch();
        this.initialized = true;
      });
  }

  back() {
    this.router.navigate(['configmap'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async refetch() {
    this._loading = true;
    try {
      this.configmapData = await this.configmapService.getK8sConfigmap({
        clusterId: this.clusterName,
        namespace: this.namespaceName,
        name: this.name,
      });
      await this.getMirrorConfigmaps();
      this.configmapYAML = this.formToYaml(this.configmapData);
      this._loadError = null;
    } catch ({ status, errors }) {
      this._loadError = errors;
      if (status === 403) {
        // TODO: back to pervious state or parent state
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
        return this.router.navigate(['configmap'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      }
    }
    this._loading = false;
  }

  updateConfigmap() {
    this.router.navigate(['configmap', 'update', this.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  addConfig() {
    const dialogRef = this.dialogService.open(AddConfigDialogComponent, {
      size: DialogSize.Large,
      data: {
        configmapData: this.configmapData,
        baseParams: {
          cluster: this.clusterName,
          namespace: this.namespaceName,
          name: this.name,
        },
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (!!data) {
        this.refetch();
      }
    });
  }

  get empty() {
    return !this.configmapData;
  }

  get keyEmpty() {
    if (this.configmapData) {
      return !this.configmapData.hasOwnProperty('referenced_by');
    }
    return false;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  get description() {
    return get(
      this.configmapData,
      `kubernetes.metadata.annotations['resource.${
        this.env.label_base_domain
      }/description']`,
      '',
    );
  }

  handleData() {
    let data: any;
    if (this.configmapData) {
      data = Object.entries(this.configmapData.kubernetes.data);
      data = data.sort((kvArr: any, kvArr_next: any) => {
        if (kvArr[0] < kvArr_next[0]) {
          return -1;
        }
        if (kvArr[0] > kvArr_next[0]) {
          return 1;
        }
        return 0;
      });
    }
    return data;
  }

  private formToYaml(formModel: any) {
    try {
      const json = JSON.parse(JSON.stringify(formModel.kubernetes));
      const yaml = jsyaml.safeDump(json, { sortKeys: true });
      return yaml;
    } catch (err) {}
  }

  async deleteConfigmap() {
    if (this.originMirrorConfigmaps.length) {
      const requireSelect = this.originMirrorConfigmaps.filter((item: any) => {
        return item.uuid === this.configmapData.kubernetes.metadata.uid;
      });
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: this.originMirrorConfigmaps,
        confirmTitle: this.translateService.get('delete_configmap'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'configmap_service_delete_selected_cluster_configmap_confirm',
          {
            configmap_name: this.configmapData.kubernetes.metadata.name,
          },
        ),
      };
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorConfigmap[]) => {
          modalRef.close();
          if (res) {
            try {
              this.configmapUtilities.deleteMirrorConfigmap(
                res,
                this.configmapData,
              );
              return this.router.navigate(['configmap'], {
                relativeTo: this.workspaceComponent.baseActivatedRoute,
              });
            } catch (err) {
              this.errorsToastService.error(err);
            }
          }
        });
    } else {
      try {
        await this.configmapUtilities.deleteConfigmap(
          this.clusterName,
          this.configmapData,
        );
        return this.router.navigate(['configmap'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      } catch (err) {
        this.errorsToastService.error(err);
      }
    }
  }

  async doRefetch() {
    await this.refetch();
  }

  async getMirrorConfigmaps() {
    const mirrorConfigmaps = await this.configmapService.getMirrorConfigmaps(
      this.clusterName,
      this.namespaceName,
      this.name,
    );
    this.originMirrorConfigmaps = mirrorConfigmaps;
    this.mirrorConfigmaps = mirrorConfigmaps.filter(
      (configmap: MirrorConfigmap) => {
        return configmap.cluster_name !== this.clusterName;
      },
    );
    const currentCluster = mirrorConfigmaps.find(
      (configmap: MirrorConfigmap) => {
        return configmap.cluster_name === this.clusterName;
      },
    );
    this.clusterDisplayName = currentCluster
      ? currentCluster.cluster_display_name
      : this.clusterName;
  }

  redirectToMirrorConfigmap(configmap: MirrorConfigmap) {
    this.router.navigate([
      '/console/user/workspace',
      {
        project: this.projectName,
        cluster: configmap.cluster_name,
        namespace: this.namespaceName,
      },
      'configmap',
      'detail',
      configmap.name,
    ]);
    this.configmapData = null;
  }
}
