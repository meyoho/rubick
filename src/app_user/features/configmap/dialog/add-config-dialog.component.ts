import { DIALOG_DATA } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { ConfigmapService } from 'app/services/api/configmap.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';

import { ConfigMap, K8sResourceWithActions } from 'app/typings';
import { K8S_CONFIGMAP_KEY } from 'app/utils/patterns';
import { get } from 'lodash-es';

@Component({
  selector: 'rc-add-config-dialog',
  templateUrl: './add-config-dialog.component.html',
})
export class AddConfigDialogComponent implements OnInit {
  @ViewChild(NgForm)
  form: NgForm;
  close = new EventEmitter();
  formModel = {
    key: '',
    value: '',
  };
  keyReg = K8S_CONFIGMAP_KEY;
  submitting = false;
  cluster: string;
  namespace: string;

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    if (!this.data.configmapData.kubernetes.data) {
      this.data.configmapData.kubernetes.data = {};
    }
    this.data.configmapData.kubernetes.data[
      this.formModel.key
    ] = this.formModel.value;
    this.configmapService
      .updateConfigmap(this.cluster, this.data.configmapData)
      .then(() => {
        this.submitting = false;
        this.close.next(true);
      })
      .catch(err => {
        this.errorToastService.error(err);
        this.submitting = false;
      });
  }

  cancel() {
    this.close.next();
  }

  ngOnInit(): void {}
  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      baseParams: {
        cluster: string;
        namespace: string;
        name: string;
      };
      configmapData: K8sResourceWithActions<ConfigMap>;
    },
    private configmapService: ConfigmapService,
    private errorToastService: ErrorsToastService,
  ) {
    this.cluster = get(this.data, 'baseParams.cluster', '');
    this.namespace = get(this.data, 'baseParams.namespace', '');
  }
}
