import { DialogService, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  Component,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { ConfigmapService } from 'app/services/api/configmap.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import {
  Cluster,
  ClusterSelect,
  RegionService,
} from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { ConfigMap, Environments, K8sResourceWithActions } from 'app/typings';
import { updateActions } from 'app/utils/code-editor-config';
import { BatchReponse } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import * as jsyaml from 'js-yaml';
import { merge } from 'lodash-es';

import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';

@Component({
  selector: 'rc-configmap-form',
  templateUrl: './k8s-configmap-form.component.html',
})
export class K8sConfigmapFormComponent implements OnInit, OnDestroy {
  private namespaceName: string;
  private clusterName: string;
  @Input()
  data: K8sResourceWithActions<ConfigMap>;
  formModel = 'UI';
  submitting = false;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };
  initialized = false;

  configMap: K8sResourceWithActions<ConfigMap>;

  mirrorClusters: ClusterSelect[] = [];
  clusters: string[];
  configmapYaml = '';
  configmapYamlOrigin = '';
  editorActionsConfig = updateActions;
  currentCluster: string;
  RESOURCE_TYPE = 'configmaps';

  @ViewChild('configmapForm')
  form: NgForm;

  configmapMirrorResource: BatchReponse;

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    private configmapService: ConfigmapService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private router: Router,
    private location: Location,
    private errorsToast: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    this.configMap = {
      kubernetes: this.genDefaultConfigmap(),
      resource_actions: [],
    };
  }

  async ngOnInit() {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = baseParams['cluster'];
    this.namespaceName = baseParams['namespace'];
    this.getMirrorClusters()
      .then(() => {
        if (!this.data) {
          return;
        } else {
          merge(this.configMap.kubernetes, this.data.kubernetes);
          // 联邦集群更新需要删除下面两个属性
          delete this.configMap.kubernetes.metadata.uid;
          delete this.configMap.kubernetes.metadata.resourceVersion;
          // 获取该资源名称在联邦集群的所有资源
          // 更新时可选联邦，应该是存在该资源名称的集群的集合
          return this.getMirrorResource(
            this.configMap.kubernetes.metadata.name,
          ).then(() => {
            const hasResourceClusters: string[] = [];
            Object.entries(this.configmapMirrorResource.responses).map(item => {
              if (item[1] && item[1].code < 300) {
                hasResourceClusters.push(item[0]);
              }
            });
            this.mirrorClusters = this.mirrorClusters.filter(
              (item: ClusterSelect) => {
                return hasResourceClusters.includes(item.name);
              },
            );
            this.clusters = this.clusters.filter(item => {
              return hasResourceClusters.includes(item);
            });
            return;
          });
        }
      })
      .finally(() => {
        this.initialized = true;
      });
  }

  genDefaultConfigmap() {
    return {
      apiVersion: 'v1',
      kind: 'ConfigMap',
      data: {},
      metadata: {
        name: '',
        annotations: {
          [`resource.${this.env.label_base_domain}/description`]: '',
        },
      },
    } as ConfigMap;
  }

  ngOnDestroy() {}

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        try {
          this.formatConfigmap();
          this.configmapYamlOrigin = this.configmapYaml = this.formToYaml(
            this.configMap.kubernetes,
          );
        } catch (e) {
          this.auiNotificationService.error(e.message);
        } finally {
          break;
        }
      case 'UI':
        this.configMap.kubernetes = this.yamlToForm(this.configmapYaml);
        break;
    }
  }

  private yamlToForm(yaml: string) {
    let formModel: any;
    try {
      formModel = jsyaml.safeLoad(yaml);
    } catch (e) {
      this.auiNotificationService.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return merge(this.genDefaultConfigmap(), formModel);
  }

  private formToYaml(formModel: any) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  private formatConfigmap() {
    this.configMap.kubernetes.metadata.namespace = this.namespaceName;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    try {
      if (this.formModel === 'YAML') {
        this.configMap.kubernetes = this.yamlToForm(this.configmapYaml);
      }
    } catch (err) {
      this.errorsToast.error(err);
      return;
    }

    try {
      this.formatConfigmap();
    } catch (e) {
      return this.auiNotificationService.error(e.message);
    }

    try {
      let comfirmTitle, comfirmContent;
      if (this.data) {
        comfirmTitle = 'update';
        comfirmContent = 'configmap_service_update_configmap_confirm';
      } else {
        comfirmTitle = 'create';
        comfirmContent = 'configmap_service_create_configmap_confirm';
      }
      await this.dialogService.confirm({
        title: this.translate.get(comfirmTitle),
        content: this.translate.get(comfirmContent, {
          configmap_name: this.configMap.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.submitting = true;

    try {
      if (this.data) {
        await this.configmapService.updateK8sConfigmap(
          this.clusterName,
          this.configMap,
          this.clusters,
        );
      } else {
        await this.configmapService.createK8sConfigmap(
          this.clusterName,
          this.configMap.kubernetes,
          this.clusters,
        );
      }
      this.router.navigate(
        ['configmap', 'detail', this.configMap.kubernetes.metadata.name],
        {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        },
      );
    } catch (e) {
      this.errorsToast.error(e);
    }

    this.submitting = false;
  }

  cancel() {
    this.location.back();
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror && cluster.mirror.regions) {
      this.mirrorClusters = cluster.mirror.regions || [];
      this.currentCluster = this.mirrorClusters.find(item => {
        return item.name === this.clusterName;
      }).name;
      this.clusters = this.mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
    } else {
      this.clusters = [this.clusterName];
    }
  }

  private async getMirrorResource(name: string) {
    try {
      this.configmapMirrorResource = await this.k8sResourceService.getMirrorResources(
        this.clusters,
        this.namespaceName,
        this.RESOURCE_TYPE,
        name,
      );
      await this.k8sResourceService.currentClusterResponseStatus(
        this.configmapMirrorResource,
        this.clusterName,
      );
    } catch (err) {
      this.errorsToast.error(err);
      this.router.navigate(['configmap'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
