import { DialogService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, Subject } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import {
  ConfigmapService,
  MirrorConfigmap,
} from 'app/services/api/configmap.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { HttpService } from 'app/services/http.service';
import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import {
  ConfigMap,
  Environments,
  K8sResourceWithActions,
  WorkspaceBaseParams,
} from 'app/typings';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import { ConfigmapUtilService } from '../util.service';

@Component({
  templateUrl: './configmap-list.component.html',
  styleUrls: ['./configmap-list.component.scss'],
  providers: [ResourceListPermissionsService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigmapListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  configmapCreateEnabled: boolean;
  initialized: boolean;

  columns = ['name', 'description', 'action'];
  private onDestroy$ = new Subject<any>();
  private baseParams: WorkspaceBaseParams;

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private translateService: TranslateService,
    public configmapUtilities: ConfigmapUtilService,
    private errorsToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private httpService: HttpService,
    private dialogService: DialogService,
    private configmapService: ConfigmapService,
    private workspaceComponent: WorkspaceComponent,
    public resourcePermissionService: ResourceListPermissionsService,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.resourcePermissionService.initContext({
      context: {
        project_name: this.baseParams.project,
        cluster_name: this.baseParams.cluster,
        namespace_name: this.baseParams.namespace,
      },
    });
    this.roleUtil
      .resourceTypeSupportPermissions(
        'configmap',
        {
          cluster_name: this.baseParams['cluster'],
          project_name: this.baseParams['project'],
          namespace_name: this.baseParams['namespace'],
        },
        'create',
      )
      .then(res => (this.configmapCreateEnabled = res));

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe(() => {
        this.initialized = true;
      });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.httpService.request<ResourceList>(
        '/ajax/v2/kubernetes/clusters/' +
          this.baseParams['cluster'] +
          '/configmaps/' +
          (this.baseParams['namespace']
            ? this.baseParams['namespace'] + '/'
            : ''),
        {
          method: 'GET',
          params: {
            name: params.search,
            page: params.pageParams.pageIndex,
            page_size: params.pageParams.pageSize,
          },
        },
      ),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: K8sResourceWithActions<ConfigMap>) {
    return item.kubernetes.metadata.uid;
  }

  showDetail(name: string) {
    this.router.navigate(['configmap', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  createConfigmap() {
    this.router.navigate(['configmap', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  updateConfigmap(name: string) {
    this.router.navigate(['configmap', 'update', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async deleteConfigmap(configmap: K8sResourceWithActions<ConfigMap>) {
    const mirrorConfigmaps = await this.configmapService.getMirrorConfigmaps(
      this.baseParams['cluster'],
      this.baseParams['namespace'],
      configmap.kubernetes.metadata.name,
    );
    if (mirrorConfigmaps.length) {
      const requireSelect = mirrorConfigmaps.filter((item: any) => {
        return item.uuid === configmap.kubernetes.metadata.uid;
      });
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: mirrorConfigmaps,
        confirmTitle: this.translateService.get('delete_configmap'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'configmap_service_delete_selected_cluster_configmap_confirm',
          {
            configmap_name: configmap.kubernetes.metadata.name,
          },
        ),
      };
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorConfigmap[]) => {
          modalRef.close();
          if (res) {
            try {
              await this.configmapUtilities.deleteMirrorConfigmap(
                res,
                configmap,
              );
              this.onUpdate(null);
            } catch (err) {
              this.errorsToastService.error(err);
            }
          }
        });
    } else {
      try {
        await this.configmapUtilities.deleteConfigmap(
          this.baseParams['cluster'],
          configmap,
        );
        this.onUpdate(null);
      } catch (err) {
        this.errorsToastService.error(err);
      }
    }
  }

  pageChanged(param: string, value: number) {
    this.onPageEvent({ [param]: value });
  }
}
