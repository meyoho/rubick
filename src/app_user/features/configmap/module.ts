import { NgModule } from '@angular/core';
import { ExecSharedModule } from 'app/features-shared/exec/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { K8sConfigItemsComponent } from './config-items/k8s-config-items.component';
import { ConfigmapCreateComponent } from './create/configmap-create.component';
import { ConfigmapDetailComponent } from './detail/configmap-detail.component';
import { AddConfigDialogComponent } from './dialog/add-config-dialog.component';
import { ConfigItemsFieldsetComponent } from './fieldset/config-items-fieldset.component';
import { K8sConfigmapFormComponent } from './form/k8s-configmap-form.component';
import { ConfigmapListComponent } from './list/configmap-list.component';
import { ConfigmapRoutingModule } from './routing.module';
import { ConfigmapUpdateComponent } from './update/configmap-update.component';
import { ConfigmapUtilService } from './util.service';
import { ConfigmapWorkloadsComponent } from './workloads-table/component';

@NgModule({
  imports: [SharedModule, ConfigmapRoutingModule, ExecSharedModule],
  providers: [ConfigmapUtilService],
  declarations: [
    ConfigmapListComponent,
    ConfigmapCreateComponent,
    ConfigmapDetailComponent,
    K8sConfigmapFormComponent,
    ConfigItemsFieldsetComponent,
    ConfigmapUpdateComponent,
    K8sConfigItemsComponent,
    AddConfigDialogComponent,
    ConfigmapWorkloadsComponent,
  ],
  entryComponents: [AddConfigDialogComponent],
})
export class ConfigmapModule {}
