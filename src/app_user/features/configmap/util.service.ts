import { DialogService, NotificationService } from '@alauda/ui';
import { Injectable } from '@angular/core';

import {
  ConfigmapService,
  MirrorConfigmap,
} from 'app/services/api/configmap.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';
import { ConfigMap, K8sResourceWithActions } from 'app/typings';
import { BatchReponse } from 'app_user/core/types';

@Injectable()
export class ConfigmapUtilService {
  constructor(
    private dialogService: DialogService,
    private configmapService: ConfigmapService,
    private roleUtilities: RoleUtilitiesService,
    private translateService: TranslateService,
    private k8sResourceService: K8sResourceService,
    private notificationService: NotificationService,
  ) {}

  showUpdate(configmap: K8sResourceWithActions<ConfigMap>) {
    return this.roleUtilities.resourceHasPermission(
      configmap,
      'configmap',
      'update',
    );
  }

  showDelete(configmap: K8sResourceWithActions<ConfigMap>) {
    return this.roleUtilities.resourceHasPermission(
      configmap,
      'configmap',
      'delete',
    );
  }

  async updateConfigmap(
    clusterId: string,
    configmap: K8sResourceWithActions<ConfigMap>,
    clusters?: string[],
  ) {
    await this.dialogService.confirm({
      title: this.translateService.get('update'),
      content: this.translateService.get(
        'configmap_service_update_configmap_confirm',
        {
          configmap_name: configmap.kubernetes.metadata.name,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    if (clusters && clusters.length) {
      const res: BatchReponse = await this.configmapService.updateConfigmapBatch(
        clusters,
        configmap,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_configMap_update',
        res,
        configmap.kubernetes.metadata.name,
      );
      return;
    }
    return this.configmapService.updateConfigmap(clusterId, configmap);
  }

  async updateConfigmapAddKey(
    clusterId: string,
    configmap: K8sResourceWithActions<ConfigMap>,
    key: string,
  ) {
    await this.dialogService.confirm({
      title: this.translateService.get('add'),
      content: this.translateService.get(
        'configmap_service_add_key_configmap_confirm',
        {
          configmap_key: key,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    return this.configmapService.updateConfigmap(clusterId, configmap);
  }

  async updateConfigmapUpdateKey(
    clusterId: string,
    configmap: K8sResourceWithActions<ConfigMap>,
    key: string,
  ) {
    await this.dialogService.confirm({
      title: this.translateService.get('update'),
      content: this.translateService.get(
        'configmap_service_update_key_configmap_confirm',
        {
          configmap_key: key,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    return this.configmapService.updateConfigmap(clusterId, configmap);
  }

  async deleteConfigmapKey(
    clusterId: string,
    configmap: K8sResourceWithActions<ConfigMap>,
    key: string,
  ) {
    await this.dialogService.confirm({
      title: this.translateService.get('delete'),
      content: this.translateService.get(
        'configmap_service_delete_key_configmap_confirm',
        {
          configmap_key: key,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    return this.configmapService.updateConfigmap(clusterId, configmap);
  }

  async deleteConfigmap(
    clusterId: string,
    configmap: K8sResourceWithActions<ConfigMap>,
  ) {
    await this.dialogService.confirm({
      title: this.translateService.get('delete'),
      content: this.translateService.get(
        'configmap_service_delete_configmap_confirm',
        {
          configmap_name: configmap.kubernetes.metadata.name,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    try {
      await this.configmapService.deleteK8sConfigmap(clusterId, configmap);
      this.notificationService.success({
        content: this.translateService.get('delete_configmap_success'),
      });
      return;
    } catch (err) {
      throw err;
    }
  }

  async deleteMirrorConfigmap(
    mirrorConfigmap: MirrorConfigmap[],
    configmap: K8sResourceWithActions<ConfigMap>,
  ) {
    if (mirrorConfigmap && mirrorConfigmap.length) {
      const res: BatchReponse = await this.configmapService.deleteConfigmapBatch(
        mirrorConfigmap,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_configMap_delete',
        res,
        configmap.kubernetes.metadata.name,
      );
      return;
    }
  }
}
