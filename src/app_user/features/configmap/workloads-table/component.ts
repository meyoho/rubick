import { DialogService } from '@alauda/ui';
import {
  animate,
  state,
  style,
  transition,
  trigger,
} from '@angular/animations';
import { Component, Input, OnInit } from '@angular/core';

import { get, values } from 'lodash-es';
import { first } from 'rxjs/operators';

import { ExecCommandDialogComponent } from 'app/features-shared/exec/exec-command/component';
import { AppService } from 'app/services/api/app.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { TerminalService } from 'app/services/api/terminal.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  K8sResourceWithActions,
  PodController,
  TopologyResponse,
} from 'app/typings';
import { ConfigMap, Container, Pod } from 'app/typings/raw-k8s';

interface Dictionary<T> {
  [key: string]: T;
}

interface WorkloadInfo {
  id: number;
  name: string;
  kind: string;
  containers: Array<Container>;
  raw: PodController;
}

@Component({
  selector: 'rc-configmap-workloads',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  animations: [
    trigger('expand', [
      state('*', style({ height: 0 })),
      state('expanded', style({ height: '*' })),
      transition('* => expanded', [animate(250)]),
      transition('expanded => *', [animate(250)]),
    ]),
  ],
})
export class ConfigmapWorkloadsComponent implements OnInit {
  @Input() configmap: K8sResourceWithActions<ConfigMap>;
  @Input() clusterName: string;
  @Input() workloads: WorkloadInfo[];
  columns = ['name', 'type', 'detail'];
  rowExpanded: Dictionary<boolean> = {};

  loading = true;

  constructor(
    private k8sResourceService: K8sResourceService,
    private appService: AppService,
    private dialogService: DialogService,
    private terminalService: TerminalService,
    private translateService: TranslateService,
  ) {}

  ngOnInit() {
    this.k8sResourceService
      .getK8sTopologyByResource({
        cluster: this.clusterName,
        namespace: this.configmap.kubernetes.metadata.namespace,
        name: this.configmap.kubernetes.metadata.name,
        kind: 'configmap',
      })
      .then((res: TopologyResponse) => {
        const workloads: WorkloadInfo[] = [];
        values(res.nodes).forEach((node, id) => {
          const containers = get(node, 'spec.template.spec.containers', null);
          if (containers) {
            workloads.push({
              id,
              kind: node.kind,
              name: node.metadata.name,
              containers,
              raw: node,
            });
          }
        });
        this.workloads = workloads;
      })
      .catch(() => {})
      .then(() => {
        this.loading = false;
      });
  }

  async getPods(resource: PodController) {
    const matchLabels = get(resource, 'spec.selector.matchLabels', {});
    const labelSelector = Object.keys(matchLabels || {})
      .map(key => `${key}=${matchLabels[key]}`)
      .join(',');

    return this.appService
      .getResourcesInNamespace({
        cluster: this.clusterName,
        namespace: this.configmap.kubernetes.metadata.namespace,
        labelSelector,
        resource_type: 'pods',
      })
      .then((res: any) => res.results || [])
      .catch(_err => []);
  }

  openTerminal(
    pods: K8sResourceWithActions<Pod>[],
    container: Container,
    componentName: string,
  ) {
    const dialogRef = this.dialogService.open(ExecCommandDialogComponent, {
      data: { title: this.translateService.get('reload_configration') },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(data => {
      dialogRef.close();
      if (!!data) {
        const podNames = pods
          .map(pod => get(pod, 'kubernetes.metadata.name', ''))
          .join(';');
        const containerName = container.name;
        this.terminalService.openTerminal({
          ctl: componentName,
          pods: podNames,
          selectedPod: '',
          container: containerName,
          namespace: this.configmap.kubernetes.metadata.namespace,
          cluster: this.clusterName,
          user: data.user,
          command: data.command,
        });
      }
    });
  }

  async reloadConfig(item: WorkloadInfo, container: Container) {
    const pods = await this.getPods(item.raw);
    this.openTerminal(pods, container, item.name);
  }

  toggleRow(id: number) {
    this.rowExpanded = {
      ...this.rowExpanded,
      [id]: !this.rowExpanded[id],
    };
  }
}
