import { DialogService, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import {
  ConfigSecretService,
  K8sSecret,
  MirrorSecret,
} from 'app/services/api/config-secret.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import { get } from 'lodash-es';
import { Subject } from 'rxjs';
import { first, map, takeUntil } from 'rxjs/operators';

import { ConfigSecretUtilService } from '../util.service';

@Component({
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
})
export class ConfigSecretDetailComponent implements OnInit {
  private _loading: boolean;
  private _loadError: any;
  private projectName: string;
  private clusterName: string;
  private namespaceName: string;
  private name: string;
  private onDestroy$ = new Subject<void>();
  secretData: K8sSecret;
  mirrorSecrets: MirrorSecret[];
  originMirrorSecrets: MirrorSecret[];
  clusterDisplayName: string;
  initialized = false;

  constructor(
    private translateService: TranslateService,
    private route: ActivatedRoute,
    private cfgsecretService: ConfigSecretService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    public secretUtilities: ConfigSecretUtilService,
    private router: Router,
    private dialogService: DialogService,
    private workspaceComponent: WorkspaceComponent,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {}

  async ngOnInit() {
    this.workspaceComponent.baseParams
      .pipe(
        takeUntil(this.onDestroy$),
        map((params: Params) => {
          return [params['project'], params['cluster'], params['namespace']];
        }),
      )
      .subscribe(async ([projectName, clusterName, namespaceName]) => {
        this.projectName = projectName;
        this.clusterName = clusterName;
        this.namespaceName = namespaceName;
        const params = this.route.snapshot.params;
        this.name = params['name'];
        await this.refetch();
        this.initialized = true;
      });
  }

  back() {
    this.router.navigate(['configsecret'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async refetch() {
    this._loading = true;
    try {
      this.secretData = await this.cfgsecretService.getK8sSecret({
        clusterId: this.clusterName,
        namespace: this.namespaceName,
        name: this.name,
      });
      await this.getMirrorSecrets();
      this._loadError = null;
    } catch ({ status, errors }) {
      this._loadError = errors;
      if (status === 403) {
        // TODO: back to pervious state or parent state
        this.auiNotificationService.warning(
          this.translateService.get('permission_denied'),
        );
        return this.router.navigate(['configsecret'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      }
    }
    this._loading = false;
  }

  updateSecret() {
    this.router.navigate(['configsecret', 'update', this.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  get empty() {
    return !this.secretData;
  }

  get keyEmpty() {
    if (this.secretData) {
      return !this.secretData.hasOwnProperty('referenced_by');
    }
    return false;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  get description() {
    return get(
      this.secretData,
      `kubernetes.metadata.annotations['resource.${
        this.env.label_base_domain
      }/description']`,
      '',
    );
  }

  handleData() {
    let data: any;
    if (this.secretData) {
      data = Object.entries(this.secretData.kubernetes.data);
      data = data.sort((kvArr: any, kvArr_next: any) => {
        if (kvArr[0] < kvArr_next[0]) {
          return -1;
        }
        if (kvArr[0] > kvArr_next[0]) {
          return 1;
        }
        return 0;
      });
    }
    return data;
  }

  async deleteSecret() {
    if (this.originMirrorSecrets.length) {
      const requireSelect = this.originMirrorSecrets.filter((item: any) => {
        return item.uuid === this.secretData.kubernetes.metadata.uid;
      });
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: this.originMirrorSecrets,
        confirmTitle: this.translateService.get('delete_secret'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'secret_service_delete_selected_cluster_secret_confirm',
          {
            secret_name: this.secretData.kubernetes.metadata.name,
          },
        ),
      };
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorSecret[]) => {
          modalRef.close();
          if (res) {
            try {
              this.secretUtilities.deleteMirrorSecret(res, this.secretData);
              return this.router.navigate(['configsecret'], {
                relativeTo: this.workspaceComponent.baseActivatedRoute,
              });
            } catch (err) {
              this.errorsToastService.error(err);
            }
          }
        });
    } else {
      try {
        await this.secretUtilities.deleteSecret(
          this.clusterName,
          this.secretData,
        );
        return this.router.navigate(['configsecret'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      } catch (err) {
        this.errorsToastService.error(err);
      }
    }
  }

  async doRefetch() {
    await this.refetch();
  }

  async getMirrorSecrets() {
    const mirrorSecrets = await this.cfgsecretService.getMirrorSecrets(
      this.clusterName,
      this.namespaceName,
      this.name,
    );
    this.originMirrorSecrets = mirrorSecrets;
    this.mirrorSecrets = mirrorSecrets.filter((secret: MirrorSecret) => {
      return secret.cluster_name !== this.clusterName;
    });
    const currentCluster = mirrorSecrets.find((secret: MirrorSecret) => {
      return secret.cluster_name === this.clusterName;
    });
    this.clusterDisplayName = currentCluster
      ? currentCluster.cluster_display_name
      : this.clusterName;
  }

  redirectToMirrorSecret(secret: MirrorSecret) {
    this.router.navigate([
      '/console/user/workspace',
      {
        project: this.projectName,
        cluster: secret.cluster_name,
        namespace: this.namespaceName,
      },
      'configsecret',
      'detail',
      secret.name,
    ]);
    this.secretData = null;
  }
}
