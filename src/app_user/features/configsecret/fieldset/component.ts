import { NotificationService } from '@alauda/ui';
import { Component, Injector } from '@angular/core';
import { ValidatorFn, Validators } from '@angular/forms';
import { TranslateService } from 'app/translate/translate.service';

import { BaseStringMapFormComponent, KeyValue } from 'app/abstract';
import { K8S_SECRET_KEY } from 'app/utils/patterns';
import { map } from 'lodash-es';

@Component({
  selector: 'rc-secret-items-fieldset',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
})
export class SecretItemsFieldsetComponent extends BaseStringMapFormComponent {
  keyReg = K8S_SECRET_KEY;

  maxSize = 1024 * 1024;

  async addRowFromFile(event: Event) {
    const files = event.target['files'];
    const readFiles: Promise<KeyValue>[] = [];
    const errorFileNames: string[] = [];
    map(files, (file: File) => {
      file.size < this.maxSize
        ? readFiles.push(this.getFileContent(file))
        : errorFileNames.push(file.name);
    });
    const readResult = await Promise.all(readFiles);
    // 导入时，去除排在前面的空白键值
    let count = this.form.controls.length;
    while (count) {
      count--;
      const control = this.form.controls[count];
      if (control) {
        if (!control.value[0] && !control.value[1]) {
          this.form.removeAt(count);
        }
      }
    }
    readResult.forEach((item: KeyValue) => {
      this.checkFileIsBinary(item[1])
        ? errorFileNames.push(item[0])
        : this.addImportItemToForm(item);
    });
    event.target['value'] = null;
    if (errorFileNames.length) {
      this.auiNotificationService.error({
        title: this.translateService.get('error'),
        content: this.translateService.get(
          'comfigmap_from_file_error_content',
          {
            fileNames: errorFileNames.join(', '),
          },
        ),
      });
    }
  }

  addImportItemToForm(item: KeyValue) {
    const control = this.createNewControl();
    control.setValue(item);
    this.form.push(control);
  }

  getFileContent(file: File): Promise<KeyValue> {
    return new Promise((resolve, reject) => {
      const fileReader = new FileReader();
      fileReader.onload = () => {
        resolve([file.name, fileReader.result as string]);
      };
      fileReader.onerror = reject;
      fileReader.readAsText(file);
    });
  }

  checkFileIsBinary(content: string) {
    return Array.from(content).some(char => char.charCodeAt(0) > 127);
  }

  createNewControl() {
    const duplicateKeyValidator: ValidatorFn = control => {
      const index = this.form.controls.indexOf(control.parent);
      const previousKeys = this.getPreviousKeys(index);
      if (previousKeys.includes(control.value)) {
        return {
          duplicateKey: control.value,
        };
      } else {
        return null;
      }
    };

    return this.fb.array([
      [
        '',
        [
          Validators.required,
          Validators.pattern(this.keyReg.pattern),
          duplicateKeyValidator,
        ],
      ],
      [],
    ]);
  }

  constructor(
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    injector: Injector,
  ) {
    super(injector);
  }
}
