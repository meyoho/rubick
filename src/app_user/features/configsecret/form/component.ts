import { DialogService, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectorRef,
  Component,
  ElementRef,
  Inject,
  Input,
  OnDestroy,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import * as jsyaml from 'js-yaml';
import { cloneDeep, forIn, fromPairs, isEmpty, isObject, merge } from 'lodash-es';

import {
  ConfigSecretService,
  K8sSecret,
} from 'app/services/api/config-secret.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import {
  Cluster,
  ClusterSelect,
  RegionService,
} from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';
import { updateActions } from 'app/utils/code-editor-config';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';
import { BatchReponse } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  selector: 'rc-configsecret-form',
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
})
export class K8sConfigSecretFormComponent implements OnInit, OnDestroy {
  @ViewChild('fileUploadInput')
  fileUploadInputElementRef: ElementRef<HTMLInputElement>;

  private namespaceName: string;
  private clusterName: string;
  @Input()
  data: K8sSecret;
  formModel = 'UI';
  submitting = false;
  resourceNameReg = K8S_RESOURCE_NAME_BASE;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };
  type = 'Opaque';
  initialized = false;
  isUpdate: boolean;

  secret: K8sSecret;

  secretItems: { [key: string]: string };
  mirrorClusters: ClusterSelect[] = [];
  clusters: string[];
  secretYaml = '';
  secretYamlOrigin = '';
  editorActionsConfig = updateActions;
  currentCluster: string;
  RESOURCE_TYPE = 'secrets';

  @ViewChild('secretForm')
  form: NgForm;

  secretMirrorResource: BatchReponse;

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    private cfgsecretService: ConfigSecretService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private router: Router,
    private location: Location,
    private errorsToast: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private cdr: ChangeDetectorRef,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    this.secret = {
      kubernetes: this.genDefaultSecret(),
    };
  }

  async ngOnInit() {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = baseParams['cluster'];
    this.namespaceName = baseParams['namespace'];
    await this.getMirrorClusters();
    if (this.data) {
      this.isUpdate = true;
      merge(this.secret.kubernetes, this.data.kubernetes);
      // 联邦集群更新需要删除下面两个属性
      delete this.secret.kubernetes.metadata.uid;
      delete this.secret.kubernetes.metadata.resourceVersion;
      // 获取该资源名称在联邦集群的所有资源
      // 更新时可选联邦，应该是存在该资源名称的集群的集合
      await this.getMirrorResource(this.secret.kubernetes.metadata.name);
      const hasResourceClusters: string[] = [];
      Object.entries(this.secretMirrorResource.responses).map(item => {
        if (item[1] && item[1].code < 300) {
          hasResourceClusters.push(item[0]);
        }
      });
      this.mirrorClusters = this.mirrorClusters.filter(
        (item: ClusterSelect) => {
          return hasResourceClusters.includes(item.name);
        },
      );
      this.clusters = this.clusters.filter(item => {
        return hasResourceClusters.includes(item);
      });
      this.initSecrets();
    }
    this.initialized = true;
  }

  private initSecrets() {
    if (this.secret.kubernetes.type === 'kubernetes.io/dockerconfigjson') {
      const data = JSON.parse(
        atob(this.secret.kubernetes.data['.dockerconfigjson']),
      ).auths;
      const docker_service_address = Object.keys(data)[0];
      this.secretItems = {
        docker_service_address,
        username: data[docker_service_address].username,
        password: data[docker_service_address].password,
        email: data[docker_service_address].email,
      };
    } else {
      this.secretItems = fromPairs(
        Object.entries(this.secret.kubernetes.data).map(([key, val]) => [
          key,
          atob(val),
        ]),
      );
    }
    this.type = this.secret.kubernetes.type;
  }

  typeChange(type: string) {
    switch (type) {
      case 'kubernetes.io/tls':
        this.secretItems = {
          'tls.crt': '',
          'tls.key': '',
        };
        break;

      case 'kubernetes.io/ssh-auth':
        this.secretItems = { 'ssh-privatekey': '' };
        break;

      case 'kubernetes.io/basic-auth':
        this.secretItems = {
          username: '',
          password: '',
        };
        break;

      case 'kubernetes.io/dockerconfigjson':
        this.secretItems = {
          docker_service_address: '',
          username: '',
          password: '',
          email: '',
        };
        break;

      default:
        this.secretItems = {};
        break;
    }
    // 改变表单格式
    this.type = type;
  }

  genDefaultSecret() {
    return {
      apiVersion: 'v1',
      kind: 'Secret',
      type: 'Opaque',
      data: {},
      metadata: {
        name: '',
        annotations: {
          [`resource.${this.env.label_base_domain}/description`]: '',
        },
      },
    } as K8sSecret['kubernetes'];
  }

  ngOnDestroy() {}

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        try {
          this.formatSecret();
          this.secretYamlOrigin = this.secretYaml = this.formToYaml(
            this.secret.kubernetes,
          );
        } catch (e) {
          this.auiNotificationService.error(e.message);
        } finally {
          break;
        }
      case 'UI':
        this.secret.kubernetes = this.yamlToForm(this.secretYaml);
        this.genSecretItems(this.secret);
        break;
    }
  }

  private yamlToForm(yaml: string) {
    let formModel: any;
    try {
      formModel = jsyaml.safeLoad(yaml);
      forIn(formModel.data, (val, key) => (formModel.data[key] = atob(val)));
    } catch (e) {
      this.auiNotificationService.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    if (this.type === 'kubernetes.io/dockerconfigjson') {
      const data = JSON.parse(formModel.data['.dockerconfigjson']).auths;
      const docker_service_address = Object.keys(data)[0];
      formModel.data = {
        docker_service_address,
        username: data[docker_service_address].username,
        password: data[docker_service_address].password,
        email: data[docker_service_address].email,
      };
    }
    return merge(this.genDefaultSecret(), formModel);
  }

  private formToYaml(formModel: any) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      if (!isEmpty(json.data)) {
        forIn(json.data, (val, key) => (json.data[key] = btoa(val)));
      }
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  private genSecretItems(secret: K8sSecret) {
    this.secretItems = {};
    if (isObject(secret.kubernetes.data) && !isEmpty(secret.kubernetes.data)) {
      for (const [key, value] of Object.entries(secret.kubernetes.data)) {
        this.secretItems[key] = value;
      }
    } else {
      this.secretItems = {};
    }
  }

  onImport(_event: Event) {
    const length = this.fileUploadInputElementRef.nativeElement.files.length;
    let counter = 0;

    function checkFileIsBinary(content: string) {
      return Array.from(content).some(char => char.charCodeAt(0) > 127);
    }

    Array.from(this.fileUploadInputElementRef.nativeElement.files).forEach(
      file => {
        const reader = new FileReader();
        reader.onloadend = () => {
          if (checkFileIsBinary(reader.result as string)) {
            this.auiNotificationService.error({
              content: this.translate.get('fileupload_binary_unsupported'),
            });
          } else {
            this.secretItems[
              Object.keys(this.secretItems)[0]
            ] = reader.result.toString();
            this.cdr.markForCheck();
          }
          counter++;
          if (length === counter) {
            this.fileUploadInputElementRef.nativeElement.value = '';
          }
        };

        reader.readAsText(file);
      },
    );
  }

  private formatSecret() {
    let secretItems = cloneDeep(this.secretItems) || {};
    if (this.type === 'kubernetes.io/dockerconfigjson') {
      secretItems = {
        '.dockerconfigjson': `{"auths":{"${
          this.secretItems['docker_service_address']
        }":{"username":"${this.secretItems['username']}","password":"${
          this.secretItems['password']
        }","email":"${this.secretItems['email']}"}}}`,
      };
    }
    const keys = Object.keys(secretItems);
    const duplicateKeys: string[] = [];

    keys.forEach((key, index) => {
      if (!duplicateKeys.includes(key) && index !== keys.lastIndexOf(key)) {
        duplicateKeys.push(key);
      }
    });

    if (duplicateKeys.length) {
      const error =
        this.translate.get('secret_duplicate_keys') + duplicateKeys.join(',');
      throw new Error(error);
    }

    if (Object.keys(secretItems).length) {
      this.secret.kubernetes.data = {};
      Object.keys(secretItems).forEach(key => {
        this.secret.kubernetes.data[key] = secretItems[key];
      });
    } else {
      this.secret.kubernetes.data = {};
    }
    this.secret.kubernetes.metadata.namespace = this.namespaceName;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }

    try {
      if (this.formModel === 'YAML') {
        this.secret.kubernetes = this.yamlToForm(this.secretYaml);
        this.genSecretItems(this.secret);
      }
    } catch (err) {
      this.errorsToast.error(err);
      return;
    }

    try {
      this.formatSecret();
    } catch (e) {
      return this.auiNotificationService.error(e.message);
    }

    try {
      let comfirmTitle, comfirmContent;
      if (this.data) {
        comfirmTitle = 'update';
        comfirmContent = 'secret_service_update_secret_confirm';
      } else {
        comfirmTitle = 'create';
        comfirmContent = 'secret_service_create_secret_confirm';
      }
      await this.dialogService.confirm({
        title: this.translate.get(comfirmTitle),
        content: this.translate.get(comfirmContent, {
          secret_name: this.secret.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.submitting = true;

    try {
      if (this.data) {
        const secretInfo = cloneDeep(this.secret);
        forIn(secretInfo.kubernetes.data, (val, key) => {
          if (!key) {
            delete secretInfo.kubernetes.data[key];
          } else {
            secretInfo.kubernetes.data[key] = btoa(val);
          }
        });
        await this.cfgsecretService.updateK8sSecret(
          this.clusterName,
          secretInfo,
          this.clusters,
        );
      } else {
        const secretInfo = cloneDeep(this.secret.kubernetes);
        forIn(secretInfo.data, (val, key) => {
          if (!key) {
            delete secretInfo.data[key];
          } else {
            secretInfo.data[key] = btoa(val);
          }
        });
        await this.cfgsecretService.createK8sSecret(
          this.clusterName,
          secretInfo,
          this.clusters,
        );
      }
      this.router.navigate(
        ['configsecret', 'detail', this.secret.kubernetes.metadata.name],
        {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        },
      );
    } catch (e) {
      this.errorsToast.error(e);
    }

    this.submitting = false;
  }

  cancel() {
    this.location.back();
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror && cluster.mirror.regions) {
      this.mirrorClusters = cluster.mirror.regions || [];
      this.currentCluster = this.mirrorClusters.find(item => {
        return item.name === this.clusterName;
      }).name;
      this.clusters = this.mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
    } else {
      this.clusters = [this.clusterName];
    }
  }

  private async getMirrorResource(name: string) {
    try {
      this.secretMirrorResource = await this.k8sResourceService.getMirrorResources(
        this.clusters,
        this.namespaceName,
        this.RESOURCE_TYPE,
        name,
      );
      await this.k8sResourceService.currentClusterResponseStatus(
        this.secretMirrorResource,
        this.clusterName,
      );
    } catch (err) {
      this.errorsToast.error(err);
      this.router.navigate(['configsecret'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
