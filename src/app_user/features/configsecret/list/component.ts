import { DialogService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, Subject } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import {
  ConfigSecretService,
  K8sSecret,
  MirrorSecret,
} from 'app/services/api/config-secret.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { HttpService } from 'app/services/http.service';
import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments, WorkspaceBaseParams } from 'app/typings';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import { ConfigSecretUtilService } from '../util.service';

@Component({
  templateUrl: './component.html',
  styleUrls: ['./component.scss'],
  providers: [ResourceListPermissionsService],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ConfigSecretListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  secretCreateEnabled: boolean;
  initialized: boolean;

  columns = ['name', 'type', 'description', 'action'];
  private onDestroy$ = new Subject<any>();
  private baseParams: WorkspaceBaseParams;

  get descriptionKey() {
    return `resource.${this.env.label_base_domain}/description`;
  }

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private translateService: TranslateService,
    public secretUtilities: ConfigSecretUtilService,
    private errorsToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private httpService: HttpService,
    private dialogService: DialogService,
    private cfgsecretService: ConfigSecretService,
    private workspaceComponent: WorkspaceComponent,
    public resourcePermissionService: ResourceListPermissionsService,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.resourcePermissionService.initContext({
      resourceType: 'k8s_others',
      context: {
        project_name: this.baseParams.project,
        cluster_name: this.baseParams.cluster,
        namespace_name: this.baseParams.namespace,
      },
    });
    this.roleUtil
      .resourceTypeSupportPermissions(
        'k8s_others',
        {
          cluster_name: this.baseParams['cluster'],
          project_name: this.baseParams['project'],
          namespace_name: this.baseParams['namespace'],
        },
        'create',
      )
      .then(res => (this.secretCreateEnabled = res));

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe(() => {
        this.initialized = true;
      });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.httpService.request<ResourceList>(
        '/ajax/v2/kubernetes/clusters/' +
          this.baseParams['cluster'] +
          '/secrets/' +
          (this.baseParams['namespace']
            ? this.baseParams['namespace'] + '/'
            : ''),
        {
          method: 'GET',
          params: {
            name: params.search,
            page: params.pageParams.pageIndex,
            page_size: params.pageParams.pageSize,
          },
          addNamespace: false,
        },
      ),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: K8sSecret) {
    return item.kubernetes.metadata.uid;
  }

  showDetail(name: string) {
    this.router.navigate(['configsecret', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  create() {
    this.router.navigate(['configsecret', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  update(name: string) {
    this.router.navigate(['configsecret', 'update', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async delete(secret: K8sSecret) {
    const mirrorSecrets = await this.cfgsecretService.getMirrorSecrets(
      this.baseParams['cluster'],
      this.baseParams['namespace'],
      secret.kubernetes.metadata.name,
    );
    if (mirrorSecrets.length) {
      const requireSelect = mirrorSecrets.filter((item: any) => {
        return item.uuid === secret.kubernetes.metadata.uid;
      });
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: mirrorSecrets,
        confirmTitle: this.translateService.get('delete_secret'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'secret_service_delete_selected_cluster_secret_confirm',
          {
            secret_name: secret.kubernetes.metadata.name,
          },
        ),
      };
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorSecret[]) => {
          modalRef.close();
          if (res) {
            try {
              await this.secretUtilities.deleteMirrorSecret(res, secret);
              this.onUpdate(null);
            } catch (err) {
              this.errorsToastService.error(err);
            }
          }
        });
    } else {
      try {
        await this.secretUtilities.deleteSecret(
          this.baseParams['cluster'],
          secret,
        );
        this.onUpdate(null);
      } catch (err) {
        this.errorsToastService.error(err);
      }
    }
  }

  pageChanged(param: string, value: number) {
    this.onPageEvent({ [param]: value });
  }
}
