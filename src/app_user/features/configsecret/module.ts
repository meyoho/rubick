import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';

import { ConfigSecretCreateComponent } from './create/component';
import { SecretDataViwerComponent } from './data-viewer/component';
import { ConfigSecretDetailComponent } from './detail/component';
import { SecretItemsFieldsetComponent } from './fieldset/component';
import { K8sConfigSecretFormComponent } from './form/component';
import { ConfigSecretListComponent } from './list/component';
import { ConfigSecretRoutingModule } from './routing.module';
import { ConfigSecretUpdateComponent } from './update/component';
import { ConfigSecretUtilService } from './util.service';

@NgModule({
  imports: [SharedModule, ConfigSecretRoutingModule],
  providers: [ConfigSecretUtilService],
  declarations: [
    ConfigSecretListComponent,
    ConfigSecretCreateComponent,
    ConfigSecretDetailComponent,
    K8sConfigSecretFormComponent,
    SecretItemsFieldsetComponent,
    ConfigSecretUpdateComponent,
    SecretDataViwerComponent,
  ],
})
export class ConfigSecretModule {}
