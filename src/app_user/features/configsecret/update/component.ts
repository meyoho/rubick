import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import {
  ConfigSecretService,
  K8sSecret,
} from 'app/services/api/config-secret.service';

import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

import { ErrorsToastService } from 'app/services/errors-toast.service';

@Component({
  template: `
    <rc-loading-mask
      class="rc-form-loading-mask"
      [loading]="!initialized"
    ></rc-loading-mask>
    <rc-configsecret-form
      *ngIf="initialized"
      [data]="data"
    ></rc-configsecret-form>
  `,
})
export class ConfigSecretUpdateComponent implements OnInit {
  private clusterName: string;
  private namespaceName: string;
  initialized = false;
  data: K8sSecret;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    private cfgsecretService: ConfigSecretService,
  ) {}
  async ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    const name = params['name'];
    try {
      const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
      this.clusterName = workspaceParams['cluster'];
      this.namespaceName = workspaceParams['namespace'];
      this.data = await this.cfgsecretService.getK8sSecret({
        clusterId: this.clusterName,
        namespace: this.namespaceName,
        name,
      });
      this.initialized = true;
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate(['service'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
