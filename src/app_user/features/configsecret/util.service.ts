import { DialogService, NotificationService } from '@alauda/ui';
import { Injectable } from '@angular/core';

import {
  ConfigSecretService,
  K8sSecret,
  MirrorSecret,
} from 'app/services/api/config-secret.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';
import { BatchReponse } from 'app_user/core/types';

@Injectable()
export class ConfigSecretUtilService {
  constructor(
    private dialogService: DialogService,
    private cfgsecretService: ConfigSecretService,
    private roleUtilities: RoleUtilitiesService,
    private translateService: TranslateService,
    private k8sResourceService: K8sResourceService,
    private notificationService: NotificationService,
  ) {}

  showUpdate(secret: K8sSecret) {
    return this.roleUtilities.resourceHasPermission(
      secret,
      'k8s_others',
      'update',
    );
  }

  showDelete(secret: K8sSecret) {
    return this.roleUtilities.resourceHasPermission(
      secret,
      'k8s_others',
      'delete',
    );
  }

  async updateSecret(
    clusterId: string,
    secret: K8sSecret,
    clusters?: string[],
  ) {
    await this.dialogService.confirm({
      title: this.translateService.get('update'),
      content: this.translateService.get(
        'secret_service_update_secret_confirm',
        {
          secret_name: secret.kubernetes.metadata.name,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    if (clusters && clusters.length) {
      const res: BatchReponse = await this.cfgsecretService.updateSecretBatch(
        clusters,
        secret,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_secret_update',
        res,
        secret.kubernetes.metadata.name,
      );
      return;
    }
    return this.cfgsecretService.updateSecret(clusterId, secret);
  }

  async updateSecretAddKey(clusterId: string, secret: K8sSecret, key: string) {
    await this.dialogService.confirm({
      title: this.translateService.get('add'),
      content: this.translateService.get(
        'secret_service_add_key_secret_confirm',
        {
          secret_key: key,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    return this.cfgsecretService.updateSecret(clusterId, secret);
  }

  async updateSecretUpdateKey(
    clusterId: string,
    secret: K8sSecret,
    key: string,
  ) {
    await this.dialogService.confirm({
      title: this.translateService.get('update'),
      content: this.translateService.get(
        'secret_service_update_key_secret_confirm',
        {
          secret_key: key,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    return this.cfgsecretService.updateSecret(clusterId, secret);
  }

  async deleteSecretKey(clusterId: string, secret: K8sSecret, key: string) {
    await this.dialogService.confirm({
      title: this.translateService.get('delete'),
      content: this.translateService.get(
        'secret_service_delete_key_secret_confirm',
        {
          secret_key: key,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    return this.cfgsecretService.updateSecret(clusterId, secret);
  }

  async deleteSecret(clusterId: string, secret: K8sSecret) {
    await this.dialogService.confirm({
      title: this.translateService.get('delete'),
      content: this.translateService.get(
        'secret_service_delete_secret_confirm',
        {
          secret_name: secret.kubernetes.metadata.name,
        },
      ),
      confirmText: this.translateService.get('confirm'),
      cancelText: this.translateService.get('cancel'),
    });
    try {
      await this.cfgsecretService.deleteK8sSecret(clusterId, secret);
      this.notificationService.success({
        content: this.translateService.get('delete_secret_success'),
      });
      return;
    } catch (err) {
      throw err;
    }
  }

  async deleteMirrorSecret(mirrorSecret: MirrorSecret[], secret: K8sSecret) {
    if (mirrorSecret && mirrorSecret.length) {
      const res: BatchReponse = await this.cfgsecretService.deleteSecretBatch(
        mirrorSecret,
      );
      this.k8sResourceService.reourceBatchNotification(
        'cluster_secret_delete',
        res,
        secret.kubernetes.metadata.name,
      );
      return;
    }
  }
}
