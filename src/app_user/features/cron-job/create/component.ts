import { Component } from '@angular/core';

@Component({
  template: '<rc-cron-job-form-container></rc-cron-job-form-container>',
  styles: [
    `
      :host {
        display: flex !important;
        flex-direction: column;
        flex: 1;
      }
    `,
  ],
})
export class CreateCronJobComponent {
  constructor() {}
}
