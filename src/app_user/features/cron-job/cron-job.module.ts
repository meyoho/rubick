import { NgModule } from '@angular/core';

import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { ImageSharedModule } from 'app/features-shared/image/shared.module';
import { JobSharedModule } from 'app/features-shared/job/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { CreateCronJobComponent } from 'app_user/features/cron-job/create/component';
import { CronJobRoutingModule } from 'app_user/features/cron-job/cron-job.routing.module';
import { CronJobDetailComponent } from 'app_user/features/cron-job/detail/component';
import { CronJobDetailEventComponent } from 'app_user/features/cron-job/detail/event/component';
import { CronJobDetailInfoComponent } from 'app_user/features/cron-job/detail/info/component';
import { CronJobFormContainerComponent } from 'app_user/features/cron-job/form-container/component';
import { CronJobFormComponent } from 'app_user/features/cron-job/form/component';
import { CronJobConfigTimeFormComponent } from 'app_user/features/cron-job/form/cron-job-config-time/component';
import { CronJobSpecFormComponent } from 'app_user/features/cron-job/form/cron-job-spec/component';
import { JobSpecFormComponent } from 'app_user/features/cron-job/form/job-spec/component';
import { CronJobListComponent } from 'app_user/features/cron-job/list/component';
import { CronJobUpdateComponent } from 'app_user/features/cron-job/update/component';

const components = [
  CronJobListComponent,
  CronJobFormComponent,
  CreateCronJobComponent,
  CronJobFormContainerComponent,
  CronJobSpecFormComponent,
  JobSpecFormComponent,
  CronJobConfigTimeFormComponent,
  CronJobDetailComponent,
  CronJobDetailInfoComponent,
  CronJobDetailEventComponent,
  CronJobUpdateComponent,
];
@NgModule({
  imports: [
    SharedModule,
    AppSharedModule,
    CronJobRoutingModule,
    EventSharedModule,
    FormTableModule,
    ImageSharedModule,
    JobSharedModule,
  ],
  declarations: components,
})
export class CronJobModule {}
