import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as jsyaml from 'js-yaml';
import { cloneDeep } from 'lodash-es';
import { first } from 'rxjs/operators';

import { UpdateEnvDialogComponent } from 'app/features-shared/app/dialog/update-env/component';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import {
  CronJob,
  Environments,
  Job,
  K8sResourceWithActions,
  ObjectMeta,
  StringMap,
  WorkspaceBaseParams,
} from 'app/typings';
import * as codeEditorUtil from 'app/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class CronJobDetailComponent implements OnInit {
  dataInfo: ObjectMeta;
  clusterName: string;
  namespaceName: string;
  name: string;
  yaml: string;
  private RESOURCE_TYPE = 'cronjobs';
  codeEditorOptions = codeEditorUtil.yamlOptions;
  actionsConfigView = codeEditorUtil.viewActions;
  initialized = false;
  loading = false;
  result: K8sResourceWithActions<CronJob>;
  parentPath = 'cron_job';
  baseParams: WorkspaceBaseParams;
  labelSelector: StringMap = {};

  constructor(
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private roleUtil: RoleUtilitiesService,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.name = params['name'];
    this.labelSelector[
      `cronjob.${this.environments.label_base_domain}/name`
    ] = this.name;
    const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = workspaceParams['cluster'];
    this.namespaceName = workspaceParams['namespace'];
    this.getDetail();
  }

  getYaml(data: CronJob) {
    this.yaml = this.formToYaml(data);
  }

  canUpdate() {
    return this.roleUtil.resourceHasPermission(
      this.result,
      'k8s_others',
      'update',
    );
  }

  canDelete() {
    return this.roleUtil.resourceHasPermission(
      this.result,
      'k8s_others',
      'delete',
    );
  }

  formToYaml(formModel: any) {
    try {
      const json = cloneDeep(formModel);
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  async getDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail(
        this.RESOURCE_TYPE,
        {
          clusterName: this.clusterName,
          name: this.name,
          namespace: this.namespaceName,
        },
      );
      this.result = result;
      this.getYaml(result.kubernetes);
      this.dataInfo = result.kubernetes.metadata || {};
      this.initialized = true;
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate([this.parentPath], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }

  update() {
    this.router.navigate(
      ['cron_job', 'update', this.result.kubernetes.metadata.name],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      },
    );
  }

  async executeNow() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('cron_job_execute_now_confirm', {
          name: this.result.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('execute_now'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    try {
      const job: Job = await this.k8sResourceService.manualExecJob({
        cluster: this.clusterName,
        namespace: this.namespaceName,
        name: this.result.kubernetes.metadata.name,
      });
      this.router.navigate(['job_record', 'detail', job.metadata.name], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  updateEnv(params: { containerName: string; type: 'env' | 'envFrom' }) {
    const dialogRef = this.dialogService.open(UpdateEnvDialogComponent, {
      size: params.type === 'env' ? DialogSize.Big : DialogSize.Medium,
      data: {
        cronJob: this.result.kubernetes,
        containerName: params.containerName,
        baseParams: this.baseParams,
        type: params.type,
      },
    });
    return dialogRef.componentInstance.close
      .pipe(first())
      .subscribe((res: boolean) => {
        if (res) {
          this.auiNotificationService.success(
            this.translate.get('update_success'),
          );
          this.getDetail();
        }
        dialogRef.close();
      });
  }

  async delete() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_cron_job_confirm', {
          name: this.result.kubernetes.metadata.name,
        }),
        content: this.translate.get('delete_cron_job_tip'),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        this.result.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translate.get('success'),
        content: this.translate.get('delete_cron_job_success', {
          name: this.result.kubernetes.metadata.name,
        }),
      });
      this.router.navigate(['cron_job'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }
}
