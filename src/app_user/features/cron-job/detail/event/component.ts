import { Component, Input, OnChanges, SimpleChanges } from '@angular/core';

import { CronJob, K8sResourceWithActions } from 'app/typings';

interface Filters {
  or: {
    and: {
      term?: {
        kind?: string;
        name?: string;
      };
      regexp?: {
        name: string;
      };
    }[];
  }[];
}
@Component({
  selector: 'rc-cron-job-detail-event',
  templateUrl: 'template.html',
})
export class CronJobDetailEventComponent implements OnChanges {
  @Input()
  data: K8sResourceWithActions<CronJob>;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  initialized = false;
  empty = false;
  filters: Filters = {
    or: [],
  };
  filterString: string;
  name: string;

  ngOnChanges({ data }: SimpleChanges) {
    if (data && data.currentValue) {
      this.extractAllResources();
      this.empty = !this.data;
      this.initialized = true;
    }
  }

  extractAllResources() {
    this.name = this.data.kubernetes.metadata.name;
    this.produceFilter(this.data);
    this.filterString = JSON.stringify(this.filters);
  }

  produceFilter(item: K8sResourceWithActions<CronJob>) {
    this.filters.or = [
      {
        and: [
          {
            term: {
              kind: item.kubernetes.kind,
            },
          },
          {
            term: {
              name: item.kubernetes.metadata.name,
            },
          },
        ],
      },
    ];
  }
}
