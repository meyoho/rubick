import { DialogService, DialogSize } from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Input,
  OnDestroy,
  OnInit,
  Output,
} from '@angular/core';

import { Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { UpdateImageTagDialogComponent } from 'app/features-shared/app/dialog/update-image-tag/component';
import { UpdateResourceSizeDialogComponent } from 'app/features-shared/app/dialog/update-resource-size/component';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  CronJob,
  JobSpec,
  K8sResourceWithActions,
  WorkspaceBaseParams,
} from 'app/typings';

@Component({
  selector: 'rc-cron-job-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class CronJobDetailInfoComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<any>();
  @Input()
  data: K8sResourceWithActions<CronJob>;
  @Input()
  cluster: string;
  @Input()
  canUpdate: boolean;
  @Input()
  canDelete: boolean;
  @Input()
  baseParams: WorkspaceBaseParams;
  @Output()
  onUpdateSuccess = new EventEmitter<void>();
  @Output()
  onUpdate = new EventEmitter<void>();
  @Output()
  onDelete = new EventEmitter<void>();
  @Output()
  onExecuteNow = new EventEmitter<void>();
  resource: CronJob;
  jobSpec: JobSpec;
  lang: string;
  constructor(
    private dialogService: DialogService,
    private errorsToastService: ErrorsToastService,
    private translate: TranslateService,
  ) {}
  ngOnInit() {
    this.resource = this.data.kubernetes;
    this.jobSpec = this.resource.spec.jobTemplate.spec;
    this.translate.currentLang$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(_lang => {
        if (_lang === 'zh_cn') {
          this.lang = 'zh-cn';
        }
        if (_lang === 'en') {
          this.lang = 'en';
        }
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  update() {
    this.onUpdate.next();
  }

  executeNow() {
    this.onExecuteNow.next();
  }

  updateImageTag(containerName: string) {
    const dialogRef = this.dialogService.open(UpdateImageTagDialogComponent, {
      size: DialogSize.Big,
      data: {
        cronJob: this.resource,
        containerName: containerName,
        baseParams: this.baseParams,
      },
    });
    dialogRef.componentInstance.close.pipe(first()).subscribe(async res => {
      dialogRef.close();
      if (res) {
        try {
          this.onUpdateSuccess.next();
        } catch (err) {
          this.errorsToastService.error(err);
        }
      }
    });
  }

  updateResourceSize(containerName: string) {
    const dialogRef = this.dialogService.open(
      UpdateResourceSizeDialogComponent,
      {
        size: DialogSize.Big,
        data: {
          cronJob: this.resource,
          containerName: containerName,
          baseParams: this.baseParams,
        },
      },
    );
    dialogRef.componentInstance.close.pipe(first()).subscribe(async res => {
      dialogRef.close();
      if (res) {
        try {
          this.onUpdateSuccess.next();
        } catch (err) {
          this.errorsToastService.error(err);
        }
      }
    });
  }

  async delete() {
    this.onDelete.next();
  }
}
