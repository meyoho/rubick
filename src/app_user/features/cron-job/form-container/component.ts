import { NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Inject, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import * as jsyaml from 'js-yaml';
import { merge, set } from 'lodash-es';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import {
  CronJob,
  Environments,
  K8sResourceWithActions,
  WorkspaceBaseParams,
} from 'app/typings';
import { createActions, updateActions } from 'app/utils/code-editor-config';
import { K8S_SERVICE_NAME } from 'app/utils/patterns';
import {
  checkImageSelectionParam,
  getDefaultPodController,
} from 'app_user/features/app/util';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  selector: 'rc-cron-job-form-container',
  styleUrls: ['./styles.scss'],
  templateUrl: 'template.html',
})
export class CronJobFormContainerComponent implements OnInit {
  @Input()
  data: K8sResourceWithActions<CronJob>;
  namespace: string;
  cluster: string;
  project: string;
  RESOURCE_TYPE = 'cronjobs';
  resourceNameReg = K8S_SERVICE_NAME;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };

  actionsConfig = createActions;
  submitting = false;
  initialized = false;
  baseParams: WorkspaceBaseParams;
  formModel = 'UI';
  resource: CronJob;
  yaml: string;
  createResourceNotifyContent: 'create_cron_job_success';
  updateResourceNotifyContent: 'update_cron_job_success';

  @ViewChild('resourceForm')
  resourceForm: NgForm;

  constructor(
    private activatedRoute: ActivatedRoute,
    private translateService: TranslateService,
    private router: Router,
    private location: Location,
    private auiNotificationService: NotificationService,
    private k8sResourceService: K8sResourceService,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    @Inject(ENVIRONMENTS) private environments: Environments,
  ) {
    this.resource = this.genDefaultResource();
  }

  async ngOnInit() {
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    const queryParams = this.activatedRoute.snapshot.queryParams;
    this.cluster = this.baseParams['cluster'];
    this.namespace = this.baseParams['namespace'];
    this.project = this.baseParams['project'];
    if (this.data && this.data.kubernetes) {
      this.resource = this.updateInitResource(this.data.kubernetes);
      this.actionsConfig = updateActions;
    } else {
      if (!checkImageSelectionParam(queryParams)) {
        this.cancel();
      }
      const podController = getDefaultPodController(
        queryParams,
        this.namespace,
      );
      Object.assign(
        this.resource.spec.jobTemplate.spec.template.spec,
        podController.spec.template.spec,
      );
    }
    this.resource.metadata.namespace = this.namespace;
    this.initialized = true;
  }

  genDefaultResource(): CronJob {
    return {
      apiVersion: 'batch/v1beta1',
      kind: 'CronJob',
      metadata: {
        name: '',
        namespace: '',
      },
      spec: {
        concurrencyPolicy: 'Allow',
        failedJobsHistoryLimit: 20,
        successfulJobsHistoryLimit: 20,
        schedule: '',
        suspend: false,
        jobTemplate: {
          spec: {
            activeDeadlineSeconds: 7200,
            backoffLimit: 6,
            template: {
              spec: {
                restartPolicy: 'Never',
                containers: [],
              },
            },
          },
        },
      },
    };
  }

  updateInitResource(resource: CronJob) {
    delete resource.metadata.resourceVersion;
    return resource;
  }

  goToResourceDetailPage(name: string) {
    this.router.navigate(['cron_job', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async confirm() {
    this.resourceForm.onSubmit(null);
    if (this.resourceForm.invalid) {
      return;
    }

    this.submitting = true;
    let payload: CronJob;
    try {
      payload = this.genPayload();
    } catch (err) {
      this.auiNotificationService.error({
        title: this.translateService.get('yaml_error'),
        content: err.message,
      });
      this.submitting = false;
      return;
    }

    try {
      await this.confirmResource(payload);
      this.goToResourceDetailPage(payload.metadata.name);
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.submitting = false;
    }
  }

  private async confirmResource(payload: CronJob) {
    let notifyContent;
    if (this.data) {
      // update resource
      notifyContent = this.updateResourceNotifyContent;
      await this.k8sResourceService.updateK8sReource(
        this.cluster,
        this.RESOURCE_TYPE,
        payload,
      );
    } else {
      // create resource
      notifyContent = this.createResourceNotifyContent;
      await this.k8sResourceService.createK8sReource(
        this.cluster,
        this.RESOURCE_TYPE,
        payload,
      );
    }
    this.auiNotificationService.success({
      title: this.translateService.get('success'),
      content: this.translateService.get(notifyContent, {
        name: payload.metadata.name || '',
      }),
    });
  }

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        this.yaml = this.formModelToYaml(this.formatResource());
        break;
      case 'UI':
        this.resource = this.yamlToFormModel(this.yaml);
        break;
    }
  }

  private formModelToYaml(formModel: CronJob) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.auiNotificationService.error(err.message);
    }
  }

  private yamlToFormModel(yaml: string) {
    let formModel: CronJob;
    try {
      formModel = jsyaml.safeLoad(yaml);
    } catch (e) {
      this.auiNotificationService.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return merge(this.genDefaultResource(), formModel);
  }

  genPayload() {
    if (this.formModel === 'YAML') {
      return this.genResourceFromYaml();
    }
    return this.formatResource();
  }

  formatResource() {
    set(
      this.resource.spec.jobTemplate,
      `metadata.labels['cronjob.${this.environments.label_base_domain}/name']`,
      this.resource.metadata.name,
    );
    return this.resource;
  }

  genResourceFromYaml() {
    const resource: CronJob = jsyaml.safeLoad(this.yaml);
    if (resource.metadata) {
      resource.metadata.namespace = this.namespace;
      set(
        resource.spec.jobTemplate,
        `metadata.labels['cronjob.${
          this.environments.label_base_domain
        }/name']`,
        resource.metadata.name,
      );
    }
    return resource;
  }

  cancel() {
    this.location.back();
  }
}
