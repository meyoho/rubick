import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { CronJob } from 'app/typings';
import { K8S_SERVICE_NAME } from 'app/utils/patterns';

@Component({
  selector: 'rc-cron-job-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronJobFormComponent extends BaseResourceFormGroupComponent<
  CronJob,
  CronJob
> {
  @Input()
  isUpdate: boolean;
  @Input()
  project: string;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  namePattern = K8S_SERVICE_NAME;
  constructor(injector: Injector) {
    super(injector);
  }

  getDefaultFormModel() {
    return {};
  }

  createForm() {
    const annotationsForm = this.fb.group({
      'resource.alauda.io/display-name': this.fb.control(''),
    });
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(this.namePattern.pattern),
      ]),
      namespace: this.fb.control(''),
      annotations: annotationsForm,
    });
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: metadataForm,
      spec: this.fb.control({}),
    });
  }
}
