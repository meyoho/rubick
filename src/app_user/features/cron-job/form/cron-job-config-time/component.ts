import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { NATURAL_NUMBER_PATTERN } from 'app/utils/patterns';

interface TimeFormModel {
  type?: string;
  number?: number | '';
}

@Component({
  selector: 'rc-cron-job-config-time-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CronJobConfigTimeFormComponent extends BaseResourceFormGroupComponent<
  number,
  TimeFormModel
> {
  timeType = ['second', 'minute', 'hour', 'day'];
  timeMultiple = [1, 60, 60, 24];
  constructor(injector: Injector) {
    super(injector);
  }
  pattern = NATURAL_NUMBER_PATTERN;

  getDefaultFormModel(): TimeFormModel {
    return {
      type: 'second',
      number: '',
    };
  }

  createForm() {
    return this.fb.group({
      type: ['second'],
      number: ['', Validators.pattern(this.pattern.pattern)],
    });
  }

  adaptFormModel(formModel: TimeFormModel): number {
    if (formModel && formModel.number) {
      const index = this.timeType.indexOf(formModel.type);
      const number = +this.formModel.number;
      return this.timeMultiple
        .slice(0, index + 1)
        .reduce((number, multiple) => {
          return number * multiple;
        }, number);
    }
    return 0;
  }

  adaptResourceModel(resource: number): TimeFormModel {
    if (resource) {
      if (typeof +resource === 'number') {
        return this.formatSecond(resource, 0);
      }
    }
    return {
      number: '',
      type: 'second',
    };
  }

  formatSecond(s: number, index: number): TimeFormModel {
    if (index === this.timeMultiple.length || s % this.timeMultiple[index]) {
      return {
        number: s,
        type: this.timeType[index - 1],
      };
    }
    return this.formatSecond(s / this.timeMultiple[index], index + 1);
  }
}
