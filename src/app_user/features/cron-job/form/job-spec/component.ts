import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';

import { cloneDeep } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import {
  CONTAINER_FORM_CONFIG,
  ContainerFormConfig,
  POD_SPEC_FORM_CONFIG,
  PodSpecFormConfig,
} from 'app/features-shared/app/tokens';
import { JobSpec, JobSpecFormModel } from 'app/typings';

const podSpecFormConfig: PodSpecFormConfig = {
  hiddenFields: ['advanced'],
};

const containerFormConfig: ContainerFormConfig = {
  hiddenFields: ['health_check'],
};

@Component({
  selector: 'rc-job-spec-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: POD_SPEC_FORM_CONFIG,
      useValue: podSpecFormConfig,
    },
    {
      provide: CONTAINER_FORM_CONFIG,
      useValue: containerFormConfig,
    },
  ],
})
export class JobSpecFormComponent
  extends BaseResourceFormGroupComponent<JobSpec, JobSpecFormModel>
  implements OnInit {
  @Input()
  project: string;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  jobTypes = ['single_job', 'parallel_job', 'fixed_number_job'];
  constructor(injector: Injector) {
    super(injector);
  }

  getDefaultFormModel(): JobSpecFormModel {
    return {
      parallelism: 1,
      completions: 1,
      backoffLimit: 6,
      template: {
        spec: {
          restartPolicy: 'Never',
          containers: [],
        },
      },
    };
  }

  ngOnInit() {
    super.ngOnInit();
    this.form.get('jobType').valueChanges.subscribe(_value => {
      this.jobTypeChange(_value);
    });
  }

  jobTypeChange(value: string) {
    this.form.get('completions').enable({ emitEvent: false });
    this.form.get('parallelism').enable({ emitEvent: false });
    if (value === 'single_job') {
      this.form.get('completions').disable({ emitEvent: false });
      this.form.get('parallelism').disable({ emitEvent: false });
    }
    if (value === 'parallel_job') {
      this.form.get('completions').disable({ emitEvent: false });
    }
  }

  createForm() {
    const template = this.fb.group({
      spec: this.fb.control({}),
    });
    return this.fb.group({
      jobType: this.fb.control('single_job'),
      parallelism: this.fb.control(1),
      completions: this.fb.control(1),
      backoffLimit: this.fb.control(6),
      startingDeadlineSeconds: this.fb.control(7200),
      activeDeadlineSeconds: this.fb.control(5184000),
      template,
    });
  }

  adaptFormModel(formModel: JobSpecFormModel): JobSpec {
    if (formModel) {
      const resource: JobSpec = cloneDeep(formModel);
      const jobType = formModel.jobType;
      delete resource['jobType'];
      if (jobType === 'single_job') {
        delete resource.completions;
        delete resource.parallelism;
      }
      if (jobType === 'parallel_job') {
        delete resource.completions;
      }
      return resource;
    }
    return formModel;
  }

  adaptResourceModel(resource: JobSpec): JobSpecFormModel {
    if (resource) {
      const formModel: JobSpecFormModel = cloneDeep(resource);
      formModel.jobType = 'single_job';
      if (formModel.completions) {
        formModel.jobType = 'fixed_number_job';
        this.jobTypeChange(formModel.jobType);
        return formModel;
      }
      if (formModel.parallelism) {
        formModel.jobType = 'parallel_job';
        this.jobTypeChange(formModel.jobType);
        return formModel;
      }
      return formModel;
    }
    return resource;
  }
}
