import { DialogService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, Observable, Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { AppShardService } from 'app/features-shared/app/app-shard.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  CronJob,
  Job,
  K8sResourceWithActions,
  RcImageSelection,
} from 'app/typings';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  providers: [ResourceListPermissionsService],
})
export class CronJobListComponent
  extends BaseResourceListComponent<K8sResourceWithActions<CronJob>>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<any>();
  private clusterName: string;
  private namespaceName: string;
  project: string;
  createEnabled = false;
  RESOURCE_TYPE = 'cronjobs';
  lang: string;
  columns = [
    'name',
    'execute_type',
    'next_trig_time',
    'last_schedule_time',
    'create_time',
    'action',
  ];
  initialized: boolean;
  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private k8sResourceService: K8sResourceService,
    private workspaceComponent: WorkspaceComponent,
    private roleUtil: RoleUtilitiesService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private appShardService: AppShardService,
    public resourcePermissionService: ResourceListPermissionsService,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.project = params['project'];
    this.resourcePermissionService.initContext({
      resourceType: 'k8s_others',
      context: {
        project_name: params.project,
        cluster_name: params.cluster,
        namespace_name: params.namespace,
      },
    });
    this.roleUtil
      .resourceTypeSupportPermissions(
        'k8s_others',
        {
          cluster_name: this.clusterName,
          namespace_name: this.namespaceName,
          project_name: params['project'],
        },
        'create',
      )
      .then(res => (this.createEnabled = res));
    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
    this.translate.currentLang$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(_lang => {
        if (_lang === 'zh_cn') {
          this.lang = 'zh-cn';
        }
        if (_lang === 'en') {
          this.lang = 'en';
        }
      });
  }
  ngOnDestroy() {
    this.onDestroy$.next();
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.k8sResourceService.getK8sResourcesPaged(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        namespace: this.namespaceName,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
      }),
    );
  }

  canShowUpdate(item: K8sResourceWithActions<CronJob>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'update');
  }

  canShowDelete(item: K8sResourceWithActions<CronJob>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'delete');
  }

  update(item: K8sResourceWithActions<CronJob>) {
    this.router.navigate(
      ['cron_job', 'update', item.kubernetes.metadata.name],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      },
    );
  }

  openDetail(item: K8sResourceWithActions<CronJob>) {
    this.router.navigate(
      ['cron_job', 'detail', item.kubernetes.metadata.name],
      {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      },
    );
  }

  async executeNow(item: K8sResourceWithActions<CronJob>) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('cron_job_execute_now_confirm', {
          name: ` "${item.kubernetes.metadata.name}" `,
        }),
        confirmText: this.translate.get('execute_now'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    try {
      const job: Job = await this.k8sResourceService.manualExecJob({
        cluster: this.clusterName,
        namespace: this.namespaceName,
        name: item.kubernetes.metadata.name,
      });
      this.router.navigate(['job_record', 'detail', job.metadata.name], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  async delete(item: K8sResourceWithActions<CronJob>) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_cron_job_confirm', {
          name: ` "${item.kubernetes.metadata.name}" `,
        }),
        content: this.translate.get('delete_cron_job_tip'),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        item.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translate.get('success'),
        content: this.translate.get('delete_cron_job_success', {
          name: item.kubernetes.metadata.name,
        }),
      });
      this.onUpdate(null);
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  getDisplayName(rowData: K8sResourceWithActions) {
    return rowData.kubernetes.metadata.annotations
      ? rowData.kubernetes.metadata.annotations[
          'resource.alauda.io/display-name'
        ]
      : '';
  }

  create() {
    this.appShardService
      .selectImage({ projectName: this.project })
      .subscribe((res: RcImageSelection) => {
        if (res) {
          this.router.navigate(['cron_job', 'create'], {
            relativeTo: this.workspaceComponent.baseActivatedRoute,
            queryParams: res,
          });
        }
      });
  }

  pageChanged(param: string, value: number) {
    this.onPageEvent({ [param]: value });
  }

  trackByFn(_index: number, item: K8sResourceWithActions<CronJob>) {
    return item.kubernetes.metadata.uid;
  }
}
