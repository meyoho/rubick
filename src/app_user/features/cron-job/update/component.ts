import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { CronJob, K8sResourceWithActions } from 'app/typings';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  template:
    '<rc-cron-job-form-container *ngIf="data" [data]="data"></rc-cron-job-form-container>',
  styles: [
    `
      :host {
        display: flex !important;
        flex-direction: column;
        flex: 1;
      }
    `,
  ],
})
export class CronJobUpdateComponent implements OnInit {
  private RESOURCE_TYPE = 'cronjobs';
  clusterName: string;
  namespaceName: string;
  data: K8sResourceWithActions<CronJob>;
  name: string;
  parentPatn = 'cron_job';
  constructor(
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.name = params['name'];
    const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = workspaceParams['cluster'];
    this.namespaceName = workspaceParams['namespace'];
    this.getResourceDetail();
  }

  async getResourceDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail(
        this.RESOURCE_TYPE,
        {
          clusterName: this.clusterName,
          name: this.name,
          namespace: this.namespaceName,
        },
      );
      this.data = result;
    } catch (err) {
      this.errorsToastService.error(err);
      // this.jumpToListPage();
    }
  }

  jumpToListPage() {
    this.router.navigate([this.parentPatn], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
