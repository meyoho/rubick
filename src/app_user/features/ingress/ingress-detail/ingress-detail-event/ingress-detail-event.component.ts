import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Ingress, K8sResourceWithActions } from 'app/typings';

interface Filters {
  or: {
    and: {
      term?: {
        kind?: string;
        name?: string;
      };
      regexp?: {
        name: string;
      };
    }[];
  }[];
}
@Component({
  selector: 'rc-ingress-detail-event',
  templateUrl: './ingress-detail-event.component.html',
})
export class IngressDetailEventComponent implements OnInit, OnChanges {
  @Input()
  appData: K8sResourceWithActions<Ingress>;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  initialized = false;
  empty = false;
  filters: Filters = {
    or: [],
  };
  filterString: string;
  name: string;
  ingress: Ingress;
  constructor() {}

  ngOnInit() {}
  ngOnChanges({ appData }: SimpleChanges) {
    if (appData && appData.currentValue) {
      this.extractAllResources();
      this.empty = !this.appData;
      this.initialized = true;
    }
  }

  extractAllResources() {
    this.name = this.appData.kubernetes.metadata.name;
    this.produceFilter(this.appData);
    this.filterString = JSON.stringify(this.filters);
  }

  produceFilter(item: K8sResourceWithActions<Ingress>) {
    this.filters.or = [
      {
        and: [
          {
            term: {
              kind: item.kubernetes.kind,
            },
          },
          {
            term: {
              name: item.kubernetes.metadata.name,
            },
          },
        ],
      },
    ];
  }
}
