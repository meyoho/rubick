import { Component, Input, OnInit } from '@angular/core';
import { ObjectMeta } from 'app/typings';
import { get } from 'lodash-es';

@Component({
  selector: 'rc-ingress-detail-info',
  templateUrl: './ingress-detail-info.component.html',
  styleUrls: ['./ingress-detail-info.component.scss'],
})
export class IngressDetailInfoComponent implements OnInit {
  @Input()
  data: ObjectMeta;
  constructor() {}
  ngOnInit() {}

  getController() {
    return get(this.data.annotations, 'kubernetes.io/ingress.class');
  }
}
