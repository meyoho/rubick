import { Component, Input, OnInit } from '@angular/core';

import { IngressRule } from 'app/typings';
interface IngressRules {
  host: string;
  service: string;
  port: string | number;
}
@Component({
  selector: 'rc-ingress-detail-rules',
  templateUrl: './ingress-detail-rules.component.html',
  styleUrls: ['../ingress-detail-info/ingress-detail-info.component.scss'],
})
export class IngressDetailRulesComponent implements OnInit {
  columns = ['host', 'service', 'port'];
  @Input()
  data: IngressRule[];
  transformData: IngressRules[];
  constructor() {}
  ngOnInit() {
    this.transformDataFn();
  }

  transformDataFn() {
    const data = this.data.flatMap(item => {
      if (item.http && item.http.paths.length) {
        return item.http.paths.map(path => ({
          host: `${item.host || ''}${path.path || ''}`,
          service: path.backend.serviceName,
          port: path.backend.servicePort,
        }));
      } else {
        return {
          host: item.host,
          service: '',
          port: '',
        };
      }
    });
    this.transformData = data;
  }
}
