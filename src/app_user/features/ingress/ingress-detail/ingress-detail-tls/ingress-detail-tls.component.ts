import { Component, Input, OnInit } from '@angular/core';
import { IngressTLS } from 'app/typings';

@Component({
  selector: 'rc-ingress-detail-tls',
  templateUrl: './ingress-detail-tls.component.html',
  styleUrls: ['../ingress-detail-info/ingress-detail-info.component.scss'],
})
export class IngressDetailTlsComponent implements OnInit {
  @Input()
  data: IngressTLS[];
  columns = ['secretName', 'hosts'];
  constructor() {}

  ngOnInit() {
    if (!this.data) {
      this.data = [];
      return;
    }
    if (this.data && this.data.length === 1) {
      const arr = Object.keys(this.data[0]);
      if (arr.length < 1) {
        this.data = [];
      }
    }
  }
}
