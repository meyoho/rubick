import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  Ingress,
  IngressRule,
  IngressTLS,
  K8sResourceWithActions,
  ObjectMeta,
} from 'app/typings';
import * as codeEditorUtil from 'app/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import * as jsyaml from 'js-yaml';
import { cloneDeep } from 'lodash-es';

@Component({
  templateUrl: './ingress-detail.component.html',
  styleUrls: ['./ingress-detail.component.scss'],
})
export class IngressDetailComponent implements OnInit {
  dataInfo: ObjectMeta;
  dataRules: IngressRule[];
  dataTls: IngressTLS[];
  clusterName: string;
  namespaceName: string;
  name: string;
  ingressYaml: string;
  private RESOURCE_TYPE = 'ingresses';
  codeEditorOptions = codeEditorUtil.yamlOptions;
  actionsConfigView = codeEditorUtil.viewActions;
  initialized = false;
  loading = false;
  result: K8sResourceWithActions<Ingress>;

  constructor(
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.name = params['name'];
    const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = workspaceParams['cluster'];
    this.namespaceName = workspaceParams['namespace'];
    this.getIngressDetail();
  }

  canShowUpdate(item: K8sResourceWithActions<Ingress>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'update');
  }

  canShowDelete(item: K8sResourceWithActions<Ingress>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'delete');
  }

  getIngressYaml(data: Ingress) {
    this.ingressYaml = this.formToYaml(data);
  }

  formToYaml(formModel: any) {
    try {
      const json = cloneDeep(formModel);
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  async getIngressDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail(
        this.RESOURCE_TYPE,
        {
          clusterName: this.clusterName,
          name: this.name,
          namespace: this.namespaceName,
        },
      );
      this.result = result;
      this.getIngressYaml(result.kubernetes);
      this.dataInfo = result.kubernetes.metadata || {};
      this.dataRules = result.kubernetes.spec.rules || [];
      this.dataTls = result.kubernetes.spec.tls;
      this.initialized = true;
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate(['ingress'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }

  jumpToListPage() {
    this.router.navigate(['ingress'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  update() {
    this.router.navigate(['ingress', 'update', this.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
  async delete() {
    const result = this.result;
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('delete_kube_ingress_confirm', {
          name: result.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.loading = true;
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        result.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translate.get('success'),
        content: this.translate.get('delete_kube_ingress_success', {
          name: result.kubernetes.metadata.name,
        }),
      });
      this.jumpToListPage();
      this.loading = false;
    } catch (e) {
      this.loading = false;
      this.errorsToastService.error(e);
    }
  }
}
