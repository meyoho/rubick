import { NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import { safeDump, safeLoad } from 'js-yaml';
import { cloneDeep } from 'lodash-es';

import { Domain, DomainService } from 'app/services/api/domain.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { Ingress, K8sResourceWithActions } from 'app/typings';
import { K8S_SERVICE_NAME } from 'app/utils/patterns';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  selector: 'rc-ingress-form-container',
  templateUrl: './ingress-form-container.component.html',
  styleUrls: ['./ingress-form-container.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressFormContainerComponent implements OnInit {
  @Input()
  data: K8sResourceWithActions<Ingress>;
  @ViewChild(NgForm)
  form: NgForm;
  clusterName: string;
  namespaceName: string;
  private RESOURCE_TYPE = 'ingresses';
  ingressYaml: string;
  originalYaml: string;
  initialized = false;
  domain: string;
  submitting = false;
  resourceNameReg = K8S_SERVICE_NAME;
  formModel = 'UI';
  ingressModel: Ingress;
  domains: Domain[];
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };

  constructor(
    private workspaceComponent: WorkspaceComponent,
    private location: Location,
    private auiNotificationService: NotificationService,
    private K8sResourceService: K8sResourceService,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private translateService: TranslateService,
    private domainService: DomainService,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    const domains = await this.domainService.getDomains(false);
    this.domains = domains.results;
    this.initialized = true;
    this.cdr.markForCheck();
    this.updateEntrance();
  }

  updateEntrance() {
    if (this.data) {
      this.ingressModel = this.data.kubernetes;
    } else {
      this.ingressModel = this.genDefaultIngress();
    }
  }

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        this.originalYaml = this.ingressYaml = this.formToYaml(
          this.ingressModel,
        );
        break;
      case 'UI':
        const k8sIngress = this.yamlToForm(this.ingressYaml);
        this.ingressModel = k8sIngress;
        break;
    }
  }

  formChange(form: Ingress) {
    this.ingressModel = form;
  }

  formToYaml(formModel: any) {
    try {
      const json = cloneDeep(formModel);
      const yaml = safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  yamlToForm(yaml: string) {
    let formModel: any;
    try {
      formModel = safeLoad(yaml);
    } catch (e) {
      this.auiNotificationService.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    this.ingressModel = formModel;
    return this.ingressModel;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    const payload = this.genPayload();
    if (!payload) {
      this.submitting = false;
      return;
    }
    let notifyContent: string;
    try {
      if (!this.data) {
        notifyContent = 'create_kube_ingress_success';
        await this.K8sResourceService.createK8sReource(
          this.clusterName,
          this.RESOURCE_TYPE,
          payload,
        );
        this.router.navigate(['ingress'], {
          relativeTo: this.workspaceComponent.baseActivatedRoute,
        });
      } else {
        notifyContent = 'update_kube_ingress_success';
        await this.K8sResourceService.updateK8sReource(
          this.clusterName,
          this.RESOURCE_TYPE,
          payload,
        );
        this.location.back();
      }

      this.auiNotificationService.success({
        title: this.translateService.get('success'),
        content: this.translateService.get(notifyContent, {
          name: payload.metadata.name || '',
        }),
      });
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.submitting = false;
    }
    this.cdr.markForCheck();
  }

  cancel() {
    this.location.back();
  }

  genPayload() {
    if (this.formModel === 'YAML') {
      return this.genK8sIngressFromYaml();
    }
    //merge domain data into host before submit, for some history reason
    this.mergeDomain(this.ingressModel);
    return this.ingressModel;
  }
  //merge domain and host
  mergeDomain(formModel: Ingress): void {
    const rules = formModel.spec.rules;
    if (Array.isArray(rules)) {
      rules.forEach(rule => {
        //merge if exist
        if (rule.domain) {
          rule.host = rule.domain + '.' + rule.host.substring(2);
        }
      });
    }
  }

  genK8sIngressFromYaml(): Ingress {
    let sampleYaml: Ingress;
    try {
      sampleYaml = safeLoad(this.ingressYaml) || {};
      if (sampleYaml && sampleYaml.metadata) {
        sampleYaml.metadata.namespace = this.namespaceName;
      }
    } catch (err) {
      this.auiNotificationService.error(
        this.translateService.get('sample_error'),
      );
      return;
    }
    return sampleYaml;
  }
  genDefaultIngress(): Ingress {
    return {
      apiVersion: 'extensions/v1beta1',
      kind: 'Ingress',
      metadata: {
        name: '',
        namespace: this.namespaceName,
        annotations: {
          'kubernetes.io/ingress.class': '',
        },
      },
      spec: {
        tls: [
          {
            hosts: [],
            secretName: '',
          },
        ],
        rules: [
          {
            host: '',
            domain: '',
            http: {
              paths: [
                {
                  path: '',
                  backend: {
                    serviceName: '',
                    servicePort: '',
                  },
                },
              ],
            },
          },
        ],
      },
    };
  }
}
