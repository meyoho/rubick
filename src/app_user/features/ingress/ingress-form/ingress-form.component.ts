import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { FormArray, Validators } from '@angular/forms';

import { cloneDeep, get } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { Domain } from 'app/services/api/domain.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import {
  Ingress,
  IngressFormModel,
  IngressRule,
  IngressRuleFormModel,
  IngressSpec,
  IngressTLS,
  K8sResourceWithActions,
  Service,
} from 'app/typings';
import { K8S_SERVICE_NAME } from 'app/utils/patterns';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  selector: 'rc-ingress-form',
  templateUrl: './ingress-form.component.html',
  styleUrls: ['./ingress-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressFormComponent
  extends BaseResourceFormGroupComponent<Ingress, IngressFormModel>
  implements OnInit {
  @Input()
  isUpdate: boolean;

  @Input()
  domains: Domain[];
  clusterName: string;
  namespaceName: string;

  resourceNameReg = K8S_SERVICE_NAME;
  services: K8sResourceWithActions<Service>[] = [];
  secretNames: string[] = [];

  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };

  constructor(
    injector: Injector,
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.getServiceDropdowm();
    this.getSecrets();
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(/^[a-z0-9][a-z0-9-]*[a-z0-9]$/),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      annotations: this.fb.group({
        'kubernetes.io/ingress.class': this.fb.control(''),
      }),
    });
    const specForm = this.fb.group({
      tls: this.fb.control([]),
      rules: this.fb.array([]),
    });
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: metadataForm,
      spec: specForm,
    });
  }

  getDefaultFormModel(): any {
    return {
      apiVersion: 'extensions/v1beta1',
      kind: 'Ingress',
      metadata: {
        name: '',
        namespace: this.namespaceName,
        annotations: {
          'kubernetes.io/ingress.class': '',
        },
      },
    };
  }

  async getServiceDropdowm() {
    const serviceList = await this.k8sResourceService.getK8sResources(
      'services',
      {
        clusterName: this.clusterName,
        namespace: this.namespaceName,
      },
    );
    this.services = serviceList;
    this.cdr.markForCheck();
  }

  private getDomainContent(domain: string) {
    const _domain = domain || '';
    if (_domain.startsWith('*.')) {
      return {
        content: _domain.substring(1),
        isDomain: true,
      };
    }
    return {
      content: _domain,
      isDomain: false,
    };
  }

  async getSecrets() {
    const secrets = await this.k8sResourceService.getK8sResources('secrets', {
      clusterName: this.clusterName,
      namespace: this.namespaceName,
    });
    this.secretNames = secrets.map(item => item.kubernetes.metadata.name);
    this.cdr.markForCheck();
  }

  adaptFormModel(form: IngressFormModel): Ingress {
    const spec = (cloneDeep(form.spec) || {}) as IngressSpec;
    const tls: IngressTLS[] = [];
    if (spec.backend && !spec.backend.serviceName) {
      delete spec.backend;
    }
    const rules = get(spec, 'rules', []) as IngressRuleFormModel[];
    rules.forEach((rule: IngressRuleFormModel) => {
      this.mergeDomainHost(rule);
      if (rule.http && (!rule.http.paths || !rule.http.paths.length)) {
        delete rule.http;
      }
      if (rule.secretName) {
        const _tls = tls.find(_item => _item.secretName === rule.secretName);
        if (_tls) {
          _tls.hosts.push(rule.host);
        } else {
          tls.push({
            secretName: rule.secretName,
            hosts: [rule.host],
          });
        }
      }
      //in case of rule.secretName been defined as undefined or "",delete it anyway
      delete rule.secretName;
    });
    if (tls.length) {
      spec.tls = tls;
    } else {
      delete spec.tls;
    }
    return { ...form, spec };
  }

  adaptResourceModel(resource: Ingress): IngressFormModel {
    if (resource) {
      const spec = cloneDeep(resource.spec) || {};
      if (spec.tls && spec.tls.length) {
        const tlsMap = {};
        spec.tls.forEach(_tls => {
          if (Array.isArray(_tls.hosts)) {
            _tls.hosts.forEach(host => {
              tlsMap[host] = _tls.secretName;
            });
          }
        });
        spec.rules.forEach((rule: IngressRuleFormModel) => {
          const result = this.getHostValue(rule.host, tlsMap);
          if (result) {
            rule.secretName = result;
          }
        });
      }
      //in case none spec.tls
      if (spec.rules && spec.rules.length) {
        spec.rules.forEach(rule => {
          this.splitDomainHost(rule);
        });
      }
      return { ...resource, spec };
    }
    return resource;
  }

  private getHostValue(name: string, map: object) {
    if (!name) {
      return '';
    }
    for (const key in map) {
      if (map.hasOwnProperty(key)) {
        if (
          (key.startsWith('*.') && name.endsWith(key.substring(2))) ||
          key === name
        ) {
          return map[key];
        }
      }
    }
    return '';
  }

  /**
   * merge domain property into host
   * @param rule
   */
  private mergeDomainHost(rule: IngressRuleFormModel) {
    const hostContent = rule.host || '';
    const domainContent = rule.domain || '';
    // 非泛域名，直接删除domain
    if (!domainContent || !hostContent.startsWith('*.')) {
      delete rule.domain;
      return rule;
    }
    // 泛域名
    rule.host = hostContent.replace('*', domainContent);
    delete rule.domain;
    //no need to change,remain rule
  }
  /**
   * split host property into host and domain
   * @param rule
   */
  private splitDomainHost(rule: IngressRule) {
    const hostContent = rule.host || '';
    const domainContent = rule.domain || '';
    if (!domainContent) {
      //check whether exists in host and split properly
      let suffixMay = '';
      if (Array.isArray(this.domains)) {
        // 去已有domain中进行匹配
        this.domains.forEach(domain => {
          const domainContent = this.getDomainContent(domain.domain);
          if (
            domainContent.isDomain &&
            hostContent.endsWith(domainContent.content)
          ) {
            suffixMay = `*${domainContent.content}`;
          } else if (
            !domainContent.isDomain &&
            hostContent.endsWith(domainContent.content)
          ) {
            suffixMay = `${domainContent.content}`;
          }
          //suffixMay remains "" when !hostContent.endsWith(domainContent.content),nothing changes
        });
        if (suffixMay) {
          let result = hostContent.substring(
            0,
            hostContent.indexOf(suffixMay.substring(1)),
          );
          //when domainType,result would start with '*.'
          if (!suffixMay.startsWith('*.') || !result || result === '*') {
            //prevent '*' shows in domain
            result = '';
          }
          Object.assign(rule, {
            host: suffixMay,
            domain: result,
          });
        }
        //no match,remain rule
      }
    }
    //no need to change,remain rule
  }

  remove(index: number) {
    const arr = this.form.get('spec.rules') as FormArray;
    arr.removeAt(index);
  }

  add() {
    const arr = this.form.get('spec.rules') as FormArray;
    arr.push(
      this.fb.control({
        domain: '',
        host: '',
      }),
    );
  }
}
