import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { HTTPIngressPath } from 'app/typings';
import { K8sResourceWithActions, Service, ServicePort } from 'app/typings';
import { INGRESS_PATH_PATTERN } from 'app/utils/patterns';
import { get } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

@Component({
  selector: 'rc-ingress-paths-form',
  templateUrl: './ingress-paths-form.component.html',
  styleUrls: ['./ingress-paths-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressPathsFormComponent
  extends BaseResourceFormArrayComponent<HTTPIngressPath>
  implements OnInit {
  @Input()
  services: K8sResourceWithActions<Service>[];
  pathReg = INGRESS_PATH_PATTERN;

  constructor(injector: Injector) {
    super(injector);
  }

  ngOnInit() {}

  getDefaultFormModel(): HTTPIngressPath[] {
    return [
      {
        path: '',
        backend: {
          serviceName: '',
          servicePort: '',
        },
      },
    ];
  }

  createForm() {
    return this.fb.array([]);
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  adaptResourceModel(resource: HTTPIngressPath[] = []) {
    const res = resource.map(item => {
      if (!item.path) {
        item.path = '';
      }
      return item;
    });
    return res;
  }

  private createNewControl() {
    return this.fb.group({
      path: this.fb.control('', [Validators.pattern(this.pathReg.pattern)]),
      backend: this.fb.group({
        serviceName: this.fb.control('', [Validators.required]),
        servicePort: this.fb.control('', [Validators.required]),
      }),
    });
  }

  serviceChanged(control: any) {
    const service = control.get('backend.serviceName').value;
    const port = get(this.getPorts(service)[0], 'port');
    control.get('backend.servicePort').setValue(port);
  }

  getPorts(serviceName: string): ServicePort[] {
    const item = this.services.find(
      service => service.kubernetes.metadata.name === serviceName,
    );
    return item ? item.kubernetes.spec.ports : [];
  }
}
