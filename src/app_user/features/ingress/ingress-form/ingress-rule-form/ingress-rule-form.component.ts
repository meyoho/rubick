import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { Domain } from 'app/services/api/domain.service';
import {
  IngressRuleFormModel,
  K8sResourceWithActions,
  Service,
} from 'app/typings';
import { INGRESS_DOMAIN_PATTERN } from 'app/utils/patterns';

@Component({
  selector: 'rc-ingress-rule-form',
  templateUrl: './ingress-rule-form.component.html',
  styleUrls: ['./ingress-rule-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class IngressRuleFormComponent
  extends BaseResourceFormGroupComponent<IngressRuleFormModel>
  implements OnInit {
  @Input()
  domains: Domain[];
  @Input()
  services: K8sResourceWithActions<Service>[];
  @Input()
  secretNames: string[];
  domain: any = [];

  constructor(injector: Injector) {
    super(injector);
  }
  domainPattern = INGRESS_DOMAIN_PATTERN;
  ngOnInit() {
    this.form.get('host').valueChanges.subscribe(() => {
      this.getDomainDisable();
    });
    this.getDomainDisable();
  }

  createForm() {
    return this.fb.group({
      domain: this.fb.control(
        {
          value: '',
        },
        [Validators.required],
      ),
      secretName: this.fb.control(''),
      host: this.fb.control('', [Validators.required]),
      http: this.fb.group({
        paths: this.fb.control([]),
      }),
    });
  }

  getDefaultFormModel(): IngressRuleFormModel {
    return {
      host: '',
      domain: '',
      secretName: '',
      http: {
        paths: [],
      },
    };
  }

  getDomainDisable() {
    const host = this.form.get('host').value;
    if (host.startsWith('*.')) {
      this.form.get('domain').enable();
    } else {
      this.form.get('domain').disable();
      this.form.get('domain').setValue('');
    }
  }

  filterHost(value: string) {
    return value.startsWith('*.') ? value.slice(2) : value;
  }
}
