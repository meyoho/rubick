import { Component, Input, OnInit } from '@angular/core';

import { IngressRule } from 'app/typings';
interface IngressRules {
  host: string;
  service: string;
  port: string | number;
}
@Component({
  selector: 'rc-ingress-list-rules',
  templateUrl: './ingress-list-rules.component.html',
  styleUrls: ['./ingress-list-rules.component.scss'],
})
export class IngressListRulesComponent implements OnInit {
  @Input()
  rules: IngressRule[] = [];
  @Input()
  visibleTotal = true;
  @Input()
  maxLen = 3;
  data: IngressRules[] = [];
  total = 0;
  constructor() {}

  ngOnInit() {
    this.data = this.getdata();
  }
  get maxLength() {
    return this.maxLen;
  }

  getdata(): IngressRules[] {
    if (this.maxLength) {
      return [...this.transformDataFn()].splice(0, this.maxLength);
    }
    return [...this.transformDataFn()];
  }

  transformDataFn() {
    if (!this.rules) {
      return [];
    }
    const data = this.rules.flatMap(item => {
      if (item.http && item.http.paths.length) {
        return item.http.paths.map(path => ({
          host: `${item.host || ''}${path.path || ''}`,
          service: path.backend.serviceName,
          port: path.backend.servicePort,
        }));
      } else {
        return {
          host: item.host,
          service: '',
          port: '',
        };
      }
    });
    this.total = data.length;
    return data;
  }
}
