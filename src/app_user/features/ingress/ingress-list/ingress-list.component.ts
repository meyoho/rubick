import { DialogService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { Ingress, K8sResourceWithActions, Service } from 'app/typings';
import { ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { Observable, Subject, from } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
@Component({
  templateUrl: './ingress-list.component.html',
  styleUrls: ['./ingress-list.component.scss'],
  providers: [ResourceListPermissionsService],
})
export class IngressListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<any>();
  notificationVisible = true;
  private clusterName: string;
  private namespaceName: string;
  createEnabled = false;
  RESOURCE_TYPE = 'ingresses';
  columns = ['name', 'rule', 'action'];
  initialized: boolean;
  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private k8sResourceService: K8sResourceService,
    private workspaceComponent: WorkspaceComponent,
    private roleUtil: RoleUtilitiesService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    public resourcePermissionService: ResourceListPermissionsService,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.resourcePermissionService.initContext({
      resourceType: 'k8s_others',
      context: {
        project_name: params.project,
        cluster_name: params.cluster,
        namespace_name: params.namespace,
      },
    });
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.roleUtil
      .resourceTypeSupportPermissions(
        'k8s_others',
        {
          cluster_name: this.clusterName,
          namespace_name: this.namespaceName,
          project_name: params['project'],
        },
        'create',
      )
      .then(res => (this.createEnabled = res));
    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
  }
  ngOnDestroy() {
    this.onDestroy$.next();
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.k8sResourceService.getK8sResourcesPaged(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        namespace: this.namespaceName,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
      }),
    );
  }

  canShowUpdate(item: K8sResourceWithActions<Service>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'update');
  }

  canShowDelete(item: K8sResourceWithActions<Service>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'delete');
  }

  update(item: K8sResourceWithActions<Service>) {
    this.router.navigate(['ingress', 'update', item.kubernetes.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  openDetail(item: K8sResourceWithActions<Service>) {
    this.router.navigate(['ingress', 'detail', item.kubernetes.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async delete(item: K8sResourceWithActions<Service>) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('delete_kube_ingress_confirm', {
          name: item.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        item.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translate.get('success'),
        content: this.translate.get('delete_kube_ingress_success', {
          name: item.kubernetes.metadata.name,
        }),
      });
      this.onUpdate(null);
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  createIngress() {
    this.router.navigate(['ingress', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  pageChanged(param: string, value: number) {
    this.onPageEvent({ [param]: value });
  }

  trackByFn(_index: number, item: K8sResourceWithActions<Ingress>) {
    return item.kubernetes.metadata.uid;
  }
}
