import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { Ingress, K8sResourceWithActions } from 'app/typings';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: './ingress-update.component.html',
})
export class IngressUpdateComponent implements OnInit {
  private RESOURCE_TYPE = 'ingresses';
  clusterName: string;
  namespaceName: string;
  data: K8sResourceWithActions<Ingress>;
  name: string;
  constructor(
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.name = params['name'];
    const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = workspaceParams['cluster'];
    this.namespaceName = workspaceParams['namespace'];
    this.getIngressDetail();
  }

  async getIngressDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail(
        this.RESOURCE_TYPE,
        {
          clusterName: this.clusterName,
          name: this.name,
          namespace: this.namespaceName,
        },
      );
      this.data = result;
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate(['ingress'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
  jumpToListPage() {
    this.router.navigate(['ingress'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
