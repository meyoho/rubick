import { NgModule } from '@angular/core';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';

import { IngressCreateComponent } from './ingress-create/ingress-create.component';
import { IngressDetailEventComponent } from './ingress-detail/ingress-detail-event/ingress-detail-event.component';
import { IngressDetailInfoComponent } from './ingress-detail/ingress-detail-info/ingress-detail-info.component';
import { IngressDetailRulesComponent } from './ingress-detail/ingress-detail-rules/ingress-detail-rules.component';
import { IngressDetailTlsComponent } from './ingress-detail/ingress-detail-tls/ingress-detail-tls.component';
import { IngressDetailComponent } from './ingress-detail/ingress-detail.component';
import { IngressFormContainerComponent } from './ingress-form-container/ingress-form-container.component';
import { IngressFormComponent } from './ingress-form/ingress-form.component';
import { IngressPathsFormComponent } from './ingress-form/ingress-paths-form/ingress-paths-form.component';
import { IngressRuleFormComponent } from './ingress-form/ingress-rule-form/ingress-rule-form.component';
import { IngressListRulesComponent } from './ingress-list/ingress-list-rules/ingress-list-rules.component';
import { IngressListComponent } from './ingress-list/ingress-list.component';
import { IngressUpdateComponent } from './ingress-update/ingress-update.component';
import { IngressRoutingModule } from './ingress.routing.module';

@NgModule({
  imports: [
    SharedModule,
    IngressRoutingModule,
    EventSharedModule,
    FormTableModule,
  ],
  declarations: [
    IngressListComponent,
    IngressListRulesComponent,
    IngressCreateComponent,
    IngressDetailComponent,
    IngressDetailInfoComponent,
    IngressDetailRulesComponent,
    IngressDetailTlsComponent,
    IngressUpdateComponent,
    IngressDetailEventComponent,
    IngressFormComponent,
    IngressRuleFormComponent,
    IngressPathsFormComponent,
    IngressFormContainerComponent,
  ],
})
export class IngressModule {}
