import { Action } from '@ngrx/store';

import { createTypes } from 'app2/features/jenkins/utils/types';

enum ActionTypes {
  Started = 'Started',
  Canceled = 'Canceled',
  Deleted = 'Deleted',
  Search = 'Search',
  SearchComplete = 'SearchSucc',
  SearchError = 'SearchError',
  Get = 'Get',
  GetComplete = 'GetSucc',
  GetError = 'GetFail',
  Delete = 'Delete',
  DeleteError = 'DeleteError',
  Start = 'Start',
  StartError = 'StartError',
  Cancel = 'Cancel',
  CancelError = 'CancelError',
}

export const types = createTypes('@jenkins.history', ActionTypes);

export class Started implements Action {
  public readonly type = types.Started;
  constructor(
    public id: string,
    public pipeline_uuid: string,
    public from: {
      id?: string;
      pipeline_uuid: string;
    },
  ) {}
}

export class Canceled implements Action {
  public readonly type = types.Canceled;
  constructor(public id: string, public pipeline_uuid: string) {}
}

export class Deleted implements Action {
  public readonly type = types.Deleted;
  constructor(public id: string, public pipeline_uuid: string) {}
}

export class Search implements Action {
  public readonly type = types.Search;
  constructor(
    public params: {
      search: string;
      jenkins_integration_id: string;
      page: number;
      page_size: number;
    },
    public ignoreError = false,
  ) {}
}

export class SearchComplete implements Action {
  public readonly type = types.SearchComplete;
  constructor(
    public params: {
      search: string;
      jenkins_integration_id: string;
      page: number;
      page_size: number;
    },
    public result: any,
  ) {}
}

export class SearchError implements Action {
  public readonly type = types.SearchError;
  constructor(
    public params: {
      search: string;
      jenkins_integration_id: string;
      page: number;
      page_size: number;
    },
    public error: any,
  ) {}
}

export class Get implements Action {
  public readonly type = types.Get;
  constructor(public id: string, public pipeline_uuid: string) {}
}

export class GetComplete implements Action {
  public readonly type = types.GetComplete;
  constructor(
    public id: string,
    public pipeline_uuid: string,
    public result: any,
  ) {}
}

export class GetError implements Action {
  public readonly type = types.GetError;
  constructor(
    public id: string,
    public pipeline_uuid: string,
    public error: any,
  ) {}
}

export class Delete implements Action {
  public readonly type = types.Delete;
  constructor(public id: string, public pipeline_uuid: string) {}
}

export class DeleteError implements Action {
  public readonly type = types.DeleteError;
  constructor(
    public id: string,
    public pipeline_uuid: string,
    public error: any,
  ) {}
}

export class Start implements Action {
  public readonly type = types.Start;
  constructor(public id: string, public pipeline_uuid: string) {}
}

export class StartError implements Action {
  public readonly type = types.StartError;
  constructor(
    public id: string,
    public pipeline_uuid: string,
    public error: any,
  ) {}
}

export class Cancel implements Action {
  public readonly type = types.Cancel;
  constructor(public id: string, public pipeline_uuid: string) {}
}

export class CancelError implements Action {
  public readonly type = types.CancelError;
  constructor(
    public id: string,
    public pipeline_uuid: string,
    public error: any,
  ) {}
}

export type All =
  | Started
  | Canceled
  | Deleted
  | Search
  | SearchComplete
  | SearchError
  | Get
  | GetComplete
  | GetError
  | Delete
  | DeleteError
  | Start
  | StartError
  | Cancel
  | CancelError;
