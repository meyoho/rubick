import { Action } from '@ngrx/store';

import { createTypes } from 'app2/features/jenkins/utils/types';

enum ActionTypes {
  Created = 'Created',
  Updated = 'Updated',
  Deleted = 'Deleted',
  Search = 'Search',
  SearchComplete = 'SearchSucc',
  SearchError = 'SearchError',
  Get = 'Get',
  GetComplete = 'GetSucc',
  GetError = 'GetFail',
  Delete = 'Delete',
  DeleteError = 'DeleteError',
  Start = 'Start',
  StartError = 'StartError',
  GetHistories = 'GetHistories',
  GetHistoriesComplete = 'GetHistoriesComplete',
  GetHistoriesError = 'GetHistoriesError',
  Renew = 'Renew',
}

export const types = createTypes('@jenkins.pipeline', ActionTypes);

//TODO: only for bug fix, remove later
export class Renew implements Action {
  public readonly type = types.Renew;
}

export class Created implements Action {
  public readonly type = types.Created;
  constructor(public id: string) {}
}

export class Updated implements Action {
  public readonly type = types.Updated;
  constructor(public id: string) {}
}

export class Deleted implements Action {
  public readonly type = types.Deleted;
  constructor(public id: string) {}
}

export class Search implements Action {
  public readonly type = types.Search;
  constructor(
    public params: {
      search: string;
      page: number;
      page_size: number;
      template_uuids?: string;
    },
    public ignoreError = false,
  ) {}
}

export class SearchComplete implements Action {
  public readonly type = types.SearchComplete;
  constructor(
    public params: { search: string; page: number; page_size: number },
    public result: any,
  ) {}
}

export class SearchError implements Action {
  public readonly type = types.SearchError;
  constructor(
    public params: { search: string; page: number; page_size: number },
    public error: any,
  ) {}
}

export class Get implements Action {
  public readonly type = types.Get;
  constructor(public id: string) {}
}

export class GetComplete implements Action {
  public readonly type = types.GetComplete;
  constructor(public id: string, public result: any) {}
}

export class GetError implements Action {
  public readonly type = types.GetError;
  constructor(public id: string, public error: any) {}
}

export class Delete implements Action {
  public readonly type = types.Delete;
  constructor(public id: string) {}
}

export class DeleteError implements Action {
  public readonly type = types.DeleteError;
  constructor(public id: string, public error: any) {}
}

export class Start implements Action {
  public readonly type = types.Start;
  constructor(public id: string) {}
}

export class StartError implements Action {
  public readonly type = types.StartError;
  constructor(public id: string, public error: any) {}
}

export class GetHistories implements Action {
  public readonly type = types.GetHistories;
  constructor(
    public id: string,
    public params: {
      jenkins_integration_id: string;
      page: number;
      page_size: number;
    },
    public ignoreError = false,
  ) {}
}

export class GetHistoriesComplete implements Action {
  public readonly type = types.GetHistoriesComplete;
  constructor(
    public id: string,
    public params: {
      jenkins_integration_id: string;
      page: number;
      page_size: number;
    },
    public result: any,
  ) {}
}

export class GetHistoriesError implements Action {
  public readonly type = types.GetHistoriesError;
  constructor(public id: string, public error: any) {}
}

export type All =
  | Renew
  | Created
  | Updated
  | Deleted
  | Search
  | SearchComplete
  | SearchError
  | Get
  | GetComplete
  | GetError
  | Delete
  | DeleteError
  | Start
  | StartError
  | GetHistories
  | GetHistoriesComplete
  | GetHistoriesError;
