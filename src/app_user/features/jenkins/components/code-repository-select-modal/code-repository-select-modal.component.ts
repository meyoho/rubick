import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { AfterViewInit, Component, Inject } from '@angular/core';

import { get } from 'lodash-es';
import { Observable, Subject, from as fromPromise, merge } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  scan,
  shareReplay,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { NotificationService } from '@alauda/ui';
import { PrivateBuildCodeClientService } from 'app/services/api/private-build-code-client.service';

interface State {
  results: any[];
  page: number;
  total: number;
  loading: boolean;
  selected: string;
  useSelected: false;
  input: string;
}

const initialState: State = {
  results: [],
  page: 1,
  total: 0,
  loading: false,
  selected: null,
  useSelected: false,
  input: '',
};

const load = ({ page }: { page: number }) => (state: State): State => ({
  ...state,
  page,
  loading: true,
  selected: null,
});

const loadComplete = ({
  results,
  page,
  total,
}: {
  results: any[];
  page: number;
  total: number;
}) => (state: State): State => {
  if (page !== state.page) {
    return state;
  }
  return {
    ...state,
    results,
    total,
    loading: false,
  };
};

const loadError = ({ page }: { page: number }) => (state: State): State => {
  if (page !== state.page) {
    return state;
  }
  return {
    ...state,
    results: [],
    loading: false,
  };
};

const select = ({ selected }: { selected: string }) => (
  state: State,
): State => ({
  ...state,
  selected,
});

const changeTab = ({ index }: { index: number }) => (state: State) => ({
  ...state,
  useSelected: index === 1,
});

const input = ({ value }: { value: string }) => (state: State) => ({
  ...state,
  input: value,
});

@Component({
  styleUrls: ['code-repository-select-modal.component.scss'],
  templateUrl: 'code-repository-select-modal.component.html',
})
export class CodeRepositorySelectModalComponent implements AfterViewInit {
  private state$: Observable<State>;
  results$: Observable<any[]>;
  page$: Observable<number>;
  total$: Observable<number>;
  loading$: Observable<boolean>;
  selected$: Observable<string>;
  input$: Observable<string>;
  selectedRepo$: Observable<string>;

  load$ = new Subject<void>();
  pageChange$ = new Subject<number>();
  select$ = new Subject<string>();
  tabChange$ = new Subject<any>();
  inputChange$ = new Subject<string>();
  pageSize = 8;
  private dataSource: (page: number) => Promise<any>;

  constructor(
    @Inject(DIALOG_DATA) public data: any,
    private dialogRef: DialogRef,
    private privateBuildCodeClientService: PrivateBuildCodeClientService,
    private auiNotificationService: NotificationService,
  ) {
    switch (data.codeClient) {
      case 'BITBUCKET':
        this.dataSource = this.bitbucket();
        break;
      case 'GITHUB':
        this.dataSource = this.github();
        break;
    }

    const loadAction$ = merge(
      this.load$.pipe(map(() => 1)),
      this.pageChange$,
    ).pipe(
      distinctUntilChanged(),
      switchMap(page =>
        fromPromise(this.getData(page)).pipe(startWith(load({ page }))),
      ),
    );

    const selectAction$ = this.select$.pipe(
      map(selected => select({ selected })),
    );
    const changeTabAction$ = this.tabChange$.pipe(
      map(({ index }) => changeTab({ index })),
    );
    const inputAction$ = this.inputChange$.pipe(map(value => input({ value })));

    const actions$ = merge(
      loadAction$,
      selectAction$,
      changeTabAction$,
      inputAction$,
    );

    this.state$ = actions$.pipe(
      scan((state, action: any) => action(state), initialState),
      shareReplay(1),
    );

    this.results$ = this.select(state => state.results);
    this.page$ = this.select(state => state.page);
    this.total$ = this.select(state => state.total);
    this.loading$ = this.select(state => state.loading);
    this.selected$ = this.select(state => state.selected);
    this.input$ = this.select(state => state.input);
    this.selectedRepo$ = this.select(state =>
      state.useSelected ? state.selected : state.input,
    );
  }

  async ngAfterViewInit() {
    await new Promise(resolve => setTimeout(() => resolve()));
    this.load$.next();
  }

  closeModal(repo: string = '') {
    this.dialogRef.close(repo);
  }

  private select<T>(selector: (state: State) => T) {
    return this.state$.pipe(
      map(selector),
      distinctUntilChanged(),
      shareReplay(1),
    );
  }

  private getData(page: number) {
    return this.dataSource(page)
      .then(response =>
        loadComplete({
          page,
          results: response.results,
          total: response.total,
        }),
      )
      .catch(error => {
        this.errorMessage(error);
        return loadError({ page });
      });
  }

  private bitbucket() {
    return (page: number) =>
      this.privateBuildCodeClientService.findBitbucketRepos(
        this.data.codeOrg,
        page,
        this.pageSize,
      );
  }

  private github() {
    let data: any[] = null;
    return async (page: number) => {
      if (!data) {
        data = await this.privateBuildCodeClientService.findGithubRepos(
          this.data.codeOrg,
        );
      }

      return {
        total: data.length,
        results: data.slice((page - 1) * this.pageSize, page * this.pageSize),
      };
    };
  }

  private errorMessage(error: any) {
    this.auiNotificationService.error(
      get(error, 'error.errors[0].message') || error.message || 'unknow error',
    );
  }
}
