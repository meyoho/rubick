import { DialogService, DialogSize } from '@alauda/ui';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import { Component, HostBinding, Input, Optional, Self } from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';
import { FormFieldControl } from 'app2/shared/form-field-control';
import { Subject } from 'rxjs';

import { CodeRepositorySelectModalComponent } from '../code-repository-select-modal';

let nextUniqueId = 0;

@Component({
  selector: 'rc-code-repository-select',
  styleUrls: [],
  templateUrl: 'code-repository-select.component.html',
  providers: [
    {
      provide: FormFieldControl,
      useExisting: CodeRepositorySelectComponent,
    },
  ],
})
export class CodeRepositorySelectComponent
  implements FormFieldControl, ControlValueAccessor {
  private _uniqueId = `rc-code-repository-select-${++nextUniqueId}`;
  private _required = false;
  private _disabled = false;
  private _value = '';
  stateChanges = new Subject<void>();

  @Input()
  codeClient: string;
  @Input()
  codeOrg: string;

  /** Unique id of the element. */
  @Input()
  get id() {
    return this._uniqueId;
  }

  set id(value: string) {
    this._uniqueId = value || this._uniqueId;
  }

  @Input()
  get value() {
    return this._value;
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(value: any) {
    this._disabled = coerceBooleanProperty(value);
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);
    return !!(isInvalid && (isDirty || isSubmitted));
  }

  onChange = (_value: string) => {};
  onTouched = () => {};

  writeValue(value: string) {
    this._value = value;
    this.onChange(value);
  }

  registerOnChange(fn: () => {}): void {
    this.onChange = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private dialogService: DialogService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  selectRepo() {
    const dialogRef = this.dialogService.open(
      CodeRepositorySelectModalComponent,
      {
        size: DialogSize.Big,
        data: {
          codeClient: this.codeClient,
          codeOrg: this.codeOrg,
        },
      },
    );

    dialogRef.afterClosed().subscribe((repository: string) => {
      if (repository) {
        this.writeValue(repository);
      }
    });
  }
}
