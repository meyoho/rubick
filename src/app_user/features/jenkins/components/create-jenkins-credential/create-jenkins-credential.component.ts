import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { Component, EventEmitter, Inject, Output } from '@angular/core';
import { FormBuilder, FormGroup } from '@angular/forms';

import { JenkinsService } from 'app/services/api/jenkins.service';
import { JenkinsMessageService } from '../../services/jenkins-message.service';

@Component({
  styleUrls: ['./create-jenkins-credential.component.scss'],
  templateUrl: './create-jenkins-credential.component.html',
})
export class CreateJenkinsCredentialComponent {
  @Output()
  close = new EventEmitter<string>();
  form: FormGroup;

  constructor(
    private jenkins: JenkinsService,
    private message: JenkinsMessageService,
    private fb: FormBuilder,
    @Inject(DIALOG_DATA) public data: any,
    private dialogRef: DialogRef,
  ) {
    this.form = this.fb.group({
      name: '',
      username: '',
      password: '',
    });
  }

  onSubmit() {
    if (this.form.status === 'VALID') {
      this.jenkins.credentials
        .create({
          ...this.form.value,
          type: 'Username with password',
          jenkins_integration_id: this.data.jenkins_integration_id,
        })
        .then((response: any) => {
          this.dialogRef.close(response.name);
        })
        .catch(error => this.message.error(error));
    }
  }

  closeModal() {
    this.dialogRef.close();
  }
}
