import { DIALOG_DATA } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { viewActions } from 'app/utils/code-editor-config';

@Component({
  templateUrl: './jenkinsfile-content-example.component.html',
  styleUrls: ['./jenkinsfile-content-example.component.scss'],
})
export class JenkinsfileContentExampleComponent {
  content: string;
  codeEditorOptions = {
    language: 'Jenkinsfile',
    readOnly: true,
  };
  actionsConfigView = {
    fullscreen: false,
    ...viewActions,
  };

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      content: string;
      title?: string;
    },
  ) {
    this.content = this.data.content;
  }
}
