import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  Directive,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Optional,
  Output,
  Self,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  FormGroupDirective,
  NG_VALIDATORS,
  NgControl,
  NgForm,
  Validator,
} from '@angular/forms';

import { find, get } from 'lodash-es';
import { Subject } from 'rxjs';

import { PodControllerKinds } from 'app/features-shared/app/utils/pod-controller';
import { AppService } from 'app/services/api/app.service';
import {
  NamespaceOption,
  NamespaceService,
} from 'app/services/api/namespace.service';
import { RegionService } from 'app/services/api/region.service';
import { K8sResourceWithActions } from 'app/typings/backend-api';
import { FormFieldControl } from 'app2/shared/form-field-control';

interface ApplicationContainerMix {
  clusterName: string;
  namespace: string;
  applicationName: string;
  componentName: string;
  componentType: string;
  containerName: string;
}
let nextUniqueId = 0;

@Component({
  selector: 'rc-pipeline-template-application-container',
  templateUrl: './pipeline-application-container.component.html',
  styleUrls: ['./pipeline-application-container.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: PipelineApplicationContainerComponent,
    },
  ],
})
export class PipelineApplicationContainerComponent
  implements FormFieldControl, ControlValueAccessor, OnInit {
  _model: ApplicationContainerMix;
  _uniqueId = `rc-pipeline-template-application-container-${++nextUniqueId}`;

  _required: any;
  _disabled: any;
  stateChanges = new Subject<void>();

  clusterOptions: any[] = [];
  namespaceOptions: NamespaceOption[] = [];
  applicationOptions: any[] = [];
  appComponentMap: {
    [key: string]: K8sResourceWithActions['kubernetes'][];
  } = {};
  componentOptions: any[] = [];
  containerOptions: any[] = [];

  clusterLoading = false;
  namespaceLoading = false;
  applicationLoading = false;
  componentLoading = false;
  containerLoading = false;

  editBindingMode = false;

  controlType = 'rc-pipeline-template-application-container';

  @Input()
  id = this._uniqueId;

  @Input()
  name: string | null = null;
  @Output()
  change = new EventEmitter<ApplicationContainerMix>();
  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private regionService: RegionService,
    private namespaceService: NamespaceService,
    private appService: AppService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this._model = {
      clusterName: '',
      namespace: '',
      applicationName: '',
      componentName: '',
      componentType: '',
      containerName: '',
    };
  }

  ngOnInit() {
    Promise.all([
      this.getClusters(),
      this.getNamespaces(),
      this.getApplications(),
    ]);
  }

  async getClusters() {
    this.clusterLoading = true;
    try {
      this.clusterOptions = await this.regionService.getRegions();
    } catch (e) {
      this.clusterOptions = [];
    }
    this.clusterLoading = false;
  }

  async getNamespaces() {
    if (!this.value || !this.value.clusterName) {
      return;
    }
    this.namespaceLoading = true;
    try {
      const region = await this.regionService.getCluster(
        this.value.clusterName,
      );
      this.namespaceOptions = await this.namespaceService.getNamespaceOptions(
        region.id,
      );
    } catch (e) {
      this.namespaceOptions = [];
    }
    this.namespaceLoading = false;
  }

  async getApplications() {
    if (!this.value || !this.value.clusterName || !this.value.namespace) {
      return;
    }
    this.applicationLoading = true;
    const appList: any[] = [];

    try {
      const { results } = await this.appService.getAppListCRD({
        cluster_name: this.value.clusterName,
        namespace: this.value.namespace,
        params: {},
      });

      results.forEach((itemList: K8sResourceWithActions[]) => {
        const componentList: K8sResourceWithActions['kubernetes'][] = [];
        let appName = '';
        itemList.forEach((item: K8sResourceWithActions) => {
          if (item.kubernetes.kind === 'Application') {
            appName = item.kubernetes.metadata.name;
            appList.push({
              name: appName,
            });
          }
          if (PodControllerKinds.includes(item.kubernetes.kind)) {
            componentList.push(item.kubernetes);
          }
        });
        this.appComponentMap[appName] = componentList.map(item => {
          return {
            name: item.metadata.name,
            ...item,
          };
        });
      });
      this.applicationOptions = appList;
      this.getComponents();
    } catch (e) {
      this.applicationOptions = [];
    }
    this.applicationLoading = false;
  }

  getComponents() {
    if (
      !this.value ||
      !this.value.clusterName ||
      !this.value.namespace ||
      !this.value.applicationName
    ) {
      return;
    }

    this.componentOptions =
      this.value &&
      this.value.applicationName &&
      this.appComponentMap[this.value.applicationName]
        ? this.appComponentMap[this.value.applicationName]
        : [];
    if (this.componentOptions.length) {
      this.getContainers();
    }
    this.componentLoading = false;
  }

  onApplicationChange(event: any) {
    if (typeof event === 'object') {
      this.getComponents();
    }
  }

  onComponentChange(event: any) {
    const target = find(this.componentOptions, { name: event });
    if (typeof target === 'object') {
      this.value.componentName = target.name;
      this.value.componentType = target.kind;
      this.getContainers();
    }
  }

  getContainers() {
    if (
      !this.value ||
      !this.value.clusterName ||
      !this.value.namespace ||
      !this.value.applicationName ||
      !this.value.componentName
    ) {
      return;
    }

    const currentComponent = this.componentOptions.find((item: any) => {
      return item.name === this.value.componentName;
    });
    if (!currentComponent) {
      return;
    }
    this.containerLoading = true;
    this.containerOptions = get(
      currentComponent,
      'spec.template.spec.containers',
      [],
    );
    this.containerLoading = false;
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }

    this._disabled = disabled;
    this.stateChanges.next();
  }

  @Input()
  get value(): ApplicationContainerMix {
    return this.ngControl ? this.ngControl.value : this._model;
  }

  set value(value: ApplicationContainerMix) {
    if (value === this.value) {
      return;
    }
    this.stateChanges.next();
    this.onChange(value);
    this.change.emit(value);
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);
    return !!(isInvalid && (isDirty || isSubmitted));
  }

  @Input()
  get clusterName() {
    return this.value ? this.value.clusterName : '';
  }
  set clusterName(clusterName: any) {
    if (this._model.clusterName && clusterName !== this._model.clusterName) {
      this.editBindingMode = false;
    }
    this.value = { ...this.value, clusterName };
    if (clusterName && this.clusterOptions.length) {
      if (!this.editBindingMode) {
        this.namespace = '';
        this.applicationName = '';
        this.componentName = '';
        this.componentType = '';
        this.containerName = '';
      }
      this.getNamespaces();
    }
  }

  @Input()
  get namespace() {
    return this.value ? this.value.namespace : '';
  }
  set namespace(namespace: string) {
    if (this._model.namespace && namespace !== this._model.namespace) {
      this.editBindingMode = false;
    }
    this.value = { ...this.value, namespace };
    if (namespace && this.namespaceOptions.length) {
      if (!this.editBindingMode) {
        this.applicationName = '';
        this.componentName = '';
        this.componentType = '';
        this.containerName = '';
      }
      this.getApplications();
    }
  }

  @Input()
  get applicationName() {
    return this.value ? this.value.applicationName : '';
  }
  set applicationName(applicationName: string) {
    if (
      this._model.applicationName &&
      applicationName !== this._model.applicationName
    ) {
      this.editBindingMode = false;
    }
    this.value = { ...this.value, applicationName };
    if (applicationName && this.applicationOptions.length) {
      if (!this.editBindingMode) {
        this.componentName = '';
        this.componentType = '';
        this.containerName = '';
      }
      this.getComponents();
    }
  }

  @Input()
  get componentName() {
    return this.value ? this.value.componentName : '';
  }
  set componentName(componentName: string) {
    if (
      this._model.componentName &&
      componentName !== this._model.componentName
    ) {
      this.editBindingMode = false;
    }
    this.value = { ...this.value, componentName };
    if (componentName && this.componentOptions.length) {
      if (!this.editBindingMode) {
        this.componentName = '';
        this.componentType = '';
        this.containerName = '';
      }
      this.getContainers();
    }
  }

  @Input()
  get componentType() {
    return this.value ? this.value.componentType : '';
  }
  set componentType(componentType: string) {
    if (
      this._model.componentType &&
      componentType !== this._model.componentType
    ) {
      this.editBindingMode = false;
    }
    this.value = { ...this.value, componentType };
    if (componentType && this.componentOptions.length) {
      if (!this.editBindingMode) {
        this.componentName = '';
        this.componentType = '';
        this.containerName = '';
      }
    }
  }

  @Input()
  get containerName() {
    return this.value ? this.value.containerName : '';
  }
  set containerName(containerName: string) {
    this.value = { ...this.value, containerName };
  }

  writeValue(value: any): void {
    this.editBindingMode = !!value;
    this._model = value || {
      clusterName: '',
      namespace: '',
      applicationName: '',
      componentName: '',
      componentType: '',
      containerName: '',
    };
  }

  onChange(value: any) {
    this.value = value;
  }

  registerOnChange(fn: (_: any) => void) {
    this.onChange = fn;
  }

  registerOnTouched() {}
}

@Directive({
  selector:
    'rc-pipeline-template-application-container[rcApplicationContainerValid]',
  providers: [
    {
      provide: NG_VALIDATORS,
      useExisting: PipelineApplicationContainerValidatorDirective,
      multi: true,
    },
  ],
})
export class PipelineApplicationContainerValidatorDirective
  implements Validator {
  validate(control: AbstractControl): { [key: string]: any } | null {
    return this.applicationContainerValid(control);
  }

  applicationContainerValid(control: AbstractControl) {
    const container = control.value;
    const requiredParams = [
      'clusterName',
      'namespace',
      'applicationName',
      'componentName',
      'componentType',
      'containerName',
    ];
    const valid =
      container &&
      requiredParams.every((item: string) => {
        return !!container[item];
      });

    return valid
      ? null
      : {
          container_params_invalid: true,
        };
  }
}
