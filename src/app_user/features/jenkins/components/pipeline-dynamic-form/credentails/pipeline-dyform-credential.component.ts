import { DialogService, DialogSize } from '@alauda/ui';
import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  Component,
  EventEmitter,
  HostBinding,
  Input,
  OnInit,
  Optional,
  Output,
  Self,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';

import { Subject } from 'rxjs';

import { FormFieldControl } from 'app2/shared/form-field-control';
import { JenkinsMessageService } from '../../../services/jenkins-message.service';
import { PipelineFormService } from '../../../services/pipeline-form.service';
import { CreateJenkinsCredentialComponent } from '../../create-jenkins-credential';

let nextUniqueId = 0;

@Component({
  selector: 'rc-pipeline-dyform-credential',
  templateUrl: './pipeline-dyform-credential.component.html',
  styleUrls: ['./pipeline-dyform-credential.component.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: PipelineDyformCredentialComponent,
    },
  ],
})
export class PipelineDyformCredentialComponent
  implements FormFieldControl, ControlValueAccessor, OnInit {
  _uniqueId = `rc-pipeline-dyform-credential-${++nextUniqueId}`;
  private _credentialName: string;
  _required: any;
  _disabled: any;
  stateChanges = new Subject<void>();
  @Input()
  id = this._uniqueId;

  @Input()
  name: string | null = null;
  @Output()
  change = new EventEmitter<string>();
  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private formService: PipelineFormService,
    private dialogService: DialogService,
    private message: JenkinsMessageService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
  }

  get credentials() {
    return this.formService.credentials;
  }

  ngOnInit() {}

  @Input()
  get value(): string {
    return this.ngControl ? this.ngControl.value : this.credentialName;
  }

  set value(value: string) {
    if (value === this.value) {
      return;
    }
    this.stateChanges.next();
    this.onChange(value);
    this.change.emit(value);
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }

    this._disabled = disabled;
    this.stateChanges.next();
  }

  set credentialName(value: string) {
    this._credentialName = value;
    this.value = value;
  }

  get credentialName() {
    return this._credentialName;
  }

  writeValue(value: any): void {
    this._credentialName = value ? value : 'NOT_USE';
  }

  onChange(_value: any) {}

  registerOnChange(fn: (_: any) => void) {
    this.onChange = fn;
  }

  registerOnTouched() {}

  addCredential() {
    const jenkins_integration_id = this.formService.form.value
      .jenkins_integration_id;

    if (!jenkins_integration_id) {
      return;
    }

    const dialogRef = this.dialogService.open(
      CreateJenkinsCredentialComponent,
      {
        size: DialogSize.Big,
        data: { jenkins_integration_id },
      },
    );
    dialogRef.afterClosed().subscribe((created: any) => {
      if (created) {
        this.formService
          .getCredentials(jenkins_integration_id, '', true)
          .catch(error => this.message.error(error));
        this.credentialName = created;
      }
    });
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }
}
