import { DialogRef, TooltipDirective } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
  Optional,
  ViewChild,
} from '@angular/core';

import { clone, find, get, last, sortBy } from 'lodash-es';
import { BehaviorSubject, combineLatest, from, Observable } from 'rxjs';
import { debounceTime, map, share, startWith, switchMap } from 'rxjs/operators';

import {
  ImageProject as _ImageProject,
  ImageProjectService,
} from 'app/services/api/image-project.service';
import { ImageRegistryService } from 'app/services/api/image-registry.service';
import { ImageRepositoryService } from 'app/services/api/image-repository.service';
import { ACCOUNT, ENVIRONMENTS, WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, Environments, Weblabs } from 'app/typings';
import { RcImageSelection } from 'app/typings/k8s-form-model';
import { GALLERY_IMAGES, OUTSOURCING_IMAGES } from './gallery-images.constant';
type ImageProject = _ImageProject & { is_default?: boolean };
@Component({
  selector: 'rc-image-select',
  templateUrl: './image-select.component.html',
  styleUrls: ['./image-select.component.scss'],
})
export class ImageSelectComponent implements OnInit, OnDestroy, AfterViewInit {
  private registries: any[] = [];

  initialized: boolean;
  categories: any[];
  visibleRegistries: any[] = [];
  moreRegistries: any[] = [];
  visibleProjects: any = [];
  moreProjects: any[] = [];
  repositories: any[] = [];

  filterFnSubject$: BehaviorSubject<any>;
  repositoriesWrap$: BehaviorSubject<any>;
  repositories$: Observable<any>;
  filteredRepositories$: Observable<any>;

  publicOptions: any = [];
  publicRegistry: any;
  gallery: any;
  libraryRepositories: any[];

  currentCategory: any;
  currentPublicOption: any;
  currentRegistry: any;
  currentProject: any;
  currentRepository: any;
  thirdPartyRepositoryPath: string;
  loading = false;

  additionalRegistry: any;
  additionalProject: any;

  filterKey: string;
  exampleRepositoryAddress: string;

  private defaultProject: ImageProject;

  @ViewChild('registryTooltip', { read: TooltipDirective })
  registryListTooltipInstance: TooltipDirective;

  @ViewChild('projectTooltip', { read: TooltipDirective })
  projectListTooltipInstance: TooltipDirective;

  constructor(
    @Inject(ACCOUNT) private account: Account,
    @Inject(ENVIRONMENTS) private environments: Environments,
    @Inject(WEBLABS) private weblabs: Weblabs,
    @Optional() protected cdr: ChangeDetectorRef,
    private translateService: TranslateService,
    private registryService: ImageRegistryService,
    private repositoryService: ImageRepositoryService,
    private imageProjectService: ImageProjectService,
    private dialogRef: DialogRef,
  ) {}

  async ngOnInit() {
    this.initGalleryData();
    this.defaultProject = {
      project_name: this.translateService.get('default_project_name'),
      is_default: true,
    } as ImageProject;
    this.categories = [
      {
        name: this.translateService.get('image_select_private'),
        value: 'private',
        options: [],
      },
      {
        name: this.translateService.get('image_select_public'),
        value: 'public',
        options: [],
      },
    ];
    this.publicOptions = [
      {
        display_name: this.translateService.get('gallery'),
        type: 'gallery',
      },
      {
        display_name: this.translateService.get('library'),
        type: 'library',
      },
      {
        display_name: this.translateService.get('third_party'),
        type: 'third_party',
      },
    ];
    if (this.weblabs.HUATAI_ENABLED) {
      this.publicOptions = [
        {
          display_name: this.translateService.get('outsourcing'),
          type: 'outsourcing',
        },
      ];
    }
    this.exampleRepositoryAddress = `${
      this.environments.alauda_image_index
    }/alauda/hello-world`;
    this.currentCategory = this.categories[0];

    await this.getPrivateRegistry();

    this.filterFnSubject$ = new BehaviorSubject((list: any) => {
      return list;
    });

    this.repositoriesWrap$ = new BehaviorSubject([]);
    this.repositories$ = this.repositoriesWrap$.pipe(
      switchMap((repos: any[]) => {
        return repos;
      }),
      startWith([]),
    );
    this.filteredRepositories$ = combineLatest(
      this.repositories$,
      this.filterFnSubject$.pipe(debounceTime(200)),
    ).pipe(
      map(([repositories, filterFn]) => {
        return repositories.filter(filterFn);
      }),
      share(),
    );

    this.initialized = true;
  }

  ngOnDestroy() {}

  ngAfterViewInit() {}

  confirm() {
    let imageSelection: RcImageSelection;
    switch (this.currentCategory.value) {
      case 'private':
        imageSelection = this.handlePrivateRepositorySelection();
        break;
      case 'public':
        imageSelection = this.handlePublicRepositorySelection();
        break;
    }
    this.dialogRef.close(imageSelection);
  }

  cancel() {
    this.dialogRef.close();
  }

  private handlePrivateRepositorySelection() {
    const imageSelection: RcImageSelection = {
      registry_name: this.currentRegistry.name,
      registry_endpoint: this.currentRegistry.endpoint,
      is_public_registry: false,
      repository_name: this.currentRepository.name,
      is_third: get(this.currentRepository, 'registry.is_third') || null,
    };
    if (this.currentProject.is_default) {
      imageSelection.full_image_name = `${imageSelection.registry_endpoint}/${
        imageSelection.repository_name
      }`;
    } else {
      imageSelection.project_name = this.currentProject.project_name;
      imageSelection.full_image_name = `${imageSelection.registry_endpoint}/${
        imageSelection.project_name
      }/${imageSelection.repository_name}`;
    }
    return imageSelection;
  }

  private handlePublicRepositorySelection() {
    const imageSelection: RcImageSelection = {};
    switch (this.currentPublicOption.type) {
      case 'public_registry':
        imageSelection.registry_name = this.publicRegistry.name;
        imageSelection.registry_endpoint = this.publicRegistry.endpoint;
        imageSelection.is_public_registry = true;
        imageSelection.repository_name = this.currentRepository.name;
        imageSelection.full_image_name = `${this.publicRegistry.endpoint}/${
          this.account.namespace
        }/${this.currentRepository.name}`;
        break;
      case 'outsourcing':
        imageSelection.repository_name = this.currentRepository.name;
        imageSelection.full_image_name = `${
          this.environments.outsourcing_image_index
        }/${this.currentRepository.repo_path}`;
        break;
      case 'gallery':
      case 'library':
        imageSelection.full_image_name = `${
          this.environments.alauda_image_index
        }/${this.currentRepository.repo_path}`;
        imageSelection.repository_name = this.currentRepository.name;
        break;
      case 'third_party':
        imageSelection.full_image_name = this.thirdPartyRepositoryPath;
        imageSelection.repository_name = last(
          this.thirdPartyRepositoryPath.split('/'),
        );
        break;
    }
    return imageSelection;
  }

  categoryChange(category: any) {
    this.currentCategory = category;
    this.currentPublicOption = null;
    this.currentRegistry = null;
    switch (category.value) {
      case 'public':
        this.currentPublicOption = this.publicOptions[0];
        this.publicOptionChange(this.currentPublicOption);
        break;
      case 'private':
        if (this.registries.length) {
          this.currentRegistry = this.registries[0];
          this.registryChange(this.currentRegistry);
        } else {
          this.visibleRegistries = [];
          this.visibleProjects = [];
          this.repositoriesWrap$.next(from(this.getProjectRepositories(null)));
        }
        break;
    }
  }

  filterKeyChange() {
    this.filterFnSubject$.next((repo: any) => {
      return repo.name.includes(this.filterKey);
    });
  }

  repositorySelected(repo: any) {
    this.currentRepository = repo;
  }

  async projectChange(project: any, additional = false) {
    if (additional) {
      if (
        this.currentProject &&
        project.project_name === this.currentProject.project_name
      ) {
        return;
      }
      this.additionalProject = project;
      this.projectListTooltipInstance.disposeTooltip();
    } else {
      this.additionalProject = null;
    }
    this.currentRepository = null;

    this.currentProject = project;
    this.repositoriesWrap$.next(from(this.getProjectRepositories(project)));
  }

  async registryChange(registry: any, additional = false) {
    if (additional) {
      if (this.currentRegistry && registry.name === this.currentRegistry.name) {
        return;
      }
      this.additionalRegistry = registry;
      this.registryListTooltipInstance.disposeTooltip();
    } else {
      this.additionalRegistry = null;
    }
    this.currentRegistry = registry;
    await this.getRegistryProjects(registry);
  }

  publicOptionChange(option: any) {
    this.currentPublicOption = option;
    switch (option.type) {
      case 'public_registry':
        this.currentRepository = null;
        this.visibleProjects = [];
        // this.getPublicRepositories();
        this.repositoriesWrap$.next(from(this.getPublicRepositories()));
        break;
      case 'outsourcing':
        this.currentRepository = null;
        this.visibleProjects = [];
        this.repositoriesWrap$.next(from(this.getOutsourcingImages()));
        break;
      case 'gallery':
        this.visibleProjects = this.gallery.projects;
        this.projectChange(this.gallery.projects[0]);
        break;
      case 'library':
        this.currentRepository = null;
        this.visibleProjects = [];
        this.repositoriesWrap$.next(from(this.getLibraryRepositories()));
        break;
      case 'third_party':
        this.currentRepository = null;
        this.visibleProjects = [];
        break;
    }
  }

  applyExampleRepository() {
    this.thirdPartyRepositoryPath = this.exampleRepositoryAddress;
  }

  shouldEnableConfirmButton() {
    return this.currentRepository || this.thirdPartyRepositoryPath;
  }

  private async getOutsourcingImages() {
    return OUTSOURCING_IMAGES.map((image: string) => {
      return {
        name: image.split('/')[1],
        repo_path: image,
      };
    });
  }

  private async getPublicRepositories() {
    if (!this.publicRegistry.repositories) {
      this.loading = true;
      const repositories = await this.repositoryService.getRepositories({
        registryName: this.publicRegistry.name,
      });
      this.publicRegistry.repositories = repositories;
      this.loading = false;
    }
    return this.publicRegistry.repositories;
  }

  private async getProjectRepositories(project: any) {
    if (!project) {
      return [];
    }
    if (!project.repositories) {
      this.loading = true;
      const project_name = project.is_default
        ? undefined
        : project.project_name;
      const repositories = await this.repositoryService.getRepositories({
        registryName: this.currentRegistry.name,
        projectName: project_name,
        params: {
          is_third: !!this.currentRegistry.is_third,
        },
      });
      project.repositories = repositories;
      this.loading = false;
    }
    return project.repositories;
  }

  private async getRegistryProjects(registry: any) {
    if (!registry.projects) {
      let projects = await this.imageProjectService.getProjects(
        registry.name,
        registry.is_third,
      );
      projects = registry.is_third
        ? projects
        : [clone(this.defaultProject)].concat(projects);
      projects = sortBy(projects, (item: ImageProject) => {
        return item.project_name;
      });
      registry.projects = projects;
      if (projects.length > 5) {
        registry.visibleProjects = projects.slice(0, 4);
        registry.moreProjects = projects.slice(4);
      } else {
        registry.visibleProjects = projects;
        registry.moreProjects = [];
      }
    }
    this.visibleProjects = registry.visibleProjects;
    this.moreProjects = registry.moreProjects;
    this.projectChange(registry.projects[0]);
  }

  private initGalleryData() {
    this.gallery = {};
    this.gallery.projects = Object.keys(GALLERY_IMAGES).map((key: string) => {
      return {
        project_name: this.translateService.get(key),
        repositories: GALLERY_IMAGES[key].map((value: string) => {
          return {
            name: value.split('/')[1],
            repo_path: value,
          };
        }),
      };
    });
  }

  private async getLibraryRepositories() {
    if (!this.libraryRepositories) {
      this.loading = true;
      const repositories = await this.repositoryService.getLegacyPublicRepositories(
        'library',
      );
      this.libraryRepositories = repositories.map((item: any) => {
        return {
          name: item.repo_name,
          repo_path: item.repo_path,
        };
      });
      this.loading = false;
    }
    return this.libraryRepositories;
  }

  private async getPrivateRegistry() {
    let registries = await this.registryService.find();
    this.publicRegistry = find(registries, item => item.is_public);
    registries = registries.filter(item => !item.is_public);
    registries = sortBy(registries, item => item.name);
    this.registries = registries;
    if (this.publicRegistry) {
      this.publicOptions.unshift({
        display_name: this.publicRegistry.display_name,
        type: 'public_registry',
      });
    }
    if (this.registries.length) {
      this.registryChange(this.registries[0]);
    }
    if (this.registries.length > 5) {
      this.visibleRegistries = this.registries.slice(0, 4);
      this.moreRegistries = this.registries.slice(4);
    } else {
      this.visibleRegistries = this.registries;
    }
  }
}
