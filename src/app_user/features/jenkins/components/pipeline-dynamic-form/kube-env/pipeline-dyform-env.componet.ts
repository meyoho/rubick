import { coerceBooleanProperty } from '@angular/cdk/coercion';
import {
  AfterViewInit,
  Component,
  HostBinding,
  Input,
  OnDestroy,
  Optional,
  Self,
  ViewChild,
} from '@angular/core';
import {
  ControlValueAccessor,
  FormGroupDirective,
  NgControl,
  NgForm,
} from '@angular/forms';

import { cloneDeep, isArray, remove } from 'lodash-es';
import { Subject, Subscription } from 'rxjs';
import { debounceTime } from 'rxjs/operators';

import { TranslateService } from 'app/translate/translate.service';
import { K8S_ENV_VARIABLE_NAME } from 'app/utils/patterns';
import { FormFieldControl } from 'app2/shared/form-field-control';

let nextUniqueId = 0;

@Component({
  selector: 'rc-pipeline-dyform-env',
  templateUrl: './pipeline-dyform-env.componet.html',
  styleUrls: ['./pipeline-dyform-env.componet.scss'],
  providers: [
    {
      provide: FormFieldControl,
      useExisting: PipelineDyformEnvComponent,
    },
  ],
})
export class PipelineDyformEnvComponent
  implements FormFieldControl, ControlValueAccessor, OnDestroy, AfterViewInit {
  private _value: any;
  private _uniqueId = `rc-container-fields-envvars-${++nextUniqueId}`;
  private _required = false;
  protected _disabled = false;

  @ViewChild('form')
  form: NgForm;

  @Input()
  id = this._uniqueId;

  stateChanges = new Subject<void>();

  @Input()
  get value(): Array<any> {
    return this.ngControl ? this.ngControl.value : this._value;
  }

  set value(value: Array<any>) {
    this._value = value;
    this.updateModelValue();
    this.applyToNative();
  }

  @Input()
  get required() {
    return this._required;
  }

  set required(value: any) {
    this._required = coerceBooleanProperty(value);
  }

  @Input()
  get disabled() {
    return this.ngControl ? this.ngControl.disabled : this._disabled;
  }

  set disabled(disabled: any) {
    if (disabled === this.disabled) {
      return;
    }
    this._disabled = disabled;
    this.stateChanges.next();
  }

  @HostBinding('class.error-state')
  get errorState(): boolean {
    const isInvalid = this.ngControl && this.ngControl.invalid;
    const isDirty = this.ngControl && this.ngControl.dirty;
    const isSubmitted =
      (this.parentFormGroup && this.parentFormGroup.submitted) ||
      (this.parentForm && this.parentForm.submitted);

    return !!(isInvalid && (isDirty || isSubmitted));
  }

  controlType = 'rc-container-fields-envvars';

  /***** FormFieldControl end *****/
  private index = 0;
  envVars: Array<any>;

  @Input()
  namespace: string;

  @Input()
  clusterName: string;

  envVarReg = K8S_ENV_VARIABLE_NAME;
  errorMappers = {
    name: {
      pattern: this.translate.get(K8S_ENV_VARIABLE_NAME.tip),
    },
  };

  private formSubscription: Subscription;

  addEnvVar() {
    if (this.disabled) {
      return;
    }
    if (!this.envVars) {
      this.envVars = [];
      return;
    }
    this.envVars.push({
      index: this.index++,
      name: '',
      value: '',
    });
  }

  deleteEnvVar(index: number) {
    if (this.disabled) {
      return;
    }
    remove(this.envVars, (item: any) => {
      return index === item.index;
    });
  }

  trackByIndex(_index: number, item: any) {
    return item.index;
  }

  constructor(
    @Optional()
    @Self()
    public ngControl: NgControl,
    @Optional() private parentForm: NgForm,
    @Optional() private parentFormGroup: FormGroupDirective,
    private translate: TranslateService,
  ) {
    if (this.ngControl) {
      this.ngControl.valueAccessor = this;
    }
    this.addEnvVar();
  }

  ngAfterViewInit() {
    this.formSubscription = this.form.form.valueChanges
      .pipe(debounceTime(300))
      .subscribe(() => {
        const value = cloneDeep(this.envVars).filter(item => {
          return !!item.name && !!item.value;
        });
        this._value = this.formatOutputValue(value);
        this.updateModelValue();
      });
  }

  ngOnDestroy() {
    this.formSubscription.unsubscribe();
  }

  formatOutputValue(list: any) {
    const result: any[] = [];
    list.forEach((item: any) => {
      result.push({
        name: item.name,
        value: item.value,
      });
    });
    return result;
  }
  formatInputValue(list: any) {
    const result: any[] = [];
    list.forEach((item: any) => {
      const cur = {
        index: this.index++,
        name: item.name,
        value: '',
      };
      if (item.value) {
        cur.value = item.value;
      }
      result.push(cur);
    });
    return result;
  }

  // set ngModel value when native view changes
  private updateModelValue() {
    this.emitToModel();
  }

  private emitToModel() {
    this.onChangeCallback(this._value);
    this.onTouchedCallback();
  }

  private applyToNative() {
    this.envVars = this.formatInputValue(this._value);
  }

  /** Following Methods Implements ControlValueAccessor **/

  writeValue(value: Array<any>) {
    if (!value || !isArray(value)) {
      return;
    }
    this._value = value;
    this.applyToNative();
  }

  registerOnChange(fn: any): void {
    this.onChangeCallback = fn;
  }

  registerOnTouched(fn: any): void {
    this.onTouchedCallback = fn;
  }

  setDisabledState(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  private onChangeCallback: (_: any) => void = () => {
    // placeholder
  };

  private onTouchedCallback: () => void = () => {
    // placeholder
  };
}
