import {
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  Output,
  SimpleChanges,
} from '@angular/core';
import { DynamicFormFieldDefinition } from 'app2/shared/form-field-control';
import { distinctUntilChanged, map } from 'rxjs/operators';

import { DynamicFormComponent } from 'app2/shared/components/dynamic-form/dynamic-form.component';
import {
  PipelineTemplateService,
  RELATION_TYPE,
  RelationPolymer,
} from 'app_user/features/jenkins/services/pipeline-template.service';
import { Subject, Subscription } from 'rxjs';

@Component({
  selector: 'rc-pipeline-template-params-form',
  templateUrl: './pipeline-template-params-form.component.html',
  styleUrls: ['./pipeline-template-params-form.component.scss'],
})
export class PipelineTemplateParamsFormComponent
  implements OnDestroy, OnChanges, OnInit {
  language: string;
  @Input()
  fieldModels: DynamicFormFieldDefinition[];
  @Input()
  relationship: any[];

  @Output()
  valueChanged = new EventEmitter<any>();
  @Output()
  statusChanged = new EventEmitter<any>();

  valueChangesObservable: Subject<any> = new Subject();
  errorState: boolean;
  subList: Subscription[] = [];
  currentValue: any;
  initAllOptions = false;
  constructor(private templateService: PipelineTemplateService) {}

  valueChanges(value: any) {
    this.valueChangesObservable.next(value);
    this.paramsChanged(value);
  }

  paramsChanged(value: any) {
    this.valueChanged.emit(value);
  }

  ngOnInit() {
    this.language = this.templateService.language;
  }

  ngOnChanges({ relationship, fieldModels }: SimpleChanges) {
    if (relationship && relationship.currentValue) {
      this.registerRelationship();
    }
    if (
      fieldModels &&
      fieldModels.currentValue.length &&
      !this.initAllOptions
    ) {
      this.initOptionsGetter();
    }
  }

  registerRelationship() {
    this.unsubcribleAll();
    if (this.relationship) {
      Object.keys(this.relationship).forEach((key: any) => {
        this.relationship[key].forEach((relation: RelationPolymer) => {
          this.subList.push(this.fieldSubscribe(key, relation));
        });
      });
    }
  }

  private fieldSubscribe(key: string, relation: RelationPolymer) {
    return this.valueChangesObservable
      .pipe(
        map((value: any) => {
          this.currentValue = value;
          return value[key];
        }),
        distinctUntilChanged(
          (pre: any, crt: any) => JSON.stringify(pre) === JSON.stringify(crt),
        ),
      )
      .subscribe(async (currentValue: any) => {
        const newModels = await relation.cb(
          relation.type === RELATION_TYPE.LOGIC_OPERATION
            ? this.currentValue
            : currentValue,
          this.fieldModels,
        );
        if (relation.type !== RELATION_TYPE.SIMPLE) {
          this.fieldModels = newModels;
        }
      });
  }

  private initOptionsGetter() {
    this.fieldModels.forEach(async (model: any) => {
      if (model.options_getter) {
        model.options = await model.options_getter();
      }
    });

    this.initAllOptions = true;
  }

  ngOnDestroy() {
    this.unsubcribleAll();
  }

  unsubcribleAll() {
    this.subList.forEach((sub: Subscription) => {
      sub.unsubscribe();
    });
  }

  onStatusChanges(dynamicForm: DynamicFormComponent) {
    this.statusChanged.next(dynamicForm.errorState);
  }
}
