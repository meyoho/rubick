import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { JenkinsService } from 'app/services/api/jenkins.service';

import { CancelableTask } from '../../utils/cancelable-task';
import { historyStatus } from '../../utils/history-helper';
@Component({
  selector: 'rc-jenkins-recent-histories',
  styleUrls: ['recent-histories.component.scss'],
  templateUrl: 'recent-histories.component.html',
})
export class RecentHistoriesComponent implements OnInit, OnDestroy {
  @Input()
  pipelineId: string;
  @Input()
  jenkinsIntegrationId: string;
  histories: any[];

  private fetchRecentHistories = new CancelableTask(() => {
    return this.jenkins.histories
      .find({
        pipeline_uuid: this.pipelineId,
        jenkins_integration_id: this.jenkinsIntegrationId,
        page: 1,
        page_size: 5,
      })
      .then(response => response.results);
  });

  constructor(private jenkins: JenkinsService) {}

  ngOnInit() {
    this.initHistories().catch(() => {});
  }

  async initHistories() {
    if (!this.pipelineId || !this.jenkinsIntegrationId) {
      return;
    }
    this.histories = await this.fetchRecentHistories.run();
  }

  historyStatus(item: any) {
    return historyStatus(item);
  }

  ngOnDestroy() {
    this.fetchRecentHistories.cancel();
  }
}
