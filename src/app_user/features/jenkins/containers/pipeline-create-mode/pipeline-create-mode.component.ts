import { DIALOG_DATA, DialogService, DialogSize } from '@alauda/ui';
import { Component, Inject } from '@angular/core';
import { Router } from '@angular/router';
import { PipelineTemplateListComponent } from 'app2/features/jenkins/containers/pipeline-template-list/pipeline-template-list.component';
@Component({
  templateUrl: './pipeline-create-mode.component.html',
  styleUrls: ['./pipeline-create-mode.component.scss'],
})
export class PipelineCreateModeComponent {
  constructor(
    private router: Router,
    private dialogService: DialogService,
    @Inject(DIALOG_DATA)
    private data: {
      project: string;
      cluster: string;
      namespace: string;
    },
  ) {}

  selected() {
    this.dialogService.closeAll();
    this.router.navigate([
      'console',
      'user',
      'workspace',
      this.data,
      'jenkins',
      'pipelines',
      'create',
    ]);
  }

  templateList() {
    this.dialogService.closeAll();
    this.dialogService.open(PipelineTemplateListComponent, {
      size: DialogSize.Large,
      data: this.data || null,
    });
  }
}
