import { DialogService, DialogSize, TooltipDirective } from '@alauda/ui';
import {
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { Store } from '@ngrx/store';
import { TranslateService } from 'app/translate/translate.service';
import { createActions } from 'app/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import * as actions from '../../actions/pipeline';
import { CreateJenkinsCredentialComponent } from '../../components/create-jenkins-credential';
import { JenkinsfileContentExampleComponent } from '../../components/jenkinsfile-content-example';
import { EXAMPLE } from '../../components/jenkinsfile-content-example/jenkinsfile-content-example';
import { PipelineFormTriggerComponent } from '../../components/pipeline-form-trigger';
import { State } from '../../reducers';
import { JenkinsMessageService } from '../../services/jenkins-message.service';
import { PipelineFormService } from '../../services/pipeline-form.service';

const HEADER_HEIGHT = 76;
const MENU_PADDING_TOP = 20;

@Component({
  styleUrls: ['pipeline-create.component.scss'],
  templateUrl: 'pipeline-create.component.html',
  providers: [PipelineFormService],
})
export class PipelineCreateComponent implements OnInit, OnDestroy {
  targetBase: any[] = [];
  projectName: string;
  clusterName: string;
  namespaceName: string;

  @ViewChild('triggerSelect')
  triggerSelect: TooltipDirective;
  @ViewChild('tips')
  tipsElement: ElementRef;

  @ViewChildren('triggerComponent')
  triggers: QueryList<PipelineFormTriggerComponent>;

  onIntegrationChange$ = new Subject<string>();
  onCodeClientChange$ = new Subject<string>();
  onCodeOrgChange$ = new Subject<string>();
  private subs: Subscription[] = [];

  codeEditorOptions = {
    language: 'Jenkinsfile',
    readOnly: false,
  };
  actionsConfigCreate = createActions;

  get scriptTypes() {
    return this.formService.scriptTypes;
  }

  get spaces() {
    return this.formService.spaces;
  }

  get integrations() {
    return this.formService.integrations;
  }

  get codeClients() {
    return this.formService.codeClients;
  }

  get codeOrgs() {
    return this.formService.codeOrgs;
  }

  get credentials() {
    return this.formService.credentials;
  }

  get createCredentialEnabled() {
    return this.formService.createCredentialEnabled;
  }

  get form() {
    return this.formService.form;
  }

  get initialValue() {
    return this.formService.initialValue;
  }

  get initialized() {
    return this.formService.initialized;
  }

  get saving() {
    return this.formService.saving;
  }

  get isSelectedCodeClientNotBind() {
    return this.formService.isSelectedCodeClientNotBind;
  }

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private dialogService: DialogService,
    private translate: TranslateService,
    private formService: PipelineFormService,
    private store: Store<State>,
    private message: JenkinsMessageService,
    private workspaceComponent: WorkspaceComponent,
  ) {
    this.subs.push(
      this.onIntegrationChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onIntegrationChange(value)),
      this.onCodeClientChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeClientChange(value)),
      this.onCodeOrgChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeOrgChange(value)),
    );
  }

  ngOnInit() {
    if (this.workspaceComponent) {
      const baseParams = this.workspaceComponent.baseParamsSnapshot;
      this.projectName = baseParams['project'];
      this.clusterName = baseParams['cluster'];
      this.namespaceName = baseParams['namespace'];
    }
    this.targetBase = [
      '/console/user',
      'workspace',
      {
        project: this.projectName,
        cluster: this.clusterName,
        namespace: this.namespaceName,
      },
      'jenkins',
    ];
    this.route.queryParamMap.subscribe(queryParams => {
      const clone_uuid = queryParams.get('clone');
      const payload = {
        id: clone_uuid,
        isClone: !!clone_uuid,
        projectName: this.projectName,
        clusterName: this.clusterName,
        namespaceName: this.namespaceName,
      };
      this.formService.onInit(payload).catch(error => {
        this.formService.errorMessage(error);
        this.router.navigate(this.targetBase.concat(['pipelines']));
      });
    });
  }

  ngOnDestroy() {
    this.formService.release();
    this.subs.forEach(sub => sub.unsubscribe());
  }

  getFormControls(key: string) {
    return (this.form.get(key) as FormGroup).controls;
  }

  triggerIdentify(_index: number, trigger: FormGroup) {
    return trigger.value['@@id'];
  }

  addTrigger(type: 'cron' | 'image_change' | 'code_change_hook') {
    if (this.cronTriggerDisabled() && type === 'cron') {
      return;
    }

    if (this.codeChangeHookTriggerDisabled() && type === 'code_change_hook') {
      return;
    }

    this.formService.addTrigger(type);
    this.triggerSelect.disposeTooltip();
  }

  removeTrigger(trigger: FormGroup) {
    this.formService.removeTrigger(trigger);
  }

  addCredential() {
    const jenkins_integration_id = this.form.value.jenkins_integration_id;

    if (!jenkins_integration_id) {
      return;
    }

    const dialogRef = this.dialogService.open(
      CreateJenkinsCredentialComponent,
      {
        size: DialogSize.Big,
        data: { jenkins_integration_id },
      },
    );

    dialogRef.afterClosed().subscribe((created: any) => {
      if (created) {
        this.formService
          .getCredentials(jenkins_integration_id, created)
          .catch(error => this.message.error(error));
      }
    });
  }

  codeChangeHookTriggerDisabled() {
    return (
      this.form.value.source === 'NOT_USE' ||
      this.form.value.triggers.filter(
        (trigger: any) => trigger.type === 'code_change_hook',
      ).length > 0
    );
  }

  cronTriggerDisabled() {
    return (
      this.form.value.triggers.filter((trigger: any) => trigger.type === 'cron')
        .length > 0
    );
  }

  getMenuTop(scrollTop: number) {
    let tipsHeight =
      (this.tipsElement &&
        this.tipsElement.nativeElement &&
        this.tipsElement.nativeElement.offsetHeight) ||
      HEADER_HEIGHT;
    tipsHeight = tipsHeight > 100 ? 86 : tipsHeight;
    return `${Math.max(
      tipsHeight + MENU_PADDING_TOP * 2 - scrollTop,
      MENU_PADDING_TOP,
    )}px`;
  }

  jenkinsfileExample() {
    this.dialogService.open(JenkinsfileContentExampleComponent, {
      size: DialogSize.Large,
      data: {
        content: EXAMPLE,
      },
    });
  }

  onSubmit() {
    this.triggers.forEach(trigger => trigger.setSubmited());

    if (this.saving || this.form.status !== 'VALID') {
      return;
    }
    const triggers = this.triggers.map(trigger => trigger.toJson());

    this.formService.save('', triggers).then(result => {
      if (result) {
        this.store.dispatch(new actions.Created(result));
        this.router.navigate(this.targetBase.concat(['pipelines', result]));
      }
    });
  }

  async cancelCreate() {
    try {
      await this.dialogService.confirm({
        title: this.translate.get(
          'jenkins_template_create_pipeline_cancel_title',
        ),
        content: this.translate.get(
          'jenkins_template_create_pipeline_cancel_content',
        ),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
      this.router.navigate(this.targetBase.concat(['pipelines']));
    } catch (e) {
      return;
    }
  }

  back() {
    this.router.navigate(['../'], {
      relativeTo: this.route,
    });
  }
}
