import { ConfirmType, DialogService, DialogSize } from '@alauda/ui';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { find, get } from 'lodash-es';
import { Subject, interval } from 'rxjs';
import {
  combineLatest,
  distinctUntilChanged,
  filter,
  map,
  merge,
  switchMap,
  takeUntil,
  takeWhile,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';
import { viewActions } from 'app/utils/code-editor-config';
import { PipelineUpdateByTemplateComponent } from 'app_user/features/jenkins/containers/pipeline-update-by-template';
import { PipelineTemplateService } from 'app_user/features/jenkins/services/pipeline-template.service';

import * as historyActions from '../../actions/history';
import * as actions from '../../actions/pipeline';
import {
  State,
  selectPipelineDetailData,
  selectPipelineDetailHistories,
  selectPipelineDetailHistoriesLoading,
  selectPipelineDetailHistoriesPage,
  selectPipelineDetailHistoriesTotal,
  selectPipelineDetailId,
  selectPipelineDetailLoading,
  selectPipelineDetailMutating,
  selectPipelineDetailMutatingHistories,
} from '../../reducers';
import { ComponentEffect } from '../../utils/component-effect';
import { PipelineEditComponent } from '../pipeline-edit';
const AUTO_REFRESH_DELAY = 10;
const PAGE_SIZE = 10;

@Component({
  templateUrl: './pipeline-detail.component.html',
  styleUrls: ['./pipeline-detail.component.scss'],
  providers: [PipelineTemplateService],
})
export class PipelineDetailComponent implements OnInit, OnDestroy {
  id$ = this.store.select(selectPipelineDetailId);
  data$ = this.store.select(selectPipelineDetailData);
  loading$ = this.store.select(selectPipelineDetailLoading);
  mutating$ = this.store.select(selectPipelineDetailMutating);
  histories$ = this.store.select(selectPipelineDetailHistories);
  historiesTotal$ = this.store.select(selectPipelineDetailHistoriesTotal);
  historiesLoading$ = this.store.select(selectPipelineDetailHistoriesLoading);
  historiesPage$ = this.store.select(selectPipelineDetailHistoriesPage);
  mutatingHistories$ = this.store.select(selectPipelineDetailMutatingHistories);
  pageSize = PAGE_SIZE;
  creatable = false;
  private onDestroy$ = new Subject<void>();
  private jenkinsIntegrationId: string;
  fieldModels: any;

  codeEditorOptions = {
    language: 'Jenkinsfile',
    readOnly: true,
  };
  actionsConfigView = viewActions;

  @ComponentEffect()
  idChange$ = this.route.paramMap.pipe(
    takeUntil(this.onDestroy$),
    map(paramMap => paramMap.get('id')),
    distinctUntilChanged(),
    tap(id => this.store.dispatch(new actions.Get(id))),
  );

  @ComponentEffect()
  jenkinsIntegrationId$ = this.data$.pipe(
    takeUntil(this.onDestroy$),
    filter(data => !!data),
    tap(data => (this.jenkinsIntegrationId = data.jenkins_integration_id)),
  );

  @ComponentEffect()
  updated$ = this.actions$.pipe(ofType(actions.types.Updated)).pipe(
    takeUntil(this.onDestroy$),
    map(action => action as actions.Updated),
    withLatestFrom(this.id$),
    filter(([action, id]) => action.id === id),
    tap(([, id]) => this.store.dispatch(new actions.Get(id))),
  );

  @ComponentEffect()
  getError$ = this.actions$.pipe(ofType(actions.types.GetError)).pipe(
    takeUntil(this.onDestroy$),
    withLatestFrom(this.id$),
    filter(([action, id]: [actions.GetError, string]) => action.id === id),
    tap(() =>
      this.router.navigate(['../'], {
        relativeTo: this.route,
      }),
    ),
  );

  @ComponentEffect()
  started$ = this.actions$.pipe(
    ofType(historyActions.types.Started),
    takeUntil(this.onDestroy$),
    withLatestFrom(this.id$),
    filter(
      ([action, id]: [historyActions.Started, string]) =>
        action.pipeline_uuid === id,
    ),
    map(([action]) => action),
    tap(action =>
      this.router.navigate(
        ['../../', 'histories', action.pipeline_uuid, action.id],
        {
          relativeTo: this.route,
        },
      ),
    ),
  );

  @ComponentEffect()
  deleted$ = this.actions$.pipe(ofType(actions.types.Deleted)).pipe(
    takeUntil(this.onDestroy$),
    withLatestFrom(this.id$),
    filter(([action, id]: [actions.Deleted, string]) => action.id === id),
    map(([action]) => action),
    tap(() =>
      this.router.navigate(['../'], {
        relativeTo: this.route,
      }),
    ),
  );

  @ComponentEffect()
  refresh$ = this.histories$.pipe(
    switchMap(() =>
      interval(1000).pipe(
        takeWhile(value => value <= AUTO_REFRESH_DELAY),
        map(value => AUTO_REFRESH_DELAY - value),
      ),
    ),
    takeUntil(this.onDestroy$),
    filter(value => value === 0),
    merge(
      this.actions$.pipe(
        ofType(
          historyActions.types.Started,
          historyActions.types.Canceled,
          historyActions.types.Deleted,
        ),
        map(
          (
            action:
              | historyActions.Started
              | historyActions.Canceled
              | historyActions.Deleted,
          ) => action.pipeline_uuid,
        ),
        combineLatest(this.id$),
        filter(([actionId, id]) => actionId === id),
      ),
    ),
    withLatestFrom(
      this.historiesPage$,
      this.data$.pipe(filter(data => !!data)),
    ),
    map(([, page, data]) => ({
      page,
      id: data.uuid,
      jenkins_integration_id: data.jenkins_integration_id,
    })),
    tap(({ id, ...params }) => {
      this.store.dispatch(
        new actions.GetHistories(id, { ...params, page_size: PAGE_SIZE }),
      );
    }),
  );

  constructor(
    private store: Store<State>,
    private route: ActivatedRoute,
    private roleUtil: RoleUtilitiesService,
    private actions$: Actions,
    private router: Router,
    private dialogService: DialogService,
    private translate: TranslateService,
    private templateService: PipelineTemplateService,
  ) {}

  async ngOnInit() {
    this.creatable = await this.roleUtil.resourceTypeSupportPermissions(
      'jenkins_pipeline',
      null,
      'create',
    );

    this.data$.pipe(filter(data => !!data)).subscribe((data: any) => {
      if (!data.template) {
        return;
      }
      this.templateService.template = data.template.template;
      const fields = this.templateService.fields;
      const args_value = get(data, 'template.arguments_values');

      this.fieldModels = Object.keys(args_value).map((key: string) => {
        const field: any = find(fields, { name: key });
        return {
          value: this.getParamValue(get(args_value, key)),
          label: field ? field.label : '',
        };
      });
    });
  }

  canUpdate(data: any) {
    return this.roleUtil.resourceHasPermission(
      data,
      'jenkins_pipeline',
      'update',
    );
  }

  canDelete(data: any) {
    return this.roleUtil.resourceHasPermission(
      data,
      'jenkins_pipeline',
      'delete',
    );
  }

  canStart(data: any) {
    return this.roleUtil.resourceHasPermission(
      data,
      'jenkins_pipeline',
      'trigger',
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.dialogService.closeAll();
  }

  onPageChange(page: number) {
    if (!this.jenkinsIntegrationId) {
      return;
    }
    this.store.dispatch(
      new actions.GetHistories(this.route.snapshot.paramMap.get('id'), {
        jenkins_integration_id: this.jenkinsIntegrationId,
        page,
        page_size: this.pageSize,
      }),
    );
  }

  start(data: any) {
    this.store.dispatch(new actions.Start(data.uuid));
  }

  edit(data: any, upgrade = false) {
    const title = upgrade
      ? `${this.translate.get('upgrade')} ${data.name}`
      : `${this.translate.get('update')} ${data.name}`;

    const upgradeTo = upgrade ? get(data, 'template.upgrade.uuid') : '';

    if (data.template) {
      this.dialogService.open(PipelineUpdateByTemplateComponent, {
        size: DialogSize.Large,
        data: {
          id: data.uuid,
          template: data,
          upgradeTo,
          title,
        },
      });
    } else {
      this.dialogService.open(PipelineEditComponent, {
        size: DialogSize.Large,
        data: {
          id: data.uuid,
          title,
        },
      });
    }
  }

  async delete(data: any) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('jenkins_pipeline_confirm_delete', {
          pipeline_name: data.display_name || data.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Danger,
      });

      this.store.dispatch(new actions.Delete(data.uuid));
    } catch (error) {}
  }

  clone(data: any) {
    const target = data.template
      ? ['../', 'templates', 'template_create_pipeline', data.template.uuid]
      : ['../', 'create'];
    this.router.navigate(target, {
      queryParams: {
        clone: data.uuid,
      },
      relativeTo: this.route,
    });
  }

  startHistory(item: any) {
    this.store.dispatch(
      new historyActions.Start(item.history_id, item.pipeline_uuid),
    );
  }

  cancelHistory(item: any) {
    this.store.dispatch(
      new historyActions.Cancel(item.history_id, item.pipeline_uuid),
    );
  }

  deleteHistory(item: any) {
    this.store.dispatch(
      new historyActions.Delete(item.history_id, item.pipeline_uuid),
    );
  }

  private getParamValue(target: any) {
    function isPropertyNotEmpty(obj: any) {
      for (const n in obj) {
        if (obj[n] !== '') {
          return true;
        }
      }
      return false;
    }

    if (
      typeof target === 'string' ||
      !target ||
      typeof target === 'number' ||
      typeof target === 'boolean'
    ) {
      return target;
    } else if (typeof target.length === 'number') {
      return { type: 1, res: target.filter((t: any) => isPropertyNotEmpty(t)) };
    } else {
      const res: any[] = [];
      Object.keys(target).forEach((k: string) => {
        if (k.includes('UUID')) {
          return;
        }
        res.push({
          name: k,
          value: target[k],
        });
      });
      return {
        type: 1,
        res: res,
      };
    }
  }

  getTemplateName(target: any) {
    if (!target) {
      return '';
    }
    const lang = this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
    return target[lang];
  }

  back() {
    this.router.navigate(['../'], {
      relativeTo: this.route,
    });
  }
}
