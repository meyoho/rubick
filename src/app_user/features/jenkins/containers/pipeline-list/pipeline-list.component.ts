import { ConfirmType, DialogService, DialogSize } from '@alauda/ui';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Actions, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';

import { extend, find, get, isEmpty, toInteger } from 'lodash-es';
import { interval, Observable, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  map,
  publishReplay,
  refCount,
  switchMap,
  takeUntil,
  takeWhile,
  tap,
  withLatestFrom,
} from 'rxjs/operators';

import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';
import { shallowEqual } from 'app/utils/shallow-equal';
import { PipelineCreateModeComponent } from 'app_user/features/jenkins/containers/pipeline-create-mode';
import { PipelineEditComponent } from 'app_user/features/jenkins/containers/pipeline-edit/pipeline-edit.component';
import { PipelineUpdateByTemplateComponent } from 'app_user/features/jenkins/containers/pipeline-update-by-template';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import * as historyActions from '../../actions/history';
import * as actions from '../../actions/pipeline';
import {
  selectPipelineListItems,
  selectPipelineListLoading,
  selectPipelineListMutatingItems,
  selectPipelineListParams,
  selectPipelineListTotal,
  State,
} from '../../reducers';
import { ComponentEffect } from '../../utils/component-effect';
import { historyStatus } from '../../utils/history-helper';

const PAGE_SIZE = 10;
const AUTO_REFRESH_DELAY = 60;

@Component({
  templateUrl: './pipeline-list.component.html',
  styleUrls: ['./pipeline-list.component.scss'],
})
export class PipelineListComponent implements OnInit, OnDestroy {
  projectName: string;
  clusterName: string;
  namespaceName: string;
  columns: string[] = [
    'name',
    'trigger',
    'history',
    'created_by',
    'created_method',
    'action',
  ];
  creatable = false;
  params$ = this.store.select(selectPipelineListParams);
  loading$ = this.store.select(selectPipelineListLoading);
  total$ = this.store.select(selectPipelineListTotal);
  mutatingItems$ = this.store.select(selectPipelineListMutatingItems);
  items$ = this.store.select(selectPipelineListItems).pipe(
    distinctUntilChanged(),
    withLatestFrom(this.mutatingItems$),
    map(([items, mutatingItems]) => this.addActions(items, mutatingItems)),
    map((items: any[]) => this.triggerPolymer(items)),
    publishReplay(1),
    refCount(),
  );
  autoRefreshIn$: Observable<string>;
  inputValue: string;
  toggledId: string;
  private onDestroy$ = new Subject<void>();

  @ComponentEffect()
  paramsChange$ = this.route.queryParamMap.pipe(
    takeUntil(this.onDestroy$),
    map(queryParamMap =>
      extend(
        {
          search: queryParamMap.get('search') || '',
          page: toInteger(queryParamMap.get('page')) || 1,
        },
        {
          template_uuids: queryParamMap.get('uuid')
            ? queryParamMap.get('uuid')
            : undefined,
        },
      ),
    ),
    distinctUntilChanged(shallowEqual),
    tap(params => {
      this.inputValue = params.search;
      this.store.dispatch(
        new actions.Search({
          ...params,
          page_size: PAGE_SIZE,
        }),
      );
    }),
  );

  @ComponentEffect()
  itemEffects$ = this.actions$
    .pipe(
      ofType(
        actions.types.Deleted,
        actions.types.Created,
        actions.types.Updated,
      ),
    )
    .pipe(
      takeUntil(this.onDestroy$),
      tap(() => this.refresh(true)),
    );

  @ComponentEffect()
  started$ = this.actions$.pipe(ofType(historyActions.types.Started)).pipe(
    takeUntil(this.onDestroy$),
    tap(() => this.refresh()),
    tap((action: historyActions.Started) => {
      this.router.navigate(
        ['../', 'histories', action.pipeline_uuid, action.id],
        {
          relativeTo: this.route,
        },
      );
    }),
  );

  constructor(
    private store: Store<State>,
    private route: ActivatedRoute,
    private router: Router,
    private translate: TranslateService,
    private dialogService: DialogService,
    private roleUtil: RoleUtilitiesService,
    private actions$: Actions,
    private workspaceComponent: WorkspaceComponent,
  ) {
    this.autoRefreshIn$ = this.autoRefreshIn();
  }

  async ngOnInit() {
    this.workspaceComponent.baseParams
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(params => {
        this.projectName = params.project;
        this.clusterName = params.cluster;
        this.namespaceName = params.namespace;
      });

    this.creatable = await this.roleUtil.resourceTypeSupportPermissions(
      'jenkins_pipeline',
      null,
      'create',
    );
    this.store.dispatch(new actions.Renew()); // TODO: clone action depends on create permission, force render for create permission take effect.
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.dialogService.closeAll();
  }

  autoRefreshIn() {
    return this.items$.pipe(
      switchMap(() =>
        interval(1000).pipe(
          takeWhile(value => value <= AUTO_REFRESH_DELAY),
          map(value => AUTO_REFRESH_DELAY - value),
        ),
      ),
      takeUntil(this.onDestroy$),
      tap(value => {
        if (value === 0) {
          this.refresh(true);
        }
      }),
      map(value => value.toString()),
      publishReplay(1),
      refCount(),
    );
  }

  refresh(ignoreError = false) {
    this.store.dispatch(
      new actions.Search(
        {
          search: this.inputValue,
          page: toInteger(this.route.snapshot.queryParamMap.get('page')) || 1,
          page_size: PAGE_SIZE,
        },
        ignoreError,
      ),
    );
  }

  pageChange(page: number) {
    this.router.navigate([], {
      queryParams: {
        search: this.inputValue,
        page,
      },
      relativeTo: this.route,
    });
  }

  search(keyword: string) {
    this.inputValue = keyword;
    this.router.navigate([], {
      queryParams: {
        search: (this.inputValue || '').trim(),
        page: 1,
      },
      relativeTo: this.route,
    });
  }

  create() {
    const data = {
      project: this.projectName,
      cluster: this.clusterName,
      namespace: this.namespaceName,
    };
    this.dialogService.open(PipelineCreateModeComponent, {
      size: DialogSize.Medium,
      data: data,
    });
  }

  edit(item: any, upgrade = false) {
    const title = upgrade
      ? `${this.translate.get('upgrade')} ${item.name}`
      : `${this.translate.get('update')} ${item.name}`;

    const upgradeTo = upgrade ? get(item, 'template.upgrade.uuid') : '';
    if (item.template) {
      this.dialogService.open(PipelineUpdateByTemplateComponent, {
        size: DialogSize.Large,
        data: {
          id: item.uuid,
          template: item,
          upgradeTo,
          title,
        },
      });
    } else {
      this.dialogService.open(PipelineEditComponent, {
        size: DialogSize.Large,
        data: {
          id: item.uuid,
          title,
        },
      });
    }
  }

  async delete(item: any) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('jenkins_pipeline_confirm_delete', {
          pipeline_name: item.display_name || item.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
        confirmType: ConfirmType.Danger,
      });

      this.store.dispatch(new actions.Delete(item.uuid));
    } catch (error) {}
  }

  start(id: string) {
    this.store.dispatch(new actions.Start(id));
  }

  historyStatus(history: any) {
    return historyStatus(history);
  }

  addActions(items: any[], mutatingItems: any) {
    return items.map(item => ({
      item,
      actions: this.actions(item, mutatingItems[item.uuid]),
    }));
  }

  actions(item: any, mutating: any) {
    const actions = [
      {
        text: 'jenkins_pipeline_start',
        action: (item: any) => this.start(item.uuid),
        disabled: !!mutating,
        hide: !this.hasPermission(item, 'trigger'),
      },
      {
        text: 'view',
        action: (item: any) =>
          this.router.navigate([item.uuid], {
            relativeTo: this.route,
          }),
        disabled: false,
        hide: !this.hasPermission(item, 'get'),
      },
      {
        text: 'jenkins_pipeline_clone',
        action: (item: any) => {
          const target = item.template
            ? ['templates', 'template_create_pipeline', item.template.uuid]
            : ['create'];
          this.router.navigate(target, {
            queryParams: { clone: item.uuid },
            relativeTo: this.route,
          });
        },
        disabled: !!mutating,
        hide: !this.creatable,
      },
      {
        text: 'update',
        action: (item: any) => this.edit(item),
        disabled: !!mutating,
        hide: !this.hasPermission(item, 'update'),
      },
      {
        text: 'jenkins_pipeline_upgrade',
        action: (item: any) => this.edit(item, true),
        disabled: !!mutating,
        hide:
          !this.hasPermission(item, 'update') || !get(item, 'template.upgrade'),
      },
      {
        text: 'delete',
        action: (item: any) => this.delete(item),
        disabled: !!mutating,
        hide: !this.hasPermission(item, 'delete'),
      },
    ];

    return actions.filter(action => !action.hide);
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'jenkins_pipeline',
      action,
    );
  }

  getTemplateName(target: any) {
    if (!target) {
      return '';
    }
    const lang = this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
    return target[lang];
  }

  triggerPolymer(items: any[]) {
    items.map(({ item }) => {
      const trg = item.triggers;
      if (!trg) {
        item['triggers_polymer'] = [];
        return;
      }
      const ply = [
        { type: 'cron', values: find(trg, { type: 'cron' }) },
        {
          type: 'image_change',
          values: trg.filter((t: any) => t.type === 'image_change'),
        },
        {
          type: 'code_change_hook',
          values: find(trg, { type: 'code_change_hook' }),
        },
      ];
      item['triggers_polymer'] = ply.filter(t => !isEmpty(t.values));
    });
    return items;
  }

  back() {
    this.router.navigate([], {
      relativeTo: this.route,
    });
  }
}
