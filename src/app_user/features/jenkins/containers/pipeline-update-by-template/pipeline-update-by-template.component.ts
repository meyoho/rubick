import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  DialogSize,
  TooltipDirective,
} from '@alauda/ui';
import {
  Component,
  EventEmitter,
  Inject,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { FormGroup } from '@angular/forms';

import { Store } from '@ngrx/store';
import { JenkinsService } from 'app/services/api/jenkins.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';
import { PipelineGlobalVarHelpComponent } from 'app2/features/jenkins/components/pipeline-global-var-help';
import { PipelineTemplateInfoComponent } from 'app2/features/jenkins/components/pipeline-template-info';
import {
  PipelineTemplateService,
  templateStagesConvert,
} from 'app_user/features/jenkins/services/pipeline-template.service';
import { get as __get } from 'lodash-es';
import { Subject, Subscription } from 'rxjs';
import { distinctUntilChanged } from 'rxjs/operators';

import * as actions from '../../actions/pipeline';
import { CreateJenkinsCredentialComponent } from '../../components/create-jenkins-credential';
import { PipelineFormTriggerComponent } from '../../components/pipeline-form-trigger';
import { State } from '../../reducers';
import { JenkinsMessageService } from '../../services/jenkins-message.service';
import { PipelineFormService } from '../../services/pipeline-form.service';

@Component({
  templateUrl: './pipeline-update-by-template.component.html',
  styleUrls: [
    './pipeline-update-by-template.component.scss',
    '../pipeline-edit/pipeline-edit.component.scss',
  ],
  providers: [PipelineFormService, PipelineTemplateService],
})
export class PipelineUpdateByTemplateComponent
  implements OnInit, OnDestroy, OnInit, OnDestroy {
  @ViewChild('triggerSelect')
  triggerSelect: TooltipDirective;

  @ViewChildren('triggerComponent')
  triggers: QueryList<PipelineFormTriggerComponent>;

  close = new EventEmitter<void>();
  onIntegrationChange$ = new Subject<string>();
  onCodeClientChange$ = new Subject<string>();
  onCodeOrgChange$ = new Subject<string>();
  subs: Subscription[] = [];
  fieldModels: any[] = [];
  relationship: any;
  paramsForm: any;
  pipeline_data: any;
  language: string;

  paramErrorStatus = true;

  fromVersion: string;
  currentVersion: string;
  tempalteName: string;
  template: any;

  get scriptTypes() {
    return this.formService.scriptTypes;
  }

  get spaces() {
    return this.formService.spaces;
  }

  get integrations() {
    return this.formService.integrations;
  }

  get codeClients() {
    return this.formService.codeClients;
  }

  get codeOrgs() {
    return this.formService.codeOrgs;
  }

  get credentials() {
    return this.formService.credentials;
  }

  get createCredentialEnabled() {
    return this.formService.createCredentialEnabled;
  }

  get form() {
    return this.formService.form;
  }

  get initialValue() {
    return this.formService.initialValue;
  }

  get initialized() {
    return this.formService.initialized;
  }

  get saving() {
    return this.formService.saving;
  }

  get isSelectedCodeClientNotBind() {
    return this.formService.isSelectedCodeClientNotBind;
  }

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      id: string;
      template: any;
      upgradeTo?: string;
      title: string;
    },
    public dialogRef: DialogRef,
    private dialogService: DialogService,
    private translate: TranslateService,
    private jenkinsService: JenkinsService,
    private formService: PipelineFormService,
    private message: JenkinsMessageService,
    private templateService: PipelineTemplateService,
    private store: Store<State>,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    this.subs.push(
      this.onIntegrationChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onIntegrationChange(value)),
      this.onCodeClientChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeClientChange(value)),
      this.onCodeOrgChange$
        .pipe(distinctUntilChanged())
        .subscribe(value => this.formService.onCodeOrgChange(value)),
    );
    // this.formService.setRelease(false);
    // this.formService.createForm();
    this.language = this.translate.currentLang === 'en' ? 'en' : 'zh-CN';
  }

  ngOnInit() {
    if (!this.data.id) {
      this.dialogRef.close();
      return;
    }

    const payload = {
      id: this.data.id, //todo: need to add other params, like 'isClone', 'projectName', 'clusterName', 'namespaceName'
    };
    this.formService.onInit(payload).catch(error => {
      this.formService.errorMessage(error);
      this.dialogRef.close();
    });

    this.jenkinsService.pipelines.get(this.data.id).then((res: any) => {
      this.pipeline_data = res;
    });

    const orignal = __get(this.data, 'template.template');
    const values = orignal.arguments_values;

    (this.data.upgradeTo
      ? this.jenkinsService.templates.get(this.data.upgradeTo)
      : Promise.resolve(orignal.template)
    ).then(template => {
      this.template = template;
      if (this.data.upgradeTo) {
        this.fromVersion = this.getTemplateVersion(orignal.template);
      }

      this.currentVersion = this.getTemplateVersion(template);

      this.tempalteName =
        __get(template, `display_name.${this.language}`) || '';

      this.templateService.template = template;

      this.fieldModels = this.templateService.fields;
      this.fieldModels.forEach((model: any) => {
        model.value = __get(values, model.name);
      });
      this.relationship = this.templateService.relationship;
    });
  }

  getFormControls(key: string) {
    return (this.form.get(key) as FormGroup).controls;
  }

  getFormValue(path: string) {
    return __get(this.formService.form.value, path) || '';
  }

  ngOnDestroy() {
    this.formService.release();
  }

  triggerIdentify(_index: number, trigger: FormGroup) {
    return trigger.value['@@id'];
  }

  addTrigger(type: 'cron' | 'image_change' | 'code_change_hook') {
    if (this.cronTriggerDisabled() && type === 'cron') {
      return;
    }

    if (this.codeChangeHookTriggerDisabled() && type === 'code_change_hook') {
      return;
    }

    this.formService.addTrigger(type);
    this.triggerSelect.disposeTooltip();
  }

  removeTrigger(trigger: FormGroup) {
    this.formService.removeTrigger(trigger);
  }

  addCredential() {
    const jenkins_integration_id = this.form.value.jenkins_integration_id;

    if (!jenkins_integration_id) {
      return;
    }

    const dialogRef = this.dialogService.open(
      CreateJenkinsCredentialComponent,
      {
        size: DialogSize.Big,
        data: { jenkins_integration_id },
      },
    );

    dialogRef.afterClosed().subscribe((created: any) => {
      if (created) {
        this.formService
          .getCredentials(jenkins_integration_id, created)
          .catch(error => this.message.error(error));
      }
    });
  }

  codeChangeHookTriggerDisabled() {
    return (
      !__get(this.pipeline_data, 'template.template.definition.spec.withSCM') ||
      this.form.value.triggers.filter(
        (trigger: any) => trigger.type === 'code_change_hook',
      ).length > 0
    );
  }

  cronTriggerDisabled() {
    return (
      this.form.value.triggers.filter((trigger: any) => trigger.type === 'cron')
        .length > 0
    );
  }

  paramsChanged(value: any) {
    this.paramsForm = value;
  }

  showParamsHelp() {
    const template: any = __get(this.data, 'template.template.template');
    this.dialogService.open(PipelineTemplateInfoComponent, {
      size: DialogSize.Large,
      data: {
        title: template.display_name[`${this.language}`],
        template: mapperToPageModal(template),
        type: 'update',
      },
    });
  }

  showVarHelp() {
    const { pipeline_scm } = this.formService.toJson();
    const sourceInfo = {
      pipeline_scm,
      triggers: this.triggers.map(trigger => trigger.toJson()),
    };
    const withoutSCM = !__get(
      __get(this.data, 'template.template.template'),
      'definition.spec.withSCM',
      false,
    );
    if (withoutSCM) {
      delete sourceInfo.pipeline_scm;
    }
    this.dialogService.open(PipelineGlobalVarHelpComponent, {
      size: DialogSize.Large,
      data: {
        sourceInfo: sourceInfo,
      },
    });
  }

  paramStatusChanged(value: boolean) {
    this.paramErrorStatus = value;
  }

  onSubmit() {
    if (this.saving || this.form.status !== 'VALID' || this.paramErrorStatus) {
      return;
    }

    const data = {};
    this.fieldModels.forEach((field: any) => {
      if (field.controlType === 'rc-checkbox') {
        data[field.name] = !!this.paramsForm[field.name];
      } else {
        data[field.name] =
          this.paramsForm[field.name] === 'NOT_USE'
            ? ''
            : this.paramsForm[field.name];
      }
    });
    const triggers = this.triggers.map(trigger => trigger.toJson());

    const template_param = {
      uuid: this.template.uuid,
      arguments_values: data,
    };
    const withoutSCM = !__get(
      __get(this.data, 'template.template.template'),
      'definition.spec.withSCM',
      false,
    );
    if (this.data.id) {
      this.formService
        .save(this.data.id, triggers, template_param, withoutSCM)
        .then(result => {
          if (result) {
            this.store.dispatch(new actions.Updated(this.data.id));
            this.dialogRef.close();
          }
        });
    }
  }

  toggleTemplate() {
    this.dialogService.open(PipelineTemplateInfoComponent, {
      size: DialogSize.Large,
      data: {
        title: this.tempalteName,
        template: mapperToPageModal(this.template),
        type: true,
      },
    });
  }

  private getTemplateVersion(template: any): string {
    const annotations =
      __get(template, 'definition.metadata.annotations') || {};
    return annotations[`${this.env.label_base_domain}/version`] || '';
  }
}

function mapperToPageModal(template: any) {
  const {
    name,
    uuid,
    source,
    display_name,
    definition: {
      metadata: { annotations, labels },
      spec: { stages },
      status,
    },
  } = template;
  return {
    name,
    uuid,
    display_name,
    status,
    labels,
    source,
    stages: templateStagesConvert(stages),
    arguments: __get(template, 'definition.spec.arguments'),
    annotations,
  };
}
