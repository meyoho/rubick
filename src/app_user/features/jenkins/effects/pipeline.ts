import { Injectable } from '@angular/core';
import { Actions, Effect, ofType } from '@ngrx/effects';
import { Store } from '@ngrx/store';
import { Action } from '@ngrx/store';
import { get } from 'lodash-es';
import { Observable, from as fromPromise, of } from 'rxjs';
import { catchError, map, switchMap } from 'rxjs/operators';

import { ConfirmType, DialogService } from '@alauda/ui';
import { JenkinsService } from 'app/services/api/jenkins.service';

import { TranslateService } from 'app/translate/translate.service';

import * as actions from '../actions/pipeline';
import { State } from '../reducers';
import { JenkinsMessageService } from '../services/jenkins-message.service';

const DELETE_ERRORS = [
  'IntegrationNotExist',
  'JenkinsServiceConnRefused',
  'JenkinsServiceUnavailable',
];

@Injectable()
export class PipelineEffects {
  @Effect()
  search$: Observable<Action> = this.actions$
    .pipe(ofType(actions.types.Search))
    .pipe(
      switchMap((action: actions.Search) =>
        fromPromise(this.jenkins.pipelines.find(action.params))
          .pipe(
            map(response => {
              return new actions.SearchComplete(action.params, {
                total: response.count,
                items: response.results,
              });
            }),
          )
          .pipe(
            catchError(error => {
              if (!action.ignoreError) {
                this.message.error(error);
              }
              return of(new actions.SearchError(action.params, error));
            }),
          ),
      ),
    );

  @Effect()
  delete$: Observable<Action> = this.actions$
    .pipe(ofType(actions.types.Delete))
    .pipe(
      switchMap((action: actions.Delete) =>
        fromPromise(this.jenkins.pipelines.delete(action.id))
          .pipe(map(() => new actions.Deleted(action.id)))
          .pipe(
            catchError(error => {
              const errorItem = get(error, 'error.errors[0]');
              if (DELETE_ERRORS.includes(errorItem.code)) {
                this.dialogService
                  .confirm({
                    title: this.translate.get(
                      'jenkins_force_delete_pipeline_confirm_title',
                    ),
                    content: this.translate.get(
                      'jenkins_force_delete_pipeline_confirm_content',
                    ),
                    confirmText: this.translate.get('confirm'),
                    cancelText: this.translate.get('cancel'),
                    confirmType: ConfirmType.Danger,
                  })
                  .then(() => {
                    this.jenkins.pipelines
                      .delete(action.id, true)
                      .then(() => {
                        this.store.dispatch(new actions.Deleted(action.id));
                      })
                      .catch(error => {
                        this.message.error(error);
                      });
                  })
                  .catch(() => {});
              } else {
                this.message.error(error);
              }
              return of(new actions.DeleteError(action.id, error));
            }),
          ),
      ),
    );

  @Effect()
  get$: Observable<Action> = this.actions$.pipe(ofType(actions.types.Get)).pipe(
    switchMap((action: actions.Get) =>
      fromPromise(this.jenkins.pipelines.get(action.id))
        .pipe(
          map(response => {
            return new actions.GetComplete(action.id, response);
          }),
        )
        .pipe(
          catchError(error => {
            this.message.error(error);
            return of(new actions.GetError(action.id, error));
          }),
        ),
    ),
  );

  @Effect()
  getComplete$: Observable<Action> = this.actions$
    .pipe(ofType(actions.types.GetComplete))
    .pipe(
      map(
        (action: actions.GetComplete) =>
          new actions.GetHistories(action.id, {
            jenkins_integration_id: action.result.jenkins_integration_id,
            page: 1,
            page_size: 10,
          }),
      ),
    );

  @Effect()
  getHistories$: Observable<Action> = this.actions$
    .pipe(ofType(actions.types.GetHistories))
    .pipe(
      switchMap((action: actions.GetHistories) =>
        fromPromise(
          this.jenkins.histories.find({
            pipeline_uuid: action.id,
            jenkins_integration_id: action.params.jenkins_integration_id,
            page: action.params.page,
            page_size: action.params.page_size,
          }),
        )
          .pipe(
            map(response => {
              return new actions.GetHistoriesComplete(
                action.id,
                action.params,
                {
                  total: response.count,
                  items: response.results,
                },
              );
            }),
          )
          .pipe(
            catchError(error => {
              this.message.error(error);
              return of(new actions.GetHistoriesError(action.id, error));
            }),
          ),
      ),
    );

  constructor(
    private jenkins: JenkinsService,
    private message: JenkinsMessageService,
    private actions$: Actions,
    private dialogService: DialogService,
    private translate: TranslateService,
    private store: Store<State>,
  ) {}
}
