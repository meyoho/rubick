import { NgModule } from '@angular/core';
import { EffectsModule } from '@ngrx/effects';
import { StoreModule } from '@ngrx/store';
import { SharedModule } from 'app/shared/shared.module';
import { TemplateEffects } from 'app2/features/jenkins/effects/template';
import { TemplateSourcesEffects } from 'app2/features/jenkins/effects/template_sources';
import { reducers as templateReducers } from 'app2/features/jenkins/reducers';
import { JenkinsSharedModule } from 'app2/features/lazy/jenkins.shared.module';
import { CodeRepositorySelectComponent } from 'app_user/features/jenkins/components/code-repository-select';
import { CodeRepositorySelectModalComponent } from 'app_user/features/jenkins/components/code-repository-select-modal';
import { CreateJenkinsCredentialComponent } from 'app_user/features/jenkins/components/create-jenkins-credential';
import { JenkinsfileContentExampleComponent } from 'app_user/features/jenkins/components/jenkinsfile-content-example';
import { PipelineFormTriggerComponent } from 'app_user/features/jenkins/components/pipeline-form-trigger';
import { PipelineHistoryLogsComponent } from 'app_user/features/jenkins/components/pipeline-history-logs';
import { PipelineHistoryTableComponent } from 'app_user/features/jenkins/components/pipeline-history-table';
import { PipelineHistoryTestResultComponent } from 'app_user/features/jenkins/components/pipeline-history-test-result';
import { PipelineTemplateParamsFormComponent } from 'app_user/features/jenkins/components/pipeline-template-form';
import { RecentHistoriesComponent } from 'app_user/features/jenkins/components/recent-histories';
import { RepoNotBindHintComponent } from 'app_user/features/jenkins/components/repo-not-bind-hint';
import { PipelineCreateComponent } from 'app_user/features/jenkins/containers/pipeline-create';
import {
  PipelineCreateByTemplateComponent,
  PipelineCreateByTemplateStepsComponent,
} from 'app_user/features/jenkins/containers/pipeline-create-by-template';
import { PipelineCreateModeComponent } from 'app_user/features/jenkins/containers/pipeline-create-mode';
import { PipelineDetailComponent } from 'app_user/features/jenkins/containers/pipeline-detail';
import { PipelineEditComponent } from 'app_user/features/jenkins/containers/pipeline-edit';
import { PipelineHistoryDetailComponent } from 'app_user/features/jenkins/containers/pipeline-history-detail';
import { PipelineHistoryListComponent } from 'app_user/features/jenkins/containers/pipeline-history-list';
import { PipelineListComponent } from 'app_user/features/jenkins/containers/pipeline-list';
import { PipelineUpdateByTemplateComponent } from 'app_user/features/jenkins/containers/pipeline-update-by-template';
import { HistoryEffects } from 'app_user/features/jenkins/effects/histories';
import { PipelineEffects } from 'app_user/features/jenkins/effects/pipeline';
import { reducers } from 'app_user/features/jenkins/reducers';
// services
import { PipelineFormService } from 'app_user/features/jenkins/services/pipeline-form.service';
import { PipelineTemplateService } from 'app_user/features/jenkins/services/pipeline-template.service';

import { ImageSelectComponent } from './components/pipeline-dynamic-form/image-select/image-select.component';
import { JenkinsRoutingModule } from './jenkins.routing.module';

const components = [
  PipelineListComponent,
  PipelineDetailComponent,
  PipelineCreateComponent,
  PipelineEditComponent,
  PipelineHistoryListComponent,
  PipelineHistoryDetailComponent,
  PipelineCreateModeComponent,
  PipelineUpdateByTemplateComponent,
  PipelineCreateByTemplateComponent,
  PipelineCreateByTemplateStepsComponent,
  CodeRepositorySelectComponent,
  CodeRepositorySelectModalComponent,
  CreateJenkinsCredentialComponent,
  PipelineFormTriggerComponent,
  PipelineHistoryLogsComponent,
  PipelineHistoryTableComponent,
  PipelineHistoryTestResultComponent,
  PipelineTemplateParamsFormComponent,
  RecentHistoriesComponent,
  JenkinsfileContentExampleComponent,
  RepoNotBindHintComponent,
  ImageSelectComponent,
];

const entryComponents = [
  PipelineEditComponent,
  PipelineCreateModeComponent,
  PipelineUpdateByTemplateComponent,

  CodeRepositorySelectModalComponent,
  CreateJenkinsCredentialComponent,
  JenkinsfileContentExampleComponent,
  ImageSelectComponent,
];

const providers = [PipelineFormService, PipelineTemplateService];
@NgModule({
  imports: [
    SharedModule,
    JenkinsSharedModule,
    JenkinsRoutingModule,
    StoreModule.forFeature('jenkins', reducers),
    StoreModule.forFeature('jenkinsTemplate', templateReducers),
    EffectsModule.forFeature([
      PipelineEffects,
      HistoryEffects,
      TemplateEffects,
      TemplateSourcesEffects,
    ]),
  ],
  exports: components,
  declarations: components,
  providers,
  entryComponents,
})
export class JenkinsModule {}
