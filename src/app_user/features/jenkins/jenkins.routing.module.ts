import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PipelineCreateComponent } from 'app_user/features/jenkins/containers/pipeline-create';
import { PipelineCreateByTemplateComponent } from 'app_user/features/jenkins/containers/pipeline-create-by-template';
import { PipelineDetailComponent } from 'app_user/features/jenkins/containers/pipeline-detail';
import { PipelineHistoryDetailComponent } from 'app_user/features/jenkins/containers/pipeline-history-detail';
import { PipelineHistoryListComponent } from 'app_user/features/jenkins/containers/pipeline-history-list';
import { PipelineListComponent } from 'app_user/features/jenkins/containers/pipeline-list';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'pipelines',
    pathMatch: 'full',
  },
  {
    path: 'pipelines', // querystring: ?keywords=:keywords&page=:page&edit=:id
    component: PipelineListComponent,
  },
  {
    path: 'pipelines_new', // querystring: ?keywords=:keywords&page=:page&edit=:id
    component: PipelineListComponent,
  },
  {
    path: 'pipelines/create',
    component: PipelineCreateComponent,
  },
  {
    path: 'pipelines/templates/template_create_pipeline/:id',
    component: PipelineCreateByTemplateComponent,
  },
  {
    path: 'pipelines/:id', // querystring: ?edit
    component: PipelineDetailComponent,
  },
  {
    path: 'histories', // querystring: ?keywords=:keywords&page=:page
    component: PipelineHistoryListComponent,
  },
  {
    path: 'histories/:pipeline_uuid/:id',
    component: PipelineHistoryDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class JenkinsRoutingModule {}
