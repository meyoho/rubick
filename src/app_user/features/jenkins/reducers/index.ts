import { createFeatureSelector, createSelector } from '@ngrx/store';

import * as fromRoot from 'app/store';
import * as fromHistoryDetail from './history-detail';
import * as fromHistoryList from './history-list';
import * as fromPipelineDetail from './pipeline-detail';
import * as fromPipelineList from './pipeline-list';

export interface JenkinsState {
  pipelineList: fromPipelineList.State;
  pipelineDetail: fromPipelineDetail.State;
  historyList: fromHistoryList.State;
  historyDetail: fromHistoryDetail.State;
}

export interface State extends fromRoot.AppState {
  jenkins: JenkinsState;
}

export const reducers = {
  pipelineList: fromPipelineList.reducer,
  pipelineDetail: fromPipelineDetail.reducer,
  historyList: fromHistoryList.reducer,
  historyDetail: fromHistoryDetail.reducer,
};

const selectJenkinsFeature = createFeatureSelector<JenkinsState>('jenkins');

export const selectPipelineList = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.pipelineList,
);

export const selectPipelineDetail = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.pipelineDetail,
);

export const selectHistoryList = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.historyList,
);

export const selectHistoryDetail = createSelector(
  selectJenkinsFeature,
  (state: JenkinsState) => state.historyDetail,
);

// pipeline-list
export const selectPipelineListLoading = createSelector(
  selectPipelineList,
  fromPipelineList.selectLoading,
);
export const selectPipelineListTotal = createSelector(
  selectPipelineList,
  fromPipelineList.selectTotal,
);
export const selectPipelineListParams = createSelector(
  selectPipelineList,
  fromPipelineList.selectActivatedParams,
);
export const selectPipelineListMutatingItems = createSelector(
  selectPipelineList,
  fromPipelineList.selectMutatingItems,
);
export const selectPipelineListItems = createSelector(
  selectPipelineList,
  fromPipelineList.selectActivatedQuery,
);

// pipeline-detail
export const selectPipelineDetailData = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectData,
);

export const selectPipelineDetailId = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectActivatedId,
);

export const selectPipelineDetailLoading = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectLoading,
);

export const selectPipelineDetailMutating = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectMutating,
);

export const selectPipelineDetailHistoriesLoading = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectHistoriesLoading,
);

export const selectPipelineDetailHistoriesPage = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectActivatedPage,
);

export const selectPipelineDetailHistoriesTotal = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectHistoriesTotal,
);

export const selectPipelineDetailHistories = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectHistories,
);

export const selectPipelineDetailMutatingHistories = createSelector(
  selectPipelineDetail,
  fromPipelineDetail.selectMutatingHistories,
);

// history-list
export const selectHistoryListLoading = createSelector(
  selectHistoryList,
  fromHistoryList.selectLoading,
);

export const selectHistoryListPage = createSelector(
  selectHistoryList,
  fromHistoryList.selectActivatedPage,
);

export const selectHistoryListTotal = createSelector(
  selectHistoryList,
  fromHistoryList.selectTotal,
);
export const selectHistoryListParams = createSelector(
  selectHistoryList,
  fromHistoryList.selectActivatedParams,
);
export const selectHistoryListMutatingItems = createSelector(
  selectHistoryList,
  fromHistoryList.selectMutatingItems,
);
export const selectHistoryListItems = createSelector(
  selectHistoryList,
  fromHistoryList.selectActivatedQuery,
);

// history-detail
export const selectHistoryDetailData = createSelector(
  selectHistoryDetail,
  fromHistoryDetail.selectData,
);

export const selectHistoryDetailId = createSelector(
  selectHistoryDetail,
  fromHistoryDetail.selectActivatedId,
);

export const selectHistoryDetailMutating = createSelector(
  selectHistoryDetail,
  fromHistoryDetail.selectMutating,
);

export const selectHistoryDetailLoading = createSelector(
  selectHistoryDetail,
  fromHistoryDetail.selectLoading,
);
