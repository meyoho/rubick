import { createSelector } from '@ngrx/store';

import { omit } from 'lodash-es';

import * as accountActions from 'app/store/actions/account.actions';
import * as historyActions from '../actions/history';
import * as actions from '../actions/pipeline';

export interface State {
  mutating: 'starting' | 'deleting';
  loading: boolean;
  error: Error;
  data: any;
  id: string;
  historiesLoading: boolean;
  historiesError: any;
  historiesTotal: number;
  historiesPage: number;
  pages: {
    [page: number]: any[];
  };
  mutatingHistories: {
    [id: string]: 'deleting' | 'canceling';
  };
}

export const initialState: State = {
  mutating: null,
  loading: false,
  error: null,
  data: null,
  id: null,
  historiesLoading: false,
  historiesError: null,
  historiesTotal: 0,
  historiesPage: 1,
  pages: {},
  mutatingHistories: {},
};

export function reducer(
  state = initialState,
  action: actions.All | historyActions.All | accountActions.SetAccount,
) {
  if (action.type === accountActions.SET_ACCOUNT) {
    return initialState;
  }
  switch (action.type) {
    case actions.types.Delete:
      return flagMutating(state, action, 'deleting');
    case actions.types.DeleteError:
      return unflagMutating(state, action);
    case actions.types.Deleted:
      return clearWhenDeleted(state, action);
    case actions.types.Start:
      return flagMutating(state, action, 'starting');
    case actions.types.StartError:
      return unflagMutating(state, action);
    case actions.types.Get:
      return get(state, action);
    case actions.types.GetComplete:
      return getComplete(state, action);
    case actions.types.GetError:
      return getError(state, action);
    case actions.types.GetHistories:
      return getHistories(state, action);
    case actions.types.GetHistoriesComplete:
      return getHistoriesComplete(state, action);
    case actions.types.GetHistoriesError:
      return getHistoriesError(state, action);
    case historyActions.types.Started:
      return unflagStartingByHistoryAction(state, action);
    case historyActions.types.Delete:
      return flagMutatingHistories(
        state,
        action as historyActions.Delete,
        'deleting',
      );
    case historyActions.types.Deleted:
      return removeDeletedItem(state, action as historyActions.Deleted);
    case historyActions.types.DeleteError:
      return unflagMutatingHistories(
        state,
        action as historyActions.DeleteError,
      );
    case historyActions.types.Cancel:
      return flagMutatingHistories(
        state,
        action as historyActions.Cancel,
        'canceling',
      );
    case historyActions.types.Canceled:
      return unflagMutatingHistories(state, action as historyActions.Canceled);
    case historyActions.types.CancelError:
      return unflagMutatingHistories(
        state,
        action as historyActions.CancelError,
      );
    default:
      return state;
  }
}

export const selectActivatedId = (state: State) => state.id;
export const selectActivatedPage = (state: State) => state.historiesPage;
export const selectAllPages = (state: State) => state.pages;
export const selectData = (state: State) => state.data;
export const selectMutating = (state: State) => state.mutating;
export const selectHistories = createSelector(
  selectActivatedPage,
  selectAllPages,
  (page, pages) => pages[page] || [],
);
export const selectLoading = (state: State) => state.loading;
export const selectHistoriesLoading = (state: State) => state.historiesLoading;
export const selectHistoriesTotal = (state: State) => state.historiesTotal;
export const selectMutatingHistories = (state: State) =>
  state.mutatingHistories;

function historyIdentity(id: string, pipeline_uuid: string) {
  return `${pipeline_uuid}#${id}`;
}

function unflagStartingByHistoryAction(
  state: State,
  action: historyActions.Started,
): State {
  if (action.pipeline_uuid !== state.id) {
    return state;
  }

  return {
    ...state,
    mutating: null,
  };
}

function flagMutating(
  state: State,
  action: actions.Start | actions.Delete,
  mutating: 'starting' | 'deleting',
) {
  if (action.id !== state.id) {
    return state;
  }

  return {
    ...state,
    mutating,
  };
}

function unflagMutating(
  state: State,
  action: actions.StartError | actions.DeleteError,
) {
  if (action.id !== state.id) {
    return state;
  }

  return {
    ...state,
    mutating: null,
  };
}

function unflagMutatingHistories(
  state: State,
  action:
    | historyActions.Canceled
    | historyActions.CancelError
    | historyActions.DeleteError,
): State {
  const { pipeline_uuid, id } = action;
  if (pipeline_uuid !== state.id) {
    return state;
  }

  const identity = historyIdentity(id, pipeline_uuid);

  if (state.mutatingHistories[identity]) {
    return {
      ...state,
      mutatingHistories: omit(state.mutatingHistories, [identity]),
    };
  }

  return state;
}

function removeDeletedItem(
  state: State,
  action: historyActions.Deleted,
): State {
  const { id, pipeline_uuid } = action;
  if (action.pipeline_uuid !== state.id) {
    return state;
  }

  const affected = Object.keys(state.pages || {}).filter(page =>
    state.pages[+page].some(item => item.history_id === action.id),
  );

  const identity = historyIdentity(id, pipeline_uuid);

  const mutatingHistories: any = state.mutatingHistories[identity]
    ? omit(state.mutatingHistories, [identity])
    : state.mutatingHistories;

  if (affected.length || mutatingHistories !== state.mutatingHistories) {
    return {
      ...state,
      mutatingHistories,
      pages: {
        ...state.pages,
        ...affected.reduce((result, page) => {
          return {
            ...result,
            [+page]: state.pages[+page].filter(
              item => item.history_id !== action.id,
            ),
          };
        }, {}),
      },
    };
  }

  return state;
}

function flagMutatingHistories(
  state: State,
  action: historyActions.Delete | historyActions.Cancel,
  mutating: 'deleting' | 'canceling',
): State {
  const { id, pipeline_uuid } = action;
  if (pipeline_uuid !== state.id) {
    return state;
  }

  const identity = historyIdentity(id, pipeline_uuid);

  return {
    ...state,
    mutatingHistories: {
      ...state.mutatingHistories,
      [identity]: mutating,
    },
  };
}

function getHistoriesError(
  state: State,
  action: actions.GetHistoriesError,
): State {
  if (action.id !== state.id) {
    return state;
  }

  return {
    ...state,
    historiesLoading: false,
    historiesError: action.error,
  };
}

function getHistoriesComplete(
  state: State,
  action: actions.GetHistoriesComplete,
): State {
  if (action.id !== state.id) {
    return state;
  }

  return {
    ...state,
    historiesLoading: false,
    historiesTotal: action.result.total,
    pages: {
      ...state.pages,
      [action.params.page]: action.result.items,
    },
  };
}

function getHistories(state: State, action: actions.GetHistories): State {
  if (action.id !== state.id) {
    return state;
  }

  return {
    ...state,
    historiesLoading: true,
    historiesError: null,
    historiesPage: action.params.page,
  };
}

function getError(state: State, action: actions.GetError): State {
  if (action.id !== state.id) {
    return state;
  }

  return {
    ...state,
    loading: false,
    error: action.error,
  };
}

function getComplete(state: State, action: actions.GetComplete): State {
  if (action.id !== state.id) {
    return state;
  }

  return {
    ...state,
    loading: false,
    data: action.result,
  };
}

function get(state: State, action: actions.Get): State {
  if (action.id === state.id) {
    return {
      ...state,
      error: null,
      loading: true,
    };
  }

  return {
    ...initialState,
    id: action.id,
    loading: true,
    error: null,
  };
}

function clearWhenDeleted(state: State, action: actions.Deleted): State {
  if (action.id === state.id) {
    return {
      ...initialState,
      id: action.id,
    };
  }
  return state;
}
