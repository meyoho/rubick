import { NotificationService } from '@alauda/ui';
import { Injectable } from '@angular/core';

import { get } from 'lodash-es';

import { TranslateService } from 'app/translate/translate.service';

@Injectable()
export class JenkinsMessageService {
  errors = {
    JenkinsServiceConnRefused: {
      message: ({ integration_name }: any) =>
        this.translate.get('jenkins_service_conn_refused', {
          integration_name,
        }),
    },
    JenkinsServiceUnavailable: {
      title: () => this.translate.get('jenkins_service_unavailable_title'),
      message: ({ integration_id, integration_name }: any) =>
        this.translate.get('jenkins_service_unavailable_content', {
          integration_id,
          integration_name,
        }),
    },
    JenkinsIntegrationInActive: {
      title: () => this.translate.get('jenkins_integration_in_active_title'),
      message: ({ integration_id, integration_name }: any) =>
        this.translate.get('jenkins_integration_in_active_content', {
          integration_id,
          integration_name,
        }),
    },
    JenkinsPipelineHisNotSupportReplay: {
      message: () =>
        this.translate.get('jenkins_pipeline_history_not_support_replay'),
    },
    JenkinsPipelineNameAlreadyExist: {
      message: () => this.translate.get('jenkins_pipeline_name_already_exist'),
    },
    JenkinsUnauthorized: {
      message: ({ integration_name }: any) =>
        this.translate.get('jenkins_unauthorized', {
          integration_name,
        }),
    },
    IntegrationHostResolveError: {
      message: ({ integration_name }: any) =>
        this.translate.get('jenkins_integration_host_error', {
          integration_name,
        }),
    },
    IntegrationNotExist: {
      message: ({ integration_id }: any) =>
        this.translate.get('jenkins_integration_not_exist', {
          integration_id,
        }),
    },
    PipelineTemplateImportTaskIsAlreadyRunning: {
      message: () =>
        this.translate.get('jenkins_template_sync_is_already_running'),
    },
    InvalidTemplateArgValues: {
      message: () =>
        this.translate.get('jenkins_template_argument_value_invalid'),
    },
  };

  constructor(
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {}

  error(error: any) {
    if (error === 'canceled') {
      return;
    }

    const errorItem: any = get(error, 'error.errors[0]');

    if (errorItem && errorItem.code && this.errors[errorItem.code]) {
      const target = this.errors[errorItem.code];
      this.auiNotificationService.error({
        title: (target.title && target.title(errorItem.data)) || '',
        content: target.message(errorItem.data),
      });
    } else if (errorItem && errorItem.message) {
      this.auiNotificationService.error({
        content: errorItem.message,
      });
    } else {
      this.auiNotificationService.error({
        content: error.message || 'unkown error',
      });
    }
  }

  warning(error: any) {
    if (error && error.code && this.errors[error.code]) {
      const target = this.errors[error.code];
      this.auiNotificationService.warning({
        title: (target.title && target.title(error.data)) || '',
        content: target.message(error.data),
      });
    } else {
      this.auiNotificationService.warning({
        content: error.message || 'unknow error',
      });
    }
  }

  info(message: string) {
    this.auiNotificationService.info({
      title: message,
    });
  }

  success(_message: string, _description?: string) {}
}
