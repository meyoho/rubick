import { Injectable } from '@angular/core';
import { FormArray, FormBuilder, FormGroup } from '@angular/forms';

import { head, omit, pick } from 'lodash-es';

import { IntegrationService } from 'app/services/api/integration.service';
import { JenkinsService } from 'app/services/api/jenkins.service';
import { PrivateBuildCodeClientService } from 'app/services/api/private-build-code-client.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { SpaceService } from 'app/services/api/space.service';
import { TranslateService } from 'app/translate/translate.service';
import { JenkinsMessageService } from '../services/jenkins-message.service';
import { CancelableTask } from '../utils/cancelable-task';

const scriptTypes = [
  { type: 'FROM_SCM', name: 'jenkins_script_from_scm' },
  { type: 'INLINE', name: 'jenkins_script_inline' },
];

const defaultModel = {
  name: '',
  display_name: '',
  space_name: '',
  jenkins_integration_id: '',
  description: '',
  icon: '',
  source: '',
  repository_path: '',
  code_org: '',
  code_repo: '',
  credential_name: '',
  branch: 'master',
  type: '',
  script: '',
  script_path: 'Jenkinsfile',
};

const defaultTrigger = {
  type: '',
  cron: 'H * * * *',
  registry: '',
  project: '',
  repository: '',
  image_tag_filter: '',
  useImageTagFilter: false,
  // image_trigger_type: '',
  // trigger_version: '',
};

@Injectable()
export class PipelineFormService {
  form: FormGroup;
  initialValue: any = {};
  initialized = false;
  saving = false;

  projectName: string;
  clusterName: string;
  namespaceName: string;

  private released = false;
  private triggerLastId = 0;
  private _scriptTypes: any[];
  private _createCredentialEnabled = false;
  private _spaces: any[];
  private _integrations: any[];
  private _credentials: any[];
  private _codeClients: any[];
  private _codeOrgs: any[];

  private fetchData = new CancelableTask((id: string) => {
    return this.jenkins.pipelines.get(id);
  });

  private fetchCreateCredentialEnabled = new CancelableTask(() => {
    return this.roleUtil
      .resourceTypeSupportPermissions('jenkins_credential', null, 'create')
      .catch(error => {
        this.errorMessage(error);
        return false;
      });
  });

  private fetchSpaces = new CancelableTask(async () => {
    return await this.spaceService
      .getSpaces()
      .then(res => res.results)
      .catch(error => {
        this.errorMessage(error);
        return [] as any[];
      });
  });

  private fetchIntegrations = new CancelableTask(async () => {
    return await this.integrationService
      .getIntegrations({
        types: 'Jenkins',
        page: 1,
        page_size: 200,
      })
      .then(results => results.filter(item => item.enabled))
      .catch(error => {
        this.errorMessage(error);
        return [] as any[];
      });
  });

  private fetchCredentials = new CancelableTask(
    async (integrationId: string) => {
      return await this.jenkins.credentials
        .find({ jenkins_integration_id: integrationId })
        .then(result => [
          {
            name: 'NOT_USE',
            display_name: this.translate.get('jenkins_credentials_not_use'),
          },
          ...result,
        ])
        .catch(error => {
          this.errorMessage(error);
          return [];
        });
    },
  );

  private fetchCodeClients = new CancelableTask(async () => {
    return await this.privateBuildCodeClientService
      .find()
      .then(response => [
        ...response,
        {
          name: 'NOT_USE',
          displayName: this.translate.get('jenkins_client_not_use'),
        },
      ])
      .catch(error => {
        this.errorMessage(error);
        return [];
      });
  });

  private fetchCodeOrgs = new CancelableTask(async (codeClient: string) => {
    return await this.privateBuildCodeClientService
      .findCodeOrgs(codeClient)
      .catch(error => {
        this.errorMessage(error);
        return [];
      });
  });

  get scriptTypes() {
    return this._scriptTypes;
  }

  set scriptTypes(value: any[]) {
    this.safeSet(() => (this._scriptTypes = value));
  }

  get createCredentialEnabled() {
    return this._createCredentialEnabled;
  }

  set createCredentialEnabled(value: boolean) {
    this.safeSet(() => (this._createCredentialEnabled = value));
  }

  get spaces() {
    return this._spaces;
  }

  set spaces(value: any[]) {
    this.safeSet(() => (this._spaces = value));
  }

  get integrations() {
    return this._integrations;
  }

  set integrations(value: any[]) {
    this.safeSet(() => (this._integrations = value));
  }

  get credentials() {
    return this._credentials;
  }

  set credentials(value: any[]) {
    this.safeSet(() => (this._credentials = value));
  }

  get codeClients() {
    return this._codeClients;
  }

  set codeClients(value: any[]) {
    this.safeSet(() => (this._codeClients = value));
  }

  get codeOrgs() {
    return this._codeOrgs;
  }

  set codeOrgs(value: any[]) {
    this.safeSet(() => (this._codeOrgs = value));
  }

  get isSelectedCodeClientNotBind() {
    if (
      !this.form.value.source ||
      !this.codeClients ||
      !this.codeClients.length
    ) {
      return false;
    }

    const client = this.codeClients.find(
      item => item.name === this.form.value.source,
    );

    return client && !client.is_authed;
  }

  constructor(
    private jenkins: JenkinsService,
    private integrationService: IntegrationService,
    private privateBuildCodeClientService: PrivateBuildCodeClientService,
    private translate: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private message: JenkinsMessageService,
    private spaceService: SpaceService,
    private fb: FormBuilder,
  ) {
    this.createForm();
    this.scriptTypes = scriptTypes.map(item => ({
      type: item.type,
      name: this.translate.get(item.name),
    }));
  }

  createForm() {
    this.form = this.fb.group({
      ...defaultModel,
      triggers: this.fb.array([]),
    });
  }

  onInit(payload: any) {
    return this.init(payload);
  }

  async init(payload: any) {
    const { id, isClone, projectName, clusterName, namespaceName } = payload;
    this.projectName = projectName;
    this.clusterName = clusterName;
    this.namespaceName = namespaceName;
    this.initialized = false;
    this.initialValue = null;
    this.credentials = [];
    this.codeClients = [];
    this.codeOrgs = [];

    this.initialValue = id
      ? this.toFormValue(await this.fetchData.run(id), isClone)
      : {};
    this.createCredentialEnabled = await this.fetchCreateCredentialEnabled.run();
    this.resetForm(this.initialValue);
    await Promise.all([
      this.getSpaces(),
      this.getIntegrations(),
      this.getCodeClients(),
      this.getCredentials(this.initialValue.jenkins_integration_id),
      this.getCodeOrgs(this.initialValue.source),
    ]);
    this.initialized = true;
  }

  onIntegrationChange(value: string) {
    this.setValuePrisine('credential_name', '');
    this.getCredentials(value).catch(() => {});
  }

  onCodeClientChange(value: string) {
    if (value === 'NOT_USE') {
      this.setValuePrisine('type', 'INLINE');
    }
    this.setValuePrisine('code_org', '');
    this.setValuePrisine('code_repo', '');
    this.getCodeOrgs(value).catch(() => {});
  }

  onCodeOrgChange(value: string) {
    if (value) {
      this.setValuePrisine('code_repo', '');
    }
  }

  async getSpaces() {
    this.spaces = null;
    this.spaces = await this.fetchSpaces.run();
    this.setDefaultValue(space => space.name, 'space_name', this.spaces, '');
  }

  async getIntegrations() {
    this.integrations = null;
    this.integrations = await this.fetchIntegrations.run();
    this.setDefaultValue(
      integration => integration.id,
      'jenkins_integration_id',
      this.integrations,
      '',
    );
  }

  async getCredentials(
    integrationId: string,
    created = '',
    notSetValue?: boolean,
  ) {
    if (!integrationId) {
      return;
    }
    this.credentials = null;
    this.credentials = await this.fetchCredentials.run(integrationId);
    if (!notSetValue) {
      if (!created) {
        this.setDefaultValue(
          credential => credential.name,
          'credential_name',
          this.credentials,
          '',
        );
      } else {
        this.setValuePrisine('credential_name', created);
      }
    }
  }

  async getCodeClients() {
    this.codeClients = null;
    this.codeClients = await this.fetchCodeClients.run();
    this.setDefaultValue(
      codeClient => codeClient.name,
      'source',
      this.codeClients,
      '',
    );
  }

  async getCodeOrgs(codeClient: string) {
    if (codeClient && this.codeClients && this.codeClients.length) {
      const codeClientOption = this.codeClients.find(
        item => item.name === codeClient,
      );

      if (codeClientOption && !codeClientOption.is_authed) {
        return;
      }
    }

    if (!codeClient || ['GIT', 'SVN', 'NOT_USE'].includes(codeClient)) {
      return;
    }
    this.codeOrgs = null;
    this.codeOrgs = await this.fetchCodeOrgs.run(codeClient);
    this.setDefaultValue(
      codeOrg => codeOrg.name,
      'code_org',
      this.codeOrgs,
      '',
    );
  }

  setValuePrisine(field: string, value: any) {
    if (this.released) {
      return;
    }
    if (this.form.controls[field].value === value) {
      return;
    }
    this.form.controls[field].setValue(value);
    this.form.controls[field].markAsPristine();
  }

  setDefaultValue<TValue>(
    getter: (option: any) => TValue,
    field: string,
    options: any[],
    _defaultTo: TValue,
  ) {
    if (this.released) {
      return;
    }
    const control = this.form.controls[field];
    if (!control.value) {
      const first = head(options);
      const value = first && getter(first);
      if (value) {
        control.setValue(value);
        control.markAsPristine();
      }
    }
  }

  resetForm(value: any) {
    if (this.released) {
      return;
    }
    const { triggers, ...rest } = value;

    const model = {
      ...defaultModel,
      type: 'FROM_SCM',
      ...rest,
    };

    this.form.reset(model);
    const triggerFGs = (triggers || []).map((trigger: any) =>
      this.fb.group({
        ...defaultTrigger,
        ...trigger,
        '@@id': ++this.triggerLastId,
      }),
    );
    this.form.setControl('triggers', this.fb.array(triggerFGs));
    this.form.markAsPristine();
  }

  addTrigger(type: 'cron' | 'image_change' | 'code_change_hook') {
    const triggers = this.form.get('triggers') as FormArray;
    triggers.push(
      this.fb.group({
        ...defaultTrigger,
        type,
        '@@id': ++this.triggerLastId,
      }),
    );
  }

  removeTrigger(trigger: FormGroup) {
    const triggers = this.form.get('triggers') as FormArray;
    const index = triggers.controls.indexOf(trigger);
    triggers.removeAt(index);
  }

  toFormValue(json: any, isClone?: boolean) {
    const {
      name,
      display_name,
      pipeline_scm,
      triggers,
      script_path,
      ...rest
    } = json;
    const picked = pick(rest, [
      'space_name',
      'jenkins_integration_id',
      'description',
      'icon',
      'type',
      'script',
    ]);

    const { repository_path, credential_name, source, branch } =
      pipeline_scm || ({} as any);
    const useCustomRepo = ['GIT', 'SVN'].includes(source);
    const [code_org, code_repo] = useCustomRepo
      ? ['', '']
      : (repository_path || '').split('/');

    return {
      ...picked,
      name: isClone ? `${name}-copy` : name,
      display_name: isClone ? `${display_name}-copy` : display_name,
      source: source || 'NOT_USE',
      repository_path: useCustomRepo ? repository_path : '',
      credential_name: useCustomRepo ? credential_name || '' : '',
      code_org: useCustomRepo || !source ? '' : code_org,
      code_repo: useCustomRepo || !source ? '' : code_repo,
      branch: branch || 'master',
      script_path: script_path || 'Jenkinsfile',
      triggers: (triggers || []).map(({ type, meta }: any) => {
        const projectedRepository =
          (meta.repository || '').indexOf('/') >= 0
            ? meta.repository
            : `/${meta.repository || ''}`;

        const [project, repository] =
          type === 'image_change' ? projectedRepository.split('/') : ['', ''];

        return {
          type,
          cron: meta.cron || '',
          registry: meta.registry || '',
          project: project || '@@shared',
          repository,
          image_tag_filter: meta.image_tag_filter || '',
          useImageTagFilter: !!meta.image_tag_filter,
        };
      }),
    };
  }

  getJenkinsIntegrationName(id: string) {
    const integration = this.integrations.find(item => item.id === id);
    return (integration && integration.name) || '';
  }

  toJson() {
    const {
      code_org,
      code_repo,
      source,
      repository_path,
      branch,
      credential_name,
      ...rest
    } = this.form.value;

    const pipeline_scm =
      source === 'NOT_USE'
        ? null
        : {
            source,
            repository_path: ['GIT', 'SVN'].includes(source)
              ? repository_path
              : `${code_org}/${code_repo}`,
            branch: source === 'SVN' ? null : branch,
            credential_name: !['GIT', 'SVN'].includes(source)
              ? ''
              : credential_name === 'NOT_USE'
              ? ''
              : credential_name,
          };

    return {
      ...omit(rest, ['triggers']),
      jenkins_integration_name: this.getJenkinsIntegrationName(
        rest.jenkins_integration_id,
      ),
      pipeline_scm,
    };
  }

  async save(
    id = '',
    triggers: any[] = [],
    template?: any,
    withoutSCM?: boolean,
    preview = false,
  ) {
    if (
      ['NOT_USE', ''].includes(this.form.value.source) &&
      triggers.some(trigger => trigger.type === 'code_change_hook')
    ) {
      return;
    }

    const data: any = { ...this.toJson(), triggers, template };
    if (withoutSCM) {
      delete data.pipeline_scm;
    }
    if (!data.display_name) {
      data.display_name = data.name;
    }
    if (!preview) {
      this.saving = true;
      const { response, error } = await (id
        ? this.jenkins.pipelines.update(id, data)
        : this.jenkins.pipelines.create(data)
      )
        .then(response => ({ response: response, error: null }))
        .catch(error => ({ response: null, error }));

      if (error) {
        this.errorMessage(error);
      }

      this.saving = false;

      if (!error) {
        return id || response.uuid;
      } else {
        return null;
      }
    } else {
      return this.jenkins.templates.previewJenkinsfile(id, data);
    }
  }

  errorMessage(error: any) {
    this.message.error(error);
  }

  safeSet(fn: any) {
    if (this.released) {
      return;
    }
    fn();
  }

  release() {
    this.released = true;
    this.fetchData.cancel();
    this.fetchCreateCredentialEnabled.cancel();
    this.fetchSpaces.cancel();
    this.fetchIntegrations.cancel();
    this.fetchCredentials.cancel();
    this.fetchCodeClients.cancel();
    this.fetchCodeOrgs.cancel();
  }

  setRelease(value: boolean) {
    this.released = value;
  }
}
