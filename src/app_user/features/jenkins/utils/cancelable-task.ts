export class CancelableTask<T> {
  private cancelToken: (reason?: any) => void;
  constructor(private task: (...params: any[]) => Promise<T>) {}

  cancel() {
    if (this.cancelToken) {
      this.cancelToken();
      this.cancelToken = null;
    }
  }

  run(...params: any[]): Promise<T> {
    this.cancel();
    return new Promise((resolve, reject) => {
      const cancelToken = () => {
        reject('canceled');
      };
      const taskResolve = (result: T) => {
        if (this.cancelToken === cancelToken) {
          resolve(result);
          this.cancelToken = null;
        }
      };
      const taskReject = (reason?: any) => {
        if (this.cancelToken === cancelToken) {
          reject(reason);
          this.cancelToken = null;
        }
      };

      this.task(...params).then(taskResolve, taskReject);
      this.cancelToken = cancelToken;
    });
  }
}
