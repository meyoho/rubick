import { Observable, Subscription } from 'rxjs';

// TODO: use only when raised effect depends on component or effect change component local state.
// maybe remove when ngrx take control whole application state.
export function ComponentEffect() {
  return function(target: any, propertyName: string) {
    let _value = target[propertyName];
    let _subscription: Subscription = null;
    const getter = function() {
      return _value;
    };

    const setter = function(value: Observable<any>) {
      if (_subscription) {
        _subscription.unsubscribe();
      }
      _value = value;
      if (_value) {
        _subscription = _value.subscribe(
          () => {},
          () => {},
          () => {
            _subscription.unsubscribe();
            _value = null;
          },
        );
      }
    };

    if (delete target[propertyName]) {
      Object.defineProperty(target, propertyName, {
        get: getter,
        set: setter,
        enumerable: true,
        configurable: true,
      });
    }
  };
}
