const statusClasses = {
  QUEUED: 'queued',
  RUNNING: 'running',
  PAUSED: 'paused',
  SUCCESS: 'success',
  FAILURE: 'failure',
  ABORTED: 'aborted',
  UNKNOWN: 'unknown',
};

const statusTexts = {
  QUEUED: 'jenkins_history_queued',
  RUNNING: 'jenkins_history_running',
  PAUSED: 'jenkins_history_wait',
  SUCCESS: 'jenkins_history_success',
  FAILURE: 'jenkins_history_failure',
  ABORTED: 'jenkins_history_canceled',
  UNKNOWN: 'jenkins_history_unknown',
};

const statusIcons = {
  QUEUED: 'fa-clock-o',
  RUNNING: 'fa-refresh',
  PAUSED: 'fa-pause',
  SUCCESS: 'fa-check',
  FAILURE: 'fa-close',
  ABORTED: 'fa-minus',
  UNKNOWN: 'fa-question',
};

export function historyStatus(history: any) {
  let status = history.status === 'FINISHED' ? history.result : history.status;

  if (
    !['QUEUED', 'RUNNING', 'PAUSED', 'SUCCESS', 'FAILURE', 'ABORTED'].includes(
      status,
    )
  ) {
    status = 'UNKNOWN';
  }

  return {
    class: statusClasses[status],
    text: statusTexts[status],
    icon: statusIcons[status],
  };
}
