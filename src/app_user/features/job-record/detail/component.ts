import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as jsyaml from 'js-yaml';
import { cloneDeep } from 'lodash-es';

import { JobQueryParams } from 'app/features-shared/job/job-record-list/component';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  Job,
  K8sResourceWithActions,
  ObjectMeta,
  WorkspaceBaseParams,
} from 'app/typings';
import * as codeEditorUtil from 'app/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class JobDetailComponent implements OnInit {
  dataInfo: ObjectMeta;
  clusterName: string;
  namespaceName: string;
  name: string;
  yaml: string;
  private RESOURCE_TYPE = 'jobs';
  codeEditorOptions = codeEditorUtil.yamlOptions;
  actionsConfigView = codeEditorUtil.viewActions;
  initialized = false;
  loading = false;
  queryParams: JobQueryParams = {
    activeTabIndex: 0,
    pod: '',
    container: '',
  };
  logInitialState = {
    pod: '',
    container: '',
  };
  result: K8sResourceWithActions<Job>;
  parentPath = 'job_record';
  baseParams: WorkspaceBaseParams;

  constructor(
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.activatedRoute.queryParams.subscribe(queryParams => {
      this.queryParams = queryParams as JobQueryParams;
      this.logInitialState.pod = queryParams.pod;
      this.logInitialState.container = queryParams.container;
    });
    this.name = params['name'];
    const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
    this.baseParams = workspaceParams;
    this.clusterName = workspaceParams['cluster'];
    this.namespaceName = workspaceParams['namespace'];
    this.getDetail();
  }

  getYaml(data: Job) {
    this.yaml = this.formToYaml(data);
  }

  formToYaml(formModel: any) {
    try {
      const json = cloneDeep(formModel);
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  async getDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail(
        this.RESOURCE_TYPE,
        {
          clusterName: this.clusterName,
          name: this.name,
          namespace: this.namespaceName,
        },
      );
      this.result = result;
      this.getYaml(result.kubernetes);
      this.dataInfo = result.kubernetes.metadata || {};
      this.initialized = true;
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate([this.parentPath], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }

  jumpToListPage() {
    this.router.navigate([this.parentPath], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async delete() {
    const result = this.result;
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_job_record_confirm', {
          name: ` "${result.kubernetes.metadata.name}" `,
        }),
        content: this.translate.get('delete_job_record_tip'),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.loading = true;
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        result.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translate.get('success'),
        content: this.translate.get('delete_job_record_success', {
          name: result.kubernetes.metadata.name,
        }),
      });
      this.jumpToListPage();
      this.loading = false;
    } catch (e) {
      this.loading = false;
      this.errorsToastService.error(e);
    }
  }

  onTabIndexChange(index: number) {
    const params: JobQueryParams = {
      activeTabIndex: index,
    };
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: params,
      queryParamsHandling: 'merge',
    });
  }

  openCronJobDetail(name: string) {
    this.router.navigate(['cron_job', 'detail', name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
