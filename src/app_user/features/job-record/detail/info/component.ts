import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { JobQueryParams } from 'app/features-shared/job/job-record-list/component';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import {
  GenericStatus,
  Job,
  K8sResourceWithActions,
  WorkspaceBaseParams,
} from 'app/typings';

@Component({
  selector: 'rc-job-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class JobDetailInfoComponent implements OnInit {
  @Input()
  data: K8sResourceWithActions<Job>;
  @Input()
  baseParams: WorkspaceBaseParams;
  @Output()
  onDelete = new EventEmitter<void>();
  @Output()
  onOpenCronJobDetail = new EventEmitter<string>();
  resource: Job;
  constructor(
    private roleUtil: RoleUtilitiesService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
  ) {}
  ngOnInit() {
    this.resource = this.data.kubernetes;
  }

  canShowDelete(item: K8sResourceWithActions<Job>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'delete');
  }

  getJobStatus() {
    const status = this.data.kubernetes.status;
    if (
      status.conditions &&
      status.conditions.find(_item => _item.type === 'Failed')
    ) {
      return {
        icon: GenericStatus.Error,
        job: 'execute_failed',
      };
    }
    if (status.active) {
      return {
        icon: GenericStatus.Running,
        job: 'executing',
      };
    }
    if (status.succeeded) {
      return {
        icon: GenericStatus.Success,
        job: 'execute_succeeded',
      };
    }
    if (status.failed) {
      return {
        icon: GenericStatus.Error,
        job: 'execute_failed',
      };
    }
    return {
      icon: GenericStatus.Unknown,
      job: 'unknown',
    };
  }

  getOwnerCronJob() {
    const ownerReferences = this.data.kubernetes.metadata.ownerReferences;
    if (ownerReferences) {
      const ownerCronJob = ownerReferences.find(
        reference => reference.kind === 'CronJob',
      );
      return ownerCronJob
        ? { link: true, name: ownerCronJob.name }
        : {
            link: false,
            name: `${ownerReferences[0].kind}:${ownerReferences[0].name}`,
          };
    }
    return { link: false, name: '-' };
  }

  onOpenLogHandler(data: { pod: string; container: string }) {
    const params: JobQueryParams = {
      activeTabIndex: 4,
      ...data,
    };
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: params,
      queryParamsHandling: 'merge',
    });
  }
}
