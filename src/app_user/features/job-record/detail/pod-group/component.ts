import { DialogService } from '@alauda/ui';
import { Component, Input, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { get } from 'lodash-es';

import { JobQueryParams } from 'app/features-shared/job/job-record-list/component';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { Container, GenericStatus, JobSpec, Pod } from 'app/typings';

enum Status {
  Failed = 'Failed',
  Succeeded = 'Succeeded',
  Running = 'Running',
  Pending = 'Pending',
  Unknown = 'Unknown',
}

interface JobPodGroup {
  name: string;
  status: string;
  creationTimestamp: string;
  containers: Container[];
}

@Component({
  selector: 'rc-job-detail-pod-group',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class JobDetailPodGroupComponent implements OnInit {
  @Input()
  jobName: string;
  @Input()
  jobSpec: JobSpec;
  @Input()
  cluster: string;
  @Input()
  namespace: string;
  initialized: boolean;
  podGroup: JobPodGroup[] = [];
  columns = ['name', 'status', 'create_time', 'action'];
  RESOURCE_TYPE = 'pods';

  constructor(
    private k8sResourceService: K8sResourceService,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
    private translate: TranslateService,
  ) {}
  ngOnInit() {
    this.getPodGroup()
      .then(res => {
        this.podGroup = res;
      })
      .finally(() => {
        this.initialized = true;
      });
  }

  getPodStatus(status: Status) {
    switch (status) {
      case Status.Succeeded:
        return GenericStatus.Success;
      case Status.Pending:
        return GenericStatus.Pending;
      case Status.Failed:
        return GenericStatus.Error;
      case Status.Running:
        return GenericStatus.Running;
      default:
        return GenericStatus.Unknown;
    }
  }

  /**
   * matchLabels => labelSelector => Pod group: Pod[]
   */
  private async getPodGroup(): Promise<JobPodGroup[]> {
    const matchLabels = get(this.jobSpec, 'selector.matchLabels', {});
    const labelSelector = Object.keys(matchLabels || {})
      .map(key => `${key}=${matchLabels[key]}`)
      .join(',');
    const result = await this.k8sResourceService.getK8sResources(
      this.RESOURCE_TYPE,
      {
        clusterName: this.cluster,
        namespace: this.namespace,
        labelSelector,
      },
    );
    return result
      ? result.map(i => this.adaptPodGroupViewModel(i.kubernetes))
      : [];
  }

  private adaptPodGroupViewModel(item: Pod): JobPodGroup {
    return {
      name: get(item, 'metadata.name', ''),
      status: get(item, 'status.phase', ''),
      creationTimestamp: get(item, 'metadata.creationTimestamp', ''),
      containers: get(item, 'spec.containers', []),
    };
  }

  viewPodLog(item: JobPodGroup) {
    const params: JobQueryParams = {
      activeTabIndex: 4,
      pod: get(item, 'name', ''),
      container: item.containers[0] ? item.containers[0].name : '',
    };
    this.router.navigate([], {
      relativeTo: this.activatedRoute,
      queryParams: params,
      queryParamsHandling: 'merge',
    });
  }

  async delete(item: JobPodGroup) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete_cron_job_pod_confirm', {
          name: ` "${item.name}" `,
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    const podMetadata = {
      metadata: {
        name: item.name,
        namespace: this.namespace,
      },
    };
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.cluster,
        this.RESOURCE_TYPE,
        podMetadata,
      );
      this.getPodGroup().then(res => {
        this.podGroup = res;
      });
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }
}
