import { NgModule } from '@angular/core';

import { AppSharedModule } from 'app/features-shared/app/shared.module';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { ImageSharedModule } from 'app/features-shared/image/shared.module';
import { JobSharedModule } from 'app/features-shared/job/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { JobDetailComponent } from 'app_user/features/job-record/detail/component';
import { JobDetailEventComponent } from 'app_user/features/job-record/detail/event/component';
import { JobDetailInfoComponent } from 'app_user/features/job-record/detail/info/component';
import { JobDetailPodGroupComponent } from 'app_user/features/job-record/detail/pod-group/component';
import { JobRecordRoutingModule } from 'app_user/features/job-record/job-record.routing.module';
import { JobRecordListComponent } from 'app_user/features/job-record/list/component';

@NgModule({
  imports: [
    SharedModule,
    JobRecordRoutingModule,
    EventSharedModule,
    FormTableModule,
    AppSharedModule,
    ImageSharedModule,
    JobSharedModule,
  ],
  declarations: [
    JobRecordListComponent,
    JobDetailComponent,
    JobDetailEventComponent,
    JobDetailInfoComponent,
    JobDetailPodGroupComponent,
  ],
})
export class JobRecordModule {}
