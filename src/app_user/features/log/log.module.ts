import { NgModule } from '@angular/core';
import { LogSharedModule } from 'app/features-shared/log/shared.module';
import { SharedModule } from 'app/shared/shared.module';
import { LogRoutingModule } from 'app_user/features/log/log.routing.module';

@NgModule({
  imports: [SharedModule, LogSharedModule, LogRoutingModule],
})
export class LogModule {}
