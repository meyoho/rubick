import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';

import { get, isEmpty } from 'lodash-es';

import { AccountService } from 'app/services/api/account.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { WorkspaceBaseParams } from 'app/typings';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: './service-framework.component.html',
  styleUrls: ['./service-framework.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceFrameworkComponent implements OnInit {
  private baseParams: WorkspaceBaseParams;
  inited = false;
  asfEnabled: Boolean;
  host: string;
  token: string;
  installedServiceFramework = false;

  constructor(
    private k8sResourceService: K8sResourceService,
    private errorToast: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    private accountService: AccountService,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    try {
      const [serviceMeshInfo, token] = await Promise.all([
        this.k8sResourceService.getK8sReourceDetail('alaudaproducts', {
          clusterName: this.baseParams.cluster,
          name: 'acp-service-framework',
        }),
        this.accountService.getTokens(true),
      ]);
      this.host = serviceMeshInfo.kubernetes.spec.homepage;
      this.token = token.kubernetes_id_token || '';
      this.installedServiceFramework = true;
      await this.getProjects();
    } catch (e) {
      this.installedServiceFramework = false;
    } finally {
      this.inited = true;
      this.cdr.markForCheck();
    }
  }

  async getProjects() {
    try {
      const serviceMeshProjects = await this.k8sResourceService.getK8sResources(
        'projects',
        {
          clusterName: this.baseParams.cluster,
        },
      );
      this.asfEnabled = !isEmpty(
        (serviceMeshProjects || []).filter(project => {
          return (
            get(project, 'kubernetes.metadata.name') ===
            this.baseParams.namespace
          );
        }),
      );
    } catch (e) {
      this.errorToast.error(e);
    }
  }

  confirmToAsf() {
    window.open(
      `${this.host}?id_token=${this.token}&displayModule=asf#/workspace/${
        this.baseParams.namespace
      }/microservice/services`,
    );
  }
}
