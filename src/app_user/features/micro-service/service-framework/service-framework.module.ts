import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';

import { ServiceFrameworkComponent } from './base/service-framework.component';
import { ServiceFrameworkRoutingModule } from './service-framework.routing.module';

@NgModule({
  imports: [SharedModule, ServiceFrameworkRoutingModule],
  declarations: [ServiceFrameworkComponent],
})
export class ServiceFrameworkModule {}
