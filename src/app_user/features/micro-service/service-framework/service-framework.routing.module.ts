import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ServiceFrameworkComponent } from './base/service-framework.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceFrameworkComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiceFrameworkRoutingModule {}
