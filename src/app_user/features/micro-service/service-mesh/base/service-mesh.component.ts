import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';

import { get } from 'lodash-es';

import { AccountService } from 'app/services/api/account.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { WorkspaceBaseParams } from 'app/typings';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { AsmProject } from '../types';
import { toModel } from '../util.service';

@Component({
  templateUrl: './service-mesh.component.html',
  styleUrls: ['./service-mesh.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServiceMeshComponent implements OnInit {
  private baseParams: WorkspaceBaseParams;
  host: string;
  token: string;
  serviceMeshProject: AsmProject;
  installedServiceMesh = false;
  inited = false;

  constructor(
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private accountService: AccountService,
    private cdr: ChangeDetectorRef,
  ) {}

  ngOnInit() {
    this.baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.initServiceMeshInfo();
  }

  get asmEnabled(): boolean {
    return get(this.serviceMeshProject, 'serviceMesh', '') === 'enabled';
  }

  async initServiceMeshInfo() {
    try {
      const [serviceMeshInfo, token] = await Promise.all([
        this.k8sResourceService.getK8sReourceDetail('alaudaproducts', {
          clusterName: this.baseParams.cluster,
          name: 'acp-service-mesh',
        }),
        this.accountService.getTokens(true),
      ]);
      this.host = serviceMeshInfo.kubernetes.spec.homepage;
      this.token = token.kubernetes_id_token || '';
      this.installedServiceMesh = true;
      await this.getServiceMeshProjects(this.baseParams.cluster);
    } catch (e) {
      this.installedServiceMesh = false;
    } finally {
      this.inited = true;
      this.cdr.markForCheck();
    }
  }

  async getServiceMeshProjects(clusterName: string) {
    const serviceMeshProjects = await this.k8sResourceService.getK8sResources(
      'projects',
      { clusterName: clusterName },
    );
    this.handleServiceMeshProjects(
      serviceMeshProjects.map(item => toModel(item.kubernetes)),
    );
  }

  handleServiceMeshProjects(serviceMeshProjects: AsmProject[]) {
    serviceMeshProjects.forEach(project => {
      if (project.name === this.baseParams.namespace) {
        this.serviceMeshProject = project;
        this.cdr.markForCheck();
      }
    });
  }

  confirmToAsm() {
    window.open(
      `${this.host}?id_token=${this.token}&displayModule=asm#/workspace/${
        this.baseParams.namespace
      }/servicemesh/jaeger`,
    );
  }
}
