import { NgModule } from '@angular/core';
import { SharedModule } from 'app/shared/shared.module';

import { ServiceMeshComponent } from './base/service-mesh.component';
import { ServiceMeshRoutingModule } from './service-mesh.routing.module';

@NgModule({
  imports: [SharedModule, ServiceMeshRoutingModule],
  declarations: [ServiceMeshComponent],
  providers: [],
})
export class ServiceMeshModule {}
