import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ServiceMeshComponent } from './base/service-mesh.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceMeshComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiceMeshRoutingModule {}
