export const ANNOTATION_PREFIX = 'alauda.io';
export const AUTH_API_GROUP = 'auth.alauda.io';
export const AUTH_API_VERSION = 'v1alpha1';
export const PRODUCT_NAME = 'Alauda DevOps';
export const AUTH_API_GROUP_VERSION = `${AUTH_API_GROUP}/${AUTH_API_VERSION}`;
export const ANNOTATION_DISPLAY_NAME = `${ANNOTATION_PREFIX}/displayName`;
export const ANNOTATION_DESCRIPTION = `${ANNOTATION_PREFIX}/description`;
export const ANNOTATION_SERVICE_MESH = `${ANNOTATION_PREFIX}/serviceMesh`;
export const ANNOTATION_PRODUCT = `${ANNOTATION_PREFIX}/product`;

export interface ServiceMeshInfo {
  kubernetes: {
    metadata: {
      name: string;
    };
    spec: {
      homepage: string;
    };
  };
}

export interface AsmProject {
  name: string;
  displayName: string;
  description: string;
  projectManagers?: string[];
  creationTimestamp?: string;
  serviceMesh?: string;
  __orignal?: ProjectData;
}

export interface ProjectMetaData {
  name: string;
  annotations: {
    [name: string]: string;
  };
  creationTimestamp?: string;
  resourceVersion?: string;
  selfLink?: string;
  uid?: string;
}

export interface ProjectData {
  apiVersion: string;
  kind: string;
  metadata?: ProjectMetaData;
  spec?: {
    namespaces: [
      {
        name: string;
        type: string;
      }
    ];
  };
  status?: {
    namespaces: Array<{
      name: string;
      status: string;
    }>;
  };
}

export interface ProjectResponse extends ProjectData {
  typeMeta: {
    kind: 'Project';
    groupVersion?: string;
  };
  objectMeta?: ProjectMetaData;
  data?: ProjectData;
  project_managers?: string[];
}
