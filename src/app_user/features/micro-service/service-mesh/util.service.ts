import {
  ANNOTATION_DESCRIPTION,
  ANNOTATION_DISPLAY_NAME,
  ANNOTATION_PRODUCT,
  ANNOTATION_SERVICE_MESH,
  AUTH_API_GROUP_VERSION,
  AsmProject,
  PRODUCT_NAME,
  ProjectData,
  ProjectMetaData,
} from './types';

export function toResource({ __orignal, ...model }: AsmProject): ProjectData {
  const { metadata, ...orignal } = __orignal || ({} as ProjectData);
  const annotations = (metadata && metadata.annotations) || {};

  return {
    apiVersion: AUTH_API_GROUP_VERSION,
    kind: 'Project',
    ...orignal,
    metadata: {
      ...(metadata || {}),
      name: model.name,
      annotations: {
        ...annotations,
        [ANNOTATION_DISPLAY_NAME]: model.displayName,
        [ANNOTATION_DESCRIPTION]: model.description,
        [ANNOTATION_SERVICE_MESH]: model.serviceMesh,
        [ANNOTATION_PRODUCT]: PRODUCT_NAME,
      },
    },
  };
}

export function toModel(resource: any): AsmProject {
  if (!resource) {
    return {} as AsmProject;
  }

  const meta =
    resource.objectMeta || resource.metadata || ({} as ProjectMetaData);
  const annotations = (meta.annotations || {}) as { [key: string]: string };

  return {
    name: meta.name,
    creationTimestamp: meta.creationTimestamp,
    projectManagers: resource.project_managers,
    displayName: annotations[ANNOTATION_DISPLAY_NAME],
    description: annotations[ANNOTATION_DESCRIPTION],
    serviceMesh: annotations[ANNOTATION_SERVICE_MESH],
    __orignal: resource,
  };
}
