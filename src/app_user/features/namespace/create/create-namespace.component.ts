import { MessageService, NotificationService } from '@alauda/ui';
import { Component, OnInit, ViewChild } from '@angular/core';
import { Store } from '@ngrx/store';
import {
  ConfigurationApi,
  EnvironmentService,
} from 'app/services/api/environment.service';
import { CreateNamespaceRes } from 'app/services/api/namespace.service';
import {
  ClusterInProject,
  NewCluster,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { RegionService } from 'app/services/api/region.service';
import * as fromStore from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { BatchReponse } from 'app_user/core/types';
import { ClustersQuota } from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';
import { find, get, isEmpty } from 'lodash-es';
import { filter, take } from 'rxjs/operators';

import { FormResource, NamespaceStatusEnum } from '../types';
import { NamespaceUtilService } from '../util.service';
import { isBatchRes, reduceBatchRes, resolveNamespace } from '../util/create';

import { CreateNamespaceLimitRangeComponent } from './limitrange-form/component';
import { CreateNamespacePrimaryComponent } from './namespace-form/component';
import { CreateNamespaceResourceQuotaComponent } from './quota-form/component';

enum CurrentStepEnum {
  NAMESPACE = 'namespace',
  RESOURCEQUOTA = 'resourcequota',
  LIMITRANGE = 'limitrange',
  ERROR = 'error',
}

type CurrentFormComponent =
  | CreateNamespacePrimaryComponent
  | CreateNamespaceResourceQuotaComponent
  | CreateNamespaceLimitRangeComponent;

@Component({
  templateUrl: './create-namespace.component.html',
  styleUrls: ['./create-namespace.component.scss'],
})
export class CreateNamespaceComponent implements OnInit {
  constructor(
    private regionService: RegionService,
    private projectService: ProjectService,
    private translate: TranslateService,
    private store: Store<fromStore.AppState>,
    private environmentService: EnvironmentService,
    private namespaceUtilService: NamespaceUtilService,
    private notificationService: NotificationService,
    private messageService: MessageService,
  ) {}

  @ViewChild('currentForm')
  currentForm: CurrentFormComponent;
  project: Project;
  isContinue = false;
  actions: string[];
  currentAction: string;
  initialized: boolean;
  StepEnum = CurrentStepEnum;
  errorCode: string;
  errorMessage: string;
  index = 0; // for testing
  clusters: NewCluster[];
  clustersQuota: ClustersQuota[];
  projectClustersQuotas: NewCluster[];
  clusterQuotaKeys: string[] = [];
  globalQuotaConfigs: ConfigurationApi[] = [];
  namePrefix = '';
  submitting: boolean;

  data: {
    cluster: NewCluster;
    namespaceName: string;
    resources: {
      cluster: string;
      resource: {
        key: string;
        value: any;
      };
    }[];
  } = {
    cluster: null,
    namespaceName: '',
    resources: [],
  };

  get currentStep(): CurrentStepEnum {
    return this.currentAction.startsWith('a_')
      ? CurrentStepEnum.ERROR
      : (this.currentAction as CurrentStepEnum);
  }

  get isLastStep(): boolean {
    return (
      this.currentAction === this.actions.filter(i => !i.startsWith('a_')).pop()
    );
  }

  async ngOnInit(): Promise<void> {
    const [project, regions, quotaConfigs] = await Promise.all([
      this.store
        .select(fromStore.getCurrentProjectDetail)
        .pipe(
          filter(project => !!project),
          take(1),
        )
        .toPromise(),
      this.regionService.getRegions(),
      this.environmentService.getConfigurations('quota'),
    ]);
    const {
      clusters: projectClustersQuotas,
    } = await this.projectService.getProjectClustersQuota(project.name);
    this.projectClustersQuotas = projectClustersQuotas;
    this.clusterQuotaKeys = quotaConfigs.map(v => v.name);
    this.globalQuotaConfigs = quotaConfigs;
    this.project = project;
    this.namePrefix = project.name && `${project.name.replace(/[\._]/g, '-')}-`;
    this.clusters = [];
    if (project.clusters) {
      project.clusters
        .slice()
        .forEach((cluster: ClusterInProject & NewCluster) => {
          const clusterDetail = find(regions, { name: cluster.name });
          if (
            clusterDetail &&
            !isEmpty(clusterDetail.mirror) &&
            !find(this.clusters, { name: clusterDetail.mirror.name })
          ) {
            this.clusters.push({
              name: clusterDetail.mirror.name,
              uuid: clusterDetail.id,
              origin_display_name: clusterDetail.mirror.display_name,
              display_name: `${clusterDetail.mirror.display_name}`,
              flag: clusterDetail.mirror.flag,
              regions: clusterDetail.mirror.regions,
              quotaDisplay: this.getClusterQuotaDisplay(cluster),
            });
          }
          if (
            clusterDetail &&
            isEmpty(clusterDetail.mirror) &&
            !find(this.clusters, { name: cluster.name })
          ) {
            this.clusters.push({
              ...cluster,
              quota: get(
                find(projectClustersQuotas, { name: cluster.name }),
                'quota',
              ),
              quotaDisplay: this.getClusterQuotaDisplay(cluster),
            });
          }
        });
    }
    if (!this.isContinue) {
      this.currentAction = CurrentStepEnum.NAMESPACE;
      this.actions = await this.namespaceUtilService.getNamespaceCreateActions();
      this.initialized = true;
    } else {
      // TODO 完善
    }
  }

  private getClusterQuotaDisplay(cluster: NewCluster) {
    if (cluster.regions) {
      return `${this.translate.get('containe')}${cluster.regions
        .map(v => v.display_name)
        .join('、')}`;
    } else if (cluster.quota) {
      return this.globalQuotaConfigs.reduce((total, curr, index) => {
        let extra = {};
        try {
          extra = JSON.parse(curr.extra);
        } catch (e) {}
        const max = this.getMaxMum(curr.name, cluster);
        return `${index === 0 ? `${total} ` : `${total}, `}${this.translate.get(
          curr.name,
        )} ${
          max === -1 ? this.translate.get('unlimited') : max
        }${this.translate.get(extra['unit'])}`;
      }, this.translate.get('can_assign'));
    }
    return '';
  }

  private getMaxMum(type: string, cluster?: ClustersQuota) {
    const max = get(cluster, `quota[${type}].max`, 0);
    return max === -1 ? -1 : max - get(cluster, `quota[${type}].used`, 0);
  }

  // form emit handling
  onFormSubmit(data: FormResource) {
    if (data.cluster) {
      this.updateClustersQuota(data.cluster);
    }
    this.data = {
      ...this.data,
      cluster: data.cluster || this.data.cluster,
      namespaceName: data.namespaceName || this.data.namespaceName,
      resources: data.resources.map(r => {
        return {
          cluster: r.cluster,
          resource: {
            key: this.currentAction,
            value: r.data,
          },
        };
      }),
    };
    this.request();
  }

  private updateClustersQuota(cluster: NewCluster) {
    const isMirror = !!cluster.regions;
    const clusterIDArr = isMirror
      ? cluster.regions.map(c => c.id)
      : [cluster.uuid || cluster.name];
    const filterClusters = this.projectClustersQuotas.filter(cluster =>
      clusterIDArr.includes(cluster.uuid),
    );
    this.clustersQuota = isMirror
      ? [
          {
            name: cluster.name,
            origin_display_name: cluster.origin_display_name,
            display_name: cluster.display_name,
            regions: filterClusters,
          },
        ]
      : filterClusters;
  }

  // req & res handling
  request() {
    this.submitting = true;
    this.namespaceUtilService
      .createNamespace({
        project: this.project.name,
        cluster: this.data.cluster.name,
        namespace: this.data.namespaceName,
        resources: this.data.resources,
      })
      .then(res => {
        this.submitting = false;
        this.resHandler(res);
      })
      .catch(_e => {
        this.submitting = false;
      });
  }

  private resHandler(res: BatchReponse | CreateNamespaceRes) {
    if (!isBatchRes(res)) {
      const namespace = resolveNamespace((<CreateNamespaceRes>res).namespace);
      if (namespace.status === NamespaceStatusEnum.UNKNOWN) {
        this.notificationService.error(this.translate.get('unknown_issue'));
        setTimeout(() => {
          this.namespaceUtilService.jumpToProjectOverview(this.project.name);
        }, 600);
      }
      if (namespace.status === NamespaceStatusEnum.READY) {
        this.notificationService.success(
          this.translate.get('create_namespace_success'),
        );
        setTimeout(() => {
          this.namespaceUtilService.jumpToProjectOverview(this.project.name);
        }, 600);
      }
      if (namespace.status === NamespaceStatusEnum.PENDING) {
        this.errorCode = namespace.reason;
        this.errorMessage = namespace.message;
        if (!this.currentAction) {
          this.currentAction = namespace.action;
          return;
        }
        if (this.currentAction !== namespace.action) {
          this.messageService.success(this.translate.get('create_success'));
          this.currentAction = namespace.action;
          return;
        }
        this.notificationService.error({
          title: this.translate.get(this.errorCode),
          content: this.errorMessage,
        });
      }
      return;
    }

    // TODO batch res handling
    // ...

    const batchRes = reduceBatchRes(res);

    if (!batchRes.success.length) {
      batchRes.failure.forEach(res => {
        this.notificationService.error({
          title: this.translate.get(JSON.parse(res.data.body).errors[0].code),
          content:
            this.translate.get('cluster_namespace_create_error', {
              clusterDisplayName: res.clusterName,
            }) +
            this.translate.get(JSON.parse(res.data.body).errors[0].message),
        });
        /** test data */
        // this.notificationService.error({
        //   title: this.translate.get('namespace_already_exist'),
        //   content: this.translate.get('namespace_already_exist'),
        // });
      });
      return;
    }
  }

  // page footer actions
  create() {
    this.currentForm.onSubmit();
  }

  complete() {
    this.currentForm.onSubmit();
  }

  retry() {
    this.request();
  }

  cancel() {
    this.namespaceUtilService.jumpToProjectOverview(this.project.name);
  }
  back() {
    this.namespaceUtilService.jumpToProjectOverview(this.project.name);
  }
}
