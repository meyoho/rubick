import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NewCluster } from 'app/services/api/project.service';
import { OverCommit, RegionService } from 'app/services/api/region.service';
import { LimitRange } from 'app/typings';
import {
  ClustersQuota,
  limitsRangeKeys,
} from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';
import { isEqual, mapValues } from 'lodash-es';

import { FormResource } from '../../types';

interface LimitRangeItem extends ClustersQuota {
  limitrange?: {
    pod_default_cpu?: string;
    pod_default_request_cpu?: string;
    pod_default_memory?: string;
    pod_default_request_memory?: string;
    pod_max_cpu?: string;
    pod_max_memory?: string;
  };
}
export interface LimitRangeFormModel {
  useSameConfig: boolean;
  canUseSameConfig: boolean;
  limitranges: LimitRangeItem[];
}

@Component({
  selector: 'rc-namespace-limitrange-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateNamespaceLimitRangeComponent implements OnInit, OnChanges {
  constructor(
    private regionService: RegionService,
    private cdr: ChangeDetectorRef,
  ) {}

  @Input()
  cluster: NewCluster;
  @Input()
  namespace: string;
  @Input()
  clusterQuotaKeys: string[] = []; // delete
  @Input()
  clustersQuota: ClustersQuota[];
  @Output()
  submit = new EventEmitter<any>();
  @ViewChild(NgForm)
  form: NgForm;

  get isMirror() {
    return this.cluster.regions;
  }

  formModel: LimitRangeFormModel = {
    useSameConfig: false,
    canUseSameConfig: false,
    limitranges: [],
  };
  folding = {};

  formFileds = [
    {
      label: 'CPU',
      items: [
        {
          name: 'pod_default_cpu',
          label: 'namesapce_quota_default',
          after: 'm',
        },
        {
          name: 'pod_default_request_cpu',
          label: 'namesapce_quota_default_request',
          after: 'm',
        },
        {
          name: 'pod_max_cpu',
          label: 'maximum',
          after: 'unit_core',
        },
      ],
    },
    {
      label: 'memory',
      items: [
        {
          name: 'pod_default_memory',
          label: 'namesapce_quota_default',
          after: 'unit_mb',
        },
        {
          name: 'pod_default_request_memory',
          label: 'namesapce_quota_default_request',
          after: 'unit_mb',
        },
        {
          name: 'pod_max_memory',
          label: 'maximum',
          after: 'unit_gb',
        },
      ],
    },
  ];

  resourceRadio: {
    [key: string]: OverCommit;
  } = {};
  initialized: boolean;

  async ngOnChanges(): Promise<void> {
    if (this.clustersQuota && this.cluster) {
      await this.fetchResourceRadio();
      this.setModel();
      this.initialized = true;
      this.cdr.markForCheck();
    }
  }

  ngOnInit(): void {}

  private async fetchResourceRadio(): Promise<void> {
    const cluster = this.cluster;
    if (!cluster.regions) {
      const res = await this.regionService
        .getOverCommit(cluster.name)
        .catch(() => ({
          cpu: 0,
          mem: 0,
        }));
      this.resourceRadio[cluster.name] = res;
    } else {
      let same = true;
      let expectation: OverCommit = null;
      const allResourceRadios: OverCommit[] = await Promise.all(
        cluster.regions.map(async r => {
          const res = await this.regionService
            .getOverCommit(r.name)
            .catch(() => ({
              cpu: 0,
              mem: 0,
            }));
          this.resourceRadio[r.name] = res;
          return res;
        }),
      );
      allResourceRadios.some((rr: OverCommit, i: Number) => {
        if (i === 0) {
          expectation = rr;
          return false;
        }
        if (!isEqual(rr, expectation)) {
          same = false;
          return true;
        }
      });
      this.formModel.canUseSameConfig = same;
      this.formModel.useSameConfig = same;
    }
  }

  setModel() {
    let data: ClustersQuota[];
    if (this.formModel.useSameConfig && this.isMirror) {
      data = [
        Object.assign({}, this.clustersQuota[0], {
          quota: this.getQuota(this.clustersQuota[0].regions),
        }),
      ];
    } else if (this.isMirror) {
      data = this.clustersQuota[0].regions;
    } else {
      data = this.clustersQuota;
    }

    this.formModel.limitranges = data.map(clustersQuota => {
      let limitrange = {};
      limitrange = limitsRangeKeys.reduce((acc, key) => {
        if (['pod_max_cpu', 'pod_max_memory'].includes(key)) {
          return {
            ...acc,
            [key]: clustersQuota.quota[key] || this.getMiniNum(key),
          };
        } else {
          return {
            ...acc,
            [key]: clustersQuota.quota[key] * 1000 || this.getMiniNum(key),
          };
        }
      }, {});
      return {
        ...clustersQuota,
        limitrange: mapValues(limitrange, v => String(v)),
      };
    });
  }

  private getQuota(clusters: ClustersQuota[]) {
    return limitsRangeKeys.reduce((acc, key) => {
      return {
        ...acc,
        [key]: Math.max(...clusters.map(c => c.quota[key])),
      };
    }, {});
  }

  private getMiniNum(type: string, _clusterId?: string) {
    let defaultMinNumb: Number;
    const rules: { rule: string[]; value: Number }[] = [
      {
        rule: ['pod_default_cpu', 'pod_default_request_cpu'],
        value: 500,
      },
      {
        rule: ['pod_default_memory', 'pod_default_request_memory'],
        value: 512,
      },
      {
        rule: ['pod_max_cpu', 'pod_max_memory'],
        value: 4,
      },
    ];

    const matched = rules.some(o => {
      if (o.rule.includes(type)) {
        defaultMinNumb = o.value;
        return true;
      }
    });
    if (!matched) {
      defaultMinNumb = 0;
    }

    return defaultMinNumb;
  }

  showFormItem(
    item: { name: string; label: string; after: string },
    model: LimitRangeItem,
  ): boolean {
    if (this.isMirror && this.formModel.useSameConfig) {
      if (item.name === 'pod_default_request_cpu') {
        return this.resourceRadio[Object.keys(this.resourceRadio)[0]].cpu === 0;
      } else if (item.name === 'pod_default_request_memory') {
        return this.resourceRadio[Object.keys(this.resourceRadio)[0]].mem === 0;
      } else {
        return true;
      }
    } else {
      if (item.name === 'pod_default_request_cpu') {
        return this.resourceRadio[model.name].cpu === 0;
      } else if (item.name === 'pod_default_request_memory') {
        return this.resourceRadio[model.name].mem === 0;
      } else {
        return true;
      }
    }
  }

  onSubmit() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      this.submit.emit(this.adaptForm(this.formModel));
    }
  }

  private adaptForm(formModel: LimitRangeFormModel) {
    let resources: FormResource['resources'];
    if (this.isMirror && formModel.useSameConfig) {
      const model = formModel.limitranges[0];
      resources = this.cluster.regions.map(
        r =>
          ({
            cluster: r.name,
            data: {
              apiVersion: 'v1',
              kind: 'LimitRange',
              metadata: {
                name: 'default',
                namespace: this.namespace,
              },
              spec: {
                limits: [
                  {
                    default: {
                      memory: model.limitrange.pod_default_memory + 'M',
                      cpu: model.limitrange.pod_default_cpu + 'm',
                    },
                    defaultRequest: {
                      memory: model.limitrange.pod_default_request_memory + 'M',
                      cpu: model.limitrange.pod_default_request_cpu + 'm',
                    },
                    max: {
                      memory: model.limitrange.pod_max_memory + 'G',
                      cpu: model.limitrange.pod_max_cpu,
                    },
                    type: 'Container',
                  },
                ],
              },
            },
          } as {
            cluster: string;
            data: LimitRange;
          }),
      );
    } else {
      resources = formModel.limitranges.map(
        limitrange =>
          ({
            cluster: limitrange.name,
            data: {
              apiVersion: 'v1',
              kind: 'LimitRange',
              metadata: {
                name: 'default',
                namespace: this.namespace,
              },
              spec: {
                limits: [
                  {
                    default: {
                      memory: limitrange.limitrange.pod_default_memory + 'M',
                      cpu: limitrange.limitrange.pod_default_cpu + 'm',
                    },
                    defaultRequest: {
                      memory:
                        limitrange.limitrange.pod_default_request_memory + 'M',
                      cpu: limitrange.limitrange.pod_default_request_cpu + 'm',
                    },
                    max: {
                      memory: limitrange.limitrange.pod_max_memory + 'G',
                      cpu: limitrange.limitrange.pod_max_cpu,
                    },
                    type: 'Container',
                  },
                ],
              },
            },
          } as {
            cluster: string;
            data: LimitRange;
          }),
      );
    }

    return { resources };
  }
}
