import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { NewCluster } from 'app/services/api/project.service';
import { StringMap } from 'app/typings/raw-k8s';

import { FormResource } from '../../types';

interface ConfigItem {
  clusterDisplayName: string;
  clusterName: string;
  labels: StringMap;
  annotations: StringMap;
}

export interface NamespaceFormModel {
  name: string;
  cluster: NewCluster;
  useSameConfig: boolean;
  config: ConfigItem[];
}

@Component({
  selector: 'rc-namespace-primary-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateNamespacePrimaryComponent implements OnInit {
  constructor(private cdr: ChangeDetectorRef) {}

  @Input()
  namePrefix: string;
  @Input()
  clusters: NewCluster[];
  @Output()
  submit = new EventEmitter<any>();

  filteredClusters: NewCluster[] = [];

  @ViewChild(NgForm)
  form: NgForm;
  formModel: NamespaceFormModel = {
    name: '',
    cluster: null,
    useSameConfig: true,
    config: [],
  };
  folding = {};

  ngOnInit(): void {
    // 2.3 暂不支持联邦集群
    this.filteredClusters = this.clusters.filter(c => !c.regions);
  }

  setConfig() {
    this.folding = {};
    const cluster = this.formModel.cluster;
    if (cluster.regions) {
      this.formModel.config = [].concat(
        this.formModel.useSameConfig
          ? [this.getConfigItem(cluster)]
          : cluster.regions.map(c => this.getConfigItem(c)),
      );
    } else {
      this.formModel.config = [].concat(this.getConfigItem(cluster));
    }
    this.cdr.markForCheck();
  }

  private getConfigItem(cluster: NewCluster): ConfigItem {
    return {
      clusterDisplayName: cluster.display_name,
      clusterName: cluster.name,
      labels: {},
      annotations: {},
    };
  }

  onSubmit() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      this.submit.emit(this.adpatForm(this.formModel));
    }
  }

  private adpatForm(formModel: NamespaceFormModel): FormResource {
    return {
      cluster: formModel.cluster,
      namespaceName: this.namePrefix + formModel.name,
      resources: this.getResources(formModel.config),
    };
  }

  private getResources(config: NamespaceFormModel['config']) {
    const cluster = this.formModel.cluster;
    if (cluster.regions && this.formModel.useSameConfig) {
      return cluster.regions.map(r => ({
        cluster: r.name,
        data: {
          apiVersion: 'v1',
          kind: 'Namespace',
          metadata: {
            name: this.namePrefix + this.formModel.name,
            labels: config[0].labels,
            annotations: config[0].annotations,
          },
        },
      }));
    } else {
      return config.map(c => ({
        cluster: c.clusterName,
        data: {
          apiVersion: 'v1',
          kind: 'Namespace',
          metadata: {
            name: this.namePrefix + this.formModel.name,
            labels: c.labels,
            annotations: c.annotations,
          },
        },
      }));
    }
  }
}
