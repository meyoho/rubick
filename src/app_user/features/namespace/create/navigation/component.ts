import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnInit,
} from '@angular/core';
import { cloneDeep } from 'lodash-es';

enum NavType {
  Normal = 'normal',
  INTERMEDIATE = 'intermediate',
  UNKNOWN = 'unknown',
}

interface NavItem {
  type: NavType;
  name?: string;
  actions: string[];
  active: boolean;
}

@Component({
  selector: 'rc-create-namespace-navigation',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateNamespaceNavigationComponent implements OnInit, OnChanges {
  constructor() {}

  @Input()
  actions: string[];
  @Input()
  currentAction: string;

  nav: NavItem[];
  nameMap = {
    namespace: 'create_namespace',
    resourcequota: 'namespace_set_resourcequota',
    limitrange: 'namespace_set_limitrange',
  };

  ngOnChanges() {
    if (this.actions && this.currentAction) {
      this.initNav();
    }
  }

  private initNav() {
    // the first step is always `namespace`
    this.nav = this.actions.reduce((acc: NavItem[], cur, idx, src) => {
      if (!cur.startsWith('a_')) {
        return acc.concat([
          {
            type: NavType.Normal,
            name: this.nameMap[cur],
            actions: [].concat(cur),
            active: cur === this.currentAction,
          },
        ]);
      } else {
        if (src[idx - 1].startsWith('a_')) {
          const len = acc.length;
          const lastNavItem = cloneDeep(acc[len - 1]);
          return acc.slice(0, len - 1).concat([
            {
              ...lastNavItem,
              actions: lastNavItem.actions.concat(cur),
              active: lastNavItem.active ? true : cur === this.currentAction,
            },
          ]);
        } else {
          return acc.concat([
            {
              type: NavType.INTERMEDIATE,
              actions: [].concat(cur),
              active: cur === this.currentAction,
            },
          ]);
        }
      }
    }, []);
  }

  ngOnInit(): void {}
}
