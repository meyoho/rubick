import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { EnvironmentService } from 'app/services/api/environment.service';
import { NewCluster } from 'app/services/api/project.service';
import { TranslateService } from 'app/translate/translate.service';
import { ResourceQuota } from 'app/typings';
import {
  ClustersQuota,
  MixinQuota,
} from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';
import { QuotaConfigEnum, QuotaRange } from 'app_user/features/project/types';
import { get, isEmpty, mapKeys, mapValues, pick } from 'lodash-es';

import { FormResource } from '../../types';

export const QUOTA_TYPES = ['cpu', 'memory', 'storage', 'pvc_num', 'pods'];

interface QuotaItem extends ClustersQuota {
  hard: {
    cpu?: string;
    memory?: string;
    storage?: string;
    pvc_num?: string;
    pods?: string;
  };
}
interface ResourceQuotaFormModel {
  useSameConfig: boolean;
  quotas: QuotaItem[];
}

@Component({
  selector: 'rc-namespace-quota-form',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateNamespaceResourceQuotaComponent
  implements OnInit, OnChanges {
  constructor(
    private environmentService: EnvironmentService,
    private translate: TranslateService,
  ) {}

  @Input()
  cluster: NewCluster;
  @Input()
  namespace: string;
  @Input()
  clusterQuotaKeys: string[] = [];
  @Input()
  clustersQuota: ClustersQuota[];
  @Input()
  minQuotaRanges: { [clusterName: string]: MixinQuota } = {};
  @Input()
  hardQuotaRanges: { [clusterName: string]: MixinQuota } = {};
  @Output()
  submit = new EventEmitter<any>();
  @ViewChild(NgForm)
  form: NgForm;

  formModel: ResourceQuotaFormModel = {
    useSameConfig: true,
    quotas: [],
  };
  folding = {};

  get isMirror() {
    return this.cluster.regions;
  }

  ngOnChanges({ clustersQuota }: SimpleChanges): void {
    if (clustersQuota && clustersQuota.currentValue) {
      this.setQuotas();
    }
  }

  ngOnInit(): void {}

  onSubmit() {
    this.form.onSubmit(null);
    if (this.form.valid) {
      this.submit.emit(this.adaptForm(this.formModel));
    }
  }

  private adaptForm(formModel: ResourceQuotaFormModel): FormResource {
    const MOCK_QUOTA_KEYS = ['cpu', 'memory', 'storage', 'pods', 'pvc_num'];
    let resources: FormResource['resources'];
    if (this.isMirror && this.formModel.useSameConfig) {
      const quota = this.formModel.quotas[0];
      if (!this.clusterQuotaKeys.includes('pods')) {
        quota.hard.pods = '1000';
      }
      if (!this.clusterQuotaKeys.includes('pvc_num')) {
        quota.hard.pvc_num = '1000';
      }
      resources = this.cluster.regions.map(
        r =>
          ({
            cluster: r.name,
            data: {
              apiVersion: 'v1',
              kind: 'ResourceQuota',
              metadata: {
                name: 'default',
                namespace: this.namespace,
              },
              spec: {
                hard: {
                  ...mapKeys(
                    pick(
                      {
                        ...quota.hard,
                        memory: quota.hard.memory + 'G',
                        storage: quota.hard.storage + 'G',
                      },
                      MOCK_QUOTA_KEYS,
                    ),
                    (_v, k) => QuotaConfigEnum[k],
                  ),
                  'limits.cpu': quota.hard.cpu,
                  'limits.memory': quota.hard.memory + 'G',
                },
              },
            },
          } as {
            cluster: string;
            data: ResourceQuota;
          }),
      );
    } else {
      resources = formModel.quotas.map(quota => {
        if (!this.clusterQuotaKeys.includes('pods')) {
          quota.hard.pods = '1000';
        }
        if (!this.clusterQuotaKeys.includes('pvc_num')) {
          quota.hard.pvc_num = '1000';
        }
        return {
          cluster: quota.name,
          data: {
            apiVersion: 'v1',
            kind: 'ResourceQuota',
            metadata: {
              name: 'default',
              namespace: this.namespace,
            },
            spec: {
              hard: {
                ...mapKeys(
                  pick(
                    {
                      ...quota.hard,
                      memory: quota.hard.memory + 'G',
                      storage: quota.hard.storage + 'G',
                    },
                    MOCK_QUOTA_KEYS,
                  ),
                  (_v, k) => QuotaConfigEnum[k],
                ),
                'limits.cpu': quota.hard.cpu,
                'limits.memory': quota.hard.memory + 'G',
              },
            },
          },
        } as {
          cluster: string;
          data: ResourceQuota;
        };
      });
    }
    return { resources };
  }

  setQuotas() {
    let data: ClustersQuota[];
    if (this.formModel.useSameConfig && this.isMirror) {
      data = [
        Object.assign({}, this.clustersQuota[0], {
          quota: this.calculateQuotaRange(this.clustersQuota[0].regions),
        }),
      ];
    } else if (this.isMirror) {
      data = this.clustersQuota[0].regions;
    } else {
      data = this.clustersQuota;
    }

    const defaultQuota = this.environmentService.getDefaultQuota('namespace');

    this.formModel.quotas = data.map(clustersQuota => {
      const hard = {};
      if (!isEmpty(this.hardQuotaRanges)) {
        QUOTA_TYPES.forEach(type => {
          if (this.formModel.useSameConfig && this.isMirror) {
            hard[type] = Math.max(
              ...Object.values(this.hardQuotaRanges).map(v => v[type]),
            );
          } else {
            hard[type] = get(
              this.hardQuotaRanges[clustersQuota.name],
              type,
              this.getMiniNum(type, clustersQuota.name),
            );
          }
        });
      } else if (!!defaultQuota) {
        QUOTA_TYPES.forEach(type => {
          hard[type] = clustersQuota.quota
            ? Math.min(
                defaultQuota[type] || this.getMiniNum(type, clustersQuota.name),
                clustersQuota.quota[type]['max'] -
                  clustersQuota.quota[type]['used'],
              )
            : defaultQuota[type] || this.getMiniNum(type, clustersQuota.name);
        });
      } else {
        QUOTA_TYPES.forEach(type => {
          hard[type] = this.getMiniNum(type, clustersQuota.name);
        });
      }
      return {
        ...clustersQuota,
        hard: mapValues(hard, v => String(v)),
      };
    });
  }

  private calculateQuotaRange(clusters: ClustersQuota[]) {
    let quotaRange = {},
      quotaLimited = false;

    clusters.some(cluster => {
      return (quotaLimited = !!cluster.quota);
    });
    if (!quotaLimited) {
      return (quotaRange = null);
    }

    this.clusterQuotaKeys.forEach((v: string) => {
      const maxNumArr = clusters.map(cluster => {
        if (cluster.quota && cluster.quota[v]) {
          return cluster.quota[v]['max'];
        }
        return Number.MAX_SAFE_INTEGER;
      });
      const minNumArr = clusters.map(cluster => {
        if (cluster.quota && cluster.quota[v]) {
          return cluster.quota[v]['used'];
        }
        return 0;
      });
      quotaRange[v] = {
        used: Math.max(...minNumArr),
        max: Math.min(...maxNumArr),
      };
    });
    return quotaRange;
  }

  private getMiniNum(type: string, clusterId?: string) {
    let defaultMinNum: Number;

    if (!['cpu', 'memory', 'pods'].includes(type)) {
      defaultMinNum = 0;
    } else {
      defaultMinNum = 1;
    }

    if (
      this.formModel.useSameConfig &&
      this.isMirror &&
      !isEmpty(this.minQuotaRanges)
    ) {
      return Math.max(
        ...Object.values(this.minQuotaRanges).map(
          v => v[type] || defaultMinNum,
        ),
      );
    }
    if (!clusterId || !this.minQuotaRanges[clusterId]) {
      return defaultMinNum;
    }

    return Math.max(get(this.minQuotaRanges[clusterId], type) || defaultMinNum);
  }

  getMaxMum(type: string, cluster?: ClustersQuota) {
    const max = get(cluster, `quota[${type}].max`, 0);
    return max === -1 ? null : max - cluster.quota[type].used;
  }

  getQuotaFormItemHint(quota: QuotaRange, type: string, clusterName: string) {
    if (!quota[type]) {
      return '';
    }
    const remaining = quota[type].max - quota[type].used;
    const limitMin = this.getMiniNum(type, clusterName);

    return quota[type].max === -1
      ? this.translate.get('quota_range_tip_no_max', {
          min: limitMin,
        })
      : this.translate.get('quota_range_tip', {
          min: limitMin,
          max: remaining,
        });
  }

  getQuotaErrorInfo(quota: QuotaRange, type: string, clusterName: string) {
    if (!quota[type]) {
      return '';
    }
    const remaining = quota[type].max - quota[type].used;
    const limitMin = this.getMiniNum(type, clusterName);
    return quota[type].max === -1
      ? this.translate.get('quota_range_tip_no_max', {
          min: limitMin,
        })
      : this.translate.get(
          remaining < limitMin ? 'quota_range_error_tip' : 'quota_range_tip',
          {
            min: limitMin,
            max: remaining,
          },
        );
  }
}
