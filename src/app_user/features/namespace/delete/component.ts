import { DIALOG_DATA, DialogRef, DialogService } from '@alauda/ui';
import { MessageService, NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { EnvironmentService } from 'app/services/api/environment.service';
import { NamespaceService } from 'app/services/api/namespace.service';
import { Cluster } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { Namespace } from 'app/typings';

interface CustomDeleteNamepace {
  cluster?: Cluster;
  kubernetes: Namespace;
}

@Component({
  selector: 'rc-delete-namespace',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class DeleteNamespaceComponent implements OnInit {
  @ViewChild('form')
  form: NgForm;

  isError = false;
  loading = false;
  inputName: string;
  submitting = false;
  deleteDisabled = true;
  clusterNames: string[] = [];
  stagedNamespaceCreate: boolean;
  namespace: CustomDeleteNamepace;

  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      namespace: CustomDeleteNamepace;
      projectName: string;
      successFun: Function;
    },
    private dialogRef: DialogRef,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    private dialogService: DialogService,
    private errorToast: ErrorsToastService,
    private messageService: MessageService,
    private namespaceService: NamespaceService,
    private environmentService: EnvironmentService,
    private notificationService: NotificationService,
  ) {
    this.stagedNamespaceCreate = this.environmentService.environments.staged_namespace_create;
  }

  async ngOnInit() {
    this.namespace = this.dialogData.namespace;
    if (
      this.namespace.cluster.mirror &&
      this.namespace.cluster.mirror.regions
    ) {
      this.loading = true;
      let count = 0;
      this.namespace.cluster.mirror.regions.forEach(async cluster => {
        count += 1;
        try {
          await this.namespaceService.getNamespace(
            cluster.id,
            this.namespace.kubernetes.metadata.name,
          );
          this.clusterNames.push(cluster.display_name);
        } catch (e) {
          if (e.errors && e.errors[0].code !== 'resource_not_exist') {
            this.errorToast.error(e);
          }
        } finally {
          if (count === this.namespace.cluster.mirror.regions.length) {
            this.loading = false;
            this.cdr.detectChanges();
          }
        }
      });
    } else {
      this.clusterNames.push(this.namespace.cluster.display_name);
    }
  }

  async delete() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    this.submitting = true;
    const reqData =
      this.namespace.cluster.mirror && this.namespace.cluster.mirror.regions
        ? this.namespace.cluster.mirror.regions.map(cluster => {
            return {
              clusterName: cluster.name,
              clusterDisplayName: cluster.display_name,
              namespace: this.namespace.kubernetes.metadata.name,
              projectName: this.dialogData.projectName,
            };
          })
        : [
            {
              clusterName: this.namespace.cluster.name,
              clusterDisplayName: this.namespace.cluster.display_name,
              namespace: this.namespace.kubernetes.metadata.name,
              projectName: this.dialogData.projectName,
            },
          ];
    try {
      const batchRes = this.stagedNamespaceCreate
        ? await this.namespaceService.deleteNamespaceBatchPaas(reqData)
        : await this.namespaceService.deleteNamespaceBatch(reqData);
      let successNum = 0;
      if (batchRes.responses) {
        const values = Object.entries(batchRes.responses);
        values.forEach(([key, value]) => {
          if ((value.code + '').startsWith('2')) {
            successNum += 1;
          } else {
            this.notificationService.error({
              title: this.translate.get(JSON.parse(value.body).errors[0].code),
              content:
                this.translate.get('cluster_namespace_delete_error', {
                  clusterDisplayName: key,
                }) +
                this.translate.get(JSON.parse(value.body).errors[0].message),
            });
          }
        });
        if (successNum === values.length) {
          this.messageService.success({
            content: this.translate.get('delete_success'),
          });
          if (this.dialogData.successFun) {
            this.dialogData.successFun();
          }
        } else {
          this.dialogService.confirm({
            title: this.translate.get('delete_namespace_fail'),
            content: this.translate.get('create_namespace_tip5'),
            confirmText: this.translate.get('got_it'),
            cancelButton: false,
            beforeConfirm: reslove => {
              reslove();
            },
          });
        }
        this.dialogRef.close();
      }
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.submitting = false;
    }
  }

  checkForm() {
    if (this.form.invalid) {
      this.deleteDisabled = true;
      return;
    }
    this.deleteDisabled = false;
    this.cdr.markForCheck();
  }
}
