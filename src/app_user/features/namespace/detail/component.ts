import { DialogRef, DialogService, DialogSize } from '@alauda/ui';
import {
  Component,
  Inject,
  OnDestroy,
  OnInit,
  TemplateRef,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import { get, trim } from 'lodash-es';
import { Subject } from 'rxjs';
import { takeUntil } from 'rxjs/operators';

import { EnvironmentService } from 'app/services/api/environment.service';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import {
  Namespace,
  NamespaceService,
} from 'app/services/api/namespace.service';
import { AdminRoleApi, ProjectService } from 'app/services/api/project.service';
import {
  Cluster,
  MirrorCluster,
  RegionService,
} from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account, LimitRangeItem } from 'app/typings';
import { Weblabs } from 'app/typings';
import {
  formatK8sCommonUnit,
  formatNumUnit,
} from 'app2/features/cluster/utils';
import { ProjectComponent } from 'app_user/features/project/project.component';
import { QuotaConfigEnum } from 'app_user/features/project/types';
import { AsmProject } from '../../micro-service/service-mesh/types';
import {
  toModel,
  toResource,
} from '../../micro-service/service-mesh/util.service';
import { DeleteNamespaceComponent } from '../delete/component';
import { UpdateQuotaComponent } from '../update-quota/component';

enum TargetEnum {
  CPU = 'cpu',
  MEMORY = 'memory',
}

@Component({
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class NamespaceDetailComponent implements OnInit, OnDestroy {
  @ViewChild('asmEnableConfirmDialog')
  private enableConfirmDialog: TemplateRef<any>;
  private clusterName: string;
  private onDestroy$ = new Subject<void>();
  private asmDialogRef: DialogRef;
  private serviceMeshProject: AsmProject;
  private adminRoles: AdminRoleApi[] = [];
  private mirrorClusters: MirrorCluster[];
  private clusterQuotaKeys: string[] = [];

  loading: boolean;
  cluster: Cluster;
  quotaItem: any = {};
  projectName: string;
  initialized: boolean;
  canViewAudit = false;
  namespace: Namespace;
  namespaceName: string;
  groupEnabled: boolean;
  displayEnterBtn = true;
  installedServiceMesh: boolean;
  stagedNamespaceCreate: boolean;
  accountListClusterNames: string;
  namespaceDeletePremission: boolean;
  namespaceUpdatePremission: boolean;

  limitRangeItems: {
    target: TargetEnum;
    limit: string;
    request: string;
    max: string;
  }[] = [];
  columns = ['target', 'limit', 'request', 'max'];
  unitMap = {
    pods: this.translateService.get('ge'),
    pvc_num: this.translateService.get('ge'),
    cpu: this.translateService.get('unit_core'),
    memory: this.translateService.get('unit_gb'),
    storage: this.translateService.get('unit_gb'),
    cpu_limits: this.translateService.get('unit_core'),
    memory_limits: this.translateService.get('unit_gb'),
    cpu_requests: this.translateService.get('unit_core'),
    memory_requests: this.translateService.get('unit_gb'),
  };

  get labels() {
    return this.k8sResourceService.getUserAndBuiltInLabelsOrAnnotations(
      get(this.namespace, 'kubernetes.metadata.labels', {}),
    ).user;
  }

  get annotations() {
    return this.k8sResourceService.getUserAndBuiltInLabelsOrAnnotations(
      get(this.namespace, 'kubernetes.metadata.annotations', {}),
    ).user;
  }

  get namespaceUuid() {
    return `${this.account.namespace}:${this.clusterName}:${
      this.namespace.kubernetes.metadata.name
    }`;
  }

  constructor(
    private router: Router,
    private dialogService: DialogService,
    private regionService: RegionService,
    private activateRoute: ActivatedRoute,
    private projectService: ProjectService,
    private roleUtil: RoleUtilitiesService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ACCOUNT) public account: Account,
    private translateService: TranslateService,
    private namespaceService: NamespaceService,
    private errorToast: ErrorsToastService,
    private projectComponent: ProjectComponent,
    private environmentService: EnvironmentService,
    private k8sResourceService: K8sResourceService,
  ) {
    this.stagedNamespaceCreate = this.environmentService.environments.staged_namespace_create;
  }

  async ngOnInit() {
    this.groupEnabled = this.weblabs.GROUP_ENABLED;
    this.roleUtil
      .resourceTypeSupportPermissions('audit', {}, 'list')
      .then(res => (this.canViewAudit = res));
    this.activateRoute.params
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(async (params: Params) => {
        this.namespaceName = params['name'];
        this.clusterName = params['cluster'];
        this.projectName = this.projectComponent.baseParamsSnapshot.project;
        await this.getNamespaceDetail();
        this.mirrorClusters =
          this.cluster && this.cluster.mirror
            ? this.cluster.mirror.regions || []
            : [];
        if (this.mirrorClusters.length) {
          this.accountListClusterNames = this.mirrorClusters
            .map(region => region.name)
            .join(',');
        } else {
          this.accountListClusterNames = this.clusterName;
        }
        this.installedServiceMesh = await this.isServiceMeshInstalled();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  get asmEnabled(): boolean {
    return get(this.serviceMeshProject, 'serviceMesh', '') === 'enabled';
  }

  get adminListStr(): string {
    if (Array.isArray(this.adminRoles) && this.adminRoles.length) {
      return this.adminRoles
        .map(
          admin =>
            `${admin.name}${admin.realname && '（' + admin.realname + ')'}`,
        )
        .join(',');
    } else {
      return '-';
    }
  }

  keys(obj: object) {
    return Object.keys(obj);
  }

  async updateQuota() {
    const isMirror = !!this.mirrorClusters.length;
    let k8sLimitRange = null;
    const promiseArr: Promise<any>[] = !isMirror
      ? [
          this.namespaceService.getNamespaceQuota(
            this.clusterName,
            this.namespaceName,
          ),
        ]
      : this.mirrorClusters.map(c =>
          this.namespaceService.getNamespaceQuota(c.name, this.namespaceName),
        );
    const [{ clusters }, ...quotaResources] = await Promise.all([
      this.projectService.getProjectClustersQuota(
        this.projectName,
        !isMirror
          ? [this.cluster.id]
          : this.cluster.mirror.regions.map(region => region.id),
      ),
      ...promiseArr,
    ]);
    const quotaSettingDate = clusters;

    try {
      const limitRangeResource = await this.namespaceService.getNamespacePodQuota(
        this.clusterName,
        this.namespaceName,
      );
      k8sLimitRange = get(limitRangeResource, 'kubernetes.spec.limits[0]');
    } catch (e) {}

    this.dialogService.open(UpdateQuotaComponent, {
      size: DialogSize.Large,
      data: {
        projectName: this.projectName,
        clustersQuota: quotaSettingDate,
        namespaceName: this.namespaceName,
        clusterName: this.clusterName,
        mirror: this.cluster ? this.cluster.mirror : null,
        selfQuotaConfigs: quotaResources.map(qr =>
          get(qr, 'kubernetes.spec.hard'),
        ),
        selfUsedConfigs: quotaResources.map(qr =>
          get(qr, 'kubernetes.status.used'),
        ),
        k8sLimitRange,
        clusterQuotaKeys: this.clusterQuotaKeys,
        successFun: () => this.refreshQuota(),
      },
    });
  }

  async refreshQuota() {
    const quotaResource = await this.namespaceService
      .getNamespaceQuota(this.clusterName, this.namespaceName)
      .catch(() => ({}));
    const k8sQuotaSpecHard = get(quotaResource, 'kubernetes.spec.hard');
    if (k8sQuotaSpecHard) {
      const quotaConfig = {
        hard: k8sQuotaSpecHard,
        used: get(quotaResource, 'kubernetes.status.used'),
      };
      this.clusterQuotaKeys.forEach(k => {
        switch (k) {
          case 'cpu':
            this.quotaItem['cpu_requests'] = [
              {
                name: this.translateService.get('used'),
                value: formatNumUnit(
                  quotaConfig.used[QuotaConfigEnum['cpu_requests']],
                ),
              },
              {
                name: this.translateService.get('unused'),
                value:
                  formatNumUnit(
                    quotaConfig.hard[QuotaConfigEnum['cpu_requests']],
                  ) -
                  formatNumUnit(
                    quotaConfig.used[QuotaConfigEnum['cpu_requests']],
                  ),
              },
            ];
            this.quotaItem['cpu_limits'] = [
              {
                name: this.translateService.get('used'),
                value: formatNumUnit(
                  quotaConfig.used[QuotaConfigEnum['cpu_limits']],
                ),
              },
              {
                name: this.translateService.get('unused'),
                value:
                  formatNumUnit(
                    quotaConfig.hard[QuotaConfigEnum['cpu_limits']],
                  ) -
                  formatNumUnit(
                    quotaConfig.used[QuotaConfigEnum['cpu_limits']],
                  ),
              },
            ];
            break;
          case 'memory':
            this.quotaItem['memory_requests'] = [
              {
                name: this.translateService.get('used'),
                value: +formatK8sCommonUnit(
                  quotaConfig.used[QuotaConfigEnum['memory_requests']],
                  true,
                ),
              },
              {
                name: this.translateService.get('unused'),
                value:
                  +formatK8sCommonUnit(
                    quotaConfig.hard[QuotaConfigEnum['memory_requests']],
                    true,
                  ) -
                  +formatK8sCommonUnit(
                    quotaConfig.used[QuotaConfigEnum['memory_requests']],
                    true,
                  ),
              },
            ];
            this.quotaItem['memory_limits'] = [
              {
                name: this.translateService.get('used'),
                value: +formatK8sCommonUnit(
                  quotaConfig.used[QuotaConfigEnum['memory_limits']],
                  true,
                ),
              },
              {
                name: this.translateService.get('unused'),
                value:
                  +formatK8sCommonUnit(
                    quotaConfig.hard[QuotaConfigEnum['memory_limits']],
                    true,
                  ) -
                  +formatK8sCommonUnit(
                    quotaConfig.used[QuotaConfigEnum['memory_limits']],
                    true,
                  ),
              },
            ];
            break;
          case 'storage':
            this.quotaItem[k] = [
              {
                name: this.translateService.get('used'),
                value: +formatK8sCommonUnit(
                  quotaConfig.used[QuotaConfigEnum[k]],
                  true,
                ),
              },
              {
                name: this.translateService.get('unused'),
                value:
                  +formatK8sCommonUnit(
                    quotaConfig.hard[QuotaConfigEnum[k]],
                    true,
                  ) -
                  +formatK8sCommonUnit(
                    quotaConfig.used[QuotaConfigEnum[k]],
                    true,
                  ),
              },
            ];
            break;
          case 'pods':
          case 'pvc_num':
            this.quotaItem[k] = [
              {
                name: this.translateService.get('used'),
                value: +quotaConfig.used[QuotaConfigEnum[k]],
              },
              {
                name: this.translateService.get('unused'),
                value:
                  formatNumUnit(quotaConfig.hard[QuotaConfigEnum[k]]) -
                  formatNumUnit(quotaConfig.used[QuotaConfigEnum[k]]),
              },
            ];
            break;
        }
      });

      try {
        const limitRangeResource = await this.namespaceService.getNamespacePodQuota(
          this.clusterName,
          this.namespaceName,
        );
        const k8sLimitRange: LimitRangeItem = get(
          limitRangeResource,
          'kubernetes.spec.limits[0]',
          {},
        );
        const targets = Object.keys(TargetEnum).map(k => k.toLowerCase());
        this.limitRangeItems = targets.map((target: TargetEnum) =>
          this.getLimitrangeItem(target, k8sLimitRange),
        );
      } catch (e) {}
    }
  }

  private getLimitrangeItem(target: TargetEnum, data: LimitRangeItem) {
    return {
      target,
      limit: data.default[target],
      request: data.defaultRequest[target],
      max: data.max[target],
    };
  }

  async deleteNamespace() {
    this.dialogService.open(DeleteNamespaceComponent, {
      data: {
        projectName: this.projectName,
        namespace: {
          cluster: this.cluster,
          kubernetes: this.namespace.kubernetes,
        },
        successFun: () => {
          this.router.navigate(['/console/user']);
        },
      },
    });
  }

  private async initQuotaConfig() {
    try {
      const quotaConfig = await this.environmentService.getConfigurations(
        'quota',
      );
      this.clusterQuotaKeys = quotaConfig.map(v => v.name);
    } catch (e) {
      this.errorToast.error(e);
    }
  }

  private async getNamespaceDetail() {
    try {
      const [
        cluster,
        namespace,
        adminRoles,
        serviceMeshProjects,
      ] = await Promise.all([
        this.regionService.getCluster(this.clusterName).catch(() => null),
        this.namespaceService.getNamespace(
          this.clusterName,
          this.namespaceName,
        ),

        this.projectService.getAdminRoles({
          level: 'namespace',
          cluster_name: this.clusterName,
          k8s_namespace_name: this.namespaceName,
          project_name: this.projectName,
        }),
        this.k8sResourceService.getK8sResources('projects', {
          clusterName: this.clusterName,
        }),
      ]);
      this.handleServiceMeshProjects(
        serviceMeshProjects.map(item => toModel(item.kubernetes)),
      );
      this.namespaceUpdatePremission = this.roleUtil.resourceHasPermission(
        namespace,
        'namespace',
        'update',
      );
      this.namespaceDeletePremission = this.roleUtil.resourceHasPermission(
        namespace,
        'namespace',
        'delete',
      );
      this.namespaceUpdatePremission = this.roleUtil.resourceHasPermission(
        namespace,
        'namespace',
        'update',
      );
      if (this.groupEnabled) {
        const {
          results,
        } = await this.projectService.getNamespacesWithSpaceInfo({
          namepsace_name: this.namespaceName,
          cluster_uuid: cluster.id,
        });
        this.displayEnterBtn = !!get(
          results.filter(
            n =>
              n.namespace_uuid ===
              `${this.account.namespace}:${cluster.name}:${
                namespace.kubernetes.metadata.name
              }`,
          )[0],
          'spaces.length',
        );
      }
      this.cluster = cluster;
      this.namespace = namespace;
      this.adminRoles = adminRoles;
    } catch (e) {
    } finally {
      this.initialized = true;
    }
    await this.initQuotaConfig();
    await this.refreshQuota();
  }

  get adminRolesDisplay(): string {
    return (
      this.adminRoles
        .map(
          role =>
            `${role.name}${
              role.realname ? '(' + trim(role.realname) + ')' : ''
            }`,
        )
        .join(', ') || '-'
    );
  }

  get asmDialogText() {
    return this.asmEnabled
      ? {
          title: 'disable_service_mesh',
          content: 'disable_service_mesh_confirm',
          value: 'disabled',
          button: 'switch_off',
        }
      : {
          title: 'enable_service_mesh',
          content: 'enable_service_mesh_confirm',
          value: 'enabled',
          button: 'switch_on',
        };
  }

  asmEnableConfirm() {
    this.asmDialogRef = this.dialogService.open(this.enableConfirmDialog, {
      size: DialogSize.Medium,
    });
  }

  async enableOrDisableServiceMesh(serviceMesh: string) {
    try {
      if (this.serviceMeshProject) {
        await this.k8sResourceService.updateK8sReource(
          this.clusterName,
          'projects',
          toResource({ ...this.serviceMeshProject, serviceMesh: serviceMesh }),
        );
        this.asmDialogRef.close();
        await this.refreshProjectDetail();
      } else {
        const createProjectBody: AsmProject = {
          name: this.namespaceName,
          displayName: '',
          description: '',
          projectManagers: null,
          creationTimestamp: null,
          serviceMesh: serviceMesh,
        };
        await this.k8sResourceService.createK8sReource(
          this.clusterName,
          'projects',
          toResource(createProjectBody),
        );
        this.asmDialogRef.close();
        await this.refreshProjectDetail();
      }
    } catch (e) {
      this.errorToast.error(e);
    }
  }

  async refreshProjectDetail() {
    try {
      const serviceMeshProjects = await this.k8sResourceService.getK8sResources(
        'projects',
        {
          clusterName: this.clusterName,
        },
      );
      this.handleServiceMeshProjects(
        serviceMeshProjects.map(item => toModel(item.kubernetes)),
      );
    } catch (e) {
      this.errorToast.error(e);
    }
  }

  async isServiceMeshInstalled() {
    try {
      await this.k8sResourceService.getK8sReourceDetail('alaudaproducts', {
        clusterName: this.clusterName,
        name: 'acp-service-mesh',
      });
      return true;
    } catch (e) {
      return false;
    }
  }

  handleServiceMeshProjects(serviceMeshProjects: AsmProject[]) {
    serviceMeshProjects.forEach(project => {
      if (project.name === this.namespaceName) {
        this.serviceMeshProject = project;
      }
    });
  }

  enterNamespace() {
    this.router.navigate([
      '/console/user/workspace',
      {
        project: this.projectName,
        cluster: this.clusterName,
        namespace: this.namespaceName,
      },
    ]);
  }
}
