import { NgModule } from '@angular/core';
import { AuditSharedModule } from 'app/features-shared/audit/shared.module';
import { NamespaceSharedModule } from 'app/features-shared/namespace/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';

import { CreateNamespaceComponent } from './create/create-namespace.component';
import { CreateNamespaceLimitRangeComponent } from './create/limitrange-form/component';
import { CreateNamespacePrimaryComponent } from './create/namespace-form/component';
import { CreateNamespaceNavigationComponent } from './create/navigation/component';
import { CreateNamespaceResourceQuotaComponent } from './create/quota-form/component';
import { NamespaceDetailComponent } from './detail/component';
import { NamespaceRoutingModule } from './routing.module';
import { NamespaceUtilService } from './util.service';

@NgModule({
  imports: [
    AuditSharedModule,
    SharedModule,
    NamespaceRoutingModule,
    NamespaceSharedModule,
    FormTableModule,
  ],
  declarations: [
    NamespaceDetailComponent,
    CreateNamespaceComponent,
    CreateNamespaceResourceQuotaComponent,
    CreateNamespacePrimaryComponent,
    CreateNamespaceLimitRangeComponent,
    CreateNamespaceNavigationComponent,
  ],
  providers: [NamespaceUtilService],
})
export class NamespaceModule {}
