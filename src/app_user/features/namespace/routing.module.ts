import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { CreateNamespaceComponent } from './create/create-namespace.component';
import { NamespaceDetailComponent } from './detail/component';

const routes: Routes = [
  {
    path: 'detail/:cluster/:name',
    component: NamespaceDetailComponent,
  },
  {
    path: 'create',
    component: CreateNamespaceComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NamespaceRoutingModule {}
