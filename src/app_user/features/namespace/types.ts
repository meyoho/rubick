import { LimitRange, Namespace, ResourceQuota } from 'app/typings';

import { NewCluster } from 'app/services/api/project.service';

/** NAMESPACE ENUM */
export enum NamespaceStatusEnum {
  PENDING = 'pending',
  READY = 'ready',
  UNKNOWN = 'unknown',
}

/** CREATE NAMESPACE INTERFACES */

type FormResourceDataType = Namespace | ResourceQuota | LimitRange;

export interface FormResource {
  cluster?: NewCluster;
  namespaceName?: string;
  resources: {
    cluster: string;
    data: FormResourceDataType;
  }[];
}
