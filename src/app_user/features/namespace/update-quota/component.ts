import { DIALOG_DATA, DialogRef, NotificationService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';

import { get, isEmpty, mapKeys, mapValues, pick } from 'lodash-es';

import {
  LimitRange,
  NamespaceService,
  QuotaStatusModel,
} from 'app/services/api/namespace.service';
import {
  NewCluster,
  TransferedNewCluster,
} from 'app/services/api/project.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { Mirror } from 'app2/features/cluster/mirror-page/mirror.service';
import {
  formatK8sCommonUnit,
  formatNumUnit,
} from 'app2/features/cluster/utils';
import {
  limitsRangeKeys,
  PodQuota,
} from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';
import { QuotaConfigEnum } from 'app_user/features/project/types';

@Component({
  selector: 'rc-app-update-quota',
  templateUrl: './template.html',
  styleUrls: ['./style.scss'],
})
export class UpdateQuotaComponent implements OnInit {
  transferedClusterSetting: TransferedNewCluster[];
  clustersQuota: NewCluster[];
  submitting = false;
  minQuotaRanges = {};
  hardQuotaRanges = {};
  @ViewChild('form')
  form: NgForm;

  constructor(
    private errorToastService: ErrorsToastService,
    private notification: NotificationService,
    private translate: TranslateService,
    private errorToast: ErrorsToastService,
    private namespaceService: NamespaceService,
    @Inject(ACCOUNT) public account: Account,
    @Inject(DIALOG_DATA)
    public dialogData: {
      clustersQuota: NewCluster[];
      namespaceName: string;
      clusterName: string;
      projectName: string;
      selfQuotaConfigs: QuotaStatusModel[];
      selfUsedConfigs: QuotaStatusModel[];
      k8sLimitRange: LimitRange;
      mirror: Mirror;
      clusterQuotaKeys: string[];
      successFun: Function;
    },
    private dialogRef: DialogRef<any>,
  ) {}

  ngOnInit() {
    this.transferedClusterSetting = this.dialogData.clustersQuota.map(
      c => Object.assign({}, c, { quota: {} }) as any,
    );

    const isMirror = !isEmpty(this.dialogData.mirror);
    //test
    const clustersQuota = this.dialogData.clustersQuota.map((newCluster, i) => {
      if (
        !get(newCluster, 'quota') ||
        isEmpty(this.dialogData.selfQuotaConfigs)
      ) {
        return newCluster;
      }
      this.minQuotaRanges[newCluster.name] = {};
      this.hardQuotaRanges[newCluster.name] = {};
      this.dialogData.clusterQuotaKeys.forEach(k => {
        switch (k) {
          case 'cpu':
          case 'pods':
          case 'pvc_num':
            newCluster.quota[k].used -= +formatNumUnit(
              this.dialogData.selfQuotaConfigs[i][QuotaConfigEnum[k]],
            );
            this.minQuotaRanges[newCluster.name][k] = +formatNumUnit(
              this.dialogData.selfUsedConfigs[i][QuotaConfigEnum[k]],
            );
            this.hardQuotaRanges[newCluster.name][k] = +formatNumUnit(
              this.dialogData.selfQuotaConfigs[i][QuotaConfigEnum[k]],
            );
            break;
          case 'memory':
          case 'storage':
            newCluster.quota[k].used -= +formatK8sCommonUnit(
              this.dialogData.selfQuotaConfigs[i][QuotaConfigEnum[k]],
              true,
            );
            this.minQuotaRanges[newCluster.name][k] = +formatK8sCommonUnit(
              this.dialogData.selfUsedConfigs[i][QuotaConfigEnum[k]],
              true,
            );
            this.hardQuotaRanges[newCluster.name][k] = +formatK8sCommonUnit(
              this.dialogData.selfQuotaConfigs[i][QuotaConfigEnum[k]],
            );
            break;
        }
      });
      if (!!this.dialogData.k8sLimitRange) {
        limitsRangeKeys.forEach(k => {
          switch (k) {
            case 'pod_default_cpu':
              newCluster.quota[k] = +formatNumUnit(
                get(this.dialogData.k8sLimitRange, 'default.cpu'),
              );
              break;
            case 'pod_default_request_cpu':
              newCluster.quota[k] = +formatNumUnit(
                get(this.dialogData.k8sLimitRange, 'defaultRequest.cpu'),
              );
              break;
            case 'pod_max_cpu':
              newCluster.quota[k] = +formatNumUnit(
                get(this.dialogData.k8sLimitRange, 'max.cpu'),
              );
              break;
            case 'pod_default_memory':
              newCluster.quota[k] = +formatK8sCommonUnit(
                get(this.dialogData.k8sLimitRange, 'default.memory'),
              );
              break;
            case 'pod_default_request_memory':
              newCluster.quota[k] = +formatK8sCommonUnit(
                get(this.dialogData.k8sLimitRange, 'defaultRequest.memory'),
              );
              break;
            case 'pod_max_memory':
              newCluster.quota[k] = +formatK8sCommonUnit(
                get(this.dialogData.k8sLimitRange, 'max.memory'),
              );
              break;
          }
        });
      }
      return Object.assign({}, newCluster, { quota: newCluster.quota });
    });
    this.clustersQuota = isMirror
      ? [
          {
            name: this.dialogData.mirror.name,
            origin_display_name: this.dialogData.mirror.display_name,
            display_name: this.dialogData.mirror.display_name,
            regions: clustersQuota,
          },
        ]
      : clustersQuota;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    try {
      this.submitting = true;
      const arr = this.transferedClusterSetting.map(c => {
        //MOCK FOR ZYRF
        const MOCK_QUOTA_KEYS = ['cpu', 'memory', 'storage', 'pods', 'pvc_num'];
        if (!this.dialogData.clusterQuotaKeys.includes('pods')) {
          c.quota['pods'] = 1000;
        }
        if (!this.dialogData.clusterQuotaKeys.includes('pvc_num')) {
          c.quota['pvc_num'] = 1000;
        }

        return {
          clusterName: c.name,
          clusterDisplayName: c.display_name,
          namespace: this.dialogData.namespaceName,
          projectName: this.dialogData.projectName,
          quotaConfig: mapValues(
            mapKeys(
              pick(c.quota, MOCK_QUOTA_KEYS),
              (_v, k) => QuotaConfigEnum[k],
            ),
            v => String(v),
          ),
          podQuotaConfig: pick(c.quota, limitsRangeKeys) as PodQuota,
        };
      });
      const batchRes = await this.namespaceService.updateNamespaceQuotaBatch(
        arr,
      );

      if (batchRes.responses) {
        let successNum = 0;
        const values = Object.entries(batchRes.responses);
        values.forEach(([key, value]) => {
          if ((value.code + '').startsWith('2')) {
            successNum += 1;
            this.notification.success({
              content: this.translate.get('cluster_quota_update_success', {
                clusterDisplayName: key,
              }),
            });
          } else {
            this.notification.error({
              title: this.translate.get(JSON.parse(value.body).errors[0].code),
              content:
                this.translate.get('cluster_quota_update_error', {
                  clusterDisplayName: key,
                }) +
                this.translate.get(JSON.parse(value.body).errors[0].message),
            });
          }
        });
        if (successNum === values.length) {
          this.dialogRef.close();
        }
        if (successNum > 0 && this.dialogData.successFun) {
          this.dialogData.successFun();
        }
      } else {
        this.errorToast.error(batchRes);
      }
    } catch (e) {
      this.errorToastService.error(e);
    } finally {
      this.submitting = false;
    }
  }
}
