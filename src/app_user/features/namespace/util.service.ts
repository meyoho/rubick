import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import {
  CreateNamespaceRes,
  NamespaceService,
} from 'app/services/api/namespace.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { BatchReponse } from 'app_user/core/types';

@Injectable()
export class NamespaceUtilService {
  constructor(
    private router: Router,
    private namespaceService: NamespaceService,
    private errorToastService: ErrorsToastService,
  ) {}

  /** Create */

  createNamespace(params: {
    project: string;
    cluster: string;
    namespace: string;
    resources: {
      cluster: string;
      resource: {
        key: string;
        value: any;
      };
    }[];
  }): Promise<BatchReponse | CreateNamespaceRes> {
    if (params.resources && params.resources.length > 1) {
      // return Promise.reject(null);
      return this.namespaceService
        .batchCreateNamespacePaas({
          project: params.project,
          namespace: params.namespace,
          resources: params.resources.map(r => ({
            cluster: r.cluster,
            payload: {
              [r.resource.key]: r.resource.value,
            },
          })),
        })
        .then((res: BatchReponse) => {
          return res;
        })
        .catch(e => {
          this.errorToastService.error(e);
          throw e;
        });
    } else {
      const key = params.resources[0].resource.key;
      const value = params.resources[0].resource.value;
      return this.namespaceService
        .createNamespacePaas({
          cluster: params.cluster,
          namespace: params.namespace,
          payload: {
            [key]: value,
          },
        })
        .then(res => res)
        .catch(e => {
          this.errorToastService.error(e);
          throw e;
        });
    }
  }

  getNamespaceCreateActions() {
    return this.namespaceService
      .getNamespaceActions()
      .then(res => ['namespace'].concat(res.actions))
      .catch(() => ['namespace']);
  }

  /** router */
  jumpToProjectOverview(projectName: string) {
    this.router.navigate(['/console/user/project', { project: projectName }]);
  }
}
