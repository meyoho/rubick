import { Namespace } from 'app/typings';
import { BatchReponse } from 'app_user/core/types';
import { get } from 'lodash-es';

export function isBatchRes(res: any): res is BatchReponse {
  return !!res.responses;
}

export function reduceBatchRes(res: BatchReponse) {
  return Object.entries(res.responses).reduce(
    (
      acc: {
        success: any[];
        failure: any[];
      },
      [clusterName, data],
    ) => {
      if (data.code >= 300) {
        return {
          ...acc,
          failure: acc.failure.concat({
            clusterName,
            data,
          }),
        };
      } else {
        return {
          ...acc,
          success: acc.success.concat({
            clusterName,
            data,
          }),
        };
      }
    },
    {
      success: [],
      failure: [],
    },
  );
}

export function resolveNamespace(ns: Namespace) {
  const status = get(
    ns,
    `metadata.labels['namespace.alauda.io/status']`,
    'unknown',
  );
  const action = get(
    ns,
    `metadata.labels['namespace.alauda.io/action']`,
    'unknown',
  );
  const reason = get(
    ns,
    `metadata.annotations['namespace.alauda.io/reason']`,
    '',
  );
  const message = get(
    ns,
    `metadata.annotations['namespace.alauda.io/message']`,
    '',
  );

  return {
    status,
    action,
    reason,
    message,
  };
}
