import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { Component, Inject, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import * as jsyaml from 'js-yaml';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { TranslateService } from 'app/translate/translate.service';
import { createActions, updateActions } from 'app/utils/code-editor-config';

@Component({
  selector: 'rc-network-policy-create-dialog',
  templateUrl: './network-policy-create-dialog.component.html',
  styleUrls: ['./network-policy-create-dialog.component.scss'],
})
export class NetworkPolicyCreateDialogComponent implements OnInit, OnDestroy {
  projectName: string;
  clusterName: string;
  namespaceName: string;
  targetBase: any[] = [];

  isUpdate = false;
  submitting = false;
  submitted = false;
  loadingYaml = false;
  model = {
    name: '',
    namespace: '',
    networkPolicyYaml: '',
  };
  networkPolicyYaml = '';
  uuid = '';
  RESOURCE_TYPE = 'networkpolicies';

  @ViewChild('form')
  form: NgForm;
  editorActionsConfig = updateActions;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: true,
  };
  constructor(
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private dialogRef: DialogRef,
    private router: Router,
    private k8sResourceService: K8sResourceService,
    @Inject(DIALOG_DATA)
    private modalData: {
      model: {
        networkPolicyYaml: string;
        name: string;
        namespace: string;
      };
      routerParams: {
        project: string;
        cluster: string;
        namespace: string;
      };
      readOnly: boolean;
      isUpdate: boolean;
    },
  ) {}

  async ngOnInit() {
    this.projectName = this.modalData.routerParams.project;
    this.clusterName = this.modalData.routerParams.cluster;
    this.namespaceName = this.modalData.routerParams.namespace;
    this.codeEditorOptions.readOnly = this.modalData.readOnly;
    this.isUpdate = this.modalData.isUpdate;
    this.editorActionsConfig = this.isUpdate ? updateActions : createActions;
    this.loadingYaml = true;
    if (this.modalData) {
      this.model.name = this.modalData.model.name;
      this.model.namespace = this.modalData.model.namespace;
      let curYaml: any;
      if (!this.isUpdate) {
        curYaml = jsyaml.safeLoad(this.modalData.model.networkPolicyYaml);
        // rewrite name and namespace in yaml
        curYaml.metadata = {
          name: this.model.name,
          namespace: this.model.namespace,
        };
      } else {
        try {
          const res = await this.k8sResourceService.getK8sReourceDetail(
            this.RESOURCE_TYPE,
            {
              name: this.modalData.model.name,
              clusterName: this.clusterName,
              namespace: this.modalData.model.namespace,
            },
          );
          this.networkPolicyYaml = jsyaml.safeDump(res.kubernetes);
          this.uuid = res.kubernetes.metadata.uid;
          curYaml = jsyaml.safeLoad(this.networkPolicyYaml);
        } catch (e) {}
      }
      this.model.networkPolicyYaml = jsyaml.safeDump(curYaml);
      this.loadingYaml = false;
    }

    this.targetBase = [
      '/console/user',
      'workspace',
      {
        project: this.projectName,
        cluster: this.clusterName,
        namespace: this.namespaceName,
      },
      'network_policy',
    ];
  }
  ngOnDestroy() {}

  onSubmit() {
    this.submitted = true;
  }

  confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    try {
      jsyaml.safeLoad(this.model.networkPolicyYaml);
    } catch (error) {
      // yaml parse error
      this.auiNotificationService.error({
        title: this.translateService.get('yaml_error'),
        content: error.message,
      });
      return;
    }
    if (this.isUpdate) {
      this.updateNetWorkPolicy();
    } else {
      this.createNetWorkPolicy();
    }
  }

  createNetWorkPolicy() {
    const data = jsyaml.safeLoad(this.model.networkPolicyYaml);
    this.submitting = true;
    this.k8sResourceService
      .createK8sReource(this.clusterName, this.RESOURCE_TYPE, data)
      .then(() => {
        this.auiMessageService.success({
          content: this.translateService.get('network_policy_create_success'),
        });
        this.submitting = false;
        this.dialogRef.close();
        this.router.navigate(
          this.targetBase.concat(['detail', this.model.name]),
        );
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error({
          title: this.translateService.get('network_policy_create_failed'),
          content: errors[0].message,
        });
        this.submitting = false;
      });
  }

  updateNetWorkPolicy() {
    const data: any = jsyaml.safeLoad(this.model.networkPolicyYaml);
    // rewrite name and namespace before submit
    data.metadata = {
      name: this.model.name,
      namespace: this.model.namespace,
    };
    this.submitting = true;
    this.k8sResourceService
      .updateK8sReource(this.clusterName, this.RESOURCE_TYPE, data)
      .then(() => {
        this.auiMessageService.success({
          content: this.translateService.get('network_policy_update_success'),
        });
        this.submitting = false;
        this.dialogRef.close();
        this.router.navigate(
          this.targetBase.concat(['detail', this.model.name]),
        );
      })
      .catch(({ errors }) => {
        this.auiNotificationService.error({
          title: this.translateService.get('network_policy_update_failed'),
          content: errors[0].message,
        });
        this.submitting = false;
        this.dialogService.closeAll();
      });
  }
  cancel() {
    if (this.isUpdate) {
      this.dialogService
        .confirm({
          title: this.translateService.get(
            'network_policy_confirm_cancel_update',
            { name: this.model.name },
          ),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        })
        .then(() => {
          this.dialogService.closeAll();
        })
        .catch(() => {});
    } else {
      this.dialogService.closeAll();
    }
  }

  trackByIndex(index: number) {
    return index;
  }
}
