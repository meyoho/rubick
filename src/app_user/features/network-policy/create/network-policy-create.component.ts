import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { TranslateService } from 'app/translate/translate.service';
import { createActions } from 'app/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import * as jsyaml from 'js-yaml';

import { NetworkPolicyCreateDialogComponent } from './network-policy-create-dialog.component';
@Component({
  templateUrl: './network-policy-create.component.html',
  styleUrls: ['./network-policy-create.component.scss'],
})
export class NetworkPolicyCreateComponent implements OnInit {
  projectName: string;
  clusterName: string;
  namespaceName: string;
  targetBase: any[] = [];

  @ViewChild('createForm')
  form: NgForm;
  model: any;
  submitting = false;

  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };
  actionsConfigCreate = createActions;

  constructor(
    private dialogService: DialogService,
    private translate: TranslateService,
    private router: Router,
    private route: ActivatedRoute,
    private auiNotificationService: NotificationService,
    private workspaceComponent: WorkspaceComponent,
  ) {
    this.model = {
      name: '',
      namespace: '',
      networkPolicyYaml: '',
      cluster_id: '',
    };
  }

  ngOnInit() {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.projectName = baseParams['project'];
    this.clusterName = baseParams['cluster'];
    this.namespaceName = baseParams['namespace'];

    this.targetBase = [
      '/console/user',
      'workspace',
      {
        project: this.projectName,
        cluster: this.clusterName,
        namespace: this.namespaceName,
      },
      'network_policy',
    ];
    this.model.namespace = this.namespaceName;
    this.model.cluster_id = this.clusterName;
  }

  async cancel() {
    if (this.model.networkPolicyYaml) {
      try {
        await this.dialogService.confirm({
          title: this.translate.get('network_policy_confirm_cancel_create', {
            name: this.model.name,
          }),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
        });
        this.router.navigate(this.targetBase);
      } catch (e) {
        return;
      }
    } else {
      this.router.navigate(this.targetBase);
    }
  }

  async confirm() {
    if (this.form.valid && this.model.namespace && !this.submitting) {
      try {
        jsyaml.safeLoad(this.model.networkPolicyYaml);
      } catch (error) {
        // yaml parse error
        this.auiNotificationService.error({
          title: this.translate.get('yaml_error'),
          content: error.message,
        });
        return;
      }
      const yaml = jsyaml.safeLoad(this.model.networkPolicyYaml);
      if (yaml.metadata && yaml.metadata.name) {
        this.model.name = yaml.metadata.name;
      }
      this.openPreviewModal(this.model);
    }
  }

  async openPreviewModal(initVal: any) {
    const init = {
      model: initVal,
      routerParams: {
        project: this.projectName,
        cluster: this.clusterName,
        namespace: this.namespaceName,
      },
    };
    try {
      await this.dialogService.open(NetworkPolicyCreateDialogComponent, {
        size: DialogSize.Large,
        data: {
          ...init,
          readOnly: true,
          isUpdate: false,
        },
      });
    } catch (error) {}
  }

  back() {
    this.router.navigate(['../'], {
      relativeTo: this.route,
    });
  }
}
