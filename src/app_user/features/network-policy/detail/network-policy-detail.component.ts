import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';

import * as jsyaml from 'js-yaml';
import { Subject } from 'rxjs';
import { map, takeUntil } from 'rxjs/operators';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, NetworkPolicy } from 'app/typings';
import { NetworkPolicyCreateDialogComponent } from 'app_user/features/network-policy/create/network-policy-create-dialog.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: './network-policy-detail.component.html',
  styleUrls: ['./network-policy-detail.component.scss'],
})
export class NetworkPolicyDetailComponent implements OnInit, OnDestroy {
  projectName: string;
  clusterName: string;
  namespaceName: string;
  targetBase: any[] = [];

  private pollingTimer: number;
  private _loading = false;
  private _loadError: any;
  private name: string;
  private onDestroy$ = new Subject<void>();

  networkPolicyData: K8sResourceWithActions<NetworkPolicy>;
  networkPolicyYaml: string;
  uuid: string;
  networkPolicyTipClosed = false;

  destroyed = false;
  initialized = false;
  RESOURCE_TYPE = 'networkpolicies';

  codeEditorOptions = {
    language: 'yaml',
    readOnly: true,
  };

  constructor(
    private route: ActivatedRoute,
    private roleUtil: RoleUtilitiesService,
    private k8sResourceService: K8sResourceService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private dialogService: DialogService,
    private router: Router,
    private workspaceComponent: WorkspaceComponent,
  ) {}

  ngOnInit() {
    this.workspaceComponent.baseParams
      .pipe(
        takeUntil(this.onDestroy$),
        map((params: Params) => {
          return [params['project'], params['cluster'], params['namespace']];
        }),
      )
      .subscribe(async ([projectName, clusterName, namespaceName]) => {
        this.projectName = projectName;
        this.clusterName = clusterName;
        this.namespaceName = namespaceName;
        const params = this.route.snapshot.params;
        this.name = params['name'];
        await this.refetch();
        this.initialized = true;
      });

    this.targetBase = [
      '/console/user',
      'workspace',
      {
        project: this.projectName,
        cluster: this.clusterName,
        namespace: this.namespaceName,
      },
      'network_policy',
    ];
  }

  ngOnDestroy() {
    clearTimeout(this.pollingTimer);
    this.destroyed = true;
    if (this.onDestroy$) {
      this.onDestroy$.next();
    }
  }

  async refetch() {
    if (this.destroyed) {
      return;
    }
    this._loading = true;
    try {
      await this.k8sResourceService
        .getK8sReourceDetail(this.RESOURCE_TYPE, {
          name: this.name,
          clusterName: this.clusterName,
          namespace: this.namespaceName,
        })
        .then((res: K8sResourceWithActions<NetworkPolicy>) => {
          this.networkPolicyData = res;
          this.networkPolicyYaml = jsyaml.safeDump(res.kubernetes);
          this.uuid = this.networkPolicyData.kubernetes.metadata.uid;
        })
        .catch(({ status, errors }) => {
          this._loadError = errors;
          if (status === 403) {
            this.auiNotificationService.error({
              title: this.translate.get('permission_denied'),
            });
          } else if (status === 404) {
            this.auiNotificationService.error({
              title: this.translate.get('network_policy_not_exist'),
            });
          }
          this.router.navigate(this.targetBase);
        });
      this._loadError = null;
    } catch (e) {}

    this._loading = false;
    if (!this.destroyed) {
      this.cdr.detectChanges();
    }
  }

  get empty() {
    return !this.networkPolicyData;
  }

  get loading() {
    return this._loading;
  }

  get loadError() {
    return this._loadError;
  }

  canShowUpdate(item: K8sResourceWithActions<NetworkPolicy>) {
    return this.roleUtil.resourceHasPermission(
      item,
      'k8s_networkpolicies',
      'update',
    );
  }

  canShowDelete(item: K8sResourceWithActions<NetworkPolicy>) {
    return this.roleUtil.resourceHasPermission(
      item,
      'k8s_networkpolicies',
      'delete',
    );
  }

  async updateNetworkPolicy(init: K8sResourceWithActions<NetworkPolicy>) {
    const initVal = {
      model: {
        name: init.kubernetes.metadata.name,
        namespace: init.kubernetes.metadata.namespace,
        networkPolicyYaml: jsyaml.safeDump(init.kubernetes),
      },
      routerParams: {
        project: this.projectName,
        cluster: this.clusterName,
        namespace: this.namespaceName,
      },
    };
    try {
      const dialogRef = await this.dialogService.open(
        NetworkPolicyCreateDialogComponent,
        {
          size: DialogSize.Large,
          data: {
            ...initVal,
            readOnly: false,
            isUpdate: true,
          },
        },
      );
      dialogRef.afterClosed().subscribe(() => {
        this.refetch();
      });
    } catch (e) {}
  }

  async deleteNetworkPolicy(np: K8sResourceWithActions<NetworkPolicy>) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('network_policy_delete_confirm', {
          name: np.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('delete'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.k8sResourceService
      .deleteK8sReource(this.clusterName, this.RESOURCE_TYPE, np.kubernetes)
      .then(() => {
        this.auiMessageService.success({
          content: this.translate.get('network_policy_delete_success'),
        });
        // 删除后立即跳转list页会获取到已经删除的数据
        setTimeout(() => {
          this.router.navigate(this.targetBase);
        }, 1000);
      })
      .catch(({ errors }) => {
        if (errors.length) {
          this.auiNotificationService.error({
            title: this.translate.get('network_policy_delete_failed'),
            content: errors[0].message,
          });
        }
      });
  }

  openNetworkPolicyTip() {
    this.networkPolicyTipClosed = false;
  }
  onNetworkPolicyTipClosed() {
    this.networkPolicyTipClosed = true;
  }

  back() {
    this.router.navigate(['network_policy'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
