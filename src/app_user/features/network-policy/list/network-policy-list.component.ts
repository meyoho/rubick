import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { Router } from '@angular/router';

import * as jsyaml from 'js-yaml';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, NetworkPolicy } from 'app/typings';
import { NetworkPolicyCreateDialogComponent } from 'app_user/features/network-policy/create/network-policy-create-dialog.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: './network-policy-list.component.html',
  styleUrls: ['./network-policy-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ResourceListPermissionsService],
})
export class NetworkPolicyListComponent implements OnInit {
  projectName: string;
  clusterName: string;
  namespaceName: string;
  targetBase: any[] = [];
  columns: string[] = ['name', 'created_time', 'action'];
  networkPolicyList: K8sResourceWithActions<NetworkPolicy>[] = [];
  showZeroState: boolean;
  loading: boolean;
  keyword = '';
  networkPolicyCreateEnabled = false;
  RESOURCE_TYPE = 'networkpolicies';

  constructor(
    private dialogService: DialogService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private translateService: TranslateService,
    private k8sResourceService: K8sResourceService,
    private router: Router,
    private workspaceComponent: WorkspaceComponent,
    private cdr: ChangeDetectorRef,
    public resourcePermissionService: ResourceListPermissionsService,
  ) {}

  async ngOnInit() {
    const baseParams = this.workspaceComponent.baseParamsSnapshot;
    this.projectName = baseParams['project'];
    this.clusterName = baseParams['cluster'];
    this.namespaceName = baseParams['namespace'];
    this.resourcePermissionService.initContext({
      resourceType: 'k8s_networkpolicies',
      context: {
        project_name: baseParams.project,
        cluster_name: baseParams.cluster,
        namespace_name: baseParams.namespace,
      },
    });
    this.targetBase = [
      '/console/user',
      'workspace',
      {
        project: this.projectName,
        cluster: this.clusterName,
        namespace: this.namespaceName,
      },
      'network_policy',
    ];

    this.roleUtil
      .resourceTypeSupportPermissions(
        'k8s_networkpolicies',
        { cluster_name: this.clusterName },
        'create',
      )
      .then((res: boolean) => {
        this.networkPolicyCreateEnabled = res;
      });
    this.fetch(this.clusterName, this.namespaceName);
  }

  fetch(clusterName: string, namespace: string) {
    this.loading = true;
    this.k8sResourceService
      .getK8sResources(this.RESOURCE_TYPE, { clusterName, namespace })
      .then(res => {
        this.networkPolicyList = res;
        this.showZeroState = !this.networkPolicyList.length;
      })
      .catch(() => {
        this.showZeroState = true;
      })
      .then(() => {
        this.loading = false;
        this.cdr.markForCheck();
      });
  }

  refetch() {
    this.fetch(this.clusterName, this.namespaceName);
  }

  filterByName(event: string) {
    this.keyword = event;
  }

  create() {
    this.router.navigate(this.targetBase.concat(['create']));
  }

  getNetworkPolicyList() {
    return this.networkPolicyList.filter(
      item =>
        item.kubernetes.metadata.name.indexOf(this.keyword.toLowerCase()) !==
        -1,
    );
  }

  viewNetworkPolicy(np: K8sResourceWithActions<NetworkPolicy>) {
    const target = ['detail', np.kubernetes.metadata.name];
    this.router.navigate(this.targetBase.concat(target));
  }

  async deleteNetworkPolicy(np: K8sResourceWithActions<NetworkPolicy>) {
    try {
      await this.dialogService.confirm({
        title: this.translateService.get('delete'),
        content: this.translateService.get('network_policy_delete_confirm', {
          name: np.kubernetes.metadata.name,
        }),
        confirmText: this.translateService.get('delete'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }
    this.k8sResourceService
      .deleteK8sReource(this.clusterName, this.RESOURCE_TYPE, np.kubernetes)
      .then(() => {
        this.auiMessageService.success({
          content: this.translateService.get('network_policy_delete_success'),
        });
        // 删除后立即调用refetch会获取到已经删除的数据
        setTimeout(() => {
          this.refetch();
        }, 1000);
      })
      .catch(({ errors }) => {
        if (errors.length) {
          this.auiNotificationService.error({
            title: this.translateService.get('network_policy_delete_failed'),
            content: errors[0].message,
          });
        }
      });
  }

  async updateNetworkPolicy(init: K8sResourceWithActions<NetworkPolicy>) {
    const initVal = {
      model: {
        name: init.kubernetes.metadata.name,
        namespace: init.kubernetes.metadata.namespace,
        networkPolicyYaml: jsyaml.safeDump(init.kubernetes),
      },
      routerParams: {
        project: this.projectName,
        cluster: this.clusterName,
        namespace: this.namespaceName,
      },
    };
    try {
      await this.dialogService.open(NetworkPolicyCreateDialogComponent, {
        size: DialogSize.Large,
        data: {
          ...initVal,
          readOnly: false,
          isUpdate: true,
        },
      });
    } catch (e) {}
  }
}
