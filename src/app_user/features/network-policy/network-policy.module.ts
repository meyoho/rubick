import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { SharedModule } from 'app/shared/shared.module';

import { NetworkPolicyCreateDialogComponent } from './create/network-policy-create-dialog.component';
import { NetworkPolicyCreateComponent } from './create/network-policy-create.component';
import { NetworkPolicyDetailComponent } from './detail/network-policy-detail.component';
import { NetworkPolicyListComponent } from './list/network-policy-list.component';
import { NetworkPolicyRoutingModule } from './network-policy.routing.module';

const components = [
  NetworkPolicyCreateDialogComponent,
  NetworkPolicyCreateComponent,
  NetworkPolicyDetailComponent,
  NetworkPolicyListComponent,
];

@NgModule({
  imports: [
    SharedModule,
    RouterModule,
    EventSharedModule,
    NetworkPolicyRoutingModule,
  ],
  declarations: components,
  exports: components,
  providers: [],
  entryComponents: [NetworkPolicyCreateDialogComponent],
})
export class NetworkPolicyModule {}
