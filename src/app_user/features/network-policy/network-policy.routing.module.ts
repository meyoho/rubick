import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { NetworkPolicyCreateComponent } from './create/network-policy-create.component';
import { NetworkPolicyDetailComponent } from './detail/network-policy-detail.component';
import { NetworkPolicyListComponent } from './list/network-policy-list.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'list',
    pathMatch: 'full',
  },
  {
    path: 'list',
    component: NetworkPolicyListComponent,
    pathMatch: 'full',
  },
  {
    path: 'create',
    component: NetworkPolicyCreateComponent,
    pathMatch: 'full',
  },
  {
    path: 'detail/:name',
    component: NetworkPolicyDetailComponent,
    pathMatch: 'full',
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: [],
})
export class NetworkPolicyRoutingModule {}
