import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import { Space } from 'app/services/api/space.service';
import { AppState, getCurrentSpace } from 'app/store';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { isEqual } from 'lodash-es';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
} from 'rxjs/operators';

@Component({
  selector: 'rc-pipeline-create',
  templateUrl: './pipeline-create.component.html',
  styleUrls: ['./pipeline-create.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineCreateComponent implements OnInit {
  space: Space;
  project: string;
  params$ = combineLatest(this.route.paramMap, this.route.queryParamMap).pipe(
    map(([params, queryParams]: ParamMap[]) => {
      return {
        namespace: params.get('project'),
        method: queryParams.get('method') || 'jenkinsfile',
        type: queryParams.get('type') || '',
        name: queryParams.get('name') || '',
      };
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  space$ = this.store.select(getCurrentSpace).pipe(filter(s => !!s));

  constructor(
    private route: ActivatedRoute,
    private store: Store<AppState>,
    private workspace: WorkspaceComponent,
  ) {}

  ngOnInit() {
    this.project = this.workspace.baseParamsSnapshot.project;
  }
}
