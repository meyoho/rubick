import {
  ConfirmType,
  DialogRef,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
  ViewChild,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { get } from 'lodash-es';
import { catchError, filter, map, switchMap, take, tap } from 'rxjs/operators';

import { PipelineHistoriesComponent } from 'app/features-shared/pipeline/histories/histories.component';
import { PipelineParameterTriggerComponent } from 'app/features-shared/pipeline/parameter-trigger/parameter-trigger.component';
import {
  PipelineApiService,
  PipelineConfig,
  TriggerPipelineParameter,
} from 'app/services/api/pipeline';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { Space } from 'app/services/api/space.service';
import { AppState, getCurrentSpace } from 'app/store';
import { TranslateService } from 'app/translate/translate.service';

@Component({
  templateUrl: 'pipeline-detail.component.html',
  styleUrls: ['pipeline-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineDetailComponent implements OnInit {
  permissions = {
    canCreate: false,
    canUpdate: false,
    canDelete: false,
    canTrigger: false,
    canAbort: false,
  };
  activeTab: 'base' | 'jenkinsfile' = 'base';
  continue = true;
  name: string;

  params$ = this.route.paramMap.pipe(
    tap(param => {
      this.name = param.get('name');
    }),
    map(param => ({
      name: param.get('name'),
      namespace: param.get('project'),
    })),
  );

  space$ = this.store.select(getCurrentSpace).pipe(filter(s => !!s));

  @ViewChild(PipelineHistoriesComponent)
  histories: PipelineHistoriesComponent;

  constructor(
    private pipelineApi: PipelineApiService,
    private roleUtil: RoleUtilitiesService,
    private route: ActivatedRoute,
    private notification: NotificationService,
    private message: MessageService,
    private translate: TranslateService,
    private router: Router,
    private dialog: DialogService,
    private cdr: ChangeDetectorRef,
    private store: Store<AppState>,
  ) {}

  ngOnInit() {
    this.space$.subscribe(space => {
      this.getPermission(space);
    });
  }

  async getPermission(space: Space) {
    [
      this.permissions.canCreate,
      this.permissions.canUpdate,
      this.permissions.canDelete,
      this.permissions.canTrigger,
      this.permissions.canAbort,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_pipelineconfig',
      {
        space_name: space.name,
      },
      ['create', 'update', 'delete', 'trigger', 'abort'],
    );
  }

  fetchData = (param: any) =>
    this.space$.pipe(
      switchMap((space: Space) =>
        this.pipelineApi.getPipelineConfig(param.name, space.name || '').pipe(
          tap((pipeline: PipelineConfig) => {
            if (pipeline.status.phase !== 'Creating' || !pipeline) {
              this.stopFetchData();
            }
          }),
          catchError((err: any) => {
            this.stopFetchData();
            if (err.status === 403) {
              this.notification.error({
                title: this.translate.get('errorType'),
                content: get(err, 'errors.[0].message', ''),
              });
            }
            throw err;
          }),
        ),
      ),
    );

  triggerPipeline(pipelineConfig: PipelineConfig) {
    const parameters: TriggerPipelineParameter[] =
      get(pipelineConfig, 'parameters') || [];
    if (parameters.length) {
      const parameterDialogRef: DialogRef<
        PipelineParameterTriggerComponent
      > = this.dialog.open(PipelineParameterTriggerComponent, {
        size: DialogSize.Medium,
        data: {
          parameters: parameters,
        },
      });
      parameterDialogRef.afterClosed().subscribe((parameterValue: any) => {
        if (parameterValue) {
          this._triggerPipeline(parameterValue);
        }
      });
    } else {
      this._triggerPipeline();
    }
  }

  deletePipeline() {
    const spaceName = this.getCurrentSpaceName();
    this.dialog
      .confirm({
        title: this.translate.get('pipeline_delete_pipelineconfig_confirm', {
          name: this.name,
        }),
        cancelText: this.translate.get('cancel'),
        confirmText: this.translate.get('pipeline_sure'),
        confirmType: ConfirmType.Danger,
        beforeConfirm: (resolve, reject) => {
          this.pipelineApi.deletePipelineConfig(spaceName, this.name).subscribe(
            () => {
              this.message.success({
                content: this.translate.get(
                  'pipeline_pipelineconfig_delete_succ',
                  { name: this.name },
                ),
              });
              resolve();
              this.router.navigate(['../'], { relativeTo: this.route });
            },
            (err: any) => {
              this.notification.error({
                title: this.translate.get(
                  'pipeline_pipelineconfig_delete_failed',
                  { name: this.name },
                ),
                content: get(err, 'errors.[0].message', ''),
              });
              reject();
            },
          );
        },
      })
      .then(() => {})
      .catch(() => {});
  }

  updatePipeline(type = '') {
    this.router.navigate(['./', 'update'], {
      relativeTo: this.route,
      queryParams: {
        type: type,
      },
    });
  }

  copyPipeline(pipelineConfig: PipelineConfig) {
    const template = get(pipelineConfig, 'strategy.template');
    this.router.navigate(['../', 'create'], {
      relativeTo: this.route,
      queryParams: {
        type: 'copy',
        name: this.name,
        method: template ? 'template' : 'jenkinsfile',
      },
    });
  }

  changeTab(tabName: 'base' | 'jenkinsfile') {
    this.activeTab = tabName;
  }

  private _triggerPipeline(parameters?: any) {
    const spaceName = this.getCurrentSpaceName();
    this.pipelineApi
      .triggerPipeline(spaceName, this.name, parameters)
      .subscribe(
        () => {
          this.message.success({
            content: this.translate.get('pipeline_start_succ'),
          });
          this.histories.historyRefresh(true);
        },
        (err: any) => {
          this.notification.error({
            title: this.translate.get('pipeline_start_failed'),
            content: get(err, 'errors.[0].message', ''),
          });
        },
      );
  }

  private stopFetchData() {
    this.continue = false;
    this.cdr.detectChanges();
  }

  private getCurrentSpaceName() {
    let space: Space;
    this.space$
      .pipe(
        filter(s => !!s),
        take(1),
      )
      .subscribe(s => {
        space = s;
      });
    return space.name;
  }
}
