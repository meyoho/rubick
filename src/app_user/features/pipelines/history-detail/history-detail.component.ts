import { NotificationService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { get, head } from 'lodash-es';
import {
  catchError,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

import { PipelineApiService, PipelineHistory } from 'app/services/api/pipeline';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { Space } from 'app/services/api/space.service';
import { AppState, getCurrentSpace } from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { filterBy, getQuery, pageBy } from 'app/utils/query-builder';

@Component({
  selector: 'rc-pipeline-history-detail',
  templateUrl: './history-detail.component.html',
  styleUrls: ['./history-detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineHistoryDetailComponent implements OnInit {
  permissions = {
    canAbort: false,
    canDelete: false,
  };
  namespace: string;
  name: string;
  historyName: string;
  running = true;
  space$ = this.store.select(getCurrentSpace).pipe(filter(s => !!s));
  params$ = this.route.paramMap.pipe(
    map((params: ParamMap) => {
      return {
        namespace: params.get('project'),
        name: params.get('name'),
        historyName: params.get('historyName'),
      };
    }),
    tap(param => {
      this.namespace = param.namespace;
      this.name = param.name;
      this.historyName = param.historyName;
    }),
    switchMap((params: any) =>
      this.space$.pipe(
        map((space: Space) => ({
          ...params,
          space_name: space.name,
        })),
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  fetchData = (params: {
    namespace: string;
    name: string;
    historyName: string;
  }) =>
    this.space$.pipe(
      switchMap((space: Space) =>
        this.api
          .getPipelineHistories({
            ...getQuery(
              filterBy('label', `pipelineConfig:${params.name}`),
              filterBy('name', params.historyName),
              pageBy(0, 10),
            ),
            space_name: space.name,
          })
          .pipe(
            map((result: any) => {
              return head(result.histories);
            }),
            tap((history: PipelineHistory) => {
              const currentStatus = get(history, 'status.phase', '');
              this.running = ['Queued', 'Pending', 'Running'].includes(
                currentStatus,
              );
              this.cdr.detectChanges();
            }),
            catchError((err: any) => {
              this.running = false;
              this.cdr.detectChanges();
              if (err.status === 403) {
                this.notification.error({
                  title: this.translate.get('errorType'),
                  content: get(err, 'errors.[0].message', ''),
                });
              }
              throw err;
            }),
          ),
      ),
    );

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private api: PipelineApiService,
    private roleUtil: RoleUtilitiesService,
    private cdr: ChangeDetectorRef,
    private notification: NotificationService,
    private translate: TranslateService,
    private store: Store<AppState>,
  ) {}

  ngOnInit() {
    this.space$.subscribe(space => {
      this.getPermission(space);
    });
  }

  async getPermission(space: Space) {
    [
      this.permissions.canAbort,
      this.permissions.canDelete,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_pipelineconfig',
      {
        space_name: space.name,
      },
      ['abort', 'delete'],
    );
  }

  pipelineStatusChange(pipelineName: string) {
    this.router.navigate(['../', pipelineName], { relativeTo: this.route });
  }
}
