import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { ModeSelectComponent } from 'app/features-shared/pipeline/mode-select/mode-select.component';
import {
  PipelineApiService,
  PipelineConfig,
  PipelineParams,
} from 'app/services/api/pipeline';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { Space } from 'app/services/api/space.service';
import { AppState, getCurrentSpace } from 'app/store';
import { filterBy, getQuery, pageBy, sortBy } from 'app/utils/query-builder';
import { isEqual } from 'lodash-es';
import { combineLatest } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  pluck,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';
@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent implements OnInit {
  permissions = {
    canCreate: false,
    canUpdate: false,
    canDelete: false,
    canTrigger: false,
  };
  currentPageSize = 20;
  currentSpace: string;
  space$ = this.store.select(getCurrentSpace).pipe(filter(s => !!s));
  category$ = this.route.paramMap.pipe(
    map(param => param.get('category')),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );
  params$ = combineLatest(this.category$, this.route.queryParamMap).pipe(
    map(([category, queryParams]) => {
      const params = getQuery(
        filterBy(
          queryParams.get('search_by') === 'display_name'
            ? 'displayName'
            : 'name',
          queryParams.get('keywords'),
        ),
        category && category !== 'all'
          ? filterBy('label', `category:${category}`)
          : filterBy('', ''),
        pageBy(
          +(queryParams.get('page') || '1') - 1,
          +(queryParams.get('page_size') || 20),
        ),
        sortBy(
          queryParams.get('sort'),
          queryParams.get('direction') === 'desc',
        ),
      );
      return params;
    }),
    distinctUntilChanged(isEqual),
    publishReplay(1),
    refCount(),
  );

  pageIndex$ = this.params$.pipe(
    pluck('page'),
    map(page => +page),
    publishReplay(1),
    refCount(),
  );

  itemsPerPage$ = this.params$.pipe(
    pluck('itemsPerPage'),
    map(pageSize => +pageSize),
    tap(size => (this.currentPageSize = size)),
    publishReplay(1),
    refCount(),
  );

  keywords$ = this.route.queryParamMap.pipe(
    tap(params => {
      this.searchBy =
        params.get('search_by') === 'display_name' ? 'display_name' : 'name';
      this.cdr.markForCheck();
    }),
    map(params => params.get('keywords')),
    publishReplay(1),
    refCount(),
  );

  searchBy = 'name';

  findPipelines = (params: PipelineParams) =>
    this.space$.pipe(
      switchMap((space: Space) =>
        this.pipelineApi.findPipelineConfigs(space.name || '', params),
      ),
    );

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private pipelineApi: PipelineApiService,
    private roleUtil: RoleUtilitiesService,
    private cdr: ChangeDetectorRef,
    private dialog: DialogService,
    private store: Store<AppState>,
  ) {}

  ngOnInit() {
    this.space$.subscribe(space => {
      this.getPermission(space);
    });
  }

  async getPermission(space: Space) {
    [
      this.permissions.canCreate,
      this.permissions.canUpdate,
      this.permissions.canDelete,
      this.permissions.canTrigger,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_pipelineconfig',
      {
        space_name: space.name,
      },
      ['create', 'update', 'delete', 'trigger'],
    );
  }

  search(keywords: string) {
    this.router.navigate([], {
      queryParams: { keywords, page: 1, search_by: this.searchBy },
      queryParamsHandling: 'merge',
    });
  }

  pageChange(event: { pageIndex: number; pageSize: number }) {
    let index = event.pageIndex;
    if (this.currentPageSize !== event.pageSize) {
      index = 0;
    }
    this.router.navigate([], {
      queryParams: { page: index + 1, page_size: event.pageSize },
      queryParamsHandling: 'merge',
    });
  }

  sortByChanged(event: { active: string; direction: string }) {
    this.router.navigate(['./'], {
      relativeTo: this.route,
      queryParams: {
        sort: event.active,
        direction: event.direction,
      },
    });
  }

  onPipelineStarted(pipeline: PipelineConfig) {
    this.router.navigate(['./', pipeline.name], { relativeTo: this.route });
  }

  modeSelect() {
    const category = this.route.snapshot.paramMap.get('category');
    const project = this.route.snapshot.paramMap.get('project');
    if (category !== 'all') {
      return this.router.navigate(
        ['/workspace', project, 'pipelines', category, 'create'],
        {
          queryParams: {
            method: 'template',
          },
        },
      );
    }
    this.dialog.open(ModeSelectComponent, {
      size: DialogSize.Medium,
      data: {
        namespace: this.route.snapshot.paramMap.get('project'),
        category: category,
        url: this.router.url,
      },
    });
  }
}
