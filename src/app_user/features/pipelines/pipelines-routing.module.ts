import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { PipelineCreateComponent } from './create/pipeline-create.component';
import { PipelineDetailComponent } from './detail/pipeline-detail.component';
import { PipelineHistoryDetailComponent } from './history-detail/history-detail.component';
import { ListPageComponent } from './list/list-page.component';
import { PipelineUpdateComponent } from './update/pipeline-update.component';

const routes: Routes = [
  {
    path: '',
    redirectTo: 'all',
    pathMatch: 'full',
  },
  { path: ':category', component: ListPageComponent },
  { path: ':category/create', component: PipelineCreateComponent },
  { path: ':category/:name/update', component: PipelineUpdateComponent },
  { path: ':category/:name', component: PipelineDetailComponent },
  {
    path: ':category/:name/:historyName',
    component: PipelineHistoryDetailComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PipelinesRoutingModule {}
