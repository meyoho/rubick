import { ChangeDetectionStrategy, Component, HostBinding } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Store } from '@ngrx/store';
import {
  PipelineApiService,
  PipelineConfigModel,
  PipelineTemplate,
  TemplateArgumentField,
} from 'app/services/api/pipeline';
import { Space } from 'app/services/api/space.service';
import { AppState, getCurrentSpace } from 'app/store';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { find, get } from 'lodash-es';
import { of } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
  tap,
} from 'rxjs/operators';

@Component({
  selector: 'rc-pipeline-update',
  templateUrl: './pipeline-update.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PipelineUpdateComponent {
  namespace: string;
  name: string;
  type: 'upgrade' | '';
  project: string;
  space$ = this.store.select(getCurrentSpace).pipe(filter(s => !!s));
  param$ = this.route.paramMap.pipe(
    map((params: ParamMap) => {
      return {
        namespace: params.get('project'),
        name: params.get('name') || '',
      };
    }),
    tap(params => {
      this.namespace = params.namespace;
      this.name = params.name;
    }),
    distinctUntilChanged(),
    publishReplay(1),
    refCount(),
  );

  @HostBinding('class.pipeline-update')
  get cls() {
    return true;
  }

  constructor(
    private route: ActivatedRoute,
    private pipelineApi: PipelineApiService,
    private store: Store<AppState>,
    private workspace: WorkspaceComponent,
  ) {
    this.project = this.workspace.baseParamsSnapshot.project;
    this.type = get(this.route.snapshot.queryParams, 'type', '');
  }

  getPipelineConfig = () =>
    this.space$.pipe(
      switchMap((space: Space) =>
        this.pipelineApi
          .getPipelineConfigToModel(space.name || '', this.name)
          .pipe(
            switchMap((pipelineconfig: PipelineConfigModel) => {
              if (this.type === 'upgrade') {
                const template = get(pipelineconfig, 'template');
                return this.pipelineApi
                  .clusterTemplateDetail(template.name)
                  .pipe(
                    map((target: PipelineTemplate) =>
                      this.toNewPipelineConfig(pipelineconfig, target),
                    ),
                  );
              } else {
                return of(pipelineconfig);
              }
            }),
          ),
      ),
    );

  private toNewPipelineConfig(
    pipelineconfig: PipelineConfigModel,
    targetTemplate: PipelineTemplate,
  ) {
    const originItems: any = pipelineconfig.template.arguments.reduce(
      (acc, arg: TemplateArgumentField) => {
        return [...acc, ...arg.items];
      },
      [],
    );
    targetTemplate.arguments.forEach((args: TemplateArgumentField) => {
      args.items.forEach(item => {
        item.value = get(find(originItems, { name: item.name }), 'value', '');
      });
    });
    return {
      ...pipelineconfig,
      ...{ template: targetTemplate },
    };
  }
}
