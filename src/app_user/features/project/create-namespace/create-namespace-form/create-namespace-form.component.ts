import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import {
  ConfigurationApi,
  EnvironmentService,
} from 'app/services/api/environment.service';
import {
  NewCluster,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { RegionService } from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { QuotaUnit } from 'app/typings/quota.types';
import { find, get, isEmpty } from 'lodash-es';
import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { QuotaRange } from '../../types';
import {
  ClustersQuota,
  MixinQuota,
} from '../quota-setting/multiple-quota-settings.component';

interface NamespaceFormModel {
  quotas?: {}[];
  name: string;
  cluster: NewCluster;
}

@Component({
  selector: 'rc-app-create-namespace-form',
  templateUrl: './create-namespace-form.component.html',
  styleUrls: ['./create-namespace-form.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class CreateNamespaceFormComponent extends BaseResourceFormGroupComponent<
  NamespaceFormModel
> {
  selectClusterNames: string[] = [];
  globalQuotaConfigs: ConfigurationApi[] = [];
  currStep = 1;
  steps: string[];
  clusters: NewCluster[] = [];
  clustersQuota: {
    name: string;
    display_name: string;
    uuid?: string;
    origin_display_name?: string;
    quota?: QuotaRange;
    regions?: NewCluster[];
  }[];
  projectClustersQuotas: NewCluster[];
  loading: boolean;
  namespaceUuid = '';
  namePrefix = '';
  clusterQuotaKeys: string[] = [];
  _project: Project;
  quotas: any[] = [];

  @Input()
  set project(v: Project) {
    this._project = v;
    if (v) {
      this.init();
    }
  }
  get project() {
    return this._project;
  }

  constructor(
    injector: Injector,
    private translate: TranslateService,
    private projectService: ProjectService,
    private environmentService: EnvironmentService,
    private regionService: RegionService,
    private errorToast: ErrorsToastService,
  ) {
    super(injector);
  }

  get nameControl() {
    return this.form.get('name');
  }

  get clusterControl() {
    return this.form.get('cluster');
  }

  async init() {
    try {
      const [regions, quotaConfigs] = await Promise.all([
        this.regionService.getRegions(),
        this.environmentService.getConfigurations('quota'),
      ]);
      this.globalQuotaConfigs = quotaConfigs;
      const {
        clusters: projectClustersQuotas,
      } = await this.projectService.getProjectClustersQuota(this.project.name);
      this.projectClustersQuotas = projectClustersQuotas;
      this.clusterQuotaKeys = quotaConfigs.map(v => v.name);
      this.namePrefix =
        this.project.name && `${this.project.name.replace(/[\._]/g, '-')}-`;
      this.clusters = [];
      if (this.project.clusters) {
        this.project.clusters.forEach(
          (cluster: {
            name: string;
            display_name: string;
            uuid: string;
            quota?: QuotaRange | MixinQuota;
          }) => {
            const clusterDetail = find(regions, { name: cluster.name });
            if (
              clusterDetail &&
              !isEmpty(clusterDetail.mirror) &&
              !find(this.clusters, { name: clusterDetail.mirror.name })
            ) {
              this.clusters.push({
                name: clusterDetail.mirror.name,
                uuid: clusterDetail.id,
                origin_display_name: clusterDetail.mirror.display_name,
                display_name: `${clusterDetail.mirror.display_name}`,
                flag: clusterDetail.mirror.flag,
                regions: clusterDetail.mirror.regions,
              });
            }
            if (
              clusterDetail &&
              isEmpty(clusterDetail.mirror) &&
              !find(this.clusters, { name: cluster.name })
            ) {
              cluster.quota = get(
                find(projectClustersQuotas, { name: cluster.name }),
                'quota',
              );
              this.clusters.push(cluster as NewCluster);
            }
          },
        );
      }
      this.cdr.detectChanges();
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.loading = false;
    }
  }

  createForm() {
    return this.fb.group({
      name: ['', [Validators.required]],
      cluster: [],
      quotas: [],
    });
  }

  getDefaultFormModel(): NamespaceFormModel {
    return {
      name: '',
      cluster: null,
    };
  }

  clusterQuotaDisplay(cluster: NewCluster) {
    if (cluster.regions) {
      return `${this.translate.get('containe')}${cluster.regions
        .map(v => v.display_name)
        .join('、')}`;
    } else if (cluster.quota) {
      return this.globalQuotaConfigs.reduce((total: any, curr, index) => {
        const max = this.getMaxMum(curr.name, cluster);
        return `${index === 0 ? `${total} ` : `${total}, `}${this.translate.get(
          curr.name,
        )} ${
          max === -1 ? this.translate.get('unlimited') : max
        }${this.translate.get(QuotaUnit[curr.name])}`;
      }, this.translate.get('can_assign'));
    }
    return '';
  }

  getMaxMum(type: string, cluster?: ClustersQuota) {
    const max = get(cluster, `quota[${type}].max`, 0);
    return max === -1 ? -1 : max - get(cluster, `quota[${type}].used`, 0);
  }

  async selectChange(cluster: NewCluster) {
    if (!cluster) {
      this.clustersQuota = null;
      return;
    }
    const isMirror = !!cluster.regions;
    const clusterIDArr = isMirror
      ? cluster.regions.map(c => c.id)
      : [cluster.uuid || cluster.name];
    this.selectClusterNames = isMirror
      ? cluster.regions.map(c => c.name)
      : [cluster.name];
    const filterClusters = this.projectClustersQuotas.filter(cluster =>
      clusterIDArr.includes(cluster.uuid),
    );
    this.clustersQuota = isMirror
      ? [
          {
            name: cluster.name,
            origin_display_name: cluster.origin_display_name,
            display_name: cluster.display_name,
            regions: filterClusters,
          },
        ]
      : filterClusters;
    this.cdr.detectChanges();
  }
}
