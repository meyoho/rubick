import { DialogService } from '@alauda/ui';
import { Component, Inject, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { mapKeys, mapValues, pick } from 'lodash-es';

import { MessageService, NotificationService } from '@alauda/ui';
import { Store } from '@ngrx/store';
import { EnvironmentService } from 'app/services/api/environment.service';
import { NamespaceService } from 'app/services/api/namespace.service';
import { NewCluster, Project } from 'app/services/api/project.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { RouterUtilService } from 'app/services/router-util.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import * as fromStore from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';
import {
  ClustersQuota,
  PodQuota,
  limitsRangeKeys,
} from 'app_user/features/project/create-namespace/quota-setting/multiple-quota-settings.component';
import { QuotaConfigEnum, QuotaRange } from 'app_user/features/project/types';
import { filter, take } from 'rxjs/operators';
import { RcAccount } from 'src/landing/app/types';

const maxStep = 2;

interface CustomClustersQuota {
  name: string;
  display_name: string;
  uuid?: string;
  origin_display_name?: string;
  quota?: QuotaRange;
  regions?: NewCluster[];
}
export interface CustomNamespaceSetting {
  quotas: CustomClustersQuota[];
  name: string;
  cluster: NewCluster;
}

@Component({
  templateUrl: 'create-namespace.component.html',
  styleUrls: ['create-namespace.component.scss'],
})
export class CreateNamespaceComponent implements OnInit {
  currStep = 1;
  steps: string[];
  createStatus: null | 'ing' | 'wrong';
  deleteLoading = false;
  namespace: CustomNamespaceSetting = {
    name: '',
    cluster: null,
    quotas: [],
  };
  clusters: NewCluster[];
  clustersQuota: CustomClustersQuota[];
  projectClustersQuotas: NewCluster[];
  loading: boolean;
  namespaceUuid = '';
  project: Project;
  isNsExistError = false;
  projectNamePrefix = '';
  canAccountDisplay: boolean;
  clusterQuotaKeys: string[] = [];

  @ViewChild('form')
  form: NgForm;

  constructor(
    private translate: TranslateService,
    private errorToast: ErrorsToastService,
    private auiMessageService: MessageService,
    private auiNotificationService: NotificationService,
    private router: Router,
    private namespaceService: NamespaceService,
    private dialogService: DialogService,
    private store: Store<fromStore.AppState>,
    private environmentService: EnvironmentService,
    private routerUtil: RouterUtilService,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ACCOUNT) public account: RcAccount,
  ) {}

  async ngOnInit() {
    this.loading = true;
    try {
      this.steps = ['create_namespace', 'add_member'];
      this.canAccountDisplay = !this.weblabs.GROUP_ENABLED;
      const [project, quotaConfigs] = await Promise.all([
        this.store
          .select(fromStore.getCurrentProjectDetail)
          .pipe(
            filter(project => !!project),
            take(1),
          )
          .toPromise(),
        this.environmentService.getConfigurations('quota'),
      ]);
      this.projectNamePrefix =
        project.name && `${project.name.replace(/[\._]/g, '-')}-`;
      this.clusterQuotaKeys = quotaConfigs.map(v => v.name);
      this.project = project;
      this.loading = false;
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.loading = false;
    }
  }

  async create() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    this.createStatus = 'ing';
    this.clustersQuota = this.namespace.quotas;
    const reqData = this.namespace.quotas.map((clusterQuota: ClustersQuota) => {
      //MOCK FOR ZYRF
      const MOCK_QUOTA_KEYS = ['cpu', 'memory', 'storage', 'pods', 'pvc_num'];
      if (!this.clusterQuotaKeys.includes('pods')) {
        clusterQuota.quota['pods'] = 1000;
      }
      if (!this.clusterQuotaKeys.includes('pvc_num')) {
        clusterQuota.quota['pvc_num'] = 1000;
      }

      return {
        clusterName: clusterQuota.name,
        clusterDisplayName: clusterQuota.display_name,
        namespace: this.projectNamePrefix + this.namespace.name,
        namespaceLabels: clusterQuota.labels,
        namespaceAnnotations: clusterQuota.annotations,
        project: this.project,
        quotaConfig: mapValues(
          mapKeys(
            pick(clusterQuota.quota, MOCK_QUOTA_KEYS),
            (_v, k) => QuotaConfigEnum[k],
          ),
          v => String(v),
        ),
        podQuotaConfig: pick(clusterQuota.quota, limitsRangeKeys) as PodQuota,
      };
    });

    try {
      const batchRes = await this.namespaceService.createNamespaceBatch(
        reqData,
      );
      let successNum = 0;
      if (batchRes.responses) {
        const values = Object.entries(batchRes.responses);
        values.forEach(([key, value]) => {
          if ((value.code + '').startsWith('2')) {
            successNum += 1;
            this.auiNotificationService.success({
              title: this.translate.get('create_namespace_success'),
              content: this.translate.get('cluster_namespace_create_success', {
                clusterDisplayName: key,
              }),
            });
          } else {
            if (
              JSON.parse(value.body).errors[0].code ===
              'namespace_already_exist'
            ) {
              this.isNsExistError = true;
            }
            this.auiNotificationService.error({
              title: this.translate.get(JSON.parse(value.body).errors[0].code),
              content:
                this.translate.get('cluster_namespace_create_error', {
                  clusterDisplayName: key,
                }) +
                this.translate.get(JSON.parse(value.body).errors[0].message),
            });
          }
        });
        if (successNum === values.length) {
          const clusterName = this.clustersQuota[0].regions
            ? this.clustersQuota[0].regions[0].name
            : this.clustersQuota[0].name;
          const namespace = await this.namespaceService.getNamespace(
            clusterName,
            this.projectNamePrefix + this.namespace.name,
          );
          this.namespaceUuid = `${this.account.namespace}:${clusterName}:${
            namespace.kubernetes.metadata.name
          }`;
          this.currStep = Math.max(this.currStep + 1, maxStep);
        } else if (this.isNsExistError) {
          this.createStatus = null;
          this.namespace.cluster = null;
        } else {
          this.createStatus = 'wrong';
        }
      } else {
        this.createStatus = null;
        this.namespace.cluster = null;
        this.errorToast.error(batchRes);
      }
    } catch (e) {
      this.namespace.cluster = null;
      this.createStatus = null;
      this.errorToast.error(e);
    }
  }

  async delete() {
    this.deleteLoading = true;
    const reqData = this.clustersQuota[0].regions
      ? this.clustersQuota[0].regions.map(cluster => ({
          clusterName: cluster.name,
          clusterDisplayName: cluster.display_name,
          namespace: this.projectNamePrefix + this.namespace.name,
          projectName: this.project.name,
        }))
      : [
          {
            projectName: this.project.name,
            clusterName: this.clustersQuota[0].name,
            clusterDisplayName: this.clustersQuota[0].display_name,
            namespace: this.projectNamePrefix + this.namespace.name,
          },
        ];
    try {
      const batchRes = await this.namespaceService.deleteNamespaceBatch(
        reqData,
      );
      let successNum = 0;
      if (batchRes.responses) {
        const values = Object.entries(batchRes.responses);
        values.forEach(([key, value]) => {
          if ((value.code + '').startsWith('2')) {
            successNum += 1;
          } else {
            this.auiNotificationService.error({
              title: this.translate.get(JSON.parse(value.body).errors[0].code),
              content:
                this.translate.get('cluster_namespace_delete_error', {
                  clusterDisplayName: key,
                }) +
                this.translate.get(JSON.parse(value.body).errors[0].message),
            });
          }
        });
        if (successNum === values.length) {
          this.auiMessageService.success({
            content: this.translate.get('delete_success'),
          });
          this.router.navigate([
            '/console/user/project',
            { project: this.project.name },
          ]);
        } else {
          this.dialogService.confirm({
            title: this.translate.get('delete_namespace'),
            content: this.translate.get('create_namespace_tip5'),
            confirmText: this.translate.get('got_it'),
            cancelButton: false,
            beforeConfirm: reslove => {
              this.router.navigate([
                '/console/user/project',
                { project: this.project.name },
              ]);
              reslove();
            },
          });
        }
      }
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.deleteLoading = false;
    }
  }

  complete() {
    if (this.clustersQuota[0].regions) {
      this.router.navigate([
        '/console/user/project',
        { project: this.project.name },
      ]);
    } else {
      this.router.navigate([
        '/console/user/project',
        {
          project: this.project.name,
        },
        'namespace',
        'detail',
        this.clustersQuota[0].name,
        this.projectNamePrefix + this.namespace.name,
      ]);
    }
  }

  get selectClusterNames() {
    const regions = this.namespace.cluster.regions;
    return regions
      ? regions.map(v => v.name).join(',')
      : this.namespace.cluster.name;
  }

  cancel() {
    this.routerUtil.goBack();
  }
}
