import {
  Component,
  ElementRef,
  forwardRef,
  Input,
  OnInit,
} from '@angular/core';
import {
  AbstractControl,
  ControlValueAccessor,
  NG_VALIDATORS,
  NG_VALUE_ACCESSOR,
  NgForm,
  ValidationErrors,
  Validator,
  Validators,
} from '@angular/forms';

import { get, isEmpty, isEqual, map } from 'lodash-es';

import {
  EnvironmentService,
  QUOTA_TYPES,
} from 'app/services/api/environment.service';
import { NewCluster } from 'app/services/api/project.service';
import { OverCommit, RegionService } from 'app/services/api/region.service';
import { TranslateService } from 'app/translate/translate.service';
import { StringMap } from 'app/typings/raw-k8s';
import {
  K8S_RESOURCE_LABEL_KEY_DOMAIN_PATTERN,
  K8S_RESOURCE_LABEL_KEY_NAME_PATTERN,
  K8S_RESOURCE_LABEL_VALUE_PATTERN,
} from 'app/utils/patterns';
import { k8sResourceLabelKeyValidator } from 'app/utils/validator';
import { QuotaRange } from 'app_user/features/project/types';

export interface PodQuota {
  pod_default_cpu?: string;
  pod_default_request_cpu?: string;
  pod_default_memory?: string;
  pod_default_request_memory?: string;
  pod_max_cpu?: string;
  pod_max_memory?: string;
}
export interface MixinQuota extends PodQuota {
  cpu?: number;
  memory?: number | string;
  storage?: number | string;
  pvc_num?: number;
  pods?: number;
}

export interface ClustersQuota {
  name: string;
  display_name: string;
  origin_display_name?: string;
  labels?: StringMap;
  annotations?: StringMap;
  quota?: QuotaRange | MixinQuota | null;
  regions?: {
    name: string;
    display_name: string;
    quota?: QuotaRange;
  }[];
}

// export const clusterQuotaKeys = ['cpu', 'memory', 'storage', 'pvc_num', 'pods'];
export const limitsRangeKeys = [
  'pod_default_cpu',
  'pod_default_memory',
  'pod_default_request_cpu',
  'pod_default_request_memory',
  'pod_max_cpu',
  'pod_max_memory',
];

@Component({
  selector: 'rc-multiple-quota-settings',
  templateUrl: './multiple-quota-settings.component.html',
  styleUrls: ['./multiple-quota-settings.component.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => MultipleQuotaSettingsComponent),
      multi: true,
    },
    {
      provide: NG_VALIDATORS,
      useExisting: forwardRef(() => MultipleQuotaSettingsComponent),
      multi: true,
    },
  ],
})
export class MultipleQuotaSettingsComponent
  implements OnInit, ControlValueAccessor, Validator {
  useSameQuota = false;
  canUseSameQuota = false;
  resourceQuota: boolean[] = [];
  podQuota: boolean[] = [];
  moreConfig: boolean[] = [];
  resourceRadio: {
    [key: string]: OverCommit;
  } = {};
  propagateChange = (_: any) => {};
  onTouched: () => void;
  quota: MixinQuota = {};
  disabled: boolean;
  _validator: any;
  initialized: boolean;
  _clustersQuota: ClustersQuota[];
  isMirror: boolean;
  originClustersQuota: ClustersQuota[];
  resultClusters: ClustersQuota[];
  podQuotaFormFileds: any[] = [];
  labelValidator = {
    key: [k8sResourceLabelKeyValidator],
    value: [Validators.pattern(K8S_RESOURCE_LABEL_VALUE_PATTERN.pattern)],
  };
  labelErrorMapper = {
    key: {
      pattern: this.translate.get('invalid_pattern'),
      namePattern: this.translate.get(K8S_RESOURCE_LABEL_KEY_NAME_PATTERN.tip),
      domainPattern: this.translate.get(
        K8S_RESOURCE_LABEL_KEY_DOMAIN_PATTERN.tip,
      ),
    },
    value: {
      pattern: this.translate.get(K8S_RESOURCE_LABEL_VALUE_PATTERN.tip),
    },
  };

  @Input()
  update: boolean;
  @Input()
  clusterQuotaKeys: string[] = [];
  @Input()
  minQuotaRanges: { [clusterName: string]: MixinQuota } = {};
  @Input()
  hardQuotaRanges: { [clusterName: string]: MixinQuota } = {};
  @Input()
  get clustersQuota() {
    return this._clustersQuota;
  }
  set clustersQuota(value: ClustersQuota[]) {
    this.initialized = false;
    this.originClustersQuota = value;
    this.isMirror = !!(value && value[0].regions);
    if (this.isMirror) {
      this.setUseSameQuota(value[0].regions)
        .then(res => {
          this.useSameQuota = res;
          this.canUseSameQuota = this.useSameQuota;
          this.refreshClustersQuota();
        })
        .catch(_err => this.handleError(value));
    } else {
      this.useSameQuota = false;
      this.regionService
        .getOverCommit(value[0].name)
        .then(res => {
          this.resourceRadio[value[0].name] = res;
          this.refreshClustersQuota();
        })
        .catch(_err => this.handleError(value));
    }
  }

  private handleError(clustersQuota: ClustersQuota[]) {
    this.resourceRadio = this.isMirror
      ? clustersQuota[0].regions.reduce((acc, cur) => {
          return {
            ...acc,
            [cur.name]: {
              cpu: 0,
              mem: 0,
            },
          };
        }, {})
      : {
          [clustersQuota[0].name]: {
            cpu: 0,
            mem: 0,
          },
        };
    this.useSameQuota = false;
    this.refreshClustersQuota();
  }

  /**
   * 联邦集群，所有集群超售比完全相同才默认选中设置相同参数
   */
  private async setUseSameQuota(regions: NewCluster[]): Promise<boolean> {
    let same = true;
    let expectation: OverCommit = null;
    const allResourceRadios: OverCommit[] = await Promise.all(
      regions.map(async r => {
        const res = await this.regionService
          .getOverCommit(r.name)
          .catch(() => ({
            cpu: 0,
            mem: 0,
          }));
        this.resourceRadio[r.name] = res;
        return res;
      }),
    );
    allResourceRadios.some((rr: OverCommit, i: Number) => {
      if (i === 0) {
        expectation = rr;
        return false;
      }
      if (!isEqual(rr, expectation)) {
        same = false;
        return true;
      }
    });
    return same;
  }

  async ngOnInit() {
    this.parentForm.ngSubmit.subscribe(() => this.change());
    this.podQuotaFormFileds = [
      {
        label: 'CPU',
        items: [
          {
            name: 'pod_default_cpu',
            label: 'namesapce_quota_default',
            after: 'm',
          },
          {
            name: 'pod_default_request_cpu',
            label: 'namesapce_quota_default_request',
            after: 'm',
          },
          {
            name: 'pod_max_cpu',
            label: 'maximum',
            after: 'unit_core',
          },
        ],
      },
      {
        label: 'memory',
        items: [
          {
            name: 'pod_default_memory',
            label: 'namesapce_quota_default',
            after: 'unit_mb',
          },
          {
            name: 'pod_default_request_memory',
            label: 'namesapce_quota_default_request',
            after: 'unit_mb',
          },
          {
            name: 'pod_max_memory',
            label: 'maximum',
            after: 'unit_gb',
          },
        ],
      },
    ];
  }

  showContainerQuotaFormItem(
    item: { name: string; label: string; after: string },
    cluster: NewCluster,
  ): boolean {
    if (this.isMirror && this.useSameQuota) {
      if (item.name === 'pod_default_request_cpu') {
        return this.resourceRadio[Object.keys(this.resourceRadio)[0]].cpu === 0;
      } else if (item.name === 'pod_default_request_memory') {
        return this.resourceRadio[Object.keys(this.resourceRadio)[0]].mem === 0;
      } else {
        return true;
      }
    } else {
      if (item.name === 'pod_default_request_cpu') {
        return this.resourceRadio[cluster.name].cpu === 0;
      } else if (item.name === 'pod_default_request_memory') {
        return this.resourceRadio[cluster.name].mem === 0;
      } else {
        return true;
      }
    }
  }

  refreshClustersQuota() {
    this.initialized = true;
    if (this.useSameQuota && this.isMirror) {
      this._clustersQuota = [
        Object.assign({}, this.originClustersQuota[0], {
          quota: this.CalculateQuotaRange(this.originClustersQuota[0].regions),
        }),
      ];
    } else if (this.isMirror) {
      this._clustersQuota = this.originClustersQuota[0].regions;
    } else {
      this._clustersQuota = this.originClustersQuota;
    }

    const defaultQuota = this.environmentService.getDefaultQuota('namespace');

    this.resultClusters = map(
      this._clustersQuota,
      (clusterQuota: ClustersQuota) => {
        const newQuota = { quota: {} };
        if (!isEmpty(this.hardQuotaRanges)) {
          QUOTA_TYPES.forEach(type => {
            if (this.useSameQuota && this.isMirror) {
              newQuota.quota[type] = Math.max(
                ...Object.values(this.hardQuotaRanges).map(v => v[type]),
              );
            } else {
              newQuota.quota[type] = get(
                this.hardQuotaRanges[clusterQuota.name],
                type,
                this.getMiniNum(type, clusterQuota.name),
              );
            }
          });
        } else if (!!defaultQuota) {
          QUOTA_TYPES.forEach(type => {
            newQuota.quota[type] = clusterQuota.quota
              ? Math.min(
                  defaultQuota[type] ||
                    this.getMiniNum(type, clusterQuota.name),
                  clusterQuota.quota[type]['max'] -
                    clusterQuota.quota[type]['used'],
                )
              : defaultQuota[type] || this.getMiniNum(type, clusterQuota.name);
          });
        } else {
          QUOTA_TYPES.forEach(type => {
            newQuota.quota[type] = this.getMiniNum(type, clusterQuota.name);
          });
        }

        limitsRangeKeys.forEach(k => {
          if (['pod_max_cpu', 'pod_max_memory'].includes(k)) {
            newQuota.quota[k] = clusterQuota.quota[k] || this.getMiniNum(k);
          } else {
            newQuota.quota[k] =
              clusterQuota.quota[k] * 1000 || this.getMiniNum(k);
          }
        });

        return Object.assign({}, clusterQuota, newQuota);
      },
    );
    this.change();
  }
  //get quota's range according to specific name
  getQuotaRange(name: string, ind: number) {
    switch (name) {
      case 'pod_default_request_cpu': {
        const maxVal = this.resultClusters[ind].quota['pod_default_cpu'];
        return {
          max: maxVal,
          min: 1,
        };
      }

      case 'pod_default_request_memory': {
        const maxVal = this.resultClusters[ind].quota['pod_default_memory'];
        return {
          max: maxVal,
          min: 1,
        };
      }
      case 'pod_max_cpu': {
        const defaultCpu =
          this.resultClusters[ind].quota['pod_default_cpu'] / 1000;
        return {
          min: defaultCpu,
          max: this.resultClusters[ind].quota.cpu,
        };
      }
      case 'pod_max_memory': {
        const defaultCpu =
          this.resultClusters[ind].quota['pod_default_memory'] / 1024;
        return {
          min: defaultCpu,
          max: this.resultClusters[ind].quota.memory,
        };
      }
      default:
        return {
          max: '',
          min: 1,
        };
    }
  }

  getErrorTip(type: string, itemName: string): string {
    //handle with pod_default_request_***, pod_max_***
    if (itemName.startsWith('pod_max_')) {
      if (type === 'min') {
        return 'less_than_quota_default';
      } else if (type === 'max') {
        return 'greater_than_resource_quota';
      }
    } else if (itemName.startsWith('pod_default_request_')) {
      if (type === 'max') {
        return 'greater_than_quota_default';
      }
    }
    return '';
  }

  getMiniNum(type: string, clusterId?: string) {
    let defaultMinNumb: Number;
    const rules: { rule: string[]; value: Number }[] = [
      {
        rule: ['cpu', 'memory', 'pods'],
        value: 1,
      },
      {
        rule: ['pod_default_cpu', 'pod_default_request_cpu'],
        value: 500,
      },
      {
        rule: ['pod_default_memory', 'pod_default_request_memory'],
        value: 512,
      },
      {
        rule: ['pod_max_cpu', 'pod_max_memory'],
        value: 4,
      },
    ];

    const matched = rules.some(o => {
      if (o.rule.includes(type)) {
        defaultMinNumb = o.value;
        return true;
      }
    });
    if (!matched) {
      defaultMinNumb = 0;
    }

    if (this.useSameQuota && this.isMirror && !isEmpty(this.minQuotaRanges)) {
      return Math.max(
        ...Object.values(this.minQuotaRanges).map(
          v => v[type] || defaultMinNumb,
        ),
      );
    }
    if (!clusterId || !this.minQuotaRanges[clusterId]) {
      return defaultMinNumb;
    }

    return Math.max(
      get(this.minQuotaRanges[clusterId], type) || defaultMinNumb,
    );
  }

  getMaxMum(type: string, cluster?: ClustersQuota) {
    const max = get(cluster, `quota[${type}].max`, 0);
    return max === -1 ? null : max - cluster.quota[type].used;
  }

  useSameQuotaChange(value: any) {
    this.useSameQuota = value;
    this.refreshClustersQuota();
  }

  CalculateQuotaRange(clusters: ClustersQuota[]): any {
    let quotaRange = {},
      quotaLimited = false;

    clusters.some(cluster => {
      return (quotaLimited = !!cluster.quota);
    });
    if (!quotaLimited) {
      return (quotaRange = null);
    }

    this.clusterQuotaKeys.forEach((v: string) => {
      const maxNumArr = clusters.map(cluster => {
        if (cluster.quota && cluster.quota[v]) {
          return cluster.quota[v]['max'];
        }
        return Number.MAX_SAFE_INTEGER;
      });
      const minNumArr = clusters.map(cluster => {
        if (cluster.quota && cluster.quota[v]) {
          return cluster.quota[v]['used'];
        }
        return 0;
      });
      quotaRange[v] = {
        used: Math.max(...minNumArr),
        max: Math.min(...maxNumArr),
      };
    });

    limitsRangeKeys.forEach((v: string) => {
      quotaRange[v] = Math.max(...clusters.map(c => c.quota[v]));
    });

    return quotaRange;
  }

  change() {
    const completeClusters = this.useSameQuota
      ? this.resultClusters[0].regions.map(c => {
          return Object.assign({}, c, {
            labels: this.resultClusters[0].labels,
            annotations: this.resultClusters[0].annotations,
            quota: this.dealQuotaWithSuffix(this.resultClusters[0]
              .quota as MixinQuota),
          });
        })
      : this.resultClusters.map((c: ClustersQuota) => {
          return Object.assign({}, c, {
            quota: this.dealQuotaWithSuffix(c.quota as MixinQuota),
          });
        });
    this.propagateChange(completeClusters);
  }

  dealQuotaWithSuffix(quota: MixinQuota) {
    return Object.assign(
      {},
      quota,
      {
        memory: quota.memory + 'G',
        storage: quota.storage + 'G',
      },
      {
        pod_default_cpu: quota.pod_default_cpu + 'm',
        pod_default_request_cpu: quota.pod_default_request_cpu + 'm',
        pod_default_memory: quota.pod_default_memory + 'M',
        pod_default_request_memory: quota.pod_default_request_memory + 'M',
        pod_max_cpu: quota.pod_max_cpu + '',
        pod_max_memory: quota.pod_max_memory + 'G',
      },
    );
  }

  writeValue(): void {}
  registerOnChange(fn: any): void {
    this.propagateChange = fn;
  }
  registerOnTouched(fn: any): void {
    this.onTouched = fn;
  }
  setDisabledState?(isDisabled: boolean): void {
    this.disabled = isDisabled;
  }

  getQuotaErrorInfo(quota: QuotaRange, type: string, clusterName: string) {
    if (!quota[type]) {
      return '';
    }
    const remaining = quota[type].max - quota[type].used;
    const limitMin = this.getMiniNum(type, clusterName);
    return quota[type].max === -1
      ? this.translate.get('quota_range_tip_no_max', {
          min: limitMin,
        })
      : this.translate.get(
          remaining < limitMin ? 'quota_range_error_tip' : 'quota_range_tip',
          {
            min: limitMin,
            max: remaining,
          },
        );
  }

  getQuotaFormItemHint(quota: QuotaRange, type: string, clusterName: string) {
    if (!quota[type]) {
      return '';
    }
    const remaining = quota[type].max - quota[type].used;
    const limitMin = this.getMiniNum(type, clusterName);

    return quota[type].max === -1
      ? this.translate.get('quota_range_tip_no_max', {
          min: limitMin,
        })
      : this.translate.get('quota_range_tip', {
          min: limitMin,
          max: remaining,
        });
  }

  quotaValidator() {
    return (control: AbstractControl): ValidationErrors => {
      return this.hostElement.nativeElement.getElementsByClassName('ng-invalid')
        .length
        ? { rangeError: { current: control.value } }
        : null;
    };
  }

  validate(c: AbstractControl): ValidationErrors {
    return this.quotaValidator()(c);
  }

  registerOnValidatorChange?(_fn: () => void): void {}

  between(_v: string, _min: number, _max: number) {}

  getDisplayMirrorName(_name: string) {}

  constructor(
    private hostElement: ElementRef,
    private environmentService: EnvironmentService,
    private parentForm: NgForm,
    private translate: TranslateService,
    private regionService: RegionService,
  ) {}
}
