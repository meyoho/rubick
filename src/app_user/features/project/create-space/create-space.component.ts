import { NotificationService } from '@alauda/ui';
import { Component, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { Project, ProjectService } from 'app/services/api/project.service';
import { TemplateType } from 'app/services/api/role.service';
import { Space, SpaceService } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { RouterUtilService } from 'app/services/router-util.service';
import * as fromStore from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { cloneDeep } from 'lodash-es';
import { filter, take } from 'rxjs/operators';

export interface SpaceFormModel extends Space {
  accounts?: {
    username: string;
    role_name: string;
    role: TemplateType;
  }[];
}

export const EmptySpaceModel: SpaceFormModel = {
  name: '',
  display_name: '',
  accounts: [],
};
@Component({
  selector: 'rc-create-space',
  templateUrl: './create-space.component.html',
  styleUrls: ['./create-space.component.scss'],
})
export class CreateSpaceComponent implements OnInit {
  project: Project;
  space: SpaceFormModel = EmptySpaceModel;
  projectNamePrefix: string;
  currNamespaceUuid: string;

  @ViewChild('form')
  form: NgForm;
  loading: boolean;

  constructor(
    private spaceService: SpaceService,
    private notification: NotificationService,
    private translate: TranslateService,
    private router: Router,
    private store: Store<Project>,
    private routerUtil: RouterUtilService,
    private projectService: ProjectService,
    private activatedRoute: ActivatedRoute,
    private errorToast: ErrorsToastService, // private store: Store<fromStore.AppState>
  ) {}

  async ngOnInit() {
    this.project = await this.store
      .select(fromStore.getCurrentProjectDetail)
      .pipe(
        filter(project => !!project),
        take(1),
      )
      .toPromise();
    this.currNamespaceUuid = this.activatedRoute.snapshot.queryParams.namespace;
    this.projectNamePrefix =
      this.project.name && `${this.project.name.replace(/[\._]/g, '-')}-`;
  }

  async create() {
    try {
      this.loading = true;
      this.form.onSubmit(null);
      if (this.form.invalid) {
        return;
      }
      const sameAccount = this.markSameUserAndRole();
      if (sameAccount) {
        return this.notification.error(
          `${this.translate.get('has_same_username_role')}:${sameAccount}`,
        );
      }
      const currSpace = cloneDeep(this.space);
      currSpace.name = this.projectNamePrefix + currSpace.name;
      currSpace['users'] = currSpace.accounts.map(v => {
        return {
          template_name: v.role.name,
          username: v.username,
        };
      });
      delete currSpace.accounts;
      const { failed_users, name, uuid } = await this.spaceService.createSpace(
        currSpace,
      );
      if (failed_users && failed_users.length > 0) {
        failed_users.forEach(({ code, username }) => {
          this.notification.error(username + this.translate.get(code));
        });
      } else {
        this.notification.success(
          this.translate.get('space_create_success', {
            spaceName: this.space.name,
          }),
        );
      }
      if (this.currNamespaceUuid) {
        try {
          await this.projectService.bindSpaceWithNamespace([
            {
              namespace_uuid: this.currNamespaceUuid,
              space_uuid: uuid,
            },
          ]);
        } catch (e) {
          this.errorToast.error(e);
        }
      }
      this.router.navigate([
        '/console/user/project',
        { project: this.project.name },
        'space',
        name,
      ]);
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.loading = false;
    }
  }

  markSameUserAndRole() {
    let sameUserAndRole;
    this.space.accounts.some(account => {
      return (
        this.space.accounts.filter(
          v =>
            v.role.name === account.role.name &&
            v.username === account.username,
        ).length >= 2 &&
        !!(sameUserAndRole = `${account.username}&${account.role.name}`)
      );
    });
    return sameUserAndRole;
  }

  cancel() {
    this.routerUtil.goBack();
  }
}
