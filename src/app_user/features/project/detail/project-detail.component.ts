import { Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { Project } from 'app/services/api/project.service';
import { AppState, getCurrentProjectDetail } from 'app/store';
import { Observable } from 'rxjs';
import { filter, publishReplay, refCount } from 'rxjs/operators';

@Component({
  templateUrl: 'project-detail.component.html',
  styleUrls: ['project-detail.component.scss'],
})
export class ProjectDetailPageComponent implements OnInit {
  currentProject$: Observable<Project>;
  initialized = false;
  nav: string;
  constructor(private store: Store<AppState>) {
    this.nav = 'detail';
  }

  async ngOnInit() {
    this.currentProject$ = this.store.select(getCurrentProjectDetail).pipe(
      filter(project => !!project),
      publishReplay(1),
      refCount(),
    );
  }
}
