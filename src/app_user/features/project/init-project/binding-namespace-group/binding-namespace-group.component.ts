import { MessageService } from '@alauda/ui';
import { Component, Injector, Input } from '@angular/core';
import { Validators } from '@angular/forms';
import {
  NamespaceService,
  UserNamespace,
} from 'app/services/api/namespace.service';
import { Project, ProjectService } from 'app/services/api/project.service';
import { Space, SpaceService } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { get, remove } from 'lodash-es';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

export interface BindingNamespaceGroupModel {
  namespace: UserNamespace;
  spaceList: Space[];
}

@Component({
  selector: 'rc-app-binding-namespace-group',
  templateUrl: './binding-namespace-group.component.html',
  styleUrls: ['./binding-namespace-group.component.scss'],
})
export class BindingNamespaceGroupComponent extends BaseResourceFormArrayComponent<
  BindingNamespaceGroupModel
> {
  spaces: Space[] = [];
  namespaces: UserNamespace[] = [];
  selectedNamepace: UserNamespace;
  loading: boolean;
  _project: Project;
  selectedBindingNamespaceGroup: { [key: string]: Space[] } = {};
  trackFn = (val: any) => (val && val.name) || val;
  @Input()
  set project(v: Project) {
    this._project = v;
    if (v) {
      this.init();
    }
  }
  get project() {
    return this._project;
  }
  @Input()
  set succBindingShip(v: { [key: string]: Space[] }) {
    if (v) {
      Object.keys(v).forEach(ns => {
        this.selectedBindingNamespaceGroup[ns] = (
          this.selectedBindingNamespaceGroup[ns] || []
        ).concat(v[ns]);
      });
      this.formControls.forEach(v => {
        v.get('spaceList').reset([]);
      });
    }
  }

  constructor(
    injector: Injector,
    private spaceService: SpaceService,
    private errorToast: ErrorsToastService,
    private namespaceService: NamespaceService,
    private projectService: ProjectService,
    private messageService: MessageService,
    private translate: TranslateService,
  ) {
    super(injector);
  }

  get formControls() {
    return this.form.controls;
  }

  get selectedFormControl() {
    return this.formControls.filter(control => {
      return (
        get(control.get('namespace'), 'value.uuid') ===
        get(this.selectedNamepace, 'uuid', '')
      );
    })[0];
  }

  nsSelectedClass(ns: UserNamespace) {
    return {
      'binding-container__ns-item': true,
      'binding-container__ns-item--selected':
        get(ns, 'uuid', '') === get(this.selectedNamepace, 'uuid'),
    };
  }

  getDefaultFormModel(): BindingNamespaceGroupModel[] {
    return [];
  }

  createForm() {
    return this.fb.array([
      this.fb.group({
        namespace: [],
        spaceList: [],
      }),
    ]);
  }

  async init() {
    try {
      this.loading = true;
      [{ results: this.spaces }, this.namespaces] = await Promise.all([
        this.spaceService.getSpaces(),
        this.namespaceService.getBatchNamespaceOptions(
          this.project.clusters || [],
          this.project.name,
        ),
      ]);
      this.selectedNamepace = this.namespaces[0];
      this.namespaces.forEach(ns => {
        const selectedSpaceList = this.spaces.reduce((t, s) => {
          if (s.namespaces.some(n => n.name === ns.name)) {
            t.push(s);
          }
          return t;
        }, []);
        this.selectedBindingNamespaceGroup[ns.name] = selectedSpaceList;
        this.form.insert(
          this.formControls.length,
          this.fb.group({
            namespace: [ns, [Validators.required]],
            spaceList: [[]],
          }),
        );
      });
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.loading = false;
    }
  }

  isPrevSelected(space: Space) {
    return (get(
      this.selectedBindingNamespaceGroup,
      this.selectedNamepace.name,
      [],
    ) as Space[]).some(s => s.name === space.name);
  }

  selectNs(ns: UserNamespace) {
    this.selectedNamepace = ns;
  }

  async unbinding(space: Space) {
    try {
      await this.projectService.unbindSpaceWithNamespace([
        {
          namespace_name: this.selectedNamepace.name,
          namespace_uuid: this.selectedNamepace.uuid,
          space_name: space.name,
          space_uuid: space.uuid,
        },
      ]);
      this.messageService.success(this.translate.get('unbind_success'));
      remove(
        this.selectedBindingNamespaceGroup[this.selectedNamepace.name],
        s => {
          return s.name === space.name;
        },
      );
    } catch (e) {
      this.errorToast.error(e);
    }
  }
}
