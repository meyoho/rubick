import { DialogService, DialogSize } from '@alauda/ui';
import { ChangeDetectorRef, Component, OnInit } from '@angular/core';

import { from, of, Subject } from 'rxjs/index';
import {
  catchError,
  debounceTime,
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
  tap,
} from 'rxjs/operators';

import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { AllocateInfo } from 'app/services/api/tool-chain/tool-chain-api.types';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ToolChainSubscribeDialogComponent } from 'app_user/features/project/tool-chain/subscribe-dialog/subscribe-dialog.component';

@Component({
  selector: 'rc-init-devops-tools',
  templateUrl: './init-devops-tools.component.html',
  styleUrls: ['./init-devops-tools.component.scss'],
})
export class InitDevopsToolsComponent implements OnInit {
  constructor(
    private cdr: ChangeDetectorRef,
    private toolChainApi: ToolChainApiService,
    private errorToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private dialog: DialogService,
  ) {}
  subscribeUpdatePermission: boolean;

  selectedType = 'all';
  loading = true;
  updated$ = new Subject<{ [key: string]: string }>();

  items$ = this.updated$.pipe(startWith({})).pipe(
    debounceTime(1000),
    switchMap(() =>
      from(
        this.toolChainApi.getSubscriptions({
          order_by: '-metadata.creationTimestamp',
        }),
      ).pipe(
        catchError(e => {
          this.errorToastService.error(e);
          return of(null);
        }),
        tap(() => {
          this.loading = false;
        }),
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  toolTypes$ = from(this.toolChainApi.getToolChains()).pipe(
    map(types => types.filter(type => type.enabled)),
    tap(types => (types['subscribeMode'] = true)),
    publishReplay(1),
    refCount(),
  );

  async ngOnInit() {
    [
      this.subscribeUpdatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_toolbindingreplica',
      {},
      ['update'],
    );
    this.cdr.markForCheck();
  }

  showSubscribeDialog() {
    const dialogRef = this.dialog.open(ToolChainSubscribeDialogComponent, {
      data: {},
      size: DialogSize.Large,
    });

    dialogRef.afterClosed().subscribe((res?: any) => {
      if (res) {
        this.updated$.next();
      }
    });
  }

  afterAllocated(res?: AllocateInfo) {
    if (res) {
      this.updated$.next();
    }
  }
}
