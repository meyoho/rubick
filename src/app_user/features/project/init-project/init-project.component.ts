import { DialogService, MessageService, NotificationService } from '@alauda/ui';
import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { Store } from '@ngrx/store';
import { EnvironmentService } from 'app/services/api/environment.service';
import { NamespaceService } from 'app/services/api/namespace.service';
import { Project, ProjectService } from 'app/services/api/project.service';
import { Space, SpaceService } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { UiStateService } from 'app/services/ui-state.service';
import * as fromStore from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { cloneDeep, mapKeys, mapValues, pick } from 'lodash-es';
import { filter, take } from 'rxjs/operators';

import { CustomNamespaceSetting } from '../create-namespace/create-namespace.component';
import {
  ClustersQuota,
  PodQuota,
  limitsRangeKeys,
} from '../create-namespace/quota-setting/multiple-quota-settings.component';
import { SpaceFormModel } from '../create-space/create-space.component';
import { QuotaConfigEnum } from '../types';

import { BindingNamespaceGroupModel } from './binding-namespace-group/binding-namespace-group.component';

@Component({
  selector: 'rc-app-init-project',
  templateUrl: './init-project.component.html',
  styleUrls: ['./init-project.component.scss'],
})
export class InitProjectComponent implements OnInit, OnDestroy {
  project: Project;
  namespace: CustomNamespaceSetting = {
    name: '',
    cluster: null,
    quotas: [],
  };
  space: SpaceFormModel;
  //0:创建ns 1:创建组 2:绑定 3:初始化DevOps工具
  stage = 0;
  clusterQuotaKeys: string[] = [];
  projectNamePrefix = '';
  loading: boolean;
  bindingLoading: boolean;
  nsCreateStatus: null | 'ing' | 'wrong' | 'success';
  isNsExistError: boolean;
  nsDeleteLoading: boolean;
  nsCreating: boolean;
  successNsNum = 0;
  successGroupNum = 0;
  bindingSucceed = false;
  bindingShip: BindingNamespaceGroupModel[] = [];
  succBindingShip: { [key: string]: Space[] };
  @ViewChild('form')
  form: NgForm;

  constructor(
    private store: Store<Project>,
    private environmentService: EnvironmentService,
    private errorToast: ErrorsToastService,
    private namespaceService: NamespaceService,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    private auiMessageService: MessageService,
    private dialogService: DialogService,
    private spaceService: SpaceService,
    private projectService: ProjectService,
    private uiStateService: UiStateService,
    private router: Router,
  ) {}

  async ngOnInit() {
    this.loading = true;
    try {
      this.uiStateService.setProjectBadgeState(false);
      const [project, quotaConfigs] = await Promise.all([
        this.store
          .select(fromStore.getCurrentProjectDetail)
          .pipe(
            filter(project => !!project),
            take(1),
          )
          .toPromise(),
        this.environmentService.getConfigurations('quota'),
      ]);
      this.projectNamePrefix =
        project.name && `${project.name.replace(/[\._]/g, '-')}-`;
      this.clusterQuotaKeys = quotaConfigs.map(v => v.name);
      this.project = project;
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.loading = false;
    }
  }

  ngOnDestroy() {
    this.uiStateService.setProjectBadgeState(true);
  }

  assignStageClass(stage: number) {
    return {
      'initialization_settings__navigation-stage': true,
      'initialization_settings__navigation-stage--on': this.displayStageStatusOn(
        stage,
      ),
      'initialization_settings__navigation-stage--ing': this.stage === stage,
    };
  }

  displayStageStatusOn(stage: number) {
    return (
      this.stage > stage ||
      (stage === 1 && this.successNsNum > 0) ||
      (stage === 2 && this.successGroupNum > 0) ||
      (stage === 3 && this.bindingSucceed)
    );
  }

  redirectStage(stage: number) {
    if (
      !!this.nsCreateStatus ||
      !this.displayStageStatusOn(stage) ||
      this.stage === stage
    ) {
      return;
    }
    this.stage = stage;
    this.resetFormDataValue();
  }

  resetFormDataValue() {
    this.namespace = null;
    this.space = null;
    this.form.reset();
  }

  create(toNext = false) {
    switch (this.stage) {
      case 0:
        this.createNs(toNext);
        break;
      case 1:
        this.createGroup(toNext);
        break;
    }
  }

  async createNs(toNext = false) {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }

    this.nsCreateStatus = 'ing';
    const reqData = this.namespace.quotas.map((clusterQuota: ClustersQuota) => {
      //MOCK FOR ZYRF
      const MOCK_QUOTA_KEYS = ['cpu', 'memory', 'storage', 'pods', 'pvc_num'];
      if (!this.clusterQuotaKeys.includes('pods')) {
        clusterQuota.quota['pods'] = 1000;
      }
      if (!this.clusterQuotaKeys.includes('pvc_num')) {
        clusterQuota.quota['pvc_num'] = 1000;
      }

      return {
        clusterName: clusterQuota.name,
        clusterDisplayName: clusterQuota.display_name,
        namespace: this.projectNamePrefix + this.namespace.name,
        namespaceLabels: clusterQuota.labels,
        namespaceAnnotations: clusterQuota.annotations,
        project: this.project,
        quotaConfig: mapValues(
          mapKeys(
            pick(clusterQuota.quota, MOCK_QUOTA_KEYS),
            (_v, k) => QuotaConfigEnum[k],
          ),
          v => String(v),
        ),
        podQuotaConfig: pick(clusterQuota.quota, limitsRangeKeys) as PodQuota,
      };
    });

    try {
      const batchRes = await this.namespaceService.createNamespaceBatch(
        reqData,
      );
      let successNum = 0;
      if (batchRes.responses) {
        const values = Object.entries(batchRes.responses);
        values.forEach(([key, value]) => {
          if ((value.code + '').startsWith('2')) {
            successNum += 1;
            this.successNsNum += 1;
            this.auiMessageService.success({
              content: this.translate.get('cluster_namespace_create_success', {
                clusterDisplayName: key,
              }),
            });
          } else {
            if (
              JSON.parse(value.body).errors[0].code ===
              'namespace_already_exist'
            ) {
              this.isNsExistError = true;
            }
            this.auiNotificationService.error({
              title: this.translate.get(JSON.parse(value.body).errors[0].code),
              content:
                this.translate.get('cluster_namespace_create_error', {
                  clusterDisplayName: key,
                }) +
                this.translate.get(JSON.parse(value.body).errors[0].message),
            });
          }
        });
        if (successNum === values.length) {
          this.resetFormDataValue();
          this.nsCreateStatus = null;
          if (toNext) {
            this.stage = Math.min(this.stage + 1, 2);
          }
        } else if (this.isNsExistError) {
          this.nsCreateStatus = 'wrong';
          this.namespace.cluster = null;
        } else {
          this.nsCreateStatus = 'wrong';
        }
      } else {
        this.nsCreateStatus = null;
        this.namespace.cluster = null;
        this.errorToast.error(batchRes);
      }
    } catch (e) {
      this.namespace.cluster = null;
      this.nsCreateStatus = null;
      this.errorToast.error(e);
    }
  }

  async deleteNs() {
    this.nsDeleteLoading = true;
    const reqData = this.namespace.quotas[0].regions
      ? this.namespace.quotas[0].regions.map(cluster => ({
          clusterName: cluster.name,
          clusterDisplayName: cluster.display_name,
          namespace: this.projectNamePrefix + this.namespace.name,
          projectName: this.project.name,
        }))
      : [
          {
            projectName: this.project.name,
            clusterName: this.namespace.quotas[0].name,
            clusterDisplayName: this.namespace.quotas[0].display_name,
            namespace: this.projectNamePrefix + this.namespace.name,
          },
        ];
    try {
      const batchRes = await this.namespaceService.deleteNamespaceBatch(
        reqData,
      );
      let successNum = 0;
      if (batchRes.responses) {
        const values = Object.entries(batchRes.responses);
        values.forEach(([key, value]) => {
          if ((value.code + '').startsWith('2')) {
            successNum += 1;
          } else {
            this.auiNotificationService.error({
              title: this.translate.get(JSON.parse(value.body).errors[0].code),
              content:
                this.translate.get('cluster_namespace_delete_error', {
                  clusterDisplayName: key,
                }) +
                this.translate.get(JSON.parse(value.body).errors[0].message),
            });
          }
        });
        if (successNum === values.length) {
          this.auiMessageService.success({
            content: this.translate.get('delete_success'),
          });
          this.nsCreateStatus = null;
          this.resetFormDataValue();
        } else {
          this.dialogService.confirm({
            title: this.translate.get('delete_namespace'),
            content: this.translate.get('create_namespace_tip5'),
            confirmText: this.translate.get('got_it'),
            cancelButton: false,
            beforeConfirm: reslove => {
              this.nsCreateStatus = null;
              this.resetFormDataValue();
              reslove();
            },
          });
        }
      }
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.nsDeleteLoading = false;
    }
  }

  async createGroup(toNext = false) {
    try {
      this.nsCreateStatus = 'ing';
      this.form.onSubmit(null);
      if (this.form.invalid) {
        return;
      }
      const sameAccount = this.markSameUserAndRole();
      if (sameAccount) {
        return this.auiNotificationService.error(
          `${this.translate.get('has_same_username_role')}:${sameAccount}`,
        );
      }
      const currSpace = cloneDeep(this.space);
      currSpace.name = this.projectNamePrefix + currSpace.name;
      currSpace['users'] =
        currSpace && currSpace.accounts
          ? currSpace.accounts.map(v => {
              return {
                template_name: v.role.name,
                username: v.username,
              };
            })
          : [];
      delete currSpace.accounts;
      const { failed_users } = await this.spaceService.createSpace(currSpace);
      if (failed_users && failed_users.length > 0) {
        failed_users.forEach(({ code, username }) => {
          this.auiNotificationService.error(
            username + this.translate.get(code),
          );
        });
      } else {
        this.auiMessageService.success(
          this.translate.get('space_create_success', {
            spaceName: this.space.name,
          }),
        );
        this.successGroupNum += 1;
        this.resetFormDataValue();
        if (toNext) {
          this.stage = Math.max(this.stage + 1, 2);
        }
      }
    } catch (e) {
      this.errorToast.error(e);
    } finally {
      this.nsCreateStatus = null;
    }
  }

  async bindingGroups() {
    this.form.onSubmit(null);
    if (this.form.invalid) {
      return;
    }
    try {
      this.bindingLoading = true;
      const data = this.bindingShip.reduce((t, { namespace, spaceList }) => {
        if (spaceList.length > 0) {
          return [
            ...t,
            ...spaceList.map(s => ({
              namespace_name: namespace.name,
              namespace_uuid: namespace.uuid,
              space_name: s.name,
              space_uuid: s.uuid,
            })),
          ];
        }
        return t;
      }, []);
      await this.projectService.bindSpaceWithNamespace(data);
      this.succBindingShip = this.bindingShip.reduce(
        (t, { namespace, spaceList }) => {
          if (spaceList.length > 0) {
            t[namespace.name] = spaceList;
          }
          return t;
        },
        {},
      );
      this.bindingSucceed = true;
    } catch (e) {
      this.errorToast.error(e);
      this.bindingSucceed = false;
    } finally {
      this.bindingLoading = false;
    }
  }

  navigateToProjectOverview() {
    this.router.navigate([
      '/console/user/project',
      { project: this.project.name },
    ]);
  }

  async initDevopsTools() {
    await this.bindingGroups();
    if (this.bindingSucceed) {
      this.stage = 3;
    }
  }

  finish() {
    this.navigateToProjectOverview();
  }

  get bindingNum() {
    let num = 0;
    if (this.bindingShip) {
      this.bindingShip.forEach(({ spaceList }) => {
        num += spaceList.length;
      });
    }
    return num;
  }

  markSameUserAndRole() {
    let sameUserAndRole;
    if (this.space.accounts && this.space.accounts) {
      this.space.accounts.some(account => {
        return (
          this.space.accounts.filter(
            v =>
              v.role.name === account.role.name &&
              v.username === account.username,
          ).length >= 2 &&
          !!(sameUserAndRole = `${account.username}&${account.role.name}`)
        );
      });
    }
    return sameUserAndRole;
  }
}
