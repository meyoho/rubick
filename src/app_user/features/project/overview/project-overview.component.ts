import { DialogService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { select, Store } from '@ngrx/store';

import { get } from 'lodash-es';
import { combineLatest, merge, Observable, Subject } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  map,
  publishReplay,
  refCount,
  startWith,
  takeUntil,
  tap,
} from 'rxjs/operators';

import { RcAccount } from 'app/services/api/account.service';
import { Namespace } from 'app/services/api/namespace.service';
import {
  NamespaceQuotaWithSpace,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { Space, SpaceService } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ACCOUNT, ENVIRONMENTS, WEBLABS } from 'app/shared/tokens';
import * as fromStore from 'app/store';
import * as namespaceActions from 'app/store/actions/namespaces.actions';
import * as spaceActions from 'app/store/actions/spaces.actions';
import * as fromNamespaces from 'app/store/selectors/namespaces.selectors';
import { TranslateService } from 'app/translate/translate.service';
import { VendorCustomer } from 'app/typings';
import { Environments, Weblabs } from 'app/typings';
import { BindingSpacesDialogComponent } from 'app2/features/projects/namespace/binding.component';
import { NamespaceStatusEnum } from 'app_user/features/namespace/types';
import { ProjectComponent } from 'app_user/features/project/project.component';

interface UserNamespaceOption {
  name: string;
  uuid?: string;
  cluster_name: string;
  cluster_display_name?: string;
  status?: string;
  phase?: string;
}

interface ClusterOption {
  name: string;
  display_name: string;
  uuid: string;
}

@Component({
  templateUrl: 'project-overview.component.html',
  styleUrls: ['project-overview.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ProjectOverviewComponent implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();

  projects$: Observable<Project[]>;
  projectsLength$: Observable<number>;
  currentProject$: Observable<Project>;

  clusters$: Observable<ClusterOption[]>;
  clustersLength$: Observable<number>;
  clusterSelect$ = new Subject<ClusterOption>();

  filteredNamespaces$: Observable<UserNamespaceOption[]>;
  namespacesLength$: Observable<number>;
  namespaceLoading$: Observable<boolean>;
  search$ = new Subject<string>();

  displayProjectInitPage = false;
  enterInitPagePermission = false;

  projectName: string;
  seletedTabIndex = 0;
  initialized: boolean;
  groupEnabled: boolean;
  clusters: ClusterOption[];
  stagedNamespaceCreate = false;
  currentCluster: ClusterOption;
  createNamespaceEnabled: boolean;
  quotas: {
    [uuid: string]: any;
  } = {};
  spaces: {
    [uuid: string]: any;
  } = {};
  typeMap = {
    [NamespaceStatusEnum.PENDING]: 'primary',
    [NamespaceStatusEnum.UNKNOWN]: 'info',
  };

  illustration$ = this.translateService.currentLang$.pipe(
    map(lang => `/static/images/project/tenant_initialization_${lang}.svg`),
  );

  actionButtonCtrl = {};
  menuShow = {};

  constructor(
    private router: Router,
    public cdr: ChangeDetectorRef,
    private spaceService: SpaceService,
    private dialogService: DialogService,
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService,
    private translateService: TranslateService,
    private roleUtil: RoleUtilitiesService,
    private errorsToast: ErrorsToastService,
    private store: Store<fromStore.AppState>,
    @Inject(ACCOUNT) public account: RcAccount,
    @Inject(WEBLABS) public weblabs: Weblabs,
    @Inject(ENVIRONMENTS) private environments: Environments,
    public projectComponent: ProjectComponent,
  ) {}

  async ngOnInit() {
    this.stagedNamespaceCreate = this.environments.staged_namespace_create;
    this.groupEnabled = this.weblabs.GROUP_ENABLED;
    this.initStateParams();
    this.initProjectObservables();
    this.initClusterObservables();
    this.initNamespaceObservables();
  }

  nameCompare(ns1: UserNamespaceOption, ns2: UserNamespaceOption) {
    return ns1.name.localeCompare(ns2.name);
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  initStateParams() {
    this.seletedTabIndex = this.activatedRoute.snapshot.params['tabIndex'] || 0;
  }

  onTabIndexChange(index: number) {
    this.clusterSelect$.next();
    return this.router.navigate(['.'], {
      relativeTo: this.activatedRoute,
      queryParams: {
        ...this.activatedRoute.snapshot.queryParams,
        tabIndex: index,
      },
    });
  }

  trackByClusterName(_index: number, cluster: any) {
    return cluster.name;
  }

  trackByNamespaceUuid(_index: number, namespace: UserNamespaceOption) {
    return namespace.uuid;
  }

  clusterSelected(cluster?: ClusterOption) {
    if (
      (this.currentCluster &&
        cluster &&
        this.currentCluster.uuid === cluster.uuid) ||
      (!this.currentCluster && !cluster)
    ) {
      return;
    }
    this.clusterSelect$.next(cluster);
    this.currentCluster = cluster;
  }

  enterNamespace(namespace: UserNamespaceOption) {
    if (!this.shouldAllowEnterNamespace(namespace)) {
      return;
    }
    const spaces = this.getSpaces(namespace.uuid);
    if (spaces && spaces.length) {
      this.store.dispatch(new spaceActions.SetCurrentSpace(spaces[0].uuid));
    }
    this.router.navigate([
      '/console/user/workspace',
      {
        project: this.projectName,
        cluster: namespace.cluster_name,
        namespace: namespace.name,
      },
    ]);
  }

  getTitle(namespace: UserNamespaceOption): string {
    if (this.isTerminating(namespace)) {
      return this.translateService.get('deleting');
    }
    return this.shouldAllowEnterNamespace(namespace)
      ? this.translateService.get('enter_namespace')
      : this.translateService.get('unable_to_enter_namespace');
  }

  getNamespaceStatusDisplay(namespace: UserNamespaceOption): string {
    return namespace.status === NamespaceStatusEnum.PENDING
      ? this.translateService.get('status_pending')
      : this.translateService.get('status_unkown');
  }

  shouldAllowEnterNamespace(namespace: UserNamespaceOption): boolean {
    if (
      (this.stagedNamespaceCreate &&
        namespace.status !== NamespaceStatusEnum.READY) ||
      this.isTerminating(namespace)
    ) {
      return false;
    }
    if (this.groupEnabled) {
      const spaces = this.getSpaces(namespace.uuid);
      return spaces && !!spaces.length;
    }
    return true;
  }

  namespaceDetail(namespace: UserNamespaceOption, event: Event) {
    event.stopPropagation();
    this.router.navigate([
      '/console/user/project',
      {
        project: this.projectName,
      },
      'namespace',
      'detail',
      namespace.cluster_name,
      namespace.name,
    ]);
  }

  createNamespace() {
    if (this.stagedNamespaceCreate) {
      this.router.navigate([
        '/console/user/project',
        { project: this.projectName },
        'namespace',
        'create',
      ]);
    } else {
      this.router.navigate([
        '/console/user/project',
        { project: this.projectName },
        'create_namespace',
      ]);
    }
  }

  getQuota(uuid: string) {
    return this.quotas[uuid];
  }

  getSpaces(uuid: string) {
    return this.spaces[uuid];
  }

  isTerminating(item: UserNamespaceOption) {
    return item.phase === 'Terminating';
  }

  private initProjectObservables() {
    this.projects$ = this.store.select(fromStore.getAllProjects).pipe(
      publishReplay(1),
      refCount(),
    );
    this.projectsLength$ = this.projects$.pipe(
      map(projects => projects.length),
    );

    this.currentProject$ = this.store
      .select(fromStore.getCurrentProjectDetail)
      .pipe(
        filter(project => !!project),
        distinctUntilChanged((a, b) => {
          return a.uuid === b.uuid;
        }),
        tap((project: Project) => {
          this.clusters = project.clusters;
          this.currentCluster = null;
        }),
        tap(async (project: Project) => {
          this.createNamespaceEnabled = await this.roleUtil.resourceTypeSupportPermissions(
            'namespace',
            { project_name: project.name },
            'create',
          );
          if (this.environments.vendor_customer === VendorCustomer.ZYRF) {
            const [
              spaceCreatePermission,
              bindNamespacePermission,
            ] = await this.roleUtil.resourceTypeSupportPermissions(
              'space',
              { project_name: project.name },
              ['create', 'bind_namespace'],
            );
            this.enterInitPagePermission =
              this.createNamespaceEnabled &&
              spaceCreatePermission &&
              bindNamespacePermission;
            this.cdr.markForCheck();
          }
        }),
        publishReplay(1),
        refCount(),
      );
    this.projectComponent.baseParams
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(({ project }) => {
        this.projectName = project;
        this.getNamespaceQuotas().then((quotas: NamespaceQuotaWithSpace[]) => {
          this.displayProjectInitPage =
            this.environments.vendor_customer === VendorCustomer.ZYRF &&
            !quotas.length;
          this.cdr.detectChanges();
        });
      });
  }

  private initClusterObservables() {
    this.clusters$ = this.currentProject$.pipe(
      map((project: Project) => {
        return (project.clusters || []).filter(cluster => !!cluster.name);
      }),
      publishReplay(1),
      refCount(),
    );
    this.clustersLength$ = this.clusters$.pipe(
      map(clusters => clusters.length),
    );
    const clusterChange$ = merge(
      this.clusterSelect$.pipe(
        map(cluster => (cluster ? [cluster] : this.clusters || [])),
      ),
      this.clusters$,
    );

    clusterChange$.pipe(takeUntil(this.onDestroy$)).subscribe(clusters => {
      clusters.forEach((cluster: ClusterOption) => {
        this.store.dispatch(
          new namespaceActions.GetNamespacesByClusterAndProject({
            cluster: cluster.name,
            project: this.projectName,
          }),
        );
      });
      if (clusters.length === 1) {
        this.getNamespaceQuotas(clusters[0].uuid);
      }
    });
  }

  private initNamespaceObservables() {
    this.namespaceLoading$ = this.store.pipe(
      select(fromNamespaces.getNamespacesMappingLoading),
      publishReplay(1),
      refCount(),
    );
    const namespacesData$ = this.store.pipe(
      select(fromNamespaces.getNamespaces),
      filter(r => !!r),
    );
    const filteredNamespacesData$ = combineLatest(
      namespacesData$,
      this.currentProject$,
    ).pipe(
      map(([entities, currentProject]) => {
        if (this.projectName !== currentProject.name) {
          return [];
        }
        const cluster = this.currentCluster;
        if (!cluster) {
          return entities
            .filter(entity => {
              return entity.project === this.projectName;
            })
            .map(entity => {
              const refCluster: ClusterOption = currentProject.clusters.find(
                c => c.name === entity.cluster,
              );
              return this.getNamespaceOption(entity.namespace, refCluster);
            });
        } else {
          const refCluster: ClusterOption = currentProject.clusters.find(
            c => c.name === cluster.name,
          );
          return entities
            .filter(entity => {
              return (
                entity.project === this.projectName &&
                entity.cluster === cluster.name
              );
            })
            .map(entity =>
              this.getNamespaceOption(entity.namespace, refCluster),
            );
        }
      }),
    );
    const namespaces$: Observable<UserNamespaceOption[]> = merge(
      this.currentProject$.pipe(map(() => [])),
      this.clusterSelect$.pipe(map(() => [])),
      filteredNamespacesData$,
    ).pipe(
      publishReplay(1),
      refCount(),
    );
    this.namespacesLength$ = namespaces$.pipe(
      map(namespaces => namespaces.length),
      publishReplay(1),
      refCount(),
    );

    this.filteredNamespaces$ = combineLatest(
      namespaces$,
      this.search$.pipe(startWith('')),
    ).pipe(
      map(([namespaces, search]) =>
        search
          ? namespaces
              .filter((item: any) => item.name.includes(search.trim()))
              .sort(this.nameCompare)
          : namespaces,
      ),
      tap(() => {
        this.cdr.markForCheck();
      }),
    );
    this.projects$.pipe(takeUntil(this.onDestroy$)).subscribe(() => {
      this.initialized = true;
    });
  }

  private async getNamespaceQuotas(cluster_uuid?: string) {
    const quotasWithSpaces = await this.projectService
      .getProjectNamespaceQuotas(this.projectName, cluster_uuid)
      .catch(() => [] as NamespaceQuotaWithSpace[]);
    quotasWithSpaces.forEach((quota: NamespaceQuotaWithSpace) => {
      this.quotas[quota.namespace_uuid] = quota.quota;
      this.spaces[quota.namespace_uuid] = quota.spaces;
    });
    return quotasWithSpaces;
  }

  private getNamespaceOption(
    namespace: Namespace,
    cluster: ClusterOption,
  ): UserNamespaceOption {
    return {
      uuid: `${this.account.namespace}:${cluster.name}:${
        namespace.kubernetes.metadata.name
      }`,
      name: namespace.kubernetes.metadata.name,
      cluster_name: cluster.name,
      cluster_display_name: cluster.display_name,
      status: get(
        namespace,
        `kubernetes.metadata.labels['namespace.alauda.io/status']`,
        NamespaceStatusEnum.UNKNOWN,
      ),
      phase: get(namespace, 'kubernetes.status.phase'),
    } as UserNamespaceOption;
  }

  async bindGroup(namespace: UserNamespaceOption, event: Event) {
    event.stopPropagation();
    try {
      const { results: spaces } = await this.spaceService.getSpaces();
      const excludesSpaces = spaces.reduce((total, curr) => {
        if (
          !this.getSpaces(namespace.uuid) ||
          !this.getSpaces(namespace.uuid).some(
            (v: Space) => v.name === curr.name,
          )
        ) {
          total.push(curr);
        }
        return total;
      }, []);
      const dialogRef = this.dialogService.open(BindingSpacesDialogComponent, {
        data: {
          namepsaceName: namespace.name,
          spaces: excludesSpaces,
        },
      });
      dialogRef.afterClosed().subscribe(async v => {
        if (v && Array.isArray(v)) {
          await this.projectService.bindSpaceWithNamespace(
            v.map((space: Space) => ({
              namespace_name: namespace.name,
              namespace_uuid: namespace.uuid,
              space_name: space.name,
              space_uuid: space.uuid,
            })),
          );
          this.clusterSelect$.next();
        }
      });
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  onMenuShow(index: number) {
    this.menuShow[index] = true;
  }

  onMenuHide(index: number) {
    this.menuShow[index] = false;
  }

  onMouseEnter(index: number) {
    this.actionButtonCtrl[index] = true;
  }

  onMouseLeave(index: number) {
    if (!this.menuShow[index]) {
      this.actionButtonCtrl[index] = false;
    }
  }
}
