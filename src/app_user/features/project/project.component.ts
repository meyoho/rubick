import { TemplatePortal } from '@angular/cdk/portal';
import {
  ChangeDetectorRef,
  Component,
  Injector,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { Title } from '@angular/platform-browser';
import { ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';

import { Observable, Subject } from 'rxjs';
import { distinctUntilChanged, pluck, takeUntil } from 'rxjs/operators';

import { Project } from 'app/services/api/project.service';
import {
  TemplateHolderType,
  UiStateService,
} from 'app/services/ui-state.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import * as fromStore from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';

@Component({
  templateUrl: 'project.component.html',
  styleUrls: [
    '../../../app/shared/layout/layout.common.scss',
    'project.component.scss',
  ],
  providers: [ProjectComponent],
})
export class ProjectComponent implements OnInit, OnDestroy {
  pageHeaderContentTemplate$: Observable<TemplatePortal<any>>;
  environments: Environments;
  userDocsUrl: string;
  projects$: Observable<Project[]>;
  currentProject$: Observable<Project>;
  userViewProjectBadgeDisplayState: boolean;
  onDestroy$ = new Subject<void>();

  constructor(
    public uiState: UiStateService,
    private activateRoute: ActivatedRoute,
    private store: Store<fromStore.AppState>,
    private title: Title,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
    injector: Injector,
  ) {
    this.environments = injector.get(ENVIRONMENTS);
  }

  /**
   * {'project': ''}
   */
  get baseParams(): Observable<Params> {
    return this.activateRoute.params;
  }

  get baseParamsSnapshot(): { project: string } {
    const params = this.activateRoute.snapshot.params;
    return {
      project: params['project'],
    };
  }

  get baseActivatedRoute() {
    return this.activateRoute;
  }

  ngOnInit(): void {
    this.userDocsUrl = this.environments.user_docs_url;
    this.pageHeaderContentTemplate$ = this.uiState.getTemplateHolder(
      TemplateHolderType.PageHeaderContent,
    ).templatePortal$;

    this.translate
      .stream('project_overview')
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(title => {
        this.title.setTitle(title);
      });

    this.baseParams
      .pipe(
        takeUntil(this.onDestroy$),
        pluck('project'),
        distinctUntilChanged(),
      )
      .subscribe((project: string) => {
        if (project) {
          this.store.dispatch(new fromStore.LoadProjectDetail(project));
        }
      });

    this.uiState.userViewProjectBadgeDisplayState$
      .pipe(
        takeUntil(this.onDestroy$),
        distinctUntilChanged(),
      )
      .subscribe(v => {
        this.userViewProjectBadgeDisplayState = v;
        this.cdr.detectChanges();
      });
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }
}
