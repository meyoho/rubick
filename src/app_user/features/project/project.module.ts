import { LayoutModule as AuiLayoutModule } from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { NamespaceSharedModule } from 'app/features-shared/namespace/shared.module';
import { ProjectSharedModule } from 'app/features-shared/project/shared.module';
import { ToolChainSharedModule } from 'app/features-shared/tool-chain/tool-chain-shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedLayoutModule } from 'app/shared/layout/module';
import { SharedModule } from 'app/shared/shared.module';
import { CreateNamespaceComponent } from 'app_user/features/project/create-namespace/create-namespace.component';
import { ProjectDetailPageComponent } from 'app_user/features/project/detail/project-detail.component';
import { ProjectOverviewComponent } from 'app_user/features/project/overview/project-overview.component';
import { ProjectComponent } from 'app_user/features/project/project.component';
import { ProjectRoutingModule } from 'app_user/features/project/project.routing.module';
import { SpaceDetailPageComponent } from 'app_user/features/project/space-detail/space-detail.component';
import { ToolChainAllocateResourceDialogComponent } from 'app_user/features/project/tool-chain/allocate-resource/allocate-resource-dialog/allocate-resource-dialog.component';
import { ToolChainAllocateResourceCardComponent } from 'app_user/features/project/tool-chain/allocate-resource/resource-card/resource-card.component';
import { ToolChainAllocatedSpacesListComponent } from 'app_user/features/project/tool-chain/allocated-spaces-list/allocated-spaces-list.component';
import { ToolChainDetailPageComponent } from 'app_user/features/project/tool-chain/detail/detail.component';

import { CreateNamespaceFormComponent } from './create-namespace/create-namespace-form/create-namespace-form.component';
import { CreateSpaceComponent } from './create-space/create-space.component';
import { BindingNamespaceGroupComponent } from './init-project/binding-namespace-group/binding-namespace-group.component';
import { InitDevopsToolsComponent } from './init-project/init-devops-tools/init-devops-tools.component';
import { InitProjectComponent } from './init-project/init-project.component';

@NgModule({
  imports: [
    SharedModule,
    SharedLayoutModule,
    AuiLayoutModule,
    FormTableModule,
    RouterModule,
    PortalModule,
    ProjectRoutingModule,
    ProjectSharedModule,
    NamespaceSharedModule,
    FormTableModule,
    ToolChainSharedModule,
  ],
  declarations: [
    ProjectComponent,
    ProjectOverviewComponent,
    ProjectDetailPageComponent,
    CreateNamespaceComponent,
    CreateSpaceComponent,
    SpaceDetailPageComponent,
    ToolChainDetailPageComponent,
    ToolChainAllocatedSpacesListComponent,
    InitProjectComponent,
    InitDevopsToolsComponent,
    CreateNamespaceFormComponent,
    BindingNamespaceGroupComponent,
    ToolChainAllocateResourceDialogComponent,
    ToolChainAllocateResourceCardComponent,
  ],
  entryComponents: [ToolChainAllocateResourceDialogComponent],
})
export class ProjectModule {}
