import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { CredentialDetailPageComponent } from 'app/features-shared/credential';
import { CreateNamespaceComponent } from 'app_user/features/project/create-namespace/create-namespace.component';
import { ProjectDetailPageComponent } from 'app_user/features/project/detail/project-detail.component';
import { ProjectOverviewComponent } from 'app_user/features/project/overview/project-overview.component';
import { ProjectComponent } from 'app_user/features/project/project.component';
import { SpaceDetailPageComponent } from 'app_user/features/project/space-detail/space-detail.component';
import { ToolChainDetailPageComponent } from 'app_user/features/project/tool-chain/detail/detail.component';

import { CreateSpaceComponent } from './create-space/create-space.component';
import { InitProjectComponent } from './init-project/init-project.component';

const routes: Routes = [
  {
    path: '',
    component: ProjectComponent,
    children: [
      {
        path: '',
        component: ProjectOverviewComponent,
      },
      {
        path: 'detail',
        component: ProjectDetailPageComponent,
      },
      {
        path: 'namespace',
        loadChildren: '../namespace/module#NamespaceModule',
      },
      {
        path: 'create_namespace',
        component: CreateNamespaceComponent,
      },
      {
        path: 'create_space',
        component: CreateSpaceComponent,
      },
      {
        path: 'space/:name',
        component: SpaceDetailPageComponent,
      },
      {
        path: 'tool_chain/:name',
        component: ToolChainDetailPageComponent,
      },
      {
        path: 'init_project',
        component: InitProjectComponent,
      },
      {
        path: 'credential/detail/:name',
        component: CredentialDetailPageComponent,
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProjectRoutingModule {}
