import {
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import { ChangeDetectorRef, Component, OnDestroy, OnInit } from '@angular/core';
import { FormControl, FormGroup } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { debounce, get, isArray } from 'lodash-es';
import { combineLatest, Observable, ReplaySubject } from 'rxjs';
import { filter, publishReplay, refCount, takeWhile } from 'rxjs/operators';

import {
  BatchImportAccountsDialogComponent,
  UserType,
} from 'app/features-shared/project/space-create-form/batch-import-accounts-dialog/component';
import { AccountType, RcAccount } from 'app/services/api/account.service';
import {
  NamespaceService,
  UserNamespace,
} from 'app/services/api/namespace.service';
import { OrgService } from 'app/services/api/org.service';
import {
  AdminRoleApi,
  Project,
  ProjectService,
} from 'app/services/api/project.service';
import { RBACService } from 'app/services/api/rbac.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { RoleService, RoleType } from 'app/services/api/role.service';
import { Namespace, Space, SpaceService } from 'app/services/api/space.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { RouterUtilService } from 'app/services/router-util.service';
import { AppState, getCurrentProjectDetail } from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { RBAC_USERNAME_PATTERN } from 'app/utils/patterns';
import { BindingSpaceDialogComponent } from 'app2/features/projects/spaces/binding.component';
import { DeleteSpaceDialogComponent } from 'app2/features/projects/spaces/delete.component';
import { UnbindingSpaceDialogComponent } from 'app2/features/projects/spaces/unbinding.component';
import { UpdateSpaceDialogComponent } from 'app2/features/projects/spaces/update.component';

interface ClusterOption {
  name: string;
  display_name: string;
  uuid: string;
}

@Component({
  templateUrl: 'space-detail.component.html',
  styleUrls: ['space-detail.component.scss'],
})
export class SpaceDetailPageComponent implements OnInit, OnDestroy {
  spaces$: ReplaySubject<Space[]> = new ReplaySubject();
  accounts: RcAccount[];
  currentSpace: Space;
  currentProject$: Observable<Project>;

  currentProject: Project;
  clusters: ClusterOption[];
  spaceName: string;
  addAccountLoading: boolean;
  admins: AdminRoleApi[];
  deletePermission: boolean;
  updatePermission: boolean;
  bindNamespacePermission: boolean;
  unbindNamespacePermission: boolean;
  roleAssignPermission: boolean;
  usernamePattern = RBAC_USERNAME_PATTERN;

  alive = true;

  userFormGroup = new FormGroup({
    userKeyword: new FormControl(''),
    roleName: new FormControl(''),
  });

  columns = [
    'username',
    'alias_name',
    'role',
    'source',
    'status',
    'created_at',
    'action',
  ];

  paginator = {
    total: 0,
    size: 10,
    current: 1,
  };

  roleTypes: RoleType[] = [];
  roles: { [username: string]: AccountType[] }[] = [];

  constructor(
    private store: Store<AppState>,
    private router: Router,
    private activatedRoute: ActivatedRoute,
    private projectService: ProjectService,
    private roleService: RoleService,
    private rbacService: RBACService,
    private orgService: OrgService,
    private spaceService: SpaceService,
    private dialogService: DialogService,
    private errorsToast: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private namespaceService: NamespaceService,
    private messageService: MessageService,
    private notification: NotificationService,
    private translate: TranslateService,
    private routerUtil: RouterUtilService,
    public cdr: ChangeDetectorRef,
  ) {
    this.currentProject$ = this.store.select(getCurrentProjectDetail).pipe(
      filter(project => !!project),
      publishReplay(1),
      refCount(),
    );

    combineLatest(
      this.activatedRoute.params,
      this.currentProject$,
      this.spaces$,
    )
      .pipe(takeWhile(() => this.alive))
      .subscribe(async ([params, project, spaces]) => {
        if (spaces.every(space => space.name !== params['name'])) {
          if (spaces && spaces[0]) {
            this.router.navigate(['../', spaces[0].name], {
              relativeTo: this.activatedRoute,
            });
          } else {
            this.router.navigate(['../../detail'], {
              relativeTo: this.activatedRoute,
            });
          }
          return;
        }

        this.spaceName = params['name'];
        this.currentProject = project;
        this.clusters = project.clusters;
        this.roles = [];
        try {
          await this.fetchCurrentSpace();
          await Promise.all([
            this.getSpaceAdminRoles(),
            this.fetchAccountlist(),
            this.fetchRoleTypes(),
            this.fetchPermissions(),
          ]);
          this.userFormGroup.setValue({
            userKeyword: '',
            roleName: get(this.roleTypes[0], 'role_name', ''),
          });
          this.cdr.detectChanges();
        } catch (err) {
          this.errorsToast.error(err);
        }
      });
  }

  async getSpaceAdminRoles() {
    try {
      this.admins = await this.projectService.getAdminRoles({
        level: 'space',
        space_name: this.currentSpace.name,
        project_name: this.currentProject.name,
      });
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  async fetchPermissions() {
    [
      [
        this.updatePermission,
        this.deletePermission,
        this.bindNamespacePermission,
        this.unbindNamespacePermission,
      ],
      this.roleAssignPermission,
    ] = await Promise.all([
      this.roleUtil.resourceTypeSupportPermissions(
        'space',
        { project_name: this.currentProject.name },
        ['update', 'delete', 'bind_namespace', 'unbind_namespace', 'assign'],
      ),
      this.roleUtil.resourceTypeSupportPermissions(
        'role',
        {
          project_name: this.currentProject.name,
          space_name: this.spaceName,
        },
        'assign',
      ),
    ]);
  }

  ngOnInit() {
    this.fetchSpaces();
  }

  ngOnDestroy() {
    this.alive = false;
  }

  get adminsNameStr() {
    if (this.admins && this.admins.length) {
      return this.admins
        .map(admin =>
          admin.realname ? `${admin.name}(${admin.realname})` : `${admin.name}`,
        )
        .join(', ');
    } else {
      return '-';
    }
  }

  batchAdd() {
    let originSelectedUsers: {
      username: string;
      role: AccountType;
    }[] = [];
    this.accounts.forEach(a => {
      originSelectedUsers = originSelectedUsers.concat(
        (this.roles[a.username] || []).map((role: RoleType) => ({
          username: a.username,
          role,
        })),
      );
    });
    const dialogRef = this.dialogService.open(
      BatchImportAccountsDialogComponent,
      {
        data: {
          roleTypes: this.roleTypes,
          isRoleType: true,
          originSelectedUsers,
        },
        size: DialogSize.Big,
      },
    );

    dialogRef
      .afterClosed()
      .subscribe((v: { users: UserType[]; roleType: RoleType }) => {
        if (v) {
          this.addAccounts(v.roleType, v.users);
        }
      });
  }

  async addAccounts(roleType: RoleType, users: UserType[]) {
    try {
      await this.roleService.addUsers(
        roleType.role_name,
        users.map(user => {
          return { user: user.username };
        }),
        {
          sync_namespace: true,
        },
      );
      await Promise.all([this.fetchAccountlist(), this.getSpaceAdminRoles()]);
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  async addSingleAccount() {
    if (this.userFormGroup.invalid) {
      return;
    }
    try {
      this.addAccountLoading = true;
      const { userKeyword, roleName } = this.userFormGroup.value;
      await this.roleService.addUsers(roleName, [{ user: userKeyword }], {
        sync_namespace: true,
      });
      await Promise.all([this.fetchAccountlist(), this.getSpaceAdminRoles()]);
      this.userFormGroup = new FormGroup({
        userKeyword: new FormControl(''),
        roleName: new FormControl(roleName),
      });
    } catch (err) {
      this.errorsToast.error(err);
    } finally {
      this.addAccountLoading = false;
    }
  }

  async fetchRoleTypes() {
    const { name } = this.currentProject;
    try {
      this.roleTypes = await this.rbacService.getValidRoleTypes({
        level: 'space',
        space_name: this.spaceName,
        project_name: name,
      });
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  getAccountStaus(is_valid: boolean) {
    return is_valid ? 'valid' : 'invalid';
  }

  async fetchSpaces() {
    try {
      const { results: spaces } = await this.spaceService.getSpaces();
      this.spaces$.next(spaces);
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  async fetchAccountlist(search: string = '') {
    try {
      const { results, count } = (await this.orgService.listOrgAccounts({
        search,
        space_name: this.spaceName,
        project_name: this.currentProject.name,
        page: this.paginator.current,
        page_size: this.paginator.size,
      })) as {
        results: RcAccount[];
        count: number;
      };

      this.paginator.total = count;
      this.accounts = results;
      this.accounts.forEach(account => {
        this.fetchAccountRoles(account.username);
      });
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  async fetchAccountRoles(username: string) {
    try {
      const data = await this.orgService.getAccountRoles(username, {
        project_name: this.currentProject.name,
        space_name: this.currentSpace.name,
      });
      const ret = data['result'] || [];
      this.roles[username] = ret;
    } catch (err) {
      this.errorsToast.error(err);
    }
  }

  async fetchCurrentSpace() {
    try {
      this.currentSpace = await this.spaceService.getCurrentSpace(
        this.spaceName,
      );
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  onSearch = debounce(this.fetchAccountlist, 300);

  onSearchKeyChange(key: string) {
    this.onSearch(key);
  }

  onSpaceSelect(space: Space) {
    this.router.navigate(['../', space.name], {
      relativeTo: this.activatedRoute,
    });
  }

  removeAccount(user: UserType) {
    this.dialogService
      .confirm({
        title: this.translate.get('delete_account_confirm', {
          name: user.username,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      })
      .then(() => {
        const roles = (this.roles[user.username] as RoleType[]).map(r => ({
          name: r.role_name,
        }));
        this.orgService
          .removeAccountRoles(user.username, roles, { sync_namespace: true })
          .then(async () => {
            this.notification.success(this.translate.get('delete_success'));
            await Promise.all([
              this.fetchAccountlist(),
              this.getSpaceAdminRoles(),
            ]);
          })
          .catch(error => {
            this.errorsToast.error(error);
          });
      })
      .catch(() => {});
  }

  pageChanged(page: number) {
    this.paginator.current = page;
    this.fetchAccountlist();
  }

  sizeChanged(size: number) {
    this.paginator.size = size;
    this.fetchAccountlist();
  }

  async openBindingNamespaceDialog() {
    try {
      const namespaces: UserNamespace[] = await this.namespaceService.getBatchNamespaceOptions(
        this.currentProject.clusters || [],
        this.currentProject.name,
      );

      const filterExcludedNamespacs = namespaces.filter(ns => {
        return !this.currentSpace.namespaces.some(sns => sns.uuid === ns.uuid);
      });

      const dialogRef = this.dialogService.open(BindingSpaceDialogComponent, {
        size: DialogSize.Medium,
        data: {
          spaceName: this.currentSpace.name,
          namespaces: filterExcludedNamespacs,
        },
      });
      dialogRef.afterClosed().subscribe(async v => {
        if (v && isArray(v)) {
          await this.projectService.bindSpaceWithNamespace(
            v.map((namespace: UserNamespace) => ({
              namespace_name: namespace.name,
              namespace_uuid: namespace.uuid,
              space_name: this.currentSpace.name,
              space_uuid: this.currentSpace.uuid,
            })),
          );
          this.messageService.success(this.translate.get('bind_success'));
          await this.fetchSpaces();
        }
      });
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  async openUnbindingNamespaceDialog() {
    try {
      const dialogRef = this.dialogService.open(UnbindingSpaceDialogComponent, {
        size: DialogSize.Medium,
        data: {
          spaceName: this.currentSpace.name,
          namespaces: this.currentSpace.namespaces,
        },
      });
      dialogRef.afterClosed().subscribe(async v => {
        if (v && isArray(v)) {
          await this.projectService.unbindSpaceWithNamespace(
            v.map((namespace: UserNamespace) => ({
              namespace_name: namespace.name,
              namespace_uuid: namespace.uuid,
              space_name: this.currentSpace.name,
              space_uuid: this.currentSpace.uuid,
            })),
          );
          this.messageService.success(this.translate.get('unbind_success'));
          await this.fetchSpaces();
        }
      });
    } catch (e) {
      this.errorsToast.error(e);
    }
  }

  openUpdateSpaceDialog() {
    const dialogRef = this.dialogService.open(UpdateSpaceDialogComponent, {
      size: DialogSize.Medium,
      data: {
        spaceName: this.currentSpace.name,
        displayName: this.currentSpace.display_name,
      },
    });
    dialogRef.afterClosed().subscribe(v => {
      if (v) {
        this.fetchCurrentSpace();
      }
    });
  }

  openDeleteSpaceDialog() {
    const dialogRef = this.dialogService.open(DeleteSpaceDialogComponent, {
      size: DialogSize.Medium,
      data: {
        spaceName: this.currentSpace.name,
        envs: this.currentSpace.namespaces
          .map(namespace => namespace.name)
          .join(', '),
      },
    });

    dialogRef.afterClosed().subscribe(v => {
      if (v) {
        this.routerUtil.goBack();
      }
    });
  }

  getNamespacesStr(namespaces: UserNamespace[]) {
    return namespaces
      ? namespaces.reduce((t, c, i) => {
          return t + (i === 0 ? c.name : `，${c.name}`);
        }, '')
      : '';
  }

  namespaceDetail(namespace: Namespace, event: Event) {
    event.stopPropagation();
    this.router.navigate([
      '/console/user/workspace',
      {
        project: this.currentProject.name,
        cluster: namespace.cluster_name,
        namespace: namespace.name,
      },
    ]);
  }

  displayRoleItem(item: RoleType) {
    return (
      item.role_name +
      (item.template_display_name ? `(${item.template_display_name})` : '')
    );
  }

  displayRolesText(roles: RoleType[]) {
    return roles.map(r => this.displayRoleItem(r)).join('，');
  }
}
