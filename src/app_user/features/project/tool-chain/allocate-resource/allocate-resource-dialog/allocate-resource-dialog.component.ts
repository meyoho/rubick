import {
  DIALOG_DATA,
  DialogRef,
  DialogService,
  MessageService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { get } from 'lodash-es';
import { from } from 'rxjs';
import { map, publishReplay, refCount, tap } from 'rxjs/operators';

import { ToolChainCreateResourceDialogComponent } from 'app/features-shared/tool-chain/create-resource-dialog/create-resource-dialog.component';
import { Space, SpaceService } from 'app/services/api/space.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  AllocatedSpace,
  AllocateInfo,
  ToolResource,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  getToolResourceDisplayName,
  isGlobalSecret,
  mapToResourceBindingBody,
} from 'app/services/api/tool-chain/utils';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';

@Component({
  selector: 'rc-allocate-resource-dialog',
  templateUrl: 'allocate-resource-dialog.component.html',
  styleUrls: ['allocate-resource-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolChainAllocateResourceDialogComponent implements OnInit {
  constructor(
    @Inject(DIALOG_DATA)
    public dialogData: {
      data: AllocateInfo;
      mode: string;
    },
    @Inject(WEBLABS) public weblabs: Weblabs,
    private dialog: DialogService,
    private dialogRef: DialogRef<any>,
    private toolChainApi: ToolChainApiService,
    private spaceService: SpaceService,
    private translate: TranslateService,
    private message: MessageService,
    private errorToast: ErrorsToastService,
    private cdr: ChangeDetectorRef,
  ) {}

  @ViewChild('form')
  form: NgForm;

  model: AllocatedSpace[] = [];
  loading = true;
  submitting = false;

  spaceEnabled: boolean = this.weblabs.GROUP_ENABLED;

  ngOnInit() {
    const bindings = this.dialogData.data.bindings;
    const resources = this.dialogData.data.resources;
    this.model = bindings.length
      ? bindings.map((item: AllocatedSpace) => {
          if (!this.spaceEnabled || item.allocatedToAll) {
            item.allocatedSpaces = [
              {
                name: this.translate.get('tool_chain_all_spaces'),
                displayName: this.translate.get('tool_chain_all_spaces'),
                devopsNamespace: this.translate.get('tool_chain_all_spaces'),
              },
            ];
          }
          return item;
        })
      : resources.map((item: ToolResource) => {
          return {
            name: item.name,
            type: item.type || '',
            allocatedSpaces: [],
            allocatedToAll: false,
          };
        });
  }

  spaces$ = from(this.spaceEnabled ? this.spaceService.getSpaces() : []).pipe(
    map(data => {
      return data.results.map((space: Space) => {
        return {
          name: space.name,
          displayName: space.display_name || space.name,
          devopsNamespace: space.devops.namespace.name,
        };
      });
    }),
    tap(() => {
      this.loading = false;
    }),
    publishReplay(1),
    refCount(),
  );

  addResource() {
    const resourceNameList = this.model.map((item: AllocatedSpace) => {
      return item.name;
    });
    const dialogRef = this.dialog.open(ToolChainCreateResourceDialogComponent, {
      data: {
        tenantName: this.dialogData.data.project,
        toolName: this.dialogData.data.toolName,
        type: this.dialogData.data.toolItemType,
        kind: this.dialogData.data.selector.kind,
        secretname: get(this.dialogData.data, ['secret', 'name'], ''),
        namespace: get(this.dialogData.data, ['secret', 'namespace'], ''),
        resourceNameList,
        options: {
          mode: isGlobalSecret(this.dialogData.data.secret)
            ? 'input'
            : 'select',
          hideOptions: false,
        },
      },
    });

    dialogRef.afterClosed().subscribe((res?: { mode: string; data: any }) => {
      if (res && res.mode === 'input') {
        const resource: AllocatedSpace = {
          name: get(res.data, ['metadata', 'name']),
          type: get(res.data, ['metadata', 'annotations', 'type'], ''),
          allocatedSpaces: this.spaceEnabled
            ? []
            : [
                {
                  name: this.translate.get('tool_chain_all_spaces'),
                  displayName: this.translate.get('tool_chain_all_spaces'),
                  devopsNamespace: this.translate.get('tool_chain_all_spaces'),
                },
              ],
          allocatedToAll: !this.spaceEnabled,
        };
        this.model.push(resource);
      } else if (res && res.mode === 'select') {
        const resources = res.data.map((item: any) => {
          return {
            name: item.name,
            type: item.type,
            allocatedSpaces: this.spaceEnabled
              ? []
              : [
                  {
                    name: this.translate.get('tool_chain_all_spaces'),
                    displayName: this.translate.get('tool_chain_all_spaces'),
                    devopsNamespace: this.translate.get(
                      'tool_chain_all_spaces',
                    ),
                  },
                ],
            allocatedToAll: !this.spaceEnabled,
          };
        });
        this.model = this.model.concat(resources);
      }
      this.cdr.markForCheck();
    });
  }

  disabledAllocate() {
    return (
      !this.model.length ||
      this.model.every(item => {
        return !item.allocatedSpaces.length;
      })
    );
  }

  confirm() {
    if (this.form.invalid || this.disabledAllocate()) {
      return;
    }
    this.submitting = true;
    const body = mapToResourceBindingBody(
      this.dialogData.data,
      this.model,
      this.spaceEnabled,
    );
    this.toolChainApi
      .updateSubscription(body)
      .then((res: AllocateInfo) => {
        this.message.success(
          this.translate.get('tool_chain_allocate_successful'),
        );
        this.dialogRef.close(res);
      })
      .catch((e: any) => {
        this.errorToast.error(e);
      })
      .then(() => {
        this.submitting = false;
      });
  }

  getToolResourceDisplayName(tool: string) {
    return this.translate.get(getToolResourceDisplayName(tool));
  }
}
