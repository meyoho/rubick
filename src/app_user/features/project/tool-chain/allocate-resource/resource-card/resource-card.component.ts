import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  forwardRef,
} from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';
import {
  AllocatedSpace,
  DevopsSpace,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import { TranslateService } from 'app/translate/translate.service';
import { map } from 'rxjs/operators';

@Component({
  selector: 'rc-tool-chain-resource-card',
  templateUrl: 'resource-card.component.html',
  styleUrls: ['resource-card.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => ToolChainAllocateResourceCardComponent),
      multi: true,
    },
    ToolChainAllocateResourceCardComponent,
  ],
})
export class ToolChainAllocateResourceCardComponent
  implements ControlValueAccessor {
  constructor(
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
  ) {}

  model: AllocatedSpace = {
    name: '',
    type: '',
    allocatedSpaces: [],
    allocatedToAll: true,
  };

  @Input()
  toolItemType: string;

  @Input()
  allocatedToAll: boolean;

  @Input()
  spaces: DevopsSpace[];

  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `/static/images/icon/enterprise_${lang}.svg`),
  );

  selectedChange(ev: any) {
    if (
      ev.some((item: DevopsSpace) => {
        return item.name === this.translate.get('tool_chain_all_spaces');
      })
    ) {
      this.model.allocatedToAll = true;
      this.model.allocatedSpaces = [
        {
          name: this.translate.get('tool_chain_all_spaces'),
          displayName: this.translate.get('tool_chain_all_spaces'),
          devopsNamespace: this.translate.get('tool_chain_all_spaces'),
        },
      ];
      return;
    }
    this.model.allocatedToAll = false;
    this.cdr.markForCheck();
  }

  valueChanged = (_: any) => {};
  onTouched = (_: any) => {};
  writeValue(value: any) {
    if (!value || typeof value === 'string') {
      return;
    }
    this.model = value;
    this.cdr.detectChanges();
  }
  registerOnChange(fn: any) {
    this.valueChanged = fn;
  }
  registerOnTouched(fn: any) {
    this.onTouched = fn;
  }

  trackFn(val: DevopsSpace) {
    return val.devopsNamespace;
  }
}
