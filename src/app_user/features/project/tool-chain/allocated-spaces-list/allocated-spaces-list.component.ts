import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  EventEmitter,
  Inject,
  Input,
  Output,
} from '@angular/core';
import {
  AllocateInfo,
  AllocatedSpace,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  allocatedDialogMode,
  getToolResourceDisplayName,
} from 'app/services/api/tool-chain/utils';
import { WEBLABS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';
import { ToolChainAllocateResourceDialogComponent } from 'app_user/features/project/tool-chain/allocate-resource/allocate-resource-dialog/allocate-resource-dialog.component';

@Component({
  selector: 'rc-allocated-spaces-list',
  templateUrl: 'allocated-spaces-list.component.html',
  styleUrls: ['allocated-spaces-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolChainAllocatedSpacesListComponent {
  allocatedToAll = !this.weblabs.GROUP_ENABLED;

  columns: string[] = this.allocatedToAll
    ? ['name']
    : ['name', 'allocated_spaces'];

  @Input()
  allocatedSpaces: AllocatedSpace[];

  @Input()
  resourceType: string;

  @Input()
  info: AllocateInfo;

  @Output()
  updated = new EventEmitter<any>();

  spaceMapper: any = {};

  constructor(
    @Inject(WEBLABS) public weblabs: Weblabs,
    private dialog: DialogService,
    private translate: TranslateService,
  ) {}

  openAllocatedDialog() {
    const dialogRef = this.dialog.open(
      ToolChainAllocateResourceDialogComponent,
      {
        data: {
          data: this.info,
          mode: allocatedDialogMode(this.info),
        },
        size: DialogSize.Large,
      },
    );
    dialogRef.afterClosed().subscribe((res?: any) => {
      if (res) {
        this.updated.emit(res);
      }
    });
  }

  getToolResourceDisplayName(tool: string) {
    return this.translate.get(getToolResourceDisplayName(tool));
  }

  getSpaceName(devopsNamespace: string) {
    return devopsNamespace; //todo: get space name from devopsnamespace
  }
}
