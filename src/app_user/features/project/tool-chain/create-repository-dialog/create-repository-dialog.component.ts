import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import { ChangeDetectorRef, Component, Inject, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { from, Observable, of } from 'rxjs';
import { catchError, publishReplay, refCount } from 'rxjs/operators';

import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { ToolService } from 'app/services/api/tool-chain/tool-chain-api.types';
import { ErrorsToastService } from 'app/services/errors-toast.service';

export interface TenantCreateArtifactRegistryForm {
  addWay: string;
  repoType: string;
  fileStorageLocation: string;
  repoName: string;
  versionPolicy: string;
  projectName: string;
}

@Component({
  selector: 'rc-create-repository-dialog',
  templateUrl: './create-repository-dialog.component.html',
  styleUrls: ['./create-repository-dialog.component.scss'],
})
export class CreateRepositoryDialogComponent implements OnInit {
  loadError = false;
  createForm: FormGroup;
  blobStore$: Observable<any>;

  constructor(
    @Inject(DIALOG_DATA)
    public data: ToolService,
    private fb: FormBuilder,
    private toolChainApi: ToolChainApiService,
    private errorService: ErrorsToastService,
    private cdr: ChangeDetectorRef,
    private errorToastService: ErrorsToastService,
    private dialogRef: DialogRef<CreateRepositoryDialogComponent>,
  ) {}

  ngOnInit() {
    this.createForm = this.buildForm();
    this.blobStore$ = this.getBlobStore();
  }

  buildForm(): FormGroup {
    return this.fb.group({
      addWay: this.fb.control('createMode'),
      repoType: this.fb.control('Maven'),
      fileStorageLocation: this.fb.control('', [Validators.required]),
      repoName: this.fb.control('', [
        Validators.required,
        Validators.maxLength(20),
      ]),
      versionPolicy: this.fb.control('Release'),
      projectName: this.fb.control(this.data.projectName),
    });
  }

  getBlobStore() {
    return from(this.toolChainApi.getArtifactBlobStore(this.data.name)).pipe(
      catchError(error => {
        this.loadError = true;
        this.errorService.error(error);
        return of(null);
      }),
      publishReplay(1),
      refCount(),
    );
  }

  addRepo() {
    const repoName = this.createForm.value['repoName'];
    this.createForm.controls['repoName'].setValue(
      `${this.data.projectName}-${repoName}`,
    );
    const formData: TenantCreateArtifactRegistryForm = this.createForm.value;
    from(
      this.toolChainApi.createArtifactRegistry<
        TenantCreateArtifactRegistryForm
      >({
        managerName: this.data.name,
        formData,
      }),
    ).subscribe(
      res => {
        this.cdr.markForCheck();
        this.dialogRef.close({ ...res });
      },
      err => {
        this.cdr.markForCheck();
        this.errorToastService.error(err);
      },
    );
  }
}
