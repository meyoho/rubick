import {
  ConfirmType,
  DialogService,
  DialogSize,
  MessageService,
  NotificationService,
} from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Store } from '@ngrx/store';

import { get, snakeCase } from 'lodash-es';
import { BehaviorSubject, EMPTY, from, Observable, Subject } from 'rxjs';
import {
  catchError,
  filter,
  first,
  map,
  publishReplay,
  refCount,
  tap,
} from 'rxjs/operators';

import { CredentialType } from 'app/services/api/credential.service';
import { PorjectTabIndex, Project } from 'app/services/api/project.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  AllocateInfo,
  AuthorizeInfo,
  ToolChainSecret,
  ToolKind,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  allocatedDialogMode,
  canViewAllocateDialog,
  getToolChainSecretSourceType,
  getToolResourceDisplayName,
  isGlobalSecret,
  resourceSwitcher,
} from 'app/services/api/tool-chain/utils';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { WEBLABS } from 'app/shared/tokens';
import { AppState, getCurrentProjectDetail } from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import { Weblabs } from 'app/typings';
import { SecretValidatingDialogComponent } from 'app2/features/tool-chain/secret-validating-dialog/secret-validating-dialog.component';
import { ForceDeleteComponent } from 'app2/shared/components/force-delete/component';
import { ToolChainAllocateResourceDialogComponent } from 'app_user/features/project/tool-chain/allocate-resource/allocate-resource-dialog/allocate-resource-dialog.component';
import { ToolChainSubscribeUpdateDialogComponent } from 'app_user/features/project/tool-chain/subscribe-update-dialog/update-dialog.component';

@Component({
  templateUrl: 'detail.component.html',
  styleUrls: ['detail.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolChainDetailPageComponent implements OnInit {
  updatePermission: boolean;
  deletePermission: boolean;

  tabIndex = PorjectTabIndex.ToolChain;
  currentProject$: Observable<Project>;
  update$ = new Subject<{ [key: string]: string }>();
  harborProjectsUpdated$ = new BehaviorSubject(null);

  columns: string[] = ['name', 'allocated_spaces'];
  spaceEnabled: boolean;
  canViewallocatedSpaces = false;
  canViewAllocateDialog = false;
  needAuthorization = false;
  name$ = this.activatedRoute.paramMap.pipe(map(params => params.get('name')));
  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `assets/icons/enterprise_${lang}.svg`),
  );

  fetchData = (name: string) => {
    return from(this.toolChainApi.getSubscription(name)).pipe(
      tap(info => {
        this.canViewallocatedSpaces =
          this.weblabs.DEVOPS_INTEGRATION &&
          resourceSwitcher(info.toolItemType);
        this.canViewAllocateDialog = canViewAllocateDialog(
          info.createdMode,
          this.weblabs.GROUP_ENABLED,
        );
        this.spaceEnabled = this.weblabs.GROUP_ENABLED;
        const message = get(info, ['status', 'message'], '');
        if (message.includes('needs human intervation')) {
          this.needAuthorization = true;
        }
      }),
      catchError(e => {
        this.errorToast.error(e);
        this.router.navigate(['../../', 'detail'], {
          queryParams: {
            tabIndex: this.tabIndex,
          },
          relativeTo: this.activatedRoute,
        });
        return EMPTY;
      }),
      publishReplay(1),
      refCount(),
    );
  };

  async ngOnInit() {
    this.currentProject$ = this.store.select(getCurrentProjectDetail).pipe(
      filter(project => !!project),
      publishReplay(1),
      refCount(),
    );

    [
      this.updatePermission,
      this.deletePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_toolbindingreplica',
      {},
      ['update', 'delete'],
    );
  }

  constructor(
    @Inject(WEBLABS) public weblabs: Weblabs,
    private store: Store<AppState>,
    private activatedRoute: ActivatedRoute,
    private toolChainApi: ToolChainApiService,
    private roleUtil: RoleUtilitiesService,
    private dialog: DialogService,
    private translate: TranslateService,
    private router: Router,
    private message: MessageService,
    private errorToast: ErrorsToastService,
    private notification: NotificationService,
  ) {}

  update(data: AllocateInfo) {
    const dialogRef = this.dialog.open(
      ToolChainSubscribeUpdateDialogComponent,
      {
        data: data,
        size: DialogSize.Large,
      },
    );
    dialogRef.afterClosed().subscribe(update => {
      if (update) {
        this.update$.next();
      }
    });
  }

  delete(data: AllocateInfo) {
    const bindings = data.bindings;
    if (bindings.length) {
      const dialogRef = this.dialog.open(ForceDeleteComponent, {
        data: {
          name: data.name,
          title: this.translate.get('delete'),
          content: this.translate.get(
            'tool_chain_resource_force_delete_content',
            {
              name: data.name,
            },
          ),
          confirmText: this.translate.get('tool_chain_delete_tool'),
        },
      });
      dialogRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: Boolean) => {
          dialogRef.close();
          if (res) {
            try {
              await this.toolChainApi.deleteSubscription(data.name);
              this.message.success(
                this.translate.get('tool_chain_delete_successful'),
              );
              this.router.navigate(['../../', 'detail'], {
                queryParams: {
                  tabIndex: this.tabIndex,
                },
                relativeTo: this.activatedRoute,
              });
            } catch (e) {
              this.errorToast.error(e);
            }
          }
        });
    } else {
      this.dialog
        .confirm({
          title: this.translate.get('confirm_delete_toolbindingreplica', {
            name: data.name,
          }),
          content:
            data.toolItemType === 'Jenkins'
              ? this.translate.get('confirm_delete_jenkins_toolbindingreplica')
              : '',
          confirmText: this.translate.get('delete'),
          confirmType: ConfirmType.Danger,
          cancelText: this.translate.get('cancel'),
          beforeConfirm: (resolve: any, reject: any) => {
            from(this.toolChainApi.deleteSubscription(data.name)).subscribe(
              resolve(),
              (e: any) => {
                this.errorToast.error(e);
                reject();
              },
            );
          },
        })
        .then(() => {
          this.message.success(
            this.translate.get('tool_chain_delete_successful'),
          );
          this.router.navigate(['../../', 'detail'], {
            queryParams: {
              tabIndex: this.tabIndex,
            },
            relativeTo: this.activatedRoute,
          });
        })
        .catch(() => {});
    }
  }

  snakeCaseKind(kind: ToolKind) {
    return 'tool_chain_' + snakeCase(kind);
  }

  getAuthType(type: CredentialType) {
    switch (type) {
      case CredentialType.BasicAuth:
        return this.translate.get('secret_username_password');
      case CredentialType.OAuth2:
        return this.translate.get('tool_chain_oauth2');
      default:
        return '-';
    }
  }

  isGlobalSecret(secret?: ToolChainSecret) {
    return isGlobalSecret(secret);
  }

  getSecretType(secret?: ToolChainSecret) {
    const type = getToolChainSecretSourceType(secret);
    switch (type) {
      case 'platform':
        return this.translate.get('tool_chain_platform_secret');
      case 'tenant':
        return this.translate.get('tool_chain_tenant_secret');
      default:
        return '-';
    }
  }

  getToolResourceDisplayName(tool: string) {
    return this.translate.get(getToolResourceDisplayName(tool));
  }

  navigateToSecretDetail(secret: ToolChainSecret) {
    this.router.navigate(['../../credential/detail', secret.name], {
      relativeTo: this.activatedRoute,
    });
  }

  openAllocateResourceDialog(data: AllocateInfo) {
    const dialogRef = this.dialog.open(
      ToolChainAllocateResourceDialogComponent,
      {
        data: {
          data,
          mode: allocatedDialogMode(data),
        },
        size: DialogSize.Large,
      },
    );
    dialogRef.afterClosed().subscribe((res?: any) => {
      if (res) {
        this.update$.next();
      }
    });
  }

  afterAllocatedSpaceUpdate(update: any) {
    if (update) {
      this.update$.next();
    }
  }

  validateStatus(data: AllocateInfo) {
    const info = {
      kind: data.toolItemKind,
      name: data.toolName,
      secretName: data.secret.name,
      namespace: data.secret.namespace,
    };
    this.toolChainApi
      .getValidateStatus(info)
      .then((authInfo: AuthorizeInfo) => {
        const dialogRef = this.dialog.open(SecretValidatingDialogComponent, {
          size: DialogSize.Small,
          data: {
            url: authInfo.authorizeUrl,
            secret: data.secret,
            name: info.name,
            kind: info.kind,
          },
        });
        return dialogRef.afterClosed().subscribe(res => {
          if (res) {
            this.needAuthorization = false;
            this.update$.next();
          }
        });
      })
      .catch(error => {
        this.notification.error({
          title: this.translate.get('tool_chain_oauth_validate_error'),
          content: error.message,
        });
      });
  }

  onSelectedIndexChange(event: number) {
    if (this.spaceEnabled && event === 1) {
      this.harborProjectsUpdated$.next(null);
    }
  }
}
