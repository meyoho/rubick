import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { from, of, Subject } from 'rxjs';
import { catchError, map, publishReplay, refCount, tap } from 'rxjs/operators';

import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { ToolService } from 'app/services/api/tool-chain/tool-chain-api.types';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { IntegrateToolComponent } from 'app2/features/tool-chain/integrate-tool/integrate-tool.component';
import { ToolChainSubscribeDialogComponent } from '../subscribe-dialog/subscribe-dialog.component';

@Component({
  selector: 'rc-user-tool-chain-list',
  templateUrl: 'list.component.html',
  styleUrls: ['list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class UserToolChainListPageComponent implements OnInit {
  integrationCreatePermission: boolean;
  subscribeCreatePermission: boolean;
  subscribeUpdatePermission: boolean;

  selectedType = 'all';
  loading = true;
  updated$ = new Subject<{ [key: string]: string }>();
  fetchData = () => {
    return from(
      this.toolChainApi.getSubscriptions({
        order_by: '-metadata.creationTimestamp',
      }),
    ).pipe(
      catchError(e => {
        this.errorToastService.error(e);
        return of(null);
      }),
      tap(() => {
        this.loading = false;
      }),
      publishReplay(1),
      refCount(),
    );
  };

  toolTypes$ = from(this.toolChainApi.getToolChains()).pipe(
    map(types =>
      types.filter(
        type => type.enabled && type.name !== 'artifactRegistryManager',
      ),
    ),
    map(types => {
      types.forEach(type => {
        if (type.name === 'artifactRepository') {
          type.items = type.items.filter(item => item.name !== 'Maven2');
        }
      });
      return types;
    }),
    tap(types => (types['subscribeMode'] = true)),
    publishReplay(1),
    refCount(),
  );

  constructor(
    private cdr: ChangeDetectorRef,
    private toolChainApi: ToolChainApiService,
    private roleUtil: RoleUtilitiesService,
    private errorToastService: ErrorsToastService,
    private router: Router,
    private route: ActivatedRoute,
    private dialog: DialogService,
  ) {}

  async ngOnInit() {
    [
      this.integrationCreatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions('devops_tool', {}, [
      'create',
    ]);
    [
      this.subscribeCreatePermission,
      this.subscribeUpdatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_toolbindingreplica',
      {},
      ['create', 'update'],
    );
    this.cdr.markForCheck();
  }

  navigateToDetail(ins: ToolService) {
    this.router.navigate(['../tool_chain', ins.name], {
      relativeTo: this.route,
    });
  }

  menuHandler(type: string) {
    switch (type) {
      case 'subscribe':
        this.showSubscribeDialog();
        break;
      case 'integration':
        this.showIntegrationDialog();
        break;
    }
  }

  showSubscribeDialog() {
    const projectName = window.sessionStorage.getItem('project');
    const dialogRef = this.dialog.open(ToolChainSubscribeDialogComponent, {
      data: {
        projectName,
      },
      size: DialogSize.Large,
    });

    dialogRef.afterClosed().subscribe((res?: any) => {
      if (res) {
        this.updated$.next();
      }
    });
  }

  showIntegrationDialog() {
    this.toolTypes$.subscribe(toolTypes => {
      const projectName = window.sessionStorage.getItem('project');
      const dialogRef = this.dialog.open(IntegrateToolComponent, {
        data: Object.assign(toolTypes, { projectName }),
        size: DialogSize.Large,
      });

      dialogRef.afterClosed().subscribe(() => {
        this.updated$.next();
      });
    });
  }

  afterAllocated(ins: ToolService) {
    if (ins) {
      this.updated$.next();
    }
  }
}
