import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  ViewChild,
} from '@angular/core';

import { isEmpty } from 'lodash-es';
import { BehaviorSubject, combineLatest, from, of, Subject } from 'rxjs';
import {
  catchError,
  debounceTime,
  map,
  publishReplay,
  refCount,
  startWith,
  tap,
} from 'rxjs/operators';

import { CredentialType } from 'app/services/api/credential.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { ToolService } from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  isGlobalSecret,
  mapToToolService,
} from 'app/services/api/tool-chain/utils';
import { ToolChainSubscribeEditComponent } from 'app_user/features/project/tool-chain/subscribe-edit/subscribe-edit.component';

@Component({
  templateUrl: 'subscribe-dialog.component.html',
  styleUrls: ['subscribe-dialog.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolChainSubscribeDialogComponent {
  selectedType = 'all';
  loading = true;
  filter$ = new BehaviorSubject<string>('');
  itemUpdated$ = new Subject<ToolService>();
  selectedTool: ToolService;
  secretTypes = CredentialType;
  supportTypesMap: any = {};
  model = {
    name: '',
    description: '',
    secretType: this.secretTypes.BasicAuth,
    useSecret: false,
    secret: {
      name: '',
      namespace: '',
    },
  };

  @ViewChild('editor')
  editor: ToolChainSubscribeEditComponent;
  selectedType$$ = new BehaviorSubject<string>(this.selectedType);

  toolTypes$ = from(this.toolChainApi.getToolChains()).pipe(
    map(types =>
      types.filter(
        type => type.enabled && type.name !== 'artifactRegistryManager',
      ),
    ),
    tap(types => {
      types.forEach(type => {
        type.items.forEach(item => {
          if (!this.supportTypesMap[item.type]) {
            this.supportTypesMap[item.type] = item.supportedSecretTypes;
          }
        });
      });
    }),
    publishReplay(1),
    refCount(),
  );

  items$ = combineLatest(
    from(this.toolChainApi.findToolServices('', true)),
    this.itemUpdated$.pipe(startWith({})),
  ).pipe(
    map(([data, item]) => {
      if (!isEmpty(item)) {
        data.items.push(mapToToolService(item));
      }
      return data.items;
    }),
    catchError(() => of([])),
    tap(() => {
      this.loading = false;
    }),
    publishReplay(1),
    refCount(),
  );

  allServices$ = combineLatest(
    this.items$,
    this.filter$.pipe(debounceTime(300)),
  ).pipe(
    map(([items, keyword]) =>
      (items || []).filter(item => item.name.includes(keyword)),
    ),
    publishReplay(1),
    refCount(),
  );

  filteredServices$ = combineLatest(
    this.allServices$,
    this.selectedType$$,
  ).pipe(
    map(([tools, selectedType]) =>
      tools.filter(
        tool => selectedType === 'all' || tool.toolType === selectedType,
      ),
    ),
    publishReplay(1),
    refCount(),
  );

  hasServices$ = this.allServices$.pipe(
    map(ins => ins && ins.length),
    publishReplay(1),
    refCount(),
  );

  constructor(
    @Inject(DIALOG_DATA)
    public data: {
      projectName: string;
    },
    public toolChainApi: ToolChainApiService,
    private dialogRef: DialogRef<any>,
  ) {}

  selectTool(tool: ToolService) {
    this.selectedTool = tool;

    this.model = isGlobalSecret(tool.secret)
      ? {
          ...this.model,
          useSecret: false,
        }
      : {
          ...this.model,
          useSecret: true,
          secret: {
            ...tool.secret,
          },
        };
  }

  afterSaved(res: any) {
    this.dialogRef.close(res);
  }
}
