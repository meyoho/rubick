import { DialogService, MessageService } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  OnInit,
  Output,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';

import { from, Subject } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  switchMap,
} from 'rxjs/operators';

import { CredentialCreateDialogComponent } from 'app/features-shared/credential';
import {
  Credential,
  CredentialApiService,
  CredentialType,
} from 'app/services/api/credential.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { API_GROUP, API_VERSION } from 'app/services/api/tool-chain/constants';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import {
  AllocateInfo,
  SupportedSecretTypes,
  ToolBindingReplica,
} from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  getMatchExpressions,
  getToolChainSecretSourceType,
  mapCredentialToToolChainSecret,
} from 'app/services/api/tool-chain/utils';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';

@Component({
  selector: 'rc-tool-chain-subscribe-edit',
  templateUrl: 'subscribe-edit.component.html',
  styleUrls: ['subscribe-edit.component.scss'],
  exportAs: 'rc-tool-chain-subscribe-edit',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolChainSubscribeEditComponent implements OnInit, OnChanges {
  secretCreatePermission: boolean;

  resourceNamePattern = K8S_RESOURCE_NAME_BASE;
  secretTypes = CredentialType;

  platformSecretDisabled: boolean;

  @Input()
  data = {
    name: '',
    description: '',
    secretType: this.secretTypes.BasicAuth,
    secretSourceType: '',
    useSecret: false,
    secret: {
      name: '',
      namespace: '',
    },
  };

  model = this.data;

  @Input()
  mode: 'create' | 'update' = 'create';

  @Input()
  tool: AllocateInfo;

  @Input()
  supportedSecretTypes: SupportedSecretTypes[] = [];

  @ViewChild('form')
  form: NgForm;

  @Output()
  saved = new EventEmitter<any>();

  // 凭据
  private secretsUpdated$$ = new Subject<CredentialType>();

  secrets$ = this.secretsUpdated$$.pipe(startWith(null)).pipe(
    switchMap(() => this.secretApi.find({})),
    map(res => res.items.filter(item => item.type === this.model.secretType)),
    publishReplay(1),
    refCount(),
  );

  addSecret() {
    const dialogRef = this.dialog.open(CredentialCreateDialogComponent, {
      data: {
        types: [this.model.secretType],
        showHint: false,
      },
    });

    dialogRef.afterClosed().subscribe((credential?: Credential) => {
      if (credential) {
        this.resetSecret(credential);
      }
    });
  }

  secretToValue(credential: Credential) {
    this.model = {
      ...this.model,
      secret: mapCredentialToToolChainSecret(credential),
    };
  }

  resetSecret(credential?: Credential) {
    if (credential) {
      this.model.secretType = credential.type;
      this.model.secret = mapCredentialToToolChainSecret(credential);
    } else {
      this.model.secret = {
        name: '',
        namespace: '',
      };
    }
    this.secretsUpdated$$.next();
  }

  constructor(
    private secretApi: CredentialApiService,
    private toolChainApi: ToolChainApiService,
    private roleUtil: RoleUtilitiesService,
    private message: MessageService,
    private translate: TranslateService,
    private cdr: ChangeDetectorRef,
    private errorToastService: ErrorsToastService,
    private dialog: DialogService,
  ) {}

  initModel() {
    if (this.mode === 'update') {
      this.model = this.data;
    }
    const disabledTools = ['Github', 'Bitbucket'];
    const toolItemType =
      this.mode === 'create' ? this.tool.type : this.tool.toolItemType;
    const secretType = getToolChainSecretSourceType(this.tool.secret);

    if (disabledTools.includes(toolItemType) || !secretType) {
      // github bitbucket 或 平台集成时没填凭据 禁用平台选项
      this.model.secretSourceType = 'tenant';
      this.model.useSecret = true;
      this.platformSecretDisabled = true;
      return;
    }

    if (secretType === 'tenant') {
      this.model.secretSourceType = 'tenant';
      this.model.useSecret = true;
      this.platformSecretDisabled = true;
      return;
    }

    if (secretType === 'platform') {
      this.model.secretSourceType = 'platform';
      this.model.useSecret = false;
      this.model.secret = {
        name: '',
        namespace: '',
      };
      this.platformSecretDisabled = false;
    }
  }

  async ngOnInit() {
    [
      this.secretCreatePermission,
    ] = await this.roleUtil.resourceTypeSupportPermissions(
      'devops_secret',
      {},
      ['create'],
    );
  }

  ngOnChanges() {
    this.initModel();
  }

  onSecretSourceTypeChange(val: string) {
    this.model.secretSourceType = val;
    this.model.useSecret = val === 'tenant';
  }

  supportedSecretType(type: string) {
    return (this.supportedSecretTypes || [])
      .map((item: SupportedSecretTypes) => item.type)
      .includes(type);
  }

  save() {
    if (!this.form.valid) {
      return;
    }

    const ins = this.tool;
    const projectName = window.sessionStorage.getItem('project');
    const spec =
      this.mode === 'create'
        ? {
            selector: { name: ins.name, kind: ins.kind as string },
          }
        : {
            selector: ins.selector,
            ...this.tool.__original.spec,
          };
    const payload: ToolBindingReplica = {
      apiVersion: `${API_GROUP}/${API_VERSION}`,
      kind: 'ToolBindingReplica',
      metadata: {
        name: this.model.name,
        namespace: '',
        annotations: {
          'alauda.io/description': this.model.description || '',
          'alauda.io/createdMode':
            this.mode === 'create' ? 'subscription' : this.tool.createdMode,
          'alauda.io/toolItemProject': this.tool.integrateBy || '',
        },
      },
      spec,
    };

    this.allocateParamsHandler(payload, projectName);
    payload.spec['secret'] = this.model.useSecret ? this.model.secret : null;

    const handler =
      this.mode === 'create'
        ? from(this.toolChainApi.subscribeTool(payload))
        : from(this.toolChainApi.updateSubscription(payload));
    handler.subscribe(
      (result: any) => {
        this.cdr.markForCheck();
        this.saved.emit(result);
        this.message.success({
          content: this.translate.get(
            this.mode === 'create'
              ? 'tool_chain_subscribe_succeed'
              : 'tool_chain_update_succeed',
          ),
        });
      },
      (e: any) => {
        this.cdr.markForCheck();
        this.errorToastService.error(e);
      },
    );
  }

  submit() {
    (this.form as any).submitted = true;
    this.form.ngSubmit.emit();
  }

  allocateParamsHandler(payload: ToolBindingReplica, projectName: string) {
    switch (payload.spec.selector.kind) {
      case 'Jenkins': {
        return Object.assign(payload, {
          spec: {
            continuousIntegration: {
              template: {
                bindings: [
                  {
                    selector: {
                      matchExpressions: getMatchExpressions(projectName),
                    },
                    spec: {
                      jenkins: {
                        name: '',
                      },
                    },
                  },
                ],
              },
            },
            selector: payload.spec.selector,
          },
        });
      }
      case 'CodeQualityTool': {
        return Object.assign(payload, {
          spec: {
            codeQualityTool: {
              template: {
                bindings: [
                  {
                    selector: {
                      matchExpressions: getMatchExpressions(projectName),
                    },
                    spec: {
                      codeQualityTool: {
                        name: '',
                      },
                    },
                  },
                ],
              },
            },
            selector: payload.spec.selector,
          },
        });
      }
      case 'ArtifactRegistry': {
        return Object.assign(payload, {
          spec: {
            artifactRegistry: {
              template: {
                bindings: [
                  {
                    selector: {
                      matchExpressions: getMatchExpressions(projectName),
                    },
                    spec: {
                      artifactRegistry: {
                        name: payload.spec.selector.name,
                      },
                    },
                  },
                ],
              },
            },
            selector: payload.spec.selector,
          },
        });
      }
    }
  }
}
