import { DialogService, DialogSize } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  EventEmitter,
  Input,
  OnChanges,
  Output,
} from '@angular/core';

import { snakeCase } from 'lodash-es';
import { map } from 'rxjs/operators';

import { ToolService } from 'app/services/api/tool-chain/tool-chain-api.types';
import { TranslateService } from 'app/translate/translate.service';
import { CreateRepositoryDialogComponent } from 'app_user/features/project/tool-chain/create-repository-dialog/create-repository-dialog.component';

@Component({
  selector: 'rc-subscribe-tool-list',
  templateUrl: 'subscribe-tool-list.component.html',
  styleUrls: ['subscribe-tool-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class SubscribeToolListComponent implements OnChanges {
  @Input()
  services: ToolService[];
  @Input()
  projectName: string;

  @Output()
  cardClick = new EventEmitter<ToolService>();
  @Output()
  repoUpdated = new EventEmitter<ToolService>();

  snakeCase = snakeCase;
  selectedTool: ToolService;
  normalServices: ToolService[];
  artifactServices: { [key: string]: ToolService }[];
  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `/static/images/icon/enterprise_${lang}.svg`),
  );

  subscription: boolean;

  constructor(
    private translate: TranslateService,
    private dialog: DialogService,
    private cdr: ChangeDetectorRef,
  ) {}

  trackByName(_: number, item: ToolService) {
    return item.name;
  }

  cardClickHandler(service: ToolService) {
    this.selectedTool = service;
    this.cardClick.emit(service);
  }

  ngOnChanges(): void {
    this.normalServices = this.services.filter(
      service =>
        service.kind !== 'ArtifactRegistryManager' &&
        service.kind !== 'ArtifactRegistry',
    );
    this.artifactServices = this.services
      .filter(service => service.kind === 'ArtifactRegistry')
      .reduce((pre, cur) => {
        const key = cur.artifactRegistryManager || 'unknown';
        pre[key] = pre[key] || [];
        pre[key].push(cur);
        return pre;
      }, []);
  }

  addRepo(serviceName: string, kind: string) {
    const service = this.getServiceByName(serviceName, kind);
    const diglogRef = this.dialog.open(CreateRepositoryDialogComponent, {
      data: {
        ...service,
        projectName: this.projectName,
      },
      size: DialogSize.Large,
    });
    diglogRef.afterClosed().subscribe(res => {
      if (res) {
        this.repoUpdated.emit(res);
        this.cdr.markForCheck();
      }
    });
  }

  getServiceByName(serviceName: string, kind: string): ToolService {
    return this.services
      .filter(service => service.kind === kind)
      .find(service => service.name === serviceName);
  }

  showAddRepo(serviceName: string, kind: string) {
    const service = this.services
      .filter(service => service.kind === kind)
      .find(service => service.name === serviceName);
    return (
      service && service.subscription && service.secret && service.secret.name
    );
  }
}
