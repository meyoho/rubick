import { DIALOG_DATA, DialogRef } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Inject,
  OnInit,
  ViewChild,
} from '@angular/core';
import { CredentialType } from 'app/services/api/credential.service';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { AllocateInfo } from 'app/services/api/tool-chain/tool-chain-api.types';
import { isGlobalSecret } from 'app/services/api/tool-chain/utils';
import { ToolChainSubscribeEditComponent } from 'app_user/features/project/tool-chain/subscribe-edit/subscribe-edit.component';
import { throwError } from 'rxjs';

@Component({
  templateUrl: 'update-dialog.component.html',
  styleUrls: [],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ToolChainSubscribeUpdateDialogComponent implements OnInit {
  loading = true;
  secretTypes = CredentialType;

  model = {
    name: '',
    description: '',
    secretType: this.secretTypes.BasicAuth,
    useSecret: false,
    secret: {
      name: '',
      namespace: '',
    },
  };

  supportTypesMap: any = {};

  @ViewChild('editor')
  editor: ToolChainSubscribeEditComponent;

  constructor(
    private toolChainApi: ToolChainApiService,
    private dialogRef: DialogRef<any>,
    @Inject(DIALOG_DATA) public tool: AllocateInfo,
  ) {}

  ngOnInit() {
    if (this.tool) {
      this.initModel(this.tool);
    } else {
      return throwError({
        title: 'tool_chain_update_input_invalid',
        data: {},
      });
    }
  }

  initModel(tool: AllocateInfo) {
    this.model.name = tool.name;
    this.model.description = tool.description;
    this.model.secretType = tool.auth_type as CredentialType;
    this.model = isGlobalSecret(tool.secret)
      ? {
          ...this.model,
          useSecret: false,
        }
      : {
          ...this.model,
          useSecret: true,
          secret: {
            ...tool.secret,
          },
        };
    this.toolChainApi.getToolChains().then(res => {
      return res
        .filter(type => type.enabled)
        .forEach(type => {
          type.items.forEach(item => {
            if (!this.supportTypesMap[item.type]) {
              this.supportTypesMap[item.type] = item.supportedSecretTypes;
            }
          });
        });
    });
  }

  afterSaved(res: AllocateInfo) {
    this.dialogRef.close(res);
  }
}
