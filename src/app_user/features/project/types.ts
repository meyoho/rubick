export interface QuotaRange {
  cpu?: {
    max: number;
    used: number;
  };
  memory?: {
    max: number;
    used: number;
  };
  storage?: {
    max: number;
    used: number;
  };
  pvc_num?: {
    max: number;
    used: number;
  };
  pods?: {
    max: number;
    used: number;
  };
}

export enum QuotaConfigEnum {
  cpu = 'requests.cpu',
  memory = 'requests.memory',
  storage = 'requests.storage',
  pvc_num = 'persistentvolumeclaims',
  pods = 'pods',
  cpu_requests = 'requests.cpu',
  cpu_limits = 'limits.cpu',
  memory_requests = 'requests.memory',
  memory_limits = 'limits.memory',
}
