import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as jsyaml from 'js-yaml';
import { cloneDeep } from 'lodash-es';

import { PodControllerKinds } from 'app/features-shared/app/utils/pod-controller';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { K8sResourceWithActions, PersistentVolumeClaim } from 'app/typings';
import { KubernetesResource } from 'app/typings/raw-k8s';
import * as codeEditorUtil from 'app/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class PvcDetailComponent implements OnInit {
  clusterName: string;
  namespaceName: string;
  name: string;
  pvcYaml: string;
  private RESOURCE_TYPE = 'persistentvolumeclaims';
  codeEditorOptions = codeEditorUtil.yamlOptions;
  actionsConfigView = codeEditorUtil.viewActions;
  initialized = false;
  loading = false;
  pvc: K8sResourceWithActions<PersistentVolumeClaim>;
  referencePod: {
    name: string;
    kind: string;
  }[] = [];

  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = [];

  constructor(
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
    private regionService: RegionService,
  ) {}

  async ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.name = params['name'];
    const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = workspaceParams['cluster'];
    this.namespaceName = workspaceParams['namespace'];
    await Promise.all([
      this.getPvcDetail(),
      this.getPvcReferenceBy(),
      this.getMirrorClusters(),
    ]);
    this.initialized = true;
  }

  canShowUpdate(item: K8sResourceWithActions<PersistentVolumeClaim>) {
    return (
      this.roleUtil.resourceHasPermission(
        item,
        'persistentvolumeclaim',
        'update',
      ) && !this.mirrorClusters.length
    );
  }

  canShowDelete(item: K8sResourceWithActions<PersistentVolumeClaim>) {
    return this.roleUtil.resourceHasPermission(
      item,
      'persistentvolumeclaim',
      'delete',
    );
  }

  getPvcYaml(data: PersistentVolumeClaim) {
    this.pvcYaml = this.formToYaml(data);
  }

  formToYaml(formModel: PersistentVolumeClaim) {
    try {
      const json = cloneDeep(formModel);
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  async getPvcDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail<
        PersistentVolumeClaim
      >(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        name: this.name,
        namespace: this.namespaceName,
      });
      this.pvc = result;
      this.getPvcYaml(result.kubernetes);
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate(['pvc'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }

  async getPvcReferenceBy() {
    try {
      const res = await this.k8sResourceService.getK8sTopologyByResource({
        cluster: this.clusterName,
        namespace: this.namespaceName,
        kind: 'persistentvolumeclaim',
        name: this.name,
      });
      this.referencePod = Object.values(res.nodes)
        .filter((item: KubernetesResource) => {
          return PodControllerKinds.includes(item.kind);
        })
        .map((item: KubernetesResource) => {
          return {
            name: item.metadata.name,
            kind: item.kind,
          };
        });
    } catch {}
  }

  jumpToListPage() {
    this.router.navigate(['pvc'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  update() {
    this.router.navigate(['pvc', 'update', this.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
  async delete() {
    const result = this.pvc;
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('delete_pvc_content', {
          name: result.kubernetes.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.loading = true;
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        result.kubernetes,
      );
      this.auiNotificationService.success({
        title: this.translate.get('success'),
        content: this.translate.get('pvc_delete_success'),
      });
      this.jumpToListPage();
      this.loading = false;
    } catch (e) {
      this.loading = false;
      this.errorsToastService.error(e);
    }
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror) {
      this.mirrorClusters = cluster.mirror.regions || [];
    }
  }
}
