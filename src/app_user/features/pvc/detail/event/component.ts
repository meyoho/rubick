import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { Pvc } from 'app/services/api/storage.service';

interface Filters {
  or: {
    and: {
      term?: {
        kind?: string;
        name?: string;
      };
      regexp?: {
        name: string;
      };
    }[];
  }[];
}
@Component({
  selector: 'rc-pvc-detail-event',
  templateUrl: 'template.html',
})
export class PvcDetailEventComponent implements OnInit, OnChanges {
  @Input()
  appData: Pvc;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  initialized = false;
  empty = false;
  filters: Filters = {
    or: [],
  };
  filterString: string;
  name: string;
  pvc: Pvc;
  constructor() {}

  ngOnInit() {}
  ngOnChanges({ appData }: SimpleChanges) {
    if (appData && appData.currentValue) {
      this.extractAllResources();
      this.empty = !this.appData;
      this.initialized = true;
    }
  }

  extractAllResources() {
    this.name = this.appData.kubernetes.metadata.name;
    this.produceFilter(this.appData);
    this.filterString = JSON.stringify(this.filters);
  }

  produceFilter(item: Pvc) {
    this.filters.or = [
      {
        and: [
          {
            term: {
              kind: item.kubernetes.kind,
            },
          },
          {
            term: {
              name: item.kubernetes.metadata.name,
            },
          },
        ],
      },
    ];
  }
}
