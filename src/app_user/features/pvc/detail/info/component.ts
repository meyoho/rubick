import { Component, Input, OnInit } from '@angular/core';
import { Pvc } from 'app/services/api/storage.service';
import {
  ACCESS_MODES,
  PVC_STATUS_MAP,
} from 'app2/features/storage/storage.constant';

@Component({
  selector: 'rc-pvc-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class PvcDetailInfoComponent implements OnInit {
  @Input()
  data: Pvc['kubernetes'];
  statusMap = PVC_STATUS_MAP;
  accessModes = ACCESS_MODES;
  constructor() {}
  ngOnInit() {}
}
