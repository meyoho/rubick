import { Component, Input, OnInit } from '@angular/core';
@Component({
  selector: 'rc-pvc-detail-pod',
  templateUrl: 'template.html',
})
export class PvcDetailPodComponent implements OnInit {
  @Input()
  data: {
    name: string;
    kind: string;
  }[];
  columns = ['pod_name', 'pod_type'];
  constructor() {}
  ngOnInit() {}
}
