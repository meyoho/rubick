import { NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Input,
  OnInit,
  ViewChild,
} from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import {
  BatchRequest,
  K8sResourceService,
} from 'app/services/api/k8s-resource.service';
import {
  Cluster,
  ClusterSelect,
  RegionService,
} from 'app/services/api/region.service';
import { Pvc } from 'app/services/api/storage.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { PersistentVolumeClaim } from 'app/typings/raw-k8s';
import { BatchReponse } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { safeDump, safeLoad } from 'js-yaml';
import { cloneDeep } from 'lodash-es';

@Component({
  selector: 'rc-pvc-form-container',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvcFormContainerComponent implements OnInit {
  @Input()
  data: Pvc;
  @ViewChild(NgForm)
  form: NgForm;
  clusterName: string;
  namespaceName: string;
  private RESOURCE_TYPE = 'persistentvolumeclaims';
  pvcYaml: string;
  originalYaml: string;
  initialized = false;
  submitting = false;
  formModel = 'UI';
  pvcModel: PersistentVolumeClaim;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };
  mirrorClusters: ClusterSelect[] = [];
  currentCluster: string;
  clusters: string[];
  mirrorResource: BatchReponse;
  resourceCreateTip = 'pvc_create_success';
  resourceUpdateTip = 'pvc_update_success';
  mirrorResourceCreateTip = 'cluster_pvc_create';
  mirrorResourceUpdateTip = 'cluster_pvc_update';

  constructor(
    private workspaceComponent: WorkspaceComponent,
    private location: Location,
    private auiNotificationService: NotificationService,
    private k8sResourceService: K8sResourceService,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private translateService: TranslateService,
    private regionService: RegionService,
    private cdr: ChangeDetectorRef,
  ) {}

  async ngOnInit() {
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    await this.getMirrorClusters();
    await this.updateEntrance();
    this.initialized = true;
    this.cdr.markForCheck();
  }

  async updateEntrance() {
    if (this.data) {
      this.pvcModel = this.data.kubernetes;
      await this.getMirrorResource(this.pvcModel.metadata.name);
    } else {
      this.pvcModel = this.genDefaultPvc();
    }
  }

  jumpToListPage() {
    this.router.navigate(['pvc'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        this.originalYaml = this.pvcYaml = this.formToYaml(this.pvcModel);
        break;
      case 'UI':
        this.pvcModel = this.yamlToForm(this.pvcYaml);
        break;
    }
  }

  formChange(form: PersistentVolumeClaim) {
    this.pvcModel = form;
  }

  formToYaml(formModel: any) {
    try {
      const yaml = safeDump(formModel);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  yamlToForm(yaml: string) {
    let formModel: any;
    try {
      formModel = safeLoad(yaml);
    } catch (e) {
      this.auiNotificationService.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return formModel;
  }

  async confirm() {
    this.form.onSubmit(null);
    if (!this.form.valid) {
      return;
    }
    this.submitting = true;
    try {
      const payload = this.genPayload();
      if (!payload) {
        this.submitting = false;
        return;
      }
      if (this.mirrorClusters.length) {
        // 联邦集群下的form提交
        await this.confirmMirror(payload);
      } else {
        // 非联邦集群下的form提交
        await this.confirmNotMirror(payload);
      }
      this.jumpToListPage();
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.submitting = false;
      this.cdr.markForCheck();
    }
  }

  private async confirmMirror(payload: PersistentVolumeClaim) {
    let res, notifyContent;
    if (this.data) {
      // update service
      notifyContent = this.mirrorResourceUpdateTip;
      const batchRequest: BatchRequest[] = [];
      delete payload.metadata.uid;
      delete payload.metadata.selfLink;
      Object.entries(this.mirrorResource.responses).map(item => {
        if (item[1] && item[1].code < 300 && this.clusters.includes(item[0])) {
          try {
            const body = JSON.parse(item[1].body);
            const itemPayload = cloneDeep(payload);
            itemPayload.metadata.resourceVersion =
              body.kubernetes.metadata.resourceVersion;
            batchRequest.push({
              cluster: item[0],
              payload: itemPayload,
            } as BatchRequest);
          } catch (e) {}
        }
      });
      res = await this.k8sResourceService.updateK8sReourceBatchWithMirrorBody(
        this.RESOURCE_TYPE,
        batchRequest,
      );
    } else {
      // create service
      notifyContent = this.mirrorResourceCreateTip;
      res = await this.k8sResourceService.createK8sReourceBatch(
        this.clusters,
        this.RESOURCE_TYPE,
        payload,
      );
    }
    this.k8sResourceService.reourceBatchNotification(
      notifyContent,
      res,
      payload.metadata.name,
    );
  }

  private async confirmNotMirror(payload: PersistentVolumeClaim) {
    let notifyContent;
    if (this.data) {
      // update service
      notifyContent = this.resourceUpdateTip;
      delete payload.metadata.resourceVersion;
      await this.k8sResourceService.updateK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        payload,
      );
    } else {
      // create service
      notifyContent = this.resourceCreateTip;
      await this.k8sResourceService.createK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        payload,
      );
    }
    this.auiNotificationService.success({
      title: this.translateService.get('success'),
      content: this.translateService.get(notifyContent, {
        name: payload.metadata.name || '',
      }),
    });
  }

  cancel() {
    this.location.back();
  }

  genPayload() {
    if (this.formModel === 'YAML') {
      return this.genK8sPvcFromYaml();
    }
    return this.pvcModel;
  }

  genK8sPvcFromYaml(): PersistentVolumeClaim {
    let sampleYaml: PersistentVolumeClaim;
    try {
      sampleYaml = safeLoad(this.pvcYaml) || {};
      if (sampleYaml && sampleYaml.metadata) {
        sampleYaml.metadata.namespace = this.namespaceName;
      }
    } catch (err) {
      this.auiNotificationService.error(
        this.translateService.get('sample_error'),
      );
      return;
    }
    return sampleYaml;
  }

  genDefaultPvc(): PersistentVolumeClaim {
    return {
      apiVersion: 'v1',
      kind: 'PersistentVolumeClaim',
      metadata: {
        name: '',
        namespace: this.namespaceName,
        annotations: {},
        labels: {},
      },
      spec: {
        storageClassName: 'no_use',
        accessModes: ['ReadWriteOnce'],
        resources: {
          requests: {
            storage: '',
          },
        },
        selector: {
          matchLabels: {},
        },
      },
    };
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror && cluster.mirror.regions) {
      this.mirrorClusters = cluster.mirror.regions;
      this.clusters = this.mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
      this.currentCluster = this.mirrorClusters.find(item => {
        return item.name === this.clusterName;
      }).name;
    } else {
      this.clusters = [this.clusterName];
    }
  }

  private async getMirrorResource(name: string) {
    try {
      this.mirrorResource = await this.k8sResourceService.getMirrorResources(
        this.clusters,
        this.namespaceName,
        this.RESOURCE_TYPE,
        name,
      );
      await this.k8sResourceService.currentClusterResponseStatus(
        this.mirrorResource,
        this.clusterName,
      );
    } catch (err) {
      this.errorsToastService.error(err);
      this.jumpToListPage();
    }
  }
}
