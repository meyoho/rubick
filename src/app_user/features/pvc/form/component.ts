import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  OnInit,
} from '@angular/core';
import { Validators } from '@angular/forms';

import { BaseResourceFormGroupComponent } from 'ng-resource-form-util';

import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { StorageClass } from 'app/services/api/storage.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  KubernetesResource,
  PersistentVolumeClaim,
  PersistentVolumeClaimSpec,
  PersistentVolumeClaimSpecMeta,
} from 'app/typings/raw-k8s';
import { K8S_SERVICE_NAME, POSITIVE_INT_PATTERN } from 'app/utils/patterns';
import { ACCESS_MODES } from 'app2/features/storage/storage.constant';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

interface PersistentVolumeClaimSpecFormModel
  extends PersistentVolumeClaimSpecMeta {
  accessModes: string;
}

interface PersistentVolumeClaimFormModel extends KubernetesResource {
  apiVersion: string;
  kind: string;
  spec: PersistentVolumeClaimSpecFormModel;
  status?: {
    phase: string;
  };
}

@Component({
  selector: 'rc-pvc-form',
  templateUrl: 'template.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class PvcFormComponent extends BaseResourceFormGroupComponent
  implements OnInit {
  clusterName: string;
  namespaceName: string;
  resourceNameReg = K8S_SERVICE_NAME;
  accessModes = Object.keys(ACCESS_MODES);
  storageClassNameList: {
    key: string;
    value: string;
  }[] = [];
  accessModesMap = ACCESS_MODES;
  POSITIVE_INT_PATTERN = POSITIVE_INT_PATTERN;
  unit = 'G';

  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };

  constructor(
    injector: Injector,
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private translateService: TranslateService,
  ) {
    super(injector);
  }

  ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.storageClassNameList = [
      {
        key: this.translateService.get('storageclass_nonuse'),
        value: 'no_use',
      },
      {
        key: this.translateService.get('storageclass_use_default'),
        value: 'use_default',
      },
    ];
    this.getStorageClass();
  }

  createForm() {
    const metadataForm = this.fb.group({
      name: this.fb.control('', [
        Validators.required,
        Validators.pattern(this.resourceNameReg.pattern),
      ]),
      namespace: this.fb.control('', [Validators.required]),
      labels: this.fb.control({}),
      annotations: this.fb.control({}),
    });
    const specForm = this.fb.group({
      storageClassName: this.fb.control(''),
      accessModes: this.fb.control([]),
      resources: this.fb.group({
        requests: this.fb.group({
          storage: this.fb.control(''),
        }),
      }),
      selector: this.fb.group({
        matchLabels: this.fb.control({}),
      }),
    });
    return this.fb.group({
      apiVersion: this.fb.control(''),
      kind: this.fb.control(''),
      metadata: metadataForm,
      spec: specForm,
    });
  }

  getDefaultFormModel(): PersistentVolumeClaimFormModel {
    return {
      apiVersion: 'v1',
      kind: 'PersistentVolumeClaim',
      metadata: {
        name: '',
        namespace: this.namespaceName,
        annotations: {},
        labels: {},
      },
      spec: {
        storageClassName: 'no_use',
        accessModes: 'ReadWriteOnce',
        resources: {
          requests: {
            storage: '',
          },
        },
        selector: {
          matchLabels: {},
        },
      },
    };
  }

  adaptResourceModel(
    resource: PersistentVolumeClaim,
  ): PersistentVolumeClaimFormModel {
    if (resource && resource.spec) {
      const spec: PersistentVolumeClaimSpecFormModel = {
        ...resource.spec,
        accessModes: resource.spec.accessModes[0] || 'ReadWriteOnce',
        resources: {
          requests: {
            storage: resource.spec.resources.requests.storage,
          },
        },
      };
      if (!spec.storageClassName) {
        switch (spec.storageClassName) {
          case '':
            spec.storageClassName = 'no_use';
            break;
          case null:
            spec.storageClassName = 'use_default';
            break;
        }
      }
      if (spec.resources.requests.storage) {
        const res = spec.resources.requests.storage.match(
          /[a-zA-Z]+|[0-9]+(?:\.[0-9]+|)/g,
        );
        spec.resources.requests.storage = res[0] || '';
        this.unit = res[1] || 'G';
      }
      return { ...resource, spec };
    }
  }

  adaptFormModel(
    formModel: PersistentVolumeClaimFormModel,
  ): PersistentVolumeClaim {
    const spec: PersistentVolumeClaimSpec = {
      ...formModel.spec,
      accessModes: [formModel.spec.accessModes],
      resources: {
        requests: {
          storage: formModel.spec.resources.requests.storage,
        },
      },
    };
    if (
      spec.selector &&
      !(
        spec.selector.matchLabels &&
        Object.keys(spec.selector.matchLabels).length
      )
    ) {
      delete spec.selector;
    }
    switch (spec.storageClassName) {
      case 'no_use':
        spec.storageClassName = '';
        break;
      case 'use_default':
        spec.storageClassName = null;
        break;
    }
    if (spec.resources.requests.storage) {
      spec.resources.requests.storage = `${spec.resources.requests.storage}${
        this.unit
      }`;
    }
    return { ...formModel, spec };
  }

  async getStorageClass() {
    const storageClass = await this.k8sResourceService.getK8sResourcesPaged(
      'storageclasses',
      {
        clusterName: this.clusterName,
        page: 1,
        page_size: 100,
      },
    );
    storageClass.results.map((item: StorageClass) => {
      this.storageClassNameList.push({
        key: item.kubernetes.metadata.name,
        value: item.kubernetes.metadata.name,
      });
    });
    this.cdr.markForCheck();
  }
}
