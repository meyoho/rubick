import { DialogService, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { Cluster, RegionService } from 'app/services/api/region.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import {
  MirrorPvc,
  Pvc,
  StorageService,
} from 'app/services/api/storage.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { PVC_STATUS_MAP } from 'app2/features/storage/storage.constant';
import { ResourceList } from 'app_user/core/types';
import { Observable, Subject, from } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments } from 'app/typings';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';

@Component({
  templateUrl: './pvc-list.component.html',
  styleUrls: ['./pvc-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ResourceListPermissionsService],
})
export class PvcListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  statusMap = PVC_STATUS_MAP;
  hasCreatePvcPermission = false;
  initialized: boolean;
  visable = true;
  RESOURCE_TYPE = 'persistentvolumeclaims';

  columns = ['name', 'status', 'connect_pv', 'size', 'created_date', 'action'];
  private onDestroy$ = new Subject<void>();
  private projectName: string;
  private namespaceName: string;
  private clusterName: string;

  mirrorClusters: {
    name: string;
    display_name: string;
  }[] = [];
  clusters: string[];
  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private storageService: StorageService,
    private translate: TranslateService,
    private errorsToastService: ErrorsToastService,
    private auiNotificationService: NotificationService,
    private roleUtil: RoleUtilitiesService,
    private regionService: RegionService,
    private dialogService: DialogService,
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    public resourcePermissionService: ResourceListPermissionsService,
    @Inject(ENVIRONMENTS) public env: Environments,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.projectName = params['project'];
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.resourcePermissionService.initContext({
      context: {
        project_name: params.project,
        cluster_name: params.cluster,
        namespace_name: params.namespace,
      },
    });
    this.roleUtil
      .resourceTypeSupportPermissions(
        'persistentvolumeclaim',
        {
          project_name: this.projectName,
          namespace_name: this.namespaceName,
          cluster_name: this.clusterName,
        },
        'create',
      )
      .then(res => (this.hasCreatePvcPermission = res));

    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
    this.getMirrorClusters();
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.k8sResourceService.getK8sResourcesPaged(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
        namespace: this.namespaceName,
      }),
    );
  }

  trackByFn(_index: number, item: Pvc) {
    return item.kubernetes.metadata.uid;
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  createPvc() {
    this.router.navigate(['pvc', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  updatePvc(item: Pvc) {
    this.router.navigate(['pvc', 'update', item.kubernetes.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  showDetail(item: Pvc) {
    this.router.navigate(['pvc', 'detail', item.kubernetes.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  pageChanged(params: string, value: number) {
    this.onPageEvent({
      [params]: value,
    });
  }

  hasPermission(item: any, action: string) {
    return this.roleUtil.resourceHasPermission(
      item,
      'persistentvolumeclaim',
      action,
    );
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror) {
      this.mirrorClusters = cluster.mirror.regions || [];
    }
  }

  async deletePvc(pvc: Pvc) {
    const mirrorPvcs: MirrorPvc[] = await this.storageService.getMirrorPvcs(
      this.clusterName,
      this.namespaceName,
      pvc.kubernetes.metadata.name,
    );
    if (mirrorPvcs.length) {
      const requireSelect = mirrorPvcs.filter((item: any) => {
        return item.uuid === pvc.kubernetes.metadata.uid;
      });
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      const mirrorPvcMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: mirrorPvcs,
        confirmTitle: this.translate.get('delete_pvc_title'),
        confirmLabel: this.translate.get('cluster'),
        confirmContent: this.translate.get('delete_pvc_content', {
          name: pvc.kubernetes.metadata.name,
        }),
      };
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorPvcMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorPvc[]) => {
          modalRef.close();
          if (res) {
            try {
              await this.storageService.deleteMirrorPvc(
                pvc.kubernetes.metadata.name,
                res,
              );
              this.onUpdate(null);
            } catch (err) {
              this.errorsToastService.error(err);
            }
          }
        });
    } else {
      try {
        await this.dialogService.confirm({
          title: this.translate.get('delete_pvc_title'),
          content: this.translate.get('delete_pvc_content', {
            name: pvc.kubernetes.metadata.name,
          }),
          confirmText: this.translate.get('confirm'),
          cancelText: this.translate.get('cancel'),
        });
      } catch (e) {
        return false;
      }

      try {
        await this.k8sResourceService.deleteK8sReource(
          this.clusterName,
          this.RESOURCE_TYPE,
          pvc.kubernetes,
        );
        this.auiNotificationService.success(
          this.translate.get('pvc_delete_success'),
        );
        this.onUpdate(null);
      } catch (err) {
        this.errorsToastService.error(err);
      }
    }
  }
}
