import { NgModule } from '@angular/core';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { PvcCreateComponent } from 'app_user/features/pvc/create/component';
import { PvcDetailComponent } from 'app_user/features/pvc/detail/component';
import { PvcDetailEventComponent } from 'app_user/features/pvc/detail/event/component';
import { PvcDetailInfoComponent } from 'app_user/features/pvc/detail/info/component';
import { PvcDetailPodComponent } from 'app_user/features/pvc/detail/pod/component';
import { PvcFormContainerComponent } from 'app_user/features/pvc/form-container/component';
import { PvcFormComponent } from 'app_user/features/pvc/form/component';
import { PvcListComponent } from 'app_user/features/pvc/list/pvc-list.component';
import { PvcRoutingModule } from 'app_user/features/pvc/pvc.routing.module';
import { PvcUpdateComponent } from 'app_user/features/pvc/update/component';

@NgModule({
  imports: [SharedModule, EventSharedModule, PvcRoutingModule, FormTableModule],
  declarations: [
    PvcListComponent,
    PvcFormComponent,
    PvcFormContainerComponent,
    PvcCreateComponent,
    PvcUpdateComponent,
    PvcDetailComponent,
    PvcDetailEventComponent,
    PvcDetailInfoComponent,
    PvcDetailPodComponent,
  ],
})
export class PvcModule {}
