import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { PvcCreateComponent } from 'app_user/features/pvc/create/component';
import { PvcDetailComponent } from 'app_user/features/pvc/detail/component';
import { PvcListComponent } from 'app_user/features/pvc/list/pvc-list.component';
import { PvcUpdateComponent } from 'app_user/features/pvc/update/component';

const routes: Routes = [
  {
    path: '',
    component: PvcListComponent,
  },
  {
    path: 'create',
    component: PvcCreateComponent,
  },
  {
    path: 'detail/:name',
    component: PvcDetailComponent,
  },
  {
    path: 'update/:name',
    component: PvcUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class PvcRoutingModule {}
