import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { K8sResourceWithActions, PersistentVolumeClaim } from 'app/typings';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  templateUrl: 'template.html',
})
export class PvcUpdateComponent implements OnInit {
  private RESOURCE_TYPE = 'persistentvolumeclaims';
  clusterName: string;
  namespaceName: string;
  data: K8sResourceWithActions<PersistentVolumeClaim>;
  name: string;
  constructor(
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private errorsToastService: ErrorsToastService,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.name = params['name'];
    const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = workspaceParams['cluster'];
    this.namespaceName = workspaceParams['namespace'];
    this.getPvcDetail();
  }

  async getPvcDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail<
        PersistentVolumeClaim
      >(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        name: this.name,
        namespace: this.namespaceName,
      });
      this.data = result;
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate(['pvc'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
  jumpToListPage() {
    this.router.navigate(['pvc'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }
}
