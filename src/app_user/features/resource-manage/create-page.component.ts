import { Component } from '@angular/core';

@Component({
  template: `
    <rc-resource-create></rc-resource-create>
  `,
})
export class ResourceCreatePageComponent {}
