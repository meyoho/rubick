import { Component } from '@angular/core';

@Component({
  template: `
    <rc-resource-management-list></rc-resource-management-list>
  `,
})
export class ResourceListPageComponent {}
