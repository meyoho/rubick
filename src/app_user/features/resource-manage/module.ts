import { NgModule } from '@angular/core';
import { ResourceManageSharedModule } from 'app/features-shared/resource-manage/module';
import { ResourceCreatePageComponent } from 'app_user/features/resource-manage/create-page.component';
import { ResourceListPageComponent } from 'app_user/features/resource-manage/list-page.component';
import { ResourceManageRoutingModule } from 'app_user/features/resource-manage/routing';

@NgModule({
  imports: [ResourceManageRoutingModule, ResourceManageSharedModule],
  declarations: [ResourceCreatePageComponent, ResourceListPageComponent],
})
export class ResourceManageModule {}
