import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { ResourceCreatePageComponent } from 'app_user/features/resource-manage/create-page.component';
import { ResourceListPageComponent } from 'app_user/features/resource-manage/list-page.component';

const routes: Routes = [
  {
    path: '',
    pathMatch: 'full',
    redirectTo: 'list',
  },
  {
    path: 'list',
    component: ResourceListPageComponent,
  },
  {
    path: 'create',
    component: ResourceCreatePageComponent,
  },
  {
    path: 'update',
    component: ResourceCreatePageComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ResourceManageRoutingModule {}
