import { DialogService, NotificationService } from '@alauda/ui';
import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { first } from 'rxjs/operators';

import {
  K8sResourceService,
  MirrorResource,
} from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { AlternateBackend, K8sResourceWithActions, Route } from 'app/typings';
import * as codeEditorUtil from 'app/utils/code-editor-config';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import * as jsyaml from 'js-yaml';
import { cloneDeep } from 'lodash-es';

@Component({
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class RouteDetailComponent implements OnInit {
  clusterName: string;
  namespaceName: string;
  name: string;
  yaml: string;
  private RESOURCE_TYPE = 'routes';
  codeEditorOptions = codeEditorUtil.yamlOptions;
  actionsConfigView = codeEditorUtil.viewActions;
  initialized = false;
  loading = false;
  route: K8sResourceWithActions<Route>;
  alternateBackends: AlternateBackend[] = [];

  mirrorResource: MirrorResource[] = [];

  constructor(
    private workspaceComponent: WorkspaceComponent,
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private roleUtil: RoleUtilitiesService,
    private errorsToastService: ErrorsToastService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private auiNotificationService: NotificationService,
  ) {}

  ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    this.name = params['name'];
    const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = workspaceParams['cluster'];
    this.namespaceName = workspaceParams['namespace'];
    Promise.all([this.getRouteDetail(), this.getMirrorResource()])
      .then(() => {
        const spec = this.route.kubernetes.spec;
        this.alternateBackends = spec.alternateBackends
          ? [spec.to].concat(spec.alternateBackends)
          : [spec.to];
      })
      .finally(() => {
        this.initialized = true;
      });
  }

  canShowUpdate(item: K8sResourceWithActions<Route>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'update');
  }

  canShowDelete(item: K8sResourceWithActions<Route>) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'delete');
  }

  getYaml(data: Route) {
    this.yaml = this.formToYaml(data);
  }

  formToYaml(formModel: Route) {
    try {
      const json = cloneDeep(formModel);
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  async getRouteDetail() {
    try {
      const result = await this.k8sResourceService.getK8sReourceDetail(
        this.RESOURCE_TYPE,
        {
          clusterName: this.clusterName,
          name: this.name,
          namespace: this.namespaceName,
        },
      );
      this.route = result;
      this.getYaml(result.kubernetes);
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate(['route'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }

  jumpToListPage() {
    this.router.navigate(['route'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  update() {
    this.router.navigate(['route', 'update', this.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  async delete() {
    if (this.mirrorResource.length) {
      this.deleteMirror(this.route.kubernetes);
    } else {
      this.deleteNotMirror(this.route.kubernetes);
    }
  }

  async deleteMirror(resource: Route) {
    const requireSelect = this.mirrorResource.filter((item: MirrorResource) => {
      return item.uuid === resource.metadata.uid;
    });
    const mirrorClusterMeta = {
      displayName: 'cluster_display_name',
      mirrorResource: this.mirrorResource,
      confirmTitle: this.translate.get('delete_route'),
      confirmLabel: this.translate.get('cluster'),
      confirmContent: this.translate.get(
        'delete_selected_cluster_route_confirm',
        {
          configmap_name: resource.metadata.name,
        },
      ),
    };
    const modalRef = await this.dialogService.open(
      MirrorResourceDeleteConfirmComponent,
    );
    modalRef.componentInstance.setMirrorResourceMeta(
      mirrorClusterMeta,
      requireSelect,
    );
    modalRef.componentInstance.initialized = true;
    modalRef.componentInstance.close
      .pipe(first())
      .subscribe(async (res: MirrorResource[]) => {
        modalRef.close();
        if (res) {
          const clusters = res.map((item: MirrorResource) => {
            return item.cluster_name;
          });
          try {
            const res = await this.k8sResourceService.deleteK8sReourceBatch(
              clusters,
              this.RESOURCE_TYPE,
              resource,
            );
            this.k8sResourceService.reourceBatchNotification(
              'cluster_route_delete',
              res,
              resource.metadata.name,
            );
            this.jumpToListPage();
          } catch (err) {
            this.errorsToastService.error(err);
          }
        }
      });
  }

  async deleteNotMirror(resource: Route) {
    try {
      await this.dialogService.confirm({
        title: this.translate.get('delete'),
        content: this.translate.get('delete_route_confirm', {
          name: resource.metadata.name,
        }),
        confirmText: this.translate.get('confirm'),
        cancelText: this.translate.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.loading = true;
    try {
      await this.k8sResourceService.deleteK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        resource,
      );
      this.auiNotificationService.success({
        title: this.translate.get('success'),
        content: this.translate.get('delete_route_success'),
      });
      this.jumpToListPage();
      this.loading = false;
    } catch (e) {
      this.loading = false;
      this.errorsToastService.error(e);
    }
  }

  async getMirrorResource() {
    this.mirrorResource = await this.k8sResourceService.getMirrorResourceList(
      this.clusterName,
      this.namespaceName,
      this.RESOURCE_TYPE,
      this.name,
    );
  }
}
