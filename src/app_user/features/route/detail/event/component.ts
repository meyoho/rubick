import {
  Component,
  Input,
  OnChanges,
  OnInit,
  SimpleChanges,
} from '@angular/core';
import { K8sResourceWithActions, Route } from 'app/typings';

interface Filters {
  or: {
    and: {
      term?: {
        kind?: string;
        name?: string;
      };
      regexp?: {
        name: string;
      };
    }[];
  }[];
}
@Component({
  selector: 'rc-route-detail-event',
  templateUrl: 'template.html',
})
export class RouteDetailEventComponent implements OnInit, OnChanges {
  @Input()
  appData: K8sResourceWithActions<Route>;
  @Input()
  cluster: string;
  @Input()
  namespace: string;

  initialized = false;
  empty = false;
  filters: Filters = {
    or: [],
  };
  filterString: string;
  name: string;
  route: Route;
  constructor() {}

  ngOnInit() {}
  ngOnChanges({ appData }: SimpleChanges) {
    if (appData && appData.currentValue) {
      this.extractAllResources();
      this.empty = !this.appData;
      this.initialized = true;
    }
  }

  extractAllResources() {
    this.name = this.appData.kubernetes.metadata.name;
    this.produceFilter(this.appData.kubernetes);
    this.filterString = JSON.stringify(this.filters);
  }

  produceFilter(item: Route) {
    this.filters.or = [
      {
        and: [
          {
            term: {
              kind: item.kind,
            },
          },
          {
            term: {
              name: item.metadata.name,
            },
          },
        ],
      },
    ];
  }
}
