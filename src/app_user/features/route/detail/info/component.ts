import { Component, Input, OnInit } from '@angular/core';
import { Route } from 'app/typings';

@Component({
  selector: 'rc-route-detail-info',
  templateUrl: 'template.html',
  styleUrls: ['style.scss'],
})
export class RouteDetailInfoComponent implements OnInit {
  @Input()
  data: Route;
  constructor() {}
  ngOnInit() {}
}
