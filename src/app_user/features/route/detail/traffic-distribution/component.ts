import { Component, Input } from '@angular/core';
import { AlternateBackend } from 'app/typings';

@Component({
  selector: 'rc-route-detail-traffic-distribution',
  templateUrl: 'template.html',
})
export class RouteDetailTrafficDistributionComponent {
  @Input()
  data: AlternateBackend[];
  columns = ['kube_service', 'weight'];
  constructor() {}

  getWeightsPercent(weight: number) {
    if (this.data) {
      const total = this.data.reduce(
        (total: number, item: AlternateBackend) => total + item.weight || 0,
        0,
      );
      return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
    }
    return '100%';
  }
}
