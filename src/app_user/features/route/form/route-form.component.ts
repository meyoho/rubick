import { DialogService, NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Input, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import * as jsyaml from 'js-yaml';
import { cloneDeep, merge } from 'lodash-es';

import { Domain, DomainService } from 'app/services/api/domain.service';
import {
  BatchRequest,
  K8sResourceService,
} from 'app/services/api/k8s-resource.service';
import {
  Cluster,
  ClusterSelect,
  RegionService,
} from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  AlternateBackend,
  K8sResourceWithActions,
  Route,
  Service,
  ServicePort,
} from 'app/typings';
import { K8S_RESOURCE_NAME_BASE } from 'app/utils/patterns';
import { BatchReponse, ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  selector: 'rc-route-form',
  templateUrl: 'route-form.component.html',
  styleUrls: ['route-form.component.scss'],
})
export class RouteFormComponent implements OnInit {
  @Input()
  data: K8sResourceWithActions<Route>;
  private clusterName: string;
  private namespaceName: string;
  private RESOURCE_TYPE = 'routes';
  routeYaml = '';
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };

  routeMeta = {
    domain: '',
    host: '',
    path: '',
  };
  domainList: string[] = [];
  extensiveDomainList: string[] = [];
  kubeServiceList: any[] = [];
  portMap = {};

  route: Route;
  baseRSReg = K8S_RESOURCE_NAME_BASE;
  submitting = false;
  initialized = false;
  formModel = 'UI';
  selector: any;
  mirrorClusters: ClusterSelect[] = [];
  currentCluster: string;
  clusters: string[];
  mirrorResource: BatchReponse;
  domainType = 'domain';
  trafficDistribution = false;

  @ViewChild('routeForm')
  routeForm: NgForm;

  constructor(
    private translateService: TranslateService,
    private router: Router,
    private location: Location,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private k8sResourceService: K8sResourceService,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    private domainService: DomainService,
    private regionService: RegionService,
  ) {
    this.route = this.genDefaultRoute();
  }

  ngOnInit() {
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    Promise.all([
      this.getHostList(),
      this.getKubeServiceList(),
      this.getMirrorClusters(),
    ])
      .then(() => {
        if (this.data && this.data.kubernetes) {
          const route = merge(this.genDefaultRoute(), this.data.kubernetes);
          this.route.spec = route.spec;
          this.route.metadata.name = route.metadata.name;
          this.route.metadata.resourceVersion = route.metadata.resourceVersion;
          this.genRouteForm();
          // 获取该资源名称在联邦集群的所有资源
          // 更新时可选联邦，应该是存在该资源名称的集群的集合
          return this.getMirrorResource(route.metadata.name).then(() => {
            const hasResourceClusters: string[] = [];
            Object.entries(this.mirrorResource.responses).map(item => {
              if (item[1] && item[1].code < 300) {
                hasResourceClusters.push(item[0]);
              }
            });
            this.mirrorClusters = this.mirrorClusters.filter(
              (item: ClusterSelect) => {
                return hasResourceClusters.includes(item.name);
              },
            );
            this.clusters = this.clusters.filter(item => {
              return hasResourceClusters.includes(item);
            });
            return;
          });
        }
        return;
      })
      .then(() => {
        this.route.metadata.namespace = this.namespaceName;
        this.initialized = true;
      });
  }

  async getHostList() {
    let domainList: ResourceList;
    try {
      domainList = await this.domainService.getDomains(false, {
        page: 1,
        page_size: 1000,
        search: '',
      });
    } catch (err) {
      return;
    }
    domainList.results.map((item: Domain) => {
      if (item.domain.startsWith('*.')) {
        this.extensiveDomainList.push(item.domain.substring(2));
      } else {
        this.domainList.push(item.domain);
      }
    });
  }

  genDefaultRoute() {
    return {
      apiVersion: 'route.openshift.io/v1',
      kind: 'Route',
      metadata: {
        name: '',
        namespace: '',
      },
      spec: {
        host: '',
        path: '',
        port: {
          targetPort: '',
        },
        to: {
          kind: 'Service',
          name: '',
          weight: 100,
        },
        wildcardPolicy: 'None',
      },
    };
  }

  getWeightsPercent(weight: number) {
    if (this.trafficDistribution && this.route.spec.alternateBackends) {
      const total = this.route.spec.alternateBackends.reduce(
        (total: number, item: AlternateBackend) => total + item.weight || 0,
        weight,
      );
      return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
    }
    return '100%';
  }

  setPort(event: string) {
    this.route.spec.port.targetPort = this.portMap[event][0];
  }

  trafficDistributionChange(event: boolean) {
    if (event) {
      this.route.spec['alternateBackends'] = [
        {
          kind: 'Service',
          name: '',
          weight: 100,
        },
      ];
    } else {
      delete this.route.spec.alternateBackends;
    }
  }

  async confirm() {
    this.routeForm.onSubmit(null);
    if (this.routeForm.invalid) {
      return;
    }

    try {
      let comfirmContent, comfirmTitle;
      if (this.data) {
        comfirmContent = 'update_route_confirm';
        comfirmTitle = 'update';
      } else {
        comfirmContent = 'create_route_confirm';
        comfirmTitle = 'create';
      }
      await this.dialogService.confirm({
        title: this.translateService.get(comfirmTitle),
        content: this.translateService.get(comfirmContent, {
          name: this.route.metadata.name,
        }),
        confirmText: this.translateService.get('confirm'),
        cancelText: this.translateService.get('cancel'),
      });
    } catch (e) {
      return;
    }

    this.submitting = true;
    let payload;
    try {
      payload = this.genRoute();
    } catch (err) {
      this.auiNotificationService.error({
        title: this.translateService.get('yaml_error'),
        content: err.message,
      });
      this.submitting = false;
      return;
    }

    try {
      if (this.mirrorClusters.length) {
        // 联邦集群下的form提交
        await this.confirmMirror(payload);
      } else {
        // 非联邦集群下的form提交
        await this.confirmNotMirror(payload);
      }
      this.router.navigate(['route'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.submitting = false;
    }
  }

  private async confirmMirror(payload: Route) {
    let res, notifyContent;
    if (this.data) {
      // update service
      notifyContent = 'cluster_route_update';
      const batchRequest: BatchRequest[] = [];
      Object.entries(this.mirrorResource.responses).map(item => {
        if (item[1] && item[1].code < 300 && this.clusters.includes(item[0])) {
          try {
            const body = JSON.parse(item[1].body);
            const itemPayload = cloneDeep(payload);
            itemPayload.metadata.resourceVersion =
              body.kubernetes.metadata.resourceVersion;
            batchRequest.push({
              cluster: item[0],
              payload: itemPayload,
            } as BatchRequest);
          } catch (e) {}
        }
      });
      res = await this.k8sResourceService.updateK8sReourceBatchWithMirrorBody(
        this.RESOURCE_TYPE,
        batchRequest,
      );
    } else {
      // create service
      notifyContent = 'cluster_route_create';
      res = await this.k8sResourceService.createK8sReourceBatch(
        this.clusters,
        this.RESOURCE_TYPE,
        payload,
      );
    }
    this.k8sResourceService.reourceBatchNotification(
      notifyContent,
      res,
      payload.metadata.name,
    );
  }

  private async confirmNotMirror(payload: Route) {
    let notifyContent;
    if (this.data) {
      // update service
      notifyContent = 'update_route_success';
      await this.k8sResourceService.updateK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        payload,
      );
    } else {
      // create service
      notifyContent = 'create_route_success';
      await this.k8sResourceService.createK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        payload,
      );
    }
    this.auiNotificationService.success({
      title: this.translateService.get('success'),
      content: this.translateService.get(notifyContent, {
        name: payload.metadata.name,
      }),
    });
  }

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        this.formatRoute();
        this.routeYaml = this.formToYaml(this.route);
        break;
      case 'UI':
        this.route = this.yamlToForm(this.routeYaml);
        this.genRouteForm();
        this.genServiceSelectForm();
        break;
    }
  }

  domainTypeChange() {
    this.routeMeta = {
      domain: '',
      host: '',
      path: '',
    };
  }

  private formToYaml(formModel: any) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.errorsToastService.error(err);
    }
  }

  private genRouteForm() {
    if (this.route.spec.path) {
      this.routeMeta.path = this.route.spec.path.substring(1);
    }
    this.trafficDistribution = !!(
      this.route.spec.alternateBackends &&
      this.route.spec.alternateBackends.length
    );
    if (!this.trafficDistribution) {
      delete this.route.spec.alternateBackends;
    }
    this.genDomainType();
  }
  //judge domainType and set view
  private genDomainType() {
    let findStrInExt;
    const rawDomainData = this.route.spec.host;
    //check whether belong to extensive_domain
    findStrInExt = this.extensiveDomainList.find(e => {
      return rawDomainData.endsWith(e);
    });
    if (!findStrInExt) {
      this.domainType = 'domain';
      this.routeMeta.host = this.route.spec.host;
      this.routeMeta.domain = '';
    } else {
      this.domainType = 'extensive_domain';
      this.routeMeta.host = findStrInExt;
      this.routeMeta.domain = rawDomainData.substring(
        0,
        rawDomainData.indexOf(findStrInExt) - 1,
      );
    }
  }
  private genServiceSelectForm() {
    if (this.kubeServiceList.length) {
      if (!this.kubeServiceList.includes(this.route.spec.to.name)) {
        this.route.spec.to.name = this.kubeServiceList[0];
      }
      if (
        !this.portMap[this.route.spec.to.name].includes(
          this.route.spec.port.targetPort,
        )
      ) {
        this.route.spec.port.targetPort = this.portMap[
          this.route.spec.to.name
        ][0];
      }
    }
  }

  private genRoute() {
    if (this.formModel === 'YAML') {
      return this.genRouteFromYaml();
    }
    return this.formatRoute();
  }

  private yamlToForm(yaml: string) {
    let formModel: any;
    try {
      formModel = jsyaml.safeLoad(yaml);
    } catch (e) {
      this.auiNotificationService.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return merge(this.genDefaultRoute(), formModel);
  }

  private genRouteFromYaml() {
    const route: any = jsyaml.safeLoad(this.routeYaml);
    if (route.metadata) {
      route.metadata.namespace = this.namespaceName;
    }
    return route;
  }

  private formatRoute() {
    this.route.spec.path = this.routeMeta.path ? `/${this.routeMeta.path}` : '';
    this.route.spec.host = this.routeMeta.domain
      ? `${this.routeMeta.domain}.${this.routeMeta.host}`
      : this.routeMeta.host;
    if (
      this.route.spec.alternateBackends &&
      !this.route.spec.alternateBackends
    ) {
      delete this.route.spec.alternateBackends;
    }
    return this.route;
  }

  cancel() {
    this.location.back();
  }

  async getKubeServiceList() {
    let k8sServiceList: K8sResourceWithActions<Service>[];
    try {
      k8sServiceList = await this.k8sResourceService.getK8sResources(
        'services',
        {
          clusterName: this.clusterName,
          namespace: this.namespaceName,
        },
      );
    } catch (err) {
      return;
    }
    k8sServiceList.map((item: K8sResourceWithActions<Service>) => {
      this.kubeServiceList.push(item.kubernetes.metadata.name);
      this.portMap[item.kubernetes.metadata.name] = item.kubernetes.spec.ports
        ? item.kubernetes.spec.ports.map((item: ServicePort) => {
            return item.name || item.targetPort || item.port;
          })
        : [];
    });
    if (this.kubeServiceList.length) {
      this.route.spec.to.name = this.kubeServiceList[0];
      this.route.spec.port.targetPort = this.portMap[
        this.kubeServiceList[0]
      ][0];
    }
  }

  private async getMirrorClusters() {
    let cluster: Cluster;
    try {
      cluster = await this.regionService.getCluster(this.clusterName);
    } catch (err) {
      return;
    }
    if (cluster.mirror && cluster.mirror.regions) {
      this.mirrorClusters = cluster.mirror.regions;
      this.clusters = this.mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
      this.currentCluster = this.mirrorClusters.find(item => {
        return item.name === this.clusterName;
      }).name;
    } else {
      this.clusters = [this.clusterName];
    }
  }

  private async getMirrorResource(name: string) {
    try {
      this.mirrorResource = await this.k8sResourceService.getMirrorResources(
        this.clusters,
        this.namespaceName,
        this.RESOURCE_TYPE,
        name,
      );
      await this.k8sResourceService.currentClusterResponseStatus(
        this.mirrorResource,
        this.clusterName,
      );
    } catch (err) {
      this.errorsToastService.error(err);
      this.router.navigate(['service'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
