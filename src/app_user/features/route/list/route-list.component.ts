import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Observable, Subject, from } from 'rxjs';
import { filter, first, takeUntil } from 'rxjs/operators';

import { DialogService, DialogSize, NotificationService } from '@alauda/ui';

import { ViewResourceYamlComponent } from 'app_user/shared/components/view-resource-yaml/view-resource-yaml.component';

import {
  K8sResourceService,
  MirrorResource,
} from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import { AlternateBackend, K8sResourceWithActions, Route } from 'app/typings';
import { PageParams, ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

import * as jsyaml from 'js-yaml';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';

interface RouteWithActions extends K8sResourceWithActions<Route> {
  services?: AlternateBackend[];
}
@Component({
  templateUrl: 'route-list.component.html',
  styleUrls: ['route-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ResourceListPermissionsService],
})
export class RouteListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<void>();
  private RESOURCE_TYPE = 'routes';
  private clusterName: string;
  private namespaceName: string;
  createEnabled: boolean;
  initialized: boolean;

  columns = ['name', 'domain', 'kube_service_name', 'weight', 'port', 'action'];

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private k8sResourceService: K8sResourceService,
    private workspaceComponent: WorkspaceComponent,
    public resourcePermissionService: ResourceListPermissionsService,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.resourcePermissionService.initContext({
      resourceType: 'k8s_others',
      context: {
        project_name: params.project,
        cluster_name: params.cluster,
        namespace_name: params.namespace,
      },
    });
    this.roleUtil
      .resourceTypeSupportPermissions(
        'k8s_others',
        {
          cluster_name: this.clusterName,
          namespace_name: this.namespaceName,
          project_name: params['project'],
        },
        'create',
      )
      .then(res => (this.createEnabled = res));

    this.list$
      .pipe(
        takeUntil(this.onDestroy$),
        filter(list => !!list),
      )
      .subscribe(list => {
        if (list.length) {
          list.forEach(_item => {
            _item.services = this.getServices(_item.kubernetes);
          });
          this.initialized = true;
        }
      });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.k8sResourceService.getK8sResourcesPaged(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        namespace: this.namespaceName,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
      }),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: RouteWithActions) {
    return item.kubernetes.metadata.uid;
  }

  createService() {
    this.router.navigate(['route', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  pageChanged(pageEvent: PageParams) {
    this.onPageEvent({
      pageIndex: ++pageEvent.pageIndex,
      pageSize: pageEvent.pageSize,
    });
  }

  canShowUpdate(item: RouteWithActions) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'update');
  }

  canShowDelete(item: RouteWithActions) {
    return this.roleUtil.resourceHasPermission(item, 'k8s_others', 'delete');
  }

  update(item: RouteWithActions) {
    this.router.navigate(['route', 'update', item.kubernetes.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  openDetail(item: RouteWithActions) {
    this.router.navigate(['route', 'detail', item.kubernetes.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  getServices(route: Route) {
    return route.spec.alternateBackends
      ? [route.spec.to].concat(route.spec.alternateBackends)
      : [route.spec.to];
  }

  getWeightsPercent(route: RouteWithActions, weight: number) {
    if (route.services) {
      const total = route.services.reduce(
        (total: number, item: AlternateBackend) => total + item.weight || 0,
        0,
      );
      return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
    }
    return '100%';
  }

  viewYaml(item: RouteWithActions) {
    try {
      const yaml = jsyaml.safeDump(item.kubernetes);
      this.dialogService.open(ViewResourceYamlComponent, {
        size: DialogSize.Big,
        data: {
          yaml: yaml,
        },
      });
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  async delete(route: RouteWithActions) {
    const mirrorService = await this.k8sResourceService.getMirrorResourceList(
      this.clusterName,
      this.namespaceName,
      this.RESOURCE_TYPE,
      route.kubernetes.metadata.name,
    );
    if (mirrorService.length) {
      // 联邦删除
      const requireSelect = mirrorService.filter((item: any) => {
        return item.uuid === route.kubernetes.metadata.uid;
      });
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: mirrorService,
        confirmTitle: this.translateService.get('delete_route'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'delete_selected_cluster_route_confirm',
          {
            name: route.kubernetes.metadata.name,
          },
        ),
      };
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorResource[]) => {
          modalRef.close();
          if (res) {
            const clusters = res.map((item: MirrorResource) => {
              return item.cluster_name;
            });
            try {
              const res = await this.k8sResourceService.deleteK8sReourceBatch(
                clusters,
                this.RESOURCE_TYPE,
                route.kubernetes,
              );
              this.k8sResourceService.reourceBatchNotification(
                'cluster_route_delete',
                res,
                route.kubernetes.metadata.name,
              );
              this.onUpdate(null);
            } catch (err) {
              this.errorsToastService.error(err);
            }
          }
        });
    } else {
      // 非联邦删除
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('delete'),
          content: this.translateService.get('delete_route_confirm', {
            name: route.kubernetes.metadata.name,
          }),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });
      } catch (e) {
        return;
      }
      try {
        await this.k8sResourceService.deleteK8sReource(
          this.clusterName,
          this.RESOURCE_TYPE,
          route.kubernetes,
        );
        this.auiNotificationService.success({
          title: this.translateService.get('success'),
          content: this.translateService.get('delete_route_success', {
            name: route.kubernetes.metadata.name,
          }),
        });
        this.onUpdate(null);
      } catch (e) {
        this.errorsToastService.error(e);
      }
    }
  }
}
