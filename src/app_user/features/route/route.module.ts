import { NgModule } from '@angular/core';
import { EventSharedModule } from 'app/features-shared/event/shared.module';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { RouteCreateComponent } from 'app_user/features/route/create/route-create.component';
import { RouteDetailComponent } from 'app_user/features/route/detail/component';
import { RouteDetailEventComponent } from 'app_user/features/route/detail/event/component';
import { RouteDetailInfoComponent } from 'app_user/features/route/detail/info/component';
import { RouteDetailTrafficDistributionComponent } from 'app_user/features/route/detail/traffic-distribution/component';
import { RouteFormComponent } from 'app_user/features/route/form/route-form.component';
import { RouteListComponent } from 'app_user/features/route/list/route-list.component';
import { RouteRoutingModule } from 'app_user/features/route/route.routing.module';
import { RouteServiceGroupFormComponent } from 'app_user/features/route/service-group/component';
import { RouteUpdateComponent } from 'app_user/features/route/update/route-update.component';

@NgModule({
  imports: [
    EventSharedModule,
    FormTableModule,
    SharedModule,
    RouteRoutingModule,
  ],
  declarations: [
    RouteListComponent,
    RouteFormComponent,
    RouteCreateComponent,
    RouteUpdateComponent,
    RouteServiceGroupFormComponent,
    RouteDetailEventComponent,
    RouteDetailInfoComponent,
    RouteDetailComponent,
    RouteDetailTrafficDistributionComponent,
  ],
  entryComponents: [],
})
export class RouteModule {}
