import {
  ChangeDetectionStrategy,
  Component,
  Injector,
  Input,
} from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';
import { Subject } from 'rxjs';

import { AlternateBackend } from 'app/typings';

@Component({
  selector: 'rc-route-service-group-form',
  templateUrl: './template.html',
  styleUrls: ['./styles.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class RouteServiceGroupFormComponent extends BaseResourceFormArrayComponent<
  AlternateBackend,
  AlternateBackend
> {
  onDestroy$ = new Subject<void>();
  @Input()
  serviceNameList: string[] = [];

  @Input()
  weight = 100;

  constructor(injector: Injector) {
    super(injector);
  }

  getDefaultFormModel(): AlternateBackend[] {
    return [
      {
        name: '',
        kind: 'Service',
        weight: 100,
      },
    ];
  }

  createForm() {
    return this.fb.array([]);
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    return this.fb.group({
      name: ['', Validators.required],
      kind: ['Service'],
      weight: [
        100,
        [Validators.required, Validators.max(100), Validators.min(0)],
      ],
    });
  }

  getWeightsPercent(weight: number) {
    const total = this.form.controls.reduce(
      (total: number, control: any) =>
        total + (control.get('weight').value || 0),
      this.weight,
    );
    return total ? ((weight / total) * 100).toFixed(2) + '%' : 'N/A';
  }

  disableAddBtn() {
    return this.form.controls.length >= 3;
  }
}
