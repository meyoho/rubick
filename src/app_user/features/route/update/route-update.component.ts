import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { K8sResourceService } from 'app/services/api/k8s-resource.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { K8sResourceWithActions, Service } from 'app/typings';

import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  template: `
    <rc-loading-mask
      class="rc-form-loading-mask"
      [loading]="!initialized"
    ></rc-loading-mask>
    <rc-route-form *ngIf="initialized" [data]="data"></rc-route-form>
  `,
})
export class RouteUpdateComponent implements OnInit {
  private clusterName: string;
  private namespaceName: string;
  private RESOURCE_TYPE = 'routes';
  initialized = false;
  data: K8sResourceWithActions<Service>;
  constructor(
    private k8sResourceService: K8sResourceService,
    private activatedRoute: ActivatedRoute,
    private router: Router,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
  ) {}
  async ngOnInit() {
    const params = this.activatedRoute.snapshot.params;
    const name = params['name'];
    try {
      const workspaceParams = this.workspaceComponent.baseParamsSnapshot;
      this.clusterName = workspaceParams['cluster'];
      this.namespaceName = workspaceParams['namespace'];
      this.data = await this.k8sResourceService.getK8sReourceDetail(
        this.RESOURCE_TYPE,
        {
          clusterName: this.clusterName,
          name,
          namespace: this.namespaceName,
        },
      );
      this.initialized = true;
    } catch (err) {
      this.errorsToastService.error(err);
      return this.router.navigate(['route'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
