import { ChangeDetectionStrategy, Component, Injector } from '@angular/core';
import { Validators } from '@angular/forms';
import { BaseResourceFormArrayComponent } from 'ng-resource-form-util';

import { ServicePort } from 'app/typings';

@Component({
  selector: 'rc-service-ports-fieldset',
  templateUrl: './service-ports-fieldset.component.html',
  styleUrls: ['service-ports-fieldset.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ServicePortsFieldsetComponent extends BaseResourceFormArrayComponent<
  ServicePort
> {
  constructor(injector: Injector) {
    super(injector);
  }

  createForm() {
    return this.fb.array([]);
  }

  getOnFormArrayResizeFn() {
    return () => this.createNewControl();
  }

  private createNewControl() {
    const validators = [
      Validators.required,
      Validators.min(1),
      Validators.max(65535),
    ];
    return this.fb.group({
      name: [''],
      protocol: ['TCP'],
      port: ['', validators],
      targetPort: ['', validators],
    });
  }

  adaptFormModel(formModel: ServicePort[]): ServicePort[] {
    return formModel.map((item: ServicePort) => {
      item.name = `${item.port}-${item.targetPort}`;
      return item;
    });
  }
}
