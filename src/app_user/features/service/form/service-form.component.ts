import { NotificationService } from '@alauda/ui';
import { Location } from '@angular/common';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';

import * as jsyaml from 'js-yaml';
import { cloneDeep, merge } from 'lodash-es';

import { AppService, WorkLoads } from 'app/services/api/app.service';
import {
  BatchRequest,
  K8sResourceService,
} from 'app/services/api/k8s-resource.service';
import {
  Cluster,
  ClusterSelect,
  RegionService,
} from 'app/services/api/region.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { TranslateService } from 'app/translate/translate.service';
import {
  K8sResourceWithActions,
  KubernetesResource,
  Service,
  ServicePort,
  StringMap,
} from 'app/typings';
import { K8S_SERVICE_NAME } from 'app/utils/patterns';
import { BatchReponse } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

@Component({
  selector: 'rc-service-create-form',
  templateUrl: './service-form.component.html',
})
export class ServiceCreateFormComponent implements OnInit, OnDestroy {
  @Input()
  data: K8sResourceWithActions<Service>;
  private namespaceName: string;
  private clusterName: string;
  private RESOURCE_TYPE = 'services';
  resourceNameReg = K8S_SERVICE_NAME;
  codeEditorOptions = {
    language: 'yaml',
    folding: true,
    minimap: { enabled: false },
    readOnly: false,
  };

  submitting = false;
  initialized = false;
  formModel = 'UI';
  typeList = [
    {
      key: 'HTTP(Cluster IP)',
      value: 'ClusterIP',
    },
    {
      key: 'TCP(Node Port)',
      value: 'NodePort',
    },
  ];
  k8sService: Service;
  selectedComponent: KubernetesResource;
  serviceYaml: string;
  mirrorClusters: ClusterSelect[] = [];
  currentCluster: string;
  clusters: string[];
  serviceMirrorResource: BatchReponse;
  workLoads: WorkLoads = {};
  keys = Object.keys;

  @ViewChild('serviceForm')
  serviceForm: NgForm;

  constructor(
    private translateService: TranslateService,
    private router: Router,
    private location: Location,
    private auiNotificationService: NotificationService,
    private k8sResourceService: K8sResourceService,
    private appService: AppService,
    private errorsToastService: ErrorsToastService,
    private workspaceComponent: WorkspaceComponent,
    private regionService: RegionService,
  ) {
    this.k8sService = this.genDefaultService();
  }

  async ngOnInit() {
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    try {
      await Promise.all([
        this.getAppList(this.clusterName, this.namespaceName),
        this.getMirrorClusters(),
      ]);
    } catch (err) {
      this.initialized = true;
      return;
    }
    if (this.data && this.data.kubernetes) {
      const { status, ...service } = this.data.kubernetes;
      this.k8sService = cloneDeep(service);
      this.genComponentSelector(this.k8sService);
      // 获取该资源名称在联邦集群的所有资源
      // 更新时可选联邦，应该是存在该资源名称的集群的集合
      await this.getMirrorResource(this.k8sService.metadata.name);
      const hasResourceClusters: string[] = [];
      Object.entries(this.serviceMirrorResource.responses).map(item => {
        if (item[1] && item[1].code < 300) {
          hasResourceClusters.push(item[0]);
        }
      });
      this.mirrorClusters = this.mirrorClusters.filter(
        (item: ClusterSelect) => {
          return hasResourceClusters.includes(item.name);
        },
      );
      this.clusters = this.clusters.filter(item => {
        return hasResourceClusters.includes(item);
      });
    }
    this.k8sService.metadata.namespace = this.namespaceName;
    this.initialized = true;
  }

  ngOnDestroy() {}

  genDefaultService() {
    const requirePort: ServicePort = {
      protocol: 'TCP',
      name: '',
      port: '',
      targetPort: '',
    };
    return {
      apiVersion: 'v1',
      kind: 'Service',
      metadata: {
        name: '',
        namespace: '',
      },
      spec: {
        type: 'ClusterIP',
        ports: [requirePort],
      },
    };
  }

  async confirm() {
    this.serviceForm.onSubmit(null);
    if (this.serviceForm.invalid) {
      return;
    }

    this.submitting = true;
    let payload: Service;
    try {
      payload = this.genPayload();
      if (this.checkDuplicatePort(payload)) {
        this.submitting = false;
        return;
      }
    } catch (err) {
      this.auiNotificationService.error({
        title: this.translateService.get('yaml_error'),
        content: err.message,
      });
      this.submitting = false;
      return;
    }

    try {
      if (this.mirrorClusters.length) {
        // 联邦集群下的form提交
        await this.confirmMirror(payload);
      } else {
        // 非联邦集群下的form提交
        await this.confirmNotMirror(payload);
      }
      this.router.navigate(['service'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    } catch (err) {
      this.errorsToastService.error(err);
    } finally {
      this.submitting = false;
    }
  }

  checkDuplicatePort(payload: Service) {
    const servicePortList = payload.spec.ports;
    const portList: number[] = [];
    let duplicatePort = '';
    servicePortList.map((item: ServicePort) => {
      if (portList.includes(item.port as number)) {
        duplicatePort = `${duplicatePort}${item.port},`;
      } else {
        portList.push(item.port as number);
      }
    });
    if (duplicatePort) {
      this.auiNotificationService.error({
        content: this.translateService.get('duplicate_service_port', {
          portList: duplicatePort,
        }),
      });
      return true;
    }
    return false;
  }

  private async confirmMirror(payload: Service) {
    let res, notifyContent;
    if (this.data) {
      // update service
      notifyContent = 'cluster_service_update';
      const batchRequest: BatchRequest[] = [];
      Object.entries(this.serviceMirrorResource.responses).map(item => {
        if (item[1] && item[1].code < 300 && this.clusters.includes(item[0])) {
          try {
            const body = JSON.parse(item[1].body);
            const itemPayload = cloneDeep(payload);
            itemPayload.metadata.resourceVersion =
              body.kubernetes.metadata.resourceVersion;
            itemPayload.spec.clusterIP = body.kubernetes.spec.clusterIP;
            batchRequest.push({
              cluster: item[0],
              payload: itemPayload,
            } as BatchRequest);
          } catch (e) {}
        }
      });
      res = await this.k8sResourceService.updateK8sReourceBatchWithMirrorBody(
        this.RESOURCE_TYPE,
        batchRequest,
      );
    } else {
      // create service
      notifyContent = 'cluster_service_create';
      res = await this.k8sResourceService.createK8sReourceBatch(
        this.clusters,
        this.RESOURCE_TYPE,
        payload,
      );
    }
    this.k8sResourceService.reourceBatchNotification(
      notifyContent,
      res,
      payload.metadata.name,
    );
  }

  private async confirmNotMirror(payload: Service) {
    let notifyContent;
    if (this.data) {
      // update service
      notifyContent = 'update_kube_service_success';
      await this.k8sResourceService.updateK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        payload,
      );
    } else {
      // create service
      notifyContent = 'create_kube_service_success';
      await this.k8sResourceService.createK8sReource(
        this.clusterName,
        this.RESOURCE_TYPE,
        payload,
      );
    }
    this.auiNotificationService.success({
      title: this.translateService.get('success'),
      content: this.translateService.get(notifyContent, {
        name: payload.metadata.name || '',
      }),
    });
  }

  formModelChange(event: string) {
    switch (event) {
      case 'YAML':
        this.serviceYaml = this.formToYaml(this.formatK8sService());
        break;
      case 'UI':
        this.k8sService = this.yamlToForm(this.serviceYaml);
        break;
    }
  }

  private formToYaml(formModel: any) {
    try {
      const json = JSON.parse(JSON.stringify(formModel));
      const yaml = jsyaml.safeDump(json);
      return yaml;
    } catch (err) {
      this.auiNotificationService.error(err.message);
    }
  }

  private yamlToForm(yaml: string) {
    let formModel: any;
    try {
      formModel = jsyaml.safeLoad(yaml);
    } catch (e) {
      this.auiNotificationService.error(e.message);
    }
    if (!formModel || formModel instanceof String) {
      formModel = {};
    }
    return merge(this.genDefaultService(), formModel);
  }

  private genComponentSelector(serviceDefinition: Service) {
    if (serviceDefinition.spec.selector) {
      Object.values(this.workLoads).forEach(
        (components: KubernetesResource[]) => {
          if (!this.selectedComponent) {
            this.selectedComponent = components.find(
              (component: KubernetesResource) => {
                return this.selectorMatch(
                  serviceDefinition.spec.selector,
                  component.spec.template.metadata.labels,
                );
              },
            );
          }
        },
      );
    }
  }

  /**
   * 根据 Service selector(类似 matchLabels) 匹配 labels 的规则，找到第一个匹配的 Workload
   */
  private selectorMatch(selector: StringMap, labels: StringMap) {
    if (!(selector && labels)) {
      return false;
    }
    return Object.keys(selector).every(key => selector[key] === labels[key]);
  }

  genPayload() {
    if (this.formModel === 'YAML') {
      return this.genK8sServiceFromYaml();
    }
    return this.formatK8sService();
  }

  genK8sServiceFromYaml() {
    const k8sService: any = jsyaml.safeLoad(this.serviceYaml);
    if (k8sService.metadata) {
      k8sService.metadata.namespace = this.namespaceName;
    }
    return k8sService;
  }

  formatK8sService() {
    const k8sService = cloneDeep(this.k8sService);
    if (this.selectedComponent) {
      k8sService.spec[
        'selector'
      ] = this.selectedComponent.spec.template.metadata.labels;
    } else {
      delete k8sService.spec.selector;
    }
    return k8sService;
  }

  cancel() {
    this.location.back();
  }

  async getAppList(clusterName: string, namespaceName: string) {
    try {
      this.workLoads = await this.appService.getWorkloads({
        cluster: clusterName,
        namespace: namespaceName,
      });
    } catch (err) {
      return;
    }
  }

  private async getMirrorClusters() {
    const cluster: Cluster = await this.regionService.getCluster(
      this.clusterName,
    );
    if (cluster.mirror && cluster.mirror.regions) {
      this.mirrorClusters = cluster.mirror.regions;
      this.clusters = this.mirrorClusters.map((item: { name: string }) => {
        return item.name;
      });
      this.currentCluster = this.mirrorClusters.find(item => {
        return item.name === this.clusterName;
      }).name;
    } else {
      this.clusters = [this.clusterName];
    }
  }

  private async getMirrorResource(name: string) {
    try {
      this.serviceMirrorResource = await this.k8sResourceService.getMirrorResources(
        this.clusters,
        this.namespaceName,
        this.RESOURCE_TYPE,
        name,
      );
      await this.k8sResourceService.currentClusterResponseStatus(
        this.serviceMirrorResource,
        this.clusterName,
      );
    } catch (err) {
      this.errorsToastService.error(err);
      this.router.navigate(['service'], {
        relativeTo: this.workspaceComponent.baseActivatedRoute,
      });
    }
  }
}
