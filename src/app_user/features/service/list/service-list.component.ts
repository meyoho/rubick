import { DialogService, DialogSize, NotificationService } from '@alauda/ui';
import { HttpClient } from '@angular/common/http';
import {
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  Inject,
  OnDestroy,
  OnInit,
} from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import * as jsyaml from 'js-yaml';
import { get } from 'lodash-es';
import { from, Observable, Subject } from 'rxjs';
import { first, takeUntil } from 'rxjs/operators';

import { BaseResourceListComponent, FetchParams } from 'app/abstract';
import {
  K8sResourceService,
  MirrorResource,
} from 'app/services/api/k8s-resource.service';
import { RoleUtilitiesService } from 'app/services/api/role-utilities.service';
import { ErrorsToastService } from 'app/services/errors-toast.service';
import { ResourceListPermissionsService } from 'app/services/resource-list-permissions.service';
import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import {
  Environments,
  K8sResourceWithActions,
  Service,
  ServiceSpecTypeMap,
} from 'app/typings';
import { PageParams, ResourceList } from 'app_user/core/types';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { MirrorResourceDeleteConfirmComponent } from 'app_user/shared/components/mirror-resource-delete-confirm/mirror-resource-delete-confirm.component';
import { ViewResourceYamlComponent } from 'app_user/shared/components/view-resource-yaml/view-resource-yaml.component';

@Component({
  templateUrl: './service-list.component.html',
  styleUrls: ['./service-list.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [ResourceListPermissionsService],
})
export class ServiceListComponent extends BaseResourceListComponent<any>
  implements OnInit, OnDestroy {
  private onDestroy$ = new Subject<any>();
  private RESOURCE_TYPE = 'services';
  serviceCreateEnabled: boolean;
  initialized: boolean;

  K8sServiceSpecTypeMap = ServiceSpecTypeMap;
  columns = ['name', 'type', 'ip_address', 'target_app', 'port', 'action'];
  notificationVisible = true;
  private clusterName: string;
  private namespaceName: string;

  get appKey() {
    return `app.${this.env.label_base_domain}/name`;
  }

  get serviceKey() {
    return `service.${this.env.label_base_domain}/name`;
  }

  constructor(
    http: HttpClient,
    router: Router,
    activedRoute: ActivatedRoute,
    cdr: ChangeDetectorRef,
    private auiNotificationService: NotificationService,
    private errorsToastService: ErrorsToastService,
    private roleUtil: RoleUtilitiesService,
    private translateService: TranslateService,
    private dialogService: DialogService,
    private k8sResourceService: K8sResourceService,
    private workspaceComponent: WorkspaceComponent,
    public resourcePermissionService: ResourceListPermissionsService,
    @Inject(ENVIRONMENTS) private env: Environments,
  ) {
    super(http, router, activedRoute, cdr);
  }

  async ngOnInit() {
    super.ngOnInit();
    const params = this.workspaceComponent.baseParamsSnapshot;
    this.resourcePermissionService.initContext({
      resourceNameParamKey: 'service_name',
      context: {
        project_name: params.project,
        cluster_name: params.cluster,
        namespace_name: params.namespace,
      },
    });
    this.clusterName = params['cluster'];
    this.namespaceName = params['namespace'];
    this.roleUtil
      .resourceTypeSupportPermissions(
        'service',
        {
          cluster_name: this.clusterName,
          namespace_name: this.namespaceName,
          project_name: params['project'],
        },
        'create',
      )
      .then(res => (this.serviceCreateEnabled = res));

    this.list$.pipe(takeUntil(this.onDestroy$)).subscribe(list => {
      if (list.length) {
        this.initialized = true;
      }
    });
  }

  fetchResources(params: FetchParams): Observable<ResourceList> {
    return from(
      this.k8sResourceService.getK8sResourcesPaged(this.RESOURCE_TYPE, {
        clusterName: this.clusterName,
        namespace: this.namespaceName,
        name: params.search,
        page: params.pageParams.pageIndex,
        page_size: params.pageParams.pageSize,
      }),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  trackByFn(_index: number, item: K8sResourceWithActions<Service>) {
    return item.kubernetes.metadata.uid;
  }

  createService() {
    this.router.navigate(['service', 'create'], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  pageChanged(pageEvent: PageParams) {
    this.onPageEvent({
      pageIndex: ++pageEvent.pageIndex,
      pageSize: pageEvent.pageSize,
    });
  }

  getTargetAppName(item: K8sResourceWithActions<Service>) {
    if (
      item.kubernetes.spec.selector &&
      item.kubernetes.spec.selector[this.appKey]
    ) {
      return item.kubernetes.spec.selector[this.appKey].split('.')[0];
    }

    // 兼容之前只选择应用的情况
    if (
      item.kubernetes.spec.selector &&
      item.kubernetes.spec.selector[this.serviceKey]
    ) {
      const value = item.kubernetes.spec.selector[this.serviceKey];
      return value.split('-').length > 1 ? '' : value;
    }
    return '';
  }

  getComponentName(item: K8sResourceWithActions<Service>) {
    if (
      item.kubernetes.spec.selector &&
      item.kubernetes.spec.selector[this.serviceKey]
    ) {
      const value = item.kubernetes.spec.selector[this.serviceKey];
      // 兼容之前只选择应用的情况
      return value.split('-').length > 1 ? value : '';
    }
    return '';
  }

  canShowUpdate(item: K8sResourceWithActions<Service>) {
    return this.roleUtil.resourceHasPermission(item, 'service', 'update');
  }

  canShowDelete(item: K8sResourceWithActions<Service>) {
    return this.roleUtil.resourceHasPermission(item, 'service', 'delete');
  }

  update(item: K8sResourceWithActions<Service>) {
    this.router.navigate(['service', 'update', item.kubernetes.metadata.name], {
      relativeTo: this.workspaceComponent.baseActivatedRoute,
    });
  }

  viewYaml(item: K8sResourceWithActions<Service>) {
    try {
      const yaml = jsyaml.safeDump(item.kubernetes);
      this.dialogService.open(ViewResourceYamlComponent, {
        size: DialogSize.Big,
        data: {
          yaml: yaml,
        },
      });
    } catch (e) {
      this.errorsToastService.error(e);
    }
  }

  async delete(service: K8sResourceWithActions<Service>) {
    const mirrorService = await this.k8sResourceService.getMirrorResourceList(
      this.clusterName,
      this.namespaceName,
      this.RESOURCE_TYPE,
      service.kubernetes.metadata.name,
    );
    if (mirrorService.length) {
      // 联邦删除
      const requireSelect = mirrorService.filter((item: any) => {
        return item.uuid === service.kubernetes.metadata.uid;
      });
      const mirrorClusterMeta = {
        displayName: 'cluster_display_name',
        mirrorResource: mirrorService,
        confirmTitle: this.translateService.get('delete_service'),
        confirmLabel: this.translateService.get('cluster'),
        confirmContent: this.translateService.get(
          'delete_selected_cluster_service_confirm',
          {
            name: service.kubernetes.metadata.name,
          },
        ),
      };
      const modalRef = await this.dialogService.open(
        MirrorResourceDeleteConfirmComponent,
      );
      modalRef.componentInstance.setMirrorResourceMeta(
        mirrorClusterMeta,
        requireSelect,
      );
      modalRef.componentInstance.initialized = true;
      modalRef.componentInstance.close
        .pipe(first())
        .subscribe(async (res: MirrorResource[]) => {
          modalRef.close();
          if (res) {
            const clusters = res.map((item: MirrorResource) => {
              return item.cluster_name;
            });
            try {
              const res = await this.k8sResourceService.deleteK8sReourceBatch(
                clusters,
                this.RESOURCE_TYPE,
                service.kubernetes,
              );
              this.k8sResourceService.reourceBatchNotification(
                'cluster_service_delete',
                res,
                service.kubernetes.metadata.name,
              );
              this.onUpdate(null);
            } catch (err) {
              this.errorsToastService.error(err);
            }
          }
        });
    } else {
      // 非联邦删除
      try {
        await this.dialogService.confirm({
          title: this.translateService.get('delete'),
          content: this.translateService.get('delete_kube_service_confirm', {
            name: service.kubernetes.metadata.name,
          }),
          confirmText: this.translateService.get('confirm'),
          cancelText: this.translateService.get('cancel'),
        });
      } catch (e) {
        return;
      }
      try {
        await this.k8sResourceService.deleteK8sReource(
          this.clusterName,
          this.RESOURCE_TYPE,
          service.kubernetes,
        );
        this.auiNotificationService.success({
          title: this.translateService.get('success'),
          content: this.translateService.get('delete_kube_service_success', {
            name: service.kubernetes.metadata.name,
          }),
        });
        this.onUpdate(null);
      } catch (e) {
        this.errorsToastService.error(e);
      }
    }
  }

  getPortsName(rowData: K8sResourceWithActions<Service>) {
    return (
      get(rowData, 'kubernetes.spec.ports[0].name') ||
      get(rowData, 'kubernetes.spec.ports[0].port')
    );
  }
}
