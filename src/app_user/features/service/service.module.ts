import { NgModule } from '@angular/core';
import { FormTableModule } from 'app/shared/form-table/moduel';
import { SharedModule } from 'app/shared/shared.module';
import { ServiceCreateComponent } from 'app_user/features/service/create/service-create.component';
import { ServicePortsFieldsetComponent } from 'app_user/features/service/fieldset/service-ports-fieldset.component';
import { ServiceCreateFormComponent } from 'app_user/features/service/form/service-form.component';
import { ServiceListComponent } from 'app_user/features/service/list/service-list.component';
import { ServiceRoutingModule } from 'app_user/features/service/service.routing.module';
import { ServiceUpdateComponent } from 'app_user/features/service/update/service-update.component';

@NgModule({
  imports: [FormTableModule, SharedModule, ServiceRoutingModule],
  declarations: [
    ServiceListComponent,
    ServiceCreateFormComponent,
    ServiceCreateComponent,
    ServiceUpdateComponent,
    ServicePortsFieldsetComponent,
  ],
  entryComponents: [],
})
export class ServiceModule {}
