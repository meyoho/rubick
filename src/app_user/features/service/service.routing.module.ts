import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { ServiceCreateComponent } from 'app_user/features/service/create/service-create.component';
import { ServiceListComponent } from 'app_user/features/service/list/service-list.component';
import { ServiceUpdateComponent } from 'app_user/features/service/update/service-update.component';

const routes: Routes = [
  {
    path: '',
    component: ServiceListComponent,
  },
  {
    path: 'create',
    component: ServiceCreateComponent,
  },
  {
    path: 'update/:name',
    component: ServiceUpdateComponent,
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ServiceRoutingModule {}
