export const CARD_WIDTH = 317;
export const ARROW_WIDTH = 40;
export const CARD_HEIGHT = 64;
export const WRAP_HEADER = 48;
export const WRAP_BOTTOM = 8;
export const LAST_WRAP_BOTTOM = 4;
