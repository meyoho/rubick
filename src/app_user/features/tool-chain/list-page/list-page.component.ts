import { ChangeDetectionStrategy, Component, OnInit } from '@angular/core';
import { Store } from '@ngrx/store';
import { ToolChainApiService } from 'app/services/api/tool-chain/tool-chain-api.service';
import { ToolBindings } from 'app/services/api/tool-chain/tool-chain-api.types';
import { AppState, getCurrentProjectDetail, getCurrentSpace } from 'app/store';
import { TranslateService } from 'app/translate/translate.service';
import {
  ARROW_WIDTH,
  CARD_WIDTH,
} from 'app_user/features/tool-chain/constants';
import { Observable, combineLatest, from } from 'rxjs';
import {
  filter,
  map,
  publishReplay,
  refCount,
  switchMap,
} from 'rxjs/operators';

@Component({
  templateUrl: 'list-page.component.html',
  styleUrls: ['list-page.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class ListPageComponent implements OnInit {
  projectName$: Observable<string>;
  spaceName$: Observable<string>;
  resourceChunk$: Observable<ToolBindings[][]>;
  enterpriseIcon$ = this.translate.currentLang$.pipe(
    map(lang => `/static/images/icon/enterprise_${lang}.svg`),
  );
  column = 3;
  sectionWidth: number;

  ngOnInit(): void {
    this.getProjectAndSpace();
    this.initRes();
    this.sectionWidth = this.getSectionWidth();
  }

  constructor(
    private store: Store<AppState>,
    private toolChainApi: ToolChainApiService,
    private translate: TranslateService,
  ) {}

  initRes() {
    this.resourceChunk$ = combineLatest(
      this.projectName$,
      this.spaceName$,
    ).pipe(
      switchMap(([projectName, spaceName]) =>
        from(
          this.toolChainApi.getToolBindings(
            projectName,
            spaceName,
            this.column,
          ),
        ),
      ),
      publishReplay(1),
      refCount(),
    );
  }

  getProjectAndSpace() {
    this.projectName$ = this.store.select(getCurrentProjectDetail).pipe(
      filter(p => !!p),
      map(p => p.name),
    );
    this.spaceName$ = this.store.select(getCurrentSpace).pipe(
      filter(s => !!s),
      map(s => s.name),
    );
  }

  showTriangle(r: number, i: number, row: ToolBindings[], reverse?: boolean) {
    if (reverse) {
      return r % 2 === 1 && i !== this.column - 1 && i !== row.length - 1;
    }
    return r % 2 === 0 && i !== this.column - 1 && i !== row.length - 1;
  }

  showTriangleVertical(
    r: number,
    i: number,
    row: ToolBindings[],
    resourceChunk: ToolBindings[][],
  ) {
    return i === row.length - 1 && r !== resourceChunk.length - 1;
  }

  getSectionWidth() {
    return this.column * CARD_WIDTH + (this.column - 1) * ARROW_WIDTH;
  }
}
