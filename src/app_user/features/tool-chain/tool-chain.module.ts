import { CommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { SharedModule } from 'app/shared/shared.module';
import { ListPageComponent } from 'app_user/features/tool-chain/list-page/list-page.component';
import { ToolChainRoutingModule } from 'app_user/features/tool-chain/tool-chain-routing.module';
import { TriangleBackwardsComponent } from 'app_user/features/tool-chain/triangle-backwards/triangle-backwards.component';
import { TriangleVerticalComponent } from 'app_user/features/tool-chain/triangle-vertical/triangle-vertical.component';
import { TriangleComponent } from 'app_user/features/tool-chain/triangle/triangle.component';

@NgModule({
  imports: [CommonModule, RouterModule, ToolChainRoutingModule, SharedModule],
  declarations: [
    ListPageComponent,
    TriangleComponent,
    TriangleBackwardsComponent,
    TriangleVerticalComponent,
  ],
})
export class ToolChainModule {}
