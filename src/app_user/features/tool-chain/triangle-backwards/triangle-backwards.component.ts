import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'rc-triangle-backwards',
  templateUrl: 'triangle-backwards.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TriangleBackwardsComponent {}
