import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnInit,
} from '@angular/core';
import { ToolBindings } from 'app/services/api/tool-chain/tool-chain-api.types';
import {
  CARD_HEIGHT,
  LAST_WRAP_BOTTOM,
  WRAP_BOTTOM,
  WRAP_HEADER,
} from 'app_user/features/tool-chain/constants';

@Component({
  selector: 'rc-triangle-vertical',
  styleUrls: ['triangle-vertical.component.scss'],
  templateUrl: 'triangle-vertical.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TriangleVerticalComponent implements OnInit {
  @Input()
  row: ToolBindings[];
  @Input()
  index: number;

  height: number;

  ngOnInit(): void {
    const maxCount = this.getMaxCount(this.row);
    const currentCount = this.row[this.index].toolList.length;
    const currentHeight =
      CARD_HEIGHT * currentCount +
      WRAP_HEADER +
      WRAP_BOTTOM * currentCount +
      LAST_WRAP_BOTTOM;
    const maxHeight =
      CARD_HEIGHT * maxCount +
      WRAP_HEADER +
      WRAP_BOTTOM * maxCount +
      LAST_WRAP_BOTTOM;
    this.height = maxHeight - currentHeight + 36;
  }

  private getMaxCount(array: ToolBindings[]) {
    return Math.max.apply(Math, array.map(el => el.toolList.length));
  }
}
