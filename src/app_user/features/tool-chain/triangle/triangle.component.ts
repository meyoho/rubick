import { ChangeDetectionStrategy, Component } from '@angular/core';

@Component({
  selector: 'rc-triangle',
  templateUrl: 'triangle.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class TriangleComponent {}
