import { TooltipDirective } from '@alauda/ui';
import {
  ChangeDetectionStrategy,
  Component,
  Input,
  OnChanges,
  OnDestroy,
  OnInit,
  SimpleChanges,
  ViewChild,
} from '@angular/core';
import { Inject } from '@angular/core';
import { Router } from '@angular/router';
import { Namespace } from 'app/services/api/namespace.service';
import { UserNamespace } from 'app/services/api/namespace.service';
import { ClusterInProject, Project } from 'app/services/api/project.service';
import { Namespace as NamespaceInSpace } from 'app/services/api/space.service';
import { Space } from 'app/services/api/space.service';
import { ACCOUNT, WEBLABS } from 'app/shared/tokens';
import * as fromStore from 'app/store';
import * as spaceActions from 'app/store/actions/spaces.actions';
import { SpacesFacadeService } from 'app/store/facades';
import { NamespacesFacadeService } from 'app/store/facades/namespaces.facade';
import { Account, Weblabs } from 'app/typings';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { get, groupBy } from 'lodash-es';
import { Observable, Subject, combineLatest, of } from 'rxjs';
import {
  map,
  publishReplay,
  refCount,
  startWith,
  takeUntil,
  tap,
} from 'rxjs/operators';

interface ClusterOption {
  name: string;
  display_name: string;
  uuid: string;
}

@Component({
  selector: 'rc-namespace-badge',
  styleUrls: ['namespace-badge.component.scss'],
  templateUrl: 'namespace-badge.component.html',
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class NamespaceBadgeComponent implements OnInit, OnDestroy, OnChanges {
  @Input()
  currentProject: Project;
  @ViewChild('namespaceTooltip', { read: TooltipDirective })
  namespaceTooltip: TooltipDirective;

  active: boolean;
  search$ = new Subject<string>();
  namespaces$: Observable<UserNamespace[]>;
  namespacesLength$: Observable<number>;
  onDestroy$ = new Subject<void>();

  selectedNamespace: UserNamespace;
  currentSpace: Space;
  spaces: Space[];

  constructor(
    private router: Router,
    private workspaceComponent: WorkspaceComponent,
    private spacesFacade: SpacesFacadeService,
    private namespacesFacade: NamespacesFacadeService,
    @Inject(ACCOUNT) private account: Account,
    @Inject(WEBLABS) private weblabs: Weblabs,
  ) {}

  get namespaceName() {
    return this.workspaceComponent.baseParamsSnapshot.namespace;
  }

  get clusterName() {
    return this.workspaceComponent.baseParamsSnapshot.cluster;
  }

  async ngOnInit() {
    const namespaces$ = this.namespacesFacade.getNamespaces$().pipe(
      map(entities => {
        return entities
          .filter(entity => {
            return entity.project === this.currentProject.name;
          })
          .map(entity => {
            const refCluster: ClusterOption = this.currentProject.clusters.find(
              c => c.name === entity.cluster,
            );
            return this.getNamespaceOption(entity.namespace, refCluster);
          });
      }),
      publishReplay(1),
      refCount(),
    );

    const currentSpace$ = this.weblabs.GROUP_ENABLED
      ? this.spacesFacade.currentSpace$.pipe(takeUntil(this.onDestroy$))
      : of(null);

    if (this.weblabs.GROUP_ENABLED) {
      this.spacesFacade
        .getSpacesByProjectName$(this.currentProject.name)
        .pipe(takeUntil(this.onDestroy$))
        .subscribe(spaces => {
          this.spaces = spaces;
          if (spaces) {
            const preSpaceUuid = window.sessionStorage.getItem('space');
            const matchingSpaces = spaces.filter(space => {
              return !!(
                space.namespaces &&
                space.namespaces.find(n => n.name === this.namespaceName)
              );
            });
            if (
              !preSpaceUuid ||
              !matchingSpaces.some(s => s.uuid === preSpaceUuid)
            ) {
              this.spacesFacade.dispatch(
                new spaceActions.SetCurrentSpace(
                  get(matchingSpaces[0], 'uuid'),
                ),
              );
            }
          }
        });
    }

    combineLatest(namespaces$, this.workspaceComponent.baseParams)
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(([namespaces, params]) => {
        this.selectedNamespace = namespaces.find(item => {
          return (
            item.name === params.namespace &&
            item.cluster_name === params.cluster
          );
        });
      });

    this.namespacesLength$ = namespaces$.pipe(
      map(namespaces => namespaces.length),
    );

    this.namespaces$ = combineLatest(
      namespaces$,
      this.search$.pipe(startWith('')),
      currentSpace$,
    ).pipe(
      tap(([namespaces, , space]) => {
        this.checkCurrentSpaceValid(namespaces, space);
      }),
      map(([namespaces, search, space]) => {
        if (space && space.name) {
          namespaces = namespaces.filter(namespace =>
            space.namespaces.find(
              (n: UserNamespace) => n.uuid === namespace.uuid,
            ),
          );
        }
        return search
          ? this.getGroupedNamespaces(
              namespaces.filter(
                namespace =>
                  namespace.name.includes(search) ||
                  namespace.cluster_display_name.includes(search),
              ),
            )
          : this.getGroupedNamespaces(namespaces);
      }),
      publishReplay(1),
      refCount(),
    );
  }

  ngOnDestroy() {
    this.onDestroy$.next();
  }

  ngOnChanges({ currentProject }: SimpleChanges) {
    if (currentProject && currentProject.currentValue) {
      this.currentProject.clusters.forEach((cluster: ClusterInProject) => {
        this.namespacesFacade.dispatch(
          new fromStore.GetNamespacesByClusterAndProject({
            project: this.currentProject.name,
            cluster: cluster.name,
          }),
        );
      });
    }
  }

  onNamespaceSelected(namespace: UserNamespace) {
    if (namespace.uuid === this.selectedNamespace.uuid) {
      return;
    }
    this.selectedNamespace = namespace;
    this.namespaceTooltip.disposeTooltip();
    this.router.navigate([
      '/console/user/workspace',
      {
        project: this.currentProject.name,
        cluster: namespace.cluster_name,
        namespace: namespace.name,
      },
    ]);
  }

  showNamespaceDetail(namespace: UserNamespace, event: Event) {
    event.stopPropagation();
    this.namespaceTooltip.disposeTooltip();
    this.router.navigate([
      '/console/user/project',
      {
        project: this.currentProject.name,
      },
      'namespace',
      'detail',
      namespace.cluster_name,
      namespace.name,
    ]);
  }

  trackByIndex(index: number) {
    return index;
  }

  private getGroupedNamespaces(namespaces: UserNamespace[]): any[] {
    const res = groupBy(namespaces, (item: UserNamespace) => {
      return item.cluster_display_name;
    });
    return Object.keys(res).map((cluster_display_name: string) => {
      return {
        cluster_display_name,
        namespaces: res[cluster_display_name],
      };
    });
  }

  private getNamespaceOption(namespace: Namespace, cluster: ClusterOption) {
    return {
      uuid: `${this.account.namespace}:${cluster.name}:${
        namespace.kubernetes.metadata.name
      }`,
      name: namespace.kubernetes.metadata.name,
      cluster_name: cluster.name,
      cluster_display_name: cluster.display_name,
    } as UserNamespace;
  }

  private checkCurrentSpaceValid(namespaces: UserNamespace[], space: Space) {
    if (space && space.namespaces && space.namespaces.length) {
      const currentNamespace = space.namespaces.find((n: NamespaceInSpace) => {
        return n.name === this.namespaceName;
      });
      if (!currentNamespace) {
        const namespace = namespaces.find(n => {
          return n.uuid === space.namespaces[0].uuid;
        });
        if (namespace) {
          this.router.navigate([
            '/console/user/workspace',
            {
              project: this.currentProject.name,
              cluster: namespace.cluster_name,
              namespace: namespace.name,
            },
          ]);
        }
      }
    }
  }
}
