import { TooltipDirective } from '@alauda/ui';
import { Component, Input, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { Space } from 'app/services/api/space.service';
import * as spaceActions from 'app/store/actions/spaces.actions';
import { SpacesFacadeService } from 'app/store/facades';
import { BehaviorSubject, Observable, Subject, combineLatest } from 'rxjs';
import {
  debounceTime,
  map,
  publishReplay,
  refCount,
  takeUntil,
} from 'rxjs/operators';

@Component({
  selector: 'rc-space-badge',
  templateUrl: './space-badge.component.html',
  styleUrls: ['./space-badge.component.scss'],
})
export class SpaceBadgeComponent implements OnInit, OnDestroy {
  currentSpace: Space;
  filterName: string;
  filterName$ = new BehaviorSubject<string>('');
  filteredSpaces$: Observable<Space[]>;
  spaces$: Observable<Space[]>;
  onDestroy$ = new Subject<void>();

  @Input()
  projectName: string;

  @ViewChild('spaceTooltip', { read: TooltipDirective })
  namespaceTooltip: TooltipDirective;

  constructor(private spacesFacade: SpacesFacadeService) {}

  ngOnInit() {
    this.spaces$ = this.spacesFacade
      .getSpacesByProjectName$(this.projectName)
      .pipe(
        takeUntil(this.onDestroy$),
        map(spaces => {
          return spaces.filter(s => {
            return s.namespaces && s.namespaces.length;
          });
        }),
      );

    this.filteredSpaces$ = combineLatest(this.filterName$, this.spaces$).pipe(
      debounceTime(200),
      map(([name, spaces]) => {
        name = name.trim();
        return spaces.filter((space: Space) => {
          const projectName = space.display_name || space.name;
          return projectName.toLowerCase().includes(name.toLowerCase());
        });
      }),
      publishReplay(1),
      refCount(),
    );

    this.spacesFacade.currentSpace$
      .pipe(takeUntil(this.onDestroy$))
      .subscribe(v => (this.currentSpace = v));
  }

  ngOnDestroy() {
    this.onDestroy$.next();
    this.setCurrentSpace(null);
  }

  selectSpace(space: Space) {
    if (this.currentSpace && this.currentSpace.name === space.name) {
      return;
    }
    this.namespaceTooltip.disposeTooltip();
    this.setCurrentSpace(space);
  }

  private setCurrentSpace(space: Space) {
    this.spacesFacade.dispatch(
      new spaceActions.SetCurrentSpace(space ? space.uuid : null),
    );
  }
}
