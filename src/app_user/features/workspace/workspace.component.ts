import { NavItemConfig } from '@alauda/ui';
import { Component, Inject, Injector, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute, Params } from '@angular/router';
import { Store } from '@ngrx/store';

import { Observable } from 'rxjs';
import {
  distinctUntilChanged,
  filter,
  pluck,
  startWith,
  takeUntil,
} from 'rxjs/operators';

import { Project } from 'app/services/api/project.service';
import { BaseLayoutComponent } from 'app/shared/layout/base-layout-component';
import { WEBLABS } from 'app/shared/tokens';
import * as fromStore from 'app/store';
import { Weblabs, WorkspaceBaseParams } from 'app/typings';

@Component({
  styleUrls: [
    '../../../app/shared/layout/layout.common.scss',
    'workspace.component.scss',
  ],
  templateUrl: 'workspace.component.html',
  providers: [WorkspaceComponent],
})
export class WorkspaceComponent extends BaseLayoutComponent
  implements OnInit, OnDestroy {
  currentProjectDetail$: Observable<Project>;
  spaceEnabled: boolean;
  embeddedMode: boolean;

  constructor(
    injector: Injector,
    private store: Store<fromStore.AppState>,
    @Inject(WEBLABS) public weblabs: Weblabs,
  ) {
    super(injector);
    this.embeddedMode = this.environments.embedded_mode;
    this.spaceEnabled = this.weblabs.GROUP_ENABLED;
  }

  /**
   * {project: '', 'cluster': '', namespace: ''}
   */
  get baseParams(): Observable<Params> {
    return this.activatedRoute.params;
  }

  get baseParamsSnapshot(): WorkspaceBaseParams {
    const params = this.activatedRoute.snapshot.params;
    return {
      project: params['project'],
      cluster: params['cluster'],
      namespace: params['namespace'],
    };
  }

  get baseActivatedRoute(): ActivatedRoute {
    return this.activatedRoute;
  }

  ngOnInit(): void {
    super.ngOnInit();
    this.initNavConfig('user', segment => {
      const paramString = Object.keys(this.baseParamsSnapshot)
        .map(key => `${key}=${this.baseParamsSnapshot[key]}`)
        .join(';');
      return `/console/user/workspace;${paramString}/${segment}`;
    });

    this.currentProjectDetail$ = this.store
      .select(fromStore.getCurrentProjectDetail)
      .pipe(filter(project => !!project));

    this.store.dispatch(new fromStore.GetClusters());
    this.baseParams
      .pipe(
        startWith(this.baseParamsSnapshot),
        takeUntil(this.destroy$),
        pluck('project'),
        distinctUntilChanged(),
      )
      .subscribe((project: string) => {
        if (project) {
          this.store.dispatch(new fromStore.LoadProjectDetail(project));
        }
      });
  }

  ngOnDestroy() {
    super.ngOnDestroy();
  }

  handleActivatedItemChange(config: NavItemConfig) {
    this.router.navigate([config.routerLink], {
      relativeTo: this.baseActivatedRoute,
    });
  }
}
