import { LayoutModule as AuiLayoutModule, PlatformNavModule } from '@alauda/ui';
import { PortalModule } from '@angular/cdk/portal';
import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
// import { WorkspaceRoutingModule } from 'app_user/features/workspace/workspace.routing.module';
import { SharedLayoutModule } from 'app/shared/layout/module';
import { SharedModule } from 'app/shared/shared.module';
import { HomeComponent } from 'app_user/features/workspace/home.component';
import { NamespaceBadgeComponent } from 'app_user/features/workspace/namespace-badge/namespace-badge.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';
import { WorkspaceRoutingModule } from 'app_user/features/workspace/workspace.routing.module';

import { SpaceBadgeComponent } from './space-badge/space-badge.component';

@NgModule({
  imports: [
    SharedModule,
    SharedLayoutModule,
    AuiLayoutModule,
    RouterModule,
    PortalModule,
    WorkspaceRoutingModule,
    PlatformNavModule,
  ],
  exports: [WorkspaceComponent],
  declarations: [
    WorkspaceComponent,
    HomeComponent,
    NamespaceBadgeComponent,
    SpaceBadgeComponent,
  ],
  providers: [WorkspaceComponent],
})
export class WorkspaceModule {}
