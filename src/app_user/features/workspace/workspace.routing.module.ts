import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { HomeComponent } from 'app_user/features/workspace/home.component';
import { WorkspaceComponent } from 'app_user/features/workspace/workspace.component';

const routes: Routes = [
  {
    path: '',
    component: WorkspaceComponent,
    children: [
      {
        path: '',
        pathMatch: 'full',
        component: HomeComponent,
      },
      {
        path: 'app',
        loadChildren: '../app/module#ApplicationModule',
      },
      {
        path: 'configmap',
        loadChildren: '../configmap/module#ConfigmapModule',
      },
      {
        path: 'configsecret',
        loadChildren: '../configsecret/module#ConfigSecretModule',
      },
      {
        path: 'pvc',
        loadChildren: '../pvc/pvc.module#PvcModule',
      },
      {
        path: 'service',
        loadChildren: '../service/service.module#ServiceModule',
      },
      {
        path: 'ingress',
        loadChildren: '../ingress/ingress.module#IngressModule',
      },
      {
        path: 'route',
        loadChildren: '../route/route.module#RouteModule',
      },
      {
        path: 'resource_management',
        loadChildren: '../resource-manage/module#ResourceManageModule',
      },
      {
        path: 'app_catalog',
        loadChildren: '../app-catalog/app-catalog.module#AppCatalogModule',
      },
      {
        path: 'platform-middleware',
        loadChildren:
          '../app-platform/platform-middleware.module#AppPlatformMiddlewareModule',
      },
      {
        path: 'platform-micro_service',
        loadChildren:
          '../app-platform/platform-micro_service.module#AppPlatformMicroServiceModule',
      },
      {
        path: 'platform-big_data',
        loadChildren:
          '../app-platform/platform-big_data.module#AppPlatformBigDataModule',
      },
      {
        path: 'jenkins',
        loadChildren: '../jenkins/jenkins.module#JenkinsModule',
      },
      {
        path: 'pipelines',
        loadChildren: '../pipelines/pipelines.module#PipelinesModule',
      },
      {
        path: 'network_policy',
        loadChildren:
          '../network-policy/network-policy.module#NetworkPolicyModule',
      },
      {
        path: 'credential',
        loadChildren: '../credential/credential.module#CredentialModule',
      },
      {
        path: 'service-mesh',
        loadChildren:
          '../micro-service/service-mesh/service-mesh.module#ServiceMeshModule',
      },
      {
        path: 'service-framework',
        loadChildren:
          '../micro-service/service-framework/service-framework.module#ServiceFrameworkModule',
      },
      {
        path: 'artifact-repositories',
        loadChildren:
          '../artifact-repositories/artifact-repositories.module#ArtifactRepositoriesModule',
      },
      {
        path: 'tool-chain',
        loadChildren: '../tool-chain/tool-chain.module#ToolChainModule',
      },
      {
        path: 'log',
        loadChildren: '../log/log.module#LogModule',
      },
      {
        path: 'cron_job',
        loadChildren: '../cron-job/cron-job.module#CronJobModule',
      },
      {
        path: 'job_record',
        loadChildren: '../job-record/job-record.module#JobRecordModule',
      },
    ],
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class WorkspaceRoutingModule {}
