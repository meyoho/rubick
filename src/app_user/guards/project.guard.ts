import { Injectable } from '@angular/core';
import { ActivatedRouteSnapshot, CanActivate, Router } from '@angular/router';

@Injectable({ providedIn: 'root' })
export class ProjectGuard implements CanActivate {
  constructor(private router: Router) {}

  canActivate(route: ActivatedRouteSnapshot) {
    const projectName = route.params['project'];
    if (projectName) {
      window.sessionStorage.setItem('project', projectName);
    } else {
      const projectNameInCookie = window.sessionStorage.getItem('project');
      if (!!projectNameInCookie) {
        this.router.navigate([
          '/console/user/project',
          { project: projectNameInCookie },
        ]);
        return false;
      }
    }
    return true;
  }
}
