import { BreadcrumbItemComponent } from '@alauda/ui';
import {
  Component,
  ContentChildren,
  OnInit,
  QueryList,
  TemplateRef,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Observable } from 'rxjs';
import { map } from 'rxjs/operators';

import { UiStateService } from 'app/services/ui-state.service';

const INGORED_PARTIALS = ['list', 'detail', 'update'];

@Component({
  selector: 'rc-default-breadcrumb',
  templateUrl: 'default-breadcrumb.component.html',
})
export class DefaultBreadcrumbComponent implements OnInit {
  partials$: Observable<{ routerLink?: string; label: string }[]>;
  @ContentChildren(BreadcrumbItemComponent, { read: TemplateRef })
  contentItemTemplates: QueryList<TemplateRef<any>>;

  constructor(
    private uiState: UiStateService,
    private activatedRoute: ActivatedRoute,
  ) {}

  ngOnInit(): void {
    this.partials$ = this.uiState.activeItemInfo$.pipe(
      map(navItemPartials => [
        ...navItemPartials,
        ...this.getLeafUrls()
          .filter(partial => !INGORED_PARTIALS.includes(partial))
          .map(partial => ({ label: partial })),
      ]),
    );
  }

  findLeaftRoute(activatedRoute: ActivatedRoute): ActivatedRoute {
    return activatedRoute.firstChild
      ? this.findLeaftRoute(activatedRoute.firstChild)
      : activatedRoute;
  }

  getLeafUrls() {
    const snapshot = this.findLeaftRoute(this.activatedRoute).snapshot;
    return snapshot ? snapshot.url.map(fragment => fragment.path) : [];
  }
}
