import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'rc-foldable-block',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
})
export class FoldableBlockComponent implements OnInit {
  @Input()
  mode = 'default'; // default, icon
  @Input()
  hint: string;
  folded = true;
  constructor() {}

  ngOnInit() {}
}
