import { Component, forwardRef, Input } from '@angular/core';
import { ControlValueAccessor, NG_VALUE_ACCESSOR } from '@angular/forms';

import { ErrorMapper } from 'app2/shared/types';

@Component({
  selector: 'rc-password-input',
  templateUrl: 'template.html',
  styleUrls: ['styles.scss'],
  providers: [
    {
      provide: NG_VALUE_ACCESSOR,
      useExisting: forwardRef(() => PasswordInputComponent),
      multi: true,
    },
  ],
})
export class PasswordInputComponent implements ControlValueAccessor {
  registerOnTouched(): void {}

  @Input()
  rcErrorsTooltipMapper: ErrorMapper;

  @Input() placeholder = '';

  private _valueChangeHandler = (_: string): void => {};

  set value(val: string) {
    this._valueChangeHandler(val);
    this._value = val;
  }

  get value() {
    return this._value;
  }

  showPassword = false;
  private _value: string;

  toggleVisible() {
    this.showPassword = !this.showPassword;
  }

  registerOnChange(fn: any): void {
    this._valueChangeHandler = fn;
  }

  writeValue(value: string): void {
    this.value = value;
  }
}
