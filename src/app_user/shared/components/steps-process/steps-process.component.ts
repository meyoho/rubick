import { Component, Input } from '@angular/core';

@Component({
  selector: 'rc-steps-process',
  templateUrl: './steps-process.component.html',
  styleUrls: ['./steps-process.component.scss'],
})
export class StepsProcessComponent {
  @Input()
  steps: string[];
  @Input()
  currStep: number;
}
