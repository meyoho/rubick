import { Component, HostBinding, Input } from '@angular/core';

@Component({
  selector: 'rc-zero-state',
  templateUrl: './zero-state.component.html',
  styleUrls: ['./zero-state.component.scss'],
})
export class ZeroStateComponent {
  @Input()
  resourceName = 'data';
  @Input()
  zeroState: boolean;
  @Input()
  fetching: boolean;
  @Input()
  @HostBinding('class.plain')
  plain: boolean;
}
