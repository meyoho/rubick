import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AppComponent } from './app.component';
import {
  LoginComponent,
  OidcLoginComponent,
  ResetPasswordCompleteComponent,
  ResetPasswordComponent,
  SendCaptchaCodeComponent,
  ThirdPartyLoginErrorComponent,
} from './feature-components';
import { GuideComponent } from './login/guide/guide.component';
import { ForgetPasswordComponent } from './login/pure_password/forget-password/forget-password.component';
import { PurePasswordLoginComponent } from './login/pure_password/pure-password.component';

export const appRoutes: Routes = [
  {
    path: '',
    component: AppComponent,
    children: [
      {
        path: 'guide',
        component: GuideComponent,
      },
      {
        path: 'login',
        redirectTo: 'landing/login/sub',
        pathMatch: 'full',
      },
      {
        path: 'login/password',
        children: [
          {
            path: 'forget-password',
            component: ForgetPasswordComponent,
          },
          {
            path: '',
            pathMatch: 'full',
            component: PurePasswordLoginComponent,
          },
        ],
      },
      {
        path: 'login/:mode',
        component: LoginComponent,
      },
      {
        path: 'oidc-auth-redirect',
        component: OidcLoginComponent,
      },
      {
        path: 'forget-password',
        children: [
          {
            path: '',
            component: SendCaptchaCodeComponent,
          },
          {
            path: 'reset/:mobileOrEmail',
            component: ResetPasswordComponent,
          },
          {
            path: 'reset-complete/:mobileOrEmail',
            component: ResetPasswordCompleteComponent,
          },
        ],
      },
      {
        path: 'validation-failure',
        component: ThirdPartyLoginErrorComponent,
      },
      // The rest two should always stay at the bottom of the list
      { path: '', redirectTo: 'guide', pathMatch: 'full' },
      { path: '**', redirectTo: 'guide', pathMatch: 'full' },
    ],
  },
];

// Refer to https://angular.io/docs/ts/latest/guide/router.html
// see why we may want to use a module to define
@NgModule({
  imports: [RouterModule.forChild(appRoutes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
