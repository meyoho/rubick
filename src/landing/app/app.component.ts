import { Component, Inject, OnDestroy, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { NavigationEnd, Router } from '@angular/router';

import { get } from 'lodash-es';
import { Subscription } from 'rxjs';
import { filter, map } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';
import { getLeafActivatedRouteSnapshot } from 'app/utils/router-state';

const LOGO_LARGE_KEY = 'logo-large';
const bottomLinkInvisibleParts = [
  'landing/login/password',
  'landing/login/password/forget-password',
];
const headerInvisibleParts = ['landing/login/password/forget-password'];

// first segment -> [ URL, Link Text ]

@Component({
  selector: 'rld-app',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit, OnDestroy {
  private footerRouterLinkObject: [string, string] = [
    '/landing/login',
    'login_link_text',
  ];
  bottomLinkInvisible = false;
  headerInvisible = false;
  urlSegmentsSubscription: Subscription;
  bottomLinks = {
    guide: ['/landing/register', 'register_link_text'],
    login: ['/landing/register', 'register_link_text'],
    'register-complete': ['/landing/login', 'register_complete_link_text'],
    'validation-failure': [null, 'validation_failure_close_window'],
  };
  routingTitles: () => string[][];
  defaultLink = ['/landing/login', 'login_link_text'];
  translateSubscription: Subscription;
  backgroundImage: string;
  /**
   * Path to the logo src. Can be configured via environment var
   * @type {string}
   */
  logoSrc = '/static/images/logo/logo-login-large.svg';

  bgSrc: string;

  constructor(
    private translate: TranslateService,
    private title: Title,
    private router: Router,
    @Inject(ENVIRONMENTS) private ENVIRONMENTS: Environments,
  ) {}

  ngOnInit(): void {
    this.bgSrc =
      this.ENVIRONMENTS.landing_bg_url ||
      '/static/images/landing/landing-bg.svg';
    this.routingTitles = () => [
      ['landing/guide', this.translate.get('login_title')],
      ['landing/login', this.translate.get('login_title')],
      ['landing/register', this.translate.get('register_title')],
      ['landing/forget-password/reset', this.translate.get('reset_password')],
      ['landing/forget-password', this.translate.get('forget_password')],
      [
        'landing/validation-failure',
        this.translate.get('title_login_auth_failure'),
      ],
      ['landing/oidc-auth-redirect', this.translate.get('oidc_auth_login')],
    ];
    const currRouterUrl = get(this.router, 'routerState.snapshot.url', '');

    this.dealWithLoginRelativeText(currRouterUrl.split('/').slice(1));

    this.translateSubscription = this.translate.currentLang$.subscribe(() => {
      const currRouterUrl = get(this.router, 'routerState.snapshot.url', '');
      const newTitle = this.routingTitles().find(([path]) =>
        currRouterUrl.includes(path),
      )[1];
      this.title.setTitle(newTitle);
    });

    // Override logo src if there is an environment variable been set.
    const logoSrcEnv =
      this.ENVIRONMENTS.overridden_logo_sources &&
      this.ENVIRONMENTS.overridden_logo_sources
        .split(',')
        .find(_logoSrcEnv => _logoSrcEnv.startsWith(LOGO_LARGE_KEY + ':'));

    if (logoSrcEnv) {
      this.logoSrc = logoSrcEnv.substr(logoSrcEnv.indexOf(':') + 1);
    }

    this.initRouterStateSubscription();
  }

  dealWithLoginRelativeText = (segments: string[]) => {
    const newTitle = this.routingTitles().find(([path]) =>
      segments.join('/').startsWith(path),
    )[1];
    // Angular encapsulates DOM api. We should not try to access DOM directly.
    this.title.setTitle(newTitle);
    this.footerRouterLinkObject = this.bottomLinks[segments[1]]
      ? this.bottomLinks[segments[1]]
      : this.defaultLink;
    this.bottomLinkInvisible = bottomLinkInvisibleParts.includes(
      segments.join('/'),
    );
    this.headerInvisible = headerInvisibleParts.includes(segments.join('/'));
    if (
      this.ENVIRONMENTS.is_private_deploy_enabled &&
      (segments.join('/').startsWith('landing/oidc-auth-redirect') ||
        this.footerRouterLinkObject[0] === '/landing/register')
    ) {
      this.footerRouterLinkObject = undefined;
    }
  };

  ngOnDestroy(): void {
    this.urlSegmentsSubscription.unsubscribe();
    this.translateSubscription.unsubscribe();
  }

  get footerRouterLink() {
    return this.footerRouterLinkObject && this.footerRouterLinkObject[0];
  }

  get footerLinkText() {
    return this.footerRouterLinkObject && this.footerRouterLinkObject[1];
  }

  private initRouterStateSubscription() {
    this.urlSegmentsSubscription = this.router.events
      .pipe(
        filter(event => event instanceof NavigationEnd),
        map(() => {
          const leafActivatedRouteSnapshot = getLeafActivatedRouteSnapshot(
            this.router.routerState,
          );
          return leafActivatedRouteSnapshot.pathFromRoot
            .map(interSnapshot => interSnapshot.url)
            .filter(urlSegments => urlSegments.length)
            .map(urlSegments => urlSegments[0].path);
        }),
      )
      .subscribe(segments => {
        this.dealWithLoginRelativeText(segments);
      });
  }
}
