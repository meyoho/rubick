import { NgModule } from '@angular/core';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CommonModule } from './common/common.module';
import {
  GuideComponent,
  LoginComponent,
  OidcLoginComponent,
  ResetPasswordCompleteComponent,
  ResetPasswordComponent,
  SendCaptchaCodeComponent,
  ThirdPartyLoginErrorComponent,
} from './feature-components';
import { ForgetPasswordComponent } from './login/pure_password/forget-password/forget-password.component';
import { PurePasswordLoginComponent } from './login/pure_password/pure-password.component';

/**
 * The root module. See https://angular.io/docs/ts/latest/guide/style-guide.html for styling guide.
 */
@NgModule({
  declarations: [
    AppComponent,
    LoginComponent,
    SendCaptchaCodeComponent,
    ResetPasswordComponent,
    ResetPasswordCompleteComponent,
    ThirdPartyLoginErrorComponent,
    OidcLoginComponent,
    GuideComponent,
    PurePasswordLoginComponent,
    ForgetPasswordComponent,
  ],
  imports: [CommonModule, AppRoutingModule],
})
export class AppModule {
  constructor() {}
}
