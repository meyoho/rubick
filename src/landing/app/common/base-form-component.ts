import { Injector } from '@angular/core';
import {
  AbstractControl,
  FormBuilder,
  FormControl,
  FormGroup,
} from '@angular/forms';

import { difference, intersection } from 'lodash-es';

import { ErrorMessageParserService } from './error-message-parser.service';
import {
  FormControlFragment,
  FormControlFragmentDeclaration,
} from './form-control-fragment.component';

const PRIORITY_OF_ERRORS = {
  passwordRequired: 1,
  organizationOrAccountRequired: 2,
  captchaRequired: 3,
};

const MIN_PRIORITY_NUMBER = 0;

/**
 * Base abstract class for a FormGroup component
 */
export abstract class BaseFormComponent<T> {
  formModel: T;
  formGroup: FormGroup;
  formControlFragments: FormControlFragment[];
  formControlFragmentMap: { [key: string]: FormControlFragment } = {};

  submitting: boolean;
  submitted: boolean;

  private _formBuilder: FormBuilder;
  private _errorMessageParser: ErrorMessageParserService;

  constructor() {}

  /**
   * Return FormControlFragmentDeclaration array for this component.
   * {@see buildForm}
   */
  protected abstract get formFragmentDeclarations(): FormControlFragmentDeclaration[];

  /**
   * Instance to the component's injector. This is to get access to the DI.
   * @returns {Injector}
   */
  protected abstract get injectorInstance(): Injector;

  private get formBuilder(): FormBuilder {
    if (!this._formBuilder) {
      this._formBuilder = this.injectorInstance.get(FormBuilder);
    }
    return this._formBuilder;
  }

  private get errorMessageParser(): ErrorMessageParserService {
    if (!this._errorMessageParser) {
      this._errorMessageParser = this.injectorInstance.get(
        ErrorMessageParserService,
      );
    }
    return this._errorMessageParser;
  }

  checkForm(): string {
    this.formGroup.updateValueAndValidity();
    return Object.values(this.formControlFragmentMap)
      .map(fragment => {
        if (fragment.error && fragment.control.errors) {
          const key = Object.keys(fragment.control.errors)[0];
          return {
            error: fragment.error,
            priority: PRIORITY_OF_ERRORS[key] || MIN_PRIORITY_NUMBER,
          };
        }
      })
      .filter(item => !!item)
      .reduce(
        (prev, curr) => {
          return curr.priority > prev.priority ? curr : prev;
        },
        { error: '', priority: MIN_PRIORITY_NUMBER },
      ).error;
  }

  /**
   * Build the form.
   */
  protected buildForm(
    formFragmentDeclarations: FormControlFragmentDeclaration[] = this
      .formFragmentDeclarations,
  ) {
    this.formControlFragments = formFragmentDeclarations.map(declaration => {
      const fragment = Object.create(declaration) as FormControlFragment;

      // Only validate the validator if the control is not hidden or not disabled
      const validators = declaration.validatorFns.map(
        validatorFn => (abstractControl: AbstractControl) => {
          return !fragment.hidden ? validatorFn(abstractControl) : null;
        },
      );

      fragment.control = new FormControl(
        this.formModel[declaration.name],
        validators,
      );

      return fragment;
    });

    const controlConfigs = {};

    for (const fragment of this.formControlFragments) {
      controlConfigs[fragment.name] = fragment.control;
      this.formControlFragmentMap[fragment.name] = fragment;
    }

    if (!this.formGroup) {
      this.formGroup = this.formBuilder.group(controlConfigs);
      // Use {@link Observable.pairwise} to detect individual field changes
      this.formGroup.valueChanges.subscribe(data => this.onValueChanged(data));
    } else {
      const previousNames = Object.keys(this.formGroup.controls);
      const newNames = this.formControlFragments.map(fragment => fragment.name);

      difference(previousNames, newNames).forEach(name =>
        this.formGroup.removeControl(name),
      );
      difference(newNames, previousNames).forEach(name =>
        this.formGroup.addControl(name, controlConfigs[name]),
      );
      intersection(newNames, previousNames).forEach(name =>
        this.formGroup.setControl(name, controlConfigs[name]),
      );
    }
  }

  /**
   * When the form value changes.
   * Typically, update the validation messages.
   */
  protected onValueChanged(value: any): void {
    if (!this.formGroup || !value) {
      return;
    }
    this.formModel = value;
    this.formControlFragments.forEach(fragment =>
      this.updateFragmentErrorMessage(fragment),
    );
  }

  /**
   * Update fragment error message
   */
  updateFragmentErrorMessage(fragment: FormControlFragment | string) {
    if (typeof fragment === 'string') {
      fragment = this.formControlFragmentMap[fragment];
    }
    fragment.error = '';
    const control = fragment.control;
    if (control && control.invalid && control.errors) {
      const errors = this.errorMessageParser.getControlErrorMessage(
        control,
        fragment.name,
      );
      if (errors.length > 0) {
        fragment.error = errors[0];
      }
    }
  }

  /**
   * Set error object of a fragment.
   */
  setFragmentError(fragment: FormControlFragment | string, error?: any) {
    if (typeof fragment === 'string') {
      fragment = this.formControlFragmentMap[fragment];
    }
    if (!error) {
      // Run native validators
      fragment.control.updateValueAndValidity();
    } else {
      fragment.control.setErrors(error);
    }

    this.updateFragmentErrorMessage(fragment);
  }

  disableForm() {
    this.formGroup.disable();
  }

  enableForm() {
    this.formGroup.enable();
  }
}
