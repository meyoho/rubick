import { Component, HostListener, OnInit } from '@angular/core';

import { HttpService } from './http.service';

const utilityStyles = require('./style/utility.style.scss');

/**
 * Simple captcha img element.
 */
@Component({
  selector: 'rld-captcha-img', // prefix rld => Rubick landing
  templateUrl: 'captcha-img.component.html',
  styles: [
    `
      :host {
        display: block;
        cursor: pointer;
      }
      img {
        width: 90px;
        height: 34px;
      }
    `,
    utilityStyles,
  ],
})
export class CaptchaImgComponent implements OnInit {
  captchaSrc: string;
  refreshing = false;

  @HostListener('click')
  onClick() {
    this.refresh();
  }

  // Refresh captcha img.
  refresh() {
    if (!this.refreshing) {
      this.refreshing = true;
      this.http
        .request('/ajax-sp/captcha-refresh/')
        .then(({ image_url }) => {
          this.captchaSrc = image_url;
          return image_url;
        })
        .catch(() => {
          console.log('failed to refresh captcha');
        })
        .then(() => {
          this.refreshing = false;
        });
    }
  }

  constructor(private http: HttpService) {}

  ngOnInit() {
    this.refresh();
  }
}
