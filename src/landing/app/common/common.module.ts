import { CommonModule as AngularCommonModule } from '@angular/common';
import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';

import { SharedModule } from 'app/shared/shared.module';
import { LanguagePickerComponent } from '../language-picker.component';
import { CaptchaImgComponent } from './captcha-img.component';
import { ErrorMessageParserService } from './error-message-parser.service';
import { FormControlFragmentComponent } from './form-control-fragment.component';
import { HttpService } from './http.service';
import { PasswordStrengthComponent } from './password-strength.component';
import { ScorePasswordService } from './score-password.service';
import { VerificationCodeButtonComponent } from './verification-code-button.component';

const SHARED_ELEMENTS = [
  CaptchaImgComponent,
  LanguagePickerComponent,
  FormControlFragmentComponent,
  VerificationCodeButtonComponent,
  PasswordStrengthComponent,
];

const EXPORTABLE_MODULES = [HttpModule, SharedModule, AngularCommonModule];

/**
 * Defines the services/components/modules which will be shared globally, such as http
 */
@NgModule({
  declarations: SHARED_ELEMENTS,
  imports: [...EXPORTABLE_MODULES],
  providers: [HttpService, ErrorMessageParserService, ScorePasswordService],
  exports: [...SHARED_ELEMENTS, ...EXPORTABLE_MODULES],
})
export class CommonModule {}
