import {
  Component,
  ContentChild,
  ElementRef,
  EventEmitter,
  HostBinding,
  Input,
  Output,
  ViewChild,
} from '@angular/core';
import { AbstractControl, ValidatorFn } from '@angular/forms';

import { CaptchaImgComponent } from './captcha-img.component';
import { PasswordStrengthComponent } from './password-strength.component';
import { VerificationCodeButtonComponent } from './verification-code-button.component';

/**
 * Declaration for a FormControl fragment.
 */
export interface FormControlFragmentDeclaration {
  name: string;
  validatorFns?: ValidatorFn[];
  placeholder?: string;
  addon?: 'captcha' | 'verification-code' | 'password-strength'; // captcha, verification code, etc
  type: 'text' | 'email' | 'password' | 'dropdown';
  multiple?: boolean; // only valid when type is 'dropdown'
  options?: any[]; // only valid when type is 'dropdown'
  afterContent?: string; // HTML content after the form fragment
  hidden?: boolean; // hide the input and also removes it from FormGroup validation
}

/**
 * Represents viewmodel for a FormControlFragment
 */
export interface FormControlFragment extends FormControlFragmentDeclaration {
  control?: AbstractControl;
  error?: string;
  verifying?: boolean; // flag to indicates verification status (like async validator)
  verified?: boolean; // flag to indicates verification status (like async validator)
}

/**
 * General purpose form element for landing forms.
 *
 * formControlFragment: A FormControlFragment object. 'view model' for this base-form-component.ts.
 */
@Component({
  selector: 'rld-form-control-fragment',
  styleUrls: [
    'form-control-fragment.component.scss',
    '../common/style/form.style.scss',
    '../common/style/utility.style.scss',
  ],
  templateUrl: 'form-control-fragment.component.html',
})
export class FormControlFragmentComponent {
  @Input()
  formControlFragment: FormControlFragment;
  @Output()
  inputBlur = new EventEmitter<Event>();
  // Hidden should be applied to the :host element
  @HostBinding('class.hidden')
  get hidden() {
    return this.formControlFragment.hidden;
  }
  // Addon list views:
  @ViewChild(CaptchaImgComponent)
  captchaImg: CaptchaImgComponent;
  @ViewChild(VerificationCodeButtonComponent)
  verificationCodeButton: VerificationCodeButtonComponent;
  @ViewChild(PasswordStrengthComponent)
  passwordStrength: PasswordStrengthComponent;

  @ContentChild('content')
  content: ElementRef;

  focused: boolean;

  constructor(private elementRef: ElementRef) {}

  onBlur($event: Event) {
    this.inputBlur.emit($event);
    this.focused = false;
  }

  onFocus() {
    this.focused = true;
  }

  onPlaceholderClicked() {
    if (this.formControlFragment.type === 'dropdown') {
      // FIXME: 失效代码，待移除
      // (this.getDropdownElement() as any).dropdown('toggle');
    } else {
      const fragmentInput = (this.elementRef
        .nativeElement as HTMLElement).querySelector('input');

      if (fragmentInput) {
        fragmentInput.focus();
      }
    }
  }

  get hasFooter(): boolean {
    return !!this.content;
  }

  get floatingPlaceholder() {
    return this.focused || this.formControlFragment.control.value;
  }

  get addon():
    | CaptchaImgComponent
    | VerificationCodeButtonComponent
    | PasswordStrengthComponent {
    return (
      this.captchaImg || this.verificationCodeButton || this.passwordStrength
    );
  }

  get shouldShowInputIcon() {
    return (
      this.formControlFragment.type !== 'dropdown' &&
      !this.formControlFragment.error
    );
  }
}
