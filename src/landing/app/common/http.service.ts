import { NotificationService } from '@alauda/ui';
import { Inject, Injectable } from '@angular/core';
import {
  Headers,
  Http,
  RequestOptionsArgs,
  Response,
  URLSearchParams,
} from '@angular/http';

import { TimeoutError } from 'rxjs';
import { timeout } from 'rxjs/operators';
import * as uuidv1 from 'uuid/v1';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Environments } from 'app/typings';

export const HEADER_AJAX_REQUEST = 'RUBICK-AJAX-REQUEST';
const HEADER_REQUEST_ID = 'Alauda-Request-ID';

/**
 * Mimic the error response object between rubick UI and Django (MathildeErrorResponse)
 */
interface ErrorResponse {
  errors: [
    {
      code: string;
      source?: number;
      message?: string;
      fields?: any;
    }
  ];
  status_code: number;
}

function safeErrorResponseJson(response: Response): { errors: [any] } {
  try {
    return response.json();
  } catch (e) {
    return { errors: [{ code: 'unknown_issue', message: response.text() }] };
  }
}

/**
 * Utility function for fetching XHR requests
 */
@Injectable()
export class HttpService {
  static buildURLSearchParams(params: any): URLSearchParams {
    const urlSearchParams = new URLSearchParams();
    Object.entries(params).forEach(([key, value]: [string, any]) => {
      urlSearchParams.set(key, value);
    });
    return urlSearchParams;
  }

  TIMEOUT_IN_MS: number;

  constructor(
    private http: Http,
    private auiNotificationService: NotificationService,
    private translate: TranslateService,
    @Inject(ENVIRONMENTS) public environments: Environments,
  ) {
    this.TIMEOUT_IN_MS = this.environments.ajax_request_timeout || 30000; // default 30 seconds
  }

  /**
   * Wraps Angular 2 http service:
   *   - Add new headers
   *   - Map Observable to Promise
   *   - Handle exceptions
   *   - Handle timeout
   * @param url
   * @param options
   * @returns {Promise<any>}
   */
  request(
    url: string,
    options: RequestOptionsArgs = { method: 'GET' },
  ): Promise<any> {
    let request_id = '';
    const buffer = new Array(32);
    uuidv1(null, buffer, 0).forEach((item: any) => {
      request_id += item.toString(16);
    });
    if (!options.headers) {
      options.headers = new Headers({
        [HEADER_REQUEST_ID]: request_id,
      });
    } else {
      options.headers.set(HEADER_REQUEST_ID, request_id);
    }

    return this.http
      .request(url, options)
      .pipe(timeout(this.TIMEOUT_IN_MS))
      .toPromise()
      .then(res => {
        try {
          res.json();
        } catch (e) {
          return res;
        }
        return res.json();
      })
      .catch(error => {
        // In a real world app, we might use a remote logging infrastructure
        // Note: using TS 2.1 new feature 'lookup types' here.
        let errors: ErrorResponse['errors'];
        if (error instanceof Response) {
          if (error.status === 0) {
            errors = [{ code: 'network_issue' }];
          } else {
            errors = safeErrorResponseJson(error).errors;
          }
        } else if (error instanceof TimeoutError) {
          errors = [{ code: 'timeout_error' }];
          if (options.method !== 'GET') {
            this.auiNotificationService.error({
              content: this.translate.get('timeout_error'),
            });
          }
        } else {
          errors = [{ code: 'unknown_issue', message: error.toString() }];
        }
        throw errors;
      });
  }
}
