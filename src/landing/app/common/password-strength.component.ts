import { Component, Input } from '@angular/core';

import { ScorePasswordService } from './score-password.service';

/**
 * Displays a simple indicator for the password's strength
 */
@Component({
  selector: 'rld-password-strength',
  template: `
    <div>
      {{ 'password_strength' | translate }}:
      <span [style.color]="strengthLabelAndColor.color">
        {{ strengthLabelAndColor.label | translate }}
      </span>
    </div>
  `,
})
export class PasswordStrengthComponent {
  @Input()
  password: string;
  constructor() {}

  get strength() {
    return ScorePasswordService.score(this.password);
  }

  get strengthLabelAndColor() {
    const strength = this.strength;
    let label = 'weak';
    let color = '#f00';
    if (strength > 60) {
      label = 'strong';
      color = '#158900';
    } else if (strength >= 45) {
      label = 'medium';
      color = '#e8cb00';
    }
    return { color, label };
  }
}
