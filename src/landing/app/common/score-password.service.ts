import { Injectable } from '@angular/core';

@Injectable()
export class ScorePasswordService {
  static score(pass: string) {
    let score = 0;
    if (!pass) {
      return score;
    }
    // award every unique letter until 5 repetitions
    const letters = {};
    for (let i = 0; i < pass.length; i++) {
      letters[pass[i]] = (letters[pass[i]] || 0) + 1;
      score += 5.0 / letters[pass[i]];
    }

    // bonus points for mixing it up
    const variations = {
      digits: /\d/,
      lower: /[a-z]/,
      upper: /[A-Z]/,
      nonWords: /\W/,
    };

    const variationCount = Object.values(variations).reduce(
      (accum, regex) => accum + (regex.test(pass) ? 1 : 0),
      0,
    );

    score += (variationCount - 1) * 10;
    return +score;
  }
}
