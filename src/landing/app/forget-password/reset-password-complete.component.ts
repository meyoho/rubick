import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

@Component({
  styleUrls: [
    '../common/style/form.style.scss',
    '../common/style/utility.style.scss',
  ],
  template: `
    <div class="form-container">
      <div class="form-header">
        <aui-icon icon="check_s" color="#72c63d" aria-hidden="true"></aui-icon>
        {{ 'reset_password_success' | translate }}
      </div>
    </div>
  `,
})
export class ResetPasswordCompleteComponent implements OnInit {
  mobileOrEmail: string;

  constructor(private route: ActivatedRoute, private router: Router) {}

  ngOnInit(): void {
    this.mobileOrEmail = this.route.snapshot.params['mobileOrEmail'];
    if (!this.mobileOrEmail) {
      this.router.navigate(['landing/forget-password']);
    }
  }
}
