import { Component, Injector, OnInit } from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { pairwise } from 'rxjs/operators';

import { TranslateService } from 'app/translate/translate.service';
import { BaseFormComponent } from '../common/base-form-component';
import { FormControlFragmentDeclaration } from '../common/form-control-fragment.component';
import { HttpService } from '../common/http.service';
import { LandingValidators as Validators } from '../common/validators';

interface ResetPasswordFormModel {
  password: string;
  passwordConfirm: string;
  captcha: string;
}

@Component({
  styleUrls: [
    '../common/style/form.style.scss',
    '../common/style/utility.style.scss',
  ],
  templateUrl: 'reset-password.component.html',
})
export class ResetPasswordComponent
  extends BaseFormComponent<ResetPasswordFormModel>
  implements OnInit {
  submitting: boolean;
  accountLabel: string;
  mobileOrEmail: string;

  constructor(
    private injector: Injector,
    private http: HttpService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
  ) {
    super();
  }

  ngOnInit(): void {
    this.mobileOrEmail = this.route.snapshot.params['mobileOrEmail'];
    if (!this.mobileOrEmail) {
      this.router.navigate(['landing/forget-password']);
    }
    const isEmail = /^\w+([-+.]\w+)*@\w+([-.]\w+)*\.\w+([-.]\w+)*$/.test(
      this.mobileOrEmail,
    );
    this.accountLabel = isEmail ? 'email' : 'mobile_no';
    this.formModel = {
      password: '',
      passwordConfirm: '',
      captcha: '',
    };
    this.buildForm();
  }

  protected get injectorInstance() {
    return this.injector;
  }

  /**
   * Return 'raw' FormControlFragment.
   * The FormControlFragment in the return list should be decorated with {@link FormControl}.
   * {@see buildForm}
   */
  protected get formFragmentDeclarations(): FormControlFragmentDeclaration[] {
    const confirmPasswordValidator = (
      control: AbstractControl,
    ): { [key: string]: any } => {
      const password =
        this.formControlFragmentMap['password'] &&
        this.formControlFragmentMap['password'].control.value;
      return control.value !== password ? { confirmPassword: true } : null;
    };

    return [
      {
        name: 'password',
        type: 'password',
        placeholder: 'new_password',
        validatorFns: [
          Validators.required,
          Validators.minLength(6),
          Validators.maxLength(20),
          Validators.passwordStrength(45), // medium
        ],
        addon: 'password-strength',
      },
      {
        name: 'passwordConfirm',
        type: 'password',
        placeholder: 'new_password_confirm',
        validatorFns: [
          Validators.required,
          Validators.minLength(4),
          confirmPasswordValidator,
        ],
      },
      {
        name: 'captcha',
        type: 'text',
        placeholder: 'verification_code_placeholder',
        validatorFns: [Validators.required, Validators.fixedLength(6)],
      },
    ];
  }

  protected buildForm() {
    super.buildForm();

    // Update passwordConfirm error message when password changes
    this.formControlFragmentMap['password'].control.valueChanges
      .pipe(pairwise())
      .subscribe(([prev, current]) => {
        if (current !== prev) {
          setTimeout(() => {
            this.formControlFragmentMap[
              'passwordConfirm'
            ].control.updateValueAndValidity();
            this.updateFragmentErrorMessage('passwordConfirm');
          });
        }
      });
  }

  get submitDisabled(): boolean {
    return !this.formGroup.valid || this.submitting;
  }

  submit() {
    if (this.submitDisabled) {
      return;
    }
    const payload = {
      account: this.mobileOrEmail,
      captcha: this.formModel.captcha,
      password: this.formModel.password,
    };
    this.submitting = true;
    return this.http
      .request('/ap/reset-password', {
        method: 'POST',
        body: payload,
      })
      .then(() => {
        // Redirect to success page
        this.router.navigate([
          'landing/forget-password/reset-complete',
          this.mobileOrEmail,
        ]);
        // Redirect to the landing page after two seconds wating
        setTimeout(() => {
          this.router.navigate(['/landing/login/root']);
        }, 2000);
      })
      .catch((errors: any[]) => {
        this.formControlFragmentMap['captcha'].error =
          errors[0] &&
          errors[0].code === 'invalid_args' &&
          errors[0].fields.some((field: { captcha: string }) => !!field.captcha)
            ? this.translate.get('invalid_captcha')
            : null;
      })
      .then(() => (this.submitting = false));
  }
}
