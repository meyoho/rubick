import {
  Component,
  Injector,
  OnDestroy,
  OnInit,
  QueryList,
  Renderer2,
  ViewChildren,
} from '@angular/core';
import { Router } from '@angular/router';

import { TranslateService } from 'app/translate/translate.service';
import { BaseFormComponent } from '../common/base-form-component';
import { CaptchaImgComponent } from '../common/captcha-img.component';
import {
  FormControlFragmentComponent,
  FormControlFragmentDeclaration,
} from '../common/form-control-fragment.component';
import { HttpService } from '../common/http.service';
import { LandingValidators } from '../common/validators';

interface SendCaptchaCodeFormModel {
  account: string;
  captcha: string;
}

const ERROR_CODE = {
  userNotExist: 'user_not_exist',
  invalidCaptcha: 'invalid_captcha',
};

@Component({
  styleUrls: [
    '../common/style/form.style.scss',
    '../common/style/utility.style.scss',
    './send-captcha-code.component.scss',
  ],
  templateUrl: 'send-captcha-code.component.html',
})
export class SendCaptchaCodeComponent
  extends BaseFormComponent<SendCaptchaCodeFormModel>
  implements OnInit, OnDestroy {
  @ViewChildren(FormControlFragmentComponent)
  fragments: QueryList<FormControlFragmentComponent>;
  submitting: boolean;
  sendVerificationCodeHint: string;
  errorMessage: string;

  constructor(
    private renderer: Renderer2,
    private injector: Injector,
    private http: HttpService,
    private translate: TranslateService,
    private router: Router,
  ) {
    super();
    this.renderer.addClass(document.querySelector('.landing'), 'center-layout');
  }

  ngOnInit(): void {
    this.sendVerificationCodeHint = 'send_verification_code_hint';
    this.formModel = { account: '', captcha: '' };
    this.buildForm();
  }

  ngOnDestroy() {
    this.renderer.removeClass(
      document.querySelector('.landing'),
      'center-layout',
    );
  }

  protected get injectorInstance() {
    return this.injector;
  }

  /**
   * Return 'raw' FormControlFragment.
   * The FormControlFragment in the return list should be decorated with {@link FormControl}.
   * {@see buildForm}
   */
  protected get formFragmentDeclarations(): FormControlFragmentDeclaration[] {
    return [
      {
        name: 'account',
        type: 'text',
        placeholder: 'mobile_or_email',
        validatorFns: [
          LandingValidators.required,
          LandingValidators.minLength(4),
        ],
      },
      {
        name: 'captcha',
        type: 'text',
        placeholder: 'captcha',
        validatorFns: [
          LandingValidators.required,
          LandingValidators.fixedLength(4),
        ],
        addon: 'captcha',
      },
    ];
  }

  get submitDisabled(): boolean {
    return !this.formGroup.valid || this.submitting;
  }

  sendCaptchaCode() {
    if (this.submitDisabled) {
      return;
    }
    const payload = this.formModel;
    this.submitting = true;
    return this.http
      .request('/ajax/user/sendCaptchaCode', {
        method: 'POST',
        body: payload,
      })
      .then(res => {
        if (res.captcha) {
          (this.getCaptchaFragment().addon as CaptchaImgComponent).captchaSrc =
            res.image_url;
          throw [{ code: ERROR_CODE.invalidCaptcha }];
        } else {
          // redirect to reset page
          return this.router.navigate([
            'landing/forget-password/reset',
            this.formModel.account,
          ]);
        }
      })
      .catch((errors: any[]) => {
        this.errorMessage = '';

        const firstError = errors.find(error => {
          return Object.values(ERROR_CODE).some(code => error.code === code);
        });

        this.errorMessage = firstError
          ? this.translate.get(firstError.code)
          : errors[0].message;
      })
      .then(() => {
        this.submitting = false;
      });
  }

  private getCaptchaFragment() {
    return this.fragments.find(
      item =>
        item.formControlFragment === this.formControlFragmentMap['captcha'],
    );
  }
}
