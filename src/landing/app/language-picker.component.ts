import { Component } from '@angular/core';

import { TranslateService } from 'app/translate/translate.service';

@Component({
  selector: 'rld-language-picker',
  template: `
    <div
      class="rld-language-picker"
      [auiTooltip]="'language_switcher_tooltip' | translate"
      auiTooltipPosition="start center"
    >
      <aui-icon [icon]="icon"></aui-icon>
      <div (click)="switchLang()">
        {{ nextLang | translate }}
      </div>
    </div>
  `,
  styles: [
    `
      .rld-language-picker {
        display: flex;
        align-items: center;
        height: 20px;
        line-height: 20px;
        font-size: 14px;
        color: #999;
        cursor: pointer;
        border-radius: 2px;
        padding: 0 4px;
      }

      .rld-language-picker:hover {
        color: #333;
      }

      aui-icon {
        font-size: 16px;
        margin-right: 4px;
      }

      .rld-language-picker div {
        margin-right: 4px;
      }
    `,
  ],
})
export class LanguagePickerComponent {
  constructor(private translate: TranslateService) {}

  switchLang() {
    this.translate.changeLanguage();
  }

  get nextLang() {
    return `lang_${this.translate.currentLang === 'zh_cn' ? 'en' : 'zh_cn'}`;
  }

  get icon() {
    return `basic:switch_to_${
      this.translate.currentLang === 'zh_cn' ? 'english' : 'chinese'
    }`;
  }
}
