import { NotificationService } from '@alauda/ui';
import {
  Component,
  Inject,
  Injector,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChildren,
} from '@angular/core';
import { AbstractControl } from '@angular/forms';
import { ActivatedRoute, Router } from '@angular/router';

import { get } from 'lodash-es';
import { Observable, Subscription } from 'rxjs';
import { map, take } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { initStateInjectToken } from 'app/utils/bootstrap';
import { BaseFormComponent } from '../../common/base-form-component';
import { CaptchaImgComponent } from '../../common/captcha-img.component';
import {
  FormControlFragmentComponent,
  FormControlFragmentDeclaration,
} from '../../common/form-control-fragment.component';
import { HttpService } from '../../common/http.service';
import { LandingValidators as Validators } from '../../common/validators';
import { Environments, OidcConfig } from '../../types';

const DEFAULT_NAMESPACE_KEY = 'alauda_default_namespace';

interface LoginFormModel {
  organization?: string; // This is actually the 'Account' field in the template when loginMode === 'sub'
  account: string; // 'Username' in 'sub' mode, and 'account' in 'root' mode
  password: string;
  captcha?: string;
}

// Login as a sub account or as a root account or a super mode for third part choose
enum LoginMode {
  sub = 'sub',
  root = 'root',
}

const CAPTCHA_ERRORS = ['invalid_captcha'];

const PASSWORD_ERRORS = [
  'provided_credentials_not_correct',
  'incorrect_authentication_credentials',
];

const TIMEOUT_ERROR = 'timeout_error';

@Component({
  styleUrls: [
    'login.component.scss',
    '../../common/style/form.style.scss',
    '../../common/style/utility.style.scss',
  ],
  templateUrl: 'login.component.html',
})
export class LoginComponent extends BaseFormComponent<LoginFormModel>
  implements OnInit, OnDestroy {
  @ViewChildren(FormControlFragmentComponent)
  fragments: QueryList<FormControlFragmentComponent>;
  LoginMode = LoginMode; // Enum cannot be declared in a class
  loginMode: LoginMode;
  loginMode$: Observable<LoginMode>;
  requiresCaptcha: boolean;
  defaultNamespace: string;
  _next = '';

  errorMsg: string;

  private loginModeSubscriber: Subscription;

  constructor(
    private injector: Injector,
    private auiNotificationService: NotificationService,
    private http: HttpService,
    private translate: TranslateService,
    private route: ActivatedRoute,
    private router: Router,
    @Inject(ENVIRONMENTS) public ENVIRONMENTS: Environments,
  ) {
    super();
  }

  ngOnInit(): void {
    this.loginMode$ = this.route.params.pipe(
      map(params => LoginMode[params['mode'] as string]),
    );

    this.defaultNamespace =
      this.ENVIRONMENTS.predefined_login_organization ||
      localStorage.getItem(DEFAULT_NAMESPACE_KEY);
    this.submitting = false;
    this.submitted = false;
    this.formModel = { organization: '', account: '', password: '' };

    this.buildForm();

    this.loginModeSubscriber = this.loginMode$.subscribe(mode => {
      this.setLoginMode(mode);
    });

    this.route.queryParams.pipe(take(1)).subscribe(params => {
      this._next = params['next'] || '';
    });
  }

  ngOnDestroy(): void {
    this.loginModeSubscriber.unsubscribe();
  }

  get loginDisabled() {
    return this.submitted && (!this.formGroup.valid || this.submitting);
  }

  get headerInfo() {
    return this.loginMode === LoginMode.sub ? 'user_login' : 'org_login';
  }

  get loginSwitchLink() {
    return this.loginMode === LoginMode.sub
      ? '/landing/login/root'
      : '/landing/login/sub';
  }

  get loginSwitchLinkText() {
    return this.loginMode !== LoginMode.sub ? 'user_login' : 'org_login';
  }

  checkRequiresCaptcha() {
    const params = Object.assign({ mode: this.loginMode });
    return this.http
      .request('/ajax/landing/login/requires-captcha', {
        method: 'GET',
        params,
      })
      .then(({ result }) => {
        this.requiresCaptcha = result;
        // captcha control影响了formGroup的验证状态
        if (result) {
          this.formControlFragmentMap['captcha'].control.enable();
        } else {
          this.formControlFragmentMap['captcha'].control.disable();
        }
      })
      .catch(errors => {
        console.error('Refresh captcha failed: ' + JSON.stringify(errors));
      })
      .then(() => {
        if (
          this.requiresCaptcha &&
          !this.formControlFragmentMap['captcha'].error
        ) {
          return (this.getCaptchaFragment()
            .addon as CaptchaImgComponent).refresh();
        }
      });
  }

  /**
   * Switch between different login modes.
   */
  setLoginMode(mode: LoginMode) {
    if (this.loginMode !== mode) {
      this.loginMode = mode;
      this.checkRequiresCaptcha();
      this.resetForm();
    }
  }

  /**
   * Handles user login state
   */
  async login(): Promise<void> {
    const msg = this.checkForm();
    if (msg) {
      this.errorMsg = msg;
      return;
    }
    this.submitted = true;
    if (this.loginDisabled) {
      return;
    }
    // Add all query parameters to the payload, including next
    const payload = Object.assign(
      { mode: this.loginMode },
      this.formModel,
      this.route.snapshot.queryParams,
    );

    // login to django first, captcha request from django
    this.submitting = true;
    this.disableForm();

    await this.http
      .request('/ajax/landing/login/', {
        method: 'POST',
        body: payload,
      })
      .then(async ({ url }) => {
        localStorage.setItem(
          DEFAULT_NAMESPACE_KEY,
          this.formModel.organization || this.formModel.account,
        );
        console.log('redirecting to ' + url);
        //only from login can tip  lincese modal
        localStorage.setItem('fromLogin', 'true');
        if (url.startsWith('http')) {
          return (window.location = url);
        }

        const observer = new MutationObserver(async () => {
          await initStateInjectToken();
          this.translate.refresh();
          this.router.navigateByUrl(url);
          observer.disconnect();
        });

        observer.observe(
          document.getElementsByClassName('alauda-settings')[0],
          {
            childList: true,
            characterData: true,
            subtree: true,
          },
        );

        this.initLoginAccountDoc(
          this.loginMode === LoginMode.sub
            ? {
                namespace: this.formModel.organization,
                username: this.formModel.account,
              }
            : {
                namespace: this.formModel.account,
                username: '',
              },
        );
      })
      .catch((errors: any[]) => {
        this.enableForm();

        if (
          !!errors &&
          errors.find(error => error.code === 'user_not_active')
        ) {
          this.router.navigate([
            'landing/register-complete',
            { fromLogin: true },
          ]);
        }

        console.error('login failed: ' + JSON.stringify(errors));

        if (errors) {
          errors.forEach(error => {
            if (
              ![TIMEOUT_ERROR, ...CAPTCHA_ERRORS, ...PASSWORD_ERRORS].includes(
                error.code,
              )
            ) {
              this.auiNotificationService.error({
                content: this.translate.get(error.code),
              });
            }
          });

          if (errors.some(error => CAPTCHA_ERRORS.includes(error.code))) {
            this.errorMsg = this.translate.get('invalid_captcha');
          } else if (
            errors.some(error => PASSWORD_ERRORS.includes(error.code))
          ) {
            this.errorMsg = this.translate.get('invalid_password');
          }

          this.submitting = false;

          // Whenever user inputs an incorrect password, his captcha will be updated.
          // Hence the current captcha input is invalid and we should reset it.
          if (this.formControlFragmentMap['password'].error) {
            this.formControlFragmentMap['captcha'].control.reset();
          }
        }
        return this.checkRequiresCaptcha();
      });
  }

  initLoginAccountDoc(account: Account) {
    document.getElementById('console-username').innerText = account.username;
    document.getElementById('cp-namespace').innerText = account.namespace;
  }

  /**
   * Reset form values, touched states, etc.
   */
  resetForm() {
    if (this.formGroup) {
      this.buildForm();
      this.formGroup.reset();
      if (this.loginMode === LoginMode.sub) {
        this.organizationControl.setValue(this.defaultNamespace);
      }
    }
  }

  protected get injectorInstance() {
    return this.injector;
  }

  protected get formFragmentDeclarations(): FormControlFragmentDeclaration[] {
    const self = this;
    const declarations: FormControlFragmentDeclaration[] = [
      {
        name: 'organization',
        type: 'text',
        placeholder: 'landing_org_name',
        validatorFns: [Validators.organizationOrAccountRequired],
        hidden:
          self.loginMode === LoginMode.root ||
          (self.loginMode === LoginMode.sub &&
            !!this.ENVIRONMENTS.predefined_login_organization),
      },
      {
        name: 'account',
        type: 'text',
        get placeholder() {
          return self.loginMode === LoginMode.sub
            ? 'username'
            : 'landing_org_name';
        },
        validatorFns: [Validators.organizationOrAccountRequired],
      },
      {
        name: 'password',
        type: 'password',
        placeholder: 'password',
        validatorFns: [Validators.passwordRequired],
      },
      {
        name: 'captcha',
        type: 'text',
        placeholder: 'captcha',
        validatorFns: [Validators.captchaRequired],
        addon: 'captcha',
        get hidden() {
          return !self.requiresCaptcha;
        },
      },
    ];

    return declarations;
  }

  private getCaptchaFragment() {
    return this.fragments.find(
      item =>
        item.formControlFragment === this.formControlFragmentMap['captcha'],
    );
  }

  private get organizationControl(): AbstractControl {
    const controlName =
      this.loginMode === LoginMode.sub ? 'organization' : 'account';
    return this.formControlFragmentMap[controlName].control;
  }

  get thirdParties() {
    return get(this.ENVIRONMENTS, 'sso_configuration.configs', []);
  }

  tpLogin(item: OidcConfig) {
    localStorage.setItem('curr_sso_name', item.source_id);
    location.href = item.auth_url;
  }
}
