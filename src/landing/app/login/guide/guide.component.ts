import { Component, Inject, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { get } from 'lodash-es';
import { take } from 'rxjs/operators';

import { ENVIRONMENTS } from 'app/shared/tokens';
import { Environments, LoginPattern, OidcConfig } from '../../types';

@Component({
  templateUrl: './guide.component.html',
  styleUrls: ['./guide.component.scss', '../../common/style/form.style.scss'],
})
export class GuideComponent implements OnInit {
  _next = '';
  constructor(
    @Inject(ENVIRONMENTS) public ENVIRONMENTS: Environments,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit() {
    this.route.queryParams.pipe(take(1)).subscribe(params => {
      this._next = params['next'] || '';
      if (this.ENVIRONMENTS.login_pattern === LoginPattern.PASSWORD) {
        return this.router.navigate(['landing/login/password'], {
          queryParams: {
            next: this._next,
          },
        });
      }
      if (!this.ENVIRONMENTS.sso_guide_page_on) {
        return this.router.navigate(['landing/login/sub'], {
          queryParams: {
            next: this._next,
          },
        });
      }
    });
  }

  tpLogin(item: OidcConfig) {
    localStorage.setItem('curr_sso_name', item.source_id);
    location.href = item.auth_url;
  }

  get firstClassThirdParties() {
    const configs = get(this.ENVIRONMENTS, 'sso_configuration.configs', []);
    return configs.length > 2 ? configs.slice(0, 2) : configs;
  }

  get secondClassThirdParties() {
    const configs = get(this.ENVIRONMENTS, 'sso_configuration.configs', []);
    return configs.length > 2 ? configs.slice(2) : [];
  }
}
