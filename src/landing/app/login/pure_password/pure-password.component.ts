import { DialogService, NotificationService } from '@alauda/ui';
import {
  Component,
  Injector,
  OnInit,
  QueryList,
  TemplateRef,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { NgForm, NgModel } from '@angular/forms';
import { Router } from '@angular/router';

import { TranslateService } from 'app/translate/translate.service';
import { Account } from 'app/typings';
import { initStateInjectToken } from 'app/utils/bootstrap';
import { BaseFormComponent } from '../../common/base-form-component';
import { CaptchaImgComponent } from '../../common/captcha-img.component';
import {
  FormControlFragmentComponent,
  FormControlFragmentDeclaration,
} from '../../common/form-control-fragment.component';
import { HttpService } from '../../common/http.service';
import { LandingValidators as Validators } from '../../common/validators';
interface LoginFormModel {
  account: string;
  password: string;
  newPassword?: string;
  confirmPassword?: string;
}

@Component({
  templateUrl: './pure-password.component.html',
  styleUrls: [
    './pure-password.component.scss',
    '../../common/style/utility.style.scss',
  ],
})
export class PurePasswordLoginComponent
  extends BaseFormComponent<LoginFormModel>
  implements OnInit {
  requiresCaptcha: boolean;
  loading = false;
  pwdLoading = false;

  @ViewChild('form')
  form: NgForm;
  @ViewChild('password')
  password: NgModel;
  @ViewChild('template')
  template: TemplateRef<any>;
  @ViewChildren(FormControlFragmentComponent)
  fragments: QueryList<FormControlFragmentComponent>;

  accountFocused = false;
  passwordFocused = false;
  errorMsg: string;
  pwdVisible = false;

  constructor(
    private injector: Injector,
    private http: HttpService,
    private auiNotificationService: NotificationService,
    private dialogService: DialogService,
    private translate: TranslateService,
    private router: Router,
  ) {
    super();
  }

  ngOnInit() {
    this.submitting = false;
    this.submitted = false;
    this.formModel = {
      account: '',
      password: '',
      confirmPassword: '',
    };
    this.checkRequiresCaptcha();
    this.buildForm();
  }

  async login() {
    const msg = this.checkForm();
    if (msg) {
      this.errorMsg = msg;
      return;
    }

    this.submitted = true;
    if (this.loginDisabled) {
      return;
    }

    const payload = Object.assign({}, this.formModel, {
      grant_type: 'password',
    });

    this.disableForm();

    try {
      this.loading = true;
      const {
        url,
        isFirstLogin,
        username,
        namespace,
      } = await this.http.request('/ajax/landing/login/password', {
        method: 'POST',
        body: payload,
      });
      if (isFirstLogin) {
        return this.dialogService.open(this.template);
      }
      console.log('redirecting to ' + url);
      //only from login can tip  lincese modal
      localStorage.setItem('fromLogin', 'true');

      this.initLoginAccountDoc({
        namespace,
        username,
      });
      await initStateInjectToken();
      this.translate.refresh();
      this.router.navigateByUrl(url);
    } catch (errors) {
      this.enableForm();
      console.error('login failed: ' + JSON.stringify(errors));
      if (errors && errors[0].code && errors[0].code === 'unauthorized') {
        this.errorMsg = this.translate.get('invalid_password');
        return;
      }
      this.errorMsg = this.translate.get(errors[0].code);
      this.checkRequiresCaptcha();
    } finally {
      this.loading = false;
    }
  }

  checkRequiresCaptcha() {
    return this.http
      .request('/ajax/landing/login/requires-captcha', {
        method: 'GET',
      })
      .then(({ result }) => {
        this.requiresCaptcha = result;
        if (result) {
          this.formControlFragmentMap['captcha'].control.enable();
        } else {
          this.formControlFragmentMap['captcha'].control.disable();
        }
      })
      .catch(errors => {
        console.error('Refresh captcha failed: ' + JSON.stringify(errors));
      })
      .then(() => {
        if (
          this.requiresCaptcha &&
          !this.formControlFragmentMap['captcha'].error
        ) {
          return (this.getCaptchaFragment()
            .addon as CaptchaImgComponent).refresh();
        }
      });
  }

  private getCaptchaFragment() {
    return this.fragments.find(
      item =>
        item.formControlFragment === this.formControlFragmentMap['captcha'],
    );
  }

  initLoginAccountDoc(account: Account) {
    document.getElementById('console-username').innerText = account.username;
    document.getElementById('cp-namespace').innerText = account.namespace;
  }

  protected get injectorInstance() {
    return this.injector;
  }

  protected get formFragmentDeclarations(): FormControlFragmentDeclaration[] {
    const self = this;
    const declarations: FormControlFragmentDeclaration[] = [
      {
        name: 'account',
        type: 'text',
        placeholder: 'username',
        validatorFns: [Validators.organizationOrAccountRequired],
      },
      {
        name: 'password',
        type: 'password',
        placeholder: 'password',
        validatorFns: [Validators.passwordRequired],
      },
      {
        name: 'captcha',
        type: 'text',
        placeholder: 'captcha',
        validatorFns: [Validators.captchaRequired],
        addon: 'captcha',
        get hidden() {
          return !self.requiresCaptcha;
        },
      },
    ];

    return declarations;
  }

  get loginDisabled() {
    return this.submitted && (!this.formGroup.valid || this.submitting);
  }

  async saveNewPassword(old_password: string, password: string) {
    try {
      this.pwdLoading = true;
      const { url } = await this.http.request(
        '/ajax/landing/login/password/change-password',
        {
          method: 'PUT',
          body: {
            old_password,
            password,
          },
        },
      );
      console.log('redirecting to ' + url);
      //only from login can tip  lincese modal
      localStorage.setItem('fromLogin', 'true');
      location.replace(url);
    } catch (errors) {
      console.error('login failed: ' + JSON.stringify(errors));
      if (errors && errors[0].code && errors[0].code === 'unauthorized') {
        this.auiNotificationService.error({
          title: this.translate.get('invalid_password'),
        });
        return;
      }

      this.auiNotificationService.error({
        title: this.translate.get(errors[0].code),
        content: this.translate.get(errors[0].message),
      });
    } finally {
      this.pwdLoading = false;
    }
  }

  get cPwdPattern() {
    return this.formModel.newPassword === this.formModel.confirmPassword
      ? /\.*/
      : /\.{,0}/;
  }
}
