import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';

import { first } from 'rxjs/operators';

import { HttpService } from '../../common/http.service';

/**
 * Handles OpenID Connect login request
 */
@Component({
  styleUrls: ['oidc-login.component.scss'],
  templateUrl: 'oidc-login.component.html',
})
export class OidcLoginComponent implements OnInit {
  constructor(
    private http: HttpService,
    private route: ActivatedRoute,
    private router: Router,
  ) {}

  ngOnInit(): void {
    this.route.fragment.pipe(first()).subscribe(fragment => {
      this.http
        .request('/ajax/landing/oidc-login/', {
          method: 'POST',
          body: this.decodeFragment(fragment),
        })
        .then(({ url }) => {
          console.log('redirecting to ' + url);
          location.replace(url);
        })
        .catch(errors => {
          console.error('Failed to login: ', errors);
          this.router.navigate(['landing/validation-failure']);
        });
    });
  }

  private decodeFragment(fragment: string) {
    return (/^[?#]/.test(fragment) ? fragment.slice(1) : fragment)
      .split('&')
      .reduce((params, param) => {
        const [key, value] = param.split('=');
        params[key] = value
          ? decodeURIComponent(value.replace(/\+/g, ' '))
          : '';
        return params;
      }, {});
  }
}
