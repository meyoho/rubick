import {
  DropdownModule,
  IconModule,
  IconRegistryService,
  InputModule,
  NotificationModule,
  NotificationService,
  TabsModule,
  TooltipModule,
} from '@alauda/ui';
import { HttpClientModule } from '@angular/common/http';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { MenuTriggerModule } from 'app2/shared/components/menu-trigger/menu-trigger.module';
import { ShellComponent } from 'terminal/app/shell/shell.component';
import { TerminalTranslateModule } from 'terminal/app/translate/translate.module';
import { TranslateService } from 'terminal/app/translate/translate.service';
import { WebShellComponent } from 'terminal/app/webshell/shell.component';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { TerminalComponent } from './terminal/terminal.component';

const AuiModules = [
  DropdownModule,
  IconModule,
  InputModule,
  TooltipModule,
  NotificationModule,
  TabsModule,
];
const Services = [TranslateService, NotificationService];
const basicIconsUrl = require('@alauda/ui/assets/basic-icons.svg');

@NgModule({
  declarations: [
    AppComponent,
    TerminalComponent,
    ShellComponent,
    WebShellComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    FormsModule,
    TerminalTranslateModule,
    BrowserAnimationsModule,
    HttpClientModule,
    MenuTriggerModule,
    ...AuiModules,
  ],
  providers: [...Services],
  bootstrap: [AppComponent],
})
export class AppModule {
  constructor(iconRegistryService: IconRegistryService) {
    iconRegistryService.registrySvgSymbolsByUrl(basicIconsUrl);
  }
}
