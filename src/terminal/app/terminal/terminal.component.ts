import { animate, style, transition, trigger } from '@angular/animations';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  OnDestroy,
  OnInit,
  QueryList,
  ViewChild,
  ViewChildren,
} from '@angular/core';
import { ActivatedRoute } from '@angular/router';

import { Subscription } from 'rxjs';
import { findNext, findPrevious } from 'xterm/lib/addons/search/search';

import { ShellComponent } from 'terminal/app/shell/shell.component';
import { TerminalPageParams } from 'terminal/app/type';
import { WebShellComponent } from 'terminal/app/webshell/shell.component';
const uuidv1 = require('uuid/v1');

const SHELL_THEME_KEY = 'rc-shell-theme';
interface PodSid {
  pod: string;
  id: string;
}

@Component({
  selector: 'rc-terminal',
  templateUrl: './terminal.component.html',
  styleUrls: ['./terminal.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
  animations: [
    trigger('zoomInOut', [
      transition(':enter', [
        style({ transform: 'scale(0.95)', opacity: '0' }),
        animate(200, style({ transform: 'scale(1)', opacity: '1' })),
      ]),
      transition(':leave', [
        style({ transform: 'scale(1)', opacity: '1' }),
        animate(200, style({ transform: 'scale(0.95)', opacity: '0' })),
      ]),
    ]),
  ],
})
export class TerminalComponent implements OnInit, AfterViewInit, OnDestroy {
  @ViewChildren(ShellComponent)
  execshells: QueryList<ShellComponent>;
  @ViewChild(WebShellComponent)
  webshell: WebShellComponent;
  @ViewChild('searchInput', { read: ElementRef })
  searchInput: ElementRef;

  params: TerminalPageParams;
  shell: ShellComponent | WebShellComponent;
  podsBackup: string[] = [];
  isFindbarActive = false;
  searchQuery = '';
  keys = Object.keys;
  activeIndex: number;
  podSid: PodSid[] = [];
  pods: string[] = [];
  url: string;
  target: string;
  theme: 'light' | 'dark';
  isDarkTheme = false;

  private keySub: Subscription;

  constructor(
    private activatedRoute: ActivatedRoute,
    private cdr: ChangeDetectorRef,
    private http: HttpClient,
  ) {}

  async ngOnInit() {
    this.setTheme(
      (localStorage.getItem(SHELL_THEME_KEY) as 'dark' | 'light') || 'dark',
    );
    this.params = this.activatedRoute.snapshot
      .queryParams as TerminalPageParams;
    switch (this.params.type) {
      case 'exec':
        try {
          const pods = this.params.pods.split(';');
          this.activeIndex = 0;
          this.pods = this.params.selectedPod
            ? [this.params.selectedPod]
            : [...pods];
          this.podsBackup = [...pods];
          this.podSid = await Promise.all(
            this.podsBackup.map(pod => {
              return this.getSessionId(pod, this.params);
            }),
          );
          this.cdr.markForCheck();
        } catch (err) {}
        break;
      case 'webssh':
        this.url = decodeURIComponent(this.params.url);
        this.target = decodeURIComponent(this.params.target);
        break;
    }
  }

  ngAfterViewInit(): void {
    // Capturing Ctrl keyboard events in Javascript
    window.addEventListener('keydown', event => {
      this.onKeyEvent(event);
    });

    switch (this.params.type) {
      case 'exec':
        this.shell = this.execshells.find(item => {
          return item.podName === this.pods[this.activeIndex];
        });
        break;
      case 'webssh':
        this.shell = this.webshell;
        break;
    }
    this.keySub = this.shell.keyEvent$.subscribe(event => {
      this.onKeyEvent(event);
    });
  }

  ngOnDestroy() {
    this.keySub.unsubscribe();
  }

  setTheme(theme: 'dark' | 'light') {
    this.isDarkTheme = theme === 'dark';
    localStorage.setItem(SHELL_THEME_KEY, theme);
    this.theme = theme;
    this.cdr.markForCheck();
  }

  toogleTheme() {
    this.setTheme(this.isDarkTheme ? 'light' : 'dark');
  }

  onKeyEvent(event: KeyboardEvent) {
    if ((event.metaKey || event.ctrlKey) && event.code === 'KeyF') {
      event.preventDefault();
      event.stopPropagation();
      this.toggleFindPanel();
    }
  }

  toggleFindPanel() {
    this.isFindbarActive = !this.isFindbarActive;
    this.searchQuery = '';
    this.cdr.markForCheck();
  }

  findPrevious(query: string) {
    findPrevious(this.shell.term, query, null);
  }

  findNext(query: string) {
    findNext(this.shell.term, query);
  }

  focusSearchInput() {
    if (this.isFindbarActive) {
      this.searchInput.nativeElement.focus();
    } else {
      this.shell.term.focus();
    }
  }

  getPodShortName(podFullName: string) {
    return podFullName.substring(this.params.ctl.length + 1);
  }

  isActiveMenuItem(pod: string) {
    return pod === this.pods[this.activeIndex];
  }

  getPodSid(pod: string) {
    const item = this.podSid.find(item => item.pod === pod);
    return item ? item.id : '';
  }

  async close(index: number) {
    this.pods.splice(index, 1);
  }

  async selectIndex(pod: string) {
    const selectIndex = this.pods.findIndex(_pod => _pod === pod);
    if (selectIndex !== this.activeIndex) {
      if (selectIndex < 0) {
        this.pods.push(pod);
        this.activeIndex = this.pods.length - 1;
      } else {
        this.activeIndex = selectIndex;
      }
    }
  }

  async onTabIndexChange(index: number) {
    this.keySub.unsubscribe();
    this.shell = this.execshells.find(item => {
      return item.podName === this.pods[index];
    });
    this.keySub = this.shell.keyEvent$.subscribe(event => {
      this.onKeyEvent(event);
    });
    this.activeIndex = index;
  }

  getSessionId(pod: string, params: TerminalPageParams) {
    return this.request(
      `/ajax/v2/misc/clusters/${params.cluster}/exec/${
        params.namespace
      }/${pod}/${params.container}?user_name=${params.user}`,
      {
        method: 'POST',
      },
    ).then(({ id }) => {
      return { pod, id };
    });
  }

  request(url: string, requestOptions: { method: string }): Promise<any> {
    const { method, options } = this.prepareRequest(requestOptions);
    const requestPromise = this.http.request(method, url, options).toPromise();
    return requestPromise;
  }

  private prepareRequest(requestOptions?: any) {
    const HEADER_REQUEST_ID = 'Alauda-Request-ID';
    const HEADER_AJAX_REQUEST = 'RUBICK-AJAX-REQUEST';
    const HEADER_NO_JSON_PARSE = 'no-json';
    const requestHeaders = requestOptions['headers'];

    let request_id =
      (requestHeaders && requestHeaders[HEADER_REQUEST_ID]) || '';

    if (!request_id) {
      const buffer = new Array(32);
      uuidv1(null, buffer, 0).forEach((item: any) => {
        request_id += item.toString(16);
      });
    }

    let headers = new HttpHeaders()
      .set(HEADER_REQUEST_ID, request_id)
      .set(HEADER_AJAX_REQUEST, 'true');
    if (
      requestOptions.responseType &&
      (requestOptions.responseType === 'blob' ||
        requestOptions.responseType === 'arraybuffer')
    ) {
      headers = headers.set(HEADER_NO_JSON_PARSE, 'true');
    }

    return {
      method: requestOptions.method || 'GET',
      options: {
        body: requestOptions.body,
        responseType: requestOptions.responseType,
        headers,
      },
    };
  }
}
