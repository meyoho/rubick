export interface TerminalResponse {
  id: string;
}

export interface WSSimpleEvent {
  type: string;
  toString(): string;
}

export interface WSCloseEvent extends WSSimpleEvent {
  code: number;
  reason: string;
  wasClean: boolean;
}

export interface WSMessageEvent extends WSSimpleEvent {
  data: string;
}

export interface ShellFrame {
  Op: string;
  Data?: any;
  SessionID?: string;
  Rows?: number;
  Cols?: number;
}

export interface TerminalPageParams {
  app?: string;
  pods: string; // 多个pod 使用 ';' 分割
  selectedPod?: string;
  container: string;
  namespace: string;
  cluster: string;
  command?: string;
  user?: string;
  ctl?: string;
  url?: string;
  target?: string;
  type?: 'exec' | 'webssh';
}
