import { NotificationService } from '@alauda/ui';
import {
  AfterViewInit,
  ChangeDetectionStrategy,
  ChangeDetectorRef,
  Component,
  ElementRef,
  Input,
  OnChanges,
  OnDestroy,
  SimpleChanges,
  ViewChild,
} from '@angular/core';

import { debounce } from 'lodash-es';
import { ReplaySubject } from 'rxjs';
import { ITheme, Terminal } from 'xterm';
import * as attach from 'xterm/lib/addons/attach/attach';
import { fit } from 'xterm/lib/addons/fit/fit';

import { TranslateService } from 'terminal/app/translate/translate.service';
import { WSCloseEvent } from 'terminal/app/type';

interface TerminalAddon extends Terminal {
  attach?(socket: any): void;
}

const DARK_THEME: ITheme = {
  background: '#283238',
  foreground: '#d4d4d4',
};
const LIGHT_THEME: ITheme = {
  background: '#fff',
  foreground: '#000',
  cursor: '#00a',
  selection: '#00000033',
};
const SHELL_THEMES = {
  dark: DARK_THEME,
  light: LIGHT_THEME,
};

@Component({
  selector: 'rc-web-shell',
  templateUrl: './shell.component.html',
  styleUrls: ['../shell/shell.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush,
})
export class WebShellComponent implements OnChanges, AfterViewInit, OnDestroy {
  @Input()
  url: string;

  @Input()
  target: string;

  @Input()
  theme: 'light' | 'dark';

  @ViewChild('anchor')
  anchorRef: ElementRef;

  connecting = false;
  connectionClosed = false;

  term: TerminalAddon;

  keyEvent$ = new ReplaySubject<KeyboardEvent>(2);

  private conn: WebSocket;
  private connected = false;
  private debouncedFit: Function;

  constructor(
    private auiNotification: NotificationService,
    private cdr: ChangeDetectorRef,
    private translate: TranslateService,
  ) {}

  ngOnChanges(changes: SimpleChanges): void {
    if (changes.theme && this.term) {
      this.term.setOption('theme', SHELL_THEMES[this.theme]);
    }
  }

  ngAfterViewInit(): void {
    Terminal.applyAddon(attach);
    this.term = new Terminal({
      fontSize: 14,
      fontFamily: 'Consolas, "Courier New", monospace',
      bellStyle: 'sound',
      cursorBlink: true,
    });
    this.term.setOption('theme', SHELL_THEMES[this.theme]);

    this.term.open(this.anchorRef.nativeElement);
    this.debouncedFit = debounce(() => {
      fit(this.term);
      this.cdr.markForCheck();
    }, 100);
    this.debouncedFit();
    window.addEventListener('resize', () => this.debouncedFit());

    this.term.on('key', (_, event) => {
      this.keyEvent$.next(event);
    });
    this.setupConnection(true);

    this.cdr.markForCheck();
  }

  ngOnDestroy() {
    if (this.conn && this.connected) {
      this.conn.close();
    }

    if (this.term) {
      this.term.destroy();
    }
  }

  /**
   * 建立websocket连接
   * @param proxy
   * proxy为true时通过nginx转发/socket/webssh
   * proxy为false时直接在浏览器与ws服务器建立连接
   */
  private setupConnection(proxy: boolean) {
    this.connecting = true;
    this.connectionClosed = false;

    // Should only have one connection per component:
    if (this.conn) {
      this.conn.close();
    }

    // 判断当前的http协议是否加密，如果加密，websocket使用wss
    const protocol = window.location.protocol === 'https:' ? 'wss://' : 'ws://';
    const url = this.target + this.url + '?cols=96&rows=5';
    if (proxy) {
      this.conn = new WebSocket(
        protocol + window.location.host + '/socket/webssh/' + url,
      );
    } else {
      this.conn = new WebSocket(protocol + url);
    }
    this.term.attach(this.conn);
    this.conn.onopen = this.onConnectionOpen.bind(this);
    this.conn.onclose = this.onConnectionClose.bind(this);
    this.conn.onerror = function(e) {
      console.error('Socket error:', e);
    };
    this.cdr.markForCheck();
  }

  private onConnectionOpen() {
    this.connected = true;
    this.connecting = false;
    this.connectionClosed = false;

    // Focus on connection
    this.term.focus();

    this.cdr.markForCheck();
  }

  private onConnectionClose(evt: WSCloseEvent) {
    if (!this.connected) {
      return;
    }
    this.conn.close();
    this.connected = false;
    this.connecting = false;
    this.connectionClosed = true;
    this.cdr.markForCheck();

    // 1006错误: ws连接非正常关闭 https://stackoverflow.com/a/19305172
    if (evt.code === 1006) {
      setTimeout(() => {
        this.setupConnection(false);
      }, 1000);
    } else {
      this.auiNotification.error({
        content: this.translate.get('exec_disconnected'),
      });
    }
  }
}
