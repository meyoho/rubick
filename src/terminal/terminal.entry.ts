import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';

import { AppModule } from './app/app.module';
import './vendor.entry';

export async function main() {
  return platformBrowserDynamic().bootstrapModule(AppModule);
}

// support async tag or hmr
(function() {
  switch (document.readyState) {
    case 'interactive':
    case 'complete':
      main();
      break;
    case 'loading':
    default:
      document.addEventListener('DOMContentLoaded', () => main());
  }
})();
