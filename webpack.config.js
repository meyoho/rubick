const path = require('path');
const chalk = require('chalk');
const webpack = require('webpack');
const RubickI18nWebpackPlugin = require('rubick-i18n-webpack-plugin');
const HtmlWebpackPlugin = require('html-webpack-plugin');
const HtmlWebpackHarddiskPlugin = require('html-webpack-harddisk-plugin');
const MomentTimezoneDataPlugin = require('moment-timezone-data-webpack-plugin');

function error(e) {
  console.log(chalk.red('[Custom webpack config] ' + e));
}

module.exports = function(config, options) {
  if (process.env.NODE_ENV === 'dev') {
    try {
      const indexHtmlPlugins = config.plugins.filter(
        p => p.constructor.name === 'IndexHtmlWebpackPlugin',
      );
      indexHtmlPlugins.forEach(plugin => {
        config.plugins.push(
          new HtmlWebpackPlugin({
            template: plugin._options.input,
            filename: plugin._options.output,
            alwaysWriteToDisk: true,
            inject: 'body',
            cache: false,
            alwaysWriteToDisk: true,
            showErrors: true,
            chunksSortMode: 'none',
          }),
        );
      });
      config.plugins.push(new HtmlWebpackHarddiskPlugin());
    } catch (e) {
      error(e);
    }
  }

  const currentYear = new Date().getFullYear();

  config.plugins.push(
    new RubickI18nWebpackPlugin({
      entry: [path.resolve('src/**/*.i18n.json')],
      outputDir: path.resolve('dist/static/i18n'),
      manifestBase: '/static/i18n',
    }),
    new webpack.ContextReplacementPlugin(/moment[/\\]locale$/, /zh-ch|en-nz/),
    new MomentTimezoneDataPlugin({
      matchZones: ['Asia/Shanghai'],
      startYear: 2015,
      endYear: currentYear + 3,
    }),
  );

  return config;
};
