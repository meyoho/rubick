Rubick 中使用的公共样式有三种来源：

### rubick 公共样式定义

rubick 中公共样式定义：

> 位于 app/styles 文件夹下

```
@import 'mixins'         // 常用的mixin
@import 'var'            // 色值，字体，border，height，width等各种变量
```

### global 样式

rubick 中的全局公共样式

> 位于/static/styles 文件夹下
> 包含所有 global 的样式定义

### alauda-ui

样式来自@alauda/ui 组件库，**通常在业务的 scss 文件中不会直接 import 这些文件**

1. aui 中使用的色值，字体，border，height，width 等变量，**包含于 app/\_var.scss**

   ```
   '~@alauda/ui/theme/base-var';
   ```

2. aui 中常用的 mixin，**包含于 app/\_mixins.scss**

   ```
   '~@alauda/ui/theme/mixin';
   ```

3. aui 在业务组件中的特殊场景对应的 mixin，以及不同种类的 shadow 定义，**包含于 app/\_mixins.scss**
   ```
   '~@alauda/ui/theme/pattern';
   ```
