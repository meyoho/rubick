> [前端快速入门任务：CN 环境 Rubick 应用部署](http://confluence.alaudatech.com/pages/viewpage.action?pageId=27178544)

# 结构简介

灵雀云服务架构遵循微服务思想。后端架构由数十个微服务组成，每个微服务只负责处理一项或者一种类型的业务或基础架构功能。这些服务有以下特征：

- 一般有一个主要的 Owner，负责整个服务的生命周期，包括设计、开发、代码维护、运维等
- 一般通过 REST 服务进行访问以及跟其他服务进行交互
- 通过 Docker 进行运维管理和测试开发
- 名字一般从 Dota 的英雄或者物品中取，并占用一个 Bitbucket 仓库名额，例如 Rubick, Jakiro, Enigma 等

Rubick 作为前端主要仓库，由 Angular 静态处理 SPA 页面 和 负责用户 Session 状态管理和 API 转发的 Nodejs 中间层 构成。

跟后端不同，前端整个组日常工作只会维护这一个仓库。

仓库地址: bitbucket.org/mathildetech/rubick/

## Rubick 服务基本架构图

> 线上由一个基于 alpine 的 Linux 镜像容器运行

![](images/rubick_architecture.png)

> 开发环境使用 Webpack dev server 启动前端编译，并 Proxy 到 Node (Koa) 本地进程

![](images/rubick_dev.png)

## Nodejs 中间服务层（koa）

- 基于 KOA 库开发，主要负责用户 登录与授权、Session(Cookie)状态管理和、API 转发等功能需求。
- 少数接口有针对业务接口进行数据封装或是表单验证，大部分 ajax 的接口调用都会通过 - login_required 修饰器针对用户的登录状态进行检查。(特殊请求都含有 ajax-sp，可以在 koa/src/routes/routes.ts 中查看)，所有 ajax 请求默认 v1，如果 url 请求不是 v1 则跟随请求版本。
- 登录验证。账号/密码/验证码 普通登陆，验证成功后返回加密 token 存放于 cookie 中
- 通过 Koa 托管静态文件，默认为 `/static` 文件夹。

## Angular 前端静态资源应用（static）

> 注：目录名为 static 的原因是由于 RUBICK 最初是一个基于 Python 框架的应用。迁移到新架构时依然保留了前端目录命名习惯。

前端应用根据功能，在代码级别通过 Webpack 分成多个 bundle，在需要的时候通过 Angular 的路由系统自动懒加载需要的包。

- landing: 登录相关界面
- console: 控制台应用分为两个子应用，分别为：
  - user: 用户视角应用
  - admin: 管理视角应用
- terminal: 独立的 Terminal 应用。
- 以及其他静态资源，如图片（点阵以及矢量图），字体。

## Jakiro

Jakiro 的作用是后端数据 API 接口中枢网管服务，一对多个后端微服务。对我们来说一般这是我们需要接触的唯一后端服务。

Jakiro 的存在意义，是为了解决后端有很多微服务，并且需要有一个统一的入口来做用户身份认证的问题。

- 使用上，用户通过单一的放在 HTTP headers 中的 Authorization 字段进行身份认证。
- 用户登录过程，实际上就是通过账号密码与 Jakiro 的 Token 接口进行交互，之后 Rubick 将会把 Token 放到 Session 中，在接下来的 HTTP 请求中使用 Token 来进行前后端交互。

# 设计思想

## Angular

目前前端的框架技术栈以 Angular 为主，使用 6.1.0+ 版本的 Angular，遵循 Angular 的组件化设计规范。

关于 Angular 的资料，可以参考如下几个网站：

- Angular 官方中文网站：https://angular.cn/  
  Angular 的官方文档时长会进行更新。经常查阅文档是个好习惯。
- Angular In Depth: https://blog.angularindepth.com/  
  比较深入的 Angular 内部运行机制的博客。网上大量的 Angular 相关的文章都会定向到这里，推荐深入阅读。
  建议在查资料的时候尽量选用 Google 搜索英文资料，并且注意版本。
  > 在从 AngularJS 迁移到 Angular 的过程中，Rubick 在相当长的一段时间内采用 AngularJS + Angular 的 Hybrid 架构。具体可以参考 [关于 NGJS => NG 你所要知道的](http://confluence.alaudatech.com/pages/viewpage.action?pageId=23366277)

## 语言和第三方库

语言上统一使用 Typescript 。

代码风格由 TSLint/ Prettier 进行规范，并且 Perfer FP over OO (倾向于函数式编程，而不是面向对象编程)。

以下是几个比较重要的第一、三方库：

- @alauda/ui 为由前端团队共同维护的公共组件库。
- @alauda/code-editor 为独立的代码编辑器组件仓库。
- lodash
- ngx translate  
  负责前端国际化  
  每个大型模块都会有单独的[en/zh_cn].i18n.json，为每个模块定义此模块使用的国际化翻译
- ngrx，Angular 的 Redux-like 状态管理
- RxJS
  - 学会学好 RXJS 是每个 Angular 开发者的必修课。学习 rxjs 有一定门槛，但一定会对你的职业生涯有很大帮助。
  - 复杂状态流管理，可参考：https://github.com/xufei/blog/issues/42
  - 关于 RxJS 的内容，可以参考
    - [RxJS 入门指引和初步应用](https://github.com/xufei/blog/issues/44)
    - [The introduction to Reactive Programming you've been missing](https://gist.github.com/staltz/868e7e9bc2a7b8c1f754)
    - [RxJS 5 Thinking Reactively | Ben Lesh](https://www.youtube.com/watch?v=3LKMwkuK0ZE)
    - https://medium.com/@benlesh
    - [Confluence - Rx.JS 学习资料](http://confluence.alaudatech.com/pages/viewpage.action?pageId=10322062)

## 构建工程化

Rubick 的工程化使用 Webpack 的方案。后期准备重构成基于 Angular CLI 的构建工具链。

主要的 Webpack loader 如下:

- Babel
- ES2015/2016/...
- Typescript
- Sass
- PostCss / Autoprefixer
- 解决部分浏览器 CSS 兼容性问题
- Uglify / Minify
  RubickI18nPlugin 负责处理国际化翻译文件。

线上的构建过程使用了 Docker。

# 项目重构历史

Pre 2015, Birth of Rubick: Python / Django / AngularJS，服务端渲染

2015 ~ 2016 下半年：半服务端渲染 + AngularJS

2017 年初：纯单页应用渲染

2017 年初：UI 风格改变

2017 年初：工程化/模块化重构

2017.6：登录页重构

2017.6 ~ 2018.6：Angular 重构

2018.9: 登陆+控制台+用户视角三个应用合并为一个

2018.10: 完全移除 AngularJS

# 开发环境配置

参考 [Rubick Readme](../README.md)  
三种环境的账号不通用，请联系 mentor 咨询登录方式。

- CN 环境：  
  前端部分由前端 Oncall 进行更新维护，一般来说一周会有两次常规上线更新（周二晚、周五晚）  
  UI :https://cloud.alauda.cn  
  API :https://api-cloud.alauda.cn

- Staging 环境:  
  这个环境 QA 同学负责的预发布环境，组件上线都需要在此环境验证通过才能放行到 CN 环境，我们自己理论上是不能在此环境随意进行变更的  
  UI: https://cloud-staging.alauda.cn  
  API: https://cloud-staging-api.alauda.cn

- 老版 CN 环境（主要用来进行镜像构建）  
  http://enterprise.alauda.cn

除此之外，每个虚拟团队可能还用有自己的测试环境（int 环境），具体地址以及登录方式可以询问虚拟团队负责人。

## 环境变量

主要用途： 用于控制整个 ACE 中部分功能的具体实现。

- 要定义一个新的环境变量并使用，需要在 koa 层 `environments.ts` 中添加对应获取环境变量的代码。
- 在开发环境中，`int.env`中定义了目前所有正在使用的环境变量及其默认值。本地开发环境启动时会自动读取`int.env`设置环境变量。
- 在 rubick 根目录下也可以添加名为 `custom.env` 的文件，用于覆盖部分本地开发运行时的环境变量设置。
- Rubick 中目前所有环境变量汇总可以参考文档：[Rubick 环境变量 等基础配置](http://confluence.alaudatech.com/pages/viewpage.action?pageId=16056425)

## Weblab

主要用途：用于**基于账号级别**控制部分功能开启和关闭状态，其基于账号下 namespace（根账号）来设置和获取。  
控制台页面会通过 weblab 服务获取 weblabs（功能标志位）得到页面中某些功能的可用状态，从而判断页面中某些模块是否要展现给用户。  
另外，控制台前端代码已经封装了 weblab 的获取过程，编写代码时仅需通过 angular 的 WEBLABS 常量获取对应的状态。

#### 新增功能时注意

- 各个环境下的实际信息由 lightkeeper 组件负责。每当前端新开发一个功能暂时不能完全展示给用户时（由产品经理决定），都需要为该功能创建一个 weblab、设置该 weblab 所开启的 namespace 白名单，方式都为通过 lightkeeper 提供的 api 来创建，即手动向http://lightkeeper-int.alauda.cn:8080/v1/weblabs/（注意此处端口为8080）端点发请求即可。详细说明见https://bitbucket.org/mathildetech/lightkeeper。每次新增的标志位要写入int.env并提交到代码仓库中，具体方式见下文。

#### 开发环境中使用方式

- 为开发环境中方便的测试某个功能的开关状态，通过在 int.env 环境变量中定义测试用的 LIGHTKEEPER_OVERRIDDEN_WEBLABS 来控制各个功能的标志位状态，其中每个标志的名字和其开关状态以:分隔，冒号之后的数字为 1 表示功能开启，0 表示功能关闭。不同的标志名字以,分隔。注意当实际功能新增了标志位或者删除了标志位记得提交到代码仓库中。
- 若想在开发环境中获取实际的功能开关状态，则可以删除 int.env 中的环境变量 LIGHTKEEPER_OVERRIDDEN_WEBLABS，删除后，rubick 就能通过 LIGHTKEEPER 服务的端点http://lightkeeper-int.alauda.cn:80/v1/(注意此处端口为80)获取到当前int环境weblab功能的实际状态了。注意删除操作不要提交到代码库中。

> 如何更新 weblab 可以参考 [Oncall 操作：创建/更新 weblab](http://confluence.alaudatech.com/pages/viewpage.action?pageId=27170898)

# 其他

### 流程规范

开发上线流程以及 FAQ [ACE / 大平台 前端流程规范](http://confluence.alaudatech.com/pages/viewpage.action?pageId=9371709)

### Oncall

- [Oncall 管理规范和流程](http://confluence.alaudatech.com/pages/viewpage.action?pageId=36866541)
- [Rubick 轮值 Owner](http://confluence.alaudatech.com/pages/viewpage.action?pageId=35686959)

### 虚拟团队

- [虚拟团队介绍](http://confluence.alaudatech.com/pages/viewpage.action?pageId=25272688)
- [基于前端视角的 虚拟团队 人员分配](http://confluence.alaudatech.com/pages/viewpage.action?pageId=23389687)

### 项目管理

- Jira: http://jira.alaudatech.com/

### 文档地址

- [Alauda Confluence](http://confluence.alaudatech.com)

### 培训资料

- [测试培训资料](http://confluence.alaudatech.com/pages/viewpage.action?pageId=13828277) （测试团队文档里有不少干货，可以看下）
