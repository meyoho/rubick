## koa

基于 KOA 库开发，主要负责用户 登录与授权、Session(Cookie)状态管理和、API 转发等功能需求。  
相关可以参考文档包括：

- [koajs](https://koajs.com/)
- [koa-router](https://github.com/ZijianHe/koa-router)
- [koa-session](https://github.com/koajs/session)
- koa-static
- koa-view  
  ...

## app

app.module 为 rubick 应用入口，包括两个子模块，登录（landing）、控制台（console）。

#### console

控制台模块，其中控制台包括用户视图（app_user）、管理视图（app2）两个模块。

#### abstract

抽象出来的业务逻辑封装。

#### services

注册在在 ConsoleModule 的 Injector 中的 service 定义。由于 Landing 模块和 Console 模块都是懒加载的，在登录后这些 Service 都是全局唯一的。

#### shared

公共的 Component，Directive，Token，Pipes。其中有部分文件定义位于 app2/shared/，由于历史原因还未迁移。

- app/shared/{group}/module.ts
  1. {group}表示某一类的公共组件集合，比如 form-table
  2. 例如 form-table/module.ts 中包含了这一类公共组件的 declaration，并 exports 出来，在需要使用这些组件的 feature module 和 features-shared 里，引入这个模块。

#### features-shared

1. 这个文件夹用来存放在多个业务 module 之间共享的 component 和 module 定义。
2. 这些 module 定义，包含一些业务组件的 declarations，并 exports 出来，命名规则为 feature/shared.module.ts，比如 app/shared.module.ts。

> TODO:
>
> 1. rubick 公共组件替换完成后，移除 shared.module.ts 里的 原 rc- 公共组件声明
> 2. shared.module.ts 里的业务组件声明需要声明到 features-shared/ 下，对应 {feature}/shared.module.ts；然后在依赖这些组件的 feature module 里 import 进来
> 3. feature module 里，AUI 相关的 module imports，也应该按需引入，从而能够移除 shared.module.ts 里的 AUI module imports

### i18n

国际化翻译文件。

### store

NgRx 相关定义。

- facades  
  应用程序和 store 的通信，可以借助 facades 中的 service。facades service 有助于隐藏 store 中的实现细节，提升开发体验。参考：
  - [NgRx + Facades: Better State Management](https://medium.com/@thomasburleson_11450/ngrx-facades-better-state-management-82a04b9a1e39)
  - [NgRx Facades: Pros and Cons](https://auth0.com/blog/ngrx-facades-pros-and-cons/?utm_source=dev&utm_medium=sc&utm_campaign=ngrx_facades_proscons)

### translate

翻译模块，借助 [ngx-translate](https://github.com/ngx-translate/core) 实现。

### typings

基础类型。

- raw-k8s.ts
  包含了 kubernetes 原生资源在前端的类型定义，可以参考 [Kubernetes API](https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/)
- vendor-customer.types.ts  
  定义了目前产品对接的私有客户对应的环境变量值。

### utils

工具函数。

## landing

登录模块

## app_user

用户视图

## app2

管理视图
