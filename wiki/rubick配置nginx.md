# Rubick Nginx 配置

## nginx

Nginx 是异步框架的 web 服务器，也可用于反向代理，负载均衡和 HTTP 缓存。

## rubick 中的 nginx 配置

rubick 的 nginx 配置文件在项目根目录下的 conf 目录中，rubick_koa_nginx.conf 是 nginx 的配置文件，mime.types 表示接受的 mime 类型，

Nginx 配置文件主要分成四部分：main（全局设置）、server（主机设置）、upstream（上游服务器设置，主要为反向代理、负载均衡相关配置）和 location（URL 匹配特定位置后的设置），每部分包含若干个指令。

- main 部分设置的指令将影响其它所有部分的设置；
- server 部分的指令主要用于指定虚拟主机域名、IP 和端口；
- upstream 的指令用于设置一系列的后端服务器，设置反向代理及后端服务器的负载均衡；
- location 部分用于匹配网页位置（比如，根目录“/”,“/images”,等等）。

它们之间的关系：server 继承 main，location 继承 server；upstream 既不会继承指令也不会被继承。它有自己的特殊指令，不需要在其他地方的应用。

### woker_processes 1

在配置文件的顶级 main 部分，worker 角色的工作进程的个数，master 进程是接收并分配请求给 worker 处理。这个数值简单一点可以设置为 cpu 的核数 grep ^processor /proc/cpuinfo | wc -l，也是 auto 值，如果开启了 ssl 和 gzip 更应该设置成与逻辑 CPU 数量一样甚至为 2 倍，可以减少 I/O 操作。如果 nginx 服务器还有其它服务，可以考虑适当减少。

### worker_connections 1024

写在 events 部分。每一个 worker 进程能并发处理（发起）的最大连接数（包含与客户端或后端被代理服务器间等所有连接数）。

nginx 作为 http 服务器的时候：

```
max_clients = worker_processes * worker_connections
```

nginx 作为反向代理服务器的时候：

```
max_clients = worker_processes * worker_connections/4
```

### server 80

监听 HTTP 端口。

### server 443

监听 HTTPS 端口。

```nginx
server {
    #ssl参数
    listen              443 ssl;
    server_name         example.com;
    #证书文件
    ssl_certificate     example.com.crt;
    #私钥文件
    ssl_certificate_key example.com.key;
    ssl_protocols       TLSv1 TLSv1.1 TLSv1.2;
    ssl_ciphers         HIGH:!aNULL:!MD5;
    #...
}
```

证书文件会作为公用实体发送到每台连接到服务器的客戶端，私钥文件作为安全实体，应该被存放在具有一定权限限制的目录文件，并保证 Nginx 主进程有存取权限。命令 ssl_protocols 和 ssl_ciphers 可以用来限制连接只包含 SSL/TLS 的加強版本和算法。

### gzip

gzip 相关配置。

### location

http 服务中，某些特定的 URL 对应的一系列配置项。

1. location /static 静态文件服务。
2. location / 将请求代理代理到 koa。
3. location ^~ "/socket/exec" 代理 WebSocket 请求
4. location /v1/openid/ 代理/v1/openid/。

### proxy_pass http:/backend

将请求转向定义的服务器列表，即反向代理，对应 upstream 负载均衡器。

## nginx 内置全局变量

```
$args ：这个变量等于请求行中的参数，同$query_string
$content_length ： 请求头中的Content-length字段。
$content_type ： 请求头中的Content-Type字段。
$document_root ： 当前请求在root指令中指定的值。
$host ： 请求主机头字段，否则为服务器名称。
$http_user_agent ： 客户端agent信息
$http_cookie ： 客户端cookie信息
$limit_rate ： 这个变量可以限制连接速率。
$request_method ： 客户端请求的动作，通常为GET或POST。
$remote_addr ： 客户端的IP地址。
$remote_port ： 客户端的端口。
$remote_user ： 已经经过Auth Basic Module验证的用户名。
$request_filename ： 当前请求的文件路径，由root或alias指令与URI请求生成。
$scheme ： HTTP方法（如http，https）。
$server_protocol ： 请求使用的协议，通常是HTTP/1.0或HTTP/1.1。
$server_addr ： 服务器地址，在完成一次系统调用后可以确定这个值。
$server_name ： 服务器名称。
$server_port ： 请求到达服务器的端口号。
$request_uri ： 包含请求参数的原始URI，不包含主机名，如：”/foo/bar.php?arg=baz”。
$uri ： 不带请求参数的当前URI，$uri不包含主机名，如”/foo/bar.html”。
$document_uri ： 与$uri相同。
```


参考文献

1. Nginx Beginner’s Guide https://nginx.org/en/docs/beginners_guide.html
2. Nginx 配置浅析 https://juejin.im/entry/59966124f265da249600b026
3. Nginx 配置 HTTPS 服务器 https://aotu.io/notes/2016/08/16/nginx-https/index.html
