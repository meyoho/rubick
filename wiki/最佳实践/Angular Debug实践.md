### 查看组件状态

开发过程中，我们经常会遇到需要查看组件当前状态的问题。一个最简单的方法是通过开发工具选中它，执行如下命令获取到组件实例。
ng.probe($0).componentInstance

### 查看 DI 信息

对与任意元素，可以通过以下命令查看元素的依赖注入令牌。

```
ng.probe($0).providerTokens
```

可以观察到，令牌不光包含组件明确注入的依赖，还包含着这个组件在宿主模板绑定的指令。

一旦有 Token 的话，我们可以进一步获取到依赖注入的对象。比如我想获取到上面 "ControlContainer" 这个指令，可执行：

```
ng.probe($0).injector.get(
ng.probe($0).providerTokens.find(token =>
(token.name || token._desc) === 'ControlContainer'))
```

### 参考资料

- [Angular Debug 实践](https://shimo.im/docs/t8gsadVKjZkpROrg/read)
- [Everything you need to know about debugging Angular applications](https://blog.angularindepth.com/everything-you-need-to-know-about-debugging-angular-applications-d308ed8a51b4)
