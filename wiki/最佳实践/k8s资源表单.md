### 如何上手

- 可以先阅读下这个 ppt https://pengx17.github.io/k8s-form-in-action/docs/#1
- ppt 对应的源码在这里，https://github.com/pengx17/k8s-form-in-action
- 具体实现范例可以参考 link 的 pvc 表单实现，在 src/frontend/app/features/persistentvolumeclaim/form 目录下。（link 代码下载地址 https://bitbucket.org/mathildetech/link/src/master/）
- link 线上地址： https://alaudak8s-prod.alauda.cn/console/#/persistentvolumeclaim/create， 登录方式为邮箱账户，需要权限可以找测试 常冬梅 加下权限。
- 根据需要实现的资源类型，比如 Ingress，阅读一下相关概念和 API 定义，分别在：https://kubernetes.io/docs/concepts/services-networking/ingress/ 和 https://kubernetes.io/docs/reference/generated/kubernetes-api/v1.11/#ingress-v1beta1-extensions
- 在项目里增加对应的资源类型定义，比如 rubick 里的定义在这里 static/app_user/typings/raw-k8s.ts
- 根据资源表单的 UI 设计，决定是否需要增加一个对应的表单模型，比如 rubick 里的定义在这里 static/app_user/typings/form-model.ts
- 根据后端 API 定义的 payload 结构，决定是否增加一个对应的 api 数据模型，比如 rubick 里的定义在这里 static/app_user/typings/backend-api.ts ，一般情况下 API 的 payload 结构如果和 yaml 结构一致不需要定义这个模型
- 开始书写对应的表单组件，根据需要选择继承资源表单的的抽象类，这些抽象类定义在 static/app_user/abstract/ (大多数直接继承 BaseResourceFormGroupComponent 即可)
- 表单组件写好后，可以开始写包括 UI 表单和 YAML 编辑器互转的创建页面，思路参考 link 的代码 src/frontend/app/features/persistentvolumeclaim/mutate-page

### Demo

https://pengx17.github.io/k8s-form-in-action/demo/ 这个 demo 可以比较直观展示出来表单嵌套逻辑、状态，适合初次学习或者归纳 bug 的最小重现步骤。同时还加了个功能，点击表单后自动高亮 yaml 编辑器的相关区域（反向 case 有技术问题待研究）。https://github.com/pengx17/k8s-form-in-action/tree/master/src 代码在这里。

### Reference

[Link: Angular 响应式表单 Kubernetes 对象实战](http://confluence.alaudatech.com/pages/viewpage.action?pageId=27170598)
