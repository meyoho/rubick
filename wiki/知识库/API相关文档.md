### Kubernetes 资源 Yaml 详解

在 Jakiro 提供的 API 中，有一部分 API 是直接操作 Kubernetes 资源的。这类 API 的 payload 和 Kubernetes 资源的原生 YAML 非常类似，但是可能也会包含一些基于我们自己的场景定制的特殊字段。

相关文档可以在这里查阅：

- [New Alauda-Kubernetes Platform Design](http://confluence.alaudatech.com/display/DEV/Platform+Design)
- [Application Crd Explain](http://confluence.alaudatech.com/display/DEV/Application+Crd+Explain)
- [Kubernetes Yaml Explain](http://confluence.alaudatech.com/display/DEV/All+Resources+YAML)

### 批处理

在某些功能实现中，比如在联邦集群中创建一个应用/ConfigMap，用户可以选择在此联邦的多个集群中，使用同一个 payload 同时创建相同的资源。

- 后端 API 提供了批处理的能力，可以允许 Rubick 在发往 Jakiro 的请求中，同时指定多个 request 数据。
- 对应 Jakiro 的 endpoint 为 `/v2/batch/`
- API 详解可参考：[批处理模块 - hydra](http://confluence.alaudatech.com/pages/viewpage.action?pageId=25270070)
