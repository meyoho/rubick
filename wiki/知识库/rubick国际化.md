## Rubick 国际化当中的可定制文案

### 背景

在目前的 rubick 场景当中，存在 project->项目/多租户、namespace->命名空间/环境、space->组这样的定制翻译需求，这里我们需要根据后端 api 返回的定制化文案，来进行解析。

### 方案

这里采取了预解析的方案，即在 ngrx-translate 添加 transtion 的关系之前，将带有占位符的文案进行解析，这里使用的是 lodash 的模板方案，一种类似于书写 EJS template 的方式

```
<%= porject %>、<%= namespace %>、<%= space %>
```

### 案例

我们需要在以前文案的书写基础上，针对带有项目、命名空间、组规则的地方进行调整。

#### 例子

```
"rbac_please_select_project": "请选择项目"
TO
"rbac_please_select_project": "请选择<%= project %>"
```
